import request from '@/utils/request'

export default {

  // 获取缓存监控信息
  getCacheInfo() {
    return request({
      url: `/admin/cache/list`,
      method: 'get'
    })
  }

}
