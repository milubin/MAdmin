import request from '@/utils/request'

export default {

  // 分页系统在线用户列表
  getPageOnlineUsers(query) {
    return request({
      url: `/admin/online`,
      method: 'get',
      params: query
    })
  },
  // 强制用户退出
  forceUserQuit(tokens) {
    return request({
      url: `/admin/online/quit`,
      method: 'post',
      data: tokens
    })
  }


}
