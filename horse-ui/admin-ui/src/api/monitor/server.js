import request from '@/utils/request'

export default {

  // 获取系统服务监控信息
  getServerInfo() {
    return request({
      url: `/admin/server/list`,
      method: 'get'
    })
  }

}
