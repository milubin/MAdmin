import request from '@/utils/request'

export default {

// 获取Gitee提交信息
  getGiteeCommits() {
    return request({
      url: `/common/gitee/commits`,
      method: 'get',
    })
  },

  // 获取区划级联数据
  getDivisionCascader(id) {
    return request({
      url: `/common/division/${id}`,
      method: 'get',
    })
  },

  // 发送短信验证码
  sendSms(phone) {
    return request({
      url: `/common/send/sms`,
      method: 'get',
      params: {phone: phone}
    })
  },

  // 发送邮箱验证码
  sendEmail(email) {
    return request({
      url: `/common/send/email`,
      method: 'get',
      params: {email: email}
    })
  },

  // 后端测试接口
  sendTest(data) {
    return request({
      url: `/test`,
      method: 'post',
      params: {json: data}
    })
  },

}
