import request from '@/utils/request'

export default {

  // MySQL远程备份
  mysqlBackup() {
    return request({
      url: `/admin/tools/mysql/backup`,
      method: 'post',
    });
  },
  // 权限数据比对
  diffPermissions() {
    return request({
      url: `/admin/tools/diff/permission`,
      method: 'get',
    });
  }

}
