import request from '@/utils/request'

export default {

  // 分页获取数据源列表
  getPageDatasource(query) {
    return request({
      url: `/admin/generate/datasource`,
      method: 'get',
      params: query
    });
  },

  // 获取数据源列表
  getDatasourceList() {
    return request({
      url: `/admin/generate/datasource/list`,
      method: 'get'
    });
  },

  // 获取数据源详情
  getDatasource(datasourceId) {
    return request({
      url: `/admin/generate/datasource/${datasourceId}`,
      method: 'get'
    });
  },

  // 新增数据源
  addDataSource(data) {
    return request({
      url: `/admin/generate/datasource`,
      method: 'post',
      data: data
    });
  },

  // 更新数据源
  updateDataSource(data) {
    return request({
      url: `/admin/generate/datasource`,
      method: 'put',
      data: data
    });
  },

  // 更新数据源
  deleteDataSource(datasourceIds) {
    return request({
      url: `/admin/generate/datasource`,
      method: 'delete',
      data: datasourceIds
    });
  },

  // 测试连接数据源
  connectDataSource(data) {
    return request({
      url: `/admin/generate/datasource/connect`,
      method: 'post',
      data: data
    });
  },

  // 输入框实时检索数据源名称
  fetchDatasource(search) {
    return request({
      url: `/admin/generate/datasource/fetch`,
      method: 'get',
      params: {search: search}
    })
  },

  // 获取数据源的所有数据库名称
  getDatabases(data) {
    return request({
      url: `/admin/generate/datasource/databases`,
      method: 'post',
      data: data
    });
  },

  // 获取数据库所有表信息
  getSchemaTables(database, datasourceId) {
    return request({
      url: `/admin/generate/datasource/tables`,
      method: 'post',
      params: {
        database: database,
        datasourceId: datasourceId
      }
    });
  },

  // 分页获取字段类型关联列表
  getPageColumnConfig(query) {
    return request({
      url: `/admin/generate/columnConfig`,
      method: 'get',
      params: query
    });
  },

  // 获取字段类型关联详情
  getColumnConfig(columnConfigId) {
    return request({
      url: `/admin/generate/columnConfig/${columnConfigId}`,
      method: 'get'
    });
  },

  // 获取字段列表: 针对每种类型组装字符串列表, type=0返回DB字段类型列表, type=1返回JAVA字段类型列表
  getColumnConfigType(type) {
    return request({
      url: `/admin/generate/columnConfig/type`,
      method: 'get',
      params: {type: type}
    });
  },

  // 新增字段类型关联
  addColumnConfig(data) {
    return request({
      url: `/admin/generate/columnConfig`,
      method: 'post',
      data: data
    });
  },

  // 更新字段类型关联
  updateColumnConfig(data) {
    return request({
      url: `/admin/generate/columnConfig`,
      method: 'put',
      data: data
    });
  },

  // 删除字段类型关联
  deleteColumnConfig(columnConfigIds) {
    return request({
      url: `/admin/generate/columnConfig`,
      method: 'delete',
      data: columnConfigIds
    });
  },

  // 分页获取业务表配置列表
  getPageTableConfig(query) {
    return request({
      url: `/admin/generate/tableConfig`,
      method: 'get',
      params: query
    });
  },

  // 分页获取业务表配置列表
  getTableConfigList() {
    return request({
      url: `/admin/generate/tableConfig/list`,
      method: 'get'
    });
  },

  // 获取业务表配置详情
  getTableConfig(tableConfigId) {
    return request({
      url: `/admin/generate/tableConfig/${tableConfigId}`,
      method: 'get'
    });
  },

  // 新增业务表配置
  addTableConfig(data) {
    return request({
      url: `/admin/generate/tableConfig`,
      method: 'post',
      data: data
    });
  },

  // 更新业务表配置
  updateTableConfig(data) {
    return request({
      url: `/admin/generate/tableConfig`,
      method: 'put',
      data: data
    });
  },

  // 删除业务表配置
  deleteTableConfig(tableConfigIds) {
    return request({
      url: `/admin/generate/tableConfig`,
      method: 'delete',
      data: tableConfigIds
    });
  },


  // 分页获取业务表列表
  getPageTable(query) {
    return request({
      url: `/admin/generate/table`,
      method: 'get',
      params: query
    });
  },

  // 获取业务表详情
  getTable(tableId) {
    return request({
      url: `/admin/generate/table/${tableId}`,
      method: 'get'
    });
  },

  // 新增业务表
  addTable(data) {
    return request({
      url: `/admin/generate/table`,
      method: 'post',
      data: data
    });
  },

  // 更新业务表
  updateTable(data) {
    return request({
      url: `/admin/generate/table`,
      method: 'put',
      data: data
    });
  },

  // 删除业务表配置
  deleteTable(tableIds) {
    return request({
      url: `/admin/generate/table`,
      method: 'delete',
      data: tableIds
    });
  },

  // 代码生成预览
  preview(tableId) {
    return request({
      url: `/admin/generate/table/preview/${tableId}`,
      method: 'get'
    });
  },

  // 代码生成预览
  sync(tableId) {
    return request({
      url: `/admin/generate/table/sync/${tableId}`,
      method: 'get'
    });
  },

  // 获取父权限菜单列表(目录、菜单)
  getPermissions() {
    return request({
      url: `/admin/generate/table/permission`,
      method: 'get'
    });
  },

  // 获取系统角色权限标识列表
  getRoles() {
    return request({
      url: `/admin/generate/table/role`,
      method: 'get'
    });
  },

  // 输入框实时检索业务表名称
  fetchTable(search) {
    return request({
      url: `/admin/generate/table/fetch`,
      method: 'get',
      params: {search: search}
    })
  },

  // 输入框实时检索业务表名称
  getTableDDL(tableId) {
    return request({
      url: `/admin/generate/table/ddl`,
      method: 'get',
      params: {tableId: tableId}
    })
  },

  // 更新业务表字段
  updateTableColumn(data) {
    return request({
      url: `/admin/generate/tableColumn`,
      method: 'put',
      data: data
    });
  },

  // 获取业务表字段列表
  getTableColumnList(tableId) {
    return request({
      url: `/admin/generate/tableColumn/${tableId}`,
      method: 'get'
    });
  },

  // 数据库设计文档预览/下载
  downloadScrew(type, tableId) {
    return request({
      url: `/admin/generate/table/screw/download`,
      method: 'post',
      params: {type: type, tableId: tableId},
      responseType: 'blob'
    });
  },

}
