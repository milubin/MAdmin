import request from '@/utils/request'

export default {

  // 分页获取开放API应用列表
  getPageApps(query) {
    return request({
      url: `/admin/app/page`,
      method: 'get',
      params: query
    })
  },

  // 获取开放API应用详情
  getApp(id) {
    return request({
      url: `/admin/app/${id}`,
      method: 'get'
    })
  },

  // 新增开放API应用
  addApp(data) {
    return request({
      url: `/admin/app`,
      method: 'post',
      data: data
    })
  },

  // 更新开放API应用
  updateApp(data) {
    return request({
      url: `/admin/app`,
      method: 'put',
      data: data
    })
  },

  // 删除开放API应用
  deleteApp(appIds) {
    return request({
      url: `/admin/app`,
      method: 'delete',
      data: appIds
    })
  },

  // 更新API应用审批状态
  updateAppStatus(data) {
    return request({
      url: `/admin/app/approval`,
      method: 'put',
      data: data
    })
  },

  // 输入框实时检索应用名称
  fetch(search) {
    return request({
      url: `/admin/app/fetch`,
      method: 'get',
      params: {search: search}
    })
  },

  // 获取开放API接口分类列表
  listApiCategory(appId) {
    return request({
      url: `/admin/app/category/${appId}`,
      method: 'get'
    })
  },

  // 获取应用AppSecret明文信息
  getAppSecret(appId) {
    return request({
      url: `/admin/app/secret/${appId}`,
      method: 'get'
    })
  },

  // API应用密钥appKey, appSecret一键发送至邮箱
  sendSecret(data) {
    return request({
      url: `/admin/app/secret/send/email`,
      method: 'post',
      data: data
    })
  },

}
