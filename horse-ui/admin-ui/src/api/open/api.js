import request from '@/utils/request'

export default {

  // 分页获取开放API接口列表
  getPageApis(query) {
    return request({
      url: `/admin/api/page`,
      method: 'get',
      params: query
    })
  },

  // 获取开放API接口详情
  getApi(id) {
    return request({
      url: `/admin/api/${id}`,
      method: 'get'
    })
  },

  // 新增开放API接口
  addApi(data) {
    return request({
      url: `/admin/api`,
      method: 'post',
      data: data
    })
  },

  // 更新开放API接口
  updateApi(data) {
    return request({
      url: `/admin/api`,
      method: 'put',
      data: data
    })
  },

  // 删除开放API接口
  deleteApi(apiIds) {
    return request({
      url: `/admin/api`,
      method: 'delete',
      data: apiIds
    })
  },

  // 更新API接口状态
  updateApiStatus(data) {
    return request({
      url: `/admin/api/status`,
      method: 'put',
      data: data
    })
  },

  // 输入框实时检索应用名称
  fetch(search) {
    return request({
      url: `/admin/api/fetch`,
      method: 'get',
      params: {search: search}
    })
  },

  // 分页获取开放API请求日志列表
  getPageApiLogs(query) {
    return request({
      url: `/admin/api/log/page`,
      method: 'get',
      params: query
    })
  },

  // 获取开放API请求日志详情
  getApiLog(id) {
    return request({
      url: `/admin/api/log/${id}`,
      method: 'get'
    })
  },

  // 删除开放API请求日志
  deleteApiLog(apiLogIds) {
    return request({
      url: `/admin/api/log`,
      method: 'delete',
      data: apiLogIds
    })
  },

  // 请求日志链路ID实时检索
  logFetch(search) {
    return request({
      url: `/admin/api/log/fetch`,
      method: 'get',
      params: {search: search}
    })
  },

}
