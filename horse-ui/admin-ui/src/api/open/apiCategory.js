import request from '@/utils/request'

export default {

  // 分页获取开放API接口类型列表
  getPageApiCategory(query) {
    return request({
      url: `/admin/api/category/page`,
      method: 'get',
      params: query
    })
  },

  // 获取开放API类型详情
  getApiCategory(id) {
    return request({
      url: `/admin/api/category/${id}`,
      method: 'get'
    })
  },

  // 获取开放API接口类型列表
  getApiCategoryList() {
    return request({
      url: `/admin/api/category/list`,
      method: 'get'
    })
  },

  // 新增开放API类型
  addApiCategory(data) {
    return request({
      url: `/admin/api/category`,
      method: 'post',
      data: data
    })
  },

  // 更新开放API类型
  updateApiCategory(data) {
    return request({
      url: `/admin/api/category`,
      method: 'put',
      data: data
    })
  },

  // 删除开放API接口
  deleteApiCategory(apiCategoryIds) {
    return request({
      url: `/admin/api/category`,
      method: 'delete',
      data: apiCategoryIds
    })
  },

  // 输入框实时检索应用名称
  fetch(search) {
    return request({
      url: `/admin/api/category/fetch`,
      method: 'get',
      params: {search: search}
    })
  },

  // 获取开放API接口列表
  listApi(apiCategoryId) {
    return request({
      url: `/admin/api/category/api/${apiCategoryId}`,
      method: 'get'
    })
  },

}
