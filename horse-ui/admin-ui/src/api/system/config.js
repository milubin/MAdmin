import request from '@/utils/request'

export default {

  // 分页获取配置列表
  getPageConfigs(query) {
    return request({
      url: `/admin/configs`,
      method: 'get',
      params: query
    })
  },
  // 获取配置详情
  getConfig(group, key) {
    return request({
      url: `/admin/configs/${group}/${key}`,
      method: 'get'
    })
  },
  // 新增配置
  addConfig(data) {
    return request({
      url: `/admin/configs`,
      method: 'post',
      data: data
    })
  },
  // 更新配置
  updateConfig(data) {
    return request({
      url: `/admin/configs`,
      method: 'put',
      data: data
    })
  },
  // 删除配置信息
  deleteConfigs(data) {
    return request({
      url: `/admin/configs`,
      method: 'delete',
      data: data
    })
  },
  // 导出配置信息
  export(query) {
    return request({
      url: `/admin/configs/export`,
      method: 'post',
      query: query
    })
  },
  // 刷新缓存配置
  refresh() {
    return request({
      url: `/admin/configs/refresh`,
      method: 'put'
    })
  },
  // 参数名称实时检索
  fetch(search) {
    return request({
      url: `/admin/configs/fetch`,
      method: 'get',
      params: {search: search}
    })
  }

}

