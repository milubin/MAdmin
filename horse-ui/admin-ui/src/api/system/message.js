import request from '@/utils/request'

export default {

  // 分页获取消息记录列表
  getPageMessages(query) {
    return request({
      url: `/admin/message/page`,
      method: 'get',
      params: query
    })
  },

  // 获取消息记录详情
  getMessage(id) {
    return request({
      url: `/admin/message/${id}`,
      method: 'get'
    })
  },

  // 删除消息日志
  deleteMessage(messageIds) {
    return request({
      url: `/admin/message`,
      method: 'delete',
      data: messageIds

    })
  },

  // 删除消息日志
  statisticsMessage() {
    return request({
      url: `/admin/message/statistics`,
      method: 'get',
    })
  },

  // 获取消息渠道列表
  getMessageChannels() {
    return request({
      url: `/admin/message/channel`,
      method: 'get',
    })
  },

  // 保存消息渠道
  addMessageChannel(data) {
    return request({
      url: `/admin/message/channel`,
      method: 'post',
      data: data
    })
  }


}
