import request from '@/utils/request'

export default {

  // 分页通知公告信息列表
  getPageNotices(query) {
    return request({
      url: `/admin/notice/page`,
      method: 'get',
      params: query
    });
  },

  // 获取通知公告信息详情
  getNotice(id) {
    return request({
      url: `/admin/notice/${id}`,
      method: 'get'
    });
  },

  // 新增通知公告
  addNotice(data) {
    return request({
      url: `/admin/notice`,
      method: 'post',
      data: data
    });
  },

  // 更新通知公告
  updateNotice(data) {
    return request({
      url: `/admin/notice`,
      method: 'put',
      data: data
    });
  },

  // 发布通知公告
  publishNotice(noticeId) {
    return request({
      url: `/admin/notice/publish`,
      method: 'put',
      params: {id: noticeId}
    });
  },

  // 撤销通知公告
  revokeNotice(noticeId) {
    return request({
      url: `/admin/notice/revoke`,
      method: 'put',
      params: {id: noticeId}
    });
  },

  // 删除通知公告
  deleteNotice(noticeIds) {
    return request({
      url: `/admin/notice`,
      method: 'delete',
      data: noticeIds
    });
  },

  // 分页获取通知公告记录列表
  getPageNoticeRecords(query) {
    return request({
      url: `/admin/notice/record/page`,
      method: 'get',
      params: query
    });
  },

  // 撤销通知公告发布记录信息(细粒度)
  revokeNoticeRecord(data) {
    return request({
      url: `/admin/notice/record/revoke`,
      method: 'put',
      data: data
    });
  },

  // 获取用户通知公告记录列表(消息中心)
  getNoticeRecords() {
    return request({
      url: `/admin/notice/record/list`,
      method: 'get'
    });
  },
  // 获取通知公告记录详情(消息中心)
  getNoticeRecord(id) {
    return request({
      url: `/admin/notice/record/${id}`,
      method: 'get'
    });
  },
  // 读取通知公告记录
  readNoticeRecord(noticeRecordId, type) {
    return request({
      url: `/admin/notice/record/read`,
      method: 'put',
      params: {
        noticeRecordId: noticeRecordId,
        type: type
      }
    });
  },
  // 删除通知公告记录
  deleteNoticeRecord(noticeRecordId, type) {
    return request({
      url: `/admin/notice/record/delete`,
      method: 'delete',
      params: {
        noticeRecordId: noticeRecordId,
        type: type
      }
    });
  },

}
