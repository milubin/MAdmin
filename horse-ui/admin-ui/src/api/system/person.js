import request from '@/utils/request'

export default {

  // 获取个人信息
  getPerson() {
    return request({
      url: `/admin/person`,
      method: "get"
    })
  },

  // 更新个人信息
  updatePerson(data) {
    return request({
      url: `/admin/person`,
      method: "put",
      data: data
    })
  },

  // 修改个人密码
  updatePassword(data) {
    return request({
      url: `/admin/person/password`,
      method: "put",
      data: data
    })
  },

  // 修改个人密码
  updateAvatar(data) {
    return request({
      url: `/admin/person/avatar`,
      method: "post",
      data: data
    })
  },

  // 用户实名认证
  authentication(data) {
    return request({
      url: `/admin/person/authentication`,
      method: "post",
      data: data
    })
  },

  // 校验用户是否需要强制更新密码
  verifyForceUpdatePassword() {
    return request({
      url: `/admin/person/force`,
      method: "get"
    })
  },

  // 强制更新用户密码
  forceUpdatePassword(data) {
    return request({
      url: `/admin/person/force`,
      method: "put",
      data: data
    })
  },

  // 获取个人快捷菜单
  getShortcutMenus() {
    return request({
      url: `/admin/person/shortcut`,
      method: "get"
    });
  },

  // 获取个人快捷菜单详情
  getShortcutMenu(id) {
    return request({
      url: `/admin/person/shortcut/${id}`,
      method: "get"
    });
  },

  // 新增个人快捷菜单
  addShortcutMenu(data) {
    return request({
      url: `/admin/person/shortcut`,
      method: "post",
      data: data
    });
  },

  // 更新个人快捷菜单
  updateShortcutMenu(data) {
    return request({
      url: `/admin/person/shortcut`,
      method: "put",
      data: data
    });
  },

  // 删除个人快捷菜单
  deleteShortcutMenu(data) {
    return request({
      url: `/admin/person/shortcut`,
      method: "delete",
      data: data
    });
  },

}
