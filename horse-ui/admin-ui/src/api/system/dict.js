/*
 * @Author: Mr.Horse
 * @Date: 2022-05-14 20:58:53
 * @LastEditTime: 2022-07-10 21:45:16
 * @Description: 字典管理API
 */
import request from '@/utils/request'

export default {

  // 分页获取字典类型列表
  getPageDictTypes(query) {
    return request({
      url: `/admin/dict/type`,
      method: "get",
      params: query
    })
  },
  // 获取字典类型列表
  getDictTypeList() {
    return request({
      url: `/admin/dict/type/list`,
      method: "get"
    })
  },
  // 获取字典类型(根据type唯一性字段)
  getDictType(type) {
    return request({
      url: `/admin/dict/type/${type}`,
      method: "get"
    })
  },
  // 新增字典类型
  addDictType(data) {
    return request({
      url: `/admin/dict/type`,
      method: "post",
      data: data
    })
  },
  // 更新字典类型
  updateDictType(data) {
    return request({
      url: `/admin/dict/type`,
      method: "put",
      data: data
    })
  },
  // 删除字典类型
  deleteDictTypes(dictTypeIds) {
    return request({
      url: `/admin/dict/type`,
      method: "delete",
      data: dictTypeIds
    })
  },
  // 导出字典类型(包括对应的字典数据)
  export(query) {
    return request({
      url: `/admin/dict/type/export`,
      method: "post",
      params: query
    })
  },
  // 切换字典类型状态
  changeDictTypeStatus(data) {
    return request({
      url: `/admin/dict/type/status`,
      method: "put",
      data: data
    })
  },

  // 根据字典类型获取字典数据列表
  getDictDatas(type) {
    return request({
      url: `/admin/dict/data/list/${type}`,
      method: 'get'
    })
  },

  // 分页获取字典数据列表
  getPageDictDatas(query) {
    return request({
      url: `/admin/dict/data`,
      method: "get",
      params: query
    })
  },
// 获取字典数据(根据type唯一性字段)
  getDictData(id, type) {
    return request({
      url: `/admin/dict/data/${id}`,
      method: "get",
      params: {type: type}
    })
  },
  // 新增字典数据
  addDictData(data) {
    return request({
      url: `/admin/dict/data`,
      method: "post",
      data: data
    })
  },
  // 更新字典数据
  updateDictData(data) {
    return request({
      url: `/admin/dict/data`,
      method: "put",
      data: data
    })
  },
  // 删除字典数据
  deleteDictDatas(dictDataIds) {
    return request({
      url: `/admin/dict/data`,
      method: "delete",
      data: dictDataIds
    })
  },
  // 刷新字典数据缓存
  refresh() {
    return request({
      url: `/admin/dict/type/refresh`,
      method: "put",
    })
  },
  // 字典类型实时检索
  fetch(search) {
    return request({
      url: `/admin/dict/type/fetch`,
      method: 'get',
      params: {search: search}
    })
  }
}

