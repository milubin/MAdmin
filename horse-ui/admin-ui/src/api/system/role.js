/*
 * @Author: Mr.Horse
 * @Date: 2022-05-14 20:58:53
 * @LastEditTime: 2022-07-12 21:20:54
 * @Description: 角色管理API
 */
import request from '@/utils/request'

export default {
  // 分页获取角色列表
  getPageRoles(query) {
    return request({
      url: `/admin/roles/page`,
      method: 'get',
      params: query
    })
  },
  // 新增角色
  addRole(data) {
    return request({
      url: `/admin/roles`,
      method: 'post',
      data: data
    })
  },
  // 更新角色
  updateRole(data) {
    return request({
      url: `/admin/roles`,
      method: 'put',
      data: data
    })
  },
  // 获取角色
  getRole(id) {
    return request({
      url: `/admin/roles/${id}`,
      method: 'get'
    });
  },
  // 获取角色信息列表
  getRoles() {
    return request({
      url: `/admin/roles/list`,
      method: 'get'
    })
  },
  // 删除角色
  deleteRoles(roleIds) {
    return request({
      url: `/admin/roles`,
      method: 'delete',
      data: roleIds
    })
  },
  // 分配用户
  deployUsers(data) {
    return request({
      url: `/admin/roles/deploy/user`,
      method: 'post',
      data: data
    })
  },
  // 分配权限
  deployPermissions(data) {
    return request({
      url: `/admin/roles/deploy/permission`,
      method: 'post',
      data: data
    })
  },
  // 取消用户的角色授权
  cancelRoleUserAuth(data) {
    return request({
      url: `/admin/roles/cancel`,
      method: 'put',
      data: data
    })
  },
  // 切换角色状态
  changeStatus(data) {
    return request({
      url: `/admin/roles/status`,
      method: 'put',
      data: data
    })
  },
  // 角色名称实时检索
  fetch(search) {
    return request({
      url: `/admin/roles/fetch`,
      method: 'get',
      params: {search: search}
    })
  }

}

