import request from '@/utils/request'

export default {

  // 获取行政区划信息列表
  getTreeDivisions(query) {
    return request({
      url: `/admin/division`,
      method: "get",
      params: query
    })
  },
  // 获取行政区划信息
  getDivision(id) {
    return request({
      url: `/admin/division/${id}`,
      method: "get"
    })
  },
  // 获取行政区划信息
  addDivision(data) {
    return request({
      url: `/admin/division`,
      method: "post",
      data: data
    })
  },
  // 更新行政区划
  updateDivision(data) {
    return request({
      url: `/admin/division`,
      method: "put",
      data: data
    })
  },
  // 删除行政区划
  deleteDivision(id) {
    return request({
      url: `/admin/division/${id}`,
      method: "delete"
    })
  },
  // 区划名称实时检索
  fetch(search) {
    return request({
      url: `/admin/division/fetch`,
      method: "get",
      params: {search: search}
    })
  },

};
