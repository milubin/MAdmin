import request from '@/utils/request'

export default {

  // 分页获取系统日志列表
  getPageLogs(query) {
    return request({
      url: `/admin/logs`,
      method: "get",
      params: query
    })
  },
  // 获取系统日志详情
  getLog(id) {
    return request({
      url: `/admin/logs/${id}`,
      method: "get"
    })
  },
  // 删除系统日志
  deleteLogs(logIds) {
    return request({
      url: `/admin/logs`,
      method: "delete",
      data: logIds
    })
  },
  // 查询系统实时日志
  searchLog(data) {
    return request({
      url: `/admin/logs/search`,
      method: "post",
      data: data
    })
  },
  // 获取系统日志统计数据
  statisticsLog() {
    return request({
      url: `/admin/logs/statistics`,
      method: "get"
    })
  },
  // 操作日志链路ID实时检索
  logFetch(search) {
    return request({
      url: `/admin/logs/fetch`,
      method: 'get',
      params: {search: search}
    })
  },
  // 分页获取登录日志列表
  getPageLoginLogs(query) {
    return request({
      url: `/admin/logs/login/page`,
      method: "get",
      params: query
    })
  },
  // 删除登录日志
  deleteLoginLogs(loginLogIds) {
    return request({
      url: `/admin/logs/login`,
      method: "delete",
      data: loginLogIds
    })
  },
  // 获取系统日志详情
  getLoginLog(id) {
    return request({
      url: `/admin/logs/login/${id}`,
      method: "get"
    })
  },
  // 获取系统日志统计数据
  statisticsLoginLog() {
    return request({
      url: `/admin/logs/login/statistics`,
      method: "get"
    })
  },
  // 登录日志链路ID实时检索
  loginFetch(search) {
    return request({
      url: `/admin/logs/login/fetch`,
      method: 'get',
      params: {search: search}
    })
  }

}
