import request from '@/utils/request'

export default {

  // 获取权限菜单列表
  getPermissionMenus(query) {
    return request({
      url: `/admin/permissions`,
      method: 'get',
      params: query
    })
  },
  // 获取权限菜单树
  getPermissionTreeMenus(query) {
    return request({
      url: `/admin/permissions/tree`,
      method: 'get',
      params: query
    })
  },
  // 获取权限菜单详情
  getPermission(id) {
    return request({
      url: `/admin/permissions/${id}`,
      method: 'get'
    })
  },
  // 新增权限菜单
  addPermission(data) {
    return request({
      url: `/admin/permissions`,
      method: 'post',
      data: data
    })
  },
  // 更新权限菜单
  updatePermission(data) {
    return request({
      url: `/admin/permissions`,
      method: 'put',
      data: data
    })
  },
  // 删除权限菜单
  deletePermission(id) {
    return request({
      url: `/admin/permissions/${id}`,
      method: 'delete'
    })
  },
  // 菜单名称实时检索
  fetch(search) {
    return request({
      url: `/admin/permissions/fetch`,
      method: 'get',
      params: {search: search}
    })
  }

}
