/*
 * @Author: Mr.Horse
 * @Date: 2022-05-14 20:58:53
 * @LastEditTime: 2022-07-12 21:19:21
 * @Description: 用户管理API
 */
import request from '@/utils/request'

export default {
  // 分页获取用户列表
  getUsers(query) {
    return request({
      url: `/admin/users/page`,
      method: 'get',
      params: query
    })
  },
  // 获取用户信息详情
  getUser(userId) {
    return request({
      url: `/admin/users/${userId}`,
      method: 'get'
    })
  },
  // 新增用户
  addUser(data) {
    return request({
      url: `/admin/users`,
      method: 'post',
      data: data
    })
  },
  // 修改用户
  updateUser(data) {
    return request({
      url: `/admin/users`,
      method: 'put',
      data: data
    })
  },
  // 删除用户
  deleteUsers(userIds) {
    return request({
      url: `/admin/users`,
      method: 'delete',
      data: userIds
    })
  },
  // 切换状态
  changeStatus(data) {
    return request({
      url: `/admin/users/status`,
      method: 'put',
      data: data
    })
  },
  // 用户密码重置
  resetPassword(data) {
    return request({
      url: `/admin/users/reset`,
      method: 'put',
      data: data
    })
  },
  // 根据角色分页获取用户列表
  getUserList(query) {
    return request({
      url: `/admin/users/list`,
      method: 'get',
      params: query
    })
  },
  // 解除账号锁定
  unlockUser(data) {
    return request({
      url: `/admin/users/unlock`,
      method: 'put',
      data: data
    })
  },
  // 用户名实时检索
  fetch(query) {
    return request({
      url: `/admin/users/fetch`,
      method: 'get',
      params: query
    });
  },

}

