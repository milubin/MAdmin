import request from '@/utils/request'

export default {

  // 分页获取Quartz任务调度列表
  getPageQuartzs(query) {
    return request({
      url: `/admin/schedule/quartz/page`,
      method: 'get',
      params: query
    })
  },
  // 创建Quartz任务调度
  addQuartzJob(data) {
    return request({
      url: `/admin/schedule/quartz/add`,
      method: 'post',
      data: data
    })
  },
  // 暂停Quartz任务调度
  pauseQuartzJob(id) {
    return request({
      url: `/admin/schedule/quartz/pause`,
      method: 'put',
      params: {id}
    })
  },
  // 重启Quartz任务调度
  rebootQuartzJob(id) {
    return request({
      url: `/admin/schedule/quartz/reboot`,
      method: 'put',
      params: {id}
    })
  },
  // 更新Quartz任务调度
  updateQuartzJob(data) {
    return request({
      url: `/admin/schedule/quartz/update`,
      method: 'put',
      data: data
    })
  },
  // 删除Quartz任务调度
  deleteQuartzJob(jobIds) {
    return request({
      url: `/admin/schedule/quartz/delete`,
      method: 'delete',
      data: jobIds
    })
  },
  // 获取Quartz任务调度信息
  getQuartzJob(id) {
    return request({
      url: `/admin/schedule/quartz/${id}`,
      method: 'get'
    })
  },
  // Quartz任务立刻运行一次
  runOnceQuartzJob(id) {
    return request({
      url: `/admin/schedule/quartz/once`,
      method: 'get',
      params: {id}
    })
  },
  // 分页获取Quartz任务调度日志列表
  getPageQuartzLog(query) {
    return request({
      url: `/admin/schedule/quartz/log`,
      method: 'get',
      params: query
    })
  },
  // 清理Quartz任务调度日志信息
  clearQuartzLogs(quartzLogIds) {
    return request({
      url: `/admin/schedule/quartz/log`,
      method: 'delete',
      data: quartzLogIds
    })
  },
  // 获取Quartz调度任务日志统计数据
  statisticsQuartzLog() {
    return request({
      url: `/admin/schedule/quartz/statistics`,
      method: 'get'
    })
  },
  // 任务名称实时检索
  quartzFetch(search) {
    return request({
      url: `/admin/schedule/quartz/fetch`,
      method: 'get',
      params: {search: search}
    })
  },
  // ======================================Spring调度任务相关===========================================

  // 分页获取Task任务调度列表
  getPageTasks(query) {
    return request({
      url: `/admin/schedule/task/page`,
      method: 'get',
      params: query
    })
  },
  // 获取Spring调度任务
  getTask(id) {
    return request({
      url: `/admin/schedule/task/${id}`,
      method: 'get'
    })
  },
  // 新增Spring调度任务
  addTask(data) {
    return request({
      url: `/admin/schedule/task/add`,
      method: 'post',
      data: data
    })
  },
  // 更新Spring调度任务
  updateTask(data) {
    return request({
      url: `/admin/schedule/task/update`,
      method: 'put',
      data: data
    })
  },
  // 切换Spring调度任务状态
  switchTaskStatus(data) {
    return request({
      url: `/admin/schedule/task/switch`,
      method: 'put',
      data: data
    })
  },
  // 删除Spring调度任务
  deleteTask(taskIds) {
    return request({
      url: `/admin/schedule/task/delete`,
      method: 'delete',
      data: taskIds
    })
  },
  // 分页获取Spring任务调度日志列表
  getPageTaskLog(query) {
    return request({
      url: `/admin/schedule/task/log`,
      method: 'get',
      params: query
    })
  },
  // 清理Spring任务调度日志信息
  clearTaskLogs(taskLogIds) {
    return request({
      url: `/admin/schedule/task/log`,
      method: 'delete',
      data: taskLogIds
    })
  },
  // 获取Spring调度任务日志统计数据
  statisticsTaskLog() {
    return request({
      url: `/admin/schedule/task/statistics`,
      method: 'get'
    })
  },
  // 任务名称实时检索
  taskFetch(search) {
    return request({
      url: `/admin/schedule/task/fetch`,
      method: 'get',
      params: {search: search}
    })
  }

}
