import request from '@/utils/request'

export default {

  // 分页获取文件列表
  getPageOss(query) {
    return request({
      url: `/admin/oss`,
      method: "get",
      params: query
    })
  },
  // 文件下载
  download(id) {
    return request({
      url: `/admin/oss/download`,
      method: 'post',
      params: {id: id}
    })
  },
  // 文件删除
  deleteOss(ossIds) {
    return request({
      url: `/admin/oss`,
      method: 'delete',
      data: ossIds
    })
  },
  // 文件预览
  preview(id) {
    return request({
      url: `/admin/oss/preview`,
      method: 'get',
      params: {id: id}
    })
  },
  // 文件删除
  updateOss(data) {
    return request({
      url: `/admin/oss`,
      method: 'put',
      data: data
    })
  },
  // 文件名称实时检索
  fetch(search) {
    return request({
      url: `/admin/oss/fetch`,
      method: 'get',
      params: {search: search}
    })
  },

  // 分页获取OSS对象存储配置列表
  getPageOssConfig(query) {
    return request({
      url: `/admin/oss/config`,
      method: 'get',
      params: query
    });
  },

  // 获取OSS对象存储配置详情
  getOssConfig(id) {
    return request({
      url: `/admin/oss/config/${id}`,
      method: 'get'
    });
  },

  // 新增OSS对象存储配置
  addOssConfig(data) {
    return request({
      url: `/admin/oss/config`,
      method: 'post',
      data: data
    });
  },

  // 更新OSS对象存储配置
  updateOssConfig(data) {
    return request({
      url: `/admin/oss/config`,
      method: 'put',
      data: data
    });
  },

  // 删除OSS对象存储配置
  deleteOssConfig(ossConfigIds) {
    return request({
      url: `/admin/oss/config`,
      method: 'delete',
      data: ossConfigIds
    });
  },

  // 实时检索
  fetchConfig(search) {
    return request({
      url: `/admin/oss/config/fetch`,
      method: 'get',
      params: {search: search}
    });
  }

}
