import axios from 'axios'
import {Loading, Message} from 'element-ui'
import {saveAs} from 'file-saver'
import {getHeaders} from "@/utils/auth";
import {blobValidate} from "@/utils/ruoyi";

const baseURL = process.env.VUE_APP_BASE_API;
let downloadLoadingInstance;

export default {

  // OSS文件下载
  oss(id, filename) {
    downloadLoadingInstance = Loading.service({
      text: "正在玩命下载中...",
      spinner: "el-icon-loading",
      background: "rgba(0, 0, 0, 0.4)",
    });
    axios({
      method: 'post',
      url: baseURL + '/admin/oss/download/?id=' + id,
      responseType: 'blob',
      headers: getHeaders()
    }).then(async (res) => {
      const isBlob = await blobValidate(res.data);
      if (isBlob) {
        const blob = new Blob([res.data], {type: 'application/octet-stream'});
        this.saveAs(blob, decodeURIComponent(filename));
      } else {
        this.printErrMsg(res.data);
      }
      downloadLoadingInstance.close();
    }).catch((r) => {
      console.error(r);
      Message.error('下载文件出现错误，请联系管理员！');
      downloadLoadingInstance.close();
    })
  },

  // 文件下载
  file(url, data, filename) {
    downloadLoadingInstance = Loading.service({
      text: "正在玩命下载中...",
      spinner: "el-icon-loading",
      background: "rgba(0, 0, 0, 0.4)",
    });
    axios({
      method: 'post',
      url: baseURL + url,
      data: data,
      responseType: 'blob',
      headers: getHeaders()
    }).then(async (res) => {
      const isLogin = await blobValidate(res.data);
      if (isLogin) {
        const blob = new Blob([res.data], {type: 'application/octet-stream'});
        this.saveAs(blob, filename)
      } else {
        this.printErrMsg(res.data);
      }
      downloadLoadingInstance.close();
    }).catch((r) => {
      console.error(r);
      Message.error('下载文件出现错误，请联系管理员！');
      downloadLoadingInstance.close();
    })
  },

  zip(url, data, name) {
    downloadLoadingInstance = Loading.service({
      text: "正在玩命下载中...",
      spinner: "el-icon-loading",
      background: "rgba(0, 0, 0, 0.4)",
    });
    axios({
      method: 'post',
      url: baseURL + url,
      data: data,
      responseType: 'blob',
      headers: getHeaders()
    }).then(async (res) => {
      const isLogin = await blobValidate(res.data);
      if (isLogin) {
        const blob = new Blob([res.data], {type: 'application/zip'})
        this.saveAs(blob, name)
      } else {
        this.printErrMsg(res.data);
      }
      downloadLoadingInstance.close();
    }).catch((r) => {
      console.error(r);
      Message.error('下载文件出现错误，请联系管理员！');
      downloadLoadingInstance.close();
    })
  },

  saveAs(text, name, opts) {
    saveAs(text, name, opts);
  },

  async printErrMsg(data) {
    const resText = await data.text();
    Message.error(JSON.parse(resText).message);
  }

}

