/*
 * @Author: Mr.Horse
 * @Date: 2022-07-10 16:14:55
 * @LastEditTime: 2022-07-10 21:32:12
 * @Description: 
 */
import Vue from 'vue'
import DataDict from '@/utils/dict'
import dictApi from '@/api/system/dict'

function install() {
  Vue.use(DataDict, {
    metas: {
      '*': {
        labelField: 'label',
        valueField: 'value',
        request(dictMeta) {
          return dictApi.getDictDatas(dictMeta.type).then(res => res.data)
        },
      },
    },
  })
}

export default {
  install,
}
