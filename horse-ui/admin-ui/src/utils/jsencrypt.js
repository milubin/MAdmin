/*
 * @Author: Mr.Horse
 * @Date: 2022-06-19 13:03:45
 * @LastEditTime: 2022-06-19 15:53:05
 * @Description:
 */
import JSEncrypt from 'jsencrypt/bin/jsencrypt.min'
import Encrypt from 'encryptlong'
import CryptoJS from 'crypto-js'
import smCrypto from 'sm-crypto'

// RSA密钥
const RSAPublicKey = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCupsUbv9Qwyqf0z2qDIvZW2zMXCT4M9NN8dq8u7fT/G2Xr4Ge4sQ9xKSltsSDk8Yt69h4NtrwV/jPNZ1wzOJceXME8uClsgyHs2H9wxjwy9wktQ4pzU2VAw63Nm9mgqezeVc+WkS30Bj/8W2PYKob+YssgzYCD2y3XYCL5L3C4+wIDAQAB'
const RSAPrivateKey = 'MIICeQIBADANBgkqhkiG9w0BAQEFAASCAmMwggJfAgEAAoGBAK6mxRu/1DDKp/TPaoMi9lbbMxcJPgz003x2ry7t9P8bZevgZ7ixD3EpKW2xIOTxi3r2Hg22vBX+M81nXDM4lx5cwTy4KWyDIezYf3DGPDL3CS1DinNTZUDDrc2b2aCp7N5Vz5aRLfQGP/xbY9gqhv5iyyDNgIPbLddgIvkvcLj7AgMBAAECgYEAgdv8aSKcCl9UvMFQaAsh8PjxJuq3WXjcmwnAPfuuk9RyVw028eutGK3LYQU3+SLkW9wOApJhjVTCfbKhqnAkY7MHn8vBnnANAKzp34pTvm79jg8NglFVeBD7JHmVQMGJ+F0GNzRBdTxxl1NdHHBw8IOCkmH1UnvVik3BCU51AckCQQDgIeoJBzyLyjyc35rGIKK5G2/d2mla3GKF1wtFV7yDLPry9h/iwBpf0qOJ/3LwBmd/GY1GAjzW40hNozTrJ56tAkEAx3vSCIaTiyoa1pne633Dk2CtN+Zn67pFNsQ7lKcrKMzFOxZsrm+mSr5AtFPHjQycxWKwirRpsdDSsGiKWVxzRwJBAJox2s+xQc8uJ/sIvnM1+pz8qj5nx3lyCvnnEuL21EMui8rg1tjprntj74y9yDYnQcaoLpAOdAqmPJTlBkANdtUCQQCUHOdx1Rovte9/b3gIubRA9QMoAjce24sQgtsKIKT3AjNfD+NaESQTUveP7LS5kafFFuWl1xazJC2LNdOjTfV5AkEAtHGvV/YEj17ObE6yUqckVjGEmtV+26539EeT4Bggd4sNV0uwaOteH/DelAMyzj0u06dgI+YEDDC4XfQxUX68Cw==';

// AES密钥
const AESKey = 'JYOlntQc+zCFnz86DzFM7XadjBTqDt9q1pOVG9+mve0=';

// SM2密钥
const SM2PublicKey = '046b3101b320a8b8665fdc55fcf90800e8bf27a16756cb52c0ecfd54193f32380020ca52a5525c15e018730c304d73534742a1a587a423da2232ea8ea7f03ae748';
const SM2PrivateKey = 'a0425814ee6be32725a36b4ffe35c7ddf5d6e4b7fd97714f3ccdb40ed62959b6';

// SM4密钥
const SM4Key = '32653676716c356a356438366b303765';

// SM加密模式
const cipherMode = 1 // 1 - C1C3C2，0 - C1C2C3，默认为1


/**
 * RSA公钥加密
 * @param txt
 * @returns {*}
 */
export function encrypt(txt) {
  const encryptor = new JSEncrypt()
  encryptor.setPublicKey(RSAPublicKey)
  return encryptor.encrypt(txt)
}

/**
 * RSA私钥解密
 * @param txt
 * @returns {*}
 */
export function decrypt(txt) {
  const encryptor = new JSEncrypt()
  encryptor.setPrivateKey(RSAPrivateKey)
  return encryptor.decrypt(txt)
}

/**
 * RSA公钥加密(大文件)
 * @param data
 * @returns {*}
 */
export function encryptLong(data) {
  const PUBLIC_KEY = RSAPublicKey
  const encryptor = new Encrypt()
  encryptor.setPublicKey(PUBLIC_KEY)
  return encryptor.encryptLong(data)
}

/**
 * RSA公钥解密(大文件)
 * @param data
 * @returns {*}
 */
export function decryptLong(data) {
  const PRIVATE_KEY = RSAPrivateKey
  const encryptor = new Encrypt()
  encryptor.setPrivateKey(PRIVATE_KEY)
  return encryptor.decryptLong(data)
}

/**
 * md5加密
 * @param txt
 * @returns {*}
 */
export function md5(txt) {
  return CryptoJS.MD5(txt).toString();
}

/**
 * AES加密
 *
 * @returns {any}
 * @constructor
 * @param txt
 */
export function AESEncrypt(txt) {
  return CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(txt), CryptoJS.enc.Utf8.parse(AESKey), {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7
  }).toString();
}

/**
 * AES解密
 *
 * @returns {any}
 * @constructor
 * @param txt
 */
export function AESDecrypt(txt) {
  return CryptoJS.enc.Utf8.stringify(CryptoJS.AES.decrypt(txt, CryptoJS.enc.Utf8.parse(AESKey), {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7
  })).toString();
}

/**
 * SM2公钥加密
 *
 * @param plainText
 * @returns {*}
 * @constructor
 */
export function SM2Encrypt(plainText) {
  return smCrypto.sm2.doEncrypt(plainText, SM2PublicKey, cipherMode);
}

/**
 * SM2私钥解密
 *
 * @param cipherText
 * @returns {*|string|[]|string}
 * @constructor
 */
export function SM2Decrypt(cipherText) {
  return smCrypto.sm2.doDecrypt(cipherText, SM2PrivateKey, cipherMode);
}

/**
 * SM4加密
 *
 * @param plainText
 * @returns {*}
 * @constructor
 */
export function SM4Encrypt(plainText) {
  return smCrypto.sm4.encrypt(plainText, SM4Key);
}

/**
 * SM4解密
 *
 * @param cipherText
 * @returns {*}
 * @constructor
 */
export function SM4Decrypt(cipherText) {
  return smCrypto.sm4.decrypt(cipherText, SM4Key);
}

