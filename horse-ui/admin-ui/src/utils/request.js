import axios from 'axios'
import {Loading, Message, MessageBox, Notification} from 'element-ui'
import {nanoid} from 'nanoid'
import {encrypt, md5, SM2Decrypt, SM2Encrypt} from '@/utils/jsencrypt'
import store from '@/store'
import {getHeaders, getToken} from '@/utils/auth'
import {blobValidate, tansParams} from "@/utils/ruoyi";
import {saveAs} from 'file-saver'

let downloadLoadingInstance;
// 是否显示重新登录
export let isRelogin = {show: false};

// 是否数据传输加解密
const isEncrypt = false;

axios.defaults.headers['Content-Type'] = 'application/json;charset=utf-8';
// 对应国际化资源文件后缀
axios.defaults.headers['Content-Language'] = 'zh_CN';
// 创建axios实例
const service = axios.create({
  // axios中请求配置有baseURL选项，表示请求URL公共部分
  baseURL: process.env.VUE_APP_BASE_API,
  // 超时
  timeout: 10000
});

// request拦截器
service.interceptors.request.use(config => {
  // 设置签名参数
  let timestamp = Date.now().toString();
  let random = nanoid(10);
  config.headers['timestamp'] = timestamp;
  config.headers['random'] = encrypt(random);
  config.headers['sign'] = md5(timestamp + md5(timestamp) + random).toUpperCase();

  // 是否需要设置 token
  const isToken = (config.headers || {}).isToken === false;
  if (getToken() && !isToken) {
    config.headers['token'] = getToken() // 让每个请求携带自定义token 请根据实际情况自行修改
  }
  // get请求映射params参数
  if (config.method === 'get' && config.params) {
    let url = config.url + '?' + tansParams(config.params);
    url = url.slice(0, -1);
    config.params = {};
    config.url = url;
  }
  // 请求参数body是否需要加密
  if (isEncrypt && config.data) {
    config.data = {cipherText: SM2Encrypt(JSON.stringify(config.data))}
  }
  return config;
}, error => {
  console.error(error)
  Promise.reject(error)
})

// 响应拦截器
service.interceptors.response.use(res => {
    // 二进制数据则直接返回
    if (res.request.responseType === 'blob' || res.request.responseType === 'arraybuffer') {
      return res.data
    }
    // 响应参数解密
    if (isEncrypt) {
      res.data = JSON.parse(SM2Decrypt(res.data));
    }
    // 未设置状态码则默认成功状态
    const code = res.data.code;
    const msg = res.data.message;
    if (code === 401) {
      if (!isRelogin.show) {
        isRelogin.show = true;
        MessageBox.confirm('登录状态已过期，您可以继续留在该页面或重新登录', '系统提示', {
            confirmButtonText: '重新登录',
            cancelButtonText: '取消',
            type: 'warning'
          }
        ).then(() => {
          isRelogin.show = false;
          store.dispatch('LogOut').then(() => {
            location.href = process.env.VUE_APP_CONTEXT_PATH + "index";
          })
        }).catch(() => {
          isRelogin.show = false;
        });
      }
      return Promise.reject('无效的会话，或者会话已过期，请重新登录')
    } else if (code === 403) {
      Message({
        message: msg,
        type: 'warning'
      });
      return Promise.reject(new Error(msg));
    } else if (code === 500) {
      Message({
        message: msg,
        type: 'error'
      });
      return Promise.reject(new Error(msg));
    } else if (code !== 200) {
      Notification.error({
        title: msg
      });
      return Promise.reject('error');
    } else {
      return res.data;
    }
  },
  error => {
    console.log('err' + error);
    let {message} = error;
    if (message === "Network Error") {
      message = "后端接口连接异常";
    } else if (message.includes("timeout")) {
      message = "系统接口请求超时";
    } else if (message.includes("Request failed with status code")) {
      message = "系统接口" + message.substr(message.length - 3) + "异常";
    }
    Message({
      message: message,
      type: 'error',
      duration: 3 * 1000
    });
    return Promise.reject(error)
  });

// 通用下载方法
export function download(url, params, filename) {
  downloadLoadingInstance = Loading.service({
    text: "正在玩命下载中...",
    spinner: "el-icon-loading",
    background: "rgba(0, 0, 0, 0.5)",
  });
  let headers = getHeaders();
  headers['Content-Type'] = 'application/x-www-form-urlencoded';
  return service.post(url, params, {
    transformRequest: [(params) => {
      return tansParams(params)
    }],
    headers: headers,
    responseType: 'blob'
  }).then(async (data) => {
    const isBlob = await blobValidate(data);
    if (isBlob) {
      const blob = new Blob([data]);
      saveAs(blob, filename)
    } else {
      const resText = await data.text();
      const rspObj = JSON.parse(resText);
      Message.error(rspObj.message);
    }
    downloadLoadingInstance.close();
  }).catch((r) => {
    console.error(r);
    Message.error('下载文件出现错误，请联系管理员！');
    downloadLoadingInstance.close();
  });
}

export default service
