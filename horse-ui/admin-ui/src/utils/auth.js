import Cookies from 'js-cookie'
import {nanoid} from "nanoid";
import {encrypt, md5} from '@/utils/jsencrypt'

const TokenKey = 'token'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function getHeaders() {
  let timestamp = Date.now().toString();
  let random = nanoid(10);
  return {
    token: getToken(),
    timestamp: timestamp,
    random: encrypt(random),
    sign: md5(timestamp + md5(timestamp) + random).toUpperCase()
  };
}
