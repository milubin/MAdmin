## openApi说明文档

### 业务参数加解密
> * 加密方式: 3DES
> * 使用模式: 无向量、默认的DESede/ECB/PKCS5Padding
> * 加解密编码: UTF-8编码
> * 加密处理: 把明文加密为Base64字符串密文
> * 解密处理: 把Base64表示的字符串密文解密为明文
> * 3DES的密钥值默认为 app_secret 的值(开发方需保密存储)

### 签名处理
> * 将所有业务参数按照key-value形式组装成json对象, 转为json字符串, 进行3DES加密作为请求参数 data 的值
> * 加密后对公共请求参数 appKey、accessToken、timestamp、version、signMethod、data (除sign参数) 外再加上 app_secret 做自然排序
> * 排序后按顺序拼接参数值, 按照 signMethod 签名方法进行签名生成并转大写到的sign值
