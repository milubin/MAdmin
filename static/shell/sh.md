## Liunx系统执行在windows上编写的shell脚本时提示执行失败问题
> 问题说明: 在DOS/Windows里，文本文件的换行符为\r\n，而在Linux系统里则为\n，所以DOS/Windows里编辑过的文本文件到了Linux里，每一行都多了个^M，从而导致运行shell脚本报错

### 解决方式

#### 方式一
```shell script
# 编辑
vi xxx.sh
#esc退出执行
:set ff=unix
#再esc保存退出
:wq
```

#### 方式二
> 如果方式一不行, 接着执行方式二操作
```shell script
#替换\r字符
sed -i 's/\r$//' xxx.sh
#如果有^M字符
sed -i 's/^M//g' xxx.sh
```

