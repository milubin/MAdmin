# MAdmin系统

## 仓库概览
[![麋鹿滨/MAdmin](https://gitee.com/milubin/MAdmin/widgets/widget_card.svg?colors=ffffff,1e252b,323d47,455059,d7deea,99a0ae)](https://gitee.com/milubin/MAdmin)

## 系统简介

[![MAdmin](https://img.shields.io/badge/MAdmin-1.0.1-success.svg)](https://gitee.com/milubin/MAdmin)
[![码云Gitee](https://gitee.com/milubin/MAdmin/badge/star.svg?theme=blue)](https://gitee.com/milubin/MAdmin)
[![License](https://img.shields.io/badge/License-MIT-blue.svg)](https://gitee.com/milubin/MAdmin/blob/master/LICENSE)
<br>
[![IntelliJ IDEA工具](https://img.shields.io/badge/IntelliJ%20IDEA-2022.2-green.svg)](https://www.jetbrains.com/?from=MAdmin)
[![Spring Boot](https://img.shields.io/badge/Spring%20Boot-2.3.x-yellow.svg)]()
[![JDK-8+](https://img.shields.io/badge/JDK-8-green.svg)]()
> MAdmin系统依托于RuoYi系统进行全方位功能点扩展升级

> 本项目代码开源免费，为学习和兴趣而开源

## 功能简介

| 功能点      | 技术      | 文档地址       | 注意事项 |
|:---------|----------|----------|------|
| 当前系统     | `MAdmin`              | [MAdmin文档](https://gitee.com/milubin/MAdmin) | 对 `RuoYi` 全方位扩展升级        |
| 原系统      | `RuoYi`               | [RuoYi官网](http://ruoyi.vip/) | `RuoYi` 系统               |
| 前端开发框架   | `Vue` `ElementUI`     | [ElementUI官网](https://element.eleme.cn/#/zh-CN)       |                          |
| 后端开发框架   | `SpringBoot`          | [SpringBoot官网](https://spring.io/projects/spring-boot/#learn)   |                          |
| 容器框架     | `Undertow`            | [Undertow官网](https://undertow.io/)      | 基于 XNIO 的高性能容器           |
| 关系数据库    | `MySQL`               | [MySQL官网](https://dev.mysql.com/)      | 5.7.x版本                  |
| ORM框架 | Mybatis-Plus|[Mybatis-Plus官网](https://baomidou.com/)| 简化crud开发效率|
| 数据库框架    | `p6spy`               | [p6spy官网](https://p6spy.readthedocs.io/)     | 更强劲的 SQL 分析              |
| 序列化工具      | `Jackson`             | [Jackson官网](https://github.com/FasterXML/jackson)       | SpringBoot内阻Jackson序列化处理 |
| 分布式幂等、限流 | `Redisson`            | [Redisson文档](https://github.com/redisson/redisson/wiki/%E7%9B%AE%E5%BD%95) | 全局分布式幂等、限流 |
| 校验框架 | `Validation`| [Validation文档](https://docs.jboss.org/hibernate/stable/validator/reference/en-US/html_single/) | 优雅的接口参数校验 |       
| Excel框架  | `EasyExcel` `EasyPoi` | [EasyExcel文档](https://www.yuque.com/easyexcel/doc/easyexcel)  | 快速导出excel、csv文件 |
| 工具类框架    | `Hutool` `Lombok` `Guava` | [Hutool文档](https://www.hutool.cn/docs/)  | 高效率工具      |

## 业务功能

|  功能   | 描述                                       |
|:-----:|:-----------------------------------------|
| 登录页| 系统登录包括账号、邮箱、手机号等多种登录方式和找回密码功能|
| 用户管理  | 系统操作者的管理模块，用于完成系统用户配置                    |
| 角色管理  | 角色菜单权限分配，用于系统角色配置                        |
| 菜单管理  | 配置系统菜单，操作权限和按钮权限标识                       |
| 字典管理  | 用于配置和维护系统中较为固定的基础数据                      |
| 配置管理  | 用于配置和维护系统动态参数数据                          |
| 文件管理  | 用于维护系统文件的上传、压缩、下载、预览、删除等                    |
| 调度管理  | 集成第三方调度（Quartz）和本地Scheduler调度，在线处理系统调度任务 |
| 日志管理  | 记录系统操作日志、异常日志、登录日志等                      |
| 消息管理  | 管理第三方（钉钉、飞书、邮箱、短信、微信公众号）服务发送消息的记录和第三方服务配置管理 |
| 通知公告  | 用于配置维护系统通知公告信息的发布维护
| 消息中心  | 用于用户管理自己的通知公告消息（站内信），包括查看、删除等            |
| 区划管理  | 用于配置和维护国家行政区划信息           |
| 缓存监控  | 对系统缓存服务信息的监控，包括基础信息和命令统计等                |
| 数据监控  | 集成Druid监控系统数据库连接池状态，可分析SQL优化系统性能         |
| 系统监控  | 对当前系统部署服务器信息的监控，包括CPU、内存、磁盘等             |
| 在线监控  | 用于监控系统在线用户信息，允许强制退出操作                  |
| 开放平台  | 集成应用管理、类型管理、API管理、API请求日志等功能，用于向第三方提供安全、高效的系统API接口|     
| 代码生成  | 集成数据源管理、表配置管理、字段管理、代码预览和下载等功能，提升本脚手架的开发效率|              
| 开发文档  | 当前系统服务接口在线文档（基于Knife4j生成）|

## 系统演示

| | |
|:---:|:---:|
|![登录页](static/images/%E7%99%BB%E5%BD%95%E9%A1%B5.png)  | ![忘记密码](static/images/%E9%87%8D%E7%BD%AE%E5%AF%86%E7%A0%81.png)|
|![首页](static/images/%E9%A6%96%E9%A1%B5.png)|![开发文档](static/images/%E5%BC%80%E5%8F%91%E6%96%87%E6%A1%A3.png)|
|![用户管理](static/images/%E7%94%A8%E6%88%B7%E7%AE%A1%E7%90%86.png)   |![角色管理](static/images/%E8%A7%92%E8%89%B2%E7%AE%A1%E7%90%86.png) |
|![菜单管理](static/images/%E8%8F%9C%E5%8D%95%E7%AE%A1%E7%90%86.png)   |![字典管理](static/images/%E5%AD%97%E5%85%B8%E7%AE%A1%E7%90%86.png)   |
|![配置管理](static/images/%E9%85%8D%E7%BD%AE%E7%AE%A1%E7%90%86.png)   |![文件管理](static/images/%E6%96%87%E4%BB%B6%E7%AE%A1%E7%90%86.png)   |
|![Quartz调度](static/images/Quartz%E8%B0%83%E5%BA%A6.png)             |![Schedule调度](static/images/Schedule%E8%B0%83%E5%BA%A6.png)   |
|![操作日志](static/images/%E6%93%8D%E4%BD%9C%E6%97%A5%E5%BF%97.png)   |![登录日志](static/images/%E7%99%BB%E5%BD%95%E6%97%A5%E5%BF%97.png)  |
|![消息管理](static/images/%E6%B6%88%E6%81%AF%E7%AE%A1%E7%90%86.png)   |![通知公告](static/images/%E9%80%9A%E7%9F%A5%E5%85%AC%E5%91%8A.png)   |
|![缓存监控](static/images/%E7%BC%93%E5%AD%98%E7%9B%91%E6%8E%A7.png)   |![数据监控](static/images/%E6%95%B0%E6%8D%AE%E7%9B%91%E6%8E%A7.png)   |
|![系统监控](static/images/%E7%B3%BB%E7%BB%9F%E7%9B%91%E6%8E%A7.png)   |![在线监控](static/images/%E5%9C%A8%E7%BA%BF%E7%9B%91%E6%8E%A7.png)   |
|![区划管理](static/images/%E5%8C%BA%E5%88%92%E7%AE%A1%E7%90%86.png)   |![我的消息](static/images/%E6%88%91%E7%9A%84%E6%B6%88%E6%81%AF.png) |
|![应用管理](static/images/%E5%BA%94%E7%94%A8%E7%AE%A1%E7%90%86.png)|![应用类型管理](static/images/%E5%BA%94%E7%94%A8%E7%B1%BB%E5%9E%8B%E7%AE%A1%E7%90%86.png)|
|![API管理](static/images/API%E7%AE%A1%E7%90%86.png)|![API日志](static/images/API%E6%97%A5%E5%BF%97.png)|
|![数据源管理](static/images/%E4%BB%A3%E7%A0%81%E7%94%9F%E6%88%90-%E6%95%B0%E6%8D%AE%E6%BA%90%E7%AE%A1%E7%90%86.png)|![表配置管理](static/images/%E4%BB%A3%E7%A0%81%E7%94%9F%E6%88%90-%E8%A1%A8%E9%85%8D%E7%BD%AE%E7%AE%A1%E7%90%86.png)|
|![字段管理](static/images/%E4%BB%A3%E7%A0%81%E7%94%9F%E6%88%90-%E5%AD%97%E6%AE%B5%E7%AE%A1%E7%90%86.png)|![代码预览](static/images/%E4%BB%A3%E7%A0%81%E7%94%9F%E6%88%90-%E4%BB%A3%E7%A0%81%E9%A2%84%E8%A7%88.png)|
|![代码生成管理](static/images/%E4%BB%A3%E7%A0%81%E7%94%9F%E6%88%90.png)|![字段配置](static/images/%E4%BB%A3%E7%A0%81%E7%94%9F%E6%88%90-%E5%AD%97%E6%AE%B5%E9%85%8D%E7%BD%AE.png)|
|![预览DDL](static/images/%E4%BB%A3%E7%A0%81%E7%94%9F%E6%88%90-%E9%A2%84%E8%A7%88%E8%A1%A8DDL.png)||


## 联系作者



