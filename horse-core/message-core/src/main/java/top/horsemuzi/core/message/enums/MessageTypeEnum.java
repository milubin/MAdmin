package top.horsemuzi.core.message.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * 消息类型枚举处理
 *
 * @author 马滨
 * @date 2022/09/25 17:11
 **/

@Getter
@ToString
@AllArgsConstructor
public enum MessageTypeEnum {

    /**
     * 消息类型
     */
    TEXT("10", "文本", "text", "text", "text"),
    VOICE("20", "语音", null, "voice", null),
    NEWS("30", "图文", "feedCard", null, null),
    FILE("40", "文件", null, "file", null),
    MARKDOWN("50", "markdown", "markdown", "markdown", null),
    IMAGE("60", "图片", null, "image", "image"),
    LINK("70", "链接消息", "link", "link", null),
    ACTION_CARD("80", "跳转卡片消息", "actionCard", null, "interactive"),
    OA("90", "OA消息", null, "oa", null),
    RICH_TEXT("100", "富文本", null, null, "post"),
    SHARE_CHAT("110", "群名片", null, null, "share_chat");

    /**
     * 消息类型code标识
     */
    private final String code;

    /**
     * 消息类型描述
     */
    private final String description;

    /**
     * 钉钉工作消息的类型值
     */
    private final String dingBotType;

    /**
     * 钉钉机器人消息的类型值
     */
    private final String dingWorkType;

    /**
     * 飞书机器人类型值
     */
    private final String flyingBotType;


    /**
     * 通过code获取钉钉机器人的Type值
     *
     * @param code
     * @return
     */
    public static String getDingBotMessageType(String code) {
        for (MessageTypeEnum value : MessageTypeEnum.values()) {
            if (value.getCode().equals(code)) {
                return value.getDingBotType();
            }
        }
        return null;
    }

    /**
     * 通过code获取钉钉工作通知的Type值
     *
     * @param code
     * @return
     */
    public static String getDingWorkMessageType(String code) {
        for (MessageTypeEnum value : MessageTypeEnum.values()) {
            if (value.getCode().equals(code)) {
                return value.getDingWorkType();
            }
        }
        return null;
    }

    /**
     * 通过code获取飞书机器人的Type值
     *
     * @param code
     * @return java.lang.String
     * @author mabin
     * @date 2023/4/11 10:29
     **/
    public static String getFlyingBotMessageType(String code) {
        for (MessageTypeEnum value : MessageTypeEnum.values()) {
            if (value.getCode().equals(code)) {
                return value.getFlyingBotType();
            }
        }
        return null;
    }

}
