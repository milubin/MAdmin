package top.horsemuzi.core.message.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Collection;

/**
 * 邮件消息体
 *
 * @author 马滨
 * @date 2022/09/25 16:33
 **/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class EmailContentModel extends ContentModel implements Serializable {

    private static final long serialVersionUID = -5509088239754716232L;

    /**
     * 标题
     */
    private String title;

    /**
     * 收件人(英文逗号分隔)
     */
    private String tos;

    /**
     * 收件人列表(如果tos和tosColl都不为空, 优先使用tosColl数据)
     */
    private Collection<String> tosColl;

    /**
     * 抄送人(英文逗号分隔)
     */
    private String ccs;

    /**
     * 抄送人列表(如果ccs和ccsColl都不为空, 优先使用ccsColl数据)
     */
    private Collection<String> ccsColl;

    /**
     * 密送人(英文逗号分隔)
     * 密送含义: 收件人只能看到邮件内容,无法看见邮件的其他抄送人信息, 从而保证其他抄送人邮箱号的隐私安全
     */
    private String bccs;

    /**
     * 密送人列表(如果bccs和bccsColl都不为空, 优先使用bccsColl数据)
     */
    private Collection<String> bccsColl;

    /**
     * 内容(可写入HTML)
     */
    private String content;

    /**
     * 是否HTML内容
     */
    private Boolean html;

    /**
     * 附件资源地址(英文逗号分隔)
     */
    private String attachUrl;

    /**
     * 附件资源地址列表(优先使用此值)
     */
    private Collection<String> attachUrlColl;

}
