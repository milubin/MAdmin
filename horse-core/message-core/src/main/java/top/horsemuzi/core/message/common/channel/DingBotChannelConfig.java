package top.horsemuzi.core.message.common.channel;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 钉钉机器人渠道配置
 *
 * @author 马滨
 * @date 2022/09/25 16:17
 **/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class DingBotChannelConfig extends ChannelConfig implements Serializable {
    private static final long serialVersionUID = 3036087387198082149L;

    @ApiModelProperty("密钥（签名验证）")
    private String secret;

    @ApiModelProperty("自定义机器人的 webhook")
    private String webhook;

}
