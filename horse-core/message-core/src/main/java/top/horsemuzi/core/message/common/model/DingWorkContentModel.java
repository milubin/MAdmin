package top.horsemuzi.core.message.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 钉钉工作通知消息内容模型
 *
 * @author 马滨
 * @date 2022/09/25 16:29
 **/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class DingWorkContentModel extends ContentModel implements Serializable {

    private static final long serialVersionUID = 4205208433388803645L;

    /**
     * 访问令牌token(由主程序获取)
     */
    private String token;

    /**
     * 发送类型(例如: MessageTypeEnum.TEXT.getCode())
     */
    private String sendType;

    /**
     * 【文本消息】内容，【markdown消息】内容，【ActionCard消息】内容
     */
    private String content;

    /**
     * 【markdown消息】标题，【ActionCard消息】标题
     */
    private String title;

    /**
     * 【ActionCard消息】按钮布局
     */
    private String btnOrientation;

    /**
     * 【ActionCard消息】按钮的文案和跳转链接的json
     * [{"title":"一个按钮","action_url":"https://www.taobao.com"},{"title":"两个按钮","action_url":"https://www.tmall.com"}]
     */
    private String btns;


    /**
     * 【链接消息】点击消息跳转的URL，
     */
    private String url;


    /**
     * 图片、文件、语音消息 需要发送使用的素材ID字段
     */
    private String mediaId;

    /**
     * 语音时长
     */
    private String duration;

    /**
     * OA消息头
     * {"bgcolor":"FFBBBBBB","text":"头部标题"}
     */
    private String dingDingOaHead;

    /**
     * OA消息内容
     * {"title":"正文标题","form":[{"key":"姓名:","value":"张三"},{"key":"年龄:","value":"20"},{"key":"身高:","value":"1.8米"},{"key":"体重:","value":"130斤"},{"key":"学历:","value":"本科"},{"key":"爱好:","value":"打球、听音乐"}],"rich":{"num":"15.6","unit":"元"},"content":"大段文本大段文本大段文本大段文本大段文本大段文本","image":"@lADOADmaWMzazQKA","file_count":"3","author":"李四 "}
     */
    private String dingDingOaBody;

}
