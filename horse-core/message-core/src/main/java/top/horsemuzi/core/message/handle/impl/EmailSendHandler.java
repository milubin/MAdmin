package top.horsemuzi.core.message.handle.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.mail.Mail;
import cn.hutool.extra.mail.MailAccount;
import com.sun.mail.util.MailSSLSocketFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.ExceptionUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.core.message.common.MessageConstants;
import top.horsemuzi.core.message.common.MessageInfo;
import top.horsemuzi.core.message.common.channel.EmailChannelConfig;
import top.horsemuzi.core.message.common.model.EmailContentModel;
import top.horsemuzi.core.message.handle.BaseSendHandler;
import top.horsemuzi.core.message.handle.SendHandler;

import javax.activation.URLDataSource;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * 邮件消息发送处理
 *
 * @author mabin
 * @date 2022/11/02 10:51
 **/

@Slf4j
@Component
public class EmailSendHandler extends BaseSendHandler implements SendHandler {

    /**
     * 发送消息
     * 异常重试: 默认重试次数3, 重试延迟2秒, 每次重试后延时提高1.5倍，当返回结果不符合要求时，主动报错触发重试
     *
     * @param messageInfo
     */
    @Retryable(value = {Exception.class}, maxAttempts = 3, backoff = @Backoff(delay = 2000, multiplier = 1.5D))
    @Override
    public boolean send(MessageInfo messageInfo) throws Exception {
        ValidatorUtil.notEmpty(messageInfo, ResultCodeEnum.MESSAGE_PARAMS_ERROR);
        EmailChannelConfig emailChannelConfig = (EmailChannelConfig) messageInfo.getChannelConfig();
        ValidatorUtil.notEmpty(emailChannelConfig, ResultCodeEnum.MESSAGE_PARAMS_ERROR);
        MailAccount mailAccount = new MailAccount();
        mailAccount.setHost(emailChannelConfig.getHost())
                .setPort(emailChannelConfig.getPort())
                .setUser(emailChannelConfig.getUser())
                .setFrom(emailChannelConfig.getFrom())
                .setPass(emailChannelConfig.getPass())
                .setAuth(emailChannelConfig.getAuth())
                .setStarttlsEnable(emailChannelConfig.getStarttlsEnable())
                .setSslEnable(emailChannelConfig.getSslEnable())
                .setCustomProperty("mail.smtp.ssl.socketFactory", new MailSSLSocketFactory())
                .setTimeout(25000)
                .setConnectionTimeout(25000);
        Mail mail = Mail.create(mailAccount);

        EmailContentModel emailContentModel = (EmailContentModel) messageInfo.getContentModel();
        ValidatorUtil.notEmpty(emailContentModel, ResultCodeEnum.MESSAGE_PARAMS_ERROR);

        // 邮件编码格式、主题
        mail.setCharset(StandardCharsets.UTF_8).setTitle(emailContentModel.getTitle());
        // 收件人
        ValidatorUtil.isTrue(CollectionUtil.isNotEmpty(emailContentModel.getTosColl()) || StrUtil.isNotBlank(emailContentModel.getTos()), ResultCodeEnum.MESSAGE_PARAMS_ERROR);
        if (CollectionUtil.isNotEmpty(emailContentModel.getTosColl())) {
            mail.setTos(ArrayUtil.toArray(emailContentModel.getTosColl(), String.class));
        } else {
            mail.setTos(emailContentModel.getTos().split(StrUtil.COMMA));
        }
        // 抄送人
        if (CollectionUtil.isNotEmpty(emailContentModel.getCcsColl()) && StrUtil.isNotBlank(emailContentModel.getCcs())) {
            mail.setCcs(ArrayUtil.toArray(emailContentModel.getCcsColl(), String.class));
        } else if (CollectionUtil.isNotEmpty(emailContentModel.getCcsColl())) {
            mail.setCcs(ArrayUtil.toArray(emailContentModel.getCcsColl(), String.class));
        } else {
            if (StrUtil.isNotBlank(emailContentModel.getCcs())) {
                mail.setCcs(emailContentModel.getCcs().split(StrUtil.COMMA));
            }
        }
        // 密送人
        if (CollectionUtil.isNotEmpty(emailContentModel.getBccsColl()) && StrUtil.isNotBlank(emailContentModel.getBccs())) {
            mail.setBccs(ArrayUtil.toArray(emailContentModel.getBccsColl(), String.class));
        } else if (CollectionUtil.isNotEmpty(emailContentModel.getBccsColl())) {
            mail.setBccs(ArrayUtil.toArray(emailContentModel.getBccsColl(), String.class));
        } else {
            if (StrUtil.isNotBlank(emailContentModel.getBccs())) {
                mail.setBccs(emailContentModel.getBccs().split(StrUtil.COMMA));
            }
        }
        // 邮件内容、是否HTML
        ValidatorUtil.notBlank(emailContentModel.getContent(), ResultCodeEnum.MESSAGE_PARAMS_ERROR);
        mail.setContent(emailContentModel.getContent());
        mail.setHtml(!ObjectUtil.isNull(emailContentModel.getHtml()) && emailContentModel.getHtml());
        // 在线附件
        List<URLDataSource> urlDataSourceList = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(emailContentModel.getAttachUrlColl()) && StrUtil.isNotBlank(emailContentModel.getAttachUrl())) {
            for (String url : emailContentModel.getAttachUrlColl()) {
                urlDataSourceList.add(new URLDataSource(new URL(url)));
            }
        } else if (CollectionUtil.isNotEmpty(emailContentModel.getAttachUrlColl())) {
            for (String url : emailContentModel.getAttachUrlColl()) {
                urlDataSourceList.add(new URLDataSource(new URL(url)));
            }
        } else {
            if (StrUtil.isNotBlank(emailContentModel.getAttachUrl())) {
                for (String url : StrUtil.split(emailContentModel.getAttachUrl(), StrUtil.COMMA)) {
                    urlDataSourceList.add(new URLDataSource(new URL(url)));
                }
            }
        }
        if (CollectionUtil.isNotEmpty(urlDataSourceList)) {
            mail.setAttachments(ArrayUtil.toArray(urlDataSourceList, URLDataSource.class));
        }
        // 发送邮件
        mail.send();
        return true;
    }

    /**
     * 当重试次数耗尽依然出现异常时触发此回调方法
     * 使用说明：
     * 1、参数异常类型需要与@Retryable的异常类型保持一致
     * 2、recover方法返回值需要与重试方法返回值保持一致
     *
     * @param e
     * @return
     */
    @Recover
    public boolean recover(Exception e) {
        log.error("{} 邮件信息发送处理异常: {}", MessageConstants.MSG_LOG_HEADER, ExceptionUtil.getMessage(e));
        return false;
    }

}
