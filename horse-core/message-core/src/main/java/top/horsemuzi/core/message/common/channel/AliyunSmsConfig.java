package top.horsemuzi.core.message.common.channel;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 阿里云SMS短信发送渠道配置
 *
 * @author mabin
 * @date 2022/11/12 16:39
 **/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class AliyunSmsConfig extends ChannelConfig implements Serializable {

    private static final long serialVersionUID = -7303269736916416519L;

    @ApiModelProperty("配置节点")
    private String endpoint;

    @ApiModelProperty("AccessKey ID")
    private String accessKeyId;

    @ApiModelProperty("AccessKey Secret")
    private String accessKeySecret;

    @ApiModelProperty("短信签名")
    private String signName;

}
