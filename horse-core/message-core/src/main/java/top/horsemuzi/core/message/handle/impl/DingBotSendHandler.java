package top.horsemuzi.core.message.handle.impl;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.ExceptionUtil;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.common.util.http.HttpUtil;
import top.horsemuzi.core.message.common.MessageConstants;
import top.horsemuzi.core.message.common.MessageInfo;
import top.horsemuzi.core.message.common.base.ding.DingBotParam;
import top.horsemuzi.core.message.common.base.ding.DingBotResponse;
import top.horsemuzi.core.message.common.channel.DingBotChannelConfig;
import top.horsemuzi.core.message.common.model.DingBotContentModel;
import top.horsemuzi.core.message.enums.MessageTypeEnum;
import top.horsemuzi.core.message.handle.BaseSendHandler;
import top.horsemuzi.core.message.handle.SendHandler;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * 钉钉机器人消息发送处理
 *
 * @author 马滨
 * @date 2022/09/25 16:54
 **/
@Slf4j
@Component
public class DingBotSendHandler extends BaseSendHandler implements SendHandler {

    /**
     * 发送消息
     *
     * @param messageInfo
     */
    @Retryable(value = {Exception.class}, maxAttempts = 3, backoff = @Backoff(delay = 2000, multiplier = 1.5D))
    @Override
    public boolean send(MessageInfo messageInfo) throws Exception {
        ValidatorUtil.notEmpty(messageInfo, ResultCodeEnum.MESSAGE_PARAMS_ERROR);
        // 组装参数
        DingBotParam dingBotParam = new DingBotParam();
        // 接收者相关
        DingBotParam.AtVO atVO = new DingBotParam.AtVO();
        if (StrUtil.equals(CollectionUtil.getFirst(messageInfo.getReceiver()), MessageConstants.SEND_ALL)) {
            atVO.setIsAtAll(Boolean.TRUE);
        } else {
            atVO.setAtUserIds(new ArrayList<>(messageInfo.getReceiver()));
        }
        dingBotParam.setAt(atVO);
        // 消息类型以及内容相关
        DingBotContentModel dingBotContentModel = (DingBotContentModel) messageInfo.getContentModel();
        ValidatorUtil.notEmpty(dingBotContentModel, ResultCodeEnum.MESSAGE_PARAMS_ERROR);
        String sendType = dingBotContentModel.getSendType();
        dingBotParam.setMsgtype(MessageTypeEnum.getDingBotMessageType(sendType));
        if (StrUtil.equals(MessageTypeEnum.TEXT.getCode(), sendType)) {
            dingBotParam.setText(new DingBotParam.TextVO()
                    .setContent(dingBotContentModel.getContent()));
        } else if (StrUtil.equals(MessageTypeEnum.MARKDOWN.getCode(), sendType)) {
            dingBotParam.setMarkdown(new DingBotParam.MarkdownVO()
                    .setTitle(dingBotContentModel.getTitle())
                    .setText(dingBotContentModel.getContent()));
        } else if (StrUtil.equals(MessageTypeEnum.LINK.getCode(), sendType)) {
            dingBotParam.setLink(new DingBotParam.LinkVO()
                    .setTitle(dingBotContentModel.getTitle())
                    .setText(dingBotContentModel.getContent())
                    .setMessageUrl(dingBotContentModel.getUrl())
                    .setPicUrl(dingBotContentModel.getPicUrl()));
        } else if (StrUtil.equals(MessageTypeEnum.NEWS.getCode(), sendType)) {
            dingBotParam.setFeedCard(new DingBotParam.FeedCardVO()
                    .setLinks(JsonUtil.fromJson(dingBotContentModel.getFeedCards(),
                            new TypeReference<List<DingBotParam.FeedCardVO.LinksVO>>() {
                            })));
        } else if (StrUtil.equals(MessageTypeEnum.ACTION_CARD.getCode(), sendType)) {
            dingBotParam.setActionCard(new DingBotParam.ActionCardVO()
                    .setTitle(dingBotContentModel.getTitle())
                    .setBtnOrientation(dingBotContentModel.getBtnOrientation())
                    .setText(dingBotContentModel.getContent())
                    .setBtns(JsonUtil.fromJson(dingBotContentModel.getBtns(),
                            new TypeReference<List<DingBotParam.ActionCardVO.BtnsVO>>() {
                            })));
        } else {
            log.error("{} 钉钉机器人发送消息, {}", MessageConstants.MSG_LOG_HEADER, ResultCodeEnum.MESSAGE_TYPE_NOT_EXIST_ERROR.getMessage());
            throw new BusinessException(ResultCodeEnum.MESSAGE_TYPE_NOT_EXIST_ERROR);
        }

        // 组装URL并签名
        DingBotChannelConfig dingBotChannelConfig = (DingBotChannelConfig) messageInfo.getChannelConfig();
        ValidatorUtil.notEmpty(dingBotChannelConfig, ResultCodeEnum.MESSAGE_PARAMS_ERROR);
        long timestamp = System.currentTimeMillis();
        String sign = sign(timestamp, dingBotChannelConfig.getSecret());
        String url = StrUtil.concat(true, dingBotChannelConfig.getWebhook(), "&timestamp=", Long.toString(timestamp), "&sign=", sign);

        String response = HttpUtil.OK_HTTP.post(url, JsonUtil.toJson(dingBotParam));
        if (StrUtil.isBlank(response)) {
            return false;
        }
        DingBotResponse dingBotResponse = JsonUtil.fromJson(response, DingBotResponse.class);
        return ObjectUtil.isNotNull(dingBotResponse) && dingBotResponse.getErrCode() == 0;
    }

    @Recover
    public boolean recover(Exception e) {
        log.error("{} 钉钉机器人发送消息处理异常: {}", MessageConstants.MSG_LOG_HEADER, ExceptionUtil.getMessage(e));
        return false;
    }

    /**
     * 使用HmacSHA256算法计算签名
     *
     * @param
     * @param secret
     * @return
     */
    private String sign(long timestamp, String secret) {
        String sign = StrUtil.EMPTY;
        try {
            String stringToSign = timestamp + String.valueOf(StrUtil.C_LF) + secret;
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(new SecretKeySpec(secret.getBytes(StandardCharsets.UTF_8), "HmacSHA256"));
            byte[] signData = mac.doFinal(stringToSign.getBytes(StandardCharsets.UTF_8));
            sign = URLEncoder.encode(Base64.encode(signData), StandardCharsets.UTF_8.toString());
        } catch (Exception e) {
            log.error("{} 钉钉机器人发送消息，签名异常: {}", MessageConstants.MSG_LOG_HEADER, ExceptionUtil.getMessage(e));
        }
        return sign;
    }

}
