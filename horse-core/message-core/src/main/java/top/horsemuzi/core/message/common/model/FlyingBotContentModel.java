package top.horsemuzi.core.message.common.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 飞书机器人
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/04/11 10:23
 **/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class FlyingBotContentModel extends ContentModel implements Serializable {
    private static final long serialVersionUID = -4548175122753472195L;

    @ApiModelProperty("发送类型")
    private String sendType;

    /**
     * 发送内容
     */
    @ApiModelProperty("发送内容")
    private String content;

    /**
     * 发送标题
     */
    @ApiModelProperty("发送标题")
    private String title;

    /**
     * 媒体Id
     */
    @ApiModelProperty("媒体Id")
    private String mediaId;


    /**
     * 富文本内容：[[{"tag":"text","text":"项目有更新: "},{"tag":"a","text":"请查看","href":"http://www.example.com/"},{"tag":"at","user_id":"ou_18eac8********17ad4f02e8bbbb"}]]
     */
    @ApiModelProperty("富文本内容")
    private String postContent;

}
