package top.horsemuzi.core.message.handle.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.request.OapiMessageCorpconversationAsyncsendV2Request;
import com.dingtalk.api.response.OapiMessageCorpconversationAsyncsendV2Response;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.ExceptionUtil;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.core.message.common.MessageConstants;
import top.horsemuzi.core.message.common.MessageInfo;
import top.horsemuzi.core.message.common.channel.DingWorkChannelConfig;
import top.horsemuzi.core.message.common.model.DingWorkContentModel;
import top.horsemuzi.core.message.enums.MessageTypeEnum;
import top.horsemuzi.core.message.handle.BaseSendHandler;
import top.horsemuzi.core.message.handle.SendHandler;

import java.util.List;
import java.util.Set;

/**
 * 钉钉工作通知消息发送处理
 *
 * @author 马滨
 * @date 2022/09/25 22:51
 **/
@Slf4j
@Component
public class DingWorkSendHandler extends BaseSendHandler implements SendHandler {


    /**
     * 发送消息
     *
     * @param messageInfo
     */
    @Retryable(value = {Exception.class}, maxAttempts = 3, backoff = @Backoff(delay = 2000, multiplier = 1.5D))
    @Override
    public boolean send(MessageInfo messageInfo) throws Exception {
        ValidatorUtil.notEmpty(messageInfo, ResultCodeEnum.MESSAGE_PARAMS_ERROR);
        // 校验参数
        DingWorkChannelConfig dingWorkChannelConfig = (DingWorkChannelConfig) messageInfo.getChannelConfig();
        DingWorkContentModel dingWorkContentModel = (DingWorkContentModel) messageInfo.getContentModel();
        ValidatorUtil.allNotEmpty(ResultCodeEnum.MESSAGE_PARAMS_ERROR, dingWorkChannelConfig, dingWorkContentModel);
        // token是否存在
        String token = dingWorkContentModel.getToken();
        String sendUrl = dingWorkChannelConfig.getSendUrl();
        ValidatorUtil.allNotBlank(ResultCodeEnum.MESSAGE_PARAMS_ERROR, token, sendUrl);
        // 构建参数
        OapiMessageCorpconversationAsyncsendV2Request requestParam = buildParam(dingWorkChannelConfig, dingWorkContentModel, messageInfo.getReceiver());
        // 发送消息
        OapiMessageCorpconversationAsyncsendV2Response response =
                new DefaultDingTalkClient(sendUrl).execute(requestParam, token);
        return ObjectUtil.isNotNull(response) && ObjectUtil.equals(Constants.COMMON_CODE.LONG_ZONE, response.getErrcode());
    }

    @Recover
    public boolean recover(Exception e) {
        log.error("{} 钉钉工作通知消息发送处理异常: {}", MessageConstants.MSG_LOG_HEADER, ExceptionUtil.getMessage(e));
        return false;
    }


    /**
     * 组装参数
     *
     * @param dingWorkChannelConfig
     * @param dingWorkContentModel
     * @param receiver
     * @return
     */
    private OapiMessageCorpconversationAsyncsendV2Request buildParam(DingWorkChannelConfig dingWorkChannelConfig, DingWorkContentModel dingWorkContentModel, Set<String> receiver) {
        OapiMessageCorpconversationAsyncsendV2Request req = new OapiMessageCorpconversationAsyncsendV2Request();
        try {
            // 接收者相关
            if (StrUtil.equals(CollectionUtil.getFirst(receiver), MessageConstants.SEND_ALL)) {
                req.setToAllUser(Boolean.TRUE);
            } else {
                req.setUseridList(CollectionUtil.join(receiver, StrUtil.COMMA));
            }
            req.setAgentId(Long.parseLong(dingWorkChannelConfig.getAgentId()));

            OapiMessageCorpconversationAsyncsendV2Request.Msg message = new OapiMessageCorpconversationAsyncsendV2Request.Msg();
            String sendType = dingWorkContentModel.getSendType();
            message.setMsgtype(MessageTypeEnum.getDingWorkMessageType(sendType));

            // 根据类型设置入参
            if (MessageTypeEnum.TEXT.getCode().equals(sendType)) {
                OapiMessageCorpconversationAsyncsendV2Request.Text textObj = new OapiMessageCorpconversationAsyncsendV2Request.Text();
                textObj.setContent(dingWorkContentModel.getContent());
                message.setText(textObj);
            } else if (MessageTypeEnum.IMAGE.getCode().equals(dingWorkContentModel.getSendType())) {
                OapiMessageCorpconversationAsyncsendV2Request.Image image = new OapiMessageCorpconversationAsyncsendV2Request.Image();
                image.setMediaId(dingWorkContentModel.getMediaId());
                message.setImage(image);
            } else if (MessageTypeEnum.VOICE.getCode().equals(dingWorkContentModel.getSendType())) {
                OapiMessageCorpconversationAsyncsendV2Request.Voice voice = new OapiMessageCorpconversationAsyncsendV2Request.Voice();
                voice.setMediaId(dingWorkContentModel.getMediaId());
                voice.setDuration(dingWorkContentModel.getDuration());
                message.setVoice(voice);
            } else if (MessageTypeEnum.FILE.getCode().equals(dingWorkContentModel.getSendType())) {
                OapiMessageCorpconversationAsyncsendV2Request.File file = new OapiMessageCorpconversationAsyncsendV2Request.File();
                file.setMediaId(dingWorkContentModel.getMediaId());
                message.setFile(file);
            } else if (MessageTypeEnum.LINK.getCode().equals(dingWorkContentModel.getSendType())) {
                OapiMessageCorpconversationAsyncsendV2Request.Link link = new OapiMessageCorpconversationAsyncsendV2Request.Link();
                link.setText(dingWorkContentModel.getContent());
                link.setTitle(dingWorkContentModel.getTitle());
                link.setPicUrl(dingWorkContentModel.getMediaId());
                link.setMessageUrl(dingWorkContentModel.getUrl());
                message.setLink(link);
            } else if (MessageTypeEnum.MARKDOWN.getCode().equals(dingWorkContentModel.getSendType())) {
                OapiMessageCorpconversationAsyncsendV2Request.Markdown markdown = new OapiMessageCorpconversationAsyncsendV2Request.Markdown();
                markdown.setText(dingWorkContentModel.getContent());
                markdown.setTitle(dingWorkContentModel.getTitle());
                message.setMarkdown(markdown);
            } else if (MessageTypeEnum.ACTION_CARD.getCode().equals(dingWorkContentModel.getSendType())) {
                OapiMessageCorpconversationAsyncsendV2Request.ActionCard actionCard = new OapiMessageCorpconversationAsyncsendV2Request.ActionCard();
                actionCard.setTitle(dingWorkContentModel.getTitle());
                actionCard.setMarkdown(dingWorkContentModel.getContent());
                actionCard.setBtnOrientation(dingWorkContentModel.getBtnOrientation());
                actionCard.setBtnJsonList(JsonUtil.fromJson(dingWorkContentModel.getBtns(), new TypeReference<List<OapiMessageCorpconversationAsyncsendV2Request.BtnJsonList>>() {
                }));
                message.setActionCard(actionCard);
            } else if (MessageTypeEnum.OA.getCode().equals(dingWorkContentModel.getSendType())) {
                OapiMessageCorpconversationAsyncsendV2Request.OA oa = new OapiMessageCorpconversationAsyncsendV2Request.OA();
                oa.setMessageUrl(dingWorkContentModel.getUrl());
                oa.setHead(JsonUtil.fromJson(dingWorkContentModel.getDingDingOaHead(), OapiMessageCorpconversationAsyncsendV2Request.Head.class));
                oa.setBody(JsonUtil.fromJson(dingWorkContentModel.getDingDingOaBody(), OapiMessageCorpconversationAsyncsendV2Request.Body.class));
                message.setOa(oa);
            } else {
                log.error("{} 钉钉工作通知发送消息, {}", MessageConstants.MSG_LOG_HEADER, ResultCodeEnum.MESSAGE_TYPE_NOT_EXIST_ERROR.getMessage());
                throw new BusinessException(ResultCodeEnum.MESSAGE_TYPE_NOT_EXIST_ERROR);
            }
            req.setMsg(message);
            return req;
        } catch (Exception e) {
            log.error("{} 钉钉工作通知发送消息异常: {}", MessageConstants.MSG_LOG_HEADER, ExceptionUtil.getMessage(e));
            return req;
        }
    }

}
