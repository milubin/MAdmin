package top.horsemuzi.core.message.common.channel;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 钉钉工作通知渠道配置
 *
 * @author 马滨
 * @date 2022/09/25 16:20
 **/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class DingWorkChannelConfig extends ChannelConfig implements Serializable {
    private static final long serialVersionUID = -5692631909551112307L;

    @ApiModelProperty("应用的唯一标识key")
    private String appKey;

    @ApiModelProperty("应用的密钥")
    private String appSecret;

    @ApiModelProperty("发送消息时使用的微应用的AgentID")
    private String agentId;

    @ApiModelProperty("发送工作消息URL地址")
    private String sendUrl;

    @ApiModelProperty("生成token的URL地址")
    private String tokenUrl;

}
