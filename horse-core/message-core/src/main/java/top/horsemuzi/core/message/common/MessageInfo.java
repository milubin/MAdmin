package top.horsemuzi.core.message.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import top.horsemuzi.core.message.common.channel.ChannelConfig;
import top.horsemuzi.core.message.common.model.ContentModel;

import java.io.Serializable;
import java.util.Set;

/**
 * 消息信息实例
 *
 * @author 马滨
 * @date 2022/09/25 16:47
 **/
@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class MessageInfo implements Serializable {

    private static final long serialVersionUID = 844707224436857682L;

    /**
     * 接收者（@all: 代表@所有人）
     */
    private Set<String> receiver;

    /**
     * 渠道配置
     */
    private ChannelConfig channelConfig;

    /**
     * 发送文案模型
     */
    private ContentModel contentModel;

}
