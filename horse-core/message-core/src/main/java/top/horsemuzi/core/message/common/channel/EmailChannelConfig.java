package top.horsemuzi.core.message.common.channel;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 邮件渠道配置
 *
 * @author mabin
 * @date 2022/10/12 19:09
 **/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class EmailChannelConfig extends ChannelConfig implements Serializable {

    private static final long serialVersionUID = 6132519179466755913L;

    /**
     * SMTP服务器域名
     */
    @ApiModelProperty("SMTP服务器域名")
    private String host;

    /**
     * SMTP服务端口
     */
    @ApiModelProperty("SMTP服务端口")
    private Integer port;

    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    private String user;

    /**
     * 密码
     */
    @ApiModelProperty("密码")
    private String pass;

    /**
     * 发送方
     */
    @ApiModelProperty("发送方")
    private String from;

    /**
     * 使用 SSL安全连接: SSL加密方式发送邮件 在使用QQ或Gmail邮箱时，需要强制开启SSL支持
     */
    @ApiModelProperty("是否使用SSL安全连接")
    private Boolean sslEnable;

    /**
     * 是否使用用户名密码认证
     */
    @ApiModelProperty("是否使用用户名密码认证")
    private Boolean auth;

    /**
     * 是否使用STARTTLS安全连接
     */
    @ApiModelProperty("是否使用STARTTLS安全连接")
    private Boolean starttlsEnable;


}
