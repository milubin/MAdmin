package top.horsemuzi.core.message.common.base.ding;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 钉钉机器人发送参数
 *
 * @author 马滨
 * @date 2022/09/25 17:17
 **/
@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class DingBotParam implements Serializable {

    private static final long serialVersionUID = -8998540123559401895L;

    /**
     * at
     */
    private AtVO at;
    /**
     * text
     */
    private TextVO text;
    /**
     * link
     */
    private LinkVO link;
    /**
     * markdown
     */
    private MarkdownVO markdown;
    /**
     * actionCard
     */
    private ActionCardVO actionCard;
    /**
     * feedCard
     */
    private FeedCardVO feedCard;
    /**
     * msgtype
     */
    private String msgtype;

    /**
     * AtVO
     */
    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public static class AtVO implements Serializable {
        private static final long serialVersionUID = -7518313869636391236L;
        /**
         * atMobiles
         */
        private List<String> atMobiles;
        /**
         * atUserIds
         */
        private List<String> atUserIds;
        /**
         * isAtAll
         */
        private Boolean isAtAll;
    }

    /**
     * TextVO
     */
    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public static class TextVO implements Serializable {
        private static final long serialVersionUID = 1167678946548595220L;
        /**
         * content
         */
        private String content;
    }

    /**
     * LinkVO
     */
    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public static class LinkVO implements Serializable {
        private static final long serialVersionUID = -6923949807737555488L;
        /**
         * text
         */
        private String text;
        /**
         * title
         */
        private String title;
        /**
         * picUrl
         */
        private String picUrl;
        /**
         * messageUrl
         */
        private String messageUrl;
    }

    /**
     * MarkdownVO
     */
    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public static class MarkdownVO implements Serializable {
        private static final long serialVersionUID = 8085779308073489486L;
        /**
         * title
         */
        private String title;
        /**
         * text
         */
        private String text;
    }

    /**
     * ActionCardVO
     */
    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public static class ActionCardVO implements Serializable{
        private static final long serialVersionUID = -9155292336800192580L;
        /**
         * title
         */
        private String title;
        /**
         * text
         */
        private String text;
        /**
         * btnOrientation
         */
        private String btnOrientation;
        /**
         * btns
         */
        private List<BtnsVO> btns;

        /**
         * BtnsVO
         */
        @NoArgsConstructor
        @Data
        @AllArgsConstructor
        public static class BtnsVO implements Serializable{
            private static final long serialVersionUID = -614812580186600701L;
            /**
             * title
             */
            private String title;
            /**
             * actionURL
             */
            private String actionURL;
        }
    }

    /**
     * FeedCardVO
     */
    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public static class FeedCardVO implements Serializable{
        private static final long serialVersionUID = 228317357305016449L;
        /**
         * links
         */
        private List<LinksVO> links;

        /**
         * LinksVO
         */
        @NoArgsConstructor
        @Data
        @AllArgsConstructor
        public static class LinksVO implements Serializable{
            private static final long serialVersionUID = 2650731304407117469L;
            /**
             * title
             */
            private String title;
            /**
             * messageURL
             */
            private String messageURL;
            /**
             * picURL
             */
            private String picURL;
        }
    }

}
