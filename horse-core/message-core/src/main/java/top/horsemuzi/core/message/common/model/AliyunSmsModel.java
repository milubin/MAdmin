package top.horsemuzi.core.message.common.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

/**
 * 阿里云SMS短信通知内容模型
 *
 * @author mabin
 * @date 2022/11/12 16:43
 **/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class AliyunSmsModel extends ContentModel implements Serializable {

    private static final long serialVersionUID = 1021641922395055423L;

    @ApiModelProperty(value = "手机号", notes = "多个使用英文逗号分隔")
    private String phones;

    /**
     * 阿里 需使用 模板变量名称对应内容 例如: code=1234
     * 腾讯 需使用 模板变量顺序对应内容 例如: 1=1234, 1为模板内第一个参数
     */
    @ApiModelProperty(value = "短信模板对应参数")
    private Map<String, String> paramsMap;

    @ApiModelProperty(value = "模板id", notes = "模板不同, 短信内容不同")
    private String templateId;

}
