package top.horsemuzi.core.message.common.base.flying;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;


/**
 * 飞书机器人消息发送结果
 *
 * @author 马滨
 * @date 2022/09/25 17:17
 **/
@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class FlyingBotResponse {

    /**
     * extra
     */
    @JsonProperty("Extra")
    private Object extra;
    /**
     * statusCode
     */
    @JsonProperty("StatusCode")
    private Integer statusCode;
    /**
     * statusMessage
     */
    @JsonProperty("StatusMessage")
    private String statusMessage;
    /**
     * code
     */
    @JsonProperty("code")
    private Integer code;
    /**
     * msg
     */
    @JsonProperty("msg")
    private String msg;
    /**
     * data
     */
    @JsonProperty("data")
    private DataDTO data;

    /**
     * DataDTO
     */
    @NoArgsConstructor
    @Data
    public static class DataDTO {
    }
}
