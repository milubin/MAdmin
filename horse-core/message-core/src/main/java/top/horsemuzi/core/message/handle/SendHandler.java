package top.horsemuzi.core.message.handle;


import top.horsemuzi.core.message.common.MessageInfo;

/**
 * 消息发送处理接口
 *
 * @author 马滨
 * @date 2022/09/25 16:22
 **/

public interface SendHandler {

    /**
     * 发送消息
     *
     * @param messageInfo
     * @return
     * @throws Exception
     */
    boolean send(MessageInfo messageInfo) throws Exception;

}
