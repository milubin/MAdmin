package top.horsemuzi.core.message.common.channel;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 飞书机器人渠道配置
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/04/11 10:25
 **/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class FlyingBotChannelConfig extends ChannelConfig implements Serializable {
    private static final long serialVersionUID = 8032168895180901303L;

    @ApiModelProperty("自定义机器人的 webhook")
    private String webhook;

    @ApiModelProperty("密钥（签名验证）")
    private String secret;

}
