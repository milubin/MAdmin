package top.horsemuzi.core.message.common.base.ding;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 钉钉机器人消息发送结果
 *
 * @author 马滨
 * @date 2022/09/25 18:04
 **/
@Data
public class DingBotResponse implements Serializable {

    private static final long serialVersionUID = -5127512625809172423L;

    @JsonProperty(value = "errcode")
    private Integer errCode;

    @JsonProperty(value = "errmsg")
    private String errMsg;

}
