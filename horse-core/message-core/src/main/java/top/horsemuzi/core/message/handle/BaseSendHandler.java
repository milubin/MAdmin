package top.horsemuzi.core.message.handle;

import lombok.extern.slf4j.Slf4j;
import top.horsemuzi.core.message.common.MessageInfo;

/**
 * 消息发送处理抽象实例
 *
 * @author 马滨
 * @date 2022/09/25 16:23
 **/

@Slf4j
public abstract class BaseSendHandler implements SendHandler {

    /**
     * 发送消息
     *
     * @param messageInfo
     */
    @Override
    public abstract boolean send(MessageInfo messageInfo) throws Exception;

}
