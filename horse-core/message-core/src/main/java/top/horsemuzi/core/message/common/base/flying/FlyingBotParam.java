package top.horsemuzi.core.message.common.base.flying;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;


/**
 * 飞书机器人发送参数
 *
 * @author 马滨
 * @date 2022/09/25 17:17
 **/
@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class FlyingBotParam {

    /**
     * 时间戳（开启签名验证后，发送任何消息均需要传参此字段）
     */
    @JsonProperty("timestamp")
    private Long timestamp;

    /**
     * 签名（开启签名验证后，发送任何消息均需要传参此字段）
     */
    @JsonProperty("sign")
    private String sign;

    /**
     * msgType
     */
    @JsonProperty("msg_type")
    private String msgType;
    /**
     * content
     */
    @JsonProperty("content")
    private ContentDTO content;
    /**
     * card
     */
    @JsonProperty("card")
    private CardDTO card;

    /**
     * ContentDTO
     */
    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class ContentDTO {
        /**
         * text
         */
        @JsonProperty("text")
        private String text;
        /**
         * post
         */
        @JsonProperty("post")
        private PostDTO post;
        /**
         * shareChatId
         */
        @JsonProperty("share_chat_id")
        private String shareChatId;
        /**
         * imageKey
         */
        @JsonProperty("image_key")
        private String imageKey;

        /**
         * PostDTO
         */
        @NoArgsConstructor
        @Data
        @AllArgsConstructor
        public static class PostDTO {
            /**
             * zhCn
             */
            @JsonProperty("zh_cn")
            private ZhCnDTO zhCn;

            /**
             * ZhCnDTO
             */
            @NoArgsConstructor
            @Data
            @AllArgsConstructor
            public static class ZhCnDTO {
                /**
                 * title
                 */
                @JsonProperty("title")
                private String title;
                /**
                 * content
                 */
                @JsonProperty("content")
                private List<List<PostContentDTO>> content;

                /**
                 * ContentDTO
                 */
                @NoArgsConstructor
                @Data
                @AllArgsConstructor
                public static class PostContentDTO {
                    /**
                     * tag
                     */
                    @JsonProperty("tag")
                    private String tag;
                    /**
                     * text
                     */
                    @JsonProperty("text")
                    private String text;
                    /**
                     * href
                     */
                    @JsonProperty("href")
                    private String href;
                    /**
                     * userId
                     */
                    @JsonProperty("user_id")
                    private String userId;
                }
            }
        }
    }

    /**
     * CardDTO
     */
    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public static class CardDTO {
        /**
         * config
         */
        @JsonProperty("config")
        private ConfigDTO config;
        /**
         * elements
         */
        @JsonProperty("elements")
        private List<ElementsDTO> elements;
        /**
         * header
         */
        @JsonProperty("header")
        private HeaderDTO header;

        /**
         * ConfigDTO
         */
        @NoArgsConstructor
        @Data
        @AllArgsConstructor
        public static class ConfigDTO {
            /**
             * wideScreenMode
             */
            @JsonProperty("wide_screen_mode")
            private Boolean wideScreenMode;
            /**
             * enableForward
             */
            @JsonProperty("enable_forward")
            private Boolean enableForward;
        }

        /**
         * HeaderDTO
         */
        @NoArgsConstructor
        @Data
        @AllArgsConstructor
        public static class HeaderDTO {
            /**
             * title
             */
            @JsonProperty("title")
            private TitleDTO title;

            /**
             * TitleDTO
             */
            @NoArgsConstructor
            @Data
            @AllArgsConstructor

            public static class TitleDTO {
                /**
                 * content
                 */
                @JsonProperty("content")
                private String content;
                /**
                 * tag
                 */
                @JsonProperty("tag")
                private String tag;
            }
        }

        /**
         * ElementsDTO
         */
        @NoArgsConstructor
        @Data
        @AllArgsConstructor
        public static class ElementsDTO {
            /**
             * tag
             */
            @JsonProperty("tag")
            private String tag;
            /**
             * text
             */
            @JsonProperty("text")
            private TextDTO text;
            /**
             * actions
             */
            @JsonProperty("actions")
            private List<ActionsDTO> actions;

            /**
             * TextDTO
             */
            @NoArgsConstructor
            @Data
            @AllArgsConstructor
            public static class TextDTO {
                /**
                 * content
                 */
                @JsonProperty("content")
                private String content;
                /**
                 * tag
                 */
                @JsonProperty("tag")
                private String tag;
            }

            /**
             * ActionsDTO
             */
            @NoArgsConstructor
            @Data
            @AllArgsConstructor
            public static class ActionsDTO {
                /**
                 * tag
                 */
                @JsonProperty("tag")
                private String tag;
                /**
                 * text
                 */
                @JsonProperty("text")
                private TextDTO text;
                /**
                 * url
                 */
                @JsonProperty("url")
                private String url;
                /**
                 * type
                 */
                @JsonProperty("type")
                private String type;


                /**
                 * TextDTO
                 */
                @NoArgsConstructor
                @Data
                public static class TextDTO {
                }

                /**
                 * ValueDTO
                 */
                @Data
                @NoArgsConstructor
                public static class ValueDTO {
                }
            }
        }
    }
}
