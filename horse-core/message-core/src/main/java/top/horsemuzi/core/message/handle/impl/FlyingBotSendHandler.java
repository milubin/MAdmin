package top.horsemuzi.core.message.handle.impl;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.HmacAlgorithm;
import cn.hutool.http.ContentType;
import cn.hutool.http.Header;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.ExceptionUtil;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.common.util.http.HttpUtil;
import top.horsemuzi.core.message.common.MessageConstants;
import top.horsemuzi.core.message.common.MessageInfo;
import top.horsemuzi.core.message.common.base.flying.FlyingBotParam;
import top.horsemuzi.core.message.common.base.flying.FlyingBotResponse;
import top.horsemuzi.core.message.common.channel.FlyingBotChannelConfig;
import top.horsemuzi.core.message.common.model.FlyingBotContentModel;
import top.horsemuzi.core.message.enums.MessageTypeEnum;
import top.horsemuzi.core.message.handle.BaseSendHandler;
import top.horsemuzi.core.message.handle.SendHandler;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Collections;
import java.util.List;

/**
 * 飞书机器人消息发送处理
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/04/11 10:30
 **/
@Slf4j
@Component
public class FlyingBotSendHandler extends BaseSendHandler implements SendHandler {


    /**
     * 发送消息
     *
     * @param messageInfo
     */
    @Retryable(value = {Exception.class}, maxAttempts = 3, backoff = @Backoff(delay = 2000, multiplier = 1.5D))
    @Override
    public boolean send(MessageInfo messageInfo) throws Exception {
        ValidatorUtil.notEmpty(messageInfo, ResultCodeEnum.MESSAGE_PARAMS_ERROR);
        // 组装参数
        FlyingBotParam flyingBotParam = new FlyingBotParam();
        FlyingBotContentModel flyingBotContentModel = (FlyingBotContentModel) messageInfo.getContentModel();
        String sendType = flyingBotContentModel.getSendType();
        flyingBotParam.setMsgType(MessageTypeEnum.getFlyingBotMessageType(sendType));
        // 消息类型及内容相关
        if (StrUtil.equals(MessageTypeEnum.TEXT.getCode(), sendType)) {
            flyingBotParam.setContent(new FlyingBotParam.ContentDTO().setText(flyingBotContentModel.getContent()));
        } else if (StrUtil.equals(MessageTypeEnum.RICH_TEXT.getCode(), sendType)) {
            flyingBotParam.setContent(new FlyingBotParam.ContentDTO().setPost(new FlyingBotParam.ContentDTO.PostDTO()
                    .setZhCn(new FlyingBotParam.ContentDTO.PostDTO.ZhCnDTO().setTitle(flyingBotContentModel.getTitle())
                            .setContent(Collections.singletonList(JsonUtil.fromJson(flyingBotContentModel.getPostContent(),
                                    new TypeReference<List<FlyingBotParam.ContentDTO.PostDTO.ZhCnDTO.PostContentDTO>>() {
                                    }))))));
        } else if (StrUtil.equals(MessageTypeEnum.SHARE_CHAT.getCode(), sendType)) {
            flyingBotParam.setContent(new FlyingBotParam.ContentDTO().setShareChatId(flyingBotContentModel.getMediaId()));
        } else if (StrUtil.equals(MessageTypeEnum.IMAGE.getCode(), sendType)) {
            flyingBotParam.setContent(new FlyingBotParam.ContentDTO().setImageKey(flyingBotContentModel.getMediaId()));
        } else if (StrUtil.equals(MessageTypeEnum.ACTION_CARD.getCode(), sendType)) {
            // 待实现
        } else {
            log.error("{} 飞书机器人发送消息, {}", MessageConstants.MSG_LOG_HEADER, ResultCodeEnum.MESSAGE_TYPE_NOT_EXIST_ERROR.getMessage());
            throw new BusinessException(ResultCodeEnum.MESSAGE_TYPE_NOT_EXIST_ERROR);
        }
        // 组装URL并发送消息
        FlyingBotChannelConfig flyingBotChannelConfig = (FlyingBotChannelConfig) messageInfo.getChannelConfig();
        ValidatorUtil.notEmpty(flyingBotChannelConfig, ResultCodeEnum.MESSAGE_PARAMS_ERROR);
        ValidatorUtil.notBlank(flyingBotChannelConfig.getSecret(), ResultCodeEnum.MESSAGE_PARAMS_ERROR);
        generateSign(flyingBotParam, flyingBotChannelConfig.getSecret());
        String response = HttpUtil.OK_HTTP.post(flyingBotChannelConfig.getWebhook(), JsonUtil.toJson(flyingBotParam),
                Collections.singletonMap(Header.CONTENT_TYPE.getValue(), ContentType.JSON.getValue()),
                null, Boolean.FALSE, Boolean.TRUE);
        if (StrUtil.isBlank(response)) {
            return false;
        }
        FlyingBotResponse flyingBotResponse = JsonUtil.fromJson(response, FlyingBotResponse.class);
        return ObjectUtil.isNotNull(flyingBotResponse) && flyingBotResponse.getCode() == 0;
    }

    /**
     * 生成签名消息
     *
     * @param flyingBotParam
     * @param secret
     * @return void
     * @author mabin
     * @date 2023/4/12 9:31
     **/
    private void generateSign(FlyingBotParam flyingBotParam, String secret) throws Exception {
        // 时间戳单位：s
        long timestamp = Instant.now().getEpochSecond();
        flyingBotParam.setTimestamp(timestamp);
        String strToSign = timestamp + StrUtil.LF + secret;
        Mac mac = Mac.getInstance(HmacAlgorithm.HmacSHA256.getValue());
        mac.init(new SecretKeySpec(strToSign.getBytes(StandardCharsets.UTF_8), HmacAlgorithm.HmacSHA256.getValue()));
        flyingBotParam.setSign(Base64.encode(mac.doFinal(new byte[]{})));
    }

    @Recover
    public boolean recover(Exception e) {
        log.error("{} 飞书机器人发送消息处理异常: {}", MessageConstants.MSG_LOG_HEADER, ExceptionUtil.getMessage(e));
        return false;
    }

}
