package top.horsemuzi.core.message.enums;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.extra.spring.SpringUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import top.horsemuzi.core.message.common.channel.*;
import top.horsemuzi.core.message.common.model.*;
import top.horsemuzi.core.message.handle.SendHandler;
import top.horsemuzi.core.message.handle.impl.*;

/**
 * 消息发送渠道服务
 *
 * @author 马滨
 * @date 2022/09/25 22:49
 **/
@Getter
@ToString
@AllArgsConstructor
public enum SendEnum {

    /**
     * 消息发送服务渠道
     */
    DING_BOT(10, "钉钉机器人", new DingBotChannelConfig(), new DingBotContentModel(), SpringUtil.getBean(DingBotSendHandler.class)),
    DING_WORK(20, "钉钉工作通知", new DingWorkChannelConfig(), new DingWorkContentModel(), SpringUtil.getBean(DingWorkSendHandler.class)),
    FLYING_BOT(30, "飞书机器人", new FlyingBotChannelConfig(), new FlyingBotContentModel(), SpringUtil.getBean(FlyingBotSendHandler.class)),
    FLYING_WORK(40, "飞书工作通知", null, null, null),
    EMAIL(50, "邮箱", new EmailChannelConfig(), new EmailContentModel(), SpringUtil.getBean(EmailSendHandler.class)),
    SMS(60, "短信", new AliyunSmsConfig(), new AliyunSmsModel(), SpringUtil.getBean(AliyunSmsSendHandler.class)),
    WX_OPEN(70, "微信公众号", null, null, null);


    /**
     * 对应DB中渠道标识
     */
    private final Integer channel;

    /**
     * 渠道名称
     */
    private final String channelName;

    /**
     * 渠道配置实例
     */
    private final ChannelConfig channelConfig;

    /**
     * 消息内容模型实例
     */
    private final ContentModel contentModel;

    /**
     * 渠道服务实现
     */
    private final SendHandler sendHandler;

    /**
     * 获取渠道实现
     *
     * @param channel
     * @return
     */
    public static SendEnum getSendEnum(Integer channel) {
        for (SendEnum sendEnum : values()) {
            if (ObjectUtil.equals(channel, sendEnum.getChannel())) {
                return sendEnum;
            }
        }
        return null;
    }

    /**
     * 获取渠道名称
     *
     * @param channel
     * @return
     */
    public static String getChannelName(Integer channel) {
        for (SendEnum sendEnum : values()) {
            if (ObjectUtil.equals(sendEnum.getChannel(), channel)) {
                return sendEnum.getChannelName();
            }
        }
        return null;
    }


}
