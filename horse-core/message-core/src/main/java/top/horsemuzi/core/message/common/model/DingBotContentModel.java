package top.horsemuzi.core.message.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 钉钉机器人消息内容模型
 *
 * @author 马滨
 * @date 2022/09/25 16:29
 **/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class DingBotContentModel extends ContentModel implements Serializable {

    private static final long serialVersionUID = 4205208433388803645L;

    /**
     * 发送类型(例如: MessageTypeEnum.TEXT.getCode())
     */
    private String sendType;

    /**
     * 钉钉机器人：【文本消息】内容，【markdown消息】内容，【ActionCard消息】内容
     */
    private String content;

    /**
     * 钉钉机器人：【markdown消息】标题，【FeedCard消息】标题，【ActionCard消息】标题
     */
    private String title;

    /**
     * 钉钉机器人：【ActionCard消息】按钮布局
     */
    private String btnOrientation;

    /**
     * 钉钉机器人：【ActionCard消息】按钮的文案和跳转链接的json
     * [{\"title\":\"别点我\",\"actionURL\":\"https://www.baidu.com/\"},{\"title\":\"没关系，还是点我把\",\"actionURL\":\"https://www.baidu.com/\\t\"}]
     */
    private String btns;


    /**
     * 钉钉机器人：【链接消息】点击消息跳转的URL，【FeedCard消息】点击消息跳转的URL
     */
    private String url;

    /**
     * 钉钉机器人：【链接消息】图片URL，【FeedCard消息】图片URL
     */
    private String picUrl;


    /**
     * 钉钉机器人：【FeedCard类型】 消息体
     * "[{\"picUrl\":\"https://img.alicdn.com/tfs/TB1NwmBEL9TBuNjy1zbXXXpepXa-2400-1218.png\",\"title\":\"{$title1}\",\"url\":\"https://www.dingtalk.com/\"},{\"picUrl\":\"https://img.alicdn.com/tfs/TB1NwmBEL9TBuNjy1zbXXXpepXa-2400-1218.png\\t\",\"title\":\"时代的火车向前开2\",\"url\":\"https://www.dingtalk.com/\"}]"}
     */
    private String feedCards;

}
