package top.horsemuzi.core.message.common;

/**
 * 消息常量实例
 *
 * @author 马滨
 * @date 2022/09/25 17:22
 **/

public class MessageConstants {

    /**
     * 消息发送给全部人的标识
     * (钉钉自定义机器人)
     * (钉钉工作消息)
     */
    public static final String SEND_ALL = "@all";

    /**
     * 消息发送处理统一日志前缀
     */
    public static final String MSG_LOG_HEADER = "【消息系统】";

}
