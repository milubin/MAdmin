package top.horsemuzi.core.message.handle.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.teaopenapi.models.Config;
import lombok.extern.slf4j.Slf4j;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.ExceptionUtil;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.core.message.common.MessageConstants;
import top.horsemuzi.core.message.common.MessageInfo;
import top.horsemuzi.core.message.common.channel.AliyunSmsConfig;
import top.horsemuzi.core.message.common.model.AliyunSmsModel;
import top.horsemuzi.core.message.handle.BaseSendHandler;
import top.horsemuzi.core.message.handle.SendHandler;

import java.util.Map;

/**
 * 阿里云短信通知处理
 *
 * @author mabin
 * @date 2022/11/12 16:54
 **/

@Slf4j
@Component
public class AliyunSmsSendHandler extends BaseSendHandler implements SendHandler {


    /**
     * 发送消息
     *
     * @param messageInfo
     */
    @Retryable(value = {Exception.class}, maxAttempts = 3, backoff = @Backoff(delay = 2000, multiplier = 1.5D))
    @Override
    public boolean send(MessageInfo messageInfo) throws Exception {
        ValidatorUtil.notEmpty(messageInfo, ResultCodeEnum.MESSAGE_PARAMS_ERROR);
        AliyunSmsConfig aliyunSmsConfig = (AliyunSmsConfig) messageInfo.getChannelConfig();
        AliyunSmsModel aliyunSmsModel = (AliyunSmsModel) messageInfo.getContentModel();
        ValidatorUtil.allNotEmpty(ResultCodeEnum.MESSAGE_PARAMS_ERROR, aliyunSmsConfig, aliyunSmsModel);

        // 构建短信发送客户端
        Config config = new Config()
                .setAccessKeyId(aliyunSmsConfig.getAccessKeyId())
                .setAccessKeySecret(aliyunSmsConfig.getAccessKeySecret())
                .setEndpoint(aliyunSmsConfig.getEndpoint());
        Client client = new Client(config);

        // 组装参数
        String phones = aliyunSmsModel.getPhones();
        Map<String, String> paramsMap = aliyunSmsModel.getParamsMap();
        String templateId = aliyunSmsModel.getTemplateId();
        ValidatorUtil.allNotEmpty(ResultCodeEnum.MESSAGE_PARAMS_ERROR, phones, paramsMap, templateId);
        SendSmsRequest smsRequest = new SendSmsRequest()
                .setPhoneNumbers(phones)
                .setSignName(aliyunSmsConfig.getSignName())
                .setTemplateCode(templateId)
                .setTemplateCode(JsonUtil.toJson(paramsMap));

        // 发送短信
        SendSmsResponse smsResponse = client.sendSms(smsRequest);
        return ObjectUtil.isNotNull(smsResponse) && StrUtil.equals("OK", smsResponse.getBody().getCode());
    }

    @Recover
    public boolean recover(Exception e) {
        log.error("{} 阿里云短信发送处理异常: {}", MessageConstants.MSG_LOG_HEADER, ExceptionUtil.getMessage(e));
        return false;
    }

}
