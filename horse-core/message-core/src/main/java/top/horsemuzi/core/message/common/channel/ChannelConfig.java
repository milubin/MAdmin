package top.horsemuzi.core.message.common.channel;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;

/**
 * 渠道配置实例
 *
 * @author 马滨
 * @date 2022/09/25 17:11
 **/
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public class ChannelConfig implements Serializable {
    private static final long serialVersionUID = 6226417347683886374L;

}
