package top.horsemuzi.core.message.common.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;

/**
 * 消息内容模型实例
 *
 * @author 马滨
 * @date 2022/09/25 16:27
 **/
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public class ContentModel implements Serializable {
    private static final long serialVersionUID = -4467453925759893408L;

}
