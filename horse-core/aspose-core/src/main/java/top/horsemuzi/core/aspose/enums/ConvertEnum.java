package top.horsemuzi.core.aspose.enums;

import cn.hutool.extra.spring.SpringUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import top.horsemuzi.core.aspose.handle.ExcelConvert;
import top.horsemuzi.core.aspose.handle.PptConvert;
import top.horsemuzi.core.aspose.handle.WordConvert;
import top.horsemuzi.core.aspose.service.Convert;


/**
 * @author Mr.Horse
 * @version 1.0
 * @description {description}
 * @date 2022/5/22 21:40
 */
@AllArgsConstructor
@Getter
public enum ConvertEnum {

    /**
     * 这里的bean必须要从spring容器中获取, 不能直接使用new的方式获取对象, 否则其实现类中的@value注解将无法获取到值
     */
    WORD(SpringUtil.getBean(WordConvert.class)),

    PPT(SpringUtil.getBean(PptConvert.class)),

    EXCEL(SpringUtil.getBean(ExcelConvert.class));

    private Convert convert;

}
