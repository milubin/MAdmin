package top.horsemuzi.core.aspose.handle;

import com.aspose.cells.HtmlSaveOptions;
import com.aspose.cells.SaveFormat;
import com.aspose.cells.Workbook;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import top.horsemuzi.core.aspose.common.ConvertHelp;
import top.horsemuzi.core.aspose.service.Convert;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description Excel文档转PDF处理
 * @date 2022/5/22 11:28
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class ExcelConvert implements Convert {

    private final ConvertHelp convertHelp;


    /**
     * 全路径文件转换
     *
     * @param sourcePath 源文件全路径名
     * @param targetPath 目标文件全路径名
     */
    @Override
    public void convertPdf(String sourcePath, String targetPath) throws Exception {
        File sourceFile = new File(sourcePath);
        if (!sourceFile.isFile() || !sourceFile.exists()) {
            log.error("sourcePath excel file is not exist");
            return;
        }
        try (FileOutputStream outputStream = new FileOutputStream(new File(targetPath));
             FileInputStream inputStream = new FileInputStream(sourceFile)) {
            Workbook workbook = new Workbook(inputStream);
            workbook.save(outputStream, SaveFormat.PDF);
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    /**
     * 文件流转pdf
     *
     * @param inputStream 输入流
     * @return 转换为pdf后的临时文件的全路径
     */
    @Override
    public String convertPdf(InputStream inputStream) throws Exception {
        if (inputStream == null) {
            log.error("excel file inputStream is null");
            return null;
        }
        // 创建临时文件
        File file = convertHelp.getTempFile("pdf");
        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            Workbook workbook = new Workbook(inputStream);
            workbook.save(outputStream, SaveFormat.PDF);
        } catch (Exception e) {
            throw new Exception(e);
        }
        return file.getAbsolutePath();
    }

    /**
     * 文件转图片
     *
     * @param inputStream
     * @throws Exception
     */
    @Override
    public String convertImage(InputStream inputStream) throws Exception {
        log.info("Currently does not support excel to image");
        return null;
    }

    /**
     * 文件转HTML(excel)
     *
     * @param inputStream
     * @return
     * @throws Exception
     */
    @Override
    public String convertHtml(InputStream inputStream) throws Exception {
        if (inputStream == null) {
            log.error("excel file inputStream is null");
            return null;
        }
        File file = convertHelp.getTempFile("html");
        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            Workbook workbook = new Workbook(inputStream);
            HtmlSaveOptions options = new HtmlSaveOptions();
            options.setPresentationPreference(true);
            options.setAddTooltipText(true);
            options.setWidthScalable(true);
            workbook.save(outputStream, options);
        } catch (Exception e) {
            throw new Exception(e);
        }
        return file.getAbsolutePath();
    }

}
