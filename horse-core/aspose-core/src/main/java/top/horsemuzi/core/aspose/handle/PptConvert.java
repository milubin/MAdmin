package top.horsemuzi.core.aspose.handle;

import cn.hutool.core.collection.CollectionUtil;
import com.aspose.slides.ISlide;
import com.aspose.slides.ISlideCollection;
import com.aspose.slides.Presentation;
import com.aspose.slides.SaveFormat;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import top.horsemuzi.core.aspose.common.ConvertHelp;
import top.horsemuzi.core.aspose.service.Convert;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Dimension2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description PPT转PDF处理
 * @date 2022/5/22 11:28
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class PptConvert implements Convert {

    private final ConvertHelp convertHelp;


    /**
     * 全路径文件转换
     *
     * @param sourcePath 源文件全路径名
     * @param targetPath 目标文件全路径名
     */
    @Override
    public void convertPdf(String sourcePath, String targetPath) throws Exception {
        File sourceFile = new File(sourcePath);
        if (!sourceFile.isFile() || !sourceFile.exists()) {
            log.error("sourcePath ppt file is not exist");
            return;
        }
        try (FileOutputStream outputStream = new FileOutputStream(new File(targetPath));
             FileInputStream inputStream = new FileInputStream(sourceFile)) {
            Presentation presentation = new Presentation(inputStream);
            presentation.save(outputStream, SaveFormat.Pdf);
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    /**
     * 文件流转pdf
     *
     * @param inputStream 输入流
     * @return 转换为pdf后的临时文件的全路径
     */
    @Override
    public String convertPdf(InputStream inputStream) throws Exception {
        if (inputStream == null) {
            log.error("ppt file inputStream is null");
            return null;
        }
        // 创建临时文件
        File file = convertHelp.getTempFile("pdf");
        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            Presentation presentation = new Presentation(inputStream);
            presentation.save(outputStream, SaveFormat.Pdf);
        } catch (Exception e) {
            throw new Exception(e);
        }
        return file.getAbsolutePath();
    }

    /**
     * 文件转图片
     *
     * @param inputStream
     * @throws Exception
     */
    @Override
    public String convertImage(InputStream inputStream) throws Exception {
        if (Objects.isNull(inputStream)) {
            return null;
        }
        Presentation presentation = new Presentation(inputStream);
        ISlideCollection slides = presentation.getSlides();
        int size = slides.size();
        List<BufferedImage> bufferedImages = new LinkedList<>();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                ISlide slide = slides.get_Item(i);
                Dimension2D dimension2D = presentation.getSlideSize().getSize();
                Dimension dimension = new Dimension((int) dimension2D.getWidth() - 150, (int) dimension2D.getHeight() - 150);
                bufferedImages.add(slide.getThumbnail(dimension));
            }
        }
        if (CollectionUtil.isNotEmpty(bufferedImages)) {
            File file = convertHelp.getTempFile("png");
            BufferedImage mergeImage = convertHelp.mergeImage(false, bufferedImages);
            ImageIO.write(mergeImage, "png", file);
            return file.getAbsolutePath();
        }
        return null;
    }

    /**
     * 文件转HTML(ppt)
     *
     * @param inputStream
     * @return
     * @throws Exception
     */
    @Override
    public String convertHtml(InputStream inputStream) throws Exception {
        if (inputStream == null) {
            log.error("ppt file inputStream is null");
            return null;
        }
        File file = convertHelp.getTempFile("html");
        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            Presentation presentation = new Presentation(inputStream);
            presentation.save(outputStream, SaveFormat.Html5);
        } catch (Exception e) {
            throw new Exception(e);
        }
        return file.getAbsolutePath();
    }

}
