package top.horsemuzi.core.aspose.handle;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.aspose.words.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.core.aspose.common.ConvertHelp;
import top.horsemuzi.core.aspose.service.Convert;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description Word文档转PDF处理
 * @date 2022/5/22 11:28
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class WordConvert implements Convert {

    private final ConvertHelp convertHelp;


    /**
     * 全路径文件转换
     *
     * @param sourcePath 源文件全路径名
     * @param targetPath 目标文件全路径名
     */
    @Override
    public void convertPdf(@NonNull String sourcePath, @NonNull String targetPath) throws Exception {
        File sourceFile = new File(sourcePath);
        if (!sourceFile.isFile() || !sourceFile.exists()) {
            log.error("sourcePath word file is not exist");
            return;
        }
        try (FileOutputStream outputStream = new FileOutputStream(new File(targetPath));
             FileInputStream inputStream = new FileInputStream(sourceFile)) {
            Document document = new Document(inputStream);
            document.save(outputStream, SaveFormat.PDF);
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    /**
     * 文件流转pdf
     *
     * @param inputStream 输入流
     * @return 转换为pdf后的临时文件的全路径
     */
    @Override
    public String convertPdf(InputStream inputStream) throws Exception {
        if (inputStream == null) {
            log.error("word file inputStream is null");
            return null;
        }
        // 创建临时文件
        File file = convertHelp.getTempFile("pdf");
        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            Document document = new Document(inputStream);
            document.save(outputStream, SaveFormat.PDF);
        } catch (Exception e) {
            throw new Exception(e);
        }
        return file.getAbsolutePath();
    }

    /**
     * 文件转图片
     *
     * @param inputStream
     * @throws Exception
     */
    @Override
    public String convertImage(InputStream inputStream) throws Exception {
        if (inputStream == null) {
            return null;
        }
        Document document = new Document(inputStream);
        int pageCount = document.getPageCount();
        ImageSaveOptions options = new ImageSaveOptions(SaveFormat.PNG);
        options.setPrettyFormat(true);
        options.setUseAntiAliasing(true);
        options.setUseHighQualityRendering(true);
        List<BufferedImage> bufferedImages = new LinkedList<>();
        for (int i = 0; i < pageCount; i++) {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            options.setPageSet(new PageSet(i));
            document.save(outputStream, options);
            ImageInputStream imageInputStream = ImageIO.createImageInputStream(new ByteArrayInputStream(outputStream.toByteArray()));
            bufferedImages.add(ImageIO.read(imageInputStream));
        }
        if (CollectionUtil.isNotEmpty(bufferedImages)) {
            File file = convertHelp.getTempFile("png");
            BufferedImage mergeImage = convertHelp.mergeImage(false, bufferedImages);
            ImageIO.write(mergeImage, "png", file);
            return file.getAbsolutePath();
        }
        return null;
    }

    /**
     * 文件转HTML(word)
     *
     * @param inputStream
     * @return
     * @throws Exception
     */
    @Override
    public String convertHtml(InputStream inputStream) throws Exception {
        if (inputStream == null) {
            log.error("word file inputStream is null");
            return null;
        }
        File file = convertHelp.getTempFile("html");
        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            Document document = new Document(inputStream);
            HtmlSaveOptions options = new HtmlSaveOptions(SaveFormat.HTML);
            options.setHtmlVersion(HtmlVersion.HTML_5);
            options.setPrettyFormat(true);
            options.setExportImagesAsBase64(true);
            options.setExportPageMargins(true);
            document.save(outputStream, options);
        } catch (Exception e) {
            throw new Exception(e);
        }
        return file.getAbsolutePath();
    }

}
