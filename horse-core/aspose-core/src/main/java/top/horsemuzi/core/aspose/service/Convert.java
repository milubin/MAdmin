package top.horsemuzi.core.aspose.service;

import java.io.InputStream;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description {description}
 * @date 2022/5/22 11:27
 */
public interface Convert {

    /**
     * 全路径文件转换
     *
     * @param sourcePath 源文件全路径名
     * @param targetPath 目标文件全路径名
     * @throws Exception
     */
    void convertPdf(String sourcePath, String targetPath) throws Exception;

    /**
     * 文件流转pdf
     *
     * @param inputStream 输入流
     * @return 转换为pdf后的临时文件的全路径(建议 : 后期使用完后删除此临时文件)
     * @throws Exception
     */
    String convertPdf(InputStream inputStream) throws Exception;

    /**
     * 文件转图片(word、ppt)
     *
     * @param inputStream
     * @return
     * @throws Exception
     */
    String convertImage(InputStream inputStream) throws Exception;

    /**
     * 文件转HTML(excel)
     *
     * @param inputStream
     * @return
     * @throws Exception
     */
    String convertHtml(InputStream inputStream) throws Exception;


}
