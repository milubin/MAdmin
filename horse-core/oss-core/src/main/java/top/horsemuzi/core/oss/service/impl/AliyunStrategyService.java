package top.horsemuzi.core.oss.service.impl;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.MD5;
import cn.hutool.http.Header;
import com.aliyun.oss.*;
import com.aliyun.oss.model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.core.oss.properties.ConfigProperties;
import top.horsemuzi.core.oss.service.StrategyAbstract;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.CountDownLatch;

/**
 * aliyun oss 服务实现
 *
 * @author mabin
 * @date 2022/05/31 09:09
 **/

@Slf4j
@Service
public class AliyunStrategyService extends StrategyAbstract {

    private OSS client;
    private String searchStr;

    /**
     * 构造函数初始化客户端
     *
     * @param properties
     * @return null
     * @author mabin
     * @date 2023/3/27 11:09
     **/
    @Override
    public void init(ConfigProperties properties) {
        super.init(properties);
        ClientBuilderConfiguration conf = new ClientBuilderConfiguration();
        // 连接空闲超时时间，超时则关闭
        conf.setIdleConnectionTime(1000);
        // 连接超时，默认15秒
        conf.setConnectionTimeout(15000);
        // socket超时，默认15秒
        conf.setSocketTimeout(15000);
        // 失败后最大重试次数
        conf.setMaxErrorRetry(2);
        client = new OSSClientBuilder().build(properties.getEndpoint(), properties.getAccessKey(), properties.getSecretKey(), conf);
        searchStr = HTTPS + properties.getBucketName() + StrUtil.DOT + properties.getEndpoint() + StrUtil.SLASH;
    }

    /**
     * 文件流上传
     *
     * @param inputStream 二进制文件流
     * @param fileName    文件名(含后缀)
     * @param isRename    是否重命名(规则: yyyyMM/10位随机字符.suffix)
     * @param isHighSpeed 是否高速上传(暂支持阿里云OSS, 腾讯云COS)
     * @return java.lang.String
     * @throws Exception
     * @author mabin
     * @date 2023/3/16 9:53
     **/
    @Override
    public String upload(InputStream inputStream, String fileName, Boolean isRename, Boolean isHighSpeed) throws Exception {
        if (ObjectUtil.isNull(inputStream) || StrUtil.isBlank(fileName)) {
            log.error("oss-core-aliyun: required parameter is null!");
            return null;
        }
        if (isRename) {
            fileName = IdUtil.nanoId(10) + StrUtil.DOT + FileUtil.extName(fileName);
        }
        if (ObjectUtil.isNull(isHighSpeed) || !isHighSpeed) {
            // 小文件普通上传
            try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
                // 数据安全处理(MD5)
                ObjectMetadata metadata = new ObjectMetadata();
                IoUtil.copy(inputStream, outputStream);
                byte[] bytes = outputStream.toByteArray();
                metadata.setContentMD5(Base64.encode(MD5.create().digest(bytes)));
                // 文件存储位置(默认:根据年月分类) & 重命名处理(默认: nanoId)
                String filePath = DateUtil.format(new Date(), StrategyAbstract.SIMPLE_DATE_FORMAT) + StrUtil.SLASH + fileName;
                PutObjectResult putObject = client.putObject(properties.getBucketName(), filePath, new ByteArrayInputStream(bytes), metadata);
                log.info("oss-core-aliyun: aliyun oss upload file response: {}", JsonUtil.toPrettyJson(putObject));
                String url = searchStr + filePath;
                log.info("oss-core-aliyun: aliyun oss upload current file access straight chain url: {}", url);
                return url;
            } catch (OSSException oe) {
                log.error("oss-core-aliyun: aliyun oss upload file fail!", oe);
                log.error("Caught an OSSException, which means your request made it to OSS, "
                        + "but was rejected with an error response for some reason.");
                log.error("Error Message: {}", oe.getErrorMessage());
                log.error("Error Code: {}", oe.getErrorCode());
                log.error("Request ID: {}", oe.getRequestId());
                log.error("Host ID: {}", oe.getHostId());
                throw oe;
            } catch (ClientException ce) {
                log.error("oss-core-aliyun: aliyun oss upload file fail!", ce);
                log.error("Caught an ClientException, which means the client encountered "
                        + "a serious internal problem while trying to communicate with OSS, "
                        + "such as not being able to access the network.");
                log.error("Error Message: {}", ce.getMessage());
                throw ce;
            } catch (Exception e) {
                log.error("oss-core-aliyun: aliyun oss upload file fail!", e);
                throw e;
            } finally {
                if (Objects.nonNull(client)) {
                    client.shutdown();
                }
                IoUtil.close(inputStream);
            }
        } else {
            // 大文件分片多线程高速上传
            // 文件路径名
            String filePath = null;
            // 分片上传全局uploadId
            String uploadId = null;
            try {
                filePath = DateUtil.format(new Date(), StrategyAbstract.SIMPLE_DATE_FORMAT) + StrUtil.SLASH + fileName;
                // 初始化InitiateMultipartUploadRequest对象
                InitiateMultipartUploadRequest multipartUploadRequest = new InitiateMultipartUploadRequest(properties.getBucketName(), filePath);
                // 设置上传请求头
                ObjectMetadata metadata = new ObjectMetadata();
                // 设置网页缓存行为
                metadata.setCacheControl("no-cache");
                // 初始化分片上传时是否覆盖同名分片, 设置为 true: 不覆盖, false: 覆盖
                metadata.setHeader("x-oss-forbid-overwrite", "false");
                // 设置被下载时的内容编码格式
                metadata.setContentEncoding(StandardCharsets.UTF_8.toString());
                // 设置文件类型
                metadata.setContentType(getContentType(FileUtil.extName(fileName)));
                multipartUploadRequest.setObjectMetadata(metadata);
                // 初始化分片
                InitiateMultipartUploadResult request = client.initiateMultipartUpload(multipartUploadRequest);
                // 返回uploadId，它是分片上传事件的唯一标识。您可以根据该uploadId发起相关的操作，例如取消分片上传、查询分片上传等
                uploadId = request.getUploadId();
                log.info("oss-core-aliyun: aliyun oss upload part global uploadId: {}", uploadId);
                // 分片大小, 默认: 1MB
                final long partSize = 2 << 19;
                // 文件流大小
                int fileLength = inputStream.available();
                // 计算分片数量
                int partCount = (int) (fileLength / partSize);
                if (fileLength % partSize != 0) {
                    partCount++;
                }
                log.info("oss-core-aliyun: aliyun oss upload split file total {} part", partCount);
                // 分片数最大限制100 (文件最大100MB)
                if (partCount >= Constants.COMMON_CODE.ONE_HUNDRED) {
                    log.error("oss-core-aliyun: aliyun oss upload file part limit error");
                    throw new BusinessException(ResultCodeEnum.OSS_PART_LIMIT_ERROR);
                }

                CountDownLatch countDownLatch = new CountDownLatch(partCount);
                // 分片上传
                List<PartETag> partETags = Collections.synchronizedList(new ArrayList<>());
                for (int i = 0; i < partCount; i++) {
                    long startPos = i * partSize;
                    long curPartSize = (i + 1 == partCount) ? (fileLength - startPos) : partSize;
                    // 跳过已上传分片
                    inputStream.skip(startPos);
                    UploadPartRequest uploadPartRequest = new UploadPartRequest();
                    uploadPartRequest.setBucketName(properties.getBucketName());
                    uploadPartRequest.setKey(filePath);
                    uploadPartRequest.setUploadId(uploadId);
                    uploadPartRequest.setInputStream(inputStream);
                    // 设置分片大小。除了最后一个分片没有大小限制，其他的分片最小为100 KB。
                    uploadPartRequest.setPartSize(curPartSize);
                    // 设置分片号。每一个上传的分片都有一个分片号，取值范围是1~10000，如果超出此范围，OSS将返回InvalidArgument错误码。
                    uploadPartRequest.setPartNumber(i + 1);

                    // 多线程上传处理
                    threadPoolTaskExecutor.execute(() -> {
                        // 每个分片不需要按顺序上传，甚至可以在不同客户端上传，OSS会按照分片号排序组成完整的文件。
                        UploadPartResult uploadPartResult = client.uploadPart(uploadPartRequest);
                        // 每次上传分片之后, OSS的返回结果包含PartETag, PartETag将被保存在partETags中、
                        partETags.add(uploadPartResult.getPartETag());
                        log.info("oss-core-aliyun: aliyun oss upload file {} part success", uploadPartRequest.getPartNumber());
                        countDownLatch.countDown();
                    });
                }
                // 保证所有分片上传成功后, 继续执行后续操作
                countDownLatch.await();
                // 分片上传验证
                if (partETags.size() != partCount) {
                    log.error("oss-core-aliyun: aliyun oss upload part fail, partETags.size: {}, partCount: {}", partETags.size(), partCount);
                    throw new BusinessException(ResultCodeEnum.OSS_SERVICE_UPLOAD_ERROR);
                }
                // partETags必须按分片号升序
                CollectionUtil.sort(partETags, Comparator.comparing(PartETag::getPartNumber));
                log.info("oss-core-aliyun: aliyun oss upload part total etag: {}", JsonUtil.toJson(partETags));
                // 创建CompleteMultipartUploadRequest对象
                // 在执行完成分片上传操作时, 需要提供所有有效的partETags, OSS收到提交的partETags后，会逐一验证每个分片的有效性
                // 当所有的数据分片验证通过后, OSS将把这些分片组合成一个完整的文件
                CompleteMultipartUploadRequest completeMultipartUploadRequest = new CompleteMultipartUploadRequest(properties.getBucketName(), filePath, uploadId, partETags);
                // 在完成文件上传的同时设置文件访问权限 (继承Bucket权限)
                completeMultipartUploadRequest.setObjectACL(CannedAccessControlList.Default);
                // 完成分片上传
                CompleteMultipartUploadResult completeMultipartUploadResult = client.completeMultipartUpload(completeMultipartUploadRequest);
                log.info("oss-core-aliyun: aliyun oss upload part etag: {}", completeMultipartUploadResult.getETag());
                // 组装访问链接
                String url = searchStr + filePath;
                log.info("oss-core-aliyun: aliyun oss upload part current file access straight chain url: {}", url);
                return url;
            } catch (OSSException oe) {
                log.error("oss-core-aliyun: aliyun oss upload file fail!", oe);
                log.error("Caught an OSSException, which means your request made it to OSS, "
                        + "but was rejected with an error response for some reason.");
                log.error("Error Message: {}", oe.getErrorMessage());
                log.error("Error Code: {}", oe.getErrorCode());
                log.error("Request ID: {}", oe.getRequestId());
                log.error("Host ID: {}", oe.getHostId());
                // 分片上传异常, 手动删除已上传的分片数据
                abortMultipartUpload(client, filePath, uploadId);
                throw oe;
            } catch (ClientException ce) {
                log.error("oss-core-aliyun: aliyun oss upload file fail!", ce);
                log.error("Caught an ClientException, which means the client encountered "
                        + "a serious internal problem while trying to communicate with OSS, "
                        + "such as not being able to access the network.");
                log.error("Error Message: {}", ce.getMessage());
                // 分片上传异常, 手动删除已上传的分片数据
                abortMultipartUpload(client, filePath, uploadId);
                throw ce;
            } catch (Exception e) {
                log.error("oss-core-aliyun: aliyun oss upload file fail!", e);
                // 分片上传异常, 手动删除已上传的分片数据
                abortMultipartUpload(client, filePath, uploadId);
                throw e;
            } finally {
                if (Objects.nonNull(client)) {
                    client.shutdown();
                }
                IoUtil.close(inputStream);
            }
        }
    }


    /**
     * 文件下载
     *
     * @param response
     */
    @Override
    public void download(HttpServletResponse response, String url, String fileName) throws Exception {
        if (StrUtil.isBlank(url)) {
            log.error("oss-core-aliyun: required parameter is null!");
            return;
        }
        OSSObject object = null;
        InputStream inputStream = null;
        String filePath = null;
        try {
            filePath = StrUtil.replace(url, searchStr, StrUtil.EMPTY);
            object = client.getObject(properties.getBucketName(), filePath);
            if (ObjectUtil.isNull(object)) {
                log.error("oss-core-aliyun: aliyun oss get file resource error!");
                return;
            }
            inputStream = object.getObjectContent();
            if (ObjectUtil.isNull(inputStream)) {
                log.error("oss-core-aliyun: aliyun oss get file: {} resource error!", filePath);
                return;
            }
            // 文件名处理(如果文件名为空,则设置默认文件名)
            if (StrUtil.isBlank(fileName)) {
                fileName = FileUtil.getName(filePath);
            }
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setHeader(Header.CONTENT_DISPOSITION.getValue(), "attachment; filename=" + encodeDownloadFilename(fileName));
            IoUtil.copy(inputStream, response.getOutputStream());
        } catch (OSSException oe) {
            log.error("oss-core-aliyun: download file: {} fail!", filePath, oe);
            log.error("Caught an OSSException, which means your request made it to OSS, "
                    + "but was rejected with an error response for some reason.");
            log.error("Error Message: {}", oe.getErrorMessage());
            log.error("Error Code: {}", oe.getErrorCode());
            log.error("Request ID: {}", oe.getRequestId());
            log.error("Host ID: {}", oe.getHostId());
            throw oe;
        } catch (ClientException ce) {
            log.error("oss-core-aliyun: download file: {} fail!", filePath, ce);
            log.error("Caught an ClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with OSS, "
                    + "such as not being able to access the network.");
            log.error("Error Message: {}", ce.getMessage());
            throw ce;
        } catch (Exception e) {
            log.error("oss-core-aliyun: download file: {} fail!", filePath, e);
            throw e;
        } finally {
            if (Objects.nonNull(client)) {
                client.shutdown();
            }
            if (Objects.nonNull(object)) {
                object.close();
            }
            IoUtil.close(inputStream);
        }
    }


    /**
     * 文件删除
     */
    @Override
    public void delete(String url, String sha) {
        if (StrUtil.isBlank(url)) {
            log.error("oss-core-aliyun: required parameter is null!");
            return;
        }
        if (StrUtil.isNotBlank(sha)) {
            log.warn("oss-core-aliyun: the current method is only compatible with github!");
            return;
        }
        String filePath = null;
        try {
            filePath = StrUtil.replace(url, searchStr, StrUtil.EMPTY);
            client.deleteObject(properties.getBucketName(), filePath);
            log.info("oss-core-aliyun: delete file: {} success!", filePath);
        } catch (OSSException oe) {
            log.error("oss-core-aliyun: delete file fail!", oe);
            log.error("Caught an OSSException, which means your request made it to OSS, "
                    + "but was rejected with an error response for some reason.");
            log.error("Error Message: {}", oe.getErrorMessage());
            log.error("Error Code: {}", oe.getErrorCode());
            log.error("Request ID: {}", oe.getRequestId());
            log.error("Host ID: {}", oe.getHostId());
            throw oe;
        } catch (ClientException ce) {
            log.error("oss-core-aliyun: delete file: {} fail!", filePath, ce);
            log.error("Caught an ClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with OSS, "
                    + "such as not being able to access the network.");
            log.error("Error Message: {}", ce.getMessage());
            throw ce;
        } catch (Exception e) {
            log.error("oss-core-aliyun: delete file: {} fail!", filePath, e);
            throw e;
        } finally {
            if (Objects.nonNull(client)) {
                client.shutdown();
            }
        }
    }


    /**
     * 删除分片数据
     *
     * @param client
     * @param filePath
     * @param uploadId
     */
    private void abortMultipartUpload(OSS client, String filePath, String uploadId) {
        if (StrUtil.hasBlank(filePath, uploadId) || ObjectUtil.isNull(client)) {
            return;
        }
        client.abortMultipartUpload(new AbortMultipartUploadRequest(properties.getBucketName(), filePath, uploadId));
    }

}
