package top.horsemuzi.core.oss.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.Header;
import io.minio.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.core.oss.properties.ConfigProperties;
import top.horsemuzi.core.oss.service.StrategyAbstract;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.Date;

/**
 * minio分布式存储服务
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/03/25 09:56
 **/
@Slf4j
@Service
public class MinioStrategyService extends StrategyAbstract {

    private MinioClient client;
    private String searchStr;

    /**
     * 初始化OSS配置属性
     *
     * @param properties
     * @return void
     * @author mabin
     * @date 2023/3/27 11:22
     **/
    @Override
    public void init(ConfigProperties properties) {
        super.init(properties);
        searchStr = properties.getDomain() + properties.getBucketName() + StrUtil.SLASH;
        client = MinioClient.builder()
                .endpoint(properties.getDomain())
                .credentials(properties.getAccessKey(), properties.getSecretKey())
                .build();
    }

    /**
     * 文件流上传
     *
     * @param inputStream 二进制文件流
     * @param fileName    文件名(含后缀)
     * @param isRename    是否重命名(规则: yyyyMM/10位随机字符.suffix)
     * @param isHighSpeed 是否高速上传(暂支持阿里云OSS, 腾讯云COS)
     * @return java.lang.String
     * @throws Exception
     * @author mabin
     * @date 2023/3/16 9:53
     **/
    @Override
    public String upload(InputStream inputStream, String fileName, Boolean isRename, Boolean isHighSpeed) throws Exception {
        if (ObjectUtil.isNull(inputStream) || StrUtil.isBlank(fileName)) {
            log.error("oss-core-minio: required parameter is null!");
            return null;
        }
        String suffixName = FileUtil.extName(fileName);
        if (isRename) {
            fileName = IdUtil.nanoId(10) + StrUtil.DOT + suffixName;
        }
        if (ObjectUtil.isNotNull(isHighSpeed) && isHighSpeed) {
            log.error("oss-core-minio: partitioning upload is not supported temporarily!");
            return null;
        }
        // 文件存储位置(默认:根据年月分类) & 重命名处理(默认: nanoId)
        String filePath = DateUtil.format(new Date(), StrategyAbstract.SIMPLE_DATE_FORMAT) + StrUtil.SLASH + fileName;
        try {
            // 文件流形式上传
            ObjectWriteResponse response = client.putObject(PutObjectArgs.builder()
                    .contentType(StrategyAbstract.getContentType(suffixName))
                    .bucket(properties.getBucketName())
                    .object(filePath)
                    .stream(inputStream, inputStream.available(), -1)
                    .build());
            log.info("oss-core-minio: minio upload file response: {}", JsonUtil.toPrettyJson(response));
            String url = searchStr + filePath;
            log.info("oss-core-minio: minio upload current file access straight chain url: {}", url);
            return url;
        } catch (Exception e) {
            log.error("oss-core-minio: minio upload file: {} fail!", filePath, e);
            throw e;
        } finally {
            IoUtil.close(inputStream);
        }
    }

    /**
     * 文件下载
     *
     * @param response
     * @param url
     * @param fileName 下载的文件名称(为空则默认截取url文件名)
     * @return void
     * @throws Exception
     * @author mabin
     * @date 2023/3/16 9:54
     **/
    @Override
    public void download(HttpServletResponse response, String url, String fileName) throws Exception {
        if (StrUtil.isBlank(url)) {
            log.error("oss-core-minio: required parameter is null!");
            return;
        }
        InputStream inputStream = null;
        String filePath = null;
        try {
            filePath = StrUtil.replace(url, searchStr, StrUtil.EMPTY);
            StatObjectResponse statObject = client.statObject(StatObjectArgs.builder()
                    .bucket(properties.getBucketName()).object(filePath).build());
            if (statObject.size() <= 0) {
                log.error("oss-core-minio: minio get file resource error!");
                return;
            }
            inputStream = client.getObject(GetObjectArgs.builder().bucket(properties.getBucketName())
                    .object(filePath).build());
            if (ObjectUtil.isNull(inputStream)) {
                log.error("oss-core-minio: minio get file: {} resource error!", filePath);
                return;
            }
            // 文件名处理(如果文件名为空,则设置默认文件名)
            if (StrUtil.isBlank(fileName)) {
                fileName = FileUtil.getName(filePath);
            }
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setHeader(Header.CONTENT_DISPOSITION.getValue(), "attachment; filename=" + encodeDownloadFilename(fileName));
            IoUtil.copy(inputStream, response.getOutputStream());
        } catch (Exception e) {
            log.error("oss-core-minio: download file: {} fail!", filePath, e);
            throw e;
        } finally {
            IoUtil.close(inputStream);
        }
    }

    /**
     * 文件删除
     *
     * @param url
     * @param sha 文件唯一标识(兼容github)
     * @return void
     * @throws Exception
     * @author mabin
     * @date 2023/3/16 9:55
     **/
    @Override
    public void delete(String url, String sha) throws Exception {
        if (StrUtil.isBlank(url)) {
            log.error("oss-core-minio: required parameter is null!");
            return;
        }
        if (StrUtil.isNotBlank(sha)) {
            log.warn("oss-core-minio: the current method is only compatible with github!");
            return;
        }
        String filePath = null;
        try {
            filePath = StrUtil.replace(url, searchStr, StrUtil.EMPTY);
            client.removeObject(RemoveObjectArgs.builder().bucket(properties.getBucketName()).object(filePath).build());
            log.info("oss-core-minio: delete file: {} success!", filePath);
        } catch (Exception e) {
            log.error("oss-core-minio: delete file: {} fail!", filePath, e);
            throw e;
        }
    }

}
