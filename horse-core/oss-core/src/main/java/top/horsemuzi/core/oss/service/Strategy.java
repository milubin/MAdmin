package top.horsemuzi.core.oss.service;

import org.springframework.lang.Nullable;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description 对象存储基础接口
 * @date 2022/6/28 20:49
 */
public interface Strategy {

    /**
     * 文件流上传
     *
     * @param inputStream 二进制文件流
     * @param fileName    文件名(含后缀)
     * @param isRename    是否重命名(规则: yyyyMM/10位随机字符.suffix)
     * @param isHighSpeed 是否高速上传(暂支持阿里云OSS, 腾讯云COS)
     * @return java.lang.String
     * @throws Exception
     * @author mabin
     * @date 2023/3/16 9:53
     **/
    String upload(InputStream inputStream, String fileName, Boolean isRename, @Nullable Boolean isHighSpeed) throws Exception;

    /**
     * 文件下载
     *
     * @param response
     * @param url
     * @param fileName 下载的文件名称(为空则默认截取url文件名)
     * @return void
     * @throws Exception
     * @author mabin
     * @date 2023/3/16 9:54
     **/
    void download(HttpServletResponse response, String url, String fileName) throws Exception;

    /**
     * 文件删除
     *
     * @param url
     * @param sha 文件唯一标识(兼容github)
     * @return void
     * @throws Exception
     * @author mabin
     * @date 2023/3/16 9:55
     **/
    void delete(String url, @Nullable String sha) throws Exception;

}
