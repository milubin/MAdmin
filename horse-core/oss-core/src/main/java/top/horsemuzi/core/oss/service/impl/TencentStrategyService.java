package top.horsemuzi.core.oss.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.Header;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.http.HttpProtocol;
import com.qcloud.cos.model.*;
import com.qcloud.cos.region.Region;
import com.qcloud.cos.transfer.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.core.oss.properties.ConfigProperties;
import top.horsemuzi.core.oss.service.StrategyAbstract;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.Date;
import java.util.Objects;

/**
 * 腾讯云COS服务
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/03/20 13:59
 **/
@Slf4j
@Service
public class TencentStrategyService extends StrategyAbstract {

    private COSClient client;
    private String searchStr;

    /**
     * 初始化OSS配置属性
     *
     * @param properties
     * @return void
     * @author mabin
     * @date 2023/3/27 11:22
     **/
    @Override
    public void init(ConfigProperties properties) {
        super.init(properties);
        searchStr = HTTPS + properties.getBucketName() + ".cos." + properties.getRegion() + ".myqcloud.com/";
        ClientConfig clientConfig = new ClientConfig(new Region(properties.getRegion()));
        // 5.6.54 版本开始，默认使用https
        clientConfig.setHttpProtocol(StrUtil.equals(Constants.COMMON_CODE.STR_ZONE, properties.getHttps()) ? HttpProtocol.https : HttpProtocol.http);
        // 请求签名的有效时间，单位：秒，默认为3600s
        clientConfig.setSignExpired(3600);
        // 连接 COS 服务的超时时间，单位：毫秒，默认为30000ms
        clientConfig.setConnectionTimeout(30000);
        // 客户端读取数据的超时时间，单位：毫秒，默认为30000ms
        clientConfig.setSocketTimeout(30000);
        // 重试策略: 失败后重试次数(默认重试3次)
        clientConfig.setMaxErrorRetry(2);
        client = new COSClient(new BasicCOSCredentials(properties.getAccessKey(), properties.getSecretKey()), clientConfig);
    }

    /**
     * 文件流上传
     *
     * @param inputStream 二进制文件流
     * @param fileName    文件名(含后缀)
     * @param isRename    是否重命名(规则: yyyyMM/10位随机字符.suffix)
     * @param isHighSpeed 是否高速上传(暂支持阿里云OSS, 腾讯云COS)
     * @return java.lang.String
     * @throws Exception
     * @author mabin
     * @date 2023/3/16 9:53
     **/
    @Override
    public String upload(InputStream inputStream, String fileName, Boolean isRename, Boolean isHighSpeed) throws Exception {
        if (ObjectUtil.isNull(inputStream) || StrUtil.isBlank(fileName)) {
            log.error("oss-core-tencent: required parameter is null!");
            return null;
        }
        // 文件存储位置(默认:根据年月分类) & 重命名处理(默认: nanoId)
        if (isRename) {
            fileName = IdUtil.nanoId(10) + StrUtil.DOT + FileUtil.extName(fileName);
        }
        String filePath = DateUtil.format(new Date(), StrategyAbstract.SIMPLE_DATE_FORMAT) + StrUtil.SLASH + fileName;
        if (ObjectUtil.isNull(isHighSpeed) || !isHighSpeed) {
            // 小文件普通上传
            try {
                ObjectMetadata metadata = new ObjectMetadata();
                metadata.setContentLength(inputStream.available());
                PutObjectRequest putObjectRequest = new PutObjectRequest(properties.getBucketName(), filePath, inputStream, metadata);
                // 设置存储类型: 低频
                putObjectRequest.setStorageClass(StorageClass.Standard_IA);
                PutObjectResult putObjectResult = client.putObject(putObjectRequest);
                log.info("oss-core-tencent: tencent cos upload file response: {}", JsonUtil.toPrettyJson(putObjectResult));
                String url = searchStr + filePath;
                log.info("oss-core-tencent: tencent cos upload current file access straight chain url: {}", url);
                return url;
            } catch (Exception e) {
                log.error("oss-core-tencent: tencent cos upload file fail!", e);
                throw e;
            } finally {
                client.shutdown();
                IoUtil.close(inputStream);
            }
        } else {
            // 大文件高速上传
            TransferManager transferManager = null;
            try {
                transferManager = new TransferManager(client, StrategyAbstract.threadPoolTaskExecutor);
                TransferManagerConfiguration configuration = new TransferManagerConfiguration();
                configuration.setMultipartUploadThreshold(5 * 1024 * 1024);
                configuration.setMinimumUploadPartSize(1024 * 1024);
                transferManager.setConfiguration(configuration);

                ObjectMetadata metadata = new ObjectMetadata();
                metadata.setContentLength(inputStream.available());
                PutObjectRequest putObjectRequest = new PutObjectRequest(properties.getBucketName(), filePath, inputStream, metadata);
                putObjectRequest.setStorageClass(StorageClass.Standard_IA);
                Upload upload = transferManager.upload(putObjectRequest);
                // 打印上传进度
                showTransferProgress(upload);
                // 可同步地调用 waitForUploadResult 方法等待上传完成，成功返回 UploadResult, 失败抛出异常
                UploadResult uploadResult = upload.waitForUploadResult();
                log.info("oss-core-tencent: tencent cos upload file response: {}", JsonUtil.toPrettyJson(uploadResult));
                String url = searchStr + filePath;
                log.info("oss-core-tencent: tencent cos upload current file access straight chain url: {}", url);
                return url;
            } catch (Exception e) {
                log.error("oss-core-tencent: tencent cos upload file fail!", e);
                throw e;
            } finally {
                if (Objects.nonNull(transferManager)) {
                    transferManager.shutdownNow(true);
                }
                IoUtil.close(inputStream);
            }
        }
    }

    /**
     * 显示上传进度
     *
     * @param transfer
     * @return void
     * @author mabin
     * @date 2023/3/20 14:49
     **/
    void showTransferProgress(Transfer transfer) {
        while (!transfer.isDone()) {
            try {
                // 每 0.5 秒获取一次进度
                Thread.sleep(500);
            } catch (InterruptedException e) {
                return;
            }
            TransferProgress progress = transfer.getProgress();
            log.info("oss-core-tencent: upload progress: [{}/{}] = {}", progress.getBytesTransferred(),
                    progress.getTotalBytesToTransfer(), progress.getPercentTransferred());
        }
        // 完成了 Completed，或者失败了 Failed
        log.info("oss-core-tencent: upload progress state: {}", transfer.getState());
    }

    /**
     * 文件下载
     *
     * @param response
     * @param url
     * @param fileName 下载的文件名称(为空则默认截取url文件名)
     * @return void
     * @throws Exception
     * @author mabin
     * @date 2023/3/16 9:54
     **/
    @Override
    public void download(HttpServletResponse response, String url, String fileName) throws Exception {
        if (StrUtil.isBlank(url)) {
            log.error("oss-core-tencent: required parameter is null!");
            return;
        }
        InputStream inputStream = null;
        COSObject object = null;
        String filePath = null;
        try {
            filePath = StrUtil.replace(url, searchStr, StrUtil.EMPTY);
            object = client.getObject(properties.getBucketName(), filePath);
            if (ObjectUtil.isNull(object)) {
                log.error("oss-core-tencent: tencent cos get file: {} resource error!", filePath);
                return;
            }
            inputStream = object.getObjectContent();
            // 文件名处理(如果文件名为空,则设置默认文件名)
            if (StrUtil.isBlank(fileName)) {
                fileName = FileUtil.getName(filePath);
            }
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setHeader(Header.CONTENT_DISPOSITION.getValue(), "attachment; filename=" + encodeDownloadFilename(fileName));
            IoUtil.copy(inputStream, response.getOutputStream());
        } catch (Exception e) {
            log.error("oss-core-tencent: download file: {} fail!", filePath, e);
            throw e;
        } finally {
            if (Objects.nonNull(object)) {
                object.close();
            }
            client.shutdown();
            IoUtil.close(inputStream);
        }
    }

    /**
     * 文件删除
     *
     * @param url
     * @param sha 文件唯一标识(兼容github)
     * @return void
     * @throws Exception
     * @author mabin
     * @date 2023/3/16 9:55
     **/
    @Override
    public void delete(String url, String sha) throws Exception {
        if (StrUtil.isBlank(url)) {
            log.error("oss-core-tencent: required parameter is null!");
            return;
        }
        if (StrUtil.isNotBlank(sha)) {
            log.warn("oss-core-tencent: the current method is only compatible with github!");
            return;
        }
        String filePath = null;
        try {
            filePath = StrUtil.replace(url, searchStr, StrUtil.EMPTY);
            client.deleteObject(properties.getBucketName(), filePath);
            log.info("oss-core-tencent: delete file: {} success!", filePath);
        } catch (Exception e) {
            log.error("oss-core-tencent: delete file: {} fail!", filePath, e);
            throw e;
        } finally {
            client.shutdown();
        }
    }

}
