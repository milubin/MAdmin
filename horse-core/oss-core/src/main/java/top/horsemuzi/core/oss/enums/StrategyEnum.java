package top.horsemuzi.core.oss.enums;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.core.oss.service.StrategyAbstract;
import top.horsemuzi.core.oss.service.impl.*;

/**
 * 对象存储枚举处理
 *
 * @author mabin
 * @date 2022/06/01 09:29
 **/
@Getter
@ToString
@AllArgsConstructor
public enum StrategyEnum {

    /**
     * 阿里云
     */
    ALIYUN("ALIYUN", new AliyunStrategyService()),

    /**
     * 七牛云
     */
    QINIU("QINIU", new QiniuStrategyService()),

    /**
     * Github存储
     */
    GITHUB("GITHUB", new GithubStrategyService()),

    /**
     * 腾讯云
     */
    TENCENT("TENCENT", new TencentStrategyService()),

    /**
     * 又拍云
     */
    UPYUN("UPYUN", new UpYunStrategyService()),

    /**
     * MinIO分布式存储
     */
    MINIO("MINIO", new MinioStrategyService());

    private String configKey;
    private StrategyAbstract storageAbstract;

    /**
     * 获取对象存储实例
     *
     * @param configKey
     * @return top.horsemuzi.core.oss.service.StorageAbstract
     * @author mabin
     * @date 2023/3/25 11:38
     **/
    public static StrategyAbstract getStrategy(String configKey) {
        for (StrategyEnum value : values()) {
            if (StrUtil.equals(configKey, value.configKey)) {
                return value.storageAbstract;
            }
        }
        throw new BusinessException(ResultCodeEnum.OSS_STORAGE_NOT_EXIST);
    }

}
