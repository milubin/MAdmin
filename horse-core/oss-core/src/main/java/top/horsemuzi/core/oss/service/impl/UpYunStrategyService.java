package top.horsemuzi.core.oss.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.Header;
import com.upyun.RestManager;
import com.upyun.UpYunUtils;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Response;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.core.oss.properties.ConfigProperties;
import top.horsemuzi.core.oss.service.StrategyAbstract;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;

/**
 * 又拍云云存储服务
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/03/23 17:08
 **/
@Slf4j
@Service
public class UpYunStrategyService extends StrategyAbstract {

    private String searchStr;
    private RestManager manager;

    /**
     * 初始化OSS配置属性
     *
     * @param properties
     * @return void
     * @author mabin
     * @date 2023/3/27 11:22
     **/
    @Override
    public void init(ConfigProperties properties) {
        super.init(properties);
        searchStr = HTTPS + properties.getDomain() + StrUtil.SLASH;
        manager = new RestManager(properties.getBucketName(), properties.getAccessKey(), properties.getSecretKey());
        manager.setApiDomain(RestManager.ED_AUTO);
        manager.setTimeout(30);
    }

    /**
     * 文件流上传
     *
     * @param inputStream 二进制文件流
     * @param fileName    文件名(含后缀)
     * @param isRename    是否重命名(规则: yyyyMM/10位随机字符.suffix)
     * @param isHighSpeed 是否高速上传(暂支持阿里云OSS, 腾讯云COS)
     * @return java.lang.String
     * @throws Exception
     * @author mabin
     * @date 2023/3/16 9:53
     **/
    @Override
    public String upload(InputStream inputStream, String fileName, Boolean isRename, Boolean isHighSpeed) throws Exception {
        if (ObjectUtil.isNull(inputStream) || StrUtil.isBlank(fileName)) {
            log.error("oss-core-upyun: required parameter is null!");
            return null;
        }
        if (ObjectUtil.isNotNull(isHighSpeed) && isHighSpeed) {
            log.error("oss-core-upyun: partitioning upload is not supported temporarily!");
            return null;
        }
        // 文件存储位置(默认:根据年月分类) & 重命名处理(默认: nanoId)
        if (isRename) {
            fileName = IdUtil.nanoId(10) + StrUtil.DOT + FileUtil.extName(fileName);
        }
        String filePath = DateUtil.format(new Date(), StrategyAbstract.SIMPLE_DATE_FORMAT) + StrUtil.SLASH + fileName;
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            HashMap<String, String> params = new HashMap<>(4);
            IoUtil.copy(inputStream, outputStream);
            byte[] bytes = outputStream.toByteArray();
            params.put(RestManager.PARAMS.CONTENT_MD5.getValue(), UpYunUtils.md5(bytes));
            Response result = manager.writeFile(filePath, new ByteArrayInputStream(bytes), params);
            log.info("oss-core-upyun: upyun upload file response: {}", JsonUtil.toPrettyJson(result));
            if (result.isSuccessful()) {
                String url = searchStr + filePath;
                log.info("oss-core-upyun: upyun upload current file access straight chain url: {}", url);
                return url;
            }
        } catch (Exception e) {
            log.error("oss-core-upyun: upyun upload file: {} fail!", filePath, e);
            throw e;
        } finally {
            IoUtil.close(inputStream);
        }
        return null;
    }

    /**
     * 文件下载
     *
     * @param response
     * @param url
     * @param fileName 下载的文件名称(为空则默认截取url文件名)
     * @return void
     * @throws Exception
     * @author mabin
     * @date 2023/3/16 9:54
     **/
    @Override
    public void download(HttpServletResponse response, String url, String fileName) throws Exception {
        if (StrUtil.isBlank(url)) {
            log.error("oss-core-upyun: required parameter is null!");
            return;
        }
        InputStream inputStream = null;
        String filePath = null;
        try {
            filePath = StrUtil.replace(url, searchStr, StrUtil.EMPTY);
            Response result = manager.readFile(filePath);
            if (ObjectUtil.isNull(result) || ObjectUtil.isNull(result.body())) {
                log.error("oss-core-upyun: upyun get file: {} resource error!", filePath);
                return;
            }
            log.info("oss-core-upyun: upyun download file response: {}", JsonUtil.toPrettyJson(result));
            inputStream = result.body().byteStream();
            // 文件名处理(如果文件名为空,则设置默认文件名)
            if (StrUtil.isBlank(fileName)) {
                fileName = FileUtil.getName(filePath);
            }
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setHeader(Header.CONTENT_DISPOSITION.getValue(), "attachment; filename=" + encodeDownloadFilename(fileName));
            IoUtil.copy(inputStream, response.getOutputStream());
        } catch (Exception e) {
            log.error("oss-core-upyun: download file: {} fail!", filePath, e);
            throw e;
        } finally {
            IoUtil.close(inputStream);
        }
    }

    /**
     * 文件删除
     *
     * @param url
     * @param sha 文件唯一标识(兼容github)
     * @return void
     * @throws Exception
     * @author mabin
     * @date 2023/3/16 9:55
     **/
    @Override
    public void delete(String url, String sha) throws Exception {
        if (StrUtil.isBlank(url)) {
            log.error("oss-core-upyun: required parameter is null!");
            return;
        }
        if (StrUtil.isNotBlank(sha)) {
            log.warn("oss-core-upyun: the current method is only compatible with github!");
            return;
        }
        String filePath = null;
        try {
            filePath = StrUtil.replace(url, searchStr, StrUtil.EMPTY);
            Response result = manager.deleteFile(filePath, null);
            if (ObjectUtil.isNull(result)) {
                log.error("oss-core-upyun: upyun delete file: {} resource error!", filePath);
                return;
            }
            log.info("oss-core-upyun: upyun delete file response: {}", JsonUtil.toPrettyJson(result));
            if (result.isSuccessful()) {
                log.info("oss-core-upyun: delete file: {} success!", filePath);
            }
        } catch (Exception e) {
            log.error("oss-core-upyun: delete file: {} fail!", filePath, e);
            throw e;
        }
    }

}
