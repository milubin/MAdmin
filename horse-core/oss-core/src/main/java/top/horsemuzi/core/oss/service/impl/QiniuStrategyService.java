package top.horsemuzi.core.oss.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.Header;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.core.oss.properties.ConfigProperties;
import top.horsemuzi.core.oss.service.StrategyAbstract;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;

/**
 * 七牛云对象存储相关处理
 *
 * @author mabin
 * @date 2022/06/02 20:00
 **/

@Slf4j
@Service
public class QiniuStrategyService extends StrategyAbstract {

    private String searchStr;
    private UploadManager uploadManager;
    private BucketManager bucketManager;


    /**
     * 初始化OSS配置属性
     *
     * @param properties
     * @return void
     * @author mabin
     * @date 2023/3/27 11:22
     **/
    @Override
    public void init(ConfigProperties properties) {
        super.init(properties);
        Configuration config = new Configuration(Region.huadong());
        config.retryMax = 3;
        config.resumableUploadAPIVersion = Configuration.ResumableUploadAPIVersion.V2;
        uploadManager = new UploadManager(config);
        bucketManager = new BucketManager(Auth.create(properties.getAccessKey(), properties.getSecretKey()), config);
        searchStr = HTTPS + properties.getDomain() + StrUtil.SLASH;
    }

    /**
     * 文件流上传
     *
     * @param inputStream 二进制文件流
     * @param fileName    文件名(含后缀)
     * @param isRename    是否重命名(规则: yyyyMM/10位随机字符.suffix)
     * @param isHighSpeed 是否高速上传(暂支持阿里云OSS, 腾讯云COS)
     * @return java.lang.String
     * @throws Exception
     * @author mabin
     * @date 2023/3/16 9:53
     **/
    @Override
    public String upload(InputStream inputStream, String fileName, Boolean isRename, Boolean isHighSpeed) throws Exception {
        if (ObjectUtil.isNull(inputStream) || StrUtil.isBlank(fileName)) {
            log.error("oss-core-qiniu: required parameter is null!");
            return null;
        }
        if (ObjectUtil.isNotNull(isHighSpeed) && isHighSpeed) {
            log.error("oss-core-qiniu: partitioning upload is not supported temporarily!");
            return null;
        }
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            IoUtil.copy(inputStream, bos);
            // 文件重命名
            if (isRename) {
                String suffixName = FileUtil.extName(fileName);
                fileName = DateUtil.format(new Date(), StrategyAbstract.SIMPLE_DATE_FORMAT) + StrUtil.SLASH + IdUtil.nanoId(10) + StrUtil.DOT + suffixName;
            }
            // 文件上传
            Response response = uploadManager.put(new ByteArrayInputStream(bos.toByteArray()), fileName, getToken(), null, null);
            String url = null;
            if (response.isOK()) {
                DefaultPutRet ret = JsonUtil.fromJson(response.bodyString(), DefaultPutRet.class);
                if (ObjectUtil.isNotNull(ret)) {
                    url = searchStr + ret.key;
                    log.info("oss-core-qiniu: upload file success url: {}", url);
                }
            }
            return url;
        } catch (Exception e) {
            log.error("oss-core-qiniu: upload file fail!", e);
            throw e;
        } finally {
            IoUtil.close(inputStream);
        }
    }

    /**
     * 文件下载
     *
     * @param response
     * @param url
     * @param fileName 下载的文件名称(为空则默认截取url文件名)
     * @return void
     * @throws Exception
     * @author mabin
     * @date 2023/3/16 9:54
     **/
    @Override
    public void download(HttpServletResponse response, String url, String fileName) throws Exception {
        if (StrUtil.isBlank(url)) {
            log.error("oss-core-qiniu: required parameter is null!");
            return;
        }
        if (StrUtil.isBlank(fileName)) {
            fileName = FileUtil.getName(url);
        }
        try (InputStream inputStream = new URL(url).openStream()) {
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setHeader(Header.CONTENT_DISPOSITION.getValue(), "attachment; filename=" + encodeDownloadFilename(fileName));
            IoUtil.copy(inputStream, response.getOutputStream());
        } catch (Exception e) {
            log.error("oss-core-qiniu: download file fail!", e);
            throw e;
        }
    }


    /**
     * 文件删除
     *
     * @param url
     * @param sha 文件唯一标识(兼容github)
     * @return void
     * @throws Exception
     * @author mabin
     * @date 2023/3/16 9:55
     **/
    @Override
    public void delete(String url, String sha) throws Exception {
        if (StrUtil.isBlank(url)) {
            log.error("oss-core-qiniu: required parameter is null!");
            return;
        }
        if (StrUtil.isNotBlank(sha)) {
            log.warn("oss-core-qiniu: the current method is only compatible with github!");
            return;
        }
        try {
            bucketManager.delete(properties.getBucketName(), StrUtil.replace(url, searchStr, StrUtil.EMPTY));
            log.info("oss-core-qiniu: delete file: {} success!", url);
        } catch (QiniuException e) {
            log.error("oss-core-qiniu: delete file: {} fail!", url, e);
            throw e;
        }
    }

    private String getToken() {
        return Auth.create(properties.getAccessKey(), properties.getSecretKey())
                .uploadToken(properties.getBucketName(), null, 1800, null);
    }
}
