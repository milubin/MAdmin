package top.horsemuzi.core.oss.service;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.http.Header;
import lombok.extern.slf4j.Slf4j;
import top.horsemuzi.common.util.NetUtil;
import top.horsemuzi.core.oss.properties.ConfigProperties;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.*;

/**
 * 对象存储抽象实例
 *
 * @author mabin
 * @date 2022/06/01 09:14
 **/
@Slf4j
public abstract class StrategyAbstract implements Strategy {

    /**
     * 获取CPU核心线程数
     */
    private static final int CORE = Runtime.getRuntime().availableProcessors();


    /**
     * HTTP协议头
     */
    public static final String HTTPS = "https://";
    public static final String HTTP = "http://";

    /**
     * 组装文件夹的按月格式化模板
     */
    public static final String SIMPLE_DATE_FORMAT = "yyyyMM";


    /**
     * 初始化执行分片上传任务的线程池
     */
    public static final ExecutorService threadPoolTaskExecutor = new ThreadPoolExecutor(
            CORE, CORE * 2, 60, TimeUnit.SECONDS,
            new ArrayBlockingQueue<>(CORE * 2),
            Executors.defaultThreadFactory(),
            new ThreadPoolExecutor.AbortPolicy()
    );

    /**
     * OSS配置属性
     */
    public ConfigProperties properties;

    /**
     * 初始化OSS配置属性
     *
     * @param properties
     * @return void
     * @author mabin
     * @date 2023/3/27 11:22
     **/
    public void init(ConfigProperties properties) {
        this.properties = properties;
    }

    /**
     * 文件流上传
     *
     * @param inputStream 二进制文件流
     * @param fileName    文件名(含后缀)
     * @param isRename    是否重命名(规则: yyyyMM/10位随机字符.suffix)
     * @param isHighSpeed 是否高速上传(暂支持阿里云OSS, 腾讯云COS)
     * @return java.lang.String
     * @throws Exception
     * @author mabin
     * @date 2023/3/16 9:53
     **/
    @Override
    public abstract String upload(InputStream inputStream, String fileName, Boolean isRename, Boolean isHighSpeed) throws Exception;

    /**
     * 文件下载
     *
     * @param response
     * @param url
     * @param fileName 下载的文件名称(为空则默认截取url文件名)
     * @return void
     * @throws Exception
     * @author mabin
     * @date 2023/3/16 9:54
     **/
    @Override
    public abstract void download(HttpServletResponse response, String url, String fileName) throws Exception;

    /**
     * 文件删除
     *
     * @param url
     * @param sha 文件唯一标识(兼容github)
     * @return void
     * @throws Exception
     * @author mabin
     * @date 2023/3/16 9:55
     **/
    @Override
    public abstract void delete(String url, String sha) throws Exception;

    /**
     * 下载文件时，针对不同浏览器，进行附件名的编码
     *
     * @param filename 载文件名
     * @return 编码后的下载附件名
     * @throws IOException
     */
    public static String encodeDownloadFilename(String filename) throws IOException {
        String agent = NetUtil.getGlobalRequest().getHeader(Header.USER_AGENT.getValue());
        // 火狐浏览器
        if (CharSequenceUtil.containsIgnoreCase(agent, "Firefox")) {
            filename = "=?UTF-8?B?" + Base64.encode(filename.getBytes(StandardCharsets.UTF_8)) + "?=";
            filename = filename.replaceAll("\r\n", "");
        } else {
            // IE及其他浏览器
            filename = URLEncoder.encode(filename, StandardCharsets.UTF_8.toString());
            filename = filename.replace("+", " ");
        }
        return filename;
    }

    /**
     * 通过文件后缀获取文件ContentType
     *
     * @param suffixName
     * @return
     */
    public static String getContentType(String suffixName) {
        if (CharSequenceUtil.equalsIgnoreCase("bmp", suffixName)) {
            return "image/bmp";
        }
        if (CharSequenceUtil.equalsIgnoreCase("gif", suffixName)) {
            return "image/gif";
        }
        if (CharSequenceUtil.equalsIgnoreCase("ico", suffixName)) {
            return "image/vnd.microsoft.icon";
        }
        if (CharSequenceUtil.equalsIgnoreCase("jar", suffixName)) {
            return "application/java-archive";
        }
        if (CharSequenceUtil.equalsIgnoreCase("png", suffixName)) {
            return "image/png";
        }
        if (CharSequenceUtil.equalsIgnoreCase("svg", suffixName)) {
            return "image/svg+xml";
        }
        if (CharSequenceUtil.equalsAnyIgnoreCase(suffixName, "jpeg", "jpg")) {
            return "image/jpeg";
        }
        if (CharSequenceUtil.equalsAnyIgnoreCase(suffixName, "tif", "tiff")) {
            return "image/tiff";
        }
        if (CharSequenceUtil.equalsAnyIgnoreCase(suffixName, "html", "htm")) {
            return "text/html";
        }
        if (CharSequenceUtil.equalsIgnoreCase("txt", suffixName)) {
            return "text/plain";
        }
        if (CharSequenceUtil.equalsIgnoreCase("vsd", suffixName)) {
            return "application/vnd.visio";
        }
        if (CharSequenceUtil.equalsAnyIgnoreCase(suffixName, "ppt", "pptx")) {
            return "application/vnd.ms-powerpoint";
        }
        if (CharSequenceUtil.equalsAnyIgnoreCase(suffixName, "doc", "docx")) {
            return "application/msword";
        }
        if (CharSequenceUtil.equalsIgnoreCase("xml", suffixName)) {
            return "text/xml";
        }
        if (CharSequenceUtil.equalsIgnoreCase("mp4", suffixName)) {
            return "video/mp4";
        }
        if (CharSequenceUtil.equalsIgnoreCase("mp3", suffixName)) {
            return "audio/mpeg";
        }
        if (CharSequenceUtil.equalsIgnoreCase("apk", suffixName)) {
            return "application/vnd.android.package-archive";
        }
        if (CharSequenceUtil.equalsIgnoreCase("ipa", suffixName)) {
            return "application/vnd.iphone";
        }
        if (CharSequenceUtil.equalsIgnoreCase("acc", suffixName)) {
            return "audio/aac";
        }
        if (CharSequenceUtil.equalsIgnoreCase("csv", suffixName)) {
            return "text/csv";
        }
        if (CharSequenceUtil.equalsIgnoreCase("css", suffixName)) {
            return "text/css";
        }
        if (CharSequenceUtil.equalsIgnoreCase("js", suffixName)) {
            return "text/javascript";
        }
        if (CharSequenceUtil.equalsIgnoreCase("json", suffixName)) {
            return "application/json";
        }
        if (CharSequenceUtil.equalsIgnoreCase("ttf", suffixName)) {
            return "font/ttf";
        }
        if (CharSequenceUtil.equalsIgnoreCase("gz", suffixName)) {
            return "application/gzip";
        }
        if (CharSequenceUtil.equalsIgnoreCase("7z", suffixName)) {
            return "application/x-7z-compressed";
        }
        if (CharSequenceUtil.equalsIgnoreCase("rar", suffixName)) {
            return "application/vnd.rar";
        }
        if (CharSequenceUtil.equalsIgnoreCase("tar", suffixName)) {
            return "application/x-tar";
        }
        // 默认返回类型(任何类型的二进制数据)
        return "application/octet-stream";
    }

}
