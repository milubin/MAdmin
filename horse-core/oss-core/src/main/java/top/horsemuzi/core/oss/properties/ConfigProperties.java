package top.horsemuzi.core.oss.properties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * OSS对象存储属性
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/03/27 11:05
 **/
@Data
public class ConfigProperties implements Serializable {

    private static final long serialVersionUID = -8190887644777579884L;

    @ApiModelProperty("系统配置key")
    private String configKey;

    @ApiModelProperty("accessKey")
    private String accessKey;

    @ApiModelProperty("秘钥")
    private String secretKey;

    @ApiModelProperty("桶名称")
    private String bucketName;

    @ApiModelProperty("前缀")
    private String prefix;

    @ApiModelProperty("访问站点")
    private String endpoint;

    @ApiModelProperty("自定义域名")
    private String domain;

    @ApiModelProperty("域")
    private String region;

    @ApiModelProperty("是否https（0=是,1=否）")
    private String https;

    @ApiModelProperty("桶权限类型(0=private 1=public 2=custom)")
    private String accessPolicy;

    @ApiModelProperty(value = "扩展字段", notes = "github.repo")
    private String ext1;

    @ApiModelProperty(value = "扩展字段", notes = "github.path")
    private String ext2;
}
