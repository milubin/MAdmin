package top.horsemuzi.core.oss.service.impl;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.core.oss.properties.ConfigProperties;
import top.horsemuzi.core.oss.service.StrategyAbstract;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * github文件上传/下载/删除处理
 *
 * @author mabin
 * @date 2022/06/02 20:52
 **/

@Slf4j
@Service
public class GithubStrategyService extends StrategyAbstract {

    /**
     * github相关配置
     */
    private static final String CDN = "https://cdn.jsdelivr.net/gh/";
    private static final String ACCEPT = "application/vnd.github.v3+json";
    private String url = "https://api.github.com/repos/{}/{}/contents/";

    private String repo;
    private String path;
    private String token;
    private String owner;

    /**
     * 初始化OSS配置属性
     *
     * @param properties
     * @return void
     * @author mabin
     * @date 2023/3/27 11:22
     **/
    @Override
    public void init(ConfigProperties properties) {
        super.init(properties);
        owner = properties.getAccessKey();
        token = properties.getSecretKey();
        repo = properties.getExt1();
        path = properties.getExt2();
    }

    /**
     * 文件流上传
     *
     * @param inputStream 二进制文件流
     * @param fileName    文件名(含后缀)
     * @param isRename    是否重命名(规则: yyyyMM/10位随机字符.suffix)
     * @param isHighSpeed 是否高速上传(暂支持阿里云OSS, 腾讯云COS)
     * @return java.lang.String
     * @throws Exception
     * @author mabin
     * @date 2023/3/16 9:53
     **/
    @Override
    public String upload(InputStream inputStream, String fileName, Boolean isRename, Boolean isHighSpeed) throws Exception {
        if (ObjectUtil.isNull(inputStream) || StrUtil.isBlank(fileName)) {
            log.error("oss-core-github: required parameter is null!");
            return null;
        }
        if (ObjectUtil.isNotNull(isHighSpeed) && isHighSpeed) {
            log.error("oss-core-github: partitioning upload is not supported temporarily!");
            return null;
        }
        try {
            String dir = path + StrUtil.SLASH + DateUtil.format(new Date(), StrategyAbstract.SIMPLE_DATE_FORMAT);
            if (isRename) {
                // 文件重命名并组装文件路径
                fileName = IdUtil.nanoId(10) + StrUtil.DOT + FileUtil.extName(fileName);
            }
            String path = dir + StrUtil.SLASH + fileName;
            // 请求上传
            Map<String, Object> paramsMap = new LinkedHashMap<>(6);
            paramsMap.put("message", "🎉🎉🎉 upload success 🎉🎉🎉");
            // 文件转换Base64格式
            paramsMap.put("content", Base64.encode(inputStream));
            String prefixUrl = StrUtil.format(url, owner, repo);
            String response = HttpRequest.put(prefixUrl + path)
                    .headerMap(headerMap(), true)
                    .body(JsonUtil.toJson(paramsMap))
                    .execute()
                    .body();
            if (StrUtil.isNotBlank(response)) {
                JsonNode jsonNode = JsonUtil.fromJson(response);
                String url = getCdnUrl(JsonUtil.atNode(jsonNode, "/content/path").asText());
                String sha = JsonUtil.atNode(jsonNode, "/content/sha").asText();
                if (StrUtil.isAllNotBlank(url, sha)) {
                    String fileUrl = StrUtil.concat(true, url, StrUtil.COMMA, sha);
                    log.info("oss-core-github: upload file success origin url: {}", fileUrl);
                    log.info("oss-core-github: upload file success cdn url: {}", url);
                }
            }
        } catch (Exception e) {
            log.error("oss-core-github: upload file fail!", e);
            throw e;
        }
        return null;
    }

    /**
     * 文件下载
     *
     * @param response
     * @param url
     * @param fileName 下载的文件名称(为空则默认截取url文件名)
     * @return void
     * @throws Exception
     * @author mabin
     * @date 2023/3/16 9:54
     **/
    @Override
    public void download(HttpServletResponse response, String url, String fileName) throws Exception {
        if (StrUtil.isBlank(url)) {
            log.error("oss-core-github: required parameter is null!");
            return;
        }
        if (StrUtil.isBlank(fileName)) {
            fileName = FileUtil.getName(url);
        }
        try (InputStream inputStream = new URL(url).openStream();) {
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setHeader(Header.CONTENT_DISPOSITION.getValue(), "attachment; filename=" + encodeDownloadFilename(fileName));
            IoUtil.copy(inputStream, response.getOutputStream());
        } catch (Exception e) {
            log.error("oss-core-github: download file: {} fail!", url, e);
            throw e;
        }
    }

    /**
     * 文件删除
     *
     * @param url
     * @param sha 文件唯一标识(兼容github)
     * @return void
     * @throws Exception
     * @author mabin
     * @date 2023/3/16 9:55
     **/
    @Override
    public void delete(String url, String sha) {
        if (StrUtil.hasBlank(url, sha)) {
            log.error("oss-core-github: required parameter is null!");
            return;
        }
        String prefixUrl = StrUtil.format(url, owner, repo);
        String path = getPath(Convert.toStr(url));
        Map<String, Object> paramsMap = new LinkedHashMap<>(6);
        paramsMap.put("message", "🎉🎉🎉 delete success 🎉🎉🎉");
        paramsMap.put("sha", sha);
        String response = HttpRequest.delete(prefixUrl + path)
                .headerMap(headerMap(), true)
                .body(JsonUtil.toJson(paramsMap))
                .execute()
                .body();
        log.info("oss-core-github: github file delete response: {}", response);
    }


    /**
     * 获取CDN加速的github文件链接
     *
     * @param path
     * @return java.lang.String
     * @author mabin
     * @date 2023/3/22 9:59
     **/
    private String getCdnUrl(String path) {
        return StrUtil.concat(true, CDN, owner, StrUtil.SLASH, repo, StrUtil.SLASH, path);
    }

    /**
     * 获取符合github格式的文件url
     *
     * @param url
     * @return java.lang.String
     * @author mabin
     * @date 2023/3/22 10:00
     **/
    private String getPath(String url) {
        return StrUtil.replace(url, StrUtil.concat(true, CDN, owner, StrUtil.SLASH, repo, StrUtil.SLASH), StrUtil.EMPTY);
    }

    /**
     * 组装特殊Header请求头
     *
     * @param
     * @return java.util.Map<java.lang.String, java.lang.String>
     * @author mabin
     * @date 2023/3/22 9:57
     **/
    public Map<String, String> headerMap() {
        Map<String, String> headerMap = new HashMap<>(6);
        headerMap.put(Header.ACCEPT.getValue(), ACCEPT);
        headerMap.put(Header.AUTHORIZATION.getValue(), "token " + token);
        return headerMap;
    }

}
