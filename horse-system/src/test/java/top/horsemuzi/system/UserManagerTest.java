package top.horsemuzi.system;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import top.horsemuzi.system.pojo.dto.user.UserDTO;
import top.horsemuzi.system.service.UserService;

/**
 * 用户管理模块单元测试
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/05/06 09:41
 **/
@Slf4j
@SpringBootTest
public class UserManagerTest {

    @Test
    public void getUserTest() {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(1L);
        userDTO.setUsername("horse");
        UserService userService = Mockito.mock(UserService.class);
        Mockito.when(userService.getUser(1L)).thenReturn(userDTO);

        UserDTO result = userService.getUser(1L);
        log.info("result:{}", result);

        Mockito.verify(userService).getUser(Mockito.anyLong());

        Assert.assertNotNull("用户不存在!", result);
        Assert.assertEquals("用户信息错误!", "horse", result.getUsername());
    }

}
