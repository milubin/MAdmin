package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.common.enums.ConfigEnum;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.config.ConfigDTO;
import top.horsemuzi.system.pojo.dto.config.ConfigQuery;
import top.horsemuzi.system.pojo.entity.Config;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统配置表 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-09 17:36:32
 */
public interface ConfigService extends IService<Config> {

    /**
     * 获取系统配置参数
     *
     * @param configGroup
     * @param configKeys
     * @return
     */
    Map<String, String> getConfigValueMap(String configGroup, String... configKeys);

    /**
     * 获取系统配置参数
     *
     * @param configGroup
     * @return
     */
    Map<String, String> getConfigValueMap(String configGroup);

    /**
     * 获取系统配置参数
     *
     * @param configGroup
     * @param configKey
     * @return
     */
    String getConfigValue(String configGroup, String configKey);

    /**
     * 获取系统配置参数
     *
     * @param configEnum
     * @return
     */
    String getConfigValue(ConfigEnum configEnum);

    /**
     * 分页获取系统配置列表
     *
     * @param configQuery
     * @return
     */
    Page<ConfigDTO> getPageConfigs(ConfigQuery configQuery);

    /**
     * 获取系统配置信息详情
     *
     * @param queryConfigDTO
     * @return
     */
    ConfigDTO getConfig(ConfigDTO queryConfigDTO);

    /**
     * 新增系统配置信息
     *
     * @param configDTO
     * @return
     */
    boolean addConfig(ConfigDTO configDTO);

    /**
     * 更新系统配置信息
     *
     * @param configDTO
     * @return
     */
    boolean updateConfig(ConfigDTO configDTO);

    /**
     * 获取系统配置信息
     *
     * @param configIds
     * @return
     */
    List<ConfigDTO> getConfigs(List<Long> configIds);

    /**
     * 删除系统配置信息
     *
     * @param configIds
     * @return
     */
    boolean deleteConfigs(List<Long> configIds);

    /**
     * 获取系统配置信息列表
     *
     * @param queryConfigDTO
     * @return
     */
    List<ConfigDTO> getConfigs(ConfigDTO queryConfigDTO);

    /**
     * 获取系统配置信息详情
     *
     * @param configEnum
     * @return
     */
    Map<String, String> getConfig(ConfigEnum configEnum);

    /**
     * 系统配置信息导出
     *
     * @param configQuery
     */
    void export(ConfigQuery configQuery);

    /**
     * 配置是否重复
     *
     * @param queryConfigDTO
     * @return
     */
    ConfigDTO checkConfigRepeat(ConfigDTO queryConfigDTO);

    /**
     * 配置参数名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> fetch(String search);
}
