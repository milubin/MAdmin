package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.role.RoleDTO;
import top.horsemuzi.system.pojo.dto.role.RoleQuery;
import top.horsemuzi.system.pojo.entity.Role;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-07 22:44:27
 */
public interface RoleService extends IService<Role> {


    /**
     * 新增角色信息
     *
     * @param roleDTO
     * @return
     */
    boolean addRole(RoleDTO roleDTO);

    /**
     * 获取角色详情
     *
     * @param queryRoleDTO
     * @return
     */
    RoleDTO getRole(RoleDTO queryRoleDTO);

    /**
     * 根据id获取角色详情
     *
     * @param id
     * @return
     */
    RoleDTO getRole(Long id);

    /**
     * 更新角色信息
     *
     * @param roleDTO
     * @return
     */
    boolean updateRole(RoleDTO roleDTO);

    /**
     * 根据id删除角色信息
     *
     * @param roleIds
     * @return
     */
    boolean deleteRoles(List<Long> roleIds);

    /**
     * 获取角色信息列表
     *
     * @param roleIds
     * @return
     */
    List<RoleDTO> getRoles(List<Long> roleIds);

    /**
     * 分页获取角色列表
     *
     * @param roleQuery
     * @return
     */
    Page<RoleDTO> getPageRoles(RoleQuery roleQuery);

    /**
     * 获取角色信息列表
     *
     * @return
     */
    List<RoleDTO> getRoles();

    /**
     * 获取角色信息列表
     *
     * @param roleCodeList
     * @return
     */
    List<RoleDTO> getRolesByCodes(List<String> roleCodeList);

    /**
     * 角色权限标识是否重复(与停用状态的角色也不能重复)
     *
     * @param roleDTO
     * @return
     */
    boolean checkRoleUnique(RoleDTO roleDTO);

    /**
     * 角色名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> fetch(String search);

    /**
     * 导出角色和菜单权限信息
     *
     * @param query
     * @param response
     */
    void export(RoleQuery query, HttpServletResponse response);
}
