package top.horsemuzi.system.service.impl;

import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.core.office.service.OfficeService;
import top.horsemuzi.system.mapper.AppMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.open.app.AppDTO;
import top.horsemuzi.system.pojo.dto.open.app.AppExportDTO;
import top.horsemuzi.system.pojo.dto.open.app.AppQuery;
import top.horsemuzi.system.pojo.entity.App;
import top.horsemuzi.system.service.AppService;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>
 * 开放API应用信息 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-18 22:09:28
 */
@Service
public class AppServiceImpl extends ExpandServiceImpl<AppMapper, App> implements AppService {

    /**
     * 分页获取开放API应用列表
     *
     * @param query
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public Page<AppDTO> getPageApps(AppQuery query) {
        Page<App> page = new Page<>(query.getPage(), query.getSize());
        LambdaQueryWrapper<App> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StrUtil.isNotBlank(query.getAppName()), App::getAppName, query.getAppName())
                .eq(StrUtil.isNotBlank(query.getApprovalStatus()), App::getApprovalStatus, query.getApprovalStatus())
                .ge(ObjectUtil.isNotNull(query.getCreateStart()), App::getGmtCreate, query.getCreateStart())
                .le(ObjectUtil.isNotNull(query.getCreateEnd()), App::getGmtCreate, query.getCreateEnd())
                .orderByDesc(App::getGmtCreate);
        return App.convertPage(page(page, wrapper), AppDTO.class);
    }

    /**
     * 获取开放API应用详情
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public AppDTO getApp(Long id) {
        ValidatorUtil.notEmpty(id);
        return OrikaUtil.convert(getById(id), AppDTO.class);
    }

    /**
     * 获取开放API应用列表
     *
     * @param appIds
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<AppDTO> getApp(List<Long> appIds) {
        ValidatorUtil.notEmpty(appIds);
        return OrikaUtil.convertList(listByIds(appIds), AppDTO.class);
    }

    /**
     * 保存开放API应用
     *
     * @param appDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addApp(AppDTO appDTO) {
        ValidatorUtil.notEmpty(appDTO);
        App app = OrikaUtil.convert(appDTO, App.class);
        ValidatorUtil.isTrue(save(app), ResultCodeEnum.DATA_SAVE_ERROR);
        appDTO.setId(app.getId());
        return true;
    }

    /**
     * 更新开放API应用
     *
     * @param appDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateApp(AppDTO appDTO) {
        ValidatorUtil.notEmpty(appDTO);
        ValidatorUtil.notEmpty(appDTO.getId());
        ValidatorUtil.isTrue(updateById(OrikaUtil.convert(appDTO, App.class)), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 删除开放API应用
     *
     * @param appIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteApp(List<Long> appIds) {
        ValidatorUtil.notEmpty(appIds);
        ValidatorUtil.isTrue(removeBatchByIds(appIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 应用名称实时检索
     *
     * @param search
     * @return
     */
    @Override
    public List<InputRemoteFetchDTO> fetch(String search) {
        return baseMapper.fetch(search);
    }

    /**
     * 开放API应用名称是否重复
     *
     * @param appDTO
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public boolean checkAppRepeat(AppDTO appDTO) {
        ValidatorUtil.notEmpty(appDTO);
        LambdaQueryWrapper<App> wrapper = new LambdaQueryWrapper<>();
        wrapper.ne(ObjectUtil.isNotNull(appDTO.getId()), App::getId, appDTO.getId())
                .eq(StrUtil.isNotBlank(appDTO.getAppName()), App::getAppName, appDTO.getAppName());
        return ObjectUtil.isNull(getOne(wrapper));
    }

    /**
     * 获取API应用信息
     *
     * @param appKey
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public AppDTO getApp(String appKey) {
        ValidatorUtil.notEmpty(appKey);
        LambdaQueryWrapper<App> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(App::getAppKey, appKey);
        return OrikaUtil.convert(getOne(wrapper), AppDTO.class);
    }

    /**
     * API应用数据导出
     *
     * @param query
     * @param response
     */
    @Override
    public void export(AppQuery query, HttpServletResponse response) {
        List<AppExportDTO> appExportDTOList = baseMapper.getAppExportData(query);
        try {
            if (CollectionUtil.isNotEmpty(appExportDTOList)) {
                OfficeService.EXCEL_POI.export(response, AppExportDTO.class, appExportDTOList,
                        "API应用数据.xlsx", "开放平台API应用信息", "API应用信息", ExcelType.XSSF);
            }
        } catch (Exception e) {
            throw new BusinessException(ResultCodeEnum.FILE_EXPORT_ERROR);
        }
    }

}
