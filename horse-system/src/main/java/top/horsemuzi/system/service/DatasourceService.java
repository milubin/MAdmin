package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.generate.*;
import top.horsemuzi.system.pojo.entity.Datasource;

import java.sql.Connection;
import java.util.List;

/**
 * <p>
 * 数据源信息 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-12-19 13:48:53
 */
public interface DatasourceService extends IService<Datasource> {

    /**
     * 分页获取数据源列表
     *
     * @param query
     * @return
     */
    Page<DatasourceDTO> getPageDatasource(DataSourceQuery query);

    /**
     * 获取数据源详情
     *
     * @param id
     * @return
     */
    DatasourceDTO getDatasource(Long id);

    /**
     * 获取数据源列表
     *
     * @param datasourceIds
     * @return
     */
    List<DatasourceDTO> getDatasource(List<Long> datasourceIds);

    /**
     * 数据源是否重复
     *
     * @param datasourceDTO
     * @return
     */
    boolean checkDatasourceRepeat(DatasourceDTO datasourceDTO);

    /**
     * 新增数据源
     *
     * @param datasourceDTO
     * @return
     */
    boolean addDataSource(DatasourceDTO datasourceDTO);

    /**
     * 更新数据源
     *
     * @param datasourceDTO
     * @return
     */
    boolean updateDataSource(DatasourceDTO datasourceDTO);

    /**
     * 删除数据源
     *
     * @param datasourceIds
     * @return
     */
    boolean deleteDataSource(List<Long> datasourceIds);

    /**
     * 获取数据源的所有数据库名称
     *
     * @param datasourceDTO
     * @return
     */
    List<String> getDatabases(DatasourceDTO datasourceDTO);

    /**
     * 获取数据源的所有数据库名称
     *
     * @param datasourceId
     * @return
     */
    List<String> getDatabases(Long datasourceId);

    /**
     * 获取建表的DDL语句
     *
     * @param tableDTO
     * @return
     */
    String getTableDDL(TableDTO tableDTO);

    /**
     * 连接数据源
     *
     * @param datasourceDTO
     * @return
     */
    Connection connectDataSource(DatasourceDTO datasourceDTO);

    /**
     * 获取数据库所有表信息
     *
     * @param database
     * @param datasourceId
     * @return
     */
    List<SchemaTableDTO> getSchemaTables(String database, Long datasourceId);

    /**
     * 获取数据库业务表所有列字段详情列表
     *
     * @param datasourceId 数据源id
     * @param database     数据库名称
     * @param tableName    表名称
     * @return
     */
    List<SchemaColumnDTO> getSchemaColumns(Long datasourceId, String database, String tableName);

    /**
     * 数据源名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> fetchDatasource(String search);

    /**
     * 获取数据源列表
     *
     * @return
     */
    List<DatasourceDTO> getDatasourceList();

    /**
     * 执行DDL建表语句
     *
     * @param datasourceId 数据源id
     * @param databaseName 数据库名称
     * @param tableDdl     DDL语句
     * @return
     */
    boolean execTableDDL(Long datasourceId, String databaseName, String tableDdl);
}
