package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.dict.DictTypeDTO;
import top.horsemuzi.system.pojo.dto.dict.DictTypeQuery;
import top.horsemuzi.system.pojo.entity.DictType;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>
 * 字典类型表 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-06-20 11:04:36
 */
public interface DictTypeService extends IService<DictType> {

    /**
     * 新增字典类型
     *
     * @param dictTypeDTO
     * @return
     */
    boolean addDictType(DictTypeDTO dictTypeDTO);

    /**
     * 更新字典类型
     *
     * @param dictTypeDTO
     * @return
     */
    boolean updateDictType(DictTypeDTO dictTypeDTO);

    /**
     * 分页获取字典类型列表
     *
     * @param dictTypeQuery
     * @return
     */
    Page<DictTypeDTO> getPageDictTypes(DictTypeQuery dictTypeQuery);

    /**
     * 批量删除字典类型
     *
     * @param dictTypeIds
     * @return
     */
    boolean deleteDictTypes(List<Long> dictTypeIds);

    /**
     * 获取字典类型信息列表
     *
     * @param dictTypeIds
     * @return
     */
    List<DictTypeDTO> getDictTypes(List<Long> dictTypeIds);

    /**
     * 获取字典类型信息
     *
     * @param dictTypeDTO
     * @return
     */
    DictTypeDTO getDictType(DictTypeDTO dictTypeDTO);

    /**
     * 根据id获取字典类型详情
     *
     * @param id
     * @return
     */
    DictTypeDTO getDictType(Long id);

    /**
     * 字典类型数据导出数据(字典类型和对应字典数据全部导出)
     *
     * @param dictTypeQuery
     * @param response
     */
    void export(DictTypeQuery dictTypeQuery, HttpServletResponse response);

    /**
     * 校验字典类型是否重复
     *
     * @param queryDictTypeDTO
     * @return
     */
    DictTypeDTO checkDictTypeRepeat(DictTypeDTO queryDictTypeDTO);

    /**
     * 获取字典类型列表
     *
     * @param queryDictTypeDTO
     * @return
     */
    List<DictTypeDTO> getDictTypes(DictTypeDTO queryDictTypeDTO);

    /**
     * 字典类型实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> fetch(String search);

    /**
     * 获取字典类型列表
     *
     * @param
     * @return java.util.List<top.horsemuzi.system.pojo.dto.dict.DictTypeDTO>
     * @author mabin
     * @date 2023/4/5 16:25
     **/
    List<DictTypeDTO> getDictTypes();

}
