package top.horsemuzi.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.mapper.TableColumnMapper;
import top.horsemuzi.system.pojo.dto.generate.TableColumnDTO;
import top.horsemuzi.system.pojo.entity.TableColumn;
import top.horsemuzi.system.service.TableColumnService;

import java.util.List;

/**
 * <p>
 * 业务字段表 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-12-19 13:48:53
 */
@Service
public class TableColumnServiceImpl extends ExpandServiceImpl<TableColumnMapper, TableColumn> implements TableColumnService {

    /**
     * 获取业务表字段列表
     *
     * @param tableId
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<TableColumnDTO> getTableColumn(Long tableId) {
        ValidatorUtil.notEmpty(tableId);
        LambdaQueryWrapper<TableColumn> wrapper = new LambdaQueryWrapper<TableColumn>()
                .eq(TableColumn::getTableId, tableId);
        return list(wrapper, TableColumnDTO.class);
    }

    /**
     * 批量更新业务表字段
     *
     * @param tableColumnDTOList
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateTableColumn(List<TableColumnDTO> tableColumnDTOList) {
        ValidatorUtil.notEmpty(tableColumnDTOList);
        ValidatorUtil.isTrue(updateBatchById(tableColumnDTOList, TableColumn.class), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 删除业务表字段
     *
     * @param tableColumnIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteTableColumn(List<Long> tableColumnIds) {
        ValidatorUtil.notEmpty(tableColumnIds);
        ValidatorUtil.isTrue(removeBatchByIds(tableColumnIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 获取业务表字段列表
     *
     * @param tableIds
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<TableColumnDTO> getTableColumn(List<Long> tableIds) {
        ValidatorUtil.notEmpty(tableIds);
        LambdaQueryWrapper<TableColumn> wrapper = new LambdaQueryWrapper<TableColumn>().in(TableColumn::getTableId, tableIds);
        return list(wrapper, TableColumnDTO.class);
    }

    /**
     * 批量保存业务表字段
     *
     * @param tableColumnDTOList
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addTableColumn(List<TableColumnDTO> tableColumnDTOList) {
        ValidatorUtil.notEmpty(tableColumnDTOList);
        ValidatorUtil.isTrue(saveBatch(tableColumnDTOList, TableColumn.class), ResultCodeEnum.DATA_SAVE_ERROR);
        return true;
    }

}
