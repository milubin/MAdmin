package top.horsemuzi.system.service.impl;

import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.core.office.service.OfficeService;
import top.horsemuzi.system.mapper.ApiLogMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.open.log.ApiLogDTO;
import top.horsemuzi.system.pojo.dto.open.log.ApiLogQuery;
import top.horsemuzi.system.pojo.entity.ApiLog;
import top.horsemuzi.system.service.ApiLogService;

import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;

/**
 * <p>
 * 开放API请求日志 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-21 22:26:38
 */
@Service
public class ApiLogServiceImpl extends ExpandServiceImpl<ApiLogMapper, ApiLog> implements ApiLogService {

    /**
     * 分页获取开放API请求日志列表
     *
     * @param query
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public Page<ApiLogDTO> getPageApiLogs(ApiLogQuery query) {
        Page<ApiLog> page = new Page<>(query.getPage(), query.getSize());
        LambdaQueryWrapper<ApiLog> wrapper = getApiLogWrapper(query);
        wrapper.select(ApiLog::getId, ApiLog::getTraceId, ApiLog::getAppId, ApiLog::getAppKey, ApiLog::getAppName, ApiLog::getApiCode,
                        ApiLog::getApiFunction, ApiLog::getIp, ApiLog::getArea, ApiLog::getOs, ApiLog::getBrowser, ApiLog::getStatus,
                        ApiLog::getCode, ApiLog::getConsume, ApiLog::getRemark, ApiLog::getGmtCreate)
                .orderByDesc(ApiLog::getGmtCreate);
        return ApiLog.convertPage(page(page, wrapper), ApiLogDTO.class);
    }

    /**
     * API日志链路ID实时检索
     *
     * @param search
     * @return
     */
    @Override
    public List<InputRemoteFetchDTO> logFetch(String search) {
        return StrUtil.isBlank(search) ? Collections.emptyList() : baseMapper.logFetch(search);
    }

    /**
     * API接口请求日志数据导出
     *
     * @param query
     * @param response
     */
    @Override
    public void exportApiLog(ApiLogQuery query, HttpServletResponse response) {
        List<ApiLogDTO> apiLogExportList = list(getApiLogWrapper(query), ApiLogDTO.class);
        try {
            if (CollectionUtil.isNotEmpty(apiLogExportList)) {
                OfficeService.EXCEL_POI.export(response, ApiLogDTO.class, apiLogExportList,
                        "API请求日志数据.xlsx", "开放平台API请求日志信息", "API请求日志信息", ExcelType.XSSF);
            }
        } catch (Exception e) {
            throw new BusinessException(e, ResultCodeEnum.FILE_EXPORT_ERROR);
        }
    }

    /**
     * 组装查询条件
     *
     * @param query
     * @return
     */
    private LambdaQueryWrapper<ApiLog> getApiLogWrapper(ApiLogQuery query) {
        return new LambdaQueryWrapper<ApiLog>()
                .eq(StrUtil.isNotBlank(query.getApiCode()), ApiLog::getApiCode, query.getApiCode())
                .eq(StrUtil.isNotBlank(query.getIp()), ApiLog::getIp, query.getIp())
                .like(StrUtil.isNotBlank(query.getTraceId()), ApiLog::getTraceId, query.getTraceId())
                .like(StrUtil.isNotBlank(query.getApiFunction()), ApiLog::getApiFunction, query.getApiFunction())
                .like(StrUtil.isNotBlank(query.getAppName()), ApiLog::getAppName, query.getAppName())
                .ge(ObjectUtil.isNotNull(query.getCreateStart()), ApiLog::getGmtCreate, query.getCreateStart())
                .le(ObjectUtil.isNotNull(query.getCreateEnd()), ApiLog::getGmtCreate, query.getCreateEnd());
    }

    /**
     * 获取开放API请求日志详情
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public ApiLogDTO getApiLog(Long id) {
        ValidatorUtil.notEmpty(id);
        return OrikaUtil.convert(getById(id), ApiLogDTO.class);
    }

    /**
     * 删除开放API请求日志
     *
     * @param apiLogIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteApiLog(List<Long> apiLogIds) {
        ValidatorUtil.notEmpty(apiLogIds);
        ValidatorUtil.isTrue(removeBatchByIds(apiLogIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 保存API请求日志
     *
     * @param apiLogDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addApiLog(ApiLogDTO apiLogDTO) {
        ValidatorUtil.notEmpty(apiLogDTO);
        ApiLog apiLog = OrikaUtil.convert(apiLogDTO, ApiLog.class);
        ValidatorUtil.isTrue(save(apiLog), ResultCodeEnum.DATA_SAVE_ERROR);
        apiLogDTO.setId(apiLog.getId());
        return true;
    }

}
