package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.log.LoginLogDTO;
import top.horsemuzi.system.pojo.dto.log.LoginLogQuery;
import top.horsemuzi.system.pojo.dto.statistics.PieChartDTO;
import top.horsemuzi.system.pojo.entity.LoginLog;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>
 * 系统登录日志记录表 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-09-26 21:50:38
 */
public interface LoginLogService extends IService<LoginLog> {

    /**
     * 分页获取登录日志列表
     *
     * @param query
     * @return
     */
    Page<LoginLogDTO> getPageLoginLogs(LoginLogQuery query);

    /**
     * 保存登录日志
     *
     * @param loginLogDTO
     * @return
     */
    boolean saveLoginLog(LoginLogDTO loginLogDTO);

    /**
     * 删除登录日志
     *
     * @param loginLogIds
     * @return
     */
    boolean deleteLoginLog(List<Long> loginLogIds);

    /**
     * 导出系统登录日志(CSV)
     *
     * @param query
     * @param response
     */
    void loginExport(LoginLogQuery query, HttpServletResponse response);

    /**
     * 获取登录日志详情
     *
     * @param id
     * @return
     */
    LoginLogDTO getLoginLog(Long id);

    /**
     * 获取登录日志成功失败饼图
     *
     * @return
     */
    List<PieChartDTO> statisticsBasePieChartData();

    /**
     * 获取登录日志隐藏原因饼图
     *
     * @return
     */
    List<PieChartDTO> statisticsPieChartData();

    /**
     * 登录日志链路ID实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> loginFetch(String search);
}
