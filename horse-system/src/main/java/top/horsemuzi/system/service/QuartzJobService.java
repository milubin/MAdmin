package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobDTO;
import top.horsemuzi.system.pojo.entity.QuartzJob;

import java.util.List;

/**
 * <p>
 * Quartz任务调度信息表 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-08-21 16:05:01
 */
public interface QuartzJobService extends IService<QuartzJob> {

    /**
     * 保存Quartz调度任务信息
     *
     * @param quartzDTO
     * @return
     */
    boolean saveQuartz(QuartzJobDTO quartzDTO);

    /**
     * 更新Quartz调度任务信息
     *
     * @param quartzDTO
     * @return
     */
    boolean updateQuartz(QuartzJobDTO quartzDTO);

    /**
     * 获取Quartz调度任务信息
     *
     * @param id
     * @return
     */
    QuartzJobDTO getQuartz(Long id);

    /**
     * 获取Quartz调度任务信息列表
     *
     * @param jobIds
     * @return
     */
    List<QuartzJobDTO> getQuartz(List<Long> jobIds);

    /**
     * 删除Quartz调度任务
     *
     * @param jobIds
     * @return
     */
    boolean deleteQuartz(List<Long> jobIds);

    /**
     * 校验jobName和jobGroup是否重复
     *
     * @param quartzDTO
     * @return
     */
    boolean checkRepeat(QuartzJobDTO quartzDTO);

    /**
     * 更新Quartz任务信息
     *
     * @param jobDTOList
     * @return boolean
     * @author mabin
     * @date 2023/3/1 11:21
     **/
    boolean updateQuartz(List<QuartzJobDTO> jobDTOList);
}
