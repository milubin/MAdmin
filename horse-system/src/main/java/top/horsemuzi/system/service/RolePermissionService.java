package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.role.RoleDTO;
import top.horsemuzi.system.pojo.entity.RolePermission;

import java.util.List;

/**
 * <p>
 * 角色权限表 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-07 22:44:27
 */
public interface RolePermissionService extends IService<RolePermission> {

    /**
     * 新增角色权限关联信息
     *
     * @param roleDTO
     * @return
     */
    boolean addRolePermission(RoleDTO roleDTO);

    /**
     * 删除角色权限关联信息
     *
     * @param roleIds
     * @return
     */
    boolean deleteRolePermission(List<Long> roleIds);

    /**
     * 获取角色权限关联信息集合
     *
     * @param rolePermission
     * @return
     */
    List<RolePermission> getRolePermissions(RolePermission rolePermission);
}
