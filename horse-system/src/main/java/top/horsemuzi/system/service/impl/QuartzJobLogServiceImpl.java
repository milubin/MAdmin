package top.horsemuzi.system.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.mapper.QuartzJobLogMapper;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobLogDTO;
import top.horsemuzi.system.pojo.dto.statistics.LineChartDTO;
import top.horsemuzi.system.pojo.dto.statistics.PieChartDTO;
import top.horsemuzi.system.pojo.dto.statistics.SourceLineData;
import top.horsemuzi.system.pojo.entity.QuartzJobLog;
import top.horsemuzi.system.service.QuartzJobLogService;

import java.util.List;

/**
 * <p>
 * Quartz任务调度日志信息表 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-08-21 16:05:01
 */
@Service
public class QuartzJobLogServiceImpl extends ExpandServiceImpl<QuartzJobLogMapper, QuartzJobLog> implements QuartzJobLogService {

    /**
     * 保存调度任务执行日志
     *
     * @param quartzJobLogDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean saveJobLog(QuartzJobLogDTO quartzJobLogDTO) {
        ValidatorUtil.notEmpty(quartzJobLogDTO);
        QuartzJobLog quartzJobLog = OrikaUtil.convert(quartzJobLogDTO, QuartzJobLog.class);
        ValidatorUtil.isTrue(save(quartzJobLog), ResultCodeEnum.DATA_SAVE_ERROR);
        quartzJobLogDTO.setId(quartzJobLog.getId());
        return true;
    }

    /**
     * 获取Quartz任务调度日志饼图统计数据
     *
     * @return
     */
    @Override
    public List<PieChartDTO> statisticsPieChartData() {
        return baseMapper.statisticsPieChartData();
    }

    /**
     * 获取Quartz任务调度日志折线图统计数据
     *
     * @return
     */
    @Override
    public List<SourceLineData> statisticsLineChartData() {
        return baseMapper.statisticsLineChartData();
    }

    /**
     * 清理Quartz任务调度日志信息
     *
     * @param quartzLogIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteLog(List<Long> quartzLogIds) {
        ValidatorUtil.notEmpty(quartzLogIds);
        ValidatorUtil.isTrue(removeBatchByIds(quartzLogIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

}
