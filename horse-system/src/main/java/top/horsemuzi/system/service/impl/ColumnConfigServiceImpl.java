package top.horsemuzi.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.mapper.ColumnConfigMapper;
import top.horsemuzi.system.pojo.dto.generate.ColumnConfigDTO;
import top.horsemuzi.system.pojo.dto.generate.ColumnConfigQuery;
import top.horsemuzi.system.pojo.entity.ColumnConfig;
import top.horsemuzi.system.service.ColumnConfigService;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 字段类型关联信息 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-12-19 13:48:53
 */
@Service
public class ColumnConfigServiceImpl extends ExpandServiceImpl<ColumnConfigMapper, ColumnConfig> implements ColumnConfigService {

    /**
     * 分页获取字段类型关联列表
     *
     * @param query
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public Page<ColumnConfigDTO> getPageColumnConfig(ColumnConfigQuery query) {
        Page<ColumnConfig> page = new Page<>(query.getPage(), query.getSize());
        LambdaQueryWrapper<ColumnConfig> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StrUtil.isNotBlank(query.getColumnType()), ColumnConfig::getColumnType, query.getColumnType())
                .like(StrUtil.isNotBlank(query.getPropertyType()), ColumnConfig::getPropertyType, query.getPropertyType())
                .ge(ObjectUtil.isNotNull(query.getCreateStart()), ColumnConfig::getGmtCreate, query.getCreateStart())
                .le(ObjectUtil.isNotNull(query.getCreateEnd()), ColumnConfig::getGmtCreate, query.getCreateEnd())
                .orderByDesc(ColumnConfig::getGmtCreate);
        return ColumnConfig.convertPage(page(page, wrapper), ColumnConfigDTO.class);
    }

    /**
     * 获取字段类型关联详情
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public ColumnConfigDTO getColumnConfig(Long id) {
        ValidatorUtil.notEmpty(id);
        return getById(id, ColumnConfigDTO.class);
    }

    /**
     * 字段类型关联是否重复
     *
     * @param columnConfigDTO
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public boolean checkColumnConfigRepeat(ColumnConfigDTO columnConfigDTO) {
        LambdaQueryWrapper<ColumnConfig> wrapper = new LambdaQueryWrapper<>();
        wrapper.ne(ObjectUtil.isNotNull(columnConfigDTO.getId()), ColumnConfig::getId, columnConfigDTO.getId())
                .eq(StrUtil.isNotBlank(columnConfigDTO.getColumnType()), ColumnConfig::getColumnType, columnConfigDTO.getColumnType())
                .eq(StrUtil.isNotBlank(columnConfigDTO.getPropertyType()), ColumnConfig::getPropertyType, columnConfigDTO.getPropertyType());
        return ObjectUtil.isNull(getOne(wrapper));
    }

    /**
     * 新增字段类型关联
     *
     * @param columnConfigDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addColumnConfig(ColumnConfigDTO columnConfigDTO) {
        ValidatorUtil.notEmpty(columnConfigDTO);
        ColumnConfig columnConfig = OrikaUtil.convert(columnConfigDTO, ColumnConfig.class);
        ValidatorUtil.isTrue(save(columnConfig), ResultCodeEnum.DATA_SAVE_ERROR);
        columnConfigDTO.setId(columnConfig.getId());
        return true;
    }

    /**
     * 更新字段类型关联
     *
     * @param columnConfigDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateColumnConfig(ColumnConfigDTO columnConfigDTO) {
        ValidatorUtil.notEmpty(columnConfigDTO);
        ValidatorUtil.notEmpty(columnConfigDTO.getId());
        ValidatorUtil.isTrue(updateById(columnConfigDTO, ColumnConfig.class), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 删除字段类型关联
     *
     * @param columnConfigIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteColumnConfig(List<Long> columnConfigIds) {
        ValidatorUtil.notEmpty(columnConfigIds);
        ValidatorUtil.isTrue(removeBatchByIds(columnConfigIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 获取字段类型关联列表
     *
     * @param columnTypes
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<ColumnConfigDTO> getColumnConfig(Set<String> columnTypes) {
        ValidatorUtil.notEmpty(columnTypes);
        LambdaQueryWrapper<ColumnConfig> wrapper = new LambdaQueryWrapper<ColumnConfig>()
                .in(ColumnConfig::getColumnType, columnTypes)
                .ne(ColumnConfig::getPackageName, StrUtil.EMPTY)
                .isNotNull(ColumnConfig::getPackageName);
        return list(wrapper, ColumnConfigDTO.class);
    }

    /**
     * 获取字段列表: 针对每种类型组装字符串列表, type=0返回DB字段类型列表, type=1返回JAVA字段类型列表
     *
     * @param type
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<String> getColumnConfigType(String type) {
        ValidatorUtil.notEmpty(type);
        List<ColumnConfig> columnConfigList = list();
        if (CollUtil.isNotEmpty(columnConfigList)) {
            if (StrUtil.equals(Constants.COMMON_JUDGMENT.ZONE_STR, type)) {
                return columnConfigList.stream().map(ColumnConfig::getColumnType)
                        .collect(Collectors.toList());
            } else if (StrUtil.equals(Constants.COMMON_JUDGMENT.ONE_STR, type)) {
                return columnConfigList.stream().map(ColumnConfig::getPropertyType)
                        .distinct().collect(Collectors.toList());
            }
        }
        return Collections.emptyList();
    }

    /**
     * 获取字段类型关联列表
     *
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<ColumnConfigDTO> getColumnConfig() {
        return list(ColumnConfigDTO.class);
    }

}
