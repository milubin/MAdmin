package top.horsemuzi.system.service.impl;

import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.core.office.service.OfficeService;
import top.horsemuzi.system.mapper.OssConfigMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.oss.OssConfigDTO;
import top.horsemuzi.system.pojo.dto.oss.OssConfigQuery;
import top.horsemuzi.system.pojo.entity.OssConfig;
import top.horsemuzi.system.service.OssConfigService;

import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;

/**
 * @author Mr.Horse
 * @version 1.0.0
 * @description OSS对象存储配置业务对象tb_oss_config的Service实现
 * @date 2023-03-26 11:32:55
 */
@Service
public class OssConfigServiceImpl extends ExpandServiceImpl<OssConfigMapper, OssConfig> implements OssConfigService {

    /**
     * 分页获取OSS对象存储配置
     *
     * @param query
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public Page<OssConfigDTO> getPageOssConfig(OssConfigQuery query) {
        Page<OssConfig> page = new Page<>(query.getPage(), query.getSize());
        LambdaQueryWrapper<OssConfig> wrapper = getQueryWrapper(query);
        wrapper.orderByDesc(OssConfig::getGmtCreate);
        return OssConfig.convertPage(page(page, wrapper), OssConfigDTO.class);
    }

    /**
     * OSS对象存储配置数据导出
     *
     * @param query
     * @param response
     */
    @Override
    public void export(OssConfigQuery query, HttpServletResponse response) {
        // 获取导出数据信息
        List<OssConfigDTO> ossConfigDTOList = list(getQueryWrapper(query), OssConfigDTO.class);
        try {
            if (CollUtil.isNotEmpty(ossConfigDTOList)) {
                OfficeService.EXCEL_POI.export(response, OssConfigDTO.class, ossConfigDTOList, "OSS配置信息.xlsx", "OSS配置信息", "OSS配置详情", ExcelType.XSSF);
            }
        } catch (Exception e) {
            throw new BusinessException(e, ResultCodeEnum.FILE_EXPORT_ERROR);
        }
    }

    /**
     * 组装查询wrapper条件
     *
     * @param query
     * @return com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper<top.horsemuzi.system.pojo.entity.OssConfig>
     * @author mabin
     * @date 2023/3/27 9:40
     **/
    private LambdaQueryWrapper<OssConfig> getQueryWrapper(OssConfigQuery query) {
        LambdaQueryWrapper<OssConfig> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StrUtil.isNotBlank(query.getConfigKey()), OssConfig::getConfigKey, query.getConfigKey());
        wrapper.eq(StrUtil.isNotBlank(query.getStatus()), OssConfig::getStatus, query.getStatus());
        wrapper.ge(ObjectUtil.isNotNull(query.getCreateStart()), OssConfig::getGmtCreate, query.getCreateStart());
        wrapper.le(ObjectUtil.isNotNull(query.getCreateEnd()), OssConfig::getGmtCreate, query.getCreateEnd());
        return wrapper;
    }

    /**
     * 获取OSS对象存储配置详情
     *
     * @param id 主键id
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public OssConfigDTO getOssConfig(Long id) {
        ValidatorUtil.notEmpty(id);
        return getById(id, OssConfigDTO.class);
    }

    /**
     * 获取OSS对象存储配置列表
     *
     * @param ossConfigIds
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<OssConfigDTO> getOssConfig(List<Long> ossConfigIds) {
        ValidatorUtil.notEmpty(ossConfigIds);
        return listByIds(ossConfigIds, OssConfigDTO.class);
    }

    /**
     * 保存OSS对象存储配置
     *
     * @param ossConfigDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addOssConfig(OssConfigDTO ossConfigDTO) {
        ValidatorUtil.notEmpty(ossConfigDTO);
        OssConfig ossConfig = OrikaUtil.convert(ossConfigDTO, OssConfig.class);
        ValidatorUtil.isTrue(save(ossConfig), ResultCodeEnum.DATA_SAVE_ERROR);
        ossConfigDTO.setId(ossConfig.getId());
        return true;
    }

    /**
     * 更新OSS对象存储配置
     *
     * @param ossConfigDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateOssConfig(OssConfigDTO ossConfigDTO) {
        ValidatorUtil.notEmpty(ossConfigDTO);
        ValidatorUtil.notEmpty(ossConfigDTO.getId());
        ValidatorUtil.isTrue(updateById(ossConfigDTO, OssConfig.class), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 删除OSS对象存储配置
     *
     * @param ossConfigIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteOssConfig(List<Long> ossConfigIds) {
        ValidatorUtil.notEmpty(ossConfigIds);
        ValidatorUtil.isTrue(removeBatchByIds(ossConfigIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 根据配置key获取配置信息
     *
     * @param configKey
     * @return top.horsemuzi.system.pojo.dto.oss.OssConfigDTO
     * @author mabin
     * @date 2023/3/27 11:28
     **/
    @Transactional(readOnly = true)
    @Override
    public OssConfigDTO getOssConfig(String configKey) {
        ValidatorUtil.notEmpty(configKey);
        LambdaQueryWrapper<OssConfig> wrapper = new LambdaQueryWrapper<OssConfig>()
                .eq(OssConfig::getConfigKey, configKey);
        return getOne(wrapper, OssConfigDTO.class);
    }

    /**
     * 实时检索
     *
     * @param search 检索关键词
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<InputRemoteFetchDTO> fetchConfig(String search) {
        return StrUtil.isBlank(search) ? Collections.emptyList() : baseMapper.fetchConfig(search);
    }

}
