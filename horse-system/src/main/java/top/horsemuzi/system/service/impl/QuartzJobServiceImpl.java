package top.horsemuzi.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.mapper.QuartzJobMapper;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobDTO;
import top.horsemuzi.system.pojo.entity.QuartzJob;
import top.horsemuzi.system.service.QuartzJobService;

import java.util.List;

/**
 * <p>
 * Quartz任务调度信息表 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-08-21 16:05:01
 */
@Service
public class QuartzJobServiceImpl extends ExpandServiceImpl<QuartzJobMapper, QuartzJob> implements QuartzJobService {

    /**
     * 保存Quartz调度任务信息
     *
     * @param quartzDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean saveQuartz(QuartzJobDTO quartzDTO) {
        ValidatorUtil.notEmpty(quartzDTO);
        QuartzJob quartzJob = OrikaUtil.convert(quartzDTO, QuartzJob.class);
        ValidatorUtil.isTrue(save(quartzJob), ResultCodeEnum.DATA_SAVE_ERROR);
        quartzDTO.setId(quartzJob.getId());
        return true;
    }

    /**
     * 更新Quartz调度任务信息
     *
     * @param quartzDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateQuartz(QuartzJobDTO quartzDTO) {
        ValidatorUtil.allNotEmpty(quartzDTO, quartzDTO.getId());
        ValidatorUtil.isTrue(updateById(OrikaUtil.convert(quartzDTO, QuartzJob.class)),
                ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 获取Quartz调度任务信息列表
     *
     * @param jobIds
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<QuartzJobDTO> getQuartz(List<Long> jobIds) {
        ValidatorUtil.notEmpty(jobIds);
        return OrikaUtil.convertList(listByIds(jobIds), QuartzJobDTO.class);
    }

    /**
     * 校验jobName和jobGroup是否重复
     *
     * @param quartzDTO
     * @return
     */
    @Override
    public boolean checkRepeat(QuartzJobDTO quartzDTO) {
        LambdaQueryWrapper<QuartzJob> wrapper = new LambdaQueryWrapper<>();
        if (ObjectUtil.isNotNull(quartzDTO)) {
            wrapper.ne(ObjectUtil.isNotNull(quartzDTO.getId()), QuartzJob::getId, quartzDTO.getId())
                    .eq(StrUtil.isNotBlank(quartzDTO.getJobName()), QuartzJob::getJobName, quartzDTO.getJobName())
                    .eq(StrUtil.isNotBlank(quartzDTO.getJobGroup()), QuartzJob::getJobGroup, quartzDTO.getJobGroup());
        }
        return ObjectUtil.isNotNull(getOne(wrapper));
    }

    /**
     * 更新Quartz任务信息
     *
     * @param jobDTOList
     * @return boolean
     * @author mabin
     * @date 2023/3/1 11:21
     **/
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateQuartz(List<QuartzJobDTO> jobDTOList) {
        ValidatorUtil.notEmpty(jobDTOList);
        ValidatorUtil.isTrue(updateBatchById(jobDTOList, QuartzJob.class), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 删除Quartz调度任务
     *
     * @param jobIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteQuartz(List<Long> jobIds) {
        ValidatorUtil.notEmpty(jobIds);
        ValidatorUtil.isTrue(removeByIds(jobIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 获取Quartz调度任务信息
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public QuartzJobDTO getQuartz(Long id) {
        ValidatorUtil.notEmpty(id);
        return OrikaUtil.convert(getById(id), QuartzJobDTO.class);
    }

}
