package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.message.MessageChannelDTO;
import top.horsemuzi.system.pojo.entity.MessageChannel;

import java.util.List;

/**
 * <p>
 * 消息渠道配置表 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-09-25 18:20:21
 */
public interface MessageChannelService extends IService<MessageChannel> {

    /**
     * 获取消息渠道配置
     *
     * @param channel 渠道标识
     * @return
     */
    MessageChannelDTO getMessageChannel(Integer channel);

    /**
     * 新增消息渠道
     *
     * @param messageChannelDTO
     * @return
     */
    boolean addMessageChannel(MessageChannelDTO messageChannelDTO);

    /**
     * 校验消息渠道是否重复
     *
     * @param messageChannelDTO
     * @return
     */
    boolean checkMessageChannelRepeat(MessageChannelDTO messageChannelDTO);

    /**
     * 获取消息渠道列表
     *
     * @return
     */
    List<MessageChannelDTO> getMessageChannels();

    /**
     * 更新消息渠道
     *
     * @param messageChannelDTO
     * @return
     */
    boolean updateMessageChannel(MessageChannelDTO messageChannelDTO);
}
