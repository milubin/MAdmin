package top.horsemuzi.system.service.impl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ConfigEnum;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.NetUtil;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.core.office.service.OfficeService;
import top.horsemuzi.system.core.redis.service.RedisService;
import top.horsemuzi.system.mapper.ConfigMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.config.ConfigDTO;
import top.horsemuzi.system.pojo.dto.config.ConfigQuery;
import top.horsemuzi.system.pojo.entity.Config;
import top.horsemuzi.system.service.ConfigService;

import java.util.*;

/**
 * <p>
 * 系统配置表 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-09 17:36:32
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ConfigServiceImpl extends ExpandServiceImpl<ConfigMapper, Config> implements ConfigService {

    private final RedisService redisService;

    /**
     * 获取系统配置信息详情
     *
     * @param configEnum
     * @return
     */
    @Override
    public Map<String, String> getConfig(ConfigEnum configEnum) {
        ValidatorUtil.notEmpty(configEnum);
        return getConfigValueMap(configEnum.getGroup(), configEnum.getKey());
    }

    /**
     * 获取系统配置参数
     *
     * @param configEnum
     * @return
     */
    @Override
    public String getConfigValue(ConfigEnum configEnum) {
        return getConfigValue(configEnum.getGroup(), configEnum.getKey());
    }

    /**
     * 获取系统配置参数
     *
     * @param configGroup
     * @param configKey
     * @return
     */
    @Override
    public String getConfigValue(String configGroup, String configKey) {
        Map<String, String> configValueMap = getConfigValueMap(configGroup, configKey);
        return MapUtil.isNotEmpty(configValueMap) ? configValueMap.get(configKey) : StrUtil.EMPTY;
    }

    /**
     * 获取系统配置参数
     *
     * @param configGroup
     * @return
     */
    @Override
    public Map<String, String> getConfigValueMap(String configGroup) {
        String[] configKeys = ArrayUtil.toArray(ConfigEnum.getKeysByGroup(configGroup), String.class);
        return getConfigValueMap(configGroup, configKeys);
    }

    /**
     * 获取系统配置参数
     *
     * @param configGroup 参数配置组别
     * @param configKeys
     * @return
     */
    @Override
    public Map<String, String> getConfigValueMap(String configGroup, String... configKeys) {
        Map<String, String> configMap = new LinkedHashMap<>(16);
        if (StrUtil.isBlank(configGroup)) {
            return configMap;
        }
        String configKey = Constants.REDIS_PREFIX.CONFIG + StrUtil.COLON + configGroup;
        String cacheConfig = redisService.get(configKey);
        List<ConfigDTO> configDTOList = null;
        if (StrUtil.isNotEmpty(cacheConfig)) {
            configDTOList = JsonUtil.fromJson(cacheConfig, new TypeReference<List<ConfigDTO>>() {
            });
        } else {
            ConfigDTO queryConfigDTO = new ConfigDTO();
            queryConfigDTO.setConfigGroup(configGroup);
            configDTOList = getConfigs(queryConfigDTO);
        }
        ValidatorUtil.notEmpty(configDTOList, ResultCodeEnum.CONFIG_NOT_EXIST_ERROR);
        for (ConfigDTO configDTO : configDTOList) {
            List<String> keys = Arrays.asList(configKeys);
            if (keys.contains(configDTO.getConfigKey())) {
                configMap.put(configDTO.getConfigKey(), configDTO.getConfigValue());
            }
        }
        redisService.set(configKey, JsonUtil.toJson(configDTOList));
        return configMap;
    }

    /**
     * 分页获取系统配置列表
     *
     * @param configQuery
     * @return
     */
    @Override
    public Page<ConfigDTO> getPageConfigs(ConfigQuery configQuery) {
        Page<Config> page = new Page<>(configQuery.getPage(), configQuery.getSize());
        LambdaQueryWrapper<Config> wrapper = getConfigWrapper(configQuery);
        wrapper.orderByDesc(Config::getGmtCreate);
        return Config.convertPage(page(page, wrapper), ConfigDTO.class);
    }

    /**
     * 组装查询条件(分页查询、导出查询)
     *
     * @param configQuery
     * @return
     */
    private LambdaQueryWrapper<Config> getConfigWrapper(ConfigQuery configQuery) {
        return new LambdaQueryWrapper<Config>()
                .like(StrUtil.isNotBlank(configQuery.getConfigGroup()), Config::getConfigGroup,
                        configQuery.getConfigGroup())
                .like(StrUtil.isNotBlank(configQuery.getConfigName()), Config::getConfigName,
                        configQuery.getConfigName())
                .like(StrUtil.isNotBlank(configQuery.getConfigKey()), Config::getConfigKey, configQuery.getConfigKey())
                .eq(StrUtil.isNotBlank(configQuery.getInternal()), Config::getInternal, configQuery.getInternal())
                .ge(Objects.nonNull(configQuery.getCreateStart()), Config::getGmtCreate, configQuery.getCreateStart())
                .le(Objects.nonNull(configQuery.getCreateEnd()), Config::getGmtCreate, configQuery.getCreateEnd());
    }

    /**
     * 系统配置信息
     *
     * @return
     */
    @Override
    public ConfigDTO getConfig(ConfigDTO configDTO) {
        ValidatorUtil.notEmpty(configDTO);
        LambdaQueryWrapper<Config> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StrUtil.isNotBlank(configDTO.getConfigGroup()), Config::getConfigGroup, configDTO.getConfigGroup())
                .eq(StrUtil.isNotBlank(configDTO.getConfigName()), Config::getConfigName, configDTO.getConfigName())
                .eq(StrUtil.isNotBlank(configDTO.getConfigKey()), Config::getConfigKey, configDTO.getConfigKey())
                .eq(StrUtil.isNotBlank(configDTO.getInternal()), Config::getInternal, configDTO.getInternal());
        Config config = getOne(wrapper);
        return OrikaUtil.convert(config, ConfigDTO.class);
    }

    /**
     * 新增系统配置信息
     *
     * @param configDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addConfig(ConfigDTO configDTO) {
        ValidatorUtil.notEmpty(configDTO);
        Config config = OrikaUtil.convert(configDTO, Config.class);
        ValidatorUtil.isTrue(save(config), ResultCodeEnum.DATA_SAVE_ERROR);
        configDTO.setId(config.getId());
        return true;
    }

    /**
     * 更新系统配置信息
     *
     * @param configDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateConfig(ConfigDTO configDTO) {
        ValidatorUtil.notEmpty(configDTO);
        ValidatorUtil.notEmpty(configDTO.getId());
        ValidatorUtil.isTrue(updateById(OrikaUtil.convert(configDTO, Config.class)), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 获取系统配置信息
     *
     * @param configIds
     * @return
     */
    @Override
    public List<ConfigDTO> getConfigs(List<Long> configIds) {
        ValidatorUtil.notEmpty(configIds);
        List<Config> configs = listByIds(configIds);
        return OrikaUtil.convertList(configs, ConfigDTO.class);
    }

    /**
     * 删除系统配置信息
     *
     * @param configIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteConfigs(List<Long> configIds) {
        ValidatorUtil.notEmpty(configIds);
        removeByIds(configIds);
        return true;
    }

    /**
     * 获取系统配置信息列表
     *
     * @return
     */
    @Override
    public List<ConfigDTO> getConfigs(ConfigDTO configDTO) {
        ValidatorUtil.notEmpty(configDTO);
        LambdaQueryWrapper<Config> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StrUtil.isNotBlank(configDTO.getConfigGroup()), Config::getConfigGroup, configDTO.getConfigGroup())
                .eq(StrUtil.isNotBlank(configDTO.getConfigName()), Config::getConfigName, configDTO.getConfigName())
                .eq(StrUtil.isNotBlank(configDTO.getConfigKey()), Config::getConfigKey, configDTO.getConfigKey())
                .eq(StrUtil.isNotBlank(configDTO.getInternal()), Config::getInternal, configDTO.getInternal());
        List<Config> configs = list(wrapper);
        return OrikaUtil.convertList(configs, ConfigDTO.class);
    }

    /**
     * 系统配置信息导出
     *
     * @param configQuery
     */
    @Override
    public void export(ConfigQuery configQuery) {
        ValidatorUtil.notEmpty(configQuery);
        try {
            List<Config> configs = list(getConfigWrapper(configQuery));
            ValidatorUtil.notEmpty(configs, ResultCodeEnum.RESOURCE_NOT_EXIST);
            String fileName = "系统配置信息-" + DateUtil.format(new Date(), DatePattern.PURE_DATETIME_PATTERN);
            OfficeService.EXCEL.export(OrikaUtil.convertList(configs, ConfigDTO.class), fileName, "后台管理系统配置信息",
                    ExcelTypeEnum.XLSX, NetUtil.getGlobalResponse());
        } catch (Exception e) {
            throw new BusinessException( ResultCodeEnum.FILE_EXPORT_ERROR);
        }
    }

    /**
     * 配置参数名称实时检索
     *
     * @param search
     * @return
     */
    @Override
    public List<InputRemoteFetchDTO> fetch(String search) {
        return baseMapper.fetch(search);
    }

    /**
     * 配置是否重复
     *
     * @return
     */
    @Override
    public ConfigDTO checkConfigRepeat(ConfigDTO query) {
        ValidatorUtil.notEmpty(query);
        LambdaQueryWrapper<Config> wrapper = new LambdaQueryWrapper<>();
        wrapper.ne(ObjectUtil.isNotNull(query.getId()), Config::getId, query.getId())
                .eq(StrUtil.isNotBlank(query.getConfigGroup()), Config::getConfigGroup, query.getConfigGroup())
                .eq(StrUtil.isNotBlank(query.getConfigKey()), Config::getConfigKey, query.getConfigKey());
        return OrikaUtil.convert(getOne(wrapper), ConfigDTO.class);
    }

}
