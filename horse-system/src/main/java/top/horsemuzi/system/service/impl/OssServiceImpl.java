package top.horsemuzi.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.mapper.OssMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.oss.OssDTO;
import top.horsemuzi.system.pojo.dto.oss.OssQuery;
import top.horsemuzi.system.pojo.entity.Oss;
import top.horsemuzi.system.service.OssService;

import java.util.List;

/**
 * <p>
 * 对象存储文件信息表 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-08-01 21:50:47
 */
@Service
public class OssServiceImpl extends ExpandServiceImpl<OssMapper, Oss> implements OssService {

    /**
     * 分页获取文件列表
     *
     * @return
     */
    @Override
    public Page<OssDTO> getPageOss(OssQuery query) {
        Page<Oss> page = new Page<>(query.getPage(), query.getSize());
        LambdaQueryWrapper<Oss> wrapper = new LambdaQueryWrapper<>();
        if (ObjectUtils.isNotEmpty(query)) {
            wrapper.like(StrUtil.isNotBlank(query.getOriginalName()), Oss::getOriginalName, query.getOriginalName())
                    .eq(StrUtil.isNotBlank(query.getSuffix()), Oss::getSuffix, query.getSuffix())
                    .eq(StrUtil.isNotBlank(query.getService()), Oss::getService, query.getService())
                    .eq(StrUtil.isNotBlank(query.getInternal()), Oss::getInternal, query.getInternal())
                    .ge(ObjectUtil.isNotNull(query.getCreateStart()), Oss::getGmtCreate, query.getCreateStart())
                    .le(ObjectUtil.isNotNull(query.getCreateEnd()), Oss::getGmtCreate, query.getCreateEnd())
                    .orderByDesc(Oss::getGmtCreate);
        }
        return Oss.convertPage(page(page, wrapper), OssDTO.class);
    }

    /**
     * 保存文件信息
     *
     * @param ossDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean saveOss(OssDTO ossDTO) {
        ValidatorUtil.notEmpty(ossDTO);
        Oss oss = OrikaUtil.convert(ossDTO, Oss.class);
        ValidatorUtil.isTrue(save(oss), ResultCodeEnum.DATA_SAVE_ERROR);
        ossDTO.setId(oss.getId());
        return true;
    }

    /**
     * 获取文件信息
     *
     * @param id
     * @return
     */
    @Override
    public OssDTO getOss(Long id) {
        ValidatorUtil.notEmpty(id);
        return getById(id, OssDTO.class);
    }

    /**
     * 获取文件信息列表
     *
     * @param ossIds
     * @return
     */
    @Override
    public List<OssDTO> getOss(List<Long> ossIds) {
        ValidatorUtil.notEmpty(ossIds);
        return listByIds(ossIds, OssDTO.class);
    }

    /**
     * 文件删除
     *
     * @param ossIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean delete(List<Long> ossIds) {
        ValidatorUtil.notEmpty(ossIds);
        ValidatorUtil.isTrue(removeBatchByIds(ossIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 文件名称实时检索
     *
     * @param search
     * @return
     */
    @Override
    public List<InputRemoteFetchDTO> fetch(String search) {
        return baseMapper.fetch(search);
    }

    /**
     * 文件更新
     *
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateOss(OssDTO ossDTO) {
        ValidatorUtil.notEmpty(ossDTO);
        ValidatorUtil.notEmpty(ossDTO.getId());
        ValidatorUtil.isTrue(updateById(ossDTO, Oss.class), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

}
