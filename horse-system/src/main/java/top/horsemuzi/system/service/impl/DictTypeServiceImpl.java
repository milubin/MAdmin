package top.horsemuzi.system.service.impl;

import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.core.office.service.OfficeService;
import top.horsemuzi.system.mapper.DictTypeMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.dict.DictTypeDTO;
import top.horsemuzi.system.pojo.dto.dict.DictTypeExportDTO;
import top.horsemuzi.system.pojo.dto.dict.DictTypeQuery;
import top.horsemuzi.system.pojo.entity.DictType;
import top.horsemuzi.system.service.DictTypeService;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 字典类型表 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-06-20 11:04:36
 */
@Service
public class DictTypeServiceImpl extends ExpandServiceImpl<DictTypeMapper, DictType> implements DictTypeService {


    /**
     * 新增字典类型
     *
     * @param dictTypeDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addDictType(DictTypeDTO dictTypeDTO) {
        ValidatorUtil.notEmpty(dictTypeDTO);
        DictType dictType = OrikaUtil.convert(dictTypeDTO, DictType.class);
        ValidatorUtil.isTrue(save(dictType), ResultCodeEnum.DATA_SAVE_ERROR);
        dictTypeDTO.setId(dictType.getId());
        return true;
    }

    /**
     * 更新字典类型
     *
     * @param dictTypeDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateDictType(DictTypeDTO dictTypeDTO) {
        ValidatorUtil.notEmpty(dictTypeDTO);
        ValidatorUtil.notEmpty(dictTypeDTO.getId(), ResultCodeEnum.OBJECT_ID_IS_NULL);
        ValidatorUtil.isTrue(updateById(dictTypeDTO, DictType.class), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 分页获取字典类型列表
     *
     * @param dictTypeQuery
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public Page<DictTypeDTO> getPageDictTypes(DictTypeQuery dictTypeQuery) {
        ValidatorUtil.notEmpty(dictTypeQuery);
        Page<DictType> page = new Page<>(dictTypeQuery.getPage(), dictTypeQuery.getSize());
        LambdaQueryWrapper<DictType> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StrUtil.isNotBlank(dictTypeQuery.getName()), DictType::getName, dictTypeQuery.getName())
                .like(StrUtil.isNotBlank(dictTypeQuery.getType()), DictType::getType, dictTypeQuery.getType())
                .eq(StrUtil.isNotBlank(dictTypeQuery.getStatus()), DictType::getStatus, dictTypeQuery.getStatus())
                .eq(StrUtil.isNotBlank(dictTypeQuery.getInternal()), DictType::getInternal, dictTypeQuery.getInternal())
                .ge(Objects.nonNull(dictTypeQuery.getCreateStart()), DictType::getGmtCreate, dictTypeQuery.getCreateStart())
                .le(Objects.nonNull(dictTypeQuery.getCreateEnd()), DictType::getGmtCreate, dictTypeQuery.getCreateEnd())
                .orderByDesc(DictType::getGmtCreate);
        return DictType.convertPage(page(page, queryWrapper), DictTypeDTO.class);
    }

    /**
     * 批量删除字典类型
     *
     * @param dictTypeIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteDictTypes(List<Long> dictTypeIds) {
        ValidatorUtil.notEmpty(dictTypeIds);
        ValidatorUtil.isTrue(removeBatchByIds(dictTypeIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 获取字典类型信息列表
     *
     * @param dictTypeIds
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<DictTypeDTO> getDictTypes(List<Long> dictTypeIds) {
        ValidatorUtil.notEmpty(dictTypeIds);
        LambdaQueryWrapper<DictType> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(DictType::getId, dictTypeIds);
        return list(queryWrapper, DictTypeDTO.class);
    }

    /**
     * 获取字典类型信息
     *
     * @param dictTypeDTO
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public DictTypeDTO getDictType(DictTypeDTO dictTypeDTO) {
        ValidatorUtil.notEmpty(dictTypeDTO);
        LambdaQueryWrapper<DictType> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Objects.nonNull(dictTypeDTO.getId()), DictType::getId, dictTypeDTO.getId())
                .eq(StrUtil.isNotBlank(dictTypeDTO.getType()), DictType::getType, dictTypeDTO.getType())
                .eq(StrUtil.isNotBlank(dictTypeDTO.getName()), DictType::getName, dictTypeDTO.getName())
                .eq(StrUtil.isNotBlank(dictTypeDTO.getStatus()), DictType::getStatus, dictTypeDTO.getStatus());
        return getOne(wrapper, DictTypeDTO.class);
    }

    /**
     * 根据id获取字典类型详情
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public DictTypeDTO getDictType(Long id) {
        ValidatorUtil.notEmpty(id);
        return getById(id, DictTypeDTO.class);
    }

    /**
     * 字典类型数据导出数据(字典类型和对应字典数据全部导出)
     *
     * @param dictTypeQuery
     * @param response
     */
    @Override
    public void export(DictTypeQuery dictTypeQuery, HttpServletResponse response) {
        // 查询一对多数据
        List<DictTypeExportDTO> dictTypeExportDTOList = baseMapper.getDictExportList(dictTypeQuery);
        try {
            if (CollectionUtil.isNotEmpty(dictTypeExportDTOList)) {
                OfficeService.EXCEL_POI.export(response, DictTypeExportDTO.class, dictTypeExportDTOList,
                        "字典数据.xlsx", "字典数据", "字典数据信息", ExcelType.XSSF);
            }
        } catch (Exception e) {
            throw new BusinessException(e, ResultCodeEnum.FILE_EXPORT_ERROR);
        }
    }

    /**
     * 校验字典类型是否重复
     *
     * @param queryDictTypeDTO
     * @return
     */
    @Override
    public DictTypeDTO checkDictTypeRepeat(DictTypeDTO queryDictTypeDTO) {
        ValidatorUtil.notEmpty(queryDictTypeDTO);
        LambdaQueryWrapper<DictType> wrapper = new LambdaQueryWrapper<>();
        wrapper.ne(ObjectUtil.isNotNull(queryDictTypeDTO.getId()), DictType::getId, queryDictTypeDTO.getId())
                .eq(StrUtil.isNotBlank(queryDictTypeDTO.getType()), DictType::getType, queryDictTypeDTO.getType());
        return getOne(wrapper, DictTypeDTO.class);
    }

    /**
     * 字典类型实时检索
     *
     * @param search
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<InputRemoteFetchDTO> fetch(String search) {
        return baseMapper.fetch(search);
    }

    @Transactional(readOnly = true)
    @Override
    public List<DictTypeDTO> getDictTypes() {
        return list(DictTypeDTO.class);
    }

    /**
     * 获取字典类型列表
     *
     * @return
     */
    @Override
    public List<DictTypeDTO> getDictTypes(DictTypeDTO query) {
        ValidatorUtil.notEmpty(query);
        LambdaQueryWrapper<DictType> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Objects.nonNull(query.getId()), DictType::getId, query.getId())
                .eq(StrUtil.isNotBlank(query.getType()), DictType::getType, query.getType())
                .eq(StrUtil.isNotBlank(query.getName()), DictType::getName, query.getName())
                .eq(StrUtil.isNotBlank(query.getStatus()), DictType::getStatus, query.getStatus());
        return list(wrapper, DictTypeDTO.class);
    }

}
