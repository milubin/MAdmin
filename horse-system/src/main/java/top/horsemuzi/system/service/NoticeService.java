package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.notice.NoticeDTO;
import top.horsemuzi.system.pojo.dto.notice.NoticeExportDTO;
import top.horsemuzi.system.pojo.dto.notice.NoticeQuery;
import top.horsemuzi.system.pojo.entity.Notice;

import java.util.List;

/**
 * <p>
 * 系统通知公告信息表 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-10-06 16:00:05
 */
public interface NoticeService extends IService<Notice> {

    /**
     * 新增通知公告
     *
     * @param noticeDTO
     * @return
     */
    boolean addNotice(NoticeDTO noticeDTO);

    /**
     * 获取通知公告信息
     *
     * @param id
     * @return
     */
    NoticeDTO getNotice(Long id);

    /**
     * 更新通知公告
     *
     * @param noticeDTO
     * @return
     */
    boolean updateNotice(NoticeDTO noticeDTO);

    /**
     * 获取通知公告信息列表
     *
     * @param noticeIds
     * @return
     */
    List<NoticeDTO> getNotice(List<Long> noticeIds);

    /**
     * 删除通知公告
     *
     * @param noticeIds
     * @return
     */
    boolean deleteNotice(List<Long> noticeIds);

    /**
     * 获取导出通知公告和对应记录数据
     *
     * @param query
     * @return
     */
    List<NoticeExportDTO> getNoticeExportList(NoticeQuery query);

    /**
     * 分页通知公告信息列表数量
     *
     * @param query
     * @return
     */
    Long getPageNoticeCount(NoticeQuery query);

    /**
     * 分页通知公告信息列表
     *
     * @param query
     * @return
     */
    List<NoticeDTO> getPageNoticeList(NoticeQuery query);
}
