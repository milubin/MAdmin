package top.horsemuzi.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.mapper.ShortcutMenuMapper;
import top.horsemuzi.system.pojo.dto.user.ShortcutMenuDTO;
import top.horsemuzi.system.pojo.entity.ShortcutMenu;
import top.horsemuzi.system.service.ShortcutMenuService;

import java.util.List;

/**
 * <p>
 * 用户快捷菜单信息表 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2023-01-07 15:26:14
 */
@Service
public class ShortcutMenuServiceImpl extends ExpandServiceImpl<ShortcutMenuMapper, ShortcutMenu> implements ShortcutMenuService {

    /**
     * 获取用户快捷菜单列表
     *
     * @param userId 用户id
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<ShortcutMenuDTO> getShortcutMenuByUserId(Long userId) {
        ValidatorUtil.notEmpty(userId);
        LambdaQueryWrapper<ShortcutMenu> wrapper = new LambdaQueryWrapper<ShortcutMenu>()
                .eq(ShortcutMenu::getUserId, userId);
        return list(wrapper, ShortcutMenuDTO.class);
    }

    /**
     * 获取用户快捷菜单详情
     *
     * @param id 主键id
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public ShortcutMenuDTO getShortcutMenu(Long id) {
        ValidatorUtil.notEmpty(id);
        return getById(id, ShortcutMenuDTO.class);
    }

    /**
     * 新增快捷菜单
     *
     * @param shortcutMenuDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addShortcutMenu(ShortcutMenuDTO shortcutMenuDTO) {
        ValidatorUtil.notEmpty(shortcutMenuDTO);
        ShortcutMenu shortcutMenu = OrikaUtil.convert(shortcutMenuDTO, ShortcutMenu.class);
        ValidatorUtil.isTrue(save(shortcutMenu), ResultCodeEnum.DATA_SAVE_ERROR);
        shortcutMenuDTO.setId(shortcutMenu.getId());
        return true;
    }

    /**
     * 更新快捷菜单
     *
     * @param shortcutMenuDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateShortcutMenu(ShortcutMenuDTO shortcutMenuDTO) {
        ValidatorUtil.notEmpty(shortcutMenuDTO);
        ValidatorUtil.notEmpty(shortcutMenuDTO.getId());
        ValidatorUtil.isTrue(updateById(shortcutMenuDTO, ShortcutMenu.class), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 快捷菜单是否重复
     *
     * @param shortcutMenuDTO
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public boolean checkShortcutMenuRepeat(ShortcutMenuDTO shortcutMenuDTO) {
        ValidatorUtil.notEmpty(shortcutMenuDTO);
        LambdaQueryWrapper<ShortcutMenu> wrapper = new LambdaQueryWrapper<ShortcutMenu>()
                .ne(ObjectUtil.isNotNull(shortcutMenuDTO.getId()), ShortcutMenu::getId, shortcutMenuDTO.getId())
                .eq(ObjectUtil.isNotNull(shortcutMenuDTO.getUserId()), ShortcutMenu::getUserId, shortcutMenuDTO.getUserId())
                .and(andWrapper -> andWrapper
                        .eq(StrUtil.isNotBlank(shortcutMenuDTO.getMenuComponent()), ShortcutMenu::getMenuComponent, shortcutMenuDTO.getMenuComponent())
                        .or()
                        .eq(StrUtil.isNotBlank(shortcutMenuDTO.getMenuName()), ShortcutMenu::getMenuName, shortcutMenuDTO.getMenuName()));
        return ObjectUtil.isNull(getOne(wrapper));
    }

    /**
     * 删除快捷菜单
     *
     * @param shortcutMenuIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteShortcutMenu(List<Long> shortcutMenuIds) {
        ValidatorUtil.notEmpty(shortcutMenuIds);
        ValidatorUtil.isTrue(removeBatchByIds(shortcutMenuIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }


}
