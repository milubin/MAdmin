package top.horsemuzi.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.mapper.ApiCategoryMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.open.category.ApiCategoryDTO;
import top.horsemuzi.system.pojo.dto.open.category.ApiCategoryQuery;
import top.horsemuzi.system.pojo.entity.ApiCategory;
import top.horsemuzi.system.service.ApiCategoryService;

import java.util.List;

/**
 * <p>
 * 开放API类型信息 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-18 22:09:28
 */
@Service
public class ApiCategoryServiceImpl extends ExpandServiceImpl<ApiCategoryMapper, ApiCategory> implements ApiCategoryService {

    /**
     * 获取开放API接口分类列表
     *
     * @param appId
     * @return
     */
    @Override
    public List<ApiCategoryDTO> listApiCategory(Long appId) {
        ValidatorUtil.notEmpty(appId);
        return baseMapper.listApiCategory(appId);
    }

    /**
     * 分页获取开放API接口类型列表
     *
     * @param query
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public Page<ApiCategoryDTO> getPageApiCategory(ApiCategoryQuery query) {
        Page<ApiCategory> page = new Page<>(query.getPage(), query.getSize());
        LambdaQueryWrapper<ApiCategory> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StrUtil.isNotBlank(query.getCategoryName()), ApiCategory::getCategoryName, query.getCategoryName())
                .ge(ObjectUtil.isNotNull(query.getCreateStart()), ApiCategory::getGmtCreate, query.getCreateStart())
                .le(ObjectUtil.isNotNull(query.getCreateEnd()), ApiCategory::getGmtCreate, query.getCreateEnd())
                .orderByDesc(ApiCategory::getGmtCreate);
        return ApiCategory.convertPage(page(page, wrapper), ApiCategoryDTO.class);
    }

    /**
     * 获取开放API类型详情
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public ApiCategoryDTO getApiCategory(Long id) {
        ValidatorUtil.notEmpty(id);
        return getById(id, ApiCategoryDTO.class);
    }

    /**
     * 新增开放API类型
     *
     * @param apiCategoryDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addApiCategory(ApiCategoryDTO apiCategoryDTO) {
        ValidatorUtil.notEmpty(apiCategoryDTO);
        ApiCategory apiCategory = OrikaUtil.convert(apiCategoryDTO, ApiCategory.class);
        ValidatorUtil.isTrue(save(apiCategory), ResultCodeEnum.DATA_SAVE_ERROR);
        apiCategoryDTO.setId(apiCategory.getId());
        return true;
    }

    /**
     * 获取开放API类型
     *
     * @param apiCategoryIds
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<ApiCategoryDTO> getApiCategory(List<Long> apiCategoryIds) {
        ValidatorUtil.notEmpty(apiCategoryIds);
        return listByIds(apiCategoryIds, ApiCategoryDTO.class);
    }

    /**
     * 移除开放API类型
     *
     * @param apiCategoryIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteApiCategory(List<Long> apiCategoryIds) {
        ValidatorUtil.notEmpty(apiCategoryIds);
        ValidatorUtil.isTrue(removeBatchByIds(apiCategoryIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 接口类型名称实时检索
     *
     * @param search
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<InputRemoteFetchDTO> fetch(String search) {
        return baseMapper.fetch(search);
    }

    /**
     * 获取开放API接口类型列表
     *
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<ApiCategoryDTO> getApiCategoryList() {
        return list(ApiCategoryDTO.class);
    }

    /**
     * 更新开放API类型
     *
     * @param apiCategoryDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateApiCategory(ApiCategoryDTO apiCategoryDTO) {
        ValidatorUtil.notEmpty(apiCategoryDTO);
        ValidatorUtil.notEmpty(apiCategoryDTO.getId());
        ValidatorUtil.isTrue(updateById(OrikaUtil.convert(apiCategoryDTO, ApiCategory.class)), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

}
