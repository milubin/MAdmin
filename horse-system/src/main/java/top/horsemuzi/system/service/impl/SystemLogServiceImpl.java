package top.horsemuzi.system.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.core.office.service.OfficeService;
import top.horsemuzi.system.mapper.SystemLogMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.log.EchartDTO;
import top.horsemuzi.system.pojo.dto.log.SystemLogDTO;
import top.horsemuzi.system.pojo.dto.log.SystemLogQuery;
import top.horsemuzi.system.pojo.entity.SystemLog;
import top.horsemuzi.system.service.SystemLogService;

import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 系统操作日志 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-06-30 20:35:31
 */
@Service
public class SystemLogServiceImpl extends ExpandServiceImpl<SystemLogMapper, SystemLog> implements SystemLogService {

    @Value("${custom.log.path}")
    private String path;

    @Value("${custom.log.level}")
    private String level;

    @Value("${custom.log.size}")
    private String size;

    @Value("${custom.log.day}")
    private Integer day;


    /**
     * 分页获取系统日志列表
     *
     * @param query
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public Page<SystemLogDTO> getPageLogs(SystemLogQuery query) {
        Page<SystemLog> page = new Page<>(query.getPage(), query.getSize());
        QueryWrapper<SystemLog> wrapper = getQueryWrapper(query);
        return SystemLog.convertPage(page(page, wrapper), SystemLogDTO.class);
    }

    private QueryWrapper<SystemLog> getQueryWrapper(SystemLogQuery query) {
        QueryWrapper<SystemLog> wrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(query)) {
            wrapper.select("id", "log_type", "trace_id", "thread_name", "module", "type", "description", "user_id", "username", "method", "ip", "area",
                            "os", "browser", "is_mobile", "url", "http_type", "origin","exc_name", "status", "data", "code", "consume", "gmt_create")
                    .eq(StrUtil.isNotBlank(query.getTraceId()), "trace_id", query.getTraceId())
                    .eq(ObjectUtil.isNotNull(query.getLogType()), "log_type", query.getLogType())
                    .eq(StrUtil.isNotBlank(query.getType()), "type", query.getType())
                    .eq(StrUtil.isNotBlank(query.getIp()), "ip", query.getIp())
                    .eq(ObjectUtil.isNotNull(query.getCode()), "code", query.getCode())
                    .ge(ObjectUtil.isNotNull(query.getCreateStart()), "gmt_create", query.getCreateStart())
                    .le(ObjectUtil.isNotNull(query.getCreateEnd()), "gmt_create", query.getCreateEnd())
                    .like(StrUtil.isNotBlank(query.getModule()), "module", query.getModule())
                    .like(StrUtil.isNotBlank(query.getUsername()), "username", query.getUsername())
                    .like(StrUtil.isNotBlank(query.getMethod()), "method", query.getMethod())
                    .like(StrUtil.isNotBlank(query.getParams()), "params", query.getParams())
                    .like(StrUtil.isNotBlank(query.getBody()), "body", query.getBody())
                    .like(StrUtil.isNotBlank(query.getExcName()), "exc_name", query.getExcName())
                    .like(StrUtil.isNotBlank(query.getExcMessage()), "exc_message", query.getExcMessage())
                    .like(StrUtil.isNotBlank(query.getArea()), "area", query.getArea())
                    .like(StrUtil.isNotBlank(query.getData()), "data", query.getData());
            if (StrUtil.isBlank(query.getSortField())) {
                wrapper.orderByDesc("gmt_create");
            } else {
                wrapper.orderBy(StrUtil.isNotBlank(query.getSortField()), StrUtil.isNotBlank(query.getSortWay()) && StrUtil.equalsIgnoreCase(query.getSortWay(), "ASC"), StrUtil.toUnderlineCase(query.getSortField()));
            }
        }
        return wrapper;
    }

    /**
     * 获取系统日志详情
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public SystemLogDTO getLog(Long id) {
        ValidatorUtil.notEmpty(id);
        return getById(id, SystemLogDTO.class);
    }

    /**
     * 删除系统日志
     *
     * @param logIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteLogs(List<Long> logIds) {
        ValidatorUtil.notEmpty(logIds);
        ValidatorUtil.isTrue(removeBatchByIds(logIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 获取系统日志统计数据
     *
     * @return
     */
    @Override
    public Map<String, Object> statisticsLog() {
        Map<String, Object> statisticsMap = new HashMap<>(16);

        // 日志文件相关配置
        Map<String, Object> configMap = new HashMap<>(16);
        configMap.put("path", path);
        configMap.put("level", level);
        configMap.put("size", size);
        configMap.put("day", day);
        statisticsMap.put("config", configMap);

        // 按日期(7天)统计分析展示生成日志的条数（线型图）
        Map<String, Object> oneMap = new HashMap<>(4);
        List<EchartDTO> dbOneList = baseMapper.statisticsLogGroupDateCount();
        Map<String, List<EchartDTO>> dateGroupMap = dbOneList.stream().collect(Collectors.groupingBy(EchartDTO::getName));
        List<Long> operateList = new ArrayList<>();
        List<Long> exceptionList = new ArrayList<>();
        List<String> dateList = new ArrayList<>();
        for (int i = Constants.COMMON_CODE.SIX; i >= 0; i--) {
            dateList.add(DateUtil.format(DateUtil.offsetDay(new Date(), -i), DatePattern.NORM_DATE_FORMAT));
        }
        oneMap.put("dateData", dateList);
        for (String date : dateList) {
            List<EchartDTO> echartDTOList = dateGroupMap.get(date);
            if (CollectionUtil.isEmpty(echartDTOList)) {
                operateList.add(Constants.COMMON_CODE.LONG_ZONE);
                exceptionList.add(Constants.COMMON_CODE.LONG_ZONE);
            } else {
                if (echartDTOList.size() == Constants.COMMON_CODE.ONE) {
                    EchartDTO echartDTO = echartDTOList.get(0);
                    if (ObjectUtil.equal(Constants.COMMON_CODE.ZONE, echartDTO.getLogType())) {
                        operateList.add(echartDTO.getValue());
                        exceptionList.add(Constants.COMMON_CODE.LONG_ZONE);
                    } else if (ObjectUtil.equal(Constants.COMMON_CODE.ONE, echartDTO.getLogType())) {
                        exceptionList.add(echartDTO.getValue());
                        operateList.add(Constants.COMMON_CODE.LONG_ZONE);
                    }
                } else {
                    echartDTOList.forEach(item -> {
                        if (ObjectUtil.equal(Constants.COMMON_CODE.ZONE, item.getLogType())) {
                            operateList.add(item.getValue());
                        } else {
                            exceptionList.add(item.getValue());
                        }
                    });
                }
            }
        }
        List<List<Long>> oneDataList = new ArrayList<>();
        oneDataList.add(operateList);
        oneDataList.add(exceptionList);
        oneMap.put("valueData", oneDataList);
        statisticsMap.put("one", oneMap);

        // 按日志类型（操作、异常）统计分析各自的日志条数（基础饼图）
        List<Map<String, Object>> dbTwoList = baseMapper.statisticsLogGroupTypeCount();
        statisticsMap.put("two", dbTwoList);
        return statisticsMap;
    }

    /**
     * 保存日志
     *
     * @param systemLogDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean saveLog(SystemLogDTO systemLogDTO) {
        ValidatorUtil.notEmpty(systemLogDTO);
        SystemLog systemLog = OrikaUtil.convert(systemLogDTO, SystemLog.class);
        ValidatorUtil.isTrue(save(systemLog), ResultCodeEnum.DATA_SAVE_ERROR);
        systemLogDTO.setId(systemLog.getId());
        return true;
    }

    /**
     * 操作日志链路ID实时检索
     *
     * @param search
     * @return
     */
    @Override
    public List<InputRemoteFetchDTO> logFetch(String search) {
        return StrUtil.isBlank(search) ? Collections.emptyList() : baseMapper.logFetch(search);
    }

    /**
     * 导出系统操作日志
     *
     * @param query
     * @param response
     */
    @Override
    public void export(SystemLogQuery query, HttpServletResponse response) {
        try {
            QueryWrapper<SystemLog> wrapper = getQueryWrapper(query);
            List<SystemLogDTO> systemLogExportDTOList = list(wrapper, SystemLogDTO.class);
            if (CollectionUtil.isNotEmpty(systemLogExportDTOList)) {
                OfficeService.CSV.export(response, SystemLogDTO.class, systemLogExportDTOList, StrUtil.concat(true, "系统日志", DateUtil.format(new Date(), DatePattern.PURE_DATETIME_PATTERN), ".csv"));
            }
        } catch (Exception e) {
            throw new BusinessException(e, ResultCodeEnum.FILE_EXPORT_ERROR);
        }
    }

}
