package top.horsemuzi.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.mapper.UserRoleMapper;
import top.horsemuzi.system.pojo.dto.user.UserRoleDTO;
import top.horsemuzi.system.pojo.entity.UserRole;
import top.horsemuzi.system.service.UserRoleService;

import java.util.List;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-07 22:44:28
 */
@Service
public class UserRoleServiceImpl extends ExpandServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

    /**
     * 删除关联的用户角色信息
     *
     * @param userIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteUserRoleByUserIds(List<Long> userIds) {
        ValidatorUtil.notEmpty(userIds);
        LambdaQueryWrapper<UserRole> wrapper = new LambdaQueryWrapper<UserRole>()
                .in(UserRole::getUserId, userIds);
        ValidatorUtil.isTrue(remove(wrapper), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 根据角色id集合获取对应的用户id集合
     *
     * @param roleIds
     * @return
     */
    @Override
    public List<UserRole> getUserIdsByRoleIds(List<Long> roleIds) {
        ValidatorUtil.notEmpty(roleIds);
        LambdaQueryWrapper<UserRole> wrapper = new LambdaQueryWrapper<UserRole>()
                .in(UserRole::getRoleId, roleIds);
        return list(wrapper);
    }

    /**
     * 批量添加用户角色关联信息
     *
     * @param userRoles
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addBatchUserRole(List<UserRole> userRoles) {
        ValidatorUtil.notEmpty(userRoles);
        ValidatorUtil.isTrue(saveBatch(userRoles), ResultCodeEnum.DATA_SAVE_ERROR);
        return true;
    }

    /**
     * 根据角色id集合批量删除关联信息
     *
     * @param roleIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteUserRoleByRoleIds(List<Long> roleIds) {
        ValidatorUtil.notEmpty(roleIds);
        LambdaQueryWrapper<UserRole> wrapper = new LambdaQueryWrapper<UserRole>()
                .in(UserRole::getRoleId, roleIds);
        ValidatorUtil.isTrue(remove(wrapper), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 用户角色关联信息
     *
     * @param userId
     * @param roleId
     * @return top.horsemuzi.system.pojo.dto.user.UserRoleDTO
     * @author mabin
     * @date 2023/4/18 9:58
     **/
    @Transactional(readOnly = true)
    @Override
    public UserRoleDTO getUserRole(Long userId, Long roleId) {
        ValidatorUtil.allNotEmpty(userId, roleId);
        LambdaQueryWrapper<UserRole> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(UserRole::getUserId, userId);
        wrapper.eq(UserRole::getRoleId, roleId);
        return getOne(wrapper, UserRoleDTO.class);
    }

    /**
     * 批量更新用户角色关联信息
     *
     * @param userRoleDTOList
     * @return boolean
     * @author mabin
     * @date 2023/4/18 9:59
     **/
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateBatchUserRole(List<UserRoleDTO> userRoleDTOList) {
        ValidatorUtil.notEmpty(userRoleDTOList);
        ValidatorUtil.isTrue(updateBatchById(userRoleDTOList, UserRole.class), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 根据用户id获取用户角色关联信息
     *
     * @param userId
     * @return java.util.List<top.horsemuzi.system.pojo.dto.user.UserRoleDTO>
     * @author mabin
     * @date 2023/4/18 10:19
     **/
    @Transactional(readOnly = true)
    @Override
    public List<UserRoleDTO> getUserRoleByUserId(Long userId) {
        ValidatorUtil.notEmpty(userId);
        LambdaQueryWrapper<UserRole> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(UserRole::getUserId, userId);
        return list(wrapper, UserRoleDTO.class);
    }

}
