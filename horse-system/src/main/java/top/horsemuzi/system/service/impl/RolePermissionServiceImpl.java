package top.horsemuzi.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.mapper.RolePermissionMapper;
import top.horsemuzi.system.pojo.dto.role.RoleDTO;
import top.horsemuzi.system.pojo.entity.RolePermission;
import top.horsemuzi.system.service.RolePermissionService;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 角色权限表 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-07 22:44:27
 */
@Service
public class RolePermissionServiceImpl extends ExpandServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

    /**
     * 新增角色权限关联信息
     *
     * @param roleDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addRolePermission(RoleDTO roleDTO) {
        ValidatorUtil.notEmpty(roleDTO);
        ValidatorUtil.notEmpty(roleDTO.getId());
        ValidatorUtil.notEmpty(roleDTO.getPermissionIds());
        List<RolePermission> rolePermissions = new ArrayList<>();
        RolePermission rolePermission = null;
        for (Long permissionId : roleDTO.getPermissionIds()) {
            rolePermission = new RolePermission();
            rolePermission.setRoleId(roleDTO.getId()).setPermissionId(permissionId);
            rolePermissions.add(rolePermission);
        }
        ValidatorUtil.isTrue(saveBatch(rolePermissions), ResultCodeEnum.DATA_SAVE_ERROR);
        return true;
    }

    /**
     * 删除角色权限关联信息
     *
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteRolePermission(List<Long> roleIds) {
        ValidatorUtil.notEmpty(roleIds);
        LambdaQueryWrapper<RolePermission> wrapper = new LambdaQueryWrapper<RolePermission>()
                .in(RolePermission::getRoleId, roleIds);
        remove(wrapper);
        return true;
    }

    /**
     * 获取角色权限关联信息集合 (如果查询条件为空, 则返回全部角色权限关联数据)
     *
     * @return
     */
    @Override
    public List<RolePermission> getRolePermissions(RolePermission rolePermission) {
        if (ObjectUtil.isNull(rolePermission)) {
            return list();
        }
        LambdaQueryWrapper<RolePermission> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ObjectUtil.isNotNull(rolePermission.getRoleId()), RolePermission::getRoleId, rolePermission.getRoleId());
        wrapper.eq(ObjectUtil.isNotNull(rolePermission.getPermissionId()), RolePermission::getPermissionId, rolePermission.getPermissionId());
        return list(wrapper);
    }

}
