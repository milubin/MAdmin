package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.open.api.ApiDTO;
import top.horsemuzi.system.pojo.dto.open.api.ApiQuery;
import top.horsemuzi.system.pojo.entity.Api;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>
 * 开放API应用接口信息 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-18 22:09:28
 */
public interface ApiService extends IService<Api> {

    /**
     * 获取API接口信息
     *
     * @param apiCategoryIds
     * @return
     */
    List<ApiDTO> getApiByCategory(List<Long> apiCategoryIds);

    /**
     * 分页获取开放API接口列表
     *
     * @param query
     * @return
     */
    Page<ApiDTO> getPageApis(ApiQuery query);

    /**
     * 获取开放API接口详情
     *
     * @param id
     * @return
     */
    ApiDTO getApi(Long id);

    /**
     * 接口是否重复
     *
     * @param apiDTO
     * @return
     */
    boolean checkApiRepeat(ApiDTO apiDTO);

    /**
     * 保存API接口
     *
     * @param apiDTO
     * @return
     */
    boolean addApi(ApiDTO apiDTO);

    /**
     * 更新API接口
     *
     * @param apiDTO
     * @return
     */
    boolean updateApi(ApiDTO apiDTO);

    /**
     * 删除开放API接口
     *
     * @param apiIds
     * @return
     */
    boolean deleteApi(List<Long> apiIds);

    /**
     * 接口名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> fetch(String search);

    /**
     * API接口数据导出
     *
     * @param query
     * @param response
     */
    void export(ApiQuery query, HttpServletResponse response);

    /**
     * 获取API接口详情
     *
     * @param apiCode
     * @return
     */
    ApiDTO getApi(String apiCode);

    /**
     * 获取API接口列表
     *
     * @param apiIds
     * @return
     */
    List<ApiDTO> getApi(List<Long> apiIds);

    /**
     * 批量导入API接口数据
     *
     * @param file
     * @param jwtUser
     */
    void imports(MultipartFile file, JwtUser jwtUser);

    /**
     * 新增API接口
     *
     * @param apiDTOList
     * @return
     */
    boolean addApi(List<ApiDTO> apiDTOList);
}
