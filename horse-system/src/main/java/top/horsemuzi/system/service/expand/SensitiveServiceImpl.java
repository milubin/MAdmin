package top.horsemuzi.system.service.expand;

import cn.hutool.core.util.ObjectUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.system.manager.common.CommonManager;

/**
 * 数据脱敏业务处理
 *
 * @author mabin
 * @date 2022/09/29 11:41
 **/

@Service
@RequiredArgsConstructor
public class SensitiveServiceImpl implements SensitiveService {

    private final CommonManager commonManager;

    /**
     * 是否脱敏
     * 自定义是否脱敏的业务策略, 默认: 非管理员角色交互数据进行脱敏
     *
     * @return
     */
    @Override
    public boolean isSensitive() {
        JwtUser jwtUser = commonManager.getCacheJwtUser();
        return ObjectUtil.isNull(jwtUser) || !jwtUser.getAdmin();
    }

}
