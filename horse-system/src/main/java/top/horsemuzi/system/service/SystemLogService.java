package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.log.SystemLogDTO;
import top.horsemuzi.system.pojo.dto.log.SystemLogQuery;
import top.horsemuzi.system.pojo.entity.SystemLog;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统操作日志 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-06-30 20:35:31
 */
public interface SystemLogService extends IService<SystemLog> {

    /**
     * 分页获取系统日志列表
     *
     * @param query
     * @return
     */
    Page<SystemLogDTO> getPageLogs(SystemLogQuery query);

    /**
     * 获取系统日志详情
     *
     * @param id
     * @return
     */
    SystemLogDTO getLog(Long id);

    /**
     * 删除系统日志
     *
     * @param logIds
     * @return
     */
    boolean deleteLogs(List<Long> logIds);

    /**
     * 获取系统日志统计数据
     *
     * @return
     */
    Map<String, Object> statisticsLog();

    /**
     * 导出系统操作日志
     *
     * @param query
     * @param response
     */
    void export(SystemLogQuery query, HttpServletResponse response);

    /**
     * 保存日志
     *
     * @param systemLogDTO
     * @return
     */
    boolean saveLog(SystemLogDTO systemLogDTO);

    /**
     * 操作日志链路ID实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> logFetch(String search);
}
