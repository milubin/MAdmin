package top.horsemuzi.system.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.Header;
import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.framework.AopContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.AddressUtil;
import top.horsemuzi.common.util.NetUtil;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.core.office.service.OfficeService;
import top.horsemuzi.system.handle.excel.ExcelUserImportListener;
import top.horsemuzi.system.mapper.UserMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.user.UserDTO;
import top.horsemuzi.system.pojo.dto.user.UserImportDTO;
import top.horsemuzi.system.pojo.dto.user.UserQuery;
import top.horsemuzi.system.pojo.entity.User;
import top.horsemuzi.system.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 管理用户表 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-07 22:44:27
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl extends ExpandServiceImpl<UserMapper, User> implements UserService {

    /**
     * 更新用户登录信息
     *
     * @param userDTO
     * @param request
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateLoginUser(UserDTO userDTO, HttpServletRequest request) {
        UserServiceImpl proxy = (UserServiceImpl) AopContext.currentProxy();
        proxy.asyncUpdateLoginUser(userDTO, request);
    }

    /**
     * 异步更新用户登录信息
     *
     * @param userDTO
     * @param request
     */
    @Async("async")
    public void asyncUpdateLoginUser(UserDTO userDTO, HttpServletRequest request) {
        UserAgent agent = UserAgentUtil.parse(request.getHeader(Header.USER_AGENT.getValue()));
        String ip = NetUtil.getRequestIp(request);
        userDTO.setLastLoginIp(ip);
        userDTO.setLastLoginArea(AddressUtil.offLineByIp(ip));
        userDTO.setLastLoginTime(new Date());
        userDTO.setLoginCount(userDTO.getLoginCount() + 1);
        userDTO.setLastLoginClient(agent.getBrowser().getName() + " " + agent.getVersion());
        userDTO.setLastLoginOs(agent.getOs().getName());
        userDTO.setMobile(agent.isMobile() ? Constants.COMMON_JUDGMENT.ZONE_STR :
                Constants.COMMON_JUDGMENT.ONE_STR);
        ValidatorUtil.isTrue(updateById(OrikaUtil.convert(userDTO, User.class)), ResultCodeEnum.DATA_UPDATE_ERROR);
    }

    /**
     * 获取用户信息(用户名/手机号/邮箱)
     *
     * @param username
     * @return
     */
    @Override
    public UserDTO getUserByLogin(String username) {
        ValidatorUtil.notEmpty(username);
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getUsername, username)
                .or().eq(User::getPhone, username)
                .or().eq(User::getEmail, username);
        return OrikaUtil.convert(getOne(queryWrapper), UserDTO.class);
    }

    /**
     * 获取用户列表-用户管理(密码字段剔除)
     *
     * @param userQuery
     * @return
     */
    @Override
    public Page<UserDTO> getPageUsers(UserQuery userQuery) {
        Page<User> page = page(new Page<>(userQuery.getPage(), userQuery.getSize()), generateQueryWrapper(userQuery));
        return User.convertPage(page, UserDTO.class);
    }

    /**
     * 新增用户-用户管理
     *
     * @param userDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addUser(UserDTO userDTO) {
        ValidatorUtil.notEmpty(userDTO);
        User user = OrikaUtil.convert(userDTO, User.class);
        ValidatorUtil.isTrue(save(user), ResultCodeEnum.DATA_SAVE_ERROR);
        userDTO.setId(user.getId());
        return true;
    }

    /**
     * 更新用户-用户管理
     *
     * @param userDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateUser(UserDTO userDTO) {
        ValidatorUtil.notEmpty(userDTO);
        ValidatorUtil.notEmpty(userDTO.getId());
        ValidatorUtil.isTrue(updateById(OrikaUtil.convert(userDTO, User.class)), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 更新用户在线时长
     *
     * @param id
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateUserOnlineTime(Long id) {
        UserServiceImpl proxy = (UserServiceImpl) AopContext.currentProxy();
        proxy.asyncUpdateUserOnlineTime(id);
    }

    /**
     * 异步更新用户在线时长
     *
     * @param id
     */
    @Async("async")
    public void asyncUpdateUserOnlineTime(Long id) {
        ValidatorUtil.notEmpty(id);
        UserDTO userDTO = getUser(id);
        ValidatorUtil.notEmpty(userDTO, ResultCodeEnum.USER_NOT_EXIST);
        // 计算在线时长
        long onlineTime = Math.abs(DateUtil.between(userDTO.getLastLoginTime(), new Date(), DateUnit.SECOND));
        userDTO.setTotalOnlineTime(userDTO.getTotalOnlineTime() + onlineTime);
        ValidatorUtil.isTrue(updateById(userDTO, User.class), ResultCodeEnum.DATA_UPDATE_ERROR);
    }

    /**
     * 批量删除用户-用户管理
     *
     * @param userIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteUsers(List<Long> userIds) {
        ValidatorUtil.notEmpty(userIds);
        ValidatorUtil.isTrue(removeBatchByIds(userIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 导出数据-用户管理
     *
     * @param userQuery
     * @param response
     */
    @Override
    public void export(UserQuery userQuery, HttpServletResponse response) {
        List<UserDTO> userDTOList = list(generateQueryWrapper(userQuery), UserDTO.class);
        try {
            // 设置文件名(目录名称-yyyyMMddHHmmss)
            if (CollectionUtil.isNotEmpty(userDTOList)) {
                String fileName = "用户信息-" + DateUtil.format(new Date(), DatePattern.PURE_DATETIME_PATTERN);
                OfficeService.EXCEL.export(userDTOList, fileName, "EAdmin用户信息", ExcelTypeEnum.XLSX, response);
            }
        } catch (Exception e) {
            throw new BusinessException(e, ResultCodeEnum.FILE_EXPORT_ERROR);
        }
    }

    /**
     * 导入数据-用户管理
     *
     * @param file    表单文件
     * @param flag    是否同步更新已存在用户信息标识: 0-不更新, 1-更新
     * @param jwtUser 当前操作用户基础信息
     */
    @Override
    public void imports(MultipartFile file, Integer flag, JwtUser jwtUser) {
        ValidatorUtil.allNotEmpty(file, flag);
        try {
            InputStream inputStream = file.getInputStream();
            OfficeService.EXCEL.importExcel(inputStream, UserImportDTO.class, new ExcelUserImportListener(flag, jwtUser));
        } catch (Exception e) {
            throw new BusinessException(e, ResultCodeEnum.FILE_IMPORT_ERROR);
        }
    }

    /**
     * 获取用户信息-用户管理(体现于用户新增、更新逻辑)
     *
     * @param userDTO
     * @return
     */
    @Override
    public UserDTO getExistUser(UserDTO userDTO) {
        ValidatorUtil.notEmpty(userDTO);
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.ne(Objects.nonNull(userDTO.getId()), User::getId, userDTO.getId())
                .and(wrapper -> wrapper.eq(StrUtil.isNotBlank(userDTO.getUsername()), User::getUsername, userDTO.getUsername())
                        .or()
                        .eq(StrUtil.isNotBlank(userDTO.getPhone()), User::getPhone, userDTO.getPhone())
                        .or()
                        .eq(StrUtil.isNotBlank(userDTO.getEmail()), User::getEmail, userDTO.getEmail()));
        return OrikaUtil.convert(getOne(queryWrapper), UserDTO.class);
    }

    /**
     * 获取用户信息
     *
     * @param userIds
     * @return
     */
    @Override
    public List<UserDTO> getUsers(List<Long> userIds) {
        ValidatorUtil.notEmpty(userIds);
        List<User> users = listByIds(userIds);
        return OrikaUtil.convertList(users, UserDTO.class);
    }

    /**
     * 批量更新用户信息
     *
     * @param userDTOList
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateBatchUser(List<UserDTO> userDTOList) {
        ValidatorUtil.notEmpty(userDTOList);
        ValidatorUtil.isTrue(updateBatchById(userDTOList, User.class), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 获取全部用户列表
     *
     * @param userQuery
     * @return
     */
    @Override
    public List<UserDTO> getUsers(UserQuery userQuery) {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getStatus, Constants.COMMON_JUDGMENT.ZONE_STR);
        if (ObjectUtil.isNotNull(userQuery) && StrUtil.isNotBlank(userQuery.getUsername())) {
            wrapper.and(andWrapper ->
                    andWrapper.like(User::getUsername, userQuery.getUsername())
                            .or()
                            .like(User::getPinyinUsername, userQuery.getUsername())
                            .or()
                            .like(User::getPinyinAcronymUsername, userQuery.getUsername())
                            .or()
                            .like(User::getPhone, userQuery.getUsername())
                            .or()
                            .like(User::getEmail, userQuery.getUsername()));
        }
        return list(wrapper, UserDTO.class);
    }

    /**
     * 获取用户信息-用户管理
     *
     * @param userId
     * @return
     */
    @Override
    public UserDTO getUser(Long userId) {
        ValidatorUtil.notEmpty(userId, ResultCodeEnum.OBJECT_ID_IS_NULL);
        return getById(userId, UserDTO.class);
    }

    /**
     * 用户名输入框实时检索
     *
     * @param search
     * @param filedType
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<InputRemoteFetchDTO> fetch(String search, String filedType) {
        ValidatorUtil.notBlank(filedType);
        if (StrUtil.isNotBlank(search)) {
            return baseMapper.fetch(search, filedType);
        }
        return Collections.emptyList();
    }

    /**
     * 组装用户查询的wrapper (用户查询,导出)
     *
     * @param userQuery
     * @return
     */
    private LambdaQueryWrapper<User> generateQueryWrapper(UserQuery userQuery) {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        if (StrUtil.isNotBlank(userQuery.getUsername())) {
            queryWrapper.and(wrapper ->
                    wrapper.like(User::getUsername, userQuery.getUsername()).or()
                            .like(User::getPinyinUsername, userQuery.getUsername()).or()
                            .like(User::getPinyinAcronymUsername, userQuery.getUsername())
            );
        }
        queryWrapper.eq(StrUtil.isNotBlank(userQuery.getIp()), User::getLastLoginIp, userQuery.getIp())
                .eq(StrUtil.isNotBlank(userQuery.getStatus()), User::getStatus, userQuery.getStatus())
                .like(StrUtil.isNotBlank(userQuery.getPhone()), User::getPhone, userQuery.getPhone())
                .like(StrUtil.isNotBlank(userQuery.getEmail()), User::getEmail, userQuery.getEmail())
                .like(StrUtil.isNotBlank(userQuery.getArea()), User::getLastLoginArea, userQuery.getArea())
                .in(CollectionUtil.isNotEmpty(userQuery.getUserIds()), User::getId, userQuery.getUserIds())
                .ge(ObjectUtil.isNotNull(userQuery.getCreateStart()), User::getGmtCreate, userQuery.getCreateStart())
                .le(ObjectUtil.isNotNull(userQuery.getCreateEnd()), User::getGmtCreate, userQuery.getCreateEnd())
                .orderByDesc(User::getGmtCreate);
        return queryWrapper;
    }

}
