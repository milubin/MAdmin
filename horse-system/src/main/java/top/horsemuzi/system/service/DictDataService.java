package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import top.horsemuzi.system.pojo.dto.dict.DictDataDTO;
import top.horsemuzi.system.pojo.dto.dict.DictDataQuery;
import top.horsemuzi.system.pojo.entity.DictData;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 字典数据表 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-06-20 11:04:36
 */
public interface DictDataService extends IService<DictData> {

    /**
     * 获取字典数据
     *
     * @param queryDictDataDTO
     * @return
     */
    DictDataDTO getDictData(DictDataDTO queryDictDataDTO);

    /**
     * 获取字典数据列表
     *
     * @param queryDictDataDTO
     * @return
     */
    List<DictDataDTO> getDictDatas(DictDataDTO queryDictDataDTO);

    /**
     * 保存字典数据
     *
     * @param dictDataDTO
     * @return
     */
    boolean addDictData(DictDataDTO dictDataDTO);

    /**
     * 更新对应类型的字典数据
     *
     * @param dictDataDTO
     * @return
     */
    boolean updateDictData(DictDataDTO dictDataDTO);

    /**
     * 获取字典数据
     *
     * @param dictDataIds
     * @return
     */
    List<DictDataDTO> getDictDatas(List<Long> dictDataIds);

    /**
     * 删除字典数据
     *
     * @param dictDataIds
     * @return
     */
    boolean deleteDictDatas(List<Long> dictDataIds);

    /**
     * 分页获取字典数据列表
     *
     * @param dictDataQuery
     * @return
     */
    Page<DictDataDTO> getPageDictDatas(DictDataQuery dictDataQuery);

    /**
     * 根据id获取字典数据详情
     *
     * @param id
     * @return
     */
    DictDataDTO getDictData(Long id);

    /**
     * 校验字典数据是否重复, 字典数据默认属性是否重复
     *
     * @param queryDictDataDTO
     * @return
     */
    Boolean checkDictData(DictDataDTO queryDictDataDTO);
}
