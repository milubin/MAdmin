package top.horsemuzi.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.db.Entity;
import cn.hutool.db.StatementUtil;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.EncryptUtil;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.mapper.DatasourceMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.generate.*;
import top.horsemuzi.system.pojo.entity.Datasource;
import top.horsemuzi.system.service.DatasourceService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 数据源信息 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-12-19 13:48:53
 */
@Slf4j
@Service
public class DatasourceServiceImpl extends ExpandServiceImpl<DatasourceMapper, Datasource> implements DatasourceService {

    /**
     * 分页获取数据源列表
     *
     * @param query
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public Page<DatasourceDTO> getPageDatasource(DataSourceQuery query) {
        Page<Datasource> page = new Page<>(query.getPage(), query.getSize());
        LambdaQueryWrapper<Datasource> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StrUtil.isNotBlank(query.getDbName()), Datasource::getDbName, query.getDbName())
                .ge(ObjectUtil.isNotNull(query.getCreateStart()), Datasource::getGmtCreate, query.getCreateStart())
                .le(ObjectUtil.isNotNull(query.getCreateEnd()), Datasource::getGmtCreate, query.getCreateEnd())
                .orderByDesc(Datasource::getGmtCreate);
        return Datasource.convertPage(page(page, wrapper), DatasourceDTO.class);
    }

    /**
     * 获取数据源详情
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public DatasourceDTO getDatasource(Long id) {
        ValidatorUtil.notEmpty(id);
        return getById(id, DatasourceDTO.class);
    }

    /**
     * 获取数据源列表
     *
     * @param datasourceIds
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<DatasourceDTO> getDatasource(List<Long> datasourceIds) {
        ValidatorUtil.notEmpty(datasourceIds);
        return listByIds(datasourceIds, DatasourceDTO.class);
    }

    /**
     * 数据源是否重复
     *
     * @param datasourceDTO
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public boolean checkDatasourceRepeat(DatasourceDTO datasourceDTO) {
        LambdaQueryWrapper<Datasource> wrapper = new LambdaQueryWrapper<>();
        if (ObjectUtil.isNotNull(datasourceDTO)) {
            wrapper.ne(ObjectUtil.isNotNull(datasourceDTO.getId()), Datasource::getId, datasourceDTO.getId())
                    .eq(StrUtil.isNotBlank(datasourceDTO.getDbName()), Datasource::getDbName, datasourceDTO.getDbName());
        }
        return ObjectUtil.isNull(getOne(wrapper));
    }

    /**
     * 新增数据源
     *
     * @param datasourceDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addDataSource(DatasourceDTO datasourceDTO) {
        ValidatorUtil.notEmpty(datasourceDTO);
        Datasource datasource = OrikaUtil.convert(datasourceDTO, Datasource.class);
        ValidatorUtil.isTrue(save(datasource), ResultCodeEnum.DATA_SAVE_ERROR);
        datasourceDTO.setId(datasource.getId());
        return true;
    }

    /**
     * 删除数据源
     *
     * @param datasourceIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteDataSource(List<Long> datasourceIds) {
        ValidatorUtil.notEmpty(datasourceIds);
        ValidatorUtil.isTrue(removeBatchByIds(datasourceIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 获取数据源的所有数据库名称
     *
     * @param datasourceDTO
     * @return
     */
    @Override
    public List<String> getDatabases(DatasourceDTO datasourceDTO) {
        try(Connection connection = connectDataSource(datasourceDTO)) {
            List<Entity> entityList = SqlExecutor.query(connection, "SHOW DATABASES;", new EntityListHandler());
            if (CollUtil.isNotEmpty(entityList)) {
                return entityList.stream()
                        .map(entity -> entity.getStr("Database"))
                        .filter(database -> !StrUtil.equalsAny(database, "mysql", "sys", "nacos", "information_schema", "performance_schema"))
                        .collect(Collectors.toList());
            }
            return Collections.emptyList();
        } catch (Exception e) {
            throw new BusinessException(e, ResultCodeEnum.GENERATE_DATASOURCE_EXECUTE_ERROR);
        }
    }

    /**
     * 获取数据源的所有数据库名称
     *
     * @param datasourceId
     * @return
     */
    @Override
    public List<String> getDatabases(Long datasourceId) {
        ValidatorUtil.notEmpty(datasourceId);
        DatasourceDTO datasourceDTO = getDatasource(datasourceId);
        ValidatorUtil.notEmpty(datasourceDTO, ResultCodeEnum.GENERATE_DATASOURCE_NOT_EXIST_ERROR);
        return getDatabases(datasourceDTO);
    }

    /**
     * 连接数据源
     *
     * @param datasourceDTO
     * @return
     */
    @Override
    public Connection connectDataSource(DatasourceDTO datasourceDTO) {
        try {
            Class.forName(datasourceDTO.getDbDriver());
            return DriverManager.getConnection(datasourceDTO.getConnUrl(), datasourceDTO.getConnUser(), datasourceDTO.getConnPassword());
        } catch (Exception e) {
            throw new BusinessException(e, ResultCodeEnum.GENERATE_DATASOURCE_CONNECT_ERROR);
        }
    }

    /**
     * 获取数据库所有表信息(排除QRTZ调度表、gen代码生成器表、flyway_history库版本表)
     *
     * @param database
     * @param datasourceId
     * @return
     */
    @Override
    public List<SchemaTableDTO> getSchemaTables(String database, Long datasourceId) {
        DatasourceDTO datasourceDTO = getDatasource(datasourceId);
        ValidatorUtil.notEmpty(datasourceDTO, ResultCodeEnum.GENERATE_DATASOURCE_NOT_EXIST_ERROR);
        try (Connection connection = connectDataSource(datasourceDTO)) {
            String sql = "SELECT st.TABLE_NAME    as tableName,\n" +
                    "       st.TABLE_COMMENT as tableComment\n" +
                    "FROM information_schema.TABLES st\n" +
                    "WHERE TABLE_SCHEMA = ?\n" +
                    "  and st.TABLE_NAME not like concat('QRTZ', '%')\n" +
                    "  and st.TABLE_NAME not like concat('gen', '%')\n" +
                    "  and st.TABLE_NAME != 'flyway_history'";
            List<Entity> entityList = SqlExecutor.query(connection, sql, new EntityListHandler(), database);
            if (CollUtil.isNotEmpty(entityList)) {
                List<SchemaTableDTO> schemaTableDTOList = new ArrayList<>();
                SchemaTableDTO schemaTableDTO = null;
                for (Entity entity : entityList) {
                    schemaTableDTO = new SchemaTableDTO();
                    schemaTableDTO.setTableName(entity.getStr("tableName"));
                    schemaTableDTO.setTableComment(entity.getStr("tableComment"));
                    schemaTableDTOList.add(schemaTableDTO);
                }
                return schemaTableDTOList;
            }
            return Collections.emptyList();
        } catch (Exception e) {
            throw new BusinessException(e, ResultCodeEnum.GENERATE_DATASOURCE_EXECUTE_ERROR);
        }
    }

    /**
     * 数据源名称实时检索
     *
     * @param search
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<InputRemoteFetchDTO> fetchDatasource(String search) {
        return StrUtil.isBlank(search) ? Collections.emptyList() : baseMapper.fetchDatasource(search);
    }

    /**
     * 获取数据源列表
     *
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<DatasourceDTO> getDatasourceList() {
        return list(DatasourceDTO.class);
    }

    /**
     * 执行DDL建表语句
     *
     * @param datasourceId 数据源id
     * @param databaseName 数据库名称
     * @param tableDdl     DDL语句
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean execTableDDL(Long datasourceId, String databaseName, String tableDdl) {
        ValidatorUtil.allNotEmpty(datasourceId, databaseName, tableDdl);
        DatasourceDTO datasourceDTO = getDatasource(datasourceId);
        ValidatorUtil.notEmpty(datasourceDTO, ResultCodeEnum.GENERATE_DATASOURCE_NOT_EXIST_ERROR);
        try (Connection connection = connectDataSource(datasourceDTO)) {
            // 用于执行 INSERT、UPDATE 或 DELETE 语句以及 SQL DDL（数据定义语言）语句，例如 CREATE TABLE 和 DROP TABLE
            // INSERT、UPDATE 或 DELETE 语句的效果是修改表中零行或多行中的一列或多列
            // executeUpdate 的返回值是一个整数（int），指示受影响的行数（即更新计数）
            // 对于 CREATE TABLE 或 DROP TABLE 等不操作行的语句，executeUpdate 的返回值总为零
            SqlExecutor.executeUpdate(StatementUtil.prepareStatement(connection, tableDdl));
            return true;
        } catch (Exception e) {
            throw new BusinessException(e, ResultCodeEnum.GENERATE_DATASOURCE_EXECUTE_ERROR);
        }
    }

    /**
     * 获取数据库业务表所有列字段详情列表
     *
     * @param datasourceId 数据源id
     * @param database     数据库名称
     * @param tableName    表名称
     * @return
     */
    @Override
    public List<SchemaColumnDTO> getSchemaColumns(Long datasourceId, String database, String tableName) {
        ValidatorUtil.allNotEmpty(datasourceId, database, tableName);
        DatasourceDTO datasourceDTO = getDatasource(datasourceId);
        ValidatorUtil.notEmpty(datasourceDTO, ResultCodeEnum.GENERATE_DATASOURCE_NOT_EXIST_ERROR);
        try (Connection connection = connectDataSource(datasourceDTO)) {
            String sql = "SELECT TABLE_SCHEMA AS tableSchema,\n" +
                    "       TABLE_NAME AS tableName,\n" +
                    "       COLUMN_NAME AS columnName,\n" +
                    "       ORDINAL_POSITION AS ordinalPosition,\n" +
                    "       COLUMN_DEFAULT AS columnDefault,\n" +
                    "       IS_NULLABLE AS isNullable,\n" +
                    "       DATA_TYPE AS dataType,\n" +
                    "       CHARACTER_MAXIMUM_LENGTH AS characterMaximumLength,\n" +
                    "       COLUMN_TYPE AS columnType,\n" +
                    "       COLUMN_KEY AS columnKey,\n" +
                    "       COLUMN_COMMENT AS columnComment\n" +
                    "  FROM information_schema.columns\n" +
                    " WHERE table_name = ?\n" +
                    "   AND table_schema = ?;";
            List<Entity> entityList = SqlExecutor.query(connection, sql, new EntityListHandler(), tableName, database);
            if (CollUtil.isNotEmpty(entityList)) {
                List<SchemaColumnDTO> schemaColumnDTOList = new ArrayList<>();
                SchemaColumnDTO schemaColumnDTO = null;
                for (Entity entity : entityList) {
                    schemaColumnDTO = new SchemaColumnDTO();
                    schemaColumnDTO.setTableSchema(entity.getStr("tableSchema"));
                    schemaColumnDTO.setTableName(entity.getStr("tableName"));
                    schemaColumnDTO.setColumnName(entity.getStr("columnName"));
                    schemaColumnDTO.setOrdinalPosition(entity.getInt("ordinalPosition"));
                    schemaColumnDTO.setColumnDefault(entity.getStr("columnDefault"));
                    schemaColumnDTO.setIsNullable(entity.getStr("isNullable"));
                    schemaColumnDTO.setDataType(entity.getStr("dataType"));
                    schemaColumnDTO.setCharacterMaximumLength(entity.getInt("characterMaximumLength"));
                    schemaColumnDTO.setColumnType(entity.getStr("columnType"));
                    schemaColumnDTO.setColumnKey(entity.getStr("columnKey"));
                    schemaColumnDTO.setColumnComment(entity.getStr("columnComment"));
                    schemaColumnDTOList.add(schemaColumnDTO);
                }
                return schemaColumnDTOList;
            }
            return Collections.emptyList();
        } catch (Exception e) {
            throw new BusinessException(e, ResultCodeEnum.GENERATE_DATASOURCE_EXECUTE_ERROR);
        }
    }

    /**
     * 更新数据源
     *
     * @param datasourceDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateDataSource(DatasourceDTO datasourceDTO) {
        ValidatorUtil.notEmpty(datasourceDTO);
        ValidatorUtil.notEmpty(datasourceDTO.getId());
        ValidatorUtil.isTrue(updateById(datasourceDTO, Datasource.class), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 获取建表的DDL语句
     *
     * @return
     */
    @Override
    public String getTableDDL(TableDTO tableDTO) {
        ValidatorUtil.notEmpty(tableDTO);
        DatasourceDTO datasourceDTO = getDatasource(tableDTO.getDatasourceId());
        ValidatorUtil.notEmpty(datasourceDTO, ResultCodeEnum.GENERATE_DATASOURCE_NOT_EXIST_ERROR);
        // 更新数据源链接为执行数据库
        if (!StrUtil.contains(datasourceDTO.getConnUrl(), tableDTO.getDatabaseName())) {
            datasourceDTO.setConnUrl(datasourceDTO.getConnUrl() + "/" + tableDTO.getDatabaseName());
        }
        // 添加数据库连接配置
        if (StrUtil.isNotBlank(datasourceDTO.getUrlConfig())) {
            datasourceDTO.setConnUrl(datasourceDTO.getConnUrl() + (StrUtil.startWith(datasourceDTO.getUrlConfig(), "?")
                    ? datasourceDTO.getUrlConfig() : "?" + datasourceDTO.getUrlConfig()));
        }
        try (Connection connection = connectDataSource(datasourceDTO)) {
            String sql = "show create table " + tableDTO.getTableName();
            List<Entity> entityList = SqlExecutor.query(connection, sql, new EntityListHandler());
            if (CollUtil.isNotEmpty(entityList)) {
                return entityList.get(0).getStr("Create Table");
            }
            return StrUtil.EMPTY;
        } catch (Exception e) {
            throw new BusinessException(e, ResultCodeEnum.GENERATE_DATASOURCE_EXECUTE_ERROR);
        }
    }

}
