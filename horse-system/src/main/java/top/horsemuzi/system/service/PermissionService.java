package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.login.RouterDTO;
import top.horsemuzi.system.pojo.dto.permission.PermissionDTO;
import top.horsemuzi.system.pojo.dto.permission.PermissionQuery;
import top.horsemuzi.system.pojo.entity.Permission;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 菜单权限表 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-07 22:44:27
 */
public interface PermissionService extends IService<Permission> {

    /**
     * 根据用户id获取用户权限标识信息
     *
     * @param userId
     * @return
     */
    Set<String> getPerms(Long userId);


    /**
     * 根据用户信息获取Tree接口的菜单路由
     *
     * @param jwtUser
     * @return
     */
    List<RouterDTO> getUserRouters(JwtUser jwtUser);

    /**
     * 获取条件下权限菜单列表
     *
     * @param query
     * @return
     */
    List<PermissionDTO> getPermissionMenus(PermissionQuery query);

    /**
     * 根据权限菜单id获取权限菜单详情
     *
     * @param id
     * @return
     */
    PermissionDTO getPermission(Long id);

    /**
     * 校验权限菜单名称是否唯一
     *
     * @param permissionDTO
     * @return
     */
    boolean checkPermissionNameUnique(PermissionDTO permissionDTO);

    /**
     * 新增权限菜单
     *
     * @param permissionDTO
     */
    boolean addPermission(PermissionDTO permissionDTO);

    /**
     * 根据角色id获取权限菜单数据集合
     *
     * @param roleId
     * @return
     */
    List<PermissionDTO> getPermissionMenusByRoleId(Long roleId);

    /**
     * 获取权限菜单列表
     *
     * @param permissionIds
     * @return
     */
    List<PermissionDTO> getPermissions(List<Long> permissionIds);

    /**
     * 更新权限菜单
     *
     * @param permissionDTO
     * @return
     */
    boolean updatePermission(PermissionDTO permissionDTO);

    /**
     * 获取权限菜单信息集合
     *
     * @param parentId
     * @return
     */
    List<PermissionDTO> getPermissionByParentId(Long parentId);

    /**
     * 删除权限菜单
     *
     * @param id
     * @return
     */
    boolean deletePermission(Long id);

    /**
     * 菜单名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> fetch(String search);

    /**
     * 获取权限菜单最大主键值
     *
     * @return
     */
    Long getMaxMenuId();

    /**
     * 获取父权限菜单列表(目录、菜单), 不包含按钮数据(主要用于代码生成选择父权限菜单)
     *
     * @return
     */
    List<PermissionDTO> getParentPermissions();

}
