package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.open.app.AppDTO;
import top.horsemuzi.system.pojo.dto.open.app.AppQuery;
import top.horsemuzi.system.pojo.dto.open.category.ApiCategoryDTO;
import top.horsemuzi.system.pojo.entity.App;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>
 * 开放API应用信息 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-18 22:09:28
 */
public interface AppService extends IService<App> {

    /**
     * 分页获取开放API应用列表
     *
     * @param query
     * @return
     */
    Page<AppDTO> getPageApps(AppQuery query);

    /**
     * 获取开放API应用详情
     *
     * @param id
     * @return
     */
    AppDTO getApp(Long id);

    /**
     * 获取开放API应用列表
     *
     * @param appIds
     * @return
     */
    List<AppDTO> getApp(List<Long> appIds);

    /**
     * 开放API应用名称是否重复
     *
     * @param appDTO
     * @return
     */
    boolean checkAppRepeat(AppDTO appDTO);

    /**
     * 保存开放API应用
     *
     * @param appDTO
     * @return
     */
    boolean addApp(AppDTO appDTO);

    /**
     * 更新开放API应用
     *
     * @param appDTO
     * @return
     */
    boolean updateApp(AppDTO appDTO);

    /**
     * 删除开放API应用
     *
     * @param appIds
     * @return
     */
    boolean deleteApp(List<Long> appIds);

    /**
     * 应用名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> fetch(String search);

    /**
     * API应用数据导出
     *
     * @param query
     * @param response
     */
    void export(AppQuery query, HttpServletResponse response);

    /**
     * 获取API应用信息
     *
     * @param appKey
     * @return
     */
    AppDTO getApp(String appKey);
}
