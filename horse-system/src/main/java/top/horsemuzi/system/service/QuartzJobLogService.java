package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobLogDTO;
import top.horsemuzi.system.pojo.dto.statistics.LineChartDTO;
import top.horsemuzi.system.pojo.dto.statistics.PieChartDTO;
import top.horsemuzi.system.pojo.dto.statistics.SourceLineData;
import top.horsemuzi.system.pojo.entity.QuartzJobLog;

import java.util.List;

/**
 * <p>
 * Quartz任务调度日志信息表 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-08-21 16:05:01
 */
public interface QuartzJobLogService extends IService<QuartzJobLog> {

    /**
     * 保存调度任务执行日志
     *
     * @param quartzJobLogDTO
     * @return
     */
    Boolean saveJobLog(QuartzJobLogDTO quartzJobLogDTO);

    /**
     * 清理Quartz任务调度日志信息
     *
     * @param quartzLogIds
     * @return
     */
    boolean deleteLog(List<Long> quartzLogIds);

    /**
     * 获取Quartz任务调度日志饼图统计数据
     *
     * @return
     */
    List<PieChartDTO> statisticsPieChartData();

    /**
     * 获取Quartz任务调度日志折线图统计数据
     *
     * @return
     */
    List<SourceLineData> statisticsLineChartData();

}
