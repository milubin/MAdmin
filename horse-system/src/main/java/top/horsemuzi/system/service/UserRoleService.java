package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.user.UserRoleDTO;
import top.horsemuzi.system.pojo.entity.UserRole;

import java.util.List;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-07 22:44:28
 */
public interface UserRoleService extends IService<UserRole> {

    /**
     * 删除关联的用户角色信息
     *
     * @param userIds
     * @return
     */
    boolean deleteUserRoleByUserIds(List<Long> userIds);

    /**
     * 根据角色id集合获取对应的用户id集合
     *
     * @param roleIds
     * @return
     */
    List<UserRole> getUserIdsByRoleIds(List<Long> roleIds);

    /**
     * 批量添加用户角色关联信息
     *
     * @param userRoles
     * @return
     */
    boolean addBatchUserRole(List<UserRole> userRoles);

    /**
     * 根据角色id集合批量删除关联信息
     *
     * @param roleIds
     * @return
     */
    boolean deleteUserRoleByRoleIds(List<Long> roleIds);

    /**
     * 用户角色关联信息
     *
     * @param userId
     * @param roleId
     * @return top.horsemuzi.system.pojo.dto.user.UserRoleDTO
     * @author mabin
     * @date 2023/4/18 9:58
     **/
    UserRoleDTO getUserRole(Long userId, Long roleId);

    /**
     * 批量更新用户角色关联信息
     *
     * @param userRoleDTOList
     * @return boolean
     * @author mabin
     * @date 2023/4/18 9:59
     **/
    boolean updateBatchUserRole(List<UserRoleDTO> userRoleDTOList);

    /**
     * 根据用户id获取用户角色关联信息
     *
     * @param userId
     * @return java.util.List<top.horsemuzi.system.pojo.dto.user.UserRoleDTO>
     * @author mabin
     * @date 2023/4/18 10:19
     **/
    List<UserRoleDTO> getUserRoleByUserId(Long userId);
}
