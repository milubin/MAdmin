package top.horsemuzi.system.service;

/**
 * API外部接口核心服务
 *
 * @author mabin
 * @date 2022/11/24 10:18
 **/

public interface ApiCoreService {

    /**
     * 获取ip信息
     *
     * @param data
     * @return
     */
    Object getIp(String data);

}
