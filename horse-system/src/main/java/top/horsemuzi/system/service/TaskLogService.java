package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskLogDTO;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskLogQuery;
import top.horsemuzi.system.pojo.dto.statistics.PieChartDTO;
import top.horsemuzi.system.pojo.dto.statistics.SourceLineData;
import top.horsemuzi.system.pojo.entity.TaskLog;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-08-20 17:56:39
 */
public interface TaskLogService extends IService<TaskLog> {

    /**
     * 分页获取Spring任务调度日志列表
     *
     * @param query
     * @return
     */
    Page<TaskLogDTO> getPageTaskLog(TaskLogQuery query);

    /**
     * 新增Spring调度日志
     *
     * @param taskLogDTO
     * @return
     */
    boolean addTaskLog(TaskLogDTO taskLogDTO);

    /**
     * 清理Spring任务调度日志信息
     *
     * @param taskLogIds
     * @return
     */
    Boolean clearTaskLogs(List<Long> taskLogIds);

    /**
     * 获取Spring调度任务日志的饼图统计数据
     *
     * @return
     */
    List<PieChartDTO> statisticsPieChartData();

    /**
     * 获取Spring调度任务日志的折线图统计数据
     *
     * @return
     */
    List<SourceLineData> statisticsLineChartData();

    /**
     * 导出Spring调度任务日志
     *
     * @param query
     * @param response
     */
    void exportTaskLog(TaskLogQuery query, HttpServletResponse response);
}
