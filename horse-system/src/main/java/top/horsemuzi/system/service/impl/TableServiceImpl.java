package top.horsemuzi.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.mapper.TableMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.generate.TableDTO;
import top.horsemuzi.system.pojo.dto.generate.TableQuery;
import top.horsemuzi.system.pojo.entity.Table;
import top.horsemuzi.system.service.TableService;

import java.util.Collections;
import java.util.List;

/**
 * <p>
 * 代码生成业务表 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-12-19 13:48:53
 */
@Service
public class TableServiceImpl extends ExpandServiceImpl<TableMapper, Table> implements TableService {

    /**
     * 获取业务表详情
     *
     * @param query
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<TableDTO> getTable(TableQuery query) {
        if (ObjectUtil.isNull(query)) {
            return null;
        }
        LambdaQueryWrapper<Table> wrapper = new LambdaQueryWrapper<Table>()
                .in(CollUtil.isNotEmpty(query.getTableConfigIds()), Table::getTableConfigId, query.getTableConfigIds())
                .in(CollUtil.isNotEmpty(query.getDatasourceIds()), Table::getDatasourceId, query.getDatasourceIds())
                .like(StrUtil.isNotBlank(query.getTableName()), Table::getTableName, query.getTableName());
        return list(wrapper, TableDTO.class);
    }

    /**
     * 获取业务表信息
     *
     * @param tableId
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public TableDTO getTable(Long tableId) {
        ValidatorUtil.notEmpty(tableId);
        return getById(tableId, TableDTO.class);
    }

    /**
     * 获取业务表信息列表
     *
     * @param tableIds
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<TableDTO> getTable(List<Long> tableIds) {
        ValidatorUtil.notEmpty(tableIds);
        return listByIds(tableIds, TableDTO.class);
    }

    /**
     * 分页获取业务表列表
     *
     * @param query
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public Page<TableDTO> getPageTable(TableQuery query) {
        return baseMapper.getPageTable(new Page<>(query.getPage(), query.getSize()), query);
    }

    /**
     * 新增业务表
     *
     * @param tableDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addTable(TableDTO tableDTO) {
        ValidatorUtil.notEmpty(tableDTO);
        Table table = OrikaUtil.convert(tableDTO, Table.class);
        ValidatorUtil.isTrue(save(table), ResultCodeEnum.DATA_SAVE_ERROR);
        tableDTO.setId(table.getId());
        return true;
    }

    /**
     * 更新业务表
     *
     * @param tableDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateTable(TableDTO tableDTO) {
        ValidatorUtil.notEmpty(tableDTO);
        ValidatorUtil.notEmpty(tableDTO.getId());
        ValidatorUtil.isTrue(updateById(tableDTO, Table.class), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 业务表是否重复(表名)
     *
     * @param tableDTO
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public boolean checkTableRepeat(TableDTO tableDTO) {
        ValidatorUtil.notEmpty(tableDTO);
        LambdaQueryWrapper<Table> wrapper = new LambdaQueryWrapper<Table>()
                .ne(ObjectUtil.isNotNull(tableDTO.getId()), Table::getId, tableDTO.getId())
                .eq(StrUtil.isNotBlank(tableDTO.getTableName()), Table::getTableName, tableDTO.getTableName());
        return ObjectUtil.isNull(getOne(wrapper));
    }

    /**
     * 业务表名称实时检索
     *
     * @param search
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<InputRemoteFetchDTO> fetchTable(String search) {
        return StrUtil.isBlank(search) ? Collections.emptyList() : baseMapper.fetchTable(search);
    }

    /**
     * 删除业务表
     *
     * @param tableIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteTable(List<Long> tableIds) {
        ValidatorUtil.notEmpty(tableIds);
        ValidatorUtil.isTrue(removeBatchByIds(tableIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

}
