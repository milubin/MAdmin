package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.oss.OssDTO;
import top.horsemuzi.system.pojo.dto.oss.OssQuery;
import top.horsemuzi.system.pojo.entity.Oss;

import java.util.List;

/**
 * <p>
 * 对象存储文件信息表 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-08-01 21:50:47
 */
public interface OssService extends IService<Oss> {

    /**
     * 分页获取文件列表
     *
     * @param ossQuery
     * @return
     */
    Page<OssDTO> getPageOss(OssQuery ossQuery);

    /**
     * 保存文件信息
     *
     * @param ossDTO
     * @return
     */
    boolean saveOss(OssDTO ossDTO);

    /**
     * 获取文件信息
     *
     * @param id
     * @return
     */
    OssDTO getOss(Long id);

    /**
     * 获取文件信息列表
     *
     * @param ossIds
     * @return
     */
    List<OssDTO> getOss(List<Long> ossIds);

    /**
     * 文件删除
     *
     * @param ossIds
     * @return
     */
    boolean delete(List<Long> ossIds);

    /**
     * 文件更新
     *
     * @param currentOssDTO
     * @return
     */
    boolean updateOss(OssDTO currentOssDTO);

    /**
     * 文件名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> fetch(String search);
}
