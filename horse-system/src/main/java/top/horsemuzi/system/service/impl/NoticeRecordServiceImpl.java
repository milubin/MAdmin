package top.horsemuzi.system.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.mapper.NoticeRecordMapper;
import top.horsemuzi.system.pojo.dto.notice.NoticeRecordDTO;
import top.horsemuzi.system.pojo.dto.notice.NoticeRecordQuery;
import top.horsemuzi.system.pojo.dto.notice.NoticeSocketDTO;
import top.horsemuzi.system.pojo.entity.NoticeRecord;
import top.horsemuzi.system.service.NoticeRecordService;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 通知公告发布记录表 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-10-06 16:00:06
 */
@Service
public class NoticeRecordServiceImpl extends ExpandServiceImpl<NoticeRecordMapper, NoticeRecord> implements NoticeRecordService {

    /**
     * 新增通知公告发布记录
     *
     * @param noticeRecordDTOList
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addNoticeRecord(List<NoticeRecordDTO> noticeRecordDTOList) {
        ValidatorUtil.notEmpty(noticeRecordDTOList);
        List<NoticeRecord> noticeRecords = OrikaUtil.convertList(noticeRecordDTOList, NoticeRecord.class);
        ValidatorUtil.isTrue(saveBatch(noticeRecords), ResultCodeEnum.DATA_SAVE_ERROR);
        return true;
    }

    /**
     * 读取全部通知工作
     *
     * @param userId
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean readAllNoticeRecord(Long userId) {
        ValidatorUtil.notEmpty(userId);
        LambdaUpdateWrapper<NoticeRecord> wrapper = new LambdaUpdateWrapper<>();
        wrapper.set(NoticeRecord::getStatus, Constants.COMMON_JUDGMENT.ONE_STR)
                .set(NoticeRecord::getReadTime, new Date())
                .set(NoticeRecord::getRemark, "全部已读按钮操作")
                .eq(NoticeRecord::getUserId, userId);
        ValidatorUtil.isTrue(update(wrapper), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 删除指定通知公告
     *
     * @param noticeRecordId
     * @param userId
     */
    @Override
    public boolean deleteNoticeRecord(Long noticeRecordId, Long userId) {
        ValidatorUtil.allNotEmpty(noticeRecordId, userId);
        LambdaQueryWrapper<NoticeRecord> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(NoticeRecord::getId, noticeRecordId).eq(NoticeRecord::getUserId, userId);
        NoticeRecord noticeRecord = getOne(wrapper);
        ValidatorUtil.notEmpty(noticeRecord, ResultCodeEnum.NOTICE_RECORD_NOT_EXIST_ERROR);
        // 未读状态不允许删除
        ValidatorUtil.isTrue(StrUtil.equals(Constants.COMMON_JUDGMENT.ONE_STR, noticeRecord.getStatus()), ResultCodeEnum.NOTICE_RECORD_NOT_READ_ERROR);
        ValidatorUtil.isTrue(removeById(noticeRecord.getId()), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 移除发布记录
     *
     * @param noticeRecordId
     * @return
     */

    /**
     * 批量移除发布记录
     *
     * @param noticeRecordIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteNoticeRecord(List<Long> noticeRecordIds) {
        ValidatorUtil.notEmpty(noticeRecordIds);
        ValidatorUtil.isTrue(removeBatchByIds(noticeRecordIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 获取发布记录内容信息
     *
     * @param noticeRecordId
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public NoticeRecordDTO getNoticeRecord(Long noticeRecordId) {
        ValidatorUtil.notEmpty(noticeRecordId);
        return baseMapper.getNoticeRecord(noticeRecordId);
    }

    /**
     * 获取用户通知公告记录列表
     *
     * @param userId
     * @return
     */
    @Override
    public List<NoticeRecordDTO> getNoticeRecords(Long userId) {
        return baseMapper.getNoticeRecords(userId);
    }

    /**
     * 获取用户通知公告记录未读数量
     *
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<NoticeSocketDTO> getNoticeSocketList(List<Long> userIds) {
        ValidatorUtil.notEmpty(userIds);
        return baseMapper.getNoticeSocketList(userIds);
    }

    /**
     * 分页获取通知公告记录列表
     *
     * @param query
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public Page<NoticeRecordDTO> getPageNoticeRecords(NoticeRecordQuery query) {
        ValidatorUtil.notEmpty(query);
        ValidatorUtil.notEmpty(query.getNoticeId());
        Page<NoticeRecordDTO> page = new Page<>(query.getPage(), query.getSize());
        Long total = baseMapper.getPageNoticeRecordCount(query);
        if (total <= 0) {
            return page;
        }
        query.setPage((query.getPage() - 1) * query.getSize());
        List<NoticeRecordDTO> noticeRecordDTOList = baseMapper.getPageNoticeRecordList(query);
        page.setTotal(total);
        page.setRecords(noticeRecordDTOList);
        return page;
    }

    /**
     * 清理全部通知公告
     *
     * @param userId
     */
    @Override
    public boolean deleteAllNoticeRecord(Long userId) {
        ValidatorUtil.notEmpty(userId);
        LambdaQueryWrapper<NoticeRecord> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(NoticeRecord::getUserId, userId);
        List<NoticeRecord> noticeRecordList = list(wrapper);
        ValidatorUtil.notEmpty(noticeRecordList, ResultCodeEnum.NOTICE_RECORD_NOT_EXIST_ERROR);
        // 未读状态不允许删除
        noticeRecordList.forEach(item -> ValidatorUtil.isTrue(StrUtil.equals(Constants.COMMON_JUDGMENT.ONE_STR, item.getStatus()), ResultCodeEnum.NOTICE_RECORD_NOT_READ_ERROR));
        ValidatorUtil.isTrue(remove(wrapper), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 获取通知公告发布记录
     *
     * @param noticeIds
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<NoticeRecordDTO> getNoticeRecord(List<Long> noticeIds) {
        ValidatorUtil.notEmpty(noticeIds);
        LambdaQueryWrapper<NoticeRecord> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(NoticeRecord::getNoticeId, noticeIds);
        return OrikaUtil.convertList(list(wrapper), NoticeRecordDTO.class);
    }

    /**
     * 更新通知公告读取时间
     *
     * @param userId
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean readNoticeRecord(Long noticeRecordId, Long userId) {
        ValidatorUtil.allNotEmpty(noticeRecordId, userId);
        // 通知公告记录是否存在
        LambdaQueryWrapper<NoticeRecord> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(NoticeRecord::getId, noticeRecordId).eq(NoticeRecord::getUserId, userId);
        NoticeRecord noticeRecord = getOne(queryWrapper);
        ValidatorUtil.notEmpty(noticeRecord, ResultCodeEnum.NOTICE_RECORD_NOT_EXIST_ERROR);
        ValidatorUtil.isTrue(StrUtil.equals(noticeRecord.getStatus(), Constants.COMMON_JUDGMENT.ZONE_STR), ResultCodeEnum.NOTICE_RECORD_STATUS_ERROR);
        // 更新读取时间
        noticeRecord.setReadTime(new Date());
        noticeRecord.setStatus(Constants.COMMON_JUDGMENT.ONE_STR);
        noticeRecord.setRemark("消息已读");
        ValidatorUtil.isTrue(updateById(noticeRecord), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

}
