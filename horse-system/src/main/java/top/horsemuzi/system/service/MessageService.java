package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.core.message.common.model.ContentModel;
import top.horsemuzi.system.pojo.dto.message.MessageDTO;
import top.horsemuzi.system.pojo.dto.statistics.PieChartDTO;
import top.horsemuzi.system.pojo.entity.Message;

import java.util.List;

/**
 * <p>
 * 消息信息表 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-09-30 10:59:09
 */
public interface MessageService extends IService<Message> {

    /**
     * 新增消息记录
     *
     * @param messageDTO
     * @return
     */
    boolean addMessage(MessageDTO messageDTO);

    /**
     * 获取消息记录
     *
     * @param recordId
     * @return
     */
    MessageDTO getMessage(String recordId);

    /**
     * 更新消息记录
     *
     * @param messageDTO
     * @return
     */
    boolean updateMessage(MessageDTO messageDTO);

    /**
     * 获取消息记录详情
     *
     * @param id
     * @return
     */
    MessageDTO getMessage(Long id);

    /**
     * 删除消息日志
     *
     * @param messageIds
     * @return
     */
    boolean deleteMessage(List<Long> messageIds);

    /**
     * 根据消息状态统计饼图
     *
     * @return
     */
    List<PieChartDTO> statisticsMessageByStatus();

    /**
     * 根据消息渠道统计饼图
     *
     * @return
     */
    List<PieChartDTO> statisticsMessageByChannel();

    /**
     * 发送邮件消息
     *
     * @param contentModel
     */
    void sendEmailMessage(ContentModel contentModel);

    /**
     * 发送短信消息
     *
     * @param contentModel
     */
    void sendSmsMessage(ContentModel contentModel);

    /**
     * 发送钉钉机器人消息
     *
     * @param contentModel
     * @param receiver
     */
    void sendDingBotMessage(ContentModel contentModel, String receiver);

    /**
     * 发送钉钉工作通知消息
     *
     * @param contentModel
     * @param receiver
     */
    void sendDingWorkMessage(ContentModel contentModel, String receiver);

}
