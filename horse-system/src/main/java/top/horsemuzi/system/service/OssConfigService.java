package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.oss.OssConfigDTO;
import top.horsemuzi.system.pojo.dto.oss.OssConfigQuery;
import top.horsemuzi.system.pojo.entity.OssConfig;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author Mr.Horse
 * @version 1.0.0
 * @description OSS对象存储配置业务对象tb_oss_config的Service实例
 * @date 2023-03-26 11:32:55
 */
public interface OssConfigService extends IService<OssConfig> {


    /**
     * 分页获取OSS对象存储配置
     *
     * @param query
     * @return
     */
    Page<OssConfigDTO> getPageOssConfig(OssConfigQuery query);

    /**
     * 获取OSS对象存储配置详情
     *
     * @param id 主键id
     * @return
     */
    OssConfigDTO getOssConfig(Long id);

    /**
     * 获取OSS对象存储配置列表
     *
     * @param ossConfigIds
     * @return
     */
    List<OssConfigDTO> getOssConfig(List<Long> ossConfigIds);

    /**
     * 保存OSS对象存储配置
     *
     * @param ossConfigDTO
     * @return
     */
    boolean addOssConfig(OssConfigDTO ossConfigDTO);

    /**
     * 更新OSS对象存储配置
     *
     * @param ossConfigDTO
     * @return
     */
    boolean updateOssConfig(OssConfigDTO ossConfigDTO);

    /**
     * 删除OSS对象存储配置
     *
     * @param ossConfigIds
     * @return
     */
    boolean deleteOssConfig(List<Long> ossConfigIds);

    /**
     * 导出OSS对象存储配置 EXCEL 数据
     *
     * @param query
     * @param response
     */
    void export(OssConfigQuery query, HttpServletResponse response);

    /**
     * 实时检索
     *
     * @param search 检索关键词
     * @return
     */
    List<InputRemoteFetchDTO> fetchConfig(String search);

    /**
     * 根据配置key获取配置信息
     *
     * @param configKey
     * @return top.horsemuzi.system.pojo.dto.oss.OssConfigDTO
     * @author mabin
     * @date 2023/3/27 11:28
     **/
    OssConfigDTO getOssConfig(String configKey);
}
