package top.horsemuzi.system.service.expand;

/**
 * 数据脱敏接口服务
 *
 * @author mabin
 * @date 2022/09/29 11:38
 **/

public interface SensitiveService {

    /**
     * 是否脱敏
     *
     * @return
     */
    boolean isSensitive();

}
