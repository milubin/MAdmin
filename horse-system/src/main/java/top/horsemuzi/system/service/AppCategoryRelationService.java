package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.open.AppCategoryRelationDTO;
import top.horsemuzi.system.pojo.entity.AppCategoryRelation;

import java.util.List;

/**
 * <p>
 * 开放API应用和接口类型关联信息 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-18 22:09:28
 */
public interface AppCategoryRelationService extends IService<AppCategoryRelation> {

    /**
     * 保存应用和接口类型关联信息
     *
     * @param appCategoryRelationDTOList
     * @return
     */
    boolean addAppCategoryRelation(List<AppCategoryRelationDTO> appCategoryRelationDTOList);

    /**
     * 删除应用类型关联信息
     *
     * @param appId
     * @return
     */
    boolean deleteAppCategoryRelation(Long appId);

    /**
     * 删除应用类型关联信息
     *
     * @param appIds
     * @return
     */
    boolean deleteAppCategoryRelation(List<Long> appIds);

    /**
     * 获取应用类型关联信息
     *
     * @param apiCategoryIds
     * @return
     */
    List<AppCategoryRelationDTO> getAppCategoryRelation(List<Long> apiCategoryIds);
}
