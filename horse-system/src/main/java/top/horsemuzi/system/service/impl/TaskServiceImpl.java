package top.horsemuzi.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.mapper.TaskMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskDTO;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskQuery;
import top.horsemuzi.system.pojo.entity.Task;
import top.horsemuzi.system.service.TaskService;

import java.util.List;

/**
 * <p>
 * 系统任务调度信息表 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-08-20 17:56:39
 */
@Service
public class TaskServiceImpl extends ExpandServiceImpl<TaskMapper, Task> implements TaskService {

    /**
     * @param query
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public Page<TaskDTO> getPageTasks(TaskQuery query) {
        Page<Task> page = new Page<>(query.getPage(), query.getSize());
        LambdaQueryWrapper<Task> wrapper = new LambdaQueryWrapper<>();
        if (ObjectUtil.isNotNull(query)) {
            wrapper.like(StrUtil.isNotBlank(query.getTaskName()), Task::getTaskName, query.getTaskName())
                    .like(StrUtil.isNotBlank(query.getClassName()), Task::getClassName, query.getClassName())
                    .like(StrUtil.isNotBlank(query.getMethodName()), Task::getMethodName, query.getMethodName())
                    .like(StrUtil.isNotBlank(query.getMethodParams()), Task::getMethodParams, query.getMethodParams())
                    .eq(ObjectUtil.isNotNull(query.getStatus()), Task::getStatus, query.getStatus())
                    .eq(ObjectUtil.isNotNull(query.getCreateStart()), Task::getGmtCreate, query.getCreateStart())
                    .eq(ObjectUtil.isNotNull(query.getCreateEnd()), Task::getGmtCreate, query.getCreateEnd());
        }
        return Task.convertPage(page(page, wrapper), TaskDTO.class);
    }

    /**
     * 获取调度任务信息
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public TaskDTO getTask(Long id) {
        ValidatorUtil.notEmpty(id);
        return OrikaUtil.convert(getById(id), TaskDTO.class);
    }

    /**
     * 更新调度任务
     *
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateTask(TaskDTO taskDTO) {
        ValidatorUtil.notEmpty(taskDTO);
        ValidatorUtil.notEmpty(taskDTO.getId());
        ValidatorUtil.isTrue(updateById(OrikaUtil.convert(taskDTO, Task.class)), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 保存调度任务
     *
     * @param taskDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addTask(TaskDTO taskDTO) {
        ValidatorUtil.notEmpty(taskDTO);
        Task task = OrikaUtil.convert(taskDTO, Task.class);
        ValidatorUtil.isTrue(save(task), ResultCodeEnum.DATA_SAVE_ERROR);
        taskDTO.setId(task.getId());
        return true;
    }

    /**
     * 获取调度任务列表
     *
     * @param taskIds
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<TaskDTO> getTasks(List<Long> taskIds) {
        ValidatorUtil.notEmpty(taskIds);
        return OrikaUtil.convertList(listByIds(taskIds), TaskDTO.class);
    }

    /**
     * 删除调度任务
     *
     * @param taskIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteTask(List<Long> taskIds) {
        ValidatorUtil.notEmpty(taskIds);
        ValidatorUtil.isTrue(removeBatchByIds(taskIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 获取调度任务
     *
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<TaskDTO> getTasks(TaskDTO query) {
        LambdaQueryWrapper<Task> wrapper = getTaskWrapper(query);
        return OrikaUtil.convertList(list(wrapper), TaskDTO.class);
    }

    /**
     * Spring任务名称实时检索
     *
     * @param search
     * @return
     */
    @Override
    public List<InputRemoteFetchDTO> taskFetch(String search) {
        return baseMapper.taskFetch(search);
    }

    /**
     * 更新调度任务
     *
     * @param taskDTOList
     * @return boolean
     * @author mabin
     * @date 2023/3/1 13:53
     **/
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateTask(List<TaskDTO> taskDTOList) {
        ValidatorUtil.notEmpty(taskDTOList);
        ValidatorUtil.isTrue(updateBatchById(taskDTOList, Task.class), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public TaskDTO getTask(TaskDTO query) {
        LambdaQueryWrapper<Task> wrapper = getTaskWrapper(query);
        return OrikaUtil.convert(getOne(wrapper), TaskDTO.class);
    }

    private LambdaQueryWrapper<Task> getTaskWrapper(TaskDTO query) {
        LambdaQueryWrapper<Task> wrapper = new LambdaQueryWrapper<>();
        if (ObjectUtil.isNotNull(query)) {
            wrapper.eq(ObjectUtil.isNotNull(query.getId()), Task::getId, query.getId())
                    .eq(StrUtil.isNotBlank(query.getTaskName()), Task::getTaskName, query.getTaskName())
                    .eq(StrUtil.isNotBlank(query.getClassName()), Task::getClassName, query.getClassName())
                    .eq(StrUtil.isNotBlank(query.getMethodName()), Task::getMethodName, query.getMethodName())
                    .eq(StrUtil.isNotBlank(query.getMethodParams()), Task::getMethodParams, query.getMethodParams())
                    .eq(StrUtil.isNotBlank(query.getCron()), Task::getCron, query.getCron())
                    .eq(ObjectUtil.isNotNull(query.getStatus()), Task::getStatus, query.getStatus())
                    .isNull(StrUtil.isBlank(query.getMethodParams()), Task::getMethodParams);
        }
        return wrapper;
    }

    /**
     * 校验调度任务是否重复
     *
     * @param taskDTO
     * @return true: 重复， false: 不重复
     */
    @Override
    public boolean checkRepeat(TaskDTO taskDTO) {
        ValidatorUtil.notEmpty(taskDTO);
        LambdaQueryWrapper<Task> wrapper = new LambdaQueryWrapper<>();
        wrapper.ne(ObjectUtil.isNotNull(taskDTO.getId()), Task::getId, taskDTO.getId())
                .eq(StrUtil.isNotBlank(taskDTO.getClassName()), Task::getClassName, taskDTO.getClassName())
                .eq(StrUtil.isNotBlank(taskDTO.getMethodName()), Task::getMethodName, taskDTO.getMethodName())
                .eq(StrUtil.isNotBlank(taskDTO.getMethodParams()), Task::getMethodParams, taskDTO.getMethodParams())
                .isNull(StrUtil.isBlank(taskDTO.getMethodParams()), Task::getMethodParams);
        return ObjectUtil.isNotNull(getOne(wrapper));
    }

}
