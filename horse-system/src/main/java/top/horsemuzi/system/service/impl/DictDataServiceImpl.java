package top.horsemuzi.system.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.mapper.DictDataMapper;
import top.horsemuzi.system.pojo.dto.dict.DictDataDTO;
import top.horsemuzi.system.pojo.dto.dict.DictDataQuery;
import top.horsemuzi.system.pojo.entity.DictData;
import top.horsemuzi.system.service.DictDataService;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 字典数据表 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-06-20 11:04:36
 */
@Service
public class DictDataServiceImpl extends ExpandServiceImpl<DictDataMapper, DictData> implements DictDataService {

    /**
     * 获取字典数据
     *
     * @param queryDictDataDTO
     * @return
     */
    @Override
    public DictDataDTO getDictData(DictDataDTO queryDictDataDTO) {
        ValidatorUtil.notEmpty(queryDictDataDTO);
        LambdaQueryWrapper<DictData> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StrUtil.isNotBlank(queryDictDataDTO.getType()), DictData::getType, queryDictDataDTO.getType())
                .eq(StrUtil.isNotBlank(queryDictDataDTO.getLabel()), DictData::getLabel, queryDictDataDTO.getLabel())
                .eq(StrUtil.isNotBlank(queryDictDataDTO.getValue()), DictData::getValue, queryDictDataDTO.getValue())
                .eq(StrUtil.isNotBlank(queryDictDataDTO.getStatus()), DictData::getStatus, queryDictDataDTO.getStatus());
        DictData dictData = getOne(wrapper);
        return OrikaUtil.convert(dictData, DictDataDTO.class);
    }

    /**
     * 获取字典数据列表
     *
     * @param query
     * @return
     */
    @Override
    public List<DictDataDTO> getDictDatas(DictDataDTO query) {
        ValidatorUtil.notEmpty(query);
        LambdaQueryWrapper<DictData> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StrUtil.isNotBlank(query.getType()), DictData::getType, query.getType())
                .eq(StrUtil.isNotBlank(query.getLabel()), DictData::getLabel, query.getLabel())
                .eq(StrUtil.isNotBlank(query.getValue()), DictData::getValue, query.getValue())
                .eq(StrUtil.isNotBlank(query.getStatus()), DictData::getStatus, query.getStatus())
                .in(CollectionUtil.isNotEmpty(query.getTypes()), DictData::getType, query.getTypes());
        return OrikaUtil.convertList(list(wrapper), DictDataDTO.class);
    }

    /**
     * 保存字典数据
     *
     * @param dictDataDTO
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addDictData(DictDataDTO dictDataDTO) {
        ValidatorUtil.notEmpty(dictDataDTO);
        DictData dictData = OrikaUtil.convert(dictDataDTO, DictData.class);
        ValidatorUtil.isTrue(save(dictData), ResultCodeEnum.DATA_SAVE_ERROR);
        dictDataDTO.setId(dictData.getId());
        return true;
    }

    /**
     * 更新对应类型的字典数据
     *
     * @param dictDataDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateDictData(DictDataDTO dictDataDTO) {
        ValidatorUtil.notEmpty(dictDataDTO);
        ValidatorUtil.notEmpty(dictDataDTO.getId());
        ValidatorUtil.allNotBlank(dictDataDTO.getType(), dictDataDTO.getLabel(), dictDataDTO.getValue());
        ValidatorUtil.isTrue(updateById(OrikaUtil.convert(dictDataDTO, DictData.class)),
                ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 获取字典数据
     *
     * @param dictDataIds
     * @return
     */
    @Override
    public List<DictDataDTO> getDictDatas(List<Long> dictDataIds) {
        ValidatorUtil.notEmpty(dictDataIds);
        List<DictData> dictDataList = listByIds(dictDataIds);
        return OrikaUtil.convertList(dictDataList, DictDataDTO.class);
    }

    /**
     * 删除字典数据
     *
     * @param dictDataIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteDictDatas(List<Long> dictDataIds) {
        ValidatorUtil.notEmpty(dictDataIds);
        ValidatorUtil.isTrue(removeBatchByIds(dictDataIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 分页获取字典数据列表
     *
     * @param dictDataQuery
     * @return
     */
    @Override
    public Page<DictDataDTO> getPageDictDatas(DictDataQuery dictDataQuery) {
        Page<DictData> page = new Page<>(dictDataQuery.getPage(), dictDataQuery.getSize());
        LambdaQueryWrapper<DictData> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StrUtil.isNotBlank(dictDataQuery.getType()), DictData::getType, dictDataQuery.getType())
                .like(StrUtil.isNotBlank(dictDataQuery.getLabel()), DictData::getLabel, dictDataQuery.getLabel())
                .eq(StrUtil.isNotBlank(dictDataQuery.getStatus()), DictData::getStatus, dictDataQuery.getStatus())
                .eq(StrUtil.isNotBlank(dictDataQuery.getDefaults()), DictData::getDefaults, dictDataQuery.getDefaults())
                .ge(Objects.nonNull(dictDataQuery.getCreateStart()), DictData::getGmtCreate,
                        dictDataQuery.getCreateStart())
                .le(Objects.nonNull(dictDataQuery.getCreateEnd()), DictData::getGmtCreate,
                        dictDataQuery.getCreateEnd());
        return DictData.convertPage(page(page, wrapper), DictDataDTO.class);
    }

    /**
     * 根据id获取字典数据详情
     *
     * @param id
     * @return
     */
    @Override
    public DictDataDTO getDictData(Long id) {
        ValidatorUtil.notEmpty(id, ResultCodeEnum.OBJECT_ID_IS_NULL);
        DictData dictData = getById(id);
        return OrikaUtil.convert(dictData, DictDataDTO.class);
    }

    /**
     * 校验字典数据是否重复, 字典数据默认属性是否重复
     *
     * @return
     */
    @Override
    public Boolean checkDictData(DictDataDTO query) {
        ValidatorUtil.notEmpty(query);
        LambdaQueryWrapper<DictData> wrapper = new LambdaQueryWrapper<>();
        wrapper.ne(ObjectUtil.isNotNull(query.getId()), DictData::getId, query.getId())
                .eq(StrUtil.isNotBlank(query.getType()), DictData::getType, query.getType());
        List<DictData> dictDataList = list(wrapper);
        if (CollectionUtil.isNotEmpty(dictDataList)) {
            for (DictData dictData : dictDataList) {
                if (StrUtil.equals(dictData.getLabel(), query.getLabel())) {
                    return Boolean.FALSE;
                }
                if (StrUtil.equals(query.getDefaults(), Constants.COMMON_JUDGMENT.Y)
                        && StrUtil.equals(dictData.getDefaults(), query.getDefaults())) {
                    return Boolean.FALSE;
                }
            }
        }
        return Boolean.TRUE;
    }

}
