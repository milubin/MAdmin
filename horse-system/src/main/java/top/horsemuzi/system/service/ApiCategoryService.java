package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import top.horsemuzi.system.pojo.dto.open.category.ApiCategoryDTO;
import top.horsemuzi.system.pojo.dto.open.category.ApiCategoryQuery;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.entity.ApiCategory;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 开放API类型信息 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-18 22:09:28
 */
public interface ApiCategoryService extends IService<ApiCategory> {

    /**
     * 获取开放API接口分类列表
     *
     * @param appId
     * @return
     */
    List<ApiCategoryDTO> listApiCategory(Long appId);

    /**
     * 分页获取开放API接口类型列表
     *
     * @param query
     * @return
     */
    Page<ApiCategoryDTO> getPageApiCategory(ApiCategoryQuery query);

    /**
     * 获取开放API类型详情
     *
     * @param id
     * @return
     */
    ApiCategoryDTO getApiCategory(Long id);

    /**
     * 新增开放API类型
     *
     * @param apiCategoryDTO
     * @return
     */
    boolean addApiCategory(ApiCategoryDTO apiCategoryDTO);

    /**
     * 更新开放API类型
     *
     * @param apiCategoryDTO
     * @return
     */
    boolean updateApiCategory(ApiCategoryDTO apiCategoryDTO);

    /**
     * 获取开放API类型
     *
     * @param apiCategoryIds
     * @return
     */
    List<ApiCategoryDTO> getApiCategory(List<Long> apiCategoryIds);

    /**
     * 移除开放API类型
     *
     * @param apiCategoryIds
     * @return
     */
    boolean deleteApiCategory(List<Long> apiCategoryIds);

    /**
     * 接口类型名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> fetch(String search);

    /**
     * 获取开放API接口类型列表
     *
     * @return
     */
    List<ApiCategoryDTO> getApiCategoryList();

}
