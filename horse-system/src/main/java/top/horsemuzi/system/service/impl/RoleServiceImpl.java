package top.horsemuzi.system.service.impl;

import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.core.office.service.OfficeService;
import top.horsemuzi.system.mapper.RoleMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.role.RoleDTO;
import top.horsemuzi.system.pojo.dto.role.RoleExportDTO;
import top.horsemuzi.system.pojo.dto.role.RoleQuery;
import top.horsemuzi.system.pojo.entity.Role;
import top.horsemuzi.system.service.RoleService;

import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-07 22:44:27
 */
@Service
public class RoleServiceImpl extends ExpandServiceImpl<RoleMapper, Role> implements RoleService {


    /**
     * 新增角色信息
     *
     * @param roleDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addRole(RoleDTO roleDTO) {
        ValidatorUtil.notEmpty(roleDTO);
        Role role = OrikaUtil.convert(roleDTO, Role.class);
        ValidatorUtil.isTrue(save(role), ResultCodeEnum.DATA_SAVE_ERROR);
        roleDTO.setId(role.getId());
        return true;
    }

    /**
     * 获取角色详情
     *
     * @param queryRoleDTO
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public RoleDTO getRole(RoleDTO queryRoleDTO) {
        ValidatorUtil.notEmpty(queryRoleDTO);
        LambdaQueryWrapper<Role> wrapper = new LambdaQueryWrapper<Role>()
                .eq(StrUtil.isNotBlank(queryRoleDTO.getStatus()), Role::getStatus, queryRoleDTO.getStatus())
                .eq(StrUtil.isNotBlank(queryRoleDTO.getName()), Role::getName, queryRoleDTO.getName())
                .eq(StrUtil.isNotBlank(queryRoleDTO.getRoleCode()), Role::getRoleCode, queryRoleDTO.getRoleCode());
        return getOne(wrapper, RoleDTO.class);
    }

    /**
     * 根据id获取角色详情
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public RoleDTO getRole(Long id) {
        ValidatorUtil.notEmpty(id);
        Role role = getById(id);
        return OrikaUtil.convert(role, RoleDTO.class);
    }

    /**
     * 更新角色信息
     *
     * @param roleDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateRole(RoleDTO roleDTO) {
        ValidatorUtil.notEmpty(roleDTO);
        ValidatorUtil.notEmpty(roleDTO.getId());
        ValidatorUtil.isTrue(updateById(OrikaUtil.convert(roleDTO, Role.class)), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 根据id删除角色信息
     *
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteRoles(List<Long> roleIds) {
        ValidatorUtil.notEmpty(roleIds);
        removeBatchByIds(roleIds);
        return true;
    }

    /**
     * 获取角色信息列表
     *
     * @param roleIds
     * @return
     */
    @Override
    public List<RoleDTO> getRoles(List<Long> roleIds) {
        ValidatorUtil.notEmpty(roleIds);
        List<Role> roles = listByIds(roleIds);
        return OrikaUtil.convertList(roles, RoleDTO.class);
    }

    /**
     * 分页获取角色列表
     *
     * @param roleQuery
     * @return
     */
    @Override
    public Page<RoleDTO> getPageRoles(RoleQuery roleQuery) {
        Page<Role> page = new Page<>(roleQuery.getPage(), roleQuery.getSize());
        LambdaQueryWrapper<Role> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StrUtil.isNotBlank(roleQuery.getName()), Role::getName, roleQuery.getName())
                .like(StrUtil.isNotBlank(roleQuery.getRoleCode()), Role::getRoleCode, roleQuery.getRoleCode())
                .eq(StrUtil.isNotEmpty(roleQuery.getStatus()), Role::getStatus, roleQuery.getStatus())
                .ge(Objects.nonNull(roleQuery.getCreateStart()), Role::getGmtCreate, roleQuery.getCreateStart())
                .le(Objects.nonNull(roleQuery.getCreateEnd()), Role::getGmtCreate, roleQuery.getCreateEnd())
                .orderByDesc(Role::getGmtCreate);
        return Role.convertPage(page(page, wrapper), RoleDTO.class);
    }

    /**
     * 获取角色信息列表
     *
     * @return
     */
    @Override
    public List<RoleDTO> getRoles() {
        LambdaQueryWrapper<Role> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Role::getStatus, Constants.COMMON_JUDGMENT.ZONE_STR);
        return list(wrapper, RoleDTO.class);
    }

    /**
     * 获取角色信息列表
     *
     * @param roleCodeList
     * @return
     */
    @Override
    public List<RoleDTO> getRolesByCodes(List<String> roleCodeList) {
        ValidatorUtil.notEmpty(roleCodeList);
        LambdaQueryWrapper<Role> wrapper = new LambdaQueryWrapper<Role>()
                .in(Role::getRoleCode, roleCodeList)
                .eq(Role::getStatus, Constants.COMMON_JUDGMENT.ZONE_STR);
        return list(wrapper, RoleDTO.class);
    }

    /**
     * 角色名称实时检索
     *
     * @param search
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<InputRemoteFetchDTO> fetch(String search) {
        return StrUtil.isBlank(search) ? Collections.emptyList() : baseMapper.fetch(search);
    }

    /**
     * 导出角色和菜单权限信息
     *
     * @param query
     * @param response
     */
    @Override
    public void export(RoleQuery query, HttpServletResponse response) {
        List<RoleExportDTO> roleExportDTOList = baseMapper.getRoleExportList(query);
        try {
            if (CollectionUtil.isNotEmpty(roleExportDTOList)) {
                OfficeService.EXCEL_POI.export(response, RoleExportDTO.class, roleExportDTOList,
                        "角色权限信息.xlsx", "角色权限信息", "角色权限菜单详情", ExcelType.XSSF);
            }
        } catch (Exception e) {
            throw new BusinessException(e, ResultCodeEnum.FILE_EXPORT_ERROR);
        }
    }

    /**
     * 角色权限标识是否重复(与停用状态的角色也不能重复)
     *
     * @param roleDTO
     * @return
     */
    @Override
    public boolean checkRoleUnique(RoleDTO roleDTO) {
        ValidatorUtil.notEmpty(roleDTO);
        LambdaQueryWrapper<Role> wrapper = new LambdaQueryWrapper<>();
        wrapper.ne(Objects.nonNull(roleDTO.getId()), Role::getId, roleDTO.getId())
                .eq(Role::getRoleCode, roleDTO.getRoleCode());
        return Objects.isNull(getOne(wrapper));
    }


}
