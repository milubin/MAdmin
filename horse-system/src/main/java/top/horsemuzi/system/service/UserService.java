package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.user.UserDTO;
import top.horsemuzi.system.pojo.dto.user.UserQuery;
import top.horsemuzi.system.pojo.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>
 * 管理用户表 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-07 22:44:27
 */
public interface UserService extends IService<User> {

    /**
     * 更新用户登录信息
     *
     * @param userDTO
     * @param request
     */
    void updateLoginUser(UserDTO userDTO, HttpServletRequest request);

    /**
     * 获取用户信息(用户名/手机号/邮箱)
     *
     * @param username
     * @return
     */
    UserDTO getUserByLogin(String username);

    /**
     * 获取用户列表-用户管理
     *
     * @param userQuery
     * @return
     */
    Page<UserDTO> getPageUsers(UserQuery userQuery);

    /**
     * 新增用户-用户管理
     *
     * @param userDTO
     * @return
     */
    boolean addUser(UserDTO userDTO);

    /**
     * 更新用户-用户管理
     *
     * @param userDTO
     * @return
     */
    boolean updateUser(UserDTO userDTO);

    /**
     * 更新用户在线时长
     *
     * @param id
     */
    void updateUserOnlineTime(Long id);

    /**
     * 获取用户信息-用户管理
     *
     * @param userId
     * @return
     */
    UserDTO getUser(Long userId);

    /**
     * 批量删除用户-用户管理
     *
     * @param userIds
     * @return
     */
    boolean deleteUsers(List<Long> userIds);

    /**
     * 导出数据-用户管理
     *
     * @param userQuery
     * @param response
     */
    void export(UserQuery userQuery, HttpServletResponse response);

    /**
     * 导入数据-用户管理
     *
     * @param file    表单文件
     * @param flag    是否同步更新已存在用户信息标识: 0-不更新, 1-更新
     * @param jwtUser 当前操作用户基础信息
     */
    void imports(MultipartFile file, Integer flag, JwtUser jwtUser);

    /**
     * 获取用户信息-用户管理(体现于用户新增、更新逻辑)
     *
     * @param userDTO
     * @return
     */
    UserDTO getExistUser(UserDTO userDTO);

    /**
     * 获取用户信息
     *
     * @param userIds
     * @return
     */
    List<UserDTO> getUsers(List<Long> userIds);

    /**
     * 批量更新用户信息
     *
     * @param userDTOList
     * @return
     */
    boolean updateBatchUser(List<UserDTO> userDTOList);

    /**
     * 获取全部用户列表
     *
     * @param userQuery
     * @return
     */
    List<UserDTO> getUsers(UserQuery userQuery);

    /**
     * 用户名输入框实时检索
     *
     * @param search
     * @param filedType
     * @return
     */
    List<InputRemoteFetchDTO> fetch(String search, String filedType);
}
