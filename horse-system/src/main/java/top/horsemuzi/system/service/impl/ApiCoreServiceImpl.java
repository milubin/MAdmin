package top.horsemuzi.system.service.impl;

import cn.hutool.extra.spring.SpringUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.horsemuzi.common.util.ExceptionUtil;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.system.core.gold.base.GoldRequestEnum;
import top.horsemuzi.system.core.gold.service.GoldService;
import top.horsemuzi.system.service.ApiCoreService;

import java.util.Map;

/**
 * API外部接口核心服务实现
 *
 * @author mabin
 * @date 2022/11/24 10:19
 **/
@Slf4j
@Service
@NoArgsConstructor
public class ApiCoreServiceImpl implements ApiCoreService {

    /**
     * 获取ip信息
     *
     * @param data
     * @return
     */
    @Override
    public Object getIp(String data) {
        try {
            JsonNode jsonNode = JsonUtil.fromJson(data);
            String ip = JsonUtil.getNodeStr(jsonNode, "ip");
            GoldRequestEnum.APIEnum apiEnum = GoldRequestEnum.APIEnum.IP;
            GoldRequestEnum.Ip param = (GoldRequestEnum.Ip) apiEnum.getParam();
            param.setIp(ip);
            String search = SpringUtil.getBean(GoldService.class).search(apiEnum);
            return JsonUtil.fromJson(search, new TypeReference<Map<String, Object>>() {
            });
        } catch (Exception e) {
            log.error("执行异常: {}", ExceptionUtil.getMessage(e));
        }
        return null;
    }

}
