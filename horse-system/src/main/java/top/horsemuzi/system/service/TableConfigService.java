package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.generate.TableConfigDTO;
import top.horsemuzi.system.pojo.dto.generate.TableConfigQuery;
import top.horsemuzi.system.pojo.entity.TableConfig;

import java.util.List;

/**
 * <p>
 * 业务表配置 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-12-19 13:48:53
 */
public interface TableConfigService extends IService<TableConfig> {

    /**
     * 分页获取业务表配置列表
     *
     * @param query
     * @return
     */
    Page<TableConfigDTO> getPageTableConfig(TableConfigQuery query);

    /**
     * 获取业务表配置详情
     *
     * @param id
     * @return
     */
    TableConfigDTO getTableConfig(Long id);

    /**
     * 业务表配置是否重复
     *
     * @param tableConfigDTO
     * @return
     */
    boolean checkTableConfigRepeat(TableConfigDTO tableConfigDTO);

    /**
     * 更新业务表配置
     *
     * @param tableConfigDTO
     * @return
     */
    boolean updateTableConfig(TableConfigDTO tableConfigDTO);

    /**
     * 新增业务表配置
     *
     * @param tableConfigDTO
     * @return
     */
    boolean addTableConfig(TableConfigDTO tableConfigDTO);

    /**
     * 获取业务表配置列表
     *
     * @param tableConfigIds
     * @return
     */
    List<TableConfigDTO> getTableConfig(List<Long> tableConfigIds);

    /**
     * 删除业务表配置
     *
     * @param tableConfigIds
     * @return
     */
    boolean deleteTableConfig(List<Long> tableConfigIds);

    /**
     * 获取业务表配置列表
     *
     * @return
     */
    List<TableConfigDTO> getTableConfigList();

}
