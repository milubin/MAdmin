package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.generate.TableColumnDTO;
import top.horsemuzi.system.pojo.entity.TableColumn;

import java.util.List;

/**
 * <p>
 * 业务字段表 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-12-19 13:48:53
 */
public interface TableColumnService extends IService<TableColumn> {

    /**
     * 获取业务表字段列表
     *
     * @param tableId
     * @return
     */
    List<TableColumnDTO> getTableColumn(Long tableId);

    /**
     * 批量保存业务表字段
     *
     * @param tableColumnDTOList
     * @return
     */
    boolean addTableColumn(List<TableColumnDTO> tableColumnDTOList);

    /**
     * 批量更新业务表字段
     *
     * @param tableColumnDTOList
     * @return
     */
    boolean updateTableColumn(List<TableColumnDTO> tableColumnDTOList);

    /**
     * 删除业务表字段
     *
     * @param tableColumnIds
     * @return
     */
    boolean deleteTableColumn(List<Long> tableColumnIds);

    /**
     * 获取业务表字段列表
     *
     * @param tableIds
     * @return
     */
    List<TableColumnDTO> getTableColumn(List<Long> tableIds);
}
