package top.horsemuzi.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.mapper.MessageChannelMapper;
import top.horsemuzi.system.pojo.dto.message.MessageChannelDTO;
import top.horsemuzi.system.pojo.entity.MessageChannel;
import top.horsemuzi.system.service.MessageChannelService;

import java.util.List;

/**
 * <p>
 * 消息渠道配置表 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-09-25 18:20:21
 */
@Service
public class MessageChannelServiceImpl extends ExpandServiceImpl<MessageChannelMapper, MessageChannel> implements MessageChannelService {

    /**
     * 获取消息渠道列表
     *
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<MessageChannelDTO> getMessageChannels() {
        return OrikaUtil.convertList(list(), MessageChannelDTO.class);
    }

    /**
     * 更新消息渠道
     *
     * @param messageChannelDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateMessageChannel(MessageChannelDTO messageChannelDTO) {
        ValidatorUtil.notEmpty(messageChannelDTO);
        ValidatorUtil.notEmpty(messageChannelDTO.getId());
        ValidatorUtil.isTrue(updateById(OrikaUtil.convert(messageChannelDTO, MessageChannel.class)), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 校验消息渠道是否重复
     *
     * @param messageChannelDTO
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public boolean checkMessageChannelRepeat(MessageChannelDTO messageChannelDTO) {
        ValidatorUtil.notEmpty(messageChannelDTO);
        ValidatorUtil.notEmpty(messageChannelDTO.getChannel());
        LambdaQueryWrapper<MessageChannel> wrapper = new LambdaQueryWrapper<>();
        wrapper.ne(ObjectUtil.isNotNull(messageChannelDTO.getId()), MessageChannel::getId, messageChannelDTO.getId())
                .eq(MessageChannel::getChannel, messageChannelDTO.getChannel());
        return ObjectUtil.isNull(getOne(wrapper));
    }

    /**
     * 新增消息渠道
     *
     * @param messageChannelDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addMessageChannel(MessageChannelDTO messageChannelDTO) {
        ValidatorUtil.notEmpty(messageChannelDTO);
        MessageChannel messageChannel = OrikaUtil.convert(messageChannelDTO, MessageChannel.class);
        ValidatorUtil.isTrue(save(messageChannel), ResultCodeEnum.DATA_SAVE_ERROR);
        messageChannelDTO.setId(messageChannel.getId());
        return true;
    }

    /**
     * 获取消息渠道配置
     *
     * @param channel 渠道标识
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public MessageChannelDTO getMessageChannel(Integer channel) {
        ValidatorUtil.notEmpty(channel);
        LambdaQueryWrapper<MessageChannel> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(MessageChannel::getChannel, channel);
        return OrikaUtil.convert(getOne(wrapper), MessageChannelDTO.class);
    }

}
