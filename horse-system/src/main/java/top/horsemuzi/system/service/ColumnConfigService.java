package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.generate.ColumnConfigDTO;
import top.horsemuzi.system.pojo.dto.generate.ColumnConfigQuery;
import top.horsemuzi.system.pojo.entity.ColumnConfig;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 字段类型关联信息 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-12-19 13:48:53
 */
public interface ColumnConfigService extends IService<ColumnConfig> {

    /**
     * 分页获取字段类型关联列表
     *
     * @param query
     * @return
     */
    Page<ColumnConfigDTO> getPageColumnConfig(ColumnConfigQuery query);

    /**
     * 获取字段类型关联详情
     *
     * @param id
     * @return
     */
    ColumnConfigDTO getColumnConfig(Long id);

    /**
     * 字段类型关联是否重复
     *
     * @param columnConfigDTO
     * @return
     */
    boolean checkColumnConfigRepeat(ColumnConfigDTO columnConfigDTO);

    /**
     * 新增字段类型关联
     *
     * @param columnConfigDTO
     * @return
     */
    boolean addColumnConfig(ColumnConfigDTO columnConfigDTO);

    /**
     * 更新字段类型关联
     *
     * @param columnConfigDTO
     * @return
     */
    boolean updateColumnConfig(ColumnConfigDTO columnConfigDTO);

    /**
     * 删除字段类型关联
     *
     * @param columnConfigIds
     * @return
     */
    boolean deleteColumnConfig(List<Long> columnConfigIds);

    /**
     * 获取字段类型关联列表
     *
     * @param columnTypes
     * @return
     */
    List<ColumnConfigDTO> getColumnConfig(Set<String> columnTypes);

    /**
     * 获取字段类型关联列表
     *
     * @return
     */
    List<ColumnConfigDTO> getColumnConfig();

    /**
     * 获取字段列表: 针对每种类型组装字符串列表, type=0返回DB字段类型列表, type=1返回JAVA字段类型列表
     *
     * @param type
     * @return
     */
    List<String> getColumnConfigType(String type);
}
