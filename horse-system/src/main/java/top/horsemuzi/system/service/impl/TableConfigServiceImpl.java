package top.horsemuzi.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.mapper.TableConfigMapper;
import top.horsemuzi.system.pojo.dto.generate.TableConfigDTO;
import top.horsemuzi.system.pojo.dto.generate.TableConfigQuery;
import top.horsemuzi.system.pojo.entity.TableConfig;
import top.horsemuzi.system.service.TableConfigService;

import java.util.List;

/**
 * <p>
 * 业务表配置 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-12-19 13:48:53
 */
@Service
public class TableConfigServiceImpl extends ExpandServiceImpl<TableConfigMapper, TableConfig> implements TableConfigService {

    /**
     * 分页获取业务表配置列表
     *
     * @param query
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public Page<TableConfigDTO> getPageTableConfig(TableConfigQuery query) {
        Page<TableConfig> page = new Page<>(query.getPage(), query.getSize());
        LambdaQueryWrapper<TableConfig> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StrUtil.isNotBlank(query.getAuthor()), TableConfig::getAuthor, query.getAuthor())
                .ge(ObjectUtil.isNotNull(query.getCreateStart()), TableConfig::getGmtCreate, query.getCreateStart())
                .le(ObjectUtil.isNotNull(query.getCreateEnd()), TableConfig::getGmtCreate, query.getCreateEnd())
                .orderByDesc(TableConfig::getGmtCreate);
        return TableConfig.convertPage(page(page, wrapper), TableConfigDTO.class);
    }

    /**
     * 获取业务表配置详情
     *
     * @param id
     * @return
     */
    @Override
    public TableConfigDTO getTableConfig(Long id) {
        ValidatorUtil.notEmpty(id);
        return getById(id, TableConfigDTO.class);
    }


    /**
     * 业务表配置是否重复
     *
     * @param tableConfigDTO
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public boolean checkTableConfigRepeat(TableConfigDTO tableConfigDTO) {
        LambdaQueryWrapper<TableConfig> wrapper = new LambdaQueryWrapper<TableConfig>()
                .ne(ObjectUtil.isNotNull(tableConfigDTO.getId()), TableConfig::getId, tableConfigDTO.getId())
                .eq(StrUtil.isNotBlank(tableConfigDTO.getAuthor()), TableConfig::getAuthor, tableConfigDTO.getAuthor());
        return ObjectUtil.isNull(getOne(wrapper));
    }

    /**
     * 更新业务表配置
     *
     * @param tableConfigDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateTableConfig(TableConfigDTO tableConfigDTO) {
        ValidatorUtil.notEmpty(tableConfigDTO);
        ValidatorUtil.notEmpty(tableConfigDTO.getId());
        ValidatorUtil.isTrue(updateById(tableConfigDTO, TableConfig.class), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 新增业务表配置
     *
     * @param tableConfigDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addTableConfig(TableConfigDTO tableConfigDTO) {
        ValidatorUtil.notEmpty(tableConfigDTO);
        TableConfig tableConfig = OrikaUtil.convert(tableConfigDTO, TableConfig.class);
        ValidatorUtil.isTrue(save(tableConfig), ResultCodeEnum.DATA_SAVE_ERROR);
        tableConfigDTO.setId(tableConfig.getId());
        return true;
    }

    /**
     * 获取业务表配置列表
     *
     * @param tableConfigIds
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<TableConfigDTO> getTableConfig(List<Long> tableConfigIds) {
        ValidatorUtil.notEmpty(tableConfigIds);
        return listByIds(tableConfigIds, TableConfigDTO.class);
    }

    /**
     * 删除业务表配置
     *
     * @param tableConfigIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteTableConfig(List<Long> tableConfigIds) {
        ValidatorUtil.notEmpty(tableConfigIds);
        ValidatorUtil.isTrue(removeBatchByIds(tableConfigIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 获取业务表配置列表
     *
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<TableConfigDTO> getTableConfigList() {
        return list(TableConfigDTO.class);
    }

}
