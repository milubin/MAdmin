package top.horsemuzi.system.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.enums.MessageStatusEnum;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.core.message.common.MessageConstants;
import top.horsemuzi.core.message.common.model.ContentModel;
import top.horsemuzi.core.message.enums.SendEnum;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.core.redis.base.RedisConstants;
import top.horsemuzi.system.core.redis.service.RedisService;
import top.horsemuzi.system.mapper.MessageMapper;
import top.horsemuzi.system.pojo.dto.message.MessageDTO;
import top.horsemuzi.system.pojo.dto.statistics.PieChartDTO;
import top.horsemuzi.system.pojo.entity.Message;
import top.horsemuzi.system.service.MessageService;

import java.util.Collections;
import java.util.List;

/**
 * <p>
 * 消息信息表 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-09-30 10:59:09
 */

@RequiredArgsConstructor
@Service
public class MessageServiceImpl extends ExpandServiceImpl<MessageMapper, Message> implements MessageService {

    private final RedisService redisService;

    /**
     * 新增消息记录
     *
     * @param messageDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addMessage(MessageDTO messageDTO) {
        ValidatorUtil.notEmpty(messageDTO);
        Message message = OrikaUtil.convert(messageDTO, Message.class);
        ValidatorUtil.isTrue(save(message), ResultCodeEnum.DATA_SAVE_ERROR);
        messageDTO.setId(message.getId());
        return true;
    }

    /**
     * 获取消息记录
     *
     * @param recordId
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public MessageDTO getMessage(String recordId) {
        ValidatorUtil.notEmpty(recordId);
        LambdaQueryWrapper<Message> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Message::getRecordId, recordId);
        return OrikaUtil.convert(getOne(wrapper), MessageDTO.class);
    }

    /**
     * 更新消息记录
     *
     * @param messageDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateMessage(MessageDTO messageDTO) {
        ValidatorUtil.notEmpty(messageDTO);
        ValidatorUtil.notEmpty(messageDTO.getId());
        ValidatorUtil.isTrue(updateById(OrikaUtil.convert(messageDTO, Message.class)), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 获取消息记录详情
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public MessageDTO getMessage(Long id) {
        ValidatorUtil.notEmpty(id);
        return OrikaUtil.convert(getById(id), MessageDTO.class);
    }

    /**
     * 根据消息状态统计饼图
     *
     * @return
     */
    @Override
    public List<PieChartDTO> statisticsMessageByStatus() {
        List<PieChartDTO> pieChartDTOList = baseMapper.statisticsMessageByStatus();
        if (CollectionUtil.isNotEmpty(pieChartDTOList)) {
            for (PieChartDTO chartDTO : pieChartDTOList) {
                chartDTO.setName(MessageStatusEnum.getByCode(chartDTO.getName()));
            }
        }
        return pieChartDTOList;
    }

    /**
     * 发送邮件消息
     *
     * @param contentModel
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void sendEmailMessage(ContentModel contentModel) {
        sendMessage(contentModel, SendEnum.EMAIL, RedisConstants.STREAM.EMAIL.STREAM, RedisConstants.STREAM.EMAIL.GROUP, null);
    }

    /**
     * 发送短信消息
     *
     * @param contentModel
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void sendSmsMessage(ContentModel contentModel) {
        sendMessage(contentModel, SendEnum.SMS, RedisConstants.STREAM.SMS.STREAM, RedisConstants.STREAM.SMS.GROUP, null);
    }

    /**
     * 发送钉钉机器人消息
     *
     * @param contentModel
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void sendDingBotMessage(ContentModel contentModel, String receiver) {
        sendMessage(contentModel, SendEnum.DING_BOT, RedisConstants.STREAM.DING_BOT.STREAM, RedisConstants.STREAM.DING_BOT.GROUP, receiver);
    }

    /**
     * 发送钉钉工作通知消息
     *
     * @param contentModel
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void sendDingWorkMessage(ContentModel contentModel, String receiver) {
        sendMessage(contentModel, SendEnum.DING_WORK, RedisConstants.STREAM.DING_WORK.STREAM, RedisConstants.STREAM.DING_WORK.GROUP, receiver);
    }

    /**
     * 发送消息
     *
     * @param contentModel 自定义消息内容
     * @param sendEnum
     * @param streamName   stream流名称
     * @param streamGroup
     * @param receiver     消息接收者(英文逗号分隔)
     */
    private void sendMessage(ContentModel contentModel, SendEnum sendEnum, String streamName, String streamGroup, String receiver) {
        ValidatorUtil.allNotEmpty(streamName, streamGroup, contentModel, sendEnum);
        MessageDTO messageDTO = new MessageDTO();
        // 接收者未设置则默认全部
        messageDTO.setReceiver(StrUtil.isNotBlank(receiver) && StrUtil.contains(receiver, StrUtil.COMMA) ? receiver : MessageConstants.SEND_ALL);
        messageDTO.setStreamName(streamName);
        messageDTO.setStreamGroup(streamGroup);
        messageDTO.setChannel(sendEnum.getChannel());
        String content = JsonUtil.toJson(contentModel);
        messageDTO.setContent(content);
        String recordId = System.currentTimeMillis() + StrUtil.DASHED + Convert.toStr(RandomUtil.randomInt(100));
        messageDTO.setRecordId(recordId);
        messageDTO.setStatus(MessageStatusEnum.INIT.getCode());
        messageDTO.setRemark(StrUtil.format("初始化状态消息({}-{})", streamName, streamGroup));
        if (addMessage(messageDTO)) {
            redisService.addStream(streamName, recordId, Collections.singletonMap(streamName, content));
        }
    }

    /**
     * 根据消息渠道统计饼图
     *
     * @return
     */
    @Override
    public List<PieChartDTO> statisticsMessageByChannel() {
        List<PieChartDTO> pieChartDTOList = baseMapper.statisticsMessageByChannel();
        if (CollectionUtil.isNotEmpty(pieChartDTOList)) {
            for (PieChartDTO chartDTO : pieChartDTOList) {
                chartDTO.setName(SendEnum.getChannelName(Convert.toInt(chartDTO.getName())));
            }
        }
        return pieChartDTOList;
    }

    /**
     * 删除消息日志
     *
     * @param messageIds
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteMessage(List<Long> messageIds) {
        ValidatorUtil.notEmpty(messageIds);
        ValidatorUtil.isTrue(removeBatchByIds(messageIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }
}
