package top.horsemuzi.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.mapper.AppCategoryRelationMapper;
import top.horsemuzi.system.pojo.dto.open.AppCategoryRelationDTO;
import top.horsemuzi.system.pojo.entity.AppCategoryRelation;
import top.horsemuzi.system.service.AppCategoryRelationService;

import java.util.List;

/**
 * <p>
 * 开放API应用和接口类型关联信息 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-18 22:09:28
 */
@Service
public class AppCategoryRelationServiceImpl extends ExpandServiceImpl<AppCategoryRelationMapper, AppCategoryRelation> implements AppCategoryRelationService {

    /**
     * 保存应用和接口类型关联信息
     *
     * @param appCategoryRelationDTOList
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addAppCategoryRelation(List<AppCategoryRelationDTO> appCategoryRelationDTOList) {
        ValidatorUtil.notEmpty(appCategoryRelationDTOList);
        List<AppCategoryRelation> appCategoryRelationList = OrikaUtil.convertList(appCategoryRelationDTOList, AppCategoryRelation.class);
        ValidatorUtil.isTrue(saveBatch(appCategoryRelationList), ResultCodeEnum.DATA_SAVE_ERROR);
        return true;
    }

    /**
     * 根据应用id删除类型关联信息
     *
     * @param appId
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteAppCategoryRelation(Long appId) {
        ValidatorUtil.notEmpty(appId);
        LambdaQueryWrapper<AppCategoryRelation> wrapper = new LambdaQueryWrapper<AppCategoryRelation>()
                .eq(AppCategoryRelation::getAppId, appId);
        ValidatorUtil.isTrue(remove(wrapper), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 获取应用类型关联信息
     *
     * @param apiCategoryIds
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<AppCategoryRelationDTO> getAppCategoryRelation(List<Long> apiCategoryIds) {
        ValidatorUtil.notEmpty(apiCategoryIds);
        LambdaQueryWrapper<AppCategoryRelation> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(AppCategoryRelation::getApiCategoryId, apiCategoryIds);
        return OrikaUtil.convertList(list(wrapper), AppCategoryRelationDTO.class);
    }

    /**
     * 删除类型关联信息
     *
     * @param appIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteAppCategoryRelation(List<Long> appIds) {
        ValidatorUtil.notEmpty(appIds);
        LambdaQueryWrapper<AppCategoryRelation> wrapper = new LambdaQueryWrapper<AppCategoryRelation>()
                .in(AppCategoryRelation::getAppId, appIds);
        ValidatorUtil.isTrue(remove(wrapper), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

}
