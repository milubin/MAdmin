package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.generate.TableDTO;
import top.horsemuzi.system.pojo.dto.generate.TableQuery;
import top.horsemuzi.system.pojo.entity.Table;

import java.util.List;

/**
 * <p>
 * 代码生成业务表 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-12-19 13:48:53
 */
public interface TableService extends IService<Table> {

    /**
     * 获取业务表详情
     *
     * @param query
     * @return
     */
    List<TableDTO> getTable(TableQuery query);

    /**
     * 分页获取业务表列表
     *
     * @param query
     * @return
     */
    Page<TableDTO> getPageTable(TableQuery query);

    /**
     * 获取业务表信息
     *
     * @param tableId
     * @return
     */
    TableDTO getTable(Long tableId);

    /**
     * 获取业务表信息列表
     *
     * @param tableIds
     * @return
     */
    List<TableDTO> getTable(List<Long> tableIds);

    /**
     * 新增业务表
     *
     * @param tableDTO
     * @return
     */
    boolean addTable(TableDTO tableDTO);

    /**
     * 更新业务表
     *
     * @param tableDTO
     * @return
     */
    boolean updateTable(TableDTO tableDTO);

    /**
     * 业务表是否重复(表名)
     *
     * @param tableDTO
     * @return
     */
    boolean checkTableRepeat(TableDTO tableDTO);

    /**
     * 删除业务表
     *
     * @param tableIds
     * @return
     */
    boolean deleteTable(List<Long> tableIds);

    /**
     * 业务表名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> fetchTable(String search);
}
