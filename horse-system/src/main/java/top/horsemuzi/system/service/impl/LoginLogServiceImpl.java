package top.horsemuzi.system.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.core.office.service.OfficeService;
import top.horsemuzi.system.mapper.LoginLogMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.log.LoginLogDTO;
import top.horsemuzi.system.pojo.dto.log.LoginLogQuery;
import top.horsemuzi.system.pojo.dto.statistics.PieChartDTO;
import top.horsemuzi.system.pojo.entity.LoginLog;
import top.horsemuzi.system.service.LoginLogService;

import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;

/**
 * <p>
 * 系统登录日志记录表 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-09-26 21:50:38
 */
@Service
public class LoginLogServiceImpl extends ExpandServiceImpl<LoginLogMapper, LoginLog> implements LoginLogService {

    /**
     * 分页获取登录日志列表
     *
     * @param query
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public Page<LoginLogDTO> getPageLoginLogs(LoginLogQuery query) {
        Page<LoginLog> page = new Page<>(query.getPage(), query.getSize());
        LambdaQueryWrapper<LoginLog> wrapper = getLoginLogWrapper(query);
        wrapper.select(LoginLog::getId, LoginLog::getTraceId, LoginLog::getThreadName, LoginLog::getUsername, LoginLog::getLoginWay,
                LoginLog::getIp, LoginLog::getArea, LoginLog::getOs, LoginLog::getBrowser, LoginLog::getMobile,
                LoginLog::getStatus, LoginLog::getOrigin, LoginLog::getRemark, LoginLog::getGmtCreate, LoginLog::getCreateId,
                LoginLog::getCreateUser, LoginLog::getModifiedId, LoginLog::getModifiedUser, LoginLog::getGmtModified);
        wrapper.orderByDesc(LoginLog::getGmtCreate);
        return LoginLog.convertPage(page(page, wrapper), LoginLogDTO.class);
    }

    /**
     * 导出系统登录日志(CSV)
     *
     * @param query
     * @param response
     */
    @Override
    public void loginExport(LoginLogQuery query, HttpServletResponse response) {
        LambdaQueryWrapper<LoginLog> wrapper = getLoginLogWrapper(query);
        List<LoginLogDTO> loginLogDTOList = OrikaUtil.convertList(list(wrapper), LoginLogDTO.class);
        try {
            if (CollectionUtil.isNotEmpty(loginLogDTOList)) {
                OfficeService.CSV.export(response, LoginLogDTO.class, loginLogDTOList, "登录日志.csv");
            }
        } catch (Exception e) {
            throw new BusinessException(e, ResultCodeEnum.FILE_EXPORT_ERROR);
        }
    }

    private LambdaQueryWrapper<LoginLog> getLoginLogWrapper(LoginLogQuery query) {
        LambdaQueryWrapper<LoginLog> wrapper = new LambdaQueryWrapper<>();
        if (ObjectUtil.isNotNull(query)) {
            wrapper.eq(StrUtil.isNotBlank(query.getTraceId()), LoginLog::getTraceId, query.getTraceId())
                    .eq(StrUtil.isNotBlank(query.getIp()), LoginLog::getId, query.getIp())
                    .eq(StrUtil.isNotBlank(query.getMobile()), LoginLog::getMobile, query.getMobile())
                    .eq(StrUtil.isNotBlank(query.getStatus()), LoginLog::getStatus, query.getStatus())
                    .eq(ObjectUtil.isNotNull(query.getLoginWay()), LoginLog::getLoginWay, query.getLoginWay())
                    .like(StrUtil.isNotBlank(query.getArea()), LoginLog::getArea, query.getArea())
                    .like(StrUtil.isNotBlank(query.getUsername()), LoginLog::getUsername, query.getUsername())
                    .ge(ObjectUtil.isNotNull(query.getCreateStart()), LoginLog::getGmtCreate, query.getCreateStart())
                    .le(ObjectUtil.isNotNull(query.getCreateEnd()), LoginLog::getGmtCreate, query.getCreateEnd());
        }
        return wrapper;
    }

    /**
     * 获取登录日志成功失败饼图
     *
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<PieChartDTO> statisticsBasePieChartData() {
        return baseMapper.statisticsBasePieChartData();
    }

    /**
     * 获取登录日志隐藏原因饼图
     *
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<PieChartDTO> statisticsPieChartData() {
        return baseMapper.statisticsPieChartData();
    }

    /**
     * 登录日志链路ID实时检索
     *
     * @param search
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<InputRemoteFetchDTO> loginFetch(String search) {
        if (StrUtil.isBlank(search)) {
            return Collections.emptyList();
        }
        return baseMapper.loginFetch(search);
    }

    /**
     * 获取登录日志详情
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public LoginLogDTO getLoginLog(Long id) {
        ValidatorUtil.notEmpty(id);
        return OrikaUtil.convert(getById(id), LoginLogDTO.class);
    }

    /**
     * 保存登录日志
     *
     * @param loginLogDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean saveLoginLog(LoginLogDTO loginLogDTO) {
        ValidatorUtil.notEmpty(loginLogDTO);
        LoginLog loginLog = OrikaUtil.convert(loginLogDTO, LoginLog.class);
        ValidatorUtil.isTrue(save(loginLog), ResultCodeEnum.DATA_SAVE_ERROR);
        loginLogDTO.setId(loginLogDTO.getId());
        return true;
    }

    /**
     * 删除登录日志
     *
     * @param loginLogIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteLoginLog(List<Long> loginLogIds) {
        ValidatorUtil.notEmpty(loginLogIds);
        ValidatorUtil.isTrue(removeBatchByIds(loginLogIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

}
