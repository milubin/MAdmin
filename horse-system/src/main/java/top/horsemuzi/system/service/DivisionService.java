package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.division.DivisionDTO;
import top.horsemuzi.system.pojo.dto.division.DivisionQuery;
import top.horsemuzi.system.pojo.entity.Division;

import java.util.List;

/**
 * <p>
 * 国家行政区划信息表 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-06 10:31:24
 */
public interface DivisionService extends IService<Division> {

    /**
     * 获取行政区划信息
     *
     * @param id
     * @return
     */
    DivisionDTO getDivision(Long id);

    /**
     * 获取行政区划信息列表
     *
     * @param divisionIds
     * @return
     */
    List<DivisionDTO> getDivision(List<Long> divisionIds);

    /**
     * 区划名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> fetch(String search);

    /**
     * 区划是否重复
     *
     * @param divisionDTO
     * @return
     */
    boolean checkDivisionUnique(DivisionDTO divisionDTO);

    /**
     * 父级区划是否存在
     *
     * @param divisionDTO
     * @return
     */
    boolean checkParentDivisionExist(DivisionDTO divisionDTO);

    /**
     * 新增行政区划
     *
     * @param divisionDTO
     * @return
     */
    boolean addDivision(DivisionDTO divisionDTO);

    /**
     * 更新行政区划
     *
     * @param divisionDTO
     * @return
     */
    boolean updateDivision(DivisionDTO divisionDTO);

    /**
     * 是否存在子级区划信息
     *
     * @param divisionDTO
     * @return
     */
    boolean checkSonDivisionExist(DivisionDTO divisionDTO);

    /**
     * 删除区划信息
     *
     * @param divisionIds
     * @return
     */
    boolean deleteDivision(List<Long> divisionIds);

    /**
     * 获取区划信息
     *
     * @param query
     * @return
     */
    List<DivisionDTO> getDivisions(DivisionQuery query);

    /**
     * 获取区划级联数据
     *
     * @param id
     * @return
     */
    List<DivisionDTO> getDivisionCascader(Long id);
}
