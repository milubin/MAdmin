package top.horsemuzi.system.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.mapper.PermissionMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.login.MetaDTO;
import top.horsemuzi.system.pojo.dto.login.RouterDTO;
import top.horsemuzi.system.pojo.dto.permission.PermissionDTO;
import top.horsemuzi.system.pojo.dto.permission.PermissionQuery;
import top.horsemuzi.system.pojo.entity.Permission;
import top.horsemuzi.system.service.PermissionService;

import java.util.*;

/**
 * <p>
 * 菜单权限表 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-07 22:44:27
 */
@Service
public class PermissionServiceImpl extends ExpandServiceImpl<PermissionMapper, Permission> implements PermissionService {

    /**
     * 根据用户id获取用户权限标识信息
     *
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public Set<String> getPerms(Long userId) {
        return baseMapper.getPerms(userId);
    }

    /**
     * 根据用户信息获取Tree接口的菜单路由
     *
     * @param jwtUser
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<RouterDTO> getUserRouters(JwtUser jwtUser) {
        List<PermissionDTO> permissions = baseMapper.getPermissionTree(jwtUser);
        if (CollectionUtil.isNotEmpty(permissions)) {
            return buildRouters(getChildPerms(permissions, Constants.COMMON_CODE.LONG_ZONE));
        }
        return Collections.emptyList();
    }

    /**
     * 构建Tree结构菜单路由
     *
     * @param permissions
     * @return
     */
    private List<RouterDTO> buildRouters(List<PermissionDTO> permissions) {
        List<RouterDTO> routers = new ArrayList<>();
        RouterDTO router = null;
        MetaDTO metaDTO = null;
        for (PermissionDTO permission : permissions) {
            router = new RouterDTO();
            router.setHidden(Constants.COMMON_JUDGMENT.ONE_STR.equals(permission.getVisible()));
            router.setName(getRouteName(permission));
            router.setPath(getRouterPath(permission));
            router.setComponent(getComponent(permission));
            router.setQuery(permission.getRouteQuery());
            metaDTO = new MetaDTO();
            metaDTO.setTitle(permission.getName());
            metaDTO.setIcon(permission.getIcon());
            metaDTO.setNoCache(StrUtil.equals(Constants.COMMON_JUDGMENT.ONE_STR, permission.getCache()));
            metaDTO.setLink(permission.getRoutePath());
            router.setMeta(metaDTO);
            List<PermissionDTO> childrenPermissions = permission.getChildren();
            if (CollectionUtil.isNotEmpty(childrenPermissions) && Constants.MENU.TYPE_DIR.equals(permission.getType())) {
                router.setAlwaysShow(Boolean.TRUE);
                router.setRedirect("noRedirect");
                router.setChildren(buildRouters(childrenPermissions));
            } else if (isMenuFrame(permission)) {
                router.setMeta(null);
                List<RouterDTO> childrenList = new ArrayList<>();
                RouterDTO children = new RouterDTO();
                children.setPath(permission.getRoutePath());
                children.setComponent(permission.getComponent());
                children.setName(StrUtil.upperFirst(permission.getRoutePath()));
                children.setMeta(new MetaDTO(permission.getName(), permission.getIcon(),
                        StrUtil.equals(Constants.COMMON_JUDGMENT.ONE_STR, permission.getCache()),
                        permission.getRoutePath()));
                children.setQuery(permission.getRouteQuery());
                childrenList.add(children);
                router.setChildren(childrenList);
            } else if (permission.getParentId().intValue() == Constants.COMMON_JUDGMENT.ZONE && isInnerLink(permission)) {
                router.setMeta(new MetaDTO().setIcon(permission.getIcon()).setTitle(permission.getName()));
                router.setPath(StrUtil.SLASH);
                List<RouterDTO> childrenList = new ArrayList<>();
                RouterDTO children = new RouterDTO();
                String routerPath = innerLinkReplaceEach(permission.getRoutePath());
                children.setPath(routerPath);
                children.setComponent(Constants.MENU.INNER_LINK);
                children.setName(StrUtil.upperFirst(routerPath));
                children.setMeta(new MetaDTO().setTitle(permission.getName()).setIcon(permission.getIcon()).setLink(permission.getRoutePath()));
                childrenList.add(children);
                router.setChildren(childrenList);
            }
            routers.add(router);
        }
        return routers;
    }

    /**
     * 获取路由名称
     *
     * @param permission 菜单信息
     * @return 路由名称
     */
    public String getRouteName(PermissionDTO permission) {
        String routerName = StrUtil.upperFirst(permission.getRoutePath());
        // 非外链并且是一级目录（类型为目录）
        if (isMenuFrame(permission)) {
            routerName = StrUtil.EMPTY;
        }
        return routerName;
    }

    /**
     * 获取路由地址
     *
     * @param permission 菜单信息
     * @return 路由地址
     */
    public String getRouterPath(PermissionDTO permission) {
        String routerPath = permission.getRoutePath();
        // 内链打开外网方式
        int parentId = permission.getParentId().intValue();
        if (parentId != Constants.COMMON_JUDGMENT.ZONE && isInnerLink(permission)) {
            routerPath = innerLinkReplaceEach(routerPath);
        }
        // 非外链并且是一级目录（类型为目录）
        if (parentId == Constants.COMMON_JUDGMENT.ZONE
                && Constants.MENU.TYPE_DIR.equals(permission.getType())
                && Constants.MENU.NO_FRAME.equals(permission.getFrame())) {
            routerPath = StrUtil.SLASH + permission.getRoutePath();
        }
        // 非外链并且是一级目录（类型为菜单）
        else if (isMenuFrame(permission)) {
            routerPath = StrUtil.SLASH;
        }
        return routerPath;
    }

    /**
     * 获取组件信息
     *
     * @param permission 菜单信息
     * @return 组件信息
     */
    public String getComponent(PermissionDTO permission) {
        String component = Constants.MENU.LAYOUT;
        if (StrUtil.isNotEmpty(permission.getComponent()) && !isMenuFrame(permission)) {
            component = permission.getComponent();
        } else if (StrUtil.isEmpty(permission.getComponent())
                && permission.getParentId().intValue() != Constants.COMMON_JUDGMENT.ZONE
                && isInnerLink(permission)) {
            component = Constants.MENU.INNER_LINK;
        } else if (StrUtil.isEmpty(permission.getComponent()) && isParentView(permission)) {
            component = Constants.MENU.PARENT_VIEW;
        }
        return component;
    }

    /**
     * 是否为菜单内部跳转
     *
     * @param permission 菜单信息
     * @return 结果
     */
    public boolean isMenuFrame(PermissionDTO permission) {
        return permission.getParentId().intValue() == Constants.COMMON_JUDGMENT.ZONE
                && Constants.MENU.TYPE_MENU.equals(permission.getType())
                && Constants.MENU.NO_FRAME.equals(permission.getFrame());
    }

    /**
     * 是否为内链组件
     *
     * @param permission 菜单信息
     * @return 结果
     */
    public boolean isInnerLink(PermissionDTO permission) {
        return permission.getFrame().equals(Constants.MENU.NO_FRAME)
                && StrUtil.startWithAny(permission.getRoutePath(), Constants.MENU.HTTP, Constants.MENU.HTTPS);
    }

    /**
     * 是否为parent_view组件
     *
     * @param permission 菜单信息
     * @return 结果
     */
    public boolean isParentView(PermissionDTO permission) {
        return permission.getParentId().intValue() != Constants.COMMON_JUDGMENT.ZONE
                && Constants.MENU.TYPE_DIR.equals(permission.getType());
    }

    /**
     * 根据父节点的ID获取所有子节点
     *
     * @param permissions 菜单权限路由信息
     * @param parentId    传入的父节点ID,默认:0
     * @return
     */
    public List<PermissionDTO> getChildPerms(List<PermissionDTO> permissions, Long parentId) {
        List<PermissionDTO> returnList = new ArrayList<>();
        for (PermissionDTO t : permissions) {
            if (ObjectUtil.equals(t.getParentId(), parentId)) {
                recursionFn(permissions, t);
                returnList.add(t);
            }
        }
        return returnList;
    }

    /**
     * 递归列表
     *
     * @param list
     * @param t
     */
    private void recursionFn(List<PermissionDTO> list, PermissionDTO t) {
        // 得到子节点列表
        List<PermissionDTO> childList = getChildList(list, t);
        t.setChildren(childList);
        for (PermissionDTO child : childList) {
            if (hasChild(list, child)) {
                recursionFn(list, child);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<PermissionDTO> getChildList(List<PermissionDTO> list, PermissionDTO t) {
        List<PermissionDTO> childList = new ArrayList<>();
        for (PermissionDTO n : list) {
            if (n.getParentId().longValue() == t.getId().longValue()) {
                childList.add(n);
            }
        }
        return childList;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<PermissionDTO> list, PermissionDTO t) {
        return CollectionUtil.isNotEmpty(getChildList(list, t));
    }

    /**
     * 内链域名特殊字符替换为 StrUtil.EMPTY
     *
     * @return
     */
    public String innerLinkReplaceEach(String path) {
        return path.replaceAll(Constants.MENU.HTTPS, StrUtil.EMPTY)
                .replaceAll(Constants.MENU.HTTP, StrUtil.EMPTY);
    }

    /**
     * 获取条件下权限菜单列表(权限菜单管理)
     *
     * @param query
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<PermissionDTO> getPermissionMenus(PermissionQuery query) {
        return baseMapper.getPermissionMenus(query);
    }

    /**
     * 根据权限菜单id获取权限菜单详情
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public PermissionDTO getPermission(Long id) {
        ValidatorUtil.notEmpty(id);
        return getById(id, PermissionDTO.class);
    }

    /**
     * 校验权限菜单名称是否唯一
     *
     * @param permissionDTO
     * @return
     */
    @Override
    public boolean checkPermissionNameUnique(PermissionDTO permissionDTO) {
        ValidatorUtil.notEmpty(permissionDTO);
        LambdaQueryWrapper<Permission> wrapper = new LambdaQueryWrapper<Permission>()
                .eq(Permission::getName, permissionDTO.getName())
                .eq(Permission::getParentId, permissionDTO.getParentId())
                .ne(Objects.nonNull(permissionDTO.getId()), Permission::getId, permissionDTO.getId());
        return ObjectUtil.isNull(getOne(wrapper));
    }

    /**
     * 新增权限菜单
     *
     * @param permissionDTO
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addPermission(PermissionDTO permissionDTO) {
        ValidatorUtil.notEmpty(permissionDTO);
        Permission permission = OrikaUtil.convert(permissionDTO, Permission.class);
        ValidatorUtil.isTrue(save(permission), ResultCodeEnum.DATA_SAVE_ERROR);
        permissionDTO.setId(permission.getId());
        return true;
    }

    /**
     * 根据角色id获取权限菜单数据集合
     *
     * @param roleId
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<PermissionDTO> getPermissionMenusByRoleId(Long roleId) {
        return baseMapper.getPermissionMenusByRoleId(roleId);
    }

    /**
     * 获取权限菜单列表
     *
     * @param permissionIds
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<PermissionDTO> getPermissions(List<Long> permissionIds) {
        return listByIds(permissionIds, PermissionDTO.class);
    }

    /**
     * 更新权限菜单
     *
     * @param permissionDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updatePermission(PermissionDTO permissionDTO) {
        ValidatorUtil.notEmpty(permissionDTO);
        ValidatorUtil.isTrue(updateById(permissionDTO, Permission.class), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 获取权限菜单信息集合
     *
     * @param parentId
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<PermissionDTO> getPermissionByParentId(Long parentId) {
        ValidatorUtil.notEmpty(parentId);
        LambdaQueryWrapper<Permission> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Permission::getParentId, parentId);
        wrapper.eq(Permission::getStatus, Constants.COMMON_JUDGMENT.ZONE_STR);
        return list(wrapper, PermissionDTO.class);
    }

    /**
     * 菜单名称实时检索
     *
     * @param search
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<InputRemoteFetchDTO> fetch(String search) {
        return StrUtil.isBlank(search) ? Collections.emptyList() : baseMapper.fetch(search);
    }

    /**
     * 删除权限菜单
     *
     * @param id
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deletePermission(Long id) {
        ValidatorUtil.notEmpty(id);
        ValidatorUtil.isTrue(removeById(id), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 获取权限菜单最大主键值
     *
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public Long getMaxMenuId() {
        LambdaQueryWrapper<Permission> wrapper = new LambdaQueryWrapper<Permission>()
                .orderByDesc(Permission::getId);
        return getOne(wrapper).getId();
    }

    /**
     * 获取父权限菜单列表(目录、菜单), 不包含按钮数据(主要用于代码生成选择父权限菜单)
     *
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<PermissionDTO> getParentPermissions() {
        LambdaQueryWrapper<Permission> wrapper = new LambdaQueryWrapper<Permission>()
                .in(Permission::getType, Constants.MENU.TYPE_DIR, Constants.MENU.TYPE_MENU);
        return list(wrapper, PermissionDTO.class);
    }

}
