package top.horsemuzi.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ChartNameEnum;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.ExceptionUtil;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.core.office.service.OfficeService;
import top.horsemuzi.system.core.schedule.quartz.base.QuartzConstants;
import top.horsemuzi.system.core.schedule.quartz.service.QuartzHandleService;
import top.horsemuzi.system.core.schedule.task.base.TaskConstants;
import top.horsemuzi.system.core.schedule.task.handle.TaskRegister;
import top.horsemuzi.system.core.schedule.task.handle.TaskRunnable;
import top.horsemuzi.system.mapper.QuartzMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobDTO;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobLogDTO;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobLogQuery;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobQuery;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskDTO;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskLogDTO;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskLogQuery;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskQuery;
import top.horsemuzi.system.pojo.dto.statistics.LineChartDTO;
import top.horsemuzi.system.pojo.dto.statistics.SourceLineData;
import top.horsemuzi.system.pojo.entity.QuartzJobLog;
import top.horsemuzi.system.service.*;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 任务调度服务实现
 *
 * @author mabin
 * @date 2022/06/30 20:08
 **/
@Slf4j
@RequiredArgsConstructor
@Service
public class ScheduleServiceImpl implements ScheduleService {

    private final Scheduler scheduler;
    private final QuartzMapper quartzMapper;
    private final QuartzJobService quartzJobService;
    private final QuartzJobLogService quartzJobLogService;
    private final QuartzHandleService quartzHandleService;
    private final TaskService taskService;
    private final TaskLogService taskLogService;
    private final TaskRegister taskRegister;

    /**
     * 项目启动, 初始化任务调度器
     */
    @PostConstruct
    public void init() {
        try {
            scheduler.start();
        } catch (SchedulerException e) {
            log.error("Quartz scheduler init error! {}", ExceptionUtil.getMessage(e));
        }
    }


    /**
     * 创建Quartz任务调度
     *
     * @param quartzDTO
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addQuartzJob(QuartzJobDTO quartzDTO) {
        ValidatorUtil.notEmpty(quartzDTO);
        // 校验jobName和jobGroup是否重复
        ValidatorUtil.notTrue(quartzJobService.checkRepeat(quartzDTO), ResultCodeEnum.QUARTZ_JOB_REPEAT_ERROR);
        // 默认开始时间
        if (ObjectUtil.isNull(quartzDTO.getStartTime())) {
            quartzDTO.setStartTime(new Date());
        }
        if (quartzJobService.saveQuartz(quartzDTO)) {
            quartzHandleService.addJob(scheduler, quartzDTO);
        }
    }

    /**
     * 暂停Quartz任务调度
     *
     * @param id
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void pauseQuartzJob(Long id) {
        ValidatorUtil.notEmpty(id);
        QuartzJobDTO quartzJobDTO = quartzJobService.getQuartz(id);
        ValidatorUtil.notEmpty(quartzJobDTO, ResultCodeEnum.QUARTZ_JOB_NOT_EXISTS);
        quartzJobDTO.setJobStatus(Constants.COMMON_JUDGMENT.ONE);
        if (quartzJobService.updateQuartz(quartzJobDTO)) {
            try {
                scheduler.pauseJob(new JobKey(quartzJobDTO.getJobName(), quartzJobDTO.getJobGroup()));
            } catch (Exception e) {
                throw new BusinessException(e, ResultCodeEnum.QUARTZ_JOB_PAUSE_ERROR);
            }
        }
    }

    /**
     * 重启Quartz任务调度
     *
     * @param id
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void rebootQuartzJob(Long id) {
        ValidatorUtil.notEmpty(id);
        QuartzJobDTO quartzJobDTO = quartzJobService.getQuartz(id);
        ValidatorUtil.notEmpty(quartzJobDTO, ResultCodeEnum.QUARTZ_JOB_NOT_EXISTS);
        quartzJobDTO.setJobStatus(Constants.COMMON_JUDGMENT.ZONE);
        if (quartzJobService.updateQuartz(quartzJobDTO)) {
            try {
                scheduler.resumeJob(new JobKey(quartzJobDTO.getJobName(), quartzJobDTO.getJobGroup()));
            } catch (Exception e) {
                throw new BusinessException(ResultCodeEnum.QUARTZ_JOB_REBOOT_ERROR);
            }
        }
    }

    /**
     * Quartz任务全部暂停/启动
     *
     * @param execType
     * @return void
     * @author mabin
     * @date 2023/3/1 11:09
     **/
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void allPauseOrRunQuartzJob(Integer execType) {
        ValidatorUtil.notEmpty(execType);
        List<QuartzJobDTO> jobDTOList = OrikaUtil.convertList(quartzJobService.list(), QuartzJobDTO.class);
        ValidatorUtil.notEmpty(jobDTOList, ResultCodeEnum.QUARTZ_JOB_NOT_EXISTS);
        if (ObjectUtil.equal(execType, Constants.COMMON_JUDGMENT.ZONE)) {
            // 全部暂停
            List<QuartzJobDTO> pauseJobList = jobDTOList.stream().filter(job -> ObjectUtil.equal(job.getJobStatus(), Constants.COMMON_JUDGMENT.ZONE)).peek(job -> job.setJobStatus(Constants.COMMON_JUDGMENT.ONE)).collect(Collectors.toList());
            if (CollUtil.isNotEmpty(pauseJobList) && quartzJobService.updateQuartz(pauseJobList)) {
                for (QuartzJobDTO jobDTO : pauseJobList) {
                    try {
                        scheduler.pauseJob(new JobKey(jobDTO.getJobName(), jobDTO.getJobGroup()));
                    } catch (Exception e) {
                        throw new BusinessException(e, ResultCodeEnum.QUARTZ_JOB_PAUSE_ERROR);
                    }
                }
            }
        } else if (ObjectUtil.equal(execType, Constants.COMMON_JUDGMENT.ONE)) {
            // 全部启动
            List<QuartzJobDTO> resumeJobList = jobDTOList.stream().filter(job -> ObjectUtil.equal(job.getJobStatus(), Constants.COMMON_JUDGMENT.ONE)).peek(job -> job.setJobStatus(Constants.COMMON_JUDGMENT.ZONE)).collect(Collectors.toList());
            if (CollUtil.isNotEmpty(resumeJobList) && quartzJobService.updateQuartz(jobDTOList)) {
                for (QuartzJobDTO jobDTO : jobDTOList) {
                    try {
                        scheduler.resumeJob(new JobKey(jobDTO.getJobName(), jobDTO.getJobGroup()));
                    } catch (Exception e) {
                        throw new BusinessException(e, ResultCodeEnum.QUARTZ_JOB_PAUSE_ERROR);
                    }
                }
            }
        }
    }

    /**
     * 更新Quartz任务调度
     *
     * @param quartzDTO
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateQuartzJob(QuartzJobDTO quartzDTO) {
        ValidatorUtil.notEmpty(quartzDTO);
        try {
            // 调度任务是否存在
            QuartzJobDTO currentQuartzDTO = quartzJobService.getQuartz(quartzDTO.getId());
            ValidatorUtil.notEmpty(currentQuartzDTO, ResultCodeEnum.QUARTZ_JOB_NOT_EXISTS);
            // 校验jobName和jobGroup是否重复
            ValidatorUtil.notTrue(quartzJobService.checkRepeat(quartzDTO), ResultCodeEnum.QUARTZ_JOB_REPEAT_ERROR);
            if (quartzJobService.updateQuartz(quartzDTO)) {
                // 当前被修改的job任务存在则进行删除
                JobKey jobKey = new JobKey(currentQuartzDTO.getJobName(), currentQuartzDTO.getJobGroup());
                if (scheduler.checkExists(jobKey)) {
                    scheduler.deleteJob(jobKey);
                }
                quartzHandleService.addJob(scheduler, quartzDTO);
            }
        } catch (SchedulerException e) {
            log.error(ExceptionUtil.getMessage(e));
            throw new BusinessException(ResultCodeEnum.QUARTZ_JOB_UPDATE_ERROR);
        }
    }

    /**
     * 删除Quartz任务调度
     *
     * @param jobIds
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteQuartzJob(List<Long> jobIds) {
        ValidatorUtil.notEmpty(jobIds);
        List<QuartzJobDTO> quartzJobDTOList = quartzJobService.getQuartz(jobIds);
        if (CollectionUtil.isNotEmpty(quartzJobDTOList)) {
            try {
                quartzJobService.deleteQuartz(jobIds);
                for (QuartzJobDTO quartzJobDTO : quartzJobDTOList) {
                    // 暂停触发器
                    TriggerKey triggerKey = TriggerKey.triggerKey(quartzJobDTO.getTriggerName(), quartzJobDTO.getTriggerGroup());
                    scheduler.pauseTrigger(triggerKey);
                    // 移除触发器中绑定的任务
                    scheduler.unscheduleJob(triggerKey);
                    // 删除任务
                    scheduler.deleteJob(JobKey.jobKey(quartzJobDTO.getJobName(), quartzJobDTO.getJobGroup()));
                }
            } catch (Exception e) {
                log.error(ExceptionUtil.getMessage(e));
                throw new BusinessException(ResultCodeEnum.QUARTZ_JOB_DELETE_ERROR);
            }
        }
    }

    /**
     * 获取调度任务数量
     *
     * @param quartzQuery
     * @return
     */
    @Override
    public Long getQuartzCount(QuartzJobQuery quartzQuery) {
        ValidatorUtil.notEmpty(quartzQuery);
        return quartzMapper.getQuartzCount(quartzQuery);
    }

    /**
     * 查询Quartz任务调度信息
     *
     * @param id
     * @return
     */
    @Override
    public QuartzJobDTO getQuartzJob(Long id) {
        ValidatorUtil.notEmpty(id);
        return quartzJobService.getQuartz(id);
    }

    /**
     * 清理Quartz任务调度日志信息
     *
     * @param quartzLogIds
     */
    @Override
    public void clearQuartzLogs(List<Long> quartzLogIds) {
        ValidatorUtil.notEmpty(quartzLogIds);
        quartzJobLogService.deleteLog(quartzLogIds);
    }

    /**
     * 分页获取Spring任务调度日志列表
     *
     * @param query
     * @return
     */
    @Override
    public Page<TaskLogDTO> getPageTaskLog(TaskLogQuery query) {
        return taskLogService.getPageTaskLog(query);
    }

    /**
     * 获取Quartz调度任务日志统计数据
     *
     * @return
     */
    @Override
    public Map<String, Object> statisticsQuartzLog() {
        Map<String, Object> chartMap = new HashMap<>(16);
        // 饼图
        chartMap.put(ChartNameEnum.PIE.getName(), quartzJobLogService.statisticsPieChartData());
        // 折线图
        List<SourceLineData> sourceLineDataList = quartzJobLogService.statisticsLineChartData();
        LineChartDTO lineChartDTO = new LineChartDTO();
        if (CollectionUtil.isNotEmpty(sourceLineDataList)) {
            List<String> dateList = new ArrayList<>();
            for (int i = Constants.COMMON_CODE.SIX; i >= 0; i--) {
                dateList.add(DateUtil.format(DateUtil.offsetDay(new Date(), -i), DatePattern.NORM_DATE_FORMAT));
            }
            lineChartDTO.setDateList(dateList);
            Map<String, List<SourceLineData>> sourceLineMap = sourceLineDataList.stream().collect(Collectors.groupingBy(SourceLineData::getName));
            List<Long> dataSuccessList = new ArrayList<>();
            List<Long> dataFailList = new ArrayList<>();
            for (String date : dateList) {
                List<SourceLineData> sourceLineData = sourceLineMap.get(date);
                if (CollectionUtil.isEmpty(sourceLineData)) {
                    dataSuccessList.add(Constants.COMMON_CODE.LONG_ZONE);
                    dataFailList.add(Constants.COMMON_CODE.LONG_ZONE);
                } else {
                    if (sourceLineData.size() == Constants.COMMON_CODE.TWO) {
                        sourceLineData.forEach(item -> {
                            if (StrUtil.equals(item.getFlag(), Constants.COMMON_JUDGMENT.ZONE_STR)) {
                                dataFailList.add(item.getValue());
                            } else {
                                dataSuccessList.add(item.getValue());
                            }
                        });
                    } else {
                        SourceLineData sourceData = sourceLineData.get(0);
                        if (StrUtil.equals(sourceData.getFlag(), Constants.COMMON_JUDGMENT.ZONE_STR)) {
                            dataFailList.add(sourceData.getValue());
                        } else {
                            dataSuccessList.add(sourceData.getValue());
                        }
                    }
                }
            }
            List<List<Long>> dataList = new ArrayList<>();
            dataList.add(dataFailList);
            dataList.add(dataSuccessList);
            lineChartDTO.setDataList(dataList);
        }
        chartMap.put(ChartNameEnum.LINE.getName(), lineChartDTO);
        return chartMap;
    }

    /**
     * 获取Spring调度任务日志统计数据
     *
     * @return
     */
    @Override
    public Map<String, Object> statisticsTaskLog() {
        Map<String, Object> chartMap = new HashMap<>(16);
        // 饼图
        chartMap.put(ChartNameEnum.PIE.getName(), taskLogService.statisticsPieChartData());
        // 折线图
        List<SourceLineData> sourceLineDataList = taskLogService.statisticsLineChartData();
        LineChartDTO lineChartDTO = new LineChartDTO();
        if (CollectionUtil.isNotEmpty(sourceLineDataList)) {
            List<String> dateList = new ArrayList<>();
            for (int i = Constants.COMMON_CODE.SIX; i >= 0; i--) {
                dateList.add(DateUtil.format(DateUtil.offsetDay(new Date(), -i), DatePattern.NORM_DATE_FORMAT));
            }
            lineChartDTO.setDateList(dateList);
            Map<String, List<SourceLineData>> sourceLineMap = sourceLineDataList.stream().collect(Collectors.groupingBy(SourceLineData::getName));
            List<Long> dataSuccessList = new ArrayList<>();
            List<Long> dataFailList = new ArrayList<>();
            for (String date : dateList) {
                List<SourceLineData> sourceLineData = sourceLineMap.get(date);
                if (CollectionUtil.isEmpty(sourceLineData)) {
                    dataSuccessList.add(Constants.COMMON_CODE.LONG_ZONE);
                    dataFailList.add(Constants.COMMON_CODE.LONG_ZONE);
                } else {
                    if (sourceLineData.size() == Constants.COMMON_CODE.TWO) {
                        sourceLineData.forEach(item -> {
                            if (StrUtil.equals(item.getFlag(), Constants.COMMON_JUDGMENT.ZONE_STR)) {
                                dataFailList.add(item.getValue());
                            } else {
                                dataSuccessList.add(item.getValue());
                            }
                        });
                    } else {
                        SourceLineData sourceData = sourceLineData.get(0);
                        if (StrUtil.equals(sourceData.getFlag(), Constants.COMMON_JUDGMENT.ZONE_STR)) {
                            dataFailList.add(sourceData.getValue());
                        } else {
                            dataSuccessList.add(sourceData.getValue());
                        }
                    }
                }
            }
            List<List<Long>> dataList = new ArrayList<>();
            dataList.add(dataFailList);
            dataList.add(dataSuccessList);
            lineChartDTO.setDataList(dataList);
        }
        chartMap.put(ChartNameEnum.LINE.getName(), lineChartDTO);
        return chartMap;
    }

    /**
     * 删除Spring调度任务
     *
     * @param taskIds
     */
    @Override
    public void deleteTask(List<Long> taskIds) {
        ValidatorUtil.notEmpty(taskIds);
        List<TaskDTO> taskDTOList = taskService.getTasks(taskIds);
        ValidatorUtil.notEmpty(taskDTOList, ResultCodeEnum.TASK_JOB_NOT_EXISTS);
        if (taskService.deleteTask(taskIds)) {
            for (TaskDTO taskDTO : taskDTOList) {
                taskRegister.removeTask(getRunnable(taskDTO.getClassName(), taskDTO.getMethodName(), taskDTO.getMethodParams()));
            }
        }
    }

    /**
     * 切换Spring调度任务状态
     *
     * @param taskDTO
     */
    @Override
    public void switchTaskStatus(TaskDTO taskDTO) {
        ValidatorUtil.notEmpty(taskDTO);
        // 调度任务是否存在
        TaskDTO currentTaskDTO = taskService.getTask(taskDTO.getId());
        // 调度任务cron表达式
        ValidatorUtil.isTrue(CronExpression.isValidExpression(taskDTO.getCron()), ResultCodeEnum.CRON_EXPRESSION_ERROR);
        // 根据调度任务的状态进行暂停和重启(暂停: 移除调度任务, 重启: 重新注册调度任务)
        currentTaskDTO.setStatus(taskDTO.getStatus());
        if (taskService.updateTask(currentTaskDTO)) {
            TaskRunnable task = getRunnable(taskDTO.getClassName(), taskDTO.getMethodName(), taskDTO.getMethodParams());
            if (ObjectUtil.equals(taskDTO.getStatus(), Constants.COMMON_JUDGMENT.ZONE)) {
                // 暂停任务
                taskRegister.removeTask(task);
            } else {
                // 重启任务
                taskRegister.addTask(task, taskDTO.getCron());
            }
        }
    }

    /**
     * Spring调度任务全部暂停/启动
     *
     * @param execType
     * @return void
     * @author mabin
     * @date 2023/3/1 13:42
     **/
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void allPauseOrRunTask(Integer execType) {
        ValidatorUtil.notEmpty(execType);
        List<TaskDTO> taskDTOList = OrikaUtil.convertList(taskService.list(), TaskDTO.class);
        ValidatorUtil.notEmpty(taskDTOList, ResultCodeEnum.TASK_JOB_NOT_EXISTS);
        if (ObjectUtil.equal(execType, Constants.COMMON_JUDGMENT.ZONE)) {
            List<TaskDTO> parseTaskList = taskDTOList.stream().filter(task -> ObjectUtil.equal(task.getStatus(), Constants.COMMON_JUDGMENT.ONE)).peek(task -> task.setStatus(Constants.COMMON_JUDGMENT.ZONE)).collect(Collectors.toList());
            if (CollUtil.isNotEmpty(parseTaskList)) {
                if (taskService.updateTask(parseTaskList)) {
                    for (TaskDTO taskDTO : parseTaskList) {
                        taskRegister.removeTask(getRunnable(taskDTO.getClassName(), taskDTO.getMethodName(), taskDTO.getMethodParams()));
                    }
                }
            }
        } else if (ObjectUtil.equal(execType, Constants.COMMON_JUDGMENT.ONE)) {
            List<TaskDTO> addTaskList = taskDTOList.stream().filter(task -> ObjectUtil.equal(task.getStatus(), Constants.COMMON_JUDGMENT.ZONE)).peek(task -> task.setStatus(Constants.COMMON_JUDGMENT.ONE)).collect(Collectors.toList());
            if (CollUtil.isNotEmpty(addTaskList)) {
                if (taskService.updateTask(addTaskList)) {
                    for (TaskDTO taskDTO : addTaskList) {
                        ValidatorUtil.isTrue(CronExpression.isValidExpression(taskDTO.getCron()), ResultCodeEnum.CRON_EXPRESSION_ERROR);
                        taskRegister.addTask(getRunnable(taskDTO.getClassName(), taskDTO.getMethodName(), taskDTO.getMethodParams()), taskDTO.getCron());
                    }
                }
            }
        }
    }

    /**
     * 更新Spring调度任务(只允许更新cron表达式)
     *
     * @param taskDTO
     */
    @Override
    public void updateTask(TaskDTO taskDTO) {
        ValidatorUtil.notEmpty(taskDTO);
        // 调度任务是否存在
        TaskDTO currentTaskDTO = taskService.getTask(taskDTO.getId());
        ValidatorUtil.notEmpty(currentTaskDTO, ResultCodeEnum.QUARTZ_JOB_NOT_EXISTS);
        // 校验调度任务是否重复
        ValidatorUtil.notTrue(taskService.checkRepeat(taskDTO), ResultCodeEnum.TASK_JOB_REPEAT_ERROR);
        // 校验cron表达式
        ValidatorUtil.isTrue(CronExpression.isValidExpression(taskDTO.getCron()), ResultCodeEnum.CRON_EXPRESSION_ERROR);
        // 更新调度任务 (先删除再重新注册)
        currentTaskDTO.setCron(taskDTO.getCron());
        currentTaskDTO.setTaskName(taskDTO.getTaskName());
        currentTaskDTO.setRemark(taskDTO.getRemark());
        if (taskService.updateTask(currentTaskDTO)) {
            TaskRunnable task = getRunnable(taskDTO.getClassName(), taskDTO.getMethodName(), taskDTO.getMethodParams());
            taskRegister.removeTask(task);
            taskRegister.addTask(task, taskDTO.getCron());
        }
    }

    /**
     * 清理Spring任务调度日志信息
     *
     * @param taskLogIds
     */
    @Override
    public void clearTaskLogs(List<Long> taskLogIds) {
        ValidatorUtil.notEmpty(taskLogIds);
        taskLogService.clearTaskLogs(taskLogIds);
    }

    /**
     * 新增Spring调度任务
     *
     * @param taskDTO
     */
    @Override
    public void addTask(TaskDTO taskDTO) {
        ValidatorUtil.notEmpty(taskDTO);
        // 校验调度任务是否重复
        ValidatorUtil.notTrue(taskService.checkRepeat(taskDTO), ResultCodeEnum.TASK_JOB_REPEAT_ERROR);
        // 保存并注册任务
        if (taskService.addTask(taskDTO)) {
            ValidatorUtil.isTrue(CronExpression.isValidExpression(taskDTO.getCron()), ResultCodeEnum.CRON_EXPRESSION_ERROR);
            if (ObjectUtil.equals(taskDTO.getStatus(), Constants.COMMON_JUDGMENT.ONE)) {
                taskRegister.addTask(getRunnable(taskDTO.getClassName(), taskDTO.getMethodName(), taskDTO.getMethodParams()), taskDTO.getCron());
            }
        }
    }

    /**
     * 获取执行任务实例信息
     *
     * @param className
     * @param methodName
     * @param methodParams
     * @return
     */
    private TaskRunnable getRunnable(String className, String methodName, String methodParams) {
        TaskRunnable runnable = null;
        if (StrUtil.isNotBlank(methodParams)) {
            runnable = new TaskRunnable(className, methodName, methodParams);
        } else {
            runnable = new TaskRunnable(className, methodName);
        }
        return runnable;
    }

    /**
     * 分页获取Spring任务调度列表
     *
     * @param query
     * @return
     */
    @Override
    public Page<TaskDTO> getPageTasks(TaskQuery query) {
        return taskService.getPageTasks(query);
    }

    /**
     * 分页获取Quartz任务调度日志列表
     *
     * @param query
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public Page<QuartzJobLogDTO> getPageQuartzLog(QuartzJobLogQuery query) {
        Page<QuartzJobLog> page = new Page<>(query.getPage(), query.getSize());
        LambdaQueryWrapper<QuartzJobLog> wrapper = getQuartzJobLogWrapper(query);
        return QuartzJobLog.convertPage(quartzJobLogService.page(page, wrapper), QuartzJobLogDTO.class);
    }

    /**
     * 导出Spring调度任务日志
     *
     * @param query
     * @param response
     */
    @Override
    public void exportTaskLog(TaskLogQuery query, HttpServletResponse response) {
        taskLogService.exportTaskLog(query, response);
    }

    /**
     * Spring任务名称实时检索
     *
     * @param search
     * @return
     */
    @Override
    public List<InputRemoteFetchDTO> taskFetch(String search) {
        return taskService.taskFetch(search);
    }

    /**
     * Quartz任务名称实时检索
     *
     * @param search
     * @return
     */
    @Override
    public List<InputRemoteFetchDTO> quartzFetch(String search) {
        return quartzMapper.quartzFetch(search);
    }

    /**
     * 导出Quartz调度任务日志
     *
     * @param query
     * @param response
     */
    @Override
    public void exportQuartzLog(QuartzJobLogQuery query, HttpServletResponse response) {
        List<QuartzJobLogDTO> quartzJobLogDTOList = OrikaUtil.convertList(quartzJobLogService.list(getQuartzJobLogWrapper(query)), QuartzJobLogDTO.class);
        if (CollectionUtil.isNotEmpty(quartzJobLogDTOList)) {
            try {
                OfficeService.CSV.export(response, QuartzJobLogDTO.class, quartzJobLogDTOList, "Quartz调度任务日志.csv");
            } catch (Exception e) {
                throw new BusinessException(ResultCodeEnum.FILE_EXPORT_ERROR);
            }
        }
    }

    private LambdaQueryWrapper<QuartzJobLog> getQuartzJobLogWrapper(QuartzJobLogQuery query) {
        LambdaQueryWrapper<QuartzJobLog> wrapper = new LambdaQueryWrapper<>();
        if (ObjectUtil.isNotNull(query)) {
            wrapper.eq(ObjectUtil.isNotNull(query.getJobId()), QuartzJobLog::getJobId, query.getJobId()).ge(ObjectUtil.isNotNull(query.getCreateStart()), QuartzJobLog::getGmtCreate, query.getCreateStart()).le(ObjectUtil.isNotNull(query.getCreateEnd()), QuartzJobLog::getGmtCreate, query.getCreateEnd()).orderByDesc(QuartzJobLog::getGmtCreate);
        }
        return wrapper;
    }

    /**
     * 任务立刻运行一次
     *
     * @param id
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void runOnceQuartzJob(Long id) {
        ValidatorUtil.notEmpty(id);
        QuartzJobDTO quartzJobDTO = quartzJobService.getQuartz(id);
        ValidatorUtil.notEmpty(quartzJobDTO, ResultCodeEnum.QUARTZ_JOB_NOT_EXISTS);
        try {
            JobDataMap jobDataMap = new JobDataMap();
            jobDataMap.put(QuartzConstants.QUARTZ_JOB, JsonUtil.toJson(quartzJobDTO));
            JobKey jobKey = JobKey.jobKey(quartzJobDTO.getJobName(), quartzJobDTO.getJobGroup());
            scheduler.triggerJob(jobKey, jobDataMap);
        } catch (Exception e) {
            log.error(ExceptionUtil.getMessage(e));
            throw new BusinessException(ResultCodeEnum.QUARTZ_JOB_RUN_ONCE_ERROR);
        }
    }

    /**
     * 获取调度任务信息
     *
     * @param quartzQuery
     * @return
     */
    @Override
    public List<QuartzJobDTO> getQuartzList(QuartzJobQuery quartzQuery) {
        ValidatorUtil.notEmpty(quartzQuery);
        quartzQuery.setPage((quartzQuery.getPage() - 1) * quartzQuery.getSize());
        return quartzMapper.getQuartzList(quartzQuery);
    }

    /**
     * 获取Spring调度任务
     *
     * @param id
     * @return
     */
    @Override
    public TaskDTO getTask(Long id) {
        ValidatorUtil.notEmpty(id);
        return taskService.getTask(id);
    }

    /**
     * Spring调度任务服务启动初始化
     */
    @Async("async")
    @Override
    public void asyncInit() {
        TaskDTO query = new TaskDTO();
        query.setStatus(Constants.COMMON_JUDGMENT.ONE);
        List<TaskDTO> taskDTOList = taskService.getTasks(query);
        if (CollectionUtil.isEmpty(taskDTOList)) {
            log.info("{} Currently, there are no Spring scheduling tasks to be initialized", TaskConstants.TASK_HEADER);
        }
        for (TaskDTO taskDTO : taskDTOList) {
            if (!CronExpression.isValidExpression(taskDTO.getCron())) {
                log.warn("{} Spring Task id={}, cron={} illegal Expression, please Check And Handle In Time!", TaskConstants.TASK_HEADER, taskDTO.getId(), taskDTO.getCron());
                continue;
            }
            TaskRunnable runnable = null;
            if (StrUtil.isNotBlank(taskDTO.getMethodParams())) {
                runnable = new TaskRunnable(taskDTO.getClassName(), taskDTO.getMethodName(), taskDTO.getMethodParams());
            } else {
                runnable = new TaskRunnable(taskDTO.getClassName(), taskDTO.getMethodName());
            }
            taskRegister.addTask(runnable, taskDTO.getCron());
            log.info("{} Spring Task id={}, className={}, methodName={}, methodParams={} register success", TaskConstants.TASK_HEADER, taskDTO.getId(), taskDTO.getClassName(), taskDTO.getMethodName(), taskDTO.getMethodParams());
        }
    }

}
