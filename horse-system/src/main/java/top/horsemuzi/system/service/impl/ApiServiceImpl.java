package top.horsemuzi.system.service.impl;

import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.core.office.service.OfficeService;
import top.horsemuzi.system.handle.excel.ExcelApiImportListener;
import top.horsemuzi.system.mapper.ApiMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.open.api.ApiDTO;
import top.horsemuzi.system.pojo.dto.open.api.ApiExportDTO;
import top.horsemuzi.system.pojo.dto.open.api.ApiImportDTO;
import top.horsemuzi.system.pojo.dto.open.api.ApiQuery;
import top.horsemuzi.system.pojo.entity.Api;
import top.horsemuzi.system.service.ApiService;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>
 * 开放API应用接口信息 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-18 22:09:28
 */
@Service
public class ApiServiceImpl extends ExpandServiceImpl<ApiMapper, Api> implements ApiService {

    /**
     * 获取API接口信息
     *
     * @param apiCategoryIds
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<ApiDTO> getApiByCategory(List<Long> apiCategoryIds) {
        ValidatorUtil.notEmpty(apiCategoryIds);
        LambdaQueryWrapper<Api> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(Api::getApiCategoryId, apiCategoryIds);
        return OrikaUtil.convertList(list(wrapper), ApiDTO.class);
    }

    /**
     * 分页获取开放API接口列表
     *
     * @param query
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public Page<ApiDTO> getPageApis(ApiQuery query) {
        Page<Api> page = new Page<>(query.getPage(), query.getSize());
        LambdaQueryWrapper<Api> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StrUtil.isNotBlank(query.getApiStatus()), Api::getApiStatus, query.getApiStatus())
                .like(StrUtil.isNotBlank(query.getApiCode()), Api::getApiCode, query.getApiCode())
                .like(StrUtil.isNotBlank(query.getApiFunction()), Api::getApiFunction, query.getApiFunction())
                .orderByDesc(Api::getGmtCreate);
        return Api.convertPage(page(page, wrapper), ApiDTO.class);
    }

    /**
     * 获取开放API接口详情
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public ApiDTO getApi(Long id) {
        ValidatorUtil.notEmpty(id);
        return OrikaUtil.convert(getById(id), ApiDTO.class);
    }

    /**
     * 接口是否重复
     *
     * @param apiDTO
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public boolean checkApiRepeat(ApiDTO apiDTO) {
        ValidatorUtil.notEmpty(apiDTO);
        LambdaQueryWrapper<Api> wrapper = new LambdaQueryWrapper<>();
        wrapper.ne(ObjectUtil.isNotNull(apiDTO.getId()), Api::getId, apiDTO.getId())
                .and(StrUtil.isNotBlank(apiDTO.getApiCode()) || StrUtil.isNotBlank(apiDTO.getApiFunction()),
                        andWrapper -> andWrapper.eq(StrUtil.isNotBlank(apiDTO.getApiCode()), Api::getApiCode, apiDTO.getApiCode())
                                .or().eq(StrUtil.isNotBlank(apiDTO.getApiFunction()), Api::getApiFunction, apiDTO.getApiFunction()))
                .eq(StrUtil.isNotBlank(apiDTO.getApiClass()), Api::getApiClass, apiDTO.getApiClass())
                .eq(StrUtil.isNotBlank(apiDTO.getApiMethod()), Api::getApiMethod, apiDTO.getApiMethod());
        return ObjectUtil.isNull(getOne(wrapper));
    }

    /**
     * 保存API接口
     *
     * @param apiDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addApi(ApiDTO apiDTO) {
        ValidatorUtil.notEmpty(apiDTO);
        Api api = OrikaUtil.convert(apiDTO, Api.class);
        ValidatorUtil.isTrue(save(api), ResultCodeEnum.DATA_SAVE_ERROR);
        apiDTO.setId(api.getId());
        return true;
    }

    /**
     * 删除开放API接口
     *
     * @param apiIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteApi(List<Long> apiIds) {
        ValidatorUtil.notEmpty(apiIds);
        ValidatorUtil.isTrue(removeBatchByIds(apiIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 接口名称实时检索
     *
     * @param search
     * @return
     */
    @Override
    public List<InputRemoteFetchDTO> fetch(String search) {
        return baseMapper.fetch(search);
    }

    /**
     * API接口数据导出
     *
     * @param query
     * @param response
     */
    @Override
    public void export(ApiQuery query, HttpServletResponse response) {
        List<ApiExportDTO> apiExportDTOList = baseMapper.getApiExportDTOList(query);
        try {
            if (CollectionUtil.isNotEmpty(apiExportDTOList)) {
                OfficeService.EXCEL_POI.export(response, ApiExportDTO.class, apiExportDTOList,
                        "API接口数据.xlsx", "开放平台API接口信息", "API接口信息", ExcelType.XSSF);
            }
        } catch (Exception e) {
            throw new BusinessException(e, ResultCodeEnum.FILE_EXPORT_ERROR);
        }
    }

    /**
     * 获取API接口详情
     *
     * @param apiCode
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public ApiDTO getApi(String apiCode) {
        ValidatorUtil.notEmpty(apiCode);
        LambdaQueryWrapper<Api> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Api::getApiCode, apiCode);
        return OrikaUtil.convert(getOne(wrapper), ApiDTO.class);
    }

    /**
     * 批量导入API接口数据
     *
     * @param file
     * @param jwtUser
     */
    @Override
    public void imports(MultipartFile file, JwtUser jwtUser) {
        ValidatorUtil.notEmpty(file);
        try {
            OfficeService.EXCEL.importExcel(file.getInputStream(), ApiImportDTO.class, new ExcelApiImportListener(jwtUser));
        } catch (Exception e) {
            throw new BusinessException(e, ResultCodeEnum.FILE_IMPORT_ERROR);
        }
    }

    /**
     * 新增API接口
     *
     * @param apiDTOList
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addApi(List<ApiDTO> apiDTOList) {
        ValidatorUtil.notEmpty(apiDTOList);
        List<Api> apiList = OrikaUtil.convertList(apiDTOList, Api.class);
        ValidatorUtil.isTrue(saveBatch(apiList), ResultCodeEnum.DATA_SAVE_ERROR);
        return true;
    }

    /**
     * 获取API接口列表
     *
     * @param apiIds
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<ApiDTO> getApi(List<Long> apiIds) {
        ValidatorUtil.notEmpty(apiIds);
        return OrikaUtil.convertList(listByIds(apiIds), ApiDTO.class);
    }

    /**
     * 更新API接口
     *
     * @param apiDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateApi(ApiDTO apiDTO) {
        ValidatorUtil.notEmpty(apiDTO);
        ValidatorUtil.notEmpty(apiDTO.getId());
        ValidatorUtil.isTrue(updateById(OrikaUtil.convert(apiDTO, Api.class)), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

}
