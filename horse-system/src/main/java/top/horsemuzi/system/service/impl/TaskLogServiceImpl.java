package top.horsemuzi.system.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.core.office.service.OfficeService;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.mapper.TaskLogMapper;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskLogDTO;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskLogQuery;
import top.horsemuzi.system.pojo.dto.statistics.PieChartDTO;
import top.horsemuzi.system.pojo.dto.statistics.SourceLineData;
import top.horsemuzi.system.pojo.entity.TaskLog;
import top.horsemuzi.system.service.TaskLogService;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-08-20 17:56:39
 */
@Service
public class TaskLogServiceImpl extends ExpandServiceImpl<TaskLogMapper, TaskLog> implements TaskLogService {

    /**
     * 分页获取Spring任务调度日志列表
     *
     * @param query
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public Page<TaskLogDTO> getPageTaskLog(TaskLogQuery query) {
        ValidatorUtil.notEmpty(query);
        ValidatorUtil.notEmpty(query.getTaskId());
        Page<TaskLog> page = new Page<>(query.getPage(), query.getSize());
        LambdaQueryWrapper<TaskLog> wrapper = getTaskLogWrapper(query);
        return TaskLog.convertPage(page(page, wrapper), TaskLogDTO.class);
    }

    /**
     * 新增Spring调度日志
     *
     * @param taskLogDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addTaskLog(TaskLogDTO taskLogDTO) {
        ValidatorUtil.notEmpty(taskLogDTO);
        TaskLog taskLog = OrikaUtil.convert(taskLogDTO, TaskLog.class);
        ValidatorUtil.isTrue(save(taskLog), ResultCodeEnum.DATA_SAVE_ERROR);
        taskLogDTO.setId(taskLog.getId());
        return true;
    }

    /**
     * 导出Spring调度任务日志
     *
     * @param query
     * @param response
     */
    @Override
    public void exportTaskLog(TaskLogQuery query, HttpServletResponse response) {
        List<TaskLogDTO> taskLogDTOList = OrikaUtil.convertList(list(getTaskLogWrapper(query)), TaskLogDTO.class);
        try {
            if (CollectionUtil.isNotEmpty(taskLogDTOList)) {
                OfficeService.CSV.export(response, TaskLogDTO.class, taskLogDTOList, "Task调度任务日志.csv");
            }
        } catch (Exception e) {
            throw new BusinessException( ResultCodeEnum.FILE_EXPORT_ERROR);
        }
    }

    private LambdaQueryWrapper<TaskLog> getTaskLogWrapper(TaskLogQuery query) {
        LambdaQueryWrapper<TaskLog> wrapper = new LambdaQueryWrapper<>();
        if (ObjectUtil.isNotNull(query)) {
            wrapper.eq(TaskLog::getTaskId, query.getTaskId())
                    .ge(ObjectUtil.isNotNull(query.getCreateStart()), TaskLog::getGmtCreate, query.getCreateStart())
                    .le(ObjectUtil.isNotNull(query.getCreateEnd()), TaskLog::getGmtCreate, query.getCreateEnd())
                    .orderByDesc(TaskLog::getGmtCreate);
        }
        return wrapper;
    }

    /**
     * 获取Spring调度任务日志的折线图统计数据
     *
     * @return
     */
    @Override
    public List<SourceLineData> statisticsLineChartData() {
        return baseMapper.statisticsLineChartData();
    }

    /**
     * 获取Spring调度任务日志的饼图统计数据
     *
     * @return
     */
    @Override
    public List<PieChartDTO> statisticsPieChartData() {
        return baseMapper.statisticsPieChartData();
    }

    /**
     * 清理Spring任务调度日志信息
     *
     * @param taskLogIds
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean clearTaskLogs(List<Long> taskLogIds) {
        ValidatorUtil.notEmpty(taskLogIds);
        ValidatorUtil.isTrue(removeBatchByIds(taskLogIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }
}
