package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.user.ShortcutMenuDTO;
import top.horsemuzi.system.pojo.entity.ShortcutMenu;

import java.util.List;

/**
 * <p>
 * 用户快捷菜单信息表 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2023-01-07 15:26:14
 */
public interface ShortcutMenuService extends IService<ShortcutMenu> {

    /**
     * 获取用户快捷菜单列表
     *
     * @param userId 用户id
     * @return
     */
    List<ShortcutMenuDTO> getShortcutMenuByUserId(Long userId);

    /**
     * 获取用户快捷菜单详情
     *
     * @param id 主键id
     * @return
     */
    ShortcutMenuDTO getShortcutMenu(Long id);

    /**
     * 新增快捷菜单
     *
     * @param shortcutMenuDTO
     * @return
     */
    boolean addShortcutMenu(ShortcutMenuDTO shortcutMenuDTO);

    /**
     * 更新快捷菜单
     *
     * @param shortcutMenuDTO
     * @return
     */
    boolean updateShortcutMenu(ShortcutMenuDTO shortcutMenuDTO);

    /**
     * 删除快捷菜单
     *
     * @param shortcutMenuIds
     * @return
     */
    boolean deleteShortcutMenu(List<Long> shortcutMenuIds);

    /**
     * 快捷菜单是否重复
     *
     * @param shortcutMenuDTO
     * @return
     */
    boolean checkShortcutMenuRepeat(ShortcutMenuDTO shortcutMenuDTO);
}
