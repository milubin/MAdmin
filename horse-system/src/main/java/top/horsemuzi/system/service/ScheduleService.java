package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobDTO;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobLogDTO;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobLogQuery;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobQuery;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskDTO;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskLogDTO;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskLogQuery;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskQuery;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 任务调度服务
 *
 * @author mabin
 * @date 2022/06/30 20:08
 **/

public interface ScheduleService {

    /**
     * 创建Quartz任务调度
     *
     * @param quartzDTO
     */
    void addQuartzJob(QuartzJobDTO quartzDTO);

    /**
     * 暂停Quartz任务调度
     *
     * @param id
     */
    void pauseQuartzJob(Long id);

    /**
     * 重启Quartz任务调度
     *
     * @param id
     */
    void rebootQuartzJob(Long id);

    /**
     * 更新Quartz任务调度
     *
     * @param quartzDTO
     */
    void updateQuartzJob(QuartzJobDTO quartzDTO);

    /**
     * 删除Quartz任务调度
     *
     * @param jobIds
     */
    void deleteQuartzJob(List<Long> jobIds);

    /**
     * 获取调度任务数量
     *
     * @param quartzQuery
     * @return
     */
    Long getQuartzCount(QuartzJobQuery quartzQuery);

    /**
     * 获取调度任务信息
     *
     * @param quartzQuery
     * @return
     */
    List<QuartzJobDTO> getQuartzList(QuartzJobQuery quartzQuery);

    /**
     * 查询Quartz任务调度信息
     *
     * @param id
     * @return
     */
    QuartzJobDTO getQuartzJob(Long id);

    /**
     * 任务立刻运行一次
     *
     * @param id
     */
    void runOnceQuartzJob(Long id);

    /**
     * 分页获取Quartz任务调度日志列表
     *
     * @param query
     * @return
     */
    Page<QuartzJobLogDTO> getPageQuartzLog(QuartzJobLogQuery query);

    /**
     * 清理Quartz任务调度日志信息
     *
     * @param quartzLogIds
     */
    void clearQuartzLogs(List<Long> quartzLogIds);

    /**
     * 分页获取Spring任务调度列表
     *
     * @param taskQuery
     * @return
     */
    Page<TaskDTO> getPageTasks(TaskQuery taskQuery);

    /**
     * 新增Spring调度任务
     *
     * @param taskDTO
     */
    void addTask(TaskDTO taskDTO);

    /**
     * 分页获取Spring任务调度日志列表
     *
     * @param query
     * @return
     */
    Page<TaskLogDTO> getPageTaskLog(TaskLogQuery query);

    /**
     * 清理Spring任务调度日志信息
     *
     * @param taskLogIds
     */
    void clearTaskLogs(List<Long> taskLogIds);

    /**
     * 更新Spring调度任务
     *
     * @param taskDTO
     */
    void updateTask(TaskDTO taskDTO);

    /**
     * 切换Spring调度任务状态
     *
     * @param taskDTO
     */
    void switchTaskStatus(TaskDTO taskDTO);

    /**
     * 删除Spring调度任务
     *
     * @param taskIds
     */
    void deleteTask(List<Long> taskIds);

    /**
     * 获取Quartz调度任务日志统计数据
     *
     * @return
     */
    Map<String, Object> statisticsQuartzLog();

    /**
     * 获取Spring调度任务日志统计数据
     *
     * @return
     */
    Map<String, Object> statisticsTaskLog();

    /**
     * Spring调度任务服务启动初始化
     */
    void asyncInit();

    /**
     * 获取Spring调度任务
     *
     * @param id
     * @return
     */
    TaskDTO getTask(Long id);

    /**
     * 导出Quartz调度任务日志
     *
     * @param query
     * @param response
     */
    void exportQuartzLog(QuartzJobLogQuery query, HttpServletResponse response);

    /**
     * 导出Spring调度任务日志
     *
     * @param query
     * @param response
     */
    void exportTaskLog(TaskLogQuery query, HttpServletResponse response);

    /**
     * Spring任务名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> taskFetch(String search);

    /**
     * Quartz任务名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> quartzFetch(String search);

    /**
     * Quartz任务全部暂停/启动
     *
     * @param execType
     * @return void
     * @author mabin
     * @date 2023/3/1 11:09
     **/
    void allPauseOrRunQuartzJob(Integer execType);

    /**
     * Spring调度任务全部暂停/启动
     *
     * @param execType
     * @return void
     * @author mabin
     * @date 2023/3/1 13:42
     **/
    void allPauseOrRunTask(Integer execType);
}
