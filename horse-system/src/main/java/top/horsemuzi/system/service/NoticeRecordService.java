package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.notice.NoticeRecordDTO;
import top.horsemuzi.system.pojo.dto.notice.NoticeRecordQuery;
import top.horsemuzi.system.pojo.dto.notice.NoticeSocketDTO;
import top.horsemuzi.system.pojo.entity.NoticeRecord;

import java.util.List;

/**
 * <p>
 * 通知公告发布记录表 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-10-06 16:00:06
 */
public interface NoticeRecordService extends IService<NoticeRecord> {

    /**
     * 新增通知公告发布记录
     *
     * @param noticeRecordDTOList
     * @return
     */
    boolean addNoticeRecord(List<NoticeRecordDTO> noticeRecordDTOList);

    /**
     * 获取通知公告发布记录
     *
     * @param noticeIds
     * @return
     */
    List<NoticeRecordDTO> getNoticeRecord(List<Long> noticeIds);

    /**
     * 更新通知公告读取时间
     *
     * @param noticeRecordId
     * @param userId
     * @return
     */
    boolean readNoticeRecord(Long noticeRecordId, Long userId);

    /**
     * 读取全部通知工作
     *
     * @param userId
     * @return
     */
    boolean readAllNoticeRecord(Long userId);

    /**
     * 删除指定通知公告
     *
     * @param noticeRecordId
     * @param userId
     */
    boolean deleteNoticeRecord(Long noticeRecordId, Long userId);

    /**
     * 清理全部通知公告
     *
     * @param userId
     */
    boolean deleteAllNoticeRecord(Long userId);

    /**
     * 分页获取通知公告记录列表
     *
     * @param query
     * @return
     */
    Page<NoticeRecordDTO> getPageNoticeRecords(NoticeRecordQuery query);

    /**
     * 批量移除发布记录
     *
     * @param noticeRecordIds
     * @return
     */
    boolean deleteNoticeRecord(List<Long> noticeRecordIds);

    /**
     * 获取发布记录信息
     *
     * @param noticeRecordId
     * @return
     */
    NoticeRecordDTO getNoticeRecord(Long noticeRecordId);

    /**
     * 获取用户通知公告记录列表
     *
     * @param userId
     * @return
     */
    List<NoticeRecordDTO> getNoticeRecords(Long userId);

    /**
     * 获取用户通知公告记录未读数量
     *
     * @param userIds
     * @return
     */
    List<NoticeSocketDTO> getNoticeSocketList(List<Long> userIds);
}
