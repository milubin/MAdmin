package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.open.log.ApiLogDTO;
import top.horsemuzi.system.pojo.dto.open.log.ApiLogQuery;
import top.horsemuzi.system.pojo.entity.ApiLog;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>
 * 开放API请求日志 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-21 22:26:38
 */
public interface ApiLogService extends IService<ApiLog> {

    /**
     * 保存API请求日志
     *
     * @param apiLogDTO
     * @return
     */
    boolean addApiLog(ApiLogDTO apiLogDTO);

    /**
     * 分页获取开放API请求日志列表
     *
     * @param query
     * @return
     */
    Page<ApiLogDTO> getPageApiLogs(ApiLogQuery query);

    /**
     * 获取开放API请求日志详情
     *
     * @param id
     * @return
     */
    ApiLogDTO getApiLog(Long id);

    /**
     * 删除开放API请求日志
     *
     * @param apiLogIds
     * @return
     */
    boolean deleteApiLog(List<Long> apiLogIds);

    /**
     * API接口请求日志数据导出
     *
     * @param query
     * @param response
     */
    void exportApiLog(ApiLogQuery query, HttpServletResponse response);

    /**
     * API日志链路ID实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> logFetch(String search);
}
