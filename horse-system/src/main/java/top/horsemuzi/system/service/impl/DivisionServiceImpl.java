package top.horsemuzi.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.mapper.DivisionMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.division.DivisionDTO;
import top.horsemuzi.system.pojo.dto.division.DivisionQuery;
import top.horsemuzi.system.pojo.entity.Division;
import top.horsemuzi.system.service.DivisionService;

import java.util.List;

/**
 * <p>
 * 国家行政区划信息表 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-06 10:31:24
 */
@Slf4j
@Service
public class DivisionServiceImpl extends ExpandServiceImpl<DivisionMapper, Division> implements DivisionService {

    /**
     * 获取行政区划信息列表
     *
     * @param divisionIds
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<DivisionDTO> getDivision(List<Long> divisionIds) {
        ValidatorUtil.notEmpty(divisionIds);
        return listByIds(divisionIds, DivisionDTO.class);
    }

    /**
     * 获取行政区划信息
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public DivisionDTO getDivision(Long id) {
        ValidatorUtil.notEmpty(id);
        return getById(id, DivisionDTO.class);
    }

    /**
     * 区划名称实时检索
     *
     * @param search
     * @return
     */
    @Override
    public List<InputRemoteFetchDTO> fetch(String search) {
        return baseMapper.fetch(search);
    }

    /**
     * 区划是否重复
     *
     * @param divisionDTO
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public boolean checkDivisionUnique(DivisionDTO divisionDTO) {
        ValidatorUtil.notEmpty(divisionDTO);
        LambdaQueryWrapper<Division> wrapper = new LambdaQueryWrapper<>();
        wrapper.ne(ObjectUtil.isNotNull(divisionDTO.getId()), Division::getId, divisionDTO.getId())
                .and(andWrapper -> andWrapper.eq(StrUtil.isNotBlank(divisionDTO.getName()), Division::getName, divisionDTO.getName())
                        .or().eq(StrUtil.isNotBlank(divisionDTO.getCode()), Division::getCode, divisionDTO.getCode()));
        return ObjectUtil.isNull(getOne(wrapper));
    }

    /**
     * 父级区划是否存在
     *
     * @param divisionDTO
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public boolean checkParentDivisionExist(DivisionDTO divisionDTO) {
        ValidatorUtil.notEmpty(divisionDTO);
        // 顶级区划不校验
        if (ObjectUtil.equals(divisionDTO.getLevel(), Constants.COMMON_CODE.ONE)
                && ObjectUtil.equals(divisionDTO.getParentId(), Constants.COMMON_CODE.LONG_ZONE)) {
            return true;
        }
        return ObjectUtil.isNotNull(getById(divisionDTO.getParentId()));
    }

    /**
     * 更新行政区划
     *
     * @param divisionDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateDivision(DivisionDTO divisionDTO) {
        ValidatorUtil.notEmpty(divisionDTO);
        ValidatorUtil.notEmpty(divisionDTO.getId());
        ValidatorUtil.isTrue(updateById(divisionDTO, Division.class), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }

    /**
     * 是否存在子级区划信息
     *
     * @param divisionDTO
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public boolean checkSonDivisionExist(DivisionDTO divisionDTO) {
        ValidatorUtil.notEmpty(divisionDTO);
        ValidatorUtil.notEmpty(divisionDTO.getId());
        LambdaQueryWrapper<Division> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Division::getParentId, divisionDTO.getId());
        return ObjectUtil.isNull(getOne(wrapper));
    }

    /**
     * 删除区划信息
     *
     * @param divisionIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteDivision(List<Long> divisionIds) {
        ValidatorUtil.notEmpty(divisionIds);
        ValidatorUtil.isTrue(removeBatchByIds(divisionIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 获取区划信息
     *
     * @param query
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<DivisionDTO> getDivisions(DivisionQuery query) {
        LambdaQueryWrapper<Division> wrapper = new LambdaQueryWrapper<>();
        if (ObjectUtil.isNotEmpty(query)) {
            wrapper.eq(StrUtil.isNotBlank(query.getName()), Division::getName, query.getName());
        }
        return list(wrapper, DivisionDTO.class);
    }

    /**
     * 获取区划级联数据(返回id、code、name三个字段信息)
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<DivisionDTO> getDivisionCascader(Long id) {
        ValidatorUtil.notEmpty(id);
        LambdaQueryWrapper<Division> wrapper = new LambdaQueryWrapper<>();
        wrapper.select(Division::getId, Division::getCode, Division::getName)
                .eq(Division::getParentId, id)
                .orderByAsc(Division::getCode);
        return list(wrapper, DivisionDTO.class);
    }

    /**
     * 新增行政区划
     *
     * @param divisionDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addDivision(DivisionDTO divisionDTO) {
        ValidatorUtil.notEmpty(divisionDTO);
        Division division = OrikaUtil.convert(divisionDTO, Division.class);
        ValidatorUtil.isTrue(save(division), ResultCodeEnum.DATA_SAVE_ERROR);
        divisionDTO.setId(division.getId());
        return true;
    }

}
