package top.horsemuzi.system.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.mp.expand.ExpandServiceImpl;
import top.horsemuzi.system.mapper.NoticeMapper;
import top.horsemuzi.system.pojo.dto.notice.NoticeDTO;
import top.horsemuzi.system.pojo.dto.notice.NoticeExportDTO;
import top.horsemuzi.system.pojo.dto.notice.NoticeQuery;
import top.horsemuzi.system.pojo.entity.Notice;
import top.horsemuzi.system.service.NoticeService;

import java.util.List;

/**
 * <p>
 * 系统通知公告信息表 服务实现类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-10-06 16:00:05
 */
@Service
public class NoticeServiceImpl extends ExpandServiceImpl<NoticeMapper, Notice> implements NoticeService {

    /**
     * 新增通知公告
     *
     * @param noticeDTO
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addNotice(NoticeDTO noticeDTO) {
        ValidatorUtil.notEmpty(noticeDTO);
        Notice notice = OrikaUtil.convert(noticeDTO, Notice.class);
        ValidatorUtil.isTrue(save(notice), ResultCodeEnum.DATA_SAVE_ERROR);
        noticeDTO.setId(notice.getId());
        return true;
    }

    /**
     * 获取通知公告信息列表
     *
     * @param noticeIds
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<NoticeDTO> getNotice(List<Long> noticeIds) {
        ValidatorUtil.notEmpty(noticeIds);
        return OrikaUtil.convertList(listByIds(noticeIds), NoticeDTO.class);
    }

    /**
     * 删除通知公告
     *
     * @param noticeIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteNotice(List<Long> noticeIds) {
        ValidatorUtil.notEmpty(noticeIds);
        ValidatorUtil.isTrue(removeBatchByIds(noticeIds), ResultCodeEnum.DATA_DELETE_ERROR);
        return true;
    }

    /**
     * 更新通知公告
     *
     * @param noticeDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateNotice(NoticeDTO noticeDTO) {
        ValidatorUtil.notEmpty(noticeDTO);
        ValidatorUtil.notEmpty(noticeDTO.getId());
        ValidatorUtil.isTrue(updateById(OrikaUtil.convert(noticeDTO, Notice.class)), ResultCodeEnum.DATA_UPDATE_ERROR);
        return true;
    }


    /**
     * 获取导出通知公告和对应记录数据
     *
     * @param query
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<NoticeExportDTO> getNoticeExportList(NoticeQuery query) {
        return baseMapper.getNoticeExportList(query);
    }

    /**
     * 分页通知公告信息列表数量
     *
     * @param query
     * @return
     */
    @Override
    public Long getPageNoticeCount(NoticeQuery query) {
        return baseMapper.getPageNoticeCount(query);
    }

    /**
     * 分页通知公告信息列表
     *
     * @param query
     * @return
     */
    @Override
    public List<NoticeDTO> getPageNoticeList(NoticeQuery query) {
        return baseMapper.getPageNoticeList(query);
    }

    /**
     * 获取通知公告信息
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public NoticeDTO getNotice(Long id) {
        ValidatorUtil.notEmpty(id);
        return OrikaUtil.convert(getById(id), NoticeDTO.class);
    }

}
