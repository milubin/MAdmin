package top.horsemuzi.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskDTO;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskQuery;
import top.horsemuzi.system.pojo.entity.Task;

import java.util.List;

/**
 * <p>
 * 系统任务调度信息表 服务类
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-08-20 17:56:39
 */
public interface TaskService extends IService<Task> {

    /**
     * 分页获取Spring任务调度列表
     *
     * @param query
     * @return
     */
    Page<TaskDTO> getPageTasks(TaskQuery query);

    /**
     * 保存调度任务
     *
     * @param taskDTO
     * @return
     */
    boolean addTask(TaskDTO taskDTO);

    /**
     * 校验调度任务是否重复
     *
     * @param taskDTO
     * @return true: 重复， false: 不重复
     */
    boolean checkRepeat(TaskDTO taskDTO);

    /**
     * 获取调度任务信息
     *
     * @param id
     * @return
     */
    TaskDTO getTask(Long id);

    /**
     * 更新调度任务
     *
     * @param currentTaskDTO
     * @return
     */
    boolean updateTask(TaskDTO currentTaskDTO);

    /**
     * 获取调度任务列表
     *
     * @param taskIds
     * @return
     */
    List<TaskDTO> getTasks(List<Long> taskIds);

    /**
     * 删除调度任务
     *
     * @param taskIds
     * @return
     */
    boolean deleteTask(List<Long> taskIds);

    /**
     * 获取调度任务
     *
     * @param taskDTO
     * @return
     */
    TaskDTO getTask(TaskDTO taskDTO);

    /**
     * 获取调度任务
     *
     * @param taskDTO
     * @return
     */
    List<TaskDTO> getTasks(TaskDTO taskDTO);

    /**
     * Spring任务名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> taskFetch(String search);

    /**
     * 更新调度任务
     *
     * @param taskDTOList
     * @return boolean
     * @author mabin
     * @date 2023/3/1 13:53
     **/
    boolean updateTask(List<TaskDTO> taskDTOList);
}
