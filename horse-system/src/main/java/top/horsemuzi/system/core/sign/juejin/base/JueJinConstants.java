package top.horsemuzi.system.core.sign.juejin.base;

import cn.hutool.core.date.DateUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 掘金相关常量配置
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/03/09 09:42
 **/
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class JueJinConstants {

    public static final String HEADER = "【掘金】" + DateUtil.formatDateTime(new Date());

    public static final String DATA = "data";

    public static final String ERR_NO = "err_no";

    public static final String MINERAL = "矿石";


}
