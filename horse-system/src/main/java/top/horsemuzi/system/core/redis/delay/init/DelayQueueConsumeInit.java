package top.horsemuzi.system.core.redis.delay.init;

import cn.hutool.extra.spring.SpringUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import top.horsemuzi.system.core.redis.delay.enums.DelayQueueEnum;
import top.horsemuzi.system.core.redis.delay.handle.DelayQueueHandle;
import top.horsemuzi.system.core.redis.service.RedissonService;

import java.util.Objects;

/**
 * 延时队列消费者启动初始化
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/05/13 10:29
 **/
@Slf4j
@Component
@RequiredArgsConstructor
public class DelayQueueConsumeInit {

    private final RedissonService redissonService;

    /**
     * 延时队列消费者启动初始化
     *
     * @param
     * @return void
     * @author mabin
     * @date 2023/5/13 10:30
     **/
    @Async("async")
    public void init() {
        new Thread(() -> {
            while (true) {
                try {
                    for (DelayQueueEnum queueEnum : DelayQueueEnum.values()) {
                        Object o = redissonService.getDelayQueue(queueEnum.getKey());
                        if (Objects.nonNull(o)) {
                            DelayQueueHandle<Object> delayQueueHandle = SpringUtil.getBean(queueEnum.getBean());
                            delayQueueHandle.execute(o);
                        }
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }).start();
        log.info("delay queue start init success");
    }

}
