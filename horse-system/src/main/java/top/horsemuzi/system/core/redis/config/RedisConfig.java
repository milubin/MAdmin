package top.horsemuzi.system.core.redis.config;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.google.common.base.Charsets;
import com.google.common.hash.Funnel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.stream.Consumer;
import org.springframework.data.redis.connection.stream.MapRecord;
import org.springframework.data.redis.connection.stream.ReadOffset;
import org.springframework.data.redis.connection.stream.StreamOffset;
import org.springframework.data.redis.core.*;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.data.redis.stream.StreamMessageListenerContainer;
import top.horsemuzi.system.core.redis.base.RedisConstants;
import top.horsemuzi.system.core.redis.listener.consume.*;
import top.horsemuzi.system.core.redis.listener.stream.*;
import top.horsemuzi.system.core.redis.service.RedisService;
import top.horsemuzi.system.handle.bloom.BloomHandler;

import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Redis缓存配置
 * 开启基于注解的缓存配置
 * 基于redisson辅助类配置
 *
 * @author mabin
 * @date 2022/05/09 17:45
 **/
@Slf4j
@EnableCaching
@Configuration
@RequiredArgsConstructor
public class RedisConfig {

    @Value("${custom.bloom.expectedInsertions}")
    public Integer expectedInsertions;

    @Value("${custom.bloom.fpp}")
    public Double fpp;


    /**
     * Redis缓存配置
     *
     * @param redisConnectionFactory
     * @return
     */
    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        // 注入数据源
        redisTemplate.setConnectionFactory(redisConnectionFactory);

        // 使用Jackson2JsonRedisSerialize 替换默认序列化
        // Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer<>(Object.class);
        // ObjectMapper mapper = new ObjectMapper();
        // mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        // // 序列化时将对象全类名一起保存下来
        // mapper.activateDefaultTyping(LaissezFaireSubTypeValidator.instance, ObjectMapper.DefaultTyping.NON_FINAL);
        // jackson2JsonRedisSerializer.setObjectMapper(mapper);

        // 1、用StringRedisSerializer进行序列化的值，在Java和Redis中保存的内容是一样的
        // 2、用Jackson2JsonRedisSerializer进行序列化的值，在Redis中保存的内容，比Java中多了一对双引号。
        // 3、用JdkSerializationRedisSerializer进行序列化的值，对于Key-Value的Value来说，是在Redis中是不可读的。对于Hash的Value来说，比Java的内容多了一些字符
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        // key-value结构序列化数据结构
        redisTemplate.setKeySerializer(stringRedisSerializer);
        redisTemplate.setValueSerializer(stringRedisSerializer);
        // hash数据结构序列化方式,必须这样否则存hash 就是基于jdk序列化的
        redisTemplate.setHashKeySerializer(stringRedisSerializer);
        redisTemplate.setHashValueSerializer(stringRedisSerializer);
        // 启用默认序列化方式
        redisTemplate.setEnableDefaultSerializer(true);
        redisTemplate.setDefaultSerializer(stringRedisSerializer);
        // 开启事务（如果默认开启事物，在同时执行mysql保存操作发生异常，但是需要删除redis的时候，因为事务生效，redis的值将会回滚）
        // redisTemplate.setEnableTransactionSupport(true);
        return redisTemplate;
    }

    /**
     * 开启基于注解的缓存配置
     *
     * @param redisConnectionFactory
     * @return
     */
    @Bean
    public CacheManager cacheManager(RedisConnectionFactory redisConnectionFactory) {
        RedisCacheConfiguration config = RedisCacheConfiguration.defaultCacheConfig()
                // 过期时间600秒: 600秒后会重新进行数据库查询
                .entryTtl(Duration.ofSeconds(600))
                // 配置序列化
                .serializeKeysWith(RedisSerializationContext.SerializationPair
                        .fromSerializer(new StringRedisSerializer()))
                .serializeValuesWith(RedisSerializationContext.SerializationPair
                        .fromSerializer(new GenericJackson2JsonRedisSerializer()))
                .disableCachingNullValues();
        return RedisCacheManager.builder(redisConnectionFactory).cacheDefaults(config).build();
    }

    /**
     * 初始化布隆过滤器，放入到spring容器里面(初始化默认泛型为 String)
     *
     * @return
     */
    @Bean
    public BloomHandler<String> bloomHandler() {
        return new BloomHandler<>((Funnel<String>) (from, into) -> into.putString(from, Charsets.UTF_8)
                .putString(from, Charsets.UTF_8), expectedInsertions, fpp);
    }

    // ==================初始化对常用数据类型的操作================

    /**
     * String 类型初始化
     *
     * @param redisTemplate
     * @return
     */
    @Bean
    public ValueOperations<String, String> valueOperations(RedisTemplate<String, String> redisTemplate) {
        return redisTemplate.opsForValue();
    }

    /**
     * List 类型初始化
     *
     * @param redisTemplate
     * @return
     */
    @Bean
    public ListOperations<String, Object> listOperations(RedisTemplate<String, Object> redisTemplate) {
        return redisTemplate.opsForList();
    }

    /**
     * Set 类型初始化
     *
     * @param redisTemplate
     * @return
     */
    @Bean
    public SetOperations<String, Object> setOperations(RedisTemplate<String, Object> redisTemplate) {
        return redisTemplate.opsForSet();
    }

    /**
     * ZSet 类型初始化
     *
     * @param redisTemplate
     * @return
     */
    @Bean
    public ZSetOperations<String, Object> zSetOperations(RedisTemplate<String, Object> redisTemplate) {
        return redisTemplate.opsForZSet();
    }

    /**
     * Hash 类型初始化
     *
     * @param redisTemplate
     * @return
     */
    @Bean
    public HashOperations<String, String, Object> hashOperations(RedisTemplate<String, Object> redisTemplate) {
        return redisTemplate.opsForHash();
    }

    /**
     * stream类型初始化
     *
     * @param redisTemplate
     * @return
     */
    @Bean
    public StreamOperations<String, String, String> streamOperations(RedisTemplate<String, Object> redisTemplate) {
        return redisTemplate.opsForStream();
    }

    // ======================================redis发布/订阅前置配置=================================================

    /**
     * 配置redis的监听装置
     *
     * @param redisConnectionFactory
     * @return
     */
    @Bean
    public RedisMessageListenerContainer redisMessageListenerContainer(RedisConnectionFactory redisConnectionFactory) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(redisConnectionFactory);
        container.setTopicSerializer(new StringRedisSerializer());
        // 设置发布订阅的消息监听器
        container.addMessageListener(messageDingBotAdapter(), ChannelTopic.of(RedisConstants.TOPIC.DING_BOT));
        container.addMessageListener(messageDingWorkAdapter(), ChannelTopic.of(RedisConstants.TOPIC.DING_WORK));
        container.addMessageListener(messageEmailAdapter(), ChannelTopic.of(RedisConstants.TOPIC.EMAIL));
        container.addMessageListener(messageSmsAdapter(), ChannelTopic.of(RedisConstants.TOPIC.SMS));
        container.addMessageListener(messageFlyingBotAdapter(), ChannelTopic.of(RedisConstants.TOPIC.FLYING_BOT));
        return container;
    }

    @Bean
    public MessageListenerAdapter messageDingBotAdapter() {
        return new MessageListenerAdapter(dingBotMessageListener());
    }

    @Bean
    public ConsumeDingBotMessageListener dingBotMessageListener() {
        return new ConsumeDingBotMessageListener();
    }

    @Bean
    public MessageListenerAdapter messageDingWorkAdapter() {
        return new MessageListenerAdapter(dingWorkMessageListener());
    }

    @Bean
    public ConsumeDingWorkMessageListener dingWorkMessageListener() {
        return new ConsumeDingWorkMessageListener();
    }

    @Bean
    public MessageListenerAdapter messageEmailAdapter() {
        return new MessageListenerAdapter(emailMessageListener());
    }

    @Bean
    public ConsumeEmailMessageListener emailMessageListener() {
        return new ConsumeEmailMessageListener();
    }

    @Bean
    public MessageListenerAdapter messageSmsAdapter() {
        return new MessageListenerAdapter(smsMessageListener());
    }

    @Bean
    public ConsumeSmsMessageListener smsMessageListener() {
        return new ConsumeSmsMessageListener();
    }

    @Bean
    public MessageListenerAdapter messageFlyingBotAdapter() {
        return new MessageListenerAdapter(flyingBotMessageListener());
    }

    @Bean
    public ConsumeFlyingBotMessageListener flyingBotMessageListener() {
        return new ConsumeFlyingBotMessageListener();
    }


    // ======================================redis发布/订阅前置配置(Stream)=================================================

    /**
     * 开启监听器接收消息
     */
    @Bean
    public StreamMessageListenerContainer<String, MapRecord<String, String, String>> emailListenerContainer(RedisConnectionFactory redisConnectionFactory) {
        StreamMessageListenerContainer<String, MapRecord<String, String, String>> listenerContainer =
                StreamMessageListenerContainer.create(redisConnectionFactory, streamMessageListenerContainerOptions());
        // 初始化stream流和消费者组
        Map<String, String> streamMap = new HashMap<>(16);
        streamMap.put(RedisConstants.STREAM.DING_BOT.STREAM, RedisConstants.STREAM.DING_BOT.GROUP);
        streamMap.put(RedisConstants.STREAM.DING_WORK.STREAM, RedisConstants.STREAM.DING_WORK.GROUP);
        streamMap.put(RedisConstants.STREAM.EMAIL.STREAM, RedisConstants.STREAM.EMAIL.GROUP);
        streamMap.put(RedisConstants.STREAM.SMS.STREAM, RedisConstants.STREAM.SMS.GROUP);
        streamMap.put(RedisConstants.STREAM.FLYING_BOT.STREAM, RedisConstants.STREAM.FLYING_BOT.GROUP);
        createStreamAndGroup(streamMap);
        // 注册消费者
        // 钉钉机器人消息通道消费设置
        listenerContainer.receive(Consumer.from(RedisConstants.STREAM.DING_BOT.GROUP, RedisConstants.STREAM.DING_BOT.CONSUMER),
                StreamOffset.create(RedisConstants.STREAM.DING_BOT.STREAM, ReadOffset.lastConsumed()),
                SpringUtil.getBean(ConsumeDingBotStreamListener.class));
        // 钉钉工作消息通道消费设置
        listenerContainer.receive(Consumer.from(RedisConstants.STREAM.DING_WORK.GROUP, RedisConstants.STREAM.DING_WORK.CONSUMER),
                StreamOffset.create(RedisConstants.STREAM.DING_WORK.STREAM, ReadOffset.lastConsumed()),
                SpringUtil.getBean(ConsumeDingWorkStreamListener.class));
        // 邮件消息通道消费设置
        listenerContainer.receive(Consumer.from(RedisConstants.STREAM.EMAIL.GROUP, RedisConstants.STREAM.EMAIL.CONSUMER),
                StreamOffset.create(RedisConstants.STREAM.EMAIL.STREAM, ReadOffset.lastConsumed()),
                SpringUtil.getBean(ConsumeEmailStreamListener.class));
        // 短信消息通道消费设置
        listenerContainer.receive(Consumer.from(RedisConstants.STREAM.SMS.GROUP, RedisConstants.STREAM.SMS.CONSUMER),
                StreamOffset.create(RedisConstants.STREAM.SMS.STREAM, ReadOffset.lastConsumed()),
                SpringUtil.getBean(ConsumeSmsStreamListener.class));
        // 飞书机器人消息通道消费设置
        listenerContainer.receive(Consumer.from(RedisConstants.STREAM.FLYING_BOT.GROUP, RedisConstants.STREAM.FLYING_BOT.CONSUMER),
                StreamOffset.create(RedisConstants.STREAM.FLYING_BOT.STREAM, ReadOffset.lastConsumed()),
                SpringUtil.getBean(ConsumeFlyingBotStreamListener.class));

        // TODO 后续新增消息通道即可

        listenerContainer.start();
        return listenerContainer;
    }

    @Bean
    public StreamMessageListenerContainer.StreamMessageListenerContainerOptions<String, MapRecord<String, String, String>> streamMessageListenerContainerOptions() {
        return StreamMessageListenerContainer.StreamMessageListenerContainerOptions.builder()
                // block读取超时时间
                .pollTimeout(Duration.ofSeconds(2))
                // count 数量（一次只获取一条消息）
                .batchSize(1)
                .serializer(new StringRedisSerializer())
                .build();
    }

    /**
     * 创建stream流和消费者组
     *
     * @param streamMap key => streamName, value => streamGroup
     */
    private void createStreamAndGroup(Map<String, String> streamMap) {
        if (CollectionUtil.isNotEmpty(streamMap)) {
            RedisService redisService = SpringUtil.getBean(RedisService.class);
            for (Map.Entry<String, String> entry : streamMap.entrySet()) {
                // 如果 流不存在 创建 stream 流, 并限制流的消息长度 (默认10000条)
                String streamName = entry.getKey();
                if (!redisService.hasKey(streamName)) {
                    redisService.addStream(streamName, Collections.singletonMap(StrUtil.EMPTY, StrUtil.EMPTY));
                    redisService.streamTrim(streamName, 10000L);
                }
                try {
                    // 创建消费者组
                    redisService.addGroup(streamName, entry.getValue());
                } catch (Exception e) {
                    log.info("redis stream group [{}] is exist!", entry.getValue());
                }
            }
        }
    }

}
