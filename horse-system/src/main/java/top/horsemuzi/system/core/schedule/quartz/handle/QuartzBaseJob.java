package top.horsemuzi.system.core.schedule.quartz.handle;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.slf4j.MDC;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.util.ExceptionUtil;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.NetUtil;
import top.horsemuzi.system.core.schedule.quartz.base.QuartzConstants;
import top.horsemuzi.system.handle.event.spring.event.AsyncEvent;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobDTO;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobLogDTO;

/**
 * Quartz的基础任务调度接口
 *
 * @author mabin
 * @date 2022/07/02 11:10
 **/
@Slf4j
@Component
@RequiredArgsConstructor
public abstract class QuartzBaseJob implements Job {

    private final ApplicationContext applicationContext;

    /**
     * 根据任务信息创建调度任务并记录任务执行日志
     *
     * @param context 任务上下文
     */
    @Override
    public void execute(JobExecutionContext context) {
        QuartzJobDTO quartzJobDTO = JsonUtil.fromJson(Convert.toStr(context.getMergedJobDataMap()
                .get(QuartzConstants.QUARTZ_JOB)), QuartzJobDTO.class);
        if (ObjectUtil.isNotNull(quartzJobDTO)) {
            StopWatch watch = new StopWatch();
            watch.start();
            try {
                doExecute(context, quartzJobDTO);
                saveQuartzJobLog(quartzJobDTO, watch, null);
            } catch (Exception e) {
                log.error("Quartz调度任务执行异常: {}", ExceptionUtil.getMessage(e));
                saveQuartzJobLog(quartzJobDTO, watch, e);
            }
        }
    }

    /**
     * 记录任务执行日志(异步记录)
     *
     * @param quartzJobDTO
     * @param watch
     * @param e
     */
    protected void saveQuartzJobLog(QuartzJobDTO quartzJobDTO, StopWatch watch, Exception e) {
        watch.stop();
        QuartzJobLogDTO logDTO = new QuartzJobLogDTO();
        logDTO.setTraceId(MDC.get(Constants.TRACE_ID));
        logDTO.setThreadName(Thread.currentThread().getName());
        logDTO.setConsume(watch.getTotalTimeMillis());
        logDTO.setJobId(quartzJobDTO.getId());
        logDTO.setJobModel(quartzJobDTO.getJobModel());
        logDTO.setJobName(quartzJobDTO.getJobName());
        logDTO.setJobGroup(quartzJobDTO.getJobGroup());
        logDTO.setInvokeTarget(quartzJobDTO.getInvokeTarget());
        logDTO.setCreateId(quartzJobDTO.getCreateId());
        logDTO.setCreateUser(quartzJobDTO.getCreateUser());
        logDTO.setExecStatus(Constants.COMMON_JUDGMENT.ONE);
        if (ObjectUtil.isNotNull(e)) {
            logDTO.setExecStatus(Constants.COMMON_JUDGMENT.ZONE);
            logDTO.setExecName(e.getClass().getCanonicalName());
            logDTO.setExecMessage(ExceptionUtil.getMessage(e));
        }
        publishQuartzJobLogEvent(logDTO);
    }

    /**
     * 发布调度任务执行日志监听事件
     *
     * @param quartzJobLogDTO
     */
    private void publishQuartzJobLogEvent(QuartzJobLogDTO quartzJobLogDTO) {
        NetUtil.setRequestInheritable();
        applicationContext.publishEvent(new AsyncEvent(quartzJobLogDTO));
    }

    /**
     * 执行调度任务信息中的目标方法, 由字类重载
     *
     * @param context
     * @param quartzJobDTO
     * @throws Exception
     */
    protected abstract void doExecute(JobExecutionContext context, QuartzJobDTO quartzJobDTO) throws Exception;

}
