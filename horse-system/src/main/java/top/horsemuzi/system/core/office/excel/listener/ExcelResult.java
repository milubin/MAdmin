package top.horsemuzi.system.core.office.excel.listener;

import java.util.List;

/**
 * Excel导入监听处理结果集
 *
 * @author mabin
 * @date 2022/09/21 09:47
 **/

public interface ExcelResult<T> {

    /**
     * 数据列表
     *
     * @return
     */
    List<T> getList();

    /**
     * 错误列表
     *
     * @return
     */
    List<String> getErrorList();

    /**
     * 导入回执
     *
     * @return
     */
    String getAnalysis();

}
