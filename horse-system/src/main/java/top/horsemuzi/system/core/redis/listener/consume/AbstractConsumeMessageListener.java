package top.horsemuzi.system.core.redis.listener.consume;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;

/**
 * 消费监听者抽象类
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/04/11 17:12
 **/
@Slf4j
public abstract class AbstractConsumeMessageListener implements MessageListener {

    @Override
    public void onMessage(Message message, byte[] pattern) {
        log.info("pattern={}, message={}", new String(pattern), message.toString());
    }

}
