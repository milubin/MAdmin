package top.horsemuzi.system.core.office.excel.listener;

import com.alibaba.excel.read.listener.ReadListener;

/**
 * Excel导入监听处理
 *
 * @author mabin
 * @date 2022/09/21 09:49
 **/

public interface ExcelListener<T> extends ReadListener<T> {

    /**
     * 获取导入数据结果实例
     *
     * @return
     */
    ExcelResult<T> getExcelResult();

}
