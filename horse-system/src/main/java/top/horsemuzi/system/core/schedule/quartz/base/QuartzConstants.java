package top.horsemuzi.system.core.schedule.quartz.base;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description Quartz调度常量信息
 * @date 2022/8/21 16:18
 */
public class QuartzConstants {

    /**
     * quartz执行任务信息key标识
     */
    public static final String QUARTZ_JOB = "quartz_job";

    public static final String QUARTZ_HEADER = "[Quartz调度任务]";

    /**
     * 计划执行错误策略
     */
    public interface MISFIRE_POLICY {
        // 默认
        String MISFIRE_DEFAULT = "0";
        // 立即触发执行
        String MISFIRE_IGNORE_MISFIRES = "1";
        // 触发一次执行
        String MISFIRE_FIRE_AND_PROCEED = "2";
        // 放弃执行
        String MISFIRE_DO_NOTHING = "3";
    }

}
