package top.horsemuzi.system.core.sign.aliyun.base;

import cn.hutool.core.date.DateUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 阿里云盘相关常量
 *
 * @author 马滨
 * @date 2023/05/07 16:26
 **/
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AliyunConstants {

    public static final String HEADER = "【阿里云盘】" + DateUtil.formatDateTime(new Date());

    public static final String ACCESS_TOKEN = "access_token";


}
