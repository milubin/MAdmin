package top.horsemuzi.system.core.redis.service;

import cn.hutool.core.util.ObjectUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Range;
import org.springframework.data.redis.connection.BitFieldSubCommands;
import org.springframework.data.redis.connection.RedisServerCommands;
import org.springframework.data.redis.connection.RedisZSetCommands;
import org.springframework.data.redis.connection.stream.*;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Service;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.system.handle.bloom.BloomHandler;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description RedisTemplate的API辅助实现处理
 * @date 2022/5/13 22:35
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RedisService {

    private final RedisTemplate<String, Object> redisTemplate;
    private final ValueOperations<String, String> valueOperations;
    private final ListOperations<String, Object> listOperations;
    private final SetOperations<String, Object> setOperations;
    private final ZSetOperations<String, Object> zSetOperations;
    private final HashOperations<String, String, Object> hashOperations;
    private final StreamOperations<String, String, String> streamOperations;
    private final BloomHandler<String> bloomHandler;

    @Getter
    @ToString
    @AllArgsConstructor
    public enum SECTION_TYPE {

        /**
         * 自定义参数, 默认查询所有参数信息
         */
        INFO("info"),

        /**
         * 获取缓存中可用键Key的总数
         */
        DB_SIZE("dbSize"),

        /**
         * 命令统计信息
         */
        COMMANDSTATS("commandstats"),

        /**
         * CPU 计算量统计信息
         */
        CPU("cpu"),

        /**
         * 主/从复制信息
         */
        replication("replication"),

        /**
         * 一般统计信息
         */
        stats("stats "),

        /**
         * RDB 和 AOF 的相关信息
         */
        PERSISTENCE("persistence");

        private String section;
    }


    // ===============================redisTemplate======================

    /**
     * 获取缓存服务基础信息
     *
     * @param section
     * @return
     */
    public Properties getRedisInfo(SECTION_TYPE section) {
        Properties properties = null;
        switch (section) {
            case INFO:
                properties = (Properties) redisTemplate.execute((RedisCallback<Object>) RedisServerCommands::info);
                break;
            case DB_SIZE:
                properties = new Properties();
                Object object = redisTemplate.execute((RedisCallback<Object>) RedisServerCommands::dbSize);
                properties.put(section.getSection(), ObjectUtil.isNull(object) ? 0 : object);
                break;
            case COMMANDSTATS:
                properties = (Properties) redisTemplate.execute((RedisCallback<Object>) connection -> connection.info(section.getSection()));
                break;
            default:
                break;
        }
        return properties;
    }

    /**
     * 发布指定通道的订阅消息
     *
     * @param topic
     * @param message
     */
    public void convertAndSend(String topic, Object message) {
        redisTemplate.convertAndSend(topic, message);
    }

    /**
     * 获取key集合
     *
     * @param pattern 配置键
     * @return
     */
    public Set<String> getKeys(String pattern) {
        return Optional.ofNullable(redisTemplate.keys(pattern)).orElse(new HashSet<>());
    }

    /**
     * 获取key集合(自动填充键模糊匹配标识)
     *
     * @param prefix
     * @return
     */
    public Set<String> getKeysByPrefix(String prefix) {
        return getKeys(prefix + ":*");
    }


    /**
     * 获取失效时间
     * 结果分析:
     * 如果结果大于0, 说明返回的是失效时间
     * 如果结果等于-1, 说明此缓存永不过期
     * 如果结果等于-2, 说明此缓存已过期,不存在了
     *
     * @param key
     * @return
     */
    public Long getExpire(String key) {
        return redisTemplate.getExpire(key);
    }

    /**
     * 获取指定时间单位的失效时间
     *
     * @param key
     * @param unit
     * @return
     */
    public Long getExpire(String key, TimeUnit unit) {
        return redisTemplate.getExpire(key, unit);
    }

    /**
     * 设置key的有效时间
     *
     * @param key
     * @param timeout
     * @param unit
     * @return
     */
    public Boolean expire(String key, Long timeout, TimeUnit unit) {
        return redisTemplate.expire(key, timeout, unit);
    }

    /**
     * 设置key的有效时间
     *
     * @param key
     * @param timeout
     * @return
     */
    public Boolean expire(String key, Duration timeout) {
        return redisTemplate.expire(key, timeout);
    }

    /**
     * 判断是否含有此缓存
     *
     * @param key
     * @return
     */
    public Boolean hasKey(String key) {
        return redisTemplate.hasKey(key);
    }

    /**
     * 匹配方式删除缓存信息
     *
     * @param pattern
     */
    public void deletePattern(String pattern) {
        Set<String> keys = getKeys(pattern);
        log.info("当前匹配值 pattern: {}, 配置键值集合: keys: {}", pattern, keys);
        if (keys != null && keys.size() > 0) {
            delete(keys);
        }
    }

    /**
     * 批量匹配方式删除缓存信息
     *
     * @param pattern
     */
    public void deletePattern(String... pattern) {
        if (pattern != null) {
            for (String pat : pattern) {
                deletePattern(pat);
            }
        }
    }

    /**
     * 匹配方式删除缓存信息(自动填充键模糊匹配标识)
     *
     * @param prefix
     */
    public void deletePatternByPrefix(String prefix) {
        Set<String> keys = getKeysByPrefix(prefix);
        if (keys != null && keys.size() > 0) {
            delete(keys);
        }
    }

    /**
     * 批量匹配移除缓存信息(自动填充键模糊匹配标识)
     *
     * @param prefix
     */
    public void deletePatternByPrefix(String... prefix) {
        if (prefix != null) {
            for (String pre : prefix) {
                deletePatternByPrefix(pre);
            }
        }
    }

    /**
     * 删除指定缓存
     *
     * @param key
     * @return
     */
    public Boolean delete(String key) {
        return redisTemplate.delete(key);
    }

    /**
     * 删除指定缓存
     *
     * @param keys
     * @return
     */
    public Boolean delete(String... keys) {
        if (keys != null && keys.length > 0) {
            return delete(Arrays.asList(keys));
        }
        return false;
    }

    /**
     * 删除指定缓存
     *
     * @param keys
     */
    public Boolean delete(Collection<String> keys) {
        if (keys != null && keys.size() > 0) {
            if (keys.stream().noneMatch(Objects::isNull)) {
                redisTemplate.delete(keys);
                return true;
            }
        }
        return false;
    }

    // ===============================valueOperations======================

    /**
     * 普通缓存获取
     *
     * @param key key键
     * @return java.lang.String
     */
    public String get(String key) {
        return valueOperations.get(key);
    }

    /**
     * 指定区间内缓存获取
     *
     * @param key   key键
     * @param start 开始位置
     * @param end   结束位置
     * @return java.lang.String
     */
    public String get(String key, Long start, Long end) {
        return valueOperations.get(key, start, end);
    }

    /**
     * 批量缓存获取
     *
     * @param keys
     * @return
     */
    public List<String> multiGet(String... keys) {
        return multiGet(new ArrayList<>(Arrays.asList(keys)));
    }

    /**
     * 批量缓存获取
     *
     * @param keys
     * @return
     */
    public List<String> multiGet(Collection<String> keys) {
        if (keys == null || keys.isEmpty()) {
            return Collections.emptyList();
        }
        List<String> values = valueOperations.multiGet(keys);
        return values != null && values.size() > 0
                ? values.stream().filter(Objects::nonNull).collect(Collectors.toList())
                : Collections.emptyList();
    }

    /**
     * 批量获取缓存并构建key-value键值对
     *
     * @param keys
     * @return
     */
    public Map<String, String> multiGetMap(Collection<String> keys) {
        if (keys == null || keys.isEmpty()) {
            return Collections.emptyMap();
        }
        Map<String, String> cacheMap = new LinkedHashMap<>(16);
        for (String key : keys) {
            String value = get(key);
            if (Objects.nonNull(value)) {
                cacheMap.put(key, value);
            }
        }
        return cacheMap;
    }

    /**
     * 缓存设置, 默认: 永不失效
     *
     * @param key
     * @param value
     * @return
     */
    public void set(String key, String value) {
        valueOperations.set(key, value);
    }

    /**
     * 指定失效时间的缓存设置, 默认单位: 秒
     *
     * @param key
     * @param value
     * @param timeout
     * @return
     */
    public void set(String key, String value, Long timeout) {
        valueOperations.set(key, value, timeout, TimeUnit.SECONDS);
    }

    /**
     * 指定失效时间和时间单位的缓存设置
     *
     * @param key
     * @param value
     * @param timeout
     * @param unit
     * @return
     */
    public void set(String key, String value, Long timeout, TimeUnit unit) {
        valueOperations.set(key, value, timeout, unit);
    }

    /**
     * 指定失效时间的缓存设置
     *
     * @param key
     * @param value
     * @param timeout
     */
    public void set(String key, String value, Duration timeout) {
        valueOperations.set(key, value, timeout);
    }

    /**
     * 批量缓存设置
     *
     * @param map
     * @return
     */
    public void multiSet(Map<String, String> map) {
        valueOperations.multiSet(map);
    }

    /**
     * 设置指定偏移量位置的值为 1
     *
     * @param key    key值
     * @param offset 偏移量(从 0 开始)
     * @return 返回未设置此值之前的值(bitmap默认值为 0, bitmap第一次设置值返回值为 false)
     */
    public Boolean setBit(String key, Long offset) {
        return setBit(key, offset, true);
    }

    /**
     * 设置指定偏移量位置的值
     *
     * @param key
     * @param offset 偏移量(从 0 开始)
     * @param value  返回未设置此值之前的值
     * @return
     */
    public Boolean setBit(String key, Long offset, Boolean value) {
        return valueOperations.setBit(key, offset, value);
    }

    /**
     * 获取指定偏移量位置的值
     *
     * @param key
     * @param offset
     * @return
     */
    public Boolean getBit(String key, Long offset) {
        return valueOperations.getBit(key, offset);
    }

    /**
     * 获取指定偏移量范围内值为 1 的总数量
     *
     * @param key
     * @param start
     * @param end
     * @return
     */
    public Long bitCount(String key, Long start, Long end) {
        return redisTemplate.execute((RedisCallback<Long>) exe -> exe.bitCount(key.getBytes(), start, end));
    }

    /**
     * 获取全部范围内值为 1 的总数量
     *
     * @param key
     * @return
     */
    public Long bitCount(String key) {
        return redisTemplate.execute((RedisCallback<Long>) exe -> exe.bitCount(key.getBytes()));
    }

    /**
     * 获取全部范围内第一个设置为 1 或 0 的bit位的索引
     *
     * @param key
     * @param bit
     * @return
     */
    public Long bitPos(String key, Boolean bit) {
        return redisTemplate.execute((RedisCallback<Long>) exe -> exe.bitPos(key.getBytes(), bit));
    }

    /**
     * 获取指定范围内第一个设置为 1 或 0 的bit位的索引
     *
     * @param key
     * @param bit
     * @param start
     * @param end   可以为负值(-1:倒数第一个bit位索引)
     * @return
     */
    public Long bitPos(String key, Boolean bit, Long start, Long end) {
        return redisTemplate.execute((RedisCallback<Long>) exe -> exe.bitPos(key.getBytes(), bit, Range.open(start, end)));
    }

    /**
     * 获取指定偏移量范围内所有bitmap位的十进制值的集合
     *
     * @param key    key值
     * @param bits   取几位
     * @param offset 偏移量(从哪里开始取)
     * @param isSign 是否是有符号数(true: 有符号数, false: 无符号数)
     * @return
     */
    public List<Long> bitField(String key, Integer bits, Long offset, Boolean isSign) {
        BitFieldSubCommands.BitFieldType bitFieldType = isSign ? BitFieldSubCommands.BitFieldType.signed(bits) : BitFieldSubCommands.BitFieldType.unsigned(bits);
        return valueOperations.bitField(key, BitFieldSubCommands.create().get(bitFieldType).valueAt(offset));
    }


    /**
     * 自增长设置, 默认增长因子: 1
     *
     * @param key
     * @return
     */
    public Long increment(String key) {
        return valueOperations.increment(key);
    }

    /**
     * 指定自增长因子的设置
     *
     * @param key
     * @param delta
     * @return
     */
    public Long increment(String key, Long delta) {
        if (delta < 0L) {
            throw new RuntimeException("Increment factor cannot be less than 0!");
        }
        return valueOperations.increment(key, delta);
    }

    /**
     * 指定自增长因子的设置
     *
     * @param key
     * @param delta
     * @return
     */
    public Double increment(String key, Double delta) {
        if (delta < 0L) {
            throw new RuntimeException("Increment factor cannot be less than 0!");
        }
        return valueOperations.increment(key, delta);
    }

    /**
     * 自递减设置, 默认递减因子: 1
     *
     * @param key
     * @return
     */
    public Long decrement(String key) {
        return valueOperations.decrement(key);
    }

    /**
     * 指定自递减因子的设置
     *
     * @param key
     * @param delta
     * @return
     */
    public Long decrement(String key, Long delta) {
        if (delta > 0L) {
            throw new RuntimeException("Decrease factor cannot be greater than 0!");
        }
        return valueOperations.decrement(key, delta);
    }

    // ===============================stream======================

    /**
     * 获取stream流中的消息数量
     *
     * @param streamName
     * @return
     */
    public Long streamSize(String streamName) {
        return streamOperations.size(streamName);
    }

    /**
     * 限制stream流的消息数量
     *
     * @param streamName
     * @param limitCount
     * @return 设置长度限制后被删除的数量
     */
    public Long streamTrim(String streamName, Long limitCount) {
        return streamOperations.trim(streamName, limitCount);
    }

    /**
     * ack消息应答
     *
     * @param streamName
     * @param streamGroup
     * @param record
     */
    public Long acknowledge(String streamName, String streamGroup, MapRecord<String, String, String> record) {
        return acknowledge(streamName, streamGroup, record.getId().toString());
    }

    /**
     * ack消息应答
     *
     * @param streamName
     * @param streamGroup
     * @param recordId
     */
    public Long acknowledge(String streamName, String streamGroup, String recordId) {
        return streamOperations.acknowledge(streamName, streamGroup, recordId);
    }

    /**
     * ack消息应答
     *
     * @param streamGroup
     * @param record
     */
    public Long acknowledge(String streamGroup, MapRecord<String, String, String> record) {
        return streamOperations.acknowledge(streamGroup, record);
    }

    /**
     * 发布stream流消息
     *
     * @param streamName
     * @param content
     * @return 消息ID
     */
    public RecordId addStream(String streamName, Map<String, String> content) {
        return streamOperations.add(streamName, content);
    }

    /**
     * 发布stream流消息
     *
     * @param streamName
     * @param recordId   自定义消息ID必须保持自增 (建议和时间戳结合)
     * @param content
     * @return
     */
    public RecordId addStream(String streamName, String recordId, Map<String, String> content) {
        StringRecord record = StreamRecords.newRecord().in(streamName)
                .withId(recordId)
                .ofStrings(content);
        return streamOperations.add(record);
    }

    /**
     * 创建stream组
     *
     * @param streamName
     * @param groupName
     * @return
     */
    public String addGroup(String streamName, String groupName) {
        return streamOperations.createGroup(streamName, groupName);
    }

    /**
     * 删除消息
     *
     * @param streamName
     * @param record
     * @return
     */
    public Long deleteStream(String streamName, MapRecord<String, String, String> record) {
        return deleteStream(streamName, record.getId().toString());
    }

    /**
     * 删除消息
     *
     * @param streamName
     * @param recordId
     * @return
     */
    public Long deleteStream(String streamName, String recordId) {
        return streamOperations.delete(streamName, recordId);
    }

    /**
     * 获取stream消费组中的pending消息信息, 命令: XPENDING
     *
     * @param streamName
     * @param streamGroup
     * @return
     */
    public PendingMessagesSummary pending(String streamName, String streamGroup) {
        return streamOperations.pending(streamName, streamGroup);
    }

    /**
     * 获取消费者组中指定消费者中的pending消息信息 (默认查询pending列表所有消息)
     *
     * @param streamName
     * @param streamGroup
     * @param consumerName
     * @return
     */
    public PendingMessages pending(String streamName, String streamGroup, String consumerName) {
        return streamOperations.pending(streamName, Consumer.from(streamGroup, consumerName), Range.closed("0", "+"), Integer.MAX_VALUE);
    }

    /**
     * 获取stream通道中指定消息ID的消息信息
     *
     * @param streamName
     * @param messageId
     * @return
     */
    public List<MapRecord<String, String, String>> range(String streamName, String messageId) {
        return streamOperations.range(streamName, Range.closed(messageId, messageId), RedisZSetCommands.Limit.limit().count(1));
    }

    /**
     * 获取stream通道中范围消息ID消息信息
     *
     * @param streamName
     * @param fromMessageId
     * @param toMessageId
     * @return
     */
    public List<MapRecord<String, String, String>> range(String streamName, String fromMessageId, String toMessageId) {
        return streamOperations.range(streamName, Range.closed(fromMessageId, toMessageId));
    }


    // ===============================listOperations======================

    /**
     * 将元素插入到key列表的表头(最后的值在最前面)
     *
     * @param key
     * @param object
     * @return
     */
    public void leftPush(String key, Object object) {
        leftPush(key, Constants.COMMON_CODE.LONG_ZONE, object);
    }

    /**
     * 将元素插入到key列表的表头(最后的值在最前面)
     *
     * @param key
     * @param timeout timeout > 0 设置此key的list数据过期时间, 默认时间单位: 秒
     * @param object
     */
    public void leftPush(String key, Long timeout, Object object) {
        listOperations.leftPush(key, object);
        if (timeout > Constants.COMMON_CODE.LONG_ZONE) {
            expire(key, timeout, TimeUnit.SECONDS);
        }
    }

    /**
     * 将多个元素插入到key列表的表头(最后的值在最前面)
     *
     * @param key
     * @param objects
     * @return
     */
    public void leftPushAll(String key, Object... objects) {
        leftPushAll(key, Constants.COMMON_CODE.LONG_ZONE, objects);
    }

    /**
     * 将多个元素插入到key列表的表头(最后的值在最前面)
     *
     * @param key
     * @param timeout timeout > 0 设置此key的list数据过期时间, 默认时间单位: 秒
     * @param objects
     */
    public void leftPushAll(String key, Long timeout, Object... objects) {
        listOperations.leftPushAll(key, objects);
        if (timeout > Constants.COMMON_CODE.LONG_ZONE) {
            expire(key, timeout, TimeUnit.SECONDS);
        }
    }

    /**
     * 将多个元素插入到key列表的表头(最后的值在最前面)
     *
     * @param key
     * @param collection
     * @return
     */
    public void leftPushAll(String key, Collection<?> collection) {
        listOperations.leftPushAll(key, collection);
    }

    /**
     * 将多个元素插入到key列表的表头(最后的值在最前面)
     *
     * @param key
     * @param timeout    timeout > 0 设置此key的list数据过期时间, 默认时间单位: 秒
     * @param collection
     */
    public void leftPushAll(String key, Long timeout, Collection<?> collection) {
        listOperations.leftPushAll(key, collection);
        if (timeout > Constants.COMMON_CODE.LONG_ZONE) {
            expire(key, timeout, TimeUnit.SECONDS);
        }
    }

    /**
     * 获取key列表种元素数量
     *
     * @param key
     * @return
     */
    public Long listSize(String key) {
        return listOperations.size(key);
    }

    // ===============================Redis Bloom======================

    /**
     * 根据给定的布隆过滤器添加值
     */
    public void addBloomValue(String key, String value) {
        Arrays.stream(bloomHandler.murmurHashOffset(value)).forEach(i -> valueOperations.setBit(key, i, true));
    }

    /**
     * 根据给定的布隆过滤器判断值是否存在, true: 存在, false: 不存在
     * BloomFilter 可以绝对判断某个元素一定不存在,但是不能绝对判断某个一定存在
     */
    public boolean includeBloomValue(String key, String value) {
        for (int i : bloomHandler.murmurHashOffset(value)) {
            // 如果有一个bit位的值不是 1 ,那么此值一定不存在
            Boolean bit = valueOperations.getBit(key, i);
            if (Objects.nonNull(bit) && !bit) {
                return false;
            }
        }
        return true;
    }

    // ===============================setOperations======================

    /**
     * 将一个或多个元素加入到集合 key 当中 (重复value将不会被重复添加)
     *
     * @param key
     * @param value
     * @return java.lang.Long (添加元素成功的数量)
     * @author mabin
     * @date 2023/4/20 13:52
     **/
    public Long add(String key, Object... value) {
        return setOperations.add(key, value);
    }

    /**
     * 返回集合 key 中元素数量（当key不存在时返回0）
     *
     * @param key
     * @return java.lang.Long
     * @author mabin
     * @date 2023/4/20 13:55
     **/
    public Long size(String key) {
        return setOperations.size(key);
    }

    /**
     * 获取所有集合 key 中的差集
     *
     * @param key
     * @param otherKey
     * @return java.util.Set<java.lang.Object>
     * @author mabin
     * @date 2023/4/20 15:08
     **/
    public Set<Object> difference(String key, String otherKey) {
        return setOperations.difference(key, otherKey);
    }

    /**
     * 获取所有集合 key 中的差集
     *
     * @param keys
     * @return java.util.Set<java.lang.Object>
     * @author mabin
     * @date 2023/4/20 15:09
     **/
    public Set<Object> difference(Collection<String> keys) {
        return setOperations.difference(keys);
    }

    /**
     * 获取所有集合 key 中的差集
     *
     * @param key
     * @param otherKeys
     * @return java.util.Set<java.lang.Object>
     * @author mabin
     * @date 2023/4/20 15:15
     **/
    public Set<Object> difference(String key, Collection<String> otherKeys) {
        return setOperations.difference(key, otherKeys);
    }

    /**
     * 获取所有集合 key 中的差集，将结果保存到 destKey 集合（destKey重复将被覆盖）
     *
     * @param key
     * @param otherKey
     * @param destKey
     * @return java.lang.Long
     * @author mabin
     * @date 2023/4/20 15:17
     **/
    public Long differenceAndStore(String key, String otherKey, String destKey) {
        return setOperations.differenceAndStore(key, otherKey, destKey);
    }

    /**
     * 获取所有集合 key 中的差集，将结果保存到 destKey 集合（destKey重复将被覆盖）
     *
     * @param key
     * @param otherKeys
     * @param destKey
     * @return java.lang.Long
     * @author mabin
     * @date 2023/4/20 15:18
     **/
    public Long differenceAndStore(String key, Collection<String> otherKeys, String destKey) {
        return setOperations.differenceAndStore(key, otherKeys, destKey);
    }

    /**
     * 获取所有集合 key 中的差集，将结果保存到 destKey 集合（destKey重复将被覆盖）
     *
     * @param otherKeys
     * @param destKey
     * @return java.lang.Long
     * @author mabin
     * @date 2023/4/20 15:19
     **/
    public Long differenceAndStore(Collection<String> otherKeys, String destKey) {
        return setOperations.differenceAndStore(otherKeys, destKey);
    }

    /**
     * 获取所有集合元素的交集（如果给定集合中有一个空集，结果也为空集）
     *
     * @param key
     * @param otherKey
     * @return java.util.Set<java.lang.Object>
     * @author mabin
     * @date 2023/4/21 10:33
     **/
    public Set<Object> intersect(String key, String otherKey) {
        return setOperations.intersect(key, otherKey);
    }

    /**
     * 获取所有集合元素的交集（如果给定集合中有一个空集，结果也为空集）
     *
     * @param keys
     * @return java.util.Set<java.lang.Object>
     * @author mabin
     * @date 2023/4/21 10:33
     **/
    public Set<Object> intersect(Collection<String> keys) {
        return setOperations.intersect(keys);
    }

    /**
     * 获取所有集合元素的交集（如果给定集合中有一个空集，结果也为空集）
     *
     * @param key
     * @param otherKeys
     * @return java.util.Set<java.lang.Object>
     * @author mabin
     * @date 2023/4/21 10:34
     **/
    public Set<Object> intersect(String key, Collection<String> otherKeys) {
        return setOperations.intersect(key, otherKeys);
    }

    /**
     * 获取所有集合元素的交集，将结果保存到 destKey 集合（destKey重复将被覆盖）
     *
     * @param key
     * @param otherKey
     * @param destKey
     * @return java.lang.Long
     * @author mabin
     * @date 2023/4/21 10:36
     **/
    public Long intersectAndStore(String key, String otherKey, String destKey) {
        return setOperations.intersectAndStore(key, otherKey, destKey);
    }

    /**
     * 获取所有集合元素的交集，将结果保存到 destKey 集合（destKey重复将被覆盖）
     *
     * @param key
     * @param otherKeys
     * @param destKey
     * @return java.lang.Long
     * @author mabin
     * @date 2023/4/21 10:41
     **/
    public Long intersectAndStore(String key, Collection<String> otherKeys, String destKey) {
        return setOperations.intersectAndStore(key, otherKeys, destKey);
    }

    /**
     * 获取所有集合元素的交集，将结果保存到 destKey 集合（destKey重复将被覆盖）
     *
     * @param otherKeys
     * @param destKey
     * @return java.lang.Long
     * @author mabin
     * @date 2023/4/21 10:41
     **/
    public Long intersectAndStore(Collection<String> otherKeys, String destKey) {
        return setOperations.intersectAndStore(otherKeys, destKey);
    }

    /**
     * 返回所有集合元素的并集
     *
     * @param key
     * @param otherKey
     * @return java.util.Set<java.lang.Object>
     * @author mabin
     * @date 2023/4/21 11:47
     **/
    public Set<Object> union(String key, String otherKey) {
        return setOperations.union(key, otherKey);
    }

    /**
     * 返回所有集合元素的并集
     *
     * @param keys
     * @return java.util.Set<java.lang.Object>
     * @author mabin
     * @date 2023/4/21 11:47
     **/
    public Set<Object> union(Collection<String> keys) {
        return setOperations.union(keys);
    }

    /**
     * 返回所有集合元素的并集
     *
     * @param key
     * @param otherKeys
     * @return java.util.Set<java.lang.Object>
     * @author mabin
     * @date 2023/4/21 11:47
     **/
    public Set<Object> union(String key, Collection<String> otherKeys) {
        return setOperations.union(key, otherKeys);
    }

    /**
     * 获取所有集合元素的并集，将结果保存到 destKey 集合（destKey重复将被覆盖）
     *
     * @param key
     * @param otherKey
     * @param destKey
     * @return java.lang.Long
     * @author mabin
     * @date 2023/4/21 11:48
     **/
    public Long unionAndStore(String key, String otherKey, String destKey) {
        return setOperations.unionAndStore(key, otherKey, destKey);
    }

    /**
     * 获取所有集合元素的并集，将结果保存到 destKey 集合（destKey重复将被覆盖）
     *
     * @param key
     * @param otherKeys
     * @param destKey
     * @return java.lang.Long
     * @author mabin
     * @date 2023/4/21 11:49
     **/
    public Long unionAndStore(String key, Collection<String> otherKeys, String destKey) {
        return setOperations.unionAndStore(key, otherKeys, destKey);
    }

    /**
     * 获取所有集合元素的并集，将结果保存到 destKey 集合（destKey重复将被覆盖）
     *
     * @param otherKeys
     * @param destKey
     * @return java.lang.Long
     * @author mabin
     * @date 2023/4/21 11:49
     **/
    public Long unionAndStore(Collection<String> otherKeys, String destKey) {
        return setOperations.unionAndStore(otherKeys, destKey);
    }

    /**
     * 判断 value 是否集合key的元素
     *
     * @param key
     * @param value
     * @return java.lang.Boolean
     * @author mabin
     * @date 2023/4/21 10:44
     **/
    public Boolean isMember(String key, Object value) {
        return setOperations.isMember(key, value);
    }

    /**
     * 获取集合 Key 中的所有元素
     *
     * @param key
     * @return java.util.Set<java.lang.Object>
     * @author mabin
     * @date 2023/4/21 11:12
     **/
    public Set<Object> members(String key) {
        return setOperations.members(key);
    }

    /**
     * 将 key 集合中的 value 元素 移动到 destKey 集合（如果destKey集合已包含了value元素，将会执行 把key集合中的value元素删除的操作）
     *
     * @param key
     * @param value
     * @param destKey
     * @return java.lang.Boolean
     * @author mabin
     * @date 2023/4/21 11:17
     **/
    public Boolean move(String key, Object value, String destKey) {
        return setOperations.move(key, value, destKey);
    }

    /**
     * 移除并返回集合 key 中的一个随机元素
     *
     * @param key
     * @return java.lang.Object
     * @author mabin
     * @date 2023/4/21 11:26
     **/
    public Object pop(String key) {
        List<Object> objects = pop(key, 1L);
        if (objects != null && objects.size() > 0) {
            return objects.get(0);
        }
        return null;
    }

    /**
     * 移除并返回集合 key 中的 count 个随机元素
     *
     * @param key
     * @param count
     * @return java.util.List<java.lang.Object>
     * @author mabin
     * @date 2023/4/21 11:26
     **/
    public List<Object> pop(String key, Long count) {
        return setOperations.pop(key, count);
    }

    /**
     * 返回集合 key 中一个随机元素
     *
     * @param key
     * @return java.lang.Object
     * @author mabin
     * @date 2023/4/21 11:35
     **/
    public Object randomMember(String key) {
        List<Object> objects = randomMembers(key, 1L);
        if (objects != null && objects.size() > 0) {
            return objects.get(0);
        }
        return null;
    }

    /**
     * 返回集合 key 中 count 个随机元素（如果count为负数，返回的元素可能会有重复）
     *
     * @param key
     * @param count
     * @return java.util.List<java.lang.Object>
     * @author mabin
     * @date 2023/4/21 11:35
     **/
    public List<Object> randomMembers(String key, Long count) {
        return setOperations.randomMembers(key, count);
    }

    /**
     * 返回集合 key 中 count 个随机元素（返回不重复元素集合）
     *
     * @param key
     * @param count
     * @return java.util.Set<java.lang.Object>
     * @author mabin
     * @date 2023/4/21 11:36
     **/
    public Set<Object> distinctRandomMembers(String key, Long count) {
        return setOperations.distinctRandomMembers(key, count);
    }

    /**
     * 移除集合 key 中的一个或多个指定的元素
     *
     * @param key
     * @param collection
     * @return java.lang.Long
     * @author mabin
     * @date 2023/4/21 11:43
     **/
    public Long remove(String key, Collection<?> collection) {
        return remove(key, collection.toArray());
    }

    /**
     * 移除集合 key 中的一个或多个指定的元素
     *
     * @param key
     * @param values
     * @return java.lang.Long
     * @author mabin
     * @date 2023/4/21 11:43
     **/
    public Long remove(String key, Object... values) {
        return setOperations.remove(key, values);
    }

    // ===============================zSetOperations======================
    public void zSetTest() {
        DefaultTypedTuple<Object> typedTuple = new DefaultTypedTuple<>("666", 10D);
        Set<ZSetOperations.TypedTuple<Object>> typedTuples = new HashSet<>();
        typedTuples.add(typedTuple);
        zSetOperations.add("1", typedTuples);


    }

    /**
     * 向集合 key 中添加一个元素 (如果元素已存在, 则更新元素的score值)
     *
     * @param key
     * @param value
     * @param score
     * @return java.lang.Boolean
     * @author mabin
     * @date 2023/6/10 11:12
     **/
    public Boolean zAdd(String key, Object value, Double score) {
        return zSetOperations.add(key, value, score);
    }

    /**
     * 向集合 key 中添加一个或多个元素 (如果元素已存在, 则更新元素的score值)
     *
     * @param key
     * @param typedTuples
     * @return java.lang.Long 被成功添加的新元素成员的数量, 不包括那些已经存在但被更新了的元素成员
     * @author mabin
     * @date 2023/6/10 11:14
     **/
    public Long zAdd(String key, Set<ZSetOperations.TypedTuple<Object>> typedTuples) {
        return zSetOperations.add(key, typedTuples);
    }

    /**
     * 返回集合 key 中的成员数量
     *
     * @param key
     * @return java.lang.Long
     * @author mabin
     * @date 2023/6/10 11:18
     **/
    public Long zCard(String key) {
        return zSetOperations.zCard(key);
    }

    /**
     * 返回有序集 key 中， score 值在 min 和 max 之间(默认包括 score 值等于 min 或 max )的成员的数量
     * 当 min=0, max=-1 时, 返回有序集 key 的所有成员数量
     *
     * @param key
     * @param min
     * @param max
     * @return java.lang.Long
     * @author mabin
     * @date 2023/6/10 11:21
     **/
    public Long zCount(String key, double min, double max) {
        return zSetOperations.count(key, min, max);
    }

    /**
     * 为有序集 key 中的成员 member 的 score 值加上增量 increment (当传递一个负数值时, 表示减去 score 值)
     *
     * @param key
     * @param value
     * @param delta
     * @return java.lang.Double 成员 member 更新后的 score 值
     * @author mabin
     * @date 2023/6/10 11:30
     **/
    public Double zScore(String key, Object value, double delta) {
        return zSetOperations.incrementScore(key, value, delta);
    }

    /**
     * 返回有序集 key 中，指定区间内的成员。
     * 其中成员的位置按 score 值递增(从小到大)来排序。
     * 具有相同 score 值的成员按字典序(lexicographical order )来排列。
     * 如果你需要成员按 score 值递减(从大到小)来排列，请使用 ZREVRANGE 命令。
     * 下标参数 start 和 stop 都以 0 为底，也就是说，以 0 表示有序集第一个成员，以 1 表示有序集第二个成员，以此类推。
     * 也可以使用负数下标，以 -1 表示最后一个成员， -2 表示倒数第二个成员，以此类推
     *
     * @param key
     * @param start
     * @param end
     * @return java.util.Set<java.lang.Object>
     * @author mabin
     * @date 2023/6/10 11:39
     **/
    public Set<Object> range(String key, long start, long end) {
        return zSetOperations.range(key, start, end);
    }


}
