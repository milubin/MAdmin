package top.horsemuzi.system.core.schedule.quartz.service;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.stereotype.Service;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.ExceptionUtil;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.core.schedule.quartz.base.QuartzConstants;
import top.horsemuzi.system.core.schedule.quartz.handle.QuartzDisallowConcurrentExecution;
import top.horsemuzi.system.core.schedule.quartz.handle.QuartzJobExecution;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobDTO;

import static top.horsemuzi.system.core.schedule.quartz.base.QuartzConstants.MISFIRE_POLICY.*;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description Quartz调度任务的核心处理
 * @date 2022/8/21 16:38
 */
@Slf4j
@Service
public class QuartzHandleService {

    /**
     * 新增调度任务
     *
     * @param scheduler 调度器
     * @param job 调度任务信息
     */
    public void addJob(Scheduler scheduler, QuartzJobDTO job) {
        try {
            // 创建job任务
            JobKey jobKey = new JobKey(job.getJobName(), job.getJobGroup());
            JobDetail jobDetail = JobBuilder.newJob(getJobClass(job.getConcurrent()))
                    .withIdentity(jobKey)
                    .withDescription(job.getRemark())
                    .build();
            // 放入调度任务信息, 后续执行任务调度时可获取
            jobDetail.getJobDataMap().put(QuartzConstants.QUARTZ_JOB, JsonUtil.toJson(job));

            // 创建Trigger触发器
            Trigger trigger = null;
            if (ObjectUtil.equals(job.getJobModel(), Constants.COMMON_JUDGMENT.ZONE)) {
                // cron任务模式
                // 构建Cron类型触发器
                ValidatorUtil.isTrue(CronExpression.isValidExpression(job.getCronExpression()),
                        ResultCodeEnum.CRON_EXPRESSION_ERROR);
                CronScheduleBuilder cronSchedule = CronScheduleBuilder.cronSchedule(job.getCronExpression());
                cronSchedule = getCronScheduleMisfirePolicy(job.getMisfirePolicy(), cronSchedule);
                trigger = TriggerBuilder.newTrigger()
                        .withIdentity(job.getTriggerName(), job.getTriggerGroup())
                        .withSchedule(cronSchedule)
                        .startNow()
                        .build();
            } else {
                // 间隔任务模式
                ValidatorUtil.isTrue(ObjectUtil.isNotEmpty(job.getJobInterval()) && job.getJobInterval() > Constants.COMMON_CODE.ZONE
                        , ResultCodeEnum.QUARTZ_JOB_INTERVAL_ERROR);
                if (ObjectUtil.isNull(job.getRepeatCount()) || job.getRepeatCount() < Constants.COMMON_CODE.ZONE) {
                    job.setRepeatCount(Constants.COMMON_CODE.BURDEN_ONE);
                    // 无限次数的间隔执行任务
                    TriggerBuilder<SimpleTrigger> simpleTriggerTriggerBuilder = TriggerBuilder.newTrigger()
                            .withIdentity(job.getTriggerName(), job.getTriggerGroup())
                            .withSchedule(SimpleScheduleBuilder.repeatSecondlyForever(Constants.COMMON_CODE.ONE)
                                    .withIntervalInSeconds(job.getJobInterval()));
                    if (ObjectUtil.isNull(job.getStartTime())) {
                        simpleTriggerTriggerBuilder.startNow();
                    } else {
                        simpleTriggerTriggerBuilder.startAt(job.getStartTime());
                    }
                    trigger = simpleTriggerTriggerBuilder.build();
                } else {
                    TriggerBuilder<SimpleTrigger> simpleTriggerTriggerBuilder = TriggerBuilder.newTrigger()
                            .withIdentity(job.getTriggerName(), job.getTriggerGroup())
                            .withSchedule(SimpleScheduleBuilder.repeatSecondlyForever(Constants.COMMON_CODE.ONE)
                                    .withIntervalInSeconds(job.getJobInterval())
                                    .withRepeatCount(job.getRepeatCount()));
                    if (ObjectUtil.isNull(job.getStartTime())) {
                        simpleTriggerTriggerBuilder.startNow();
                    } else {
                        simpleTriggerTriggerBuilder.startAt(job.getStartTime());
                    }
                    trigger = simpleTriggerTriggerBuilder.build();
                }
            }
            // 调度任务是否已存在
            if (scheduler.checkExists(jobKey)) {
                scheduler.deleteJob(jobKey);
            }
            // 建立任务调度关联
            scheduler.scheduleJob(jobDetail, trigger);
            // 设置任务状态
            if (ObjectUtil.isNotNull(job.getJobStatus()) && ObjectUtil.equal(job.getJobStatus(),
                    Constants.COMMON_JUDGMENT.ONE)) {
                scheduler.pauseJob(jobKey);
            }
        } catch (SchedulerException e) {
            log.error(ExceptionUtil.getMessage(e));
            throw new BusinessException(ResultCodeEnum.QUARTZ_JOB_ADD_ERROR);
        }
    }


    /**
     * 获取cron任务模式的计划执行错误策略
     * @param misfirePolicy
     * @param cronSchedule
     * @return
     */
    private CronScheduleBuilder getCronScheduleMisfirePolicy(Integer misfirePolicy,
                                                             CronScheduleBuilder cronSchedule) {
        if (ObjectUtil.isNull(misfirePolicy)) {
            return cronSchedule;
        }
        String flag = Convert.toStr(misfirePolicy);
        switch (flag) {
            case MISFIRE_DEFAULT:
                return cronSchedule;
            case MISFIRE_IGNORE_MISFIRES:
                return cronSchedule.withMisfireHandlingInstructionIgnoreMisfires();
            case MISFIRE_FIRE_AND_PROCEED:
                return cronSchedule.withMisfireHandlingInstructionFireAndProceed();
            case MISFIRE_DO_NOTHING:
                return cronSchedule.withMisfireHandlingInstructionDoNothing();
            default:
                log.error("Quartz调度任务的cron错误策略不存在");
                throw new BusinessException(ResultCodeEnum.QUARTZ_JOB_MISFIRE_POLICY_ERROR);
        }
    }

    /**
     * 获取quartz具体任务类
     *
     * @param concurrent 是否并发执行
     * @return
     */
    private Class<? extends Job> getJobClass(Integer concurrent) {
        return ObjectUtil.isNull(concurrent) || ObjectUtil.equal(concurrent, Constants.COMMON_JUDGMENT.ZONE)
                ? QuartzJobExecution.class : QuartzDisallowConcurrentExecution.class;
    }

}
