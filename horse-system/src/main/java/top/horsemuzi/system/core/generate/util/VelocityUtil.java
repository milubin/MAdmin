package top.horsemuzi.system.core.generate.util;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.velocity.VelocityContext;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.system.config.properties.SystemProperties;
import top.horsemuzi.system.pojo.dto.generate.TableConfigDTO;
import top.horsemuzi.system.pojo.dto.generate.TableDTO;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 模板处理工具类
 *
 * @author ruoyi
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class VelocityUtil {

    /**
     * 设置模板变量信息
     * 以 tb_user、tb_user_role 两个表做模板值示例
     *
     * @return 模板列表
     */
    public static VelocityContext prepareContext(TableDTO tableDTO) {
        VelocityContext velocityContext = new VelocityContext();
        // 示例: tb_user, tb_user_role
        velocityContext.put("tableName", tableDTO.getTableName());
        // 示例: 用户信息、用户角色关联信息
        velocityContext.put("functionName", StrUtil.isNotBlank(tableDTO.getFunctionName()) ? tableDTO.getFunctionName() : "【请填写功能名称】");
        // 示例: 用户信息表
        velocityContext.put("tableComment", tableDTO.getTableComment());
        // 示例: User、UserRole
        velocityContext.put("ClassName", tableDTO.getClassName());
        // 示例: user、userRole
        velocityContext.put("className", StrUtil.lowerFirst(tableDTO.getClassName()));
        // 示例: tool
        velocityContext.put("moduleName", tableDTO.getTableConfigDTO().getModuleName());
        // 示例: top.horsemuzi.system
        velocityContext.put("packageName", tableDTO.getTableConfigDTO().getPackageName());
        // 示例: top.horsemuzi
        velocityContext.put("basePackage", StrUtil.subBefore(tableDTO.getTableConfigDTO().getPackageName(), StrUtil.DOT, true));
        // 示例: Mr.Horse
        velocityContext.put("author", tableDTO.getTableConfigDTO().getAuthor());
        // 示例: 2022-02-02 22:22:22 (注释日期格式化)
        velocityContext.put("datetime", StrUtil.isBlank(tableDTO.getTableConfigDTO().getFormatCommentDate())
                ? DateUtil.formatDateTime(new Date())
                : DateUtil.format(new Date(), tableDTO.getTableConfigDTO().getFormatCommentDate()));
        // 说明: 表主键字段全部信息, 主要用到 pkColumn.javaType, pkColumn.javaField
        velocityContext.put("pkColumn", tableDTO.getPrimaryTableColumnDTO());
        // 说明: 首字母大写的主键字段名称
        velocityContext.put("pkNameUpperFirst", StrUtil.upperFirst(tableDTO.getPrimaryTableColumnDTO().getJavaName()));
        // 示例: 引入的特殊java依赖包
        velocityContext.put("importList", tableDTO.getImportPackages());
        // 示例: 权限菜单前缀, system:user
        velocityContext.put("permissionPrefix", StrUtil.format("{}:{}", tableDTO.getTableConfigDTO().getModuleName(), StrUtil.lowerFirst(tableDTO.getClassName())));
        // 说明: 业务表tb_user关联的全部表字段信息
        velocityContext.put("columns", tableDTO.getTableColumnDTOList());
        // 说明: 业务表tb_user全量信息
        velocityContext.put("table", tableDTO);
        // 说明: 业务表tb_user的全量配置信息
        velocityContext.put("tableConfig", tableDTO.getTableConfigDTO());
        // 说明: 字典名称，英文逗号分隔
        Set<String> dictTypes = new HashSet<>(16);
        tableDTO.getTableColumnDTOList().forEach(tableColumnDTO -> {
            if (StrUtil.isNotBlank(tableColumnDTO.getFormDict())) {
                dictTypes.add(tableColumnDTO.getFormDict());
            }
            if (StrUtil.isNotBlank(tableColumnDTO.getQueryFormDict())) {
                dictTypes.add(tableColumnDTO.getQueryFormDict());
            }
        });
        velocityContext.put("dicts", CollUtil.join(dictTypes, StrUtil.COMMA));
        // 说明: 是否设置实时检索字段
        velocityContext.put("fetch", CollUtil.isNotEmpty(tableDTO.getTableColumnDTOList().stream()
                .filter(item -> StrUtil.equals(Constants.COMMON_CODE.STR_ZONE, item.getQueryFormFetch())
                        && StrUtil.equals(Constants.COMMON_CODE.STR_ZONE, item.getQueryItem()))
                .collect(Collectors.toList())));
        // 说明: 项目版本, 默认: 1.0.0
        velocityContext.put("projectVersion", StrUtil.isNotBlank(tableDTO.getTableConfigDTO().getProjectVersion())
                ? tableDTO.getTableConfigDTO().getProjectVersion() : SystemProperties.version);
        return velocityContext;
    }


    /**
     * 获取模板信息(自动遍历resources/vm文件目录下的所有文件组装成文件路径列表)
     * 结果示例: [vm/java/commonCodeEnum.java.vm, vm/java/controller.java.vm, vm/java/dto.java.vm, vm/java/entity.java.vm...]
     *
     * @return 模板列表
     */
    public static List<String> getTemplateList() {
        List<String> templates = new ArrayList<>();
        try {
            PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
            Resource[] resources = resolver.getResources("classpath:vm/**/*.vm");
            for (Resource resource : resources) {
                templates.add("vm" + StrUtil.subAfter(resource.getURL().getPath(), "/vm", true));
            }
        } catch (IOException e) {
            log.error("load vm template file exception", e);
        }
        return templates;
    }


    /**
     * 获取模板文件路径名
     *
     * @param template
     * @param tableDTO
     * @return
     */
    public static String getTemplateFilePathName(String template, TableDTO tableDTO) {
        // 完整包路径: main/java/top/horsemuzi/system/
        TableConfigDTO tableConfigDTO = tableDTO.getTableConfigDTO();
        // 默认路径
        String javaPath = "main/java/" + StrUtil.replace(tableConfigDTO.getPackageName(), StrUtil.DOT, File.separator);
        // 拼接文件包路径名称
        if (template.contains("dto.java.vm")) {
            return StrUtil.format("{}/pojo/dto/{}/{}DTO.java", javaPath, StrUtil.lowerFirst(tableDTO.getClassName()), tableDTO.getClassName());
        } else if (template.contains("query.java.vm")) {
            return StrUtil.format("{}/pojo/dto/{}/{}Query.java", javaPath, StrUtil.lowerFirst(tableDTO.getClassName()), tableDTO.getClassName());
        } else if (template.contains("entity.java.vm")) {
            return StrUtil.format("{}/pojo/entity/{}.java", javaPath, tableDTO.getClassName());
        } else if (template.contains("mapper.java.vm")) {
            return StrUtil.format("{}/mapper/{}.java", javaPath, tableConfigDTO.getMapperName());
        } else if (template.contains("service.java.vm")) {
            return StrUtil.format("{}/service/{}.java", javaPath, tableConfigDTO.getServiceName());
        } else if (template.contains("serviceImpl.java.vm")) {
            return StrUtil.format("{}/service/impl/{}.java", javaPath, tableConfigDTO.getServiceImplName());
        } else if (template.contains("controller.java.vm")) {
            return StrUtil.format("{}/controller/{}.java", javaPath, tableConfigDTO.getControllerName());
        } else if (template.contains("manager.java.vm")) {
            return StrUtil.format("{}/manager/{}Manager.java", javaPath, tableDTO.getClassName());
        } else if (template.contains("commonCodeEnum.java.vm")) {
            return StrUtil.format("{}/base/{}CommonCodeEnum.java", javaPath, tableDTO.getClassName());
        } else if (template.contains("mapper.xml.vm")) {
            return StrUtil.format("main/resources/mapper/{}.xml", tableConfigDTO.getXmlName());
        } else if (template.contains("sql.vm")) {
            return "main/resources/sql/" + tableDTO.getClassName() + "permission.sql";
        } else if (template.contains("api.js.vm")) {
            return StrUtil.format("vue/api/{}.js", StrUtil.lowerFirst(tableDTO.getClassName()));
        } else if (template.contains("index.vue.vm")) {
            return StrUtil.format("vue/views/{}/index.vue", StrUtil.lowerFirst(tableDTO.getClassName()));
        }
        return StrUtil.EMPTY;
    }

}
