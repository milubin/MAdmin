package top.horsemuzi.system.core.gold.service;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.CharUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.util.EncryptUtil;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.http.HttpUtil;
import top.horsemuzi.system.core.gold.base.GoldRequestEnum;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.TreeMap;

/**
 * 高德服务API处理
 *
 * @author mabin
 * @date 2022/06/27 14:13
 **/
@Slf4j
@Service
public class GoldService {

    /**
     * 请求服务权限标识
     */
    @Value("${custom.gold.key}")
    private String key;

    /**
     * 请求参数签名私钥
     */
    @Value("${custom.gold.private_key}")
    private String privateKey;

    /**
     * 高德开放平台统一搜索
     *
     * @return
     */
    @Retryable(value = {Exception.class}, maxAttempts = 3, backoff = @Backoff(delay = 1500, multiplier = 1.0D))
    public String search(GoldRequestEnum.APIEnum apiEnum) throws Exception {
        if (ObjectUtil.isNull(apiEnum)) {
            return null;
        }
        log.info("{} 请求方法：{}, 请求URL: {}, 请求参数: {}", GoldRequestEnum.GOLD_HEADER, apiEnum.getMethod(), apiEnum.getUrl(), apiEnum.getParam());
        GoldRequestEnum.Param param = apiEnum.getParam();
        param.setKey(key);
        if (StrUtil.equals(apiEnum.getMethod(), Constants.METHOD.GET)) {
            return HttpUtil.HTTP_CLIENT.get(apiEnum.getUrl(), sign(JsonUtil.toJson(param)));
        } else if (StrUtil.equals(apiEnum.getMethod(), Constants.METHOD.POST)) {
            return HttpUtil.HTTP_CLIENT.post(apiEnum.getUrl(), sign(JsonUtil.toJson(param)), Boolean.TRUE);
        } else {
            log.error("{} 请求方法类型错误", GoldRequestEnum.GOLD_HEADER);
            return StrUtil.EMPTY;
        }
    }

    /**
     * 重试后异常回调处理
     *
     * @param e
     * @return
     */
    @Recover
    private String recover(Exception e) {
        log.error("{} 请求异常: {}", GoldRequestEnum.GOLD_HEADER, e);
        return StrUtil.EMPTY;
    }

    /**
     * 参数签名
     *
     * @param paramsJson
     * @return
     * @throws Exception
     */
    private Map<String, String> sign(String paramsJson) throws Exception {
        if (StrUtil.isBlank(paramsJson)) {
            return null;
        }
        Map<String, String> paramsMap = JsonUtil.fromJson(paramsJson, new TypeReference<Map<String, String>>() {
        });
        if (MapUtil.isEmpty(paramsMap)) {
            return null;
        }
        Map<String, String> treeMap = new TreeMap<>(paramsMap);
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<String, String> entry : treeMap.entrySet()) {
            builder.append(entry.getKey()).append("=").append(entry.getValue()).append(CharUtil.AMP);
        }
        String paramStr = StrUtil.subBefore(builder.toString(),CharUtil.AMP,true);
        String sign = EncryptUtil.md5(paramStr + privateKey);
        paramsMap.put("sig", URLEncoder.encode(sign, StandardCharsets.UTF_8.toString()));
        return paramsMap;
    }

}
