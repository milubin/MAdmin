package top.horsemuzi.system.core.redis.listener.stream;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.stream.MapRecord;
import org.springframework.data.redis.stream.StreamListener;

/**
 * Stream消费监听者
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/04/11 17:16
 **/
@Slf4j
public class AbstractConsumeStreamListener implements StreamListener<String, MapRecord<String, String, String>> {

    @SneakyThrows
    @Override
    public void onMessage(MapRecord<String, String, String> message) {

    }

}
