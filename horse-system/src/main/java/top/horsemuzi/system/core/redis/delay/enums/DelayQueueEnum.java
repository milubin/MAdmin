package top.horsemuzi.system.core.redis.delay.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * 延时队列枚举处理
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/05/13 10:15
 **/

@Getter
@ToString
@AllArgsConstructor
public enum DelayQueueEnum {

    DELAY_QUEUE_TEST("delay_queue_test", "延时队列测试", "delayQueueConsumeTest");

    /**
     * 延时队列 redis的key
     */
    private final String key;

    /**
     * 延时队列描述
     */
    private final String desc;

    /**
     * 延时队列具体业务实现的Bean名称, 可通过Spring的上下文获取
     */
    private final String bean;

}
