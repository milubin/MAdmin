package top.horsemuzi.system.core.rocketmq.producer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import top.horsemuzi.system.core.rocketmq.base.RocketConstants;
import top.horsemuzi.system.core.rocketmq.base.SendMessageResult;

/**
 * MQ消息生产者
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/06/03 16:52
 **/
@Slf4j
@Service
@RequiredArgsConstructor
@ConditionalOnProperty(prefix = "rocketmq", name = "enable", havingValue = "true")
public class ProducerService {

    private final ProducerHandle producerHandle;

    public SendMessageResult sendMessageTest(String messageBody) {
        return producerHandle.sendMessage(RocketConstants.TOPIC.TEST_TOPIC, RocketConstants.TAG.TEST_TAG, messageBody, null);
    }

    public SendMessageResult senDelayMessageTest(String messageBody, RocketConstants.DELAY delayLevel) {
        return producerHandle.sendMessage(RocketConstants.TOPIC.TEST_TOPIC, RocketConstants.TAG.TEST_TAG, messageBody, delayLevel);
    }

}
