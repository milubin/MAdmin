package top.horsemuzi.system.core.oss;

import cn.hutool.core.util.StrUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ConfigEnum;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.EncryptUtil;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.core.oss.enums.StrategyEnum;
import top.horsemuzi.core.oss.properties.ConfigProperties;
import top.horsemuzi.core.oss.service.StrategyAbstract;
import top.horsemuzi.system.config.properties.EncryptProperties;
import top.horsemuzi.system.core.redis.service.RedisService;
import top.horsemuzi.system.pojo.dto.oss.OssConfigDTO;
import top.horsemuzi.system.service.ConfigService;
import top.horsemuzi.system.service.OssConfigService;

/**
 * OSS服务实例工厂
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/03/27 10:22
 **/
@Slf4j
@Component
@RequiredArgsConstructor
public class OssFactory {

    private final ConfigService configService;
    private final RedisService redisService;
    private final OssConfigService ossConfigService;

    /**
     * 获取OSS默认实例
     *
     * @param
     * @return top.horsemuzi.core.oss.service.StrategyAbstract
     * @author mabin
     * @date 2023/3/27 10:49
     **/
    public StrategyAbstract instance() {
        // 获取默认OSS服务配置名称
        String serviceName = configService.getConfig(ConfigEnum.OSS_SERVICE).get(ConfigEnum.OSS_SERVICE.getKey());
        ValidatorUtil.notBlank(serviceName, ResultCodeEnum.CONFIG_NOT_EXIST_ERROR);
        return instance(serviceName);
    }

    /**
     * 获取指定OSS默认实例
     *
     * @param configKey
     * @return top.horsemuzi.core.oss.service.StrategyAbstract
     * @author mabin
     * @date 2023/3/27 14:31
     **/
    public StrategyAbstract instance(String configKey) {
        // 获取默认OSS服务配置
        OssConfigDTO ossConfigDTO;
        String cacheKey = Constants.REDIS_PREFIX.OSS_CONFIG + StrUtil.COLON + configKey;
        if (redisService.hasKey(cacheKey)) {
            ossConfigDTO = JsonUtil.fromJson(redisService.get(cacheKey), OssConfigDTO.class);
            ValidatorUtil.notEmpty(ossConfigDTO, ResultCodeEnum.OSS_CONFIG_NOT_EXIST_ERROR);
        } else {
            ossConfigDTO = ossConfigService.getOssConfig(configKey);
            ValidatorUtil.notEmpty(ossConfigDTO, ResultCodeEnum.OSS_CONFIG_NOT_EXIST_ERROR);
            ossConfigDTO.setSecretKey(EncryptUtil.sm4Encrypt(EncryptProperties.sm4Key, ossConfigDTO.getSecretKey()));
            redisService.set(cacheKey, JsonUtil.toJson(ossConfigDTO));
        }
        ossConfigDTO.setSecretKey(EncryptUtil.sm4Decrypt(EncryptProperties.sm4Key, ossConfigDTO.getSecretKey()));
        // 获取OSS服务实例
        StrategyAbstract strategy = StrategyEnum.getStrategy(configKey);
        // init 初始化客户端
        strategy.init(OrikaUtil.convert(ossConfigDTO, ConfigProperties.class));
        return strategy;
    }

}
