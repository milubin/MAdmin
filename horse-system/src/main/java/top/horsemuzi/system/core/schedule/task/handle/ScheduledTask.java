package top.horsemuzi.system.core.schedule.task.handle;

import cn.hutool.core.util.ObjectUtil;
import lombok.extern.slf4j.Slf4j;
import top.horsemuzi.system.core.schedule.task.base.TaskConstants;

import java.util.concurrent.ScheduledFuture;

/**
 * 取消定时任务处理
 *
 * @author mabin
 * @date 2022/09/06 09:04
 **/
@Slf4j
public final class ScheduledTask {

    volatile ScheduledFuture<?> scheduledFuture;

    public void cancel() {
        ScheduledFuture<?> future = this.scheduledFuture;
        if (ObjectUtil.isNotNull(future)) {
            log.info("{} 取消定时任务...", TaskConstants.TASK_HEADER);
            future.cancel(true);
        }
    }

}
