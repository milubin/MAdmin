package top.horsemuzi.system.core.redis.listener.stream;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.stream.MapRecord;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.MessageStatusEnum;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.core.message.common.MessageConstants;
import top.horsemuzi.core.message.common.MessageInfo;
import top.horsemuzi.core.message.enums.SendEnum;
import top.horsemuzi.system.core.redis.base.RedisConstants;
import top.horsemuzi.system.core.redis.service.RedisService;
import top.horsemuzi.system.manager.admin.MessageManager;
import top.horsemuzi.system.pojo.dto.message.MessageChannelDTO;
import top.horsemuzi.system.pojo.dto.message.MessageDTO;

import java.util.Collections;
import java.util.HashSet;

/**
 * 钉钉机器人Stream流消费者监听
 *
 * @author mabin
 * @date 2022/09/21 17:42
 **/
@Slf4j
@Component
@RequiredArgsConstructor
public class ConsumeDingBotStreamListener extends AbstractConsumeStreamListener {

    private static final String DING_BOT_STREAM_CONSUMER = "【钉钉机器人stream消费监听】";

    private final RedisService redisService;
    private final MessageManager messageManager;

    @SneakyThrows
    @Override
    public void onMessage(MapRecord<String, String, String> message) {
        log.info("{} 消费者监听消息内容 message={}", DING_BOT_STREAM_CONSUMER, message);
        // 参数校验
        String recordId = message.getId().toString();
        MessageDTO messageDTO = messageManager.getMessage(recordId);
        ValidatorUtil.notEmpty(messageDTO, ResultCodeEnum.MESSAGE_NOT_EXIST_ERROR);
        ValidatorUtil.notEmpty(messageDTO.getChannel(), ResultCodeEnum.MESSAGE_CHANNEL_NOT_EXIST_ERROR);
        log.info("{} 消息 recordId={} 的库信息 messageDTO={}", DING_BOT_STREAM_CONSUMER, message.getId(), JsonUtil.toJson(messageDTO));
        // 组装并发送消息
        if (MapUtil.isNotEmpty(message.getValue())) {
            // 获取消息内容, 默认Map结构中的key为对应Stream的字符串名称
            String content = message.getValue().get(RedisConstants.STREAM.DING_BOT.STREAM);
            if (StrUtil.isNotBlank(content)) {
                MessageInfo messageInfo = new MessageInfo();
                // 设置消息接收者
                String receiver = messageDTO.getReceiver();
                if (!StrUtil.equals(MessageConstants.SEND_ALL, receiver)) {
                    messageInfo.setReceiver(new HashSet<>(StrUtil.split(receiver, StrUtil.COMMA)));
                } else {
                    messageInfo.setReceiver(Collections.singleton(receiver));
                }
                SendEnum sendEnum = SendEnum.getSendEnum(messageDTO.getChannel());
                ValidatorUtil.notEmpty(sendEnum, ResultCodeEnum.MESSAGE_HANDLER_NOT_EXIST_ERROR);
                messageInfo.setContentModel(JsonUtil.fromJson(content, sendEnum.getContentModel().getClass()));
                MessageChannelDTO messageChannelDTO = messageManager.getMessageChannel(messageDTO.getChannel());
                ValidatorUtil.notEmpty(messageChannelDTO, ResultCodeEnum.MESSAGE_CHANNEL_NOT_EXIST_ERROR);
                messageInfo.setChannelConfig(JsonUtil.fromJson(messageChannelDTO.getConfig(), sendEnum.getChannelConfig().getClass()));
                messageDTO.setMessageInfo(JsonUtil.toJson(messageInfo));

                if (sendEnum.getSendHandler().send(messageInfo)) {
                    log.info("{} 消息 recordId={} 消费成功", DING_BOT_STREAM_CONSUMER, recordId);
                    messageDTO.setStatus(MessageStatusEnum.SUCCESS.getCode());
                    messageDTO.setRemark(MessageStatusEnum.SUCCESS.getText());
                    Long ackCount = redisService.acknowledge(message.getStream(), messageDTO.getStreamGroup(), recordId);
                    if (ackCount > Constants.COMMON_CODE.LONG_ZONE) {
                        log.info("{} 消息 recordId={} 消费且ACK应答成功", DING_BOT_STREAM_CONSUMER, recordId);
                        messageDTO.setStatus(MessageStatusEnum.ACK_SUCCESS.getCode());
                        messageDTO.setRemark(MessageStatusEnum.ACK_SUCCESS.getText());
                    }
                }
                messageManager.updateMessage(messageDTO);
            }
        }
    }

}
