package top.horsemuzi.system.core.redis.listener.expire;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * Redis的Key过期监听事件
 * 当前监听器监听范围: 监听所有db的过期事件__keyevent@*__:expired"
 *
 * @author mabin
 * @date 2022/06/13 09:10
 **/

@Slf4j
@Component
public class RedisKeyExpirationListener extends KeyExpirationEventMessageListener {

    public RedisKeyExpirationListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    @Override
    public void onMessage(Message message, byte[] pattern) {
        if (Objects.nonNull(message)) {
            String expireKey = message.toString();
            log.info("expireKey: {}", expireKey);
        }
    }
}
