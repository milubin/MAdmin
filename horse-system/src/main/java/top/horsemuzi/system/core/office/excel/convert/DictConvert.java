package top.horsemuzi.system.core.office.excel.convert;

import cn.hutool.core.annotation.AnnotationUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.metadata.property.ExcelContentProperty;
import lombok.extern.slf4j.Slf4j;
import top.horsemuzi.system.annotation.DictFormat;
import top.horsemuzi.system.core.office.service.OfficeService;
import top.horsemuzi.system.manager.admin.DictManager;
import top.horsemuzi.system.pojo.dto.dict.DictDataDTO;

import java.util.List;

/**
 * 系统数据字典字段转换
 *
 * @author 马滨
 * @date 2022/09/20 22:25
 **/
@Slf4j
public class DictConvert implements Converter<Object> {

    @Override
    public Class<?> supportJavaTypeKey() {
        return Object.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public Object convertToJavaData(ReadCellData<?> cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        DictFormat dictFormat = AnnotationUtil.getAnnotation(contentProperty.getField(), DictFormat.class);
        String type = dictFormat.type();
        // 字典数据, 数据库存储是String类型, 但是excel可能会识别为number类型, 例如(0,1)之类的字典数据
        String sourceValue = StrUtil.isBlank(cellData.getStringValue()) ? cellData.getNumberValue().stripTrailingZeros().toPlainString() : cellData.getStringValue();
        String dictValue = StrUtil.EMPTY;
        if (StrUtil.isBlank(type)) {
            dictValue = OfficeService.EXCEL.convertExp(dictFormat.convertExp(), sourceValue, Boolean.TRUE);
        } else {
            DictManager dictManager = SpringUtil.getBean(DictManager.class);
            List<DictDataDTO> dictDatas = dictManager.getDictDatas(type);
            if (CollectionUtil.isNotEmpty(dictDatas)) {
                for (DictDataDTO dictDataDTO : dictDatas) {
                    if (StrUtil.equals(dictDataDTO.getLabel(), sourceValue)) {
                        dictValue = dictDataDTO.getValue();
                        break;
                    }
                }
            }
        }
        return Convert.convert(contentProperty.getField().getType(), dictValue);
    }

    @Override
    public WriteCellData<?> convertToExcelData(Object object, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        if (ObjectUtil.isNull(object)) {
            return new WriteCellData<>(StrUtil.EMPTY);
        }
        DictFormat dictFormat = AnnotationUtil.getAnnotation(contentProperty.getField(), DictFormat.class);
        String type = dictFormat.type();
        String sourceValue = Convert.toStr(object);
        String dictValue = StrUtil.EMPTY;
        if (StrUtil.isBlank(type)) {
            dictValue = OfficeService.EXCEL.convertExp(dictFormat.convertExp(), sourceValue, Boolean.FALSE);
        } else {
            DictManager dictManager = SpringUtil.getBean(DictManager.class);
            List<DictDataDTO> dictDatas = dictManager.getDictDatas(type);
            if (CollectionUtil.isNotEmpty(dictDatas)) {
                for (DictDataDTO dictDataDTO : dictDatas) {
                    if (StrUtil.equals(dictDataDTO.getValue(), sourceValue)) {
                        dictValue = dictDataDTO.getLabel();
                        break;
                    }
                }
            }
        }
        return new WriteCellData<>(dictValue);
    }

}
