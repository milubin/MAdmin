package top.horsemuzi.system.core.schedule.task.handle;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StopWatch;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.util.ExceptionUtil;
import top.horsemuzi.common.util.NetUtil;
import top.horsemuzi.system.core.schedule.task.base.TaskConstants;
import top.horsemuzi.system.handle.event.spring.event.AsyncEvent;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskLogDTO;

import java.lang.reflect.Method;

/**
 * 异步任务实例（记录任务执行日志）
 *
 * @author mabin
 * @date 2022/09/06 09:34
 **/
@NoArgsConstructor
@Slf4j
@Component
public class TaskRunnable implements Runnable {

    private String className;
    private String methodName;
    private String methodParams;

    public TaskRunnable(String className, String methodName, String methodParams) {
        this.className = className;
        this.methodName = methodName;
        this.methodParams = methodParams;
    }

    public TaskRunnable(String className, String methodName) {
        this.className = className;
        this.methodName = methodName;
    }

    /**
     * 重写调度任务执行逻辑
     */
    @Override
    public void run() {
        MDC.put(Constants.TRACE_ID, IdUtil.nanoId(10));
        log.info("{} 任务开始执行 className={}, methodName={}, methodParams={}", TaskConstants.TASK_HEADER, className, methodName, methodParams);
        TaskLogDTO logDTO = new TaskLogDTO();
        logDTO.setThreadName(Thread.currentThread().getName());
        logDTO.setClassName(className);
        logDTO.setMethodName(methodName);
        logDTO.setMethodParams(methodParams);
        StopWatch watch = new StopWatch();
        watch.start();
        try {
            Object target = SpringUtil.getBean(Class.forName(className));
            Method method = null;
            if (StrUtil.isNotBlank(methodParams)) {
                method = target.getClass().getDeclaredMethod(methodName, String.class);
            } else {
                method = target.getClass().getDeclaredMethod(methodName);
            }
            // 放开方法的访问权限
            ReflectionUtils.makeAccessible(method);
            if (StrUtil.isNotBlank(methodParams)) {
                method.invoke(target, methodParams);
            } else {
                method.invoke(target);
            }
            watch.stop();
            logDTO.setExecStatus(Constants.COMMON_JUDGMENT.ONE);
        } catch (Exception e) {
            String message = ExceptionUtil.getMessage(e);
            log.error("{}, Spring调度任务执行异常: {}", TaskConstants.TASK_HEADER, message);
            logDTO.setExecStatus(Constants.COMMON_JUDGMENT.ZONE);
            logDTO.setExecName(e.getClass().getCanonicalName());
            logDTO.setExecMessage(message);
        }
        logDTO.setConsume(watch.getTotalTimeMillis());
        logDTO.setTraceId(MDC.get(Constants.TRACE_ID));
        publishTaskLogEvent(logDTO);
    }

    /**
     * 保存调度任务执行日志
     *
     * @param logDTO
     */
    private void publishTaskLogEvent(TaskLogDTO logDTO) {
        NetUtil.setRequestInheritable();
        ApplicationContext applicationContext = SpringUtil.getApplicationContext();
        applicationContext.publishEvent(new AsyncEvent(logDTO));
    }

}
