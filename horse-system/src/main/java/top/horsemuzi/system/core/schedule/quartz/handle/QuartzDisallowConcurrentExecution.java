package top.horsemuzi.system.core.schedule.quartz.handle;

import cn.hutool.core.util.IdUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.slf4j.MDC;
import org.springframework.context.ApplicationContext;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobDTO;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description 定时任务处理（禁止并发执行）
 * @date 2022/8/21 16:34
 */
@DisallowConcurrentExecution
public class QuartzDisallowConcurrentExecution extends QuartzBaseJob {

    public QuartzDisallowConcurrentExecution(ApplicationContext applicationContext) {
        super(applicationContext);
    }

    /**
     * 执行调度任务信息中的目标方法, 由字类重载
     *
     * @param context
     * @param quartzJobDTO
     */
    @Override
    protected void doExecute(JobExecutionContext context, QuartzJobDTO quartzJobDTO) throws Exception {
        // 设置日志链路id
        MDC.put(Constants.TRACE_ID, IdUtil.nanoId(10));
        QuartzJobInvokeHandle.invokeTargetMethod(quartzJobDTO);
    }
}
