package top.horsemuzi.system.core.schedule.task.handle;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ehcache.impl.internal.concurrent.ConcurrentHashMap;
import org.quartz.CronExpression;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.config.CronTask;
import org.springframework.stereotype.Component;
import reactor.core.Disposable;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.core.schedule.task.base.TaskConstants;

import java.util.Collection;
import java.util.Map;

/**
 * Spring的Cron任务注册器
 *
 * @author mabin
 * @date 2022/09/06 09:12
 **/
@RequiredArgsConstructor
@Slf4j
@Component
public class TaskRegister implements Disposable {

    private final TaskScheduler scheduler;
    private final Map<Runnable, ScheduledTask> taskMap = new ConcurrentHashMap<>(16);


    /**
     * 新增调度任务
     *
     * @param cronTask
     */
    public void addTask(CronTask cronTask) {
        if (ObjectUtil.isNotNull(cronTask)) {
            Runnable task = cronTask.getRunnable();
            if (taskMap.containsKey(task)) {
                removeTask(task);
            }
            taskMap.put(task, registerTask(cronTask));
        }
    }

    /**
     * 新增调度任务
     *
     * @param task
     * @param cron
     */
    public void addTask(Runnable task, String cron) {
        ValidatorUtil.isTrue(CronExpression.isValidExpression(cron), ResultCodeEnum.CRON_EXPRESSION_ERROR);
        addTask(new CronTask(task, cron));
    }

    /**
     * 注册任务
     *
     * @param cronTask
     * @return
     */
    private ScheduledTask registerTask(CronTask cronTask) {
        ScheduledTask scheduledTask = new ScheduledTask();
        scheduledTask.scheduledFuture = scheduler.schedule(cronTask.getRunnable(), cronTask.getTrigger());
        log.info("{} task registration succeeded", TaskConstants.TASK_HEADER);
        return scheduledTask;
    }

    /**
     * 移除任务
     *
     * @param task
     */
    public void removeTask(Runnable task) {
        ScheduledTask scheduledTask = taskMap.remove(task);
        if (ObjectUtil.isNotNull(scheduledTask)) {
            scheduledTask.cancel();
        }
        log.info("{} task removed succeeded", TaskConstants.TASK_HEADER);
    }


    /**
     * 销毁任务
     */
    @Override
    public void dispose() {
        Collection<ScheduledTask> tasks = taskMap.values();
        if (CollectionUtil.isNotEmpty(tasks)) {
            tasks.forEach(ScheduledTask::cancel);
        }
        taskMap.clear();
        log.info("{} task destroyed succeeded", TaskConstants.TASK_HEADER);
    }

}
