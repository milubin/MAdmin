package top.horsemuzi.system.core.office.excel.convert;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.metadata.property.ExcelContentProperty;
import com.alibaba.excel.util.DateUtils;
import com.alibaba.excel.util.StringUtils;

import java.util.Date;
import java.util.Objects;

/**
 * EasyExcel时间类型字段格式化
 *
 * @author mabin
 * @date 2022/06/15 16:55
 **/

public class DateTimeFormatConvert implements Converter<Date> {

    @Override
    public Class<?> supportJavaTypeKey() {
        return Date.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public Date convertToJavaData(ReadCellData<?> cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        if (Objects.isNull(cellData)) {
            return null;
        }
        String value = cellData.getStringValue();
        if (StringUtils.isBlank(value)) {
            return null;
        }
        return DateUtils.parseDate(value, DateUtils.DATE_FORMAT_19);
    }

    @Override
    public WriteCellData<?> convertToExcelData(Date value, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        if (Objects.isNull(value)) {
            return null;
        }
        return new WriteCellData<>(DateUtils.format(value, DateUtils.DATE_FORMAT_19));
    }

}
