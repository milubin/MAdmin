package top.horsemuzi.system.core.sign.aliyun.service;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.core.message.common.base.ding.DingBotParam;
import top.horsemuzi.core.message.common.model.DingBotContentModel;
import top.horsemuzi.core.message.enums.MessageTypeEnum;
import top.horsemuzi.system.config.properties.SystemProperties;
import top.horsemuzi.system.core.sign.aliyun.base.AliyunConstants;
import top.horsemuzi.system.service.MessageService;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 阿里云盘自动签到相关处理
 *
 * @author 马滨
 * @date 2023/05/07 16:25
 **/
@Slf4j
@Service
@RequiredArgsConstructor
public class AliyunService {

    @Value("${custom.sign.aliyun.refresh_token}")
    private String refreshToken;

    @Value("${custom.sign.aliyun.refresh_token_url}")
    private String refreshTokenUrl;

    @Value("${custom.sign.aliyun.sign_list_url}")
    private String signListUrl;

    @Value("${custom.sign.aliyun.sign_url}")
    private String signUrl;

    private final MessageService messageService;

    /**
     * 自动签到
     *
     * @param
     * @return void
     * @author mabin
     * @date 2023/5/18 9:32
     **/
    public void autoSign() {
        StopWatch watch = new StopWatch();
        watch.start();
        log.info("{} 开始进行自动签到处理", AliyunConstants.HEADER);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("startTime", DateUtil.format(new Date(), DatePattern.NORM_DATETIME_MS_FORMAT));
        getAccessToken(resultMap);
        String accessToken = Convert.toStr(resultMap.get(AliyunConstants.ACCESS_TOKEN));
        if (CharSequenceUtil.isNotBlank(accessToken)) {
            getSignList(accessToken, resultMap);
            sign(accessToken, resultMap);
        }
        watch.stop();
        resultMap.put("endTime", DateUtil.format(new Date(), DatePattern.NORM_DATETIME_MS_FORMAT));
        sendMessage(resultMap, watch.getTotalTimeMillis());
    }

    /**
     * 钉钉消息
     *
     * @param resultMap
     * @param totalTimeMillis
     * @return void
     * @author mabin
     * @date 2023/5/18 11:04
     **/
    private void sendMessage(Map<String, Object> resultMap, long totalTimeMillis) {
        DingBotContentModel model = new DingBotContentModel();
        model.setTitle("阿里云盘推送");
        model.setSendType(MessageTypeEnum.ACTION_CARD.getCode());
        model.setBtnOrientation(Constants.COMMON_CODE.STR_ONE);
        List<DingBotParam.ActionCardVO.BtnsVO> btnVOList = new ArrayList<>();
        DingBotParam.ActionCardVO.BtnsVO btnVO = new DingBotParam.ActionCardVO.BtnsVO();
        btnVO.setTitle(SystemProperties.name);
        btnVO.setActionURL("http://192.168.85.139:9527/admin/");
        btnVOList.add(btnVO);
        model.setBtns(JsonUtil.toJson(btnVOList));
        // 账号是否正常
        if (ObjectUtil.isNull(resultMap.get(AliyunConstants.ACCESS_TOKEN))) {
            model.setContent("![1](https://horse-blog.oss-cn-hangzhou.aliyuncs.com/202303/NMZlf0piTA.png) \n" +
                    "#### **阿里云盘** \n" +
                    "---  \n" +
                    "**Token状态：** Token已失效，请重新配置！");
        } else {
            String msgTemplate = "![1](https://horse-blog.oss-cn-hangzhou.aliyuncs.com/202303/NMZlf0piTA.png) \n" +
                    "#### **阿里云盘** \n" +
                    "---  \n" +
                    "**Token状态：** Token正常 \n\n" +
                    "**签到主题：** 《{} {}》 \n\n" +
                    "**签到天数：** {} 天 \n\n" +
                    "**签到奖励：** {}({}) \n\n" +
                    "**奖励详情：** {} \n\n" +
                    "**签到耗时：** 起始 {}-{}，共耗时 {}ms";
            model.setContent(StrUtil.format(msgTemplate, resultMap.get("title"), resultMap.get("subject"), resultMap.get("signInCount"), resultMap.get("name"),
                    resultMap.get("description"), resultMap.get("notice"), resultMap.get("startTime"), resultMap.get("endTime"), totalTimeMillis));
        }
        messageService.sendDingBotMessage(model, null);
    }

    /**
     * 签到处理
     *
     * @param accessToken
     * @param resultMap
     * @return void
     * @author mabin
     * @date 2023/5/18 10:38
     **/
    private void sign(String accessToken, Map<String, Object> resultMap) {
        try {
            TimeUnit.SECONDS.toMillis(2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject entries = new JSONObject();
        entries.set("signInDay", resultMap.get("signInCount"));
        String response = HttpRequest.post(signUrl)
                .header(Header.AUTHORIZATION.getValue(), accessToken)
                .body(JSONUtil.toJsonStr(entries))
                .execute().body();
        log.info("{} 签到奖励:{}", AliyunConstants.HEADER, response);
        JSONObject resObj = JSONUtil.parseObj(response);
        if (ObjectUtil.isNull(resObj.get("success")) || Boolean.FALSE.equals(resObj.getBool("success"))) {
            return;
        }
        JSONObject result = resObj.getJSONObject("result");
        resultMap.put("name", result.getStr("name"));
        resultMap.put("description", result.getStr("description"));
        resultMap.put("notice", result.getStr("notice"));
    }

    /**
     * 返回签到天数
     *
     * @param accessToken
     * @param resultMap
     * @author mabin
     * @date 2023/5/18 10:17
     **/
    private void getSignList(String accessToken, Map<String, Object> resultMap) {
        try {
            TimeUnit.SECONDS.toMillis(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject entries = new JSONObject();
        entries.set("_rx-s", "mobile");
        String response = HttpRequest.post(signListUrl)
                .body(JSONUtil.toJsonStr(entries))
                .header(Header.AUTHORIZATION.getValue(), accessToken)
                .execute().body();
        log.info("{} 获取到签到列表:{}", AliyunConstants.HEADER, response);
        JSONObject resObj = JSONUtil.parseObj(response);
        if (ObjectUtil.isNull(resObj.get("success")) || Boolean.FALSE.equals(resObj.getBool("success"))) {
            return;
        }
        JSONObject result = resObj.getJSONObject("result");
        resultMap.put("signInCount", result.getInt("signInCount"));
        resultMap.put("title", result.getStr("title"));
        resultMap.put("subject", CharSequenceUtil.subAfter(result.getStr("subject"), "：", true));
    }

    /**
     * 获取accessToken
     *
     * @param
     * @param resultMap
     * @return java.lang.String
     * @author mabin
     * @date 2023/5/18 10:12
     **/
    private void getAccessToken(Map<String, Object> resultMap) {
        JSONObject entries = new JSONObject();
        entries.set("grant_type", "refresh_token");
        entries.set("refresh_token", refreshToken);
        String response = HttpRequest.post(refreshTokenUrl)
                .body(JSONUtil.toJsonStr(entries))
                .execute().body();
        log.info("{} 获取刷新令牌:{}", AliyunConstants.HEADER, response);
        JSONObject resObj = JSONUtil.parseObj(response);
        String accessToken = resObj.getStr("access_token");
        String tokenType = resObj.getStr("token_type");
        if (CharSequenceUtil.isBlank(accessToken) || CharSequenceUtil.isBlank(tokenType)) {
            return;
        }
        resultMap.put(AliyunConstants.ACCESS_TOKEN, tokenType + " " + accessToken);
    }

}
