package top.horsemuzi.system.core.schedule.quartz.job;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import top.horsemuzi.system.core.sign.aliyun.service.AliyunService;

/**
 * 阿里云盘自动签到
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/05/18 11:24
 **/
@Component
public class AliyunSignQuartzJob {

    @Autowired
    private AliyunService aliyunService;

    public void sign() {
        aliyunService.autoSign();
    }

}
