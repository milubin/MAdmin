package top.horsemuzi.system.core.slack;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.Header;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.junit.rules.Stopwatch;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.http.HttpUtil;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Slack工作区服务
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/06/10 15:26
 **/
@Slf4j
@Service
@RequiredArgsConstructor
public class SlackService {

    @Value("${custom.slack.token}")
    private String token;

    @Value("${custom.slack.channel}")
    private String channel;

    @Value("${custom.slack.claude_id}")
    private String claudeId;

    @Value("${custom.slack.send_message_url}")
    private String sendMessageUrl;

    @Value("${custom.slack.reply_message_url}")
    private String replyMessageUrl;

    @Value("${custom.slack.each_count}")
    private Integer eachCount;

    @Value("${custom.slack.sleep_interval_count}")
    private Integer sleepIntervalCount;

    /**
     * Claude交流
     *
     * @param problem 相关问题字符串
     * @return java.util.Map<java.lang.String, java.lang.Object>
     * @author mabin
     * @date 2023/6/13 9:44
     **/
    public Map<String, Object> chatClaude(String problem) {
        StopWatch watch = new StopWatch();
        watch.start();
        Map<String, Object> chatMap = new LinkedHashMap<>(4);
        chatMap.put("problem", problem);
        try {
            if (StrUtil.isBlank(problem)) {
                return chatMap;
            }
            String sendResponse = sendMessage(problem);
            if (StrUtil.isBlank(sendResponse)) {
                chatMap.put("answer", "claude error");
                return chatMap;
            }
            JsonNode sendNode = JsonUtil.fromJson(sendResponse);
            if (Boolean.FALSE.equals(JsonUtil.getNodeBool(sendNode, "ok", Boolean.FALSE))) {
                chatMap.put("answer", JsonUtil.getNodeStr(sendNode, "error"));
                return chatMap;
            }
            TimeUnit.SECONDS.sleep(2);
            chatMap.put("answer", replyMessage(sendResponse));

            log.info("[SlackService.chatClaude] chatMap={}", JsonUtil.toJson(chatMap));
            return chatMap;
        } catch (Exception e) {
            log.error("[SlackService.chatClaude] error message={}", e.getMessage(), e);
            chatMap.put("answer", "claude error");
            return chatMap;
        }
    }

    /**
     * 发送消息
     *
     * @param problem
     * @return java.lang.String
     * @author mabin
     * @date 2023/6/13 10:06
     **/
    private String sendMessage(String problem) {
        Map<String, Object> headersMap = new HashMap<>(4);
        headersMap.put(Header.CONTENT_TYPE.getValue(), MediaType.APPLICATION_JSON_VALUE);
        headersMap.put(Header.AUTHORIZATION.getValue(), "Bearer " + token);
        Map<String, String> paramsMap = new HashMap<>(6);
        paramsMap.put("text", StrUtil.format("<@{}> {}", claudeId, problem));
        paramsMap.put("channel", channel);
        String response = HttpUtil.OK_HTTP.post(sendMessageUrl, JsonUtil.toJson(paramsMap), headersMap, null, Boolean.TRUE, Boolean.TRUE);
        log.info("[SlackService.sendMessage] response={}", response);
        return response;
    }

    /**
     * 解析回复
     *
     * @param response
     * @return java.lang.String
     * @author mabin
     * @date 2023/6/13 10:23
     **/
    private String replyMessage(String response) throws Exception {
        String ts = JsonUtil.getNodeStr(JsonUtil.fromJson(response), "ts");
        String reply = getReply(ts);
        JsonNode jsonNode = JsonUtil.fromJson(reply);
        if (Boolean.FALSE.equals(JsonUtil.getNodeBool(jsonNode, "ok"))) {
            return JsonUtil.getNodeStr(jsonNode, "error");
        }
        ArrayNode messageNode = JsonUtil.getArrayNode(reply, "messages");
        String replyText = null;
        if (Objects.nonNull(messageNode) && messageNode.size() >= 2) {
            replyText = new String(StrUtil.trim(JsonUtil.getNodeStr(JsonUtil.fromJson(JsonUtil.toJson(messageNode.get(1))), "text")).getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_8);
            log.info("[SlackService.replyMessage] replyText={}", replyText);
        }
        int replyCount = 1;
        while (StrUtil.isBlank(replyText) || StrUtil.endWith(replyText, "Typing…_")) {
            if (++replyCount > eachCount) {
                return replyText;
            }
            if (replyCount > sleepIntervalCount) {
                TimeUnit.SECONDS.sleep(5);
            } else {
                TimeUnit.SECONDS.sleep(2);
            }
            JsonNode replyNode = JsonUtil.fromJson(getReply(ts));
            if (Boolean.FALSE.equals(JsonUtil.getNodeBool(replyNode, "ok"))) {
                return JsonUtil.getNodeStr(replyNode, "error");
            }
            ArrayNode messageNode2 = JsonUtil.getArrayNode(replyNode, "messages");
            if (Objects.nonNull(messageNode2) && messageNode2.size() >= 2) {
                replyText = new String(StrUtil.trim(JsonUtil.getNodeStr(JsonUtil.fromJson(JsonUtil.toJson(messageNode2.get(1))), "text")).getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_8);
                log.info("[SlackService.replyMessage] replyText={}", replyText);
            }
        }
        log.info("[SlackService.replyMessage] claude final replyText={}", replyText);
        return replyText;
    }

    /**
     * 获取回复
     *
     * @param ts
     * @return java.lang.String
     * @author mabin
     * @date 2023/6/13 10:34
     **/
    private String getReply(String ts) {
        Map<String, Object> headersMap = new HashMap<>(4);
        headersMap.put(Header.CONTENT_TYPE.getValue(), MediaType.APPLICATION_JSON_VALUE);
        headersMap.put(Header.AUTHORIZATION.getValue(), "Bearer " + token);
        Map<String, Object> paramsMap = new HashMap<>(6);
        paramsMap.put("channel", channel);
        paramsMap.put("ts", ts);
        paramsMap.put("limit", 2);
        String response = HttpUtil.OK_HTTP.get(replyMessageUrl, headersMap, paramsMap);
        log.info("[SlackService.getReply] response={}", response);
        return response;
    }

}
