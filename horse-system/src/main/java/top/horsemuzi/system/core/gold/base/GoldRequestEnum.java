package top.horsemuzi.system.core.gold.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import top.horsemuzi.common.constants.Constants;

/**
 * 高德开放平台
 *
 * @author mabin
 * @date 2022/11/06 10:47
 **/
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class GoldRequestEnum {

    public static final String GOLD_HEADER = "【高德开放平台】";

    /**
     * 高德开放平台API枚举处理
     */
    @Getter
    @ToString
    @AllArgsConstructor
    public enum APIEnum {

        /**
         * 行政区划查询
         */
        DISTRICT(Constants.METHOD.GET, "https://restapi.amap.com/v3/config/district", new District()),

        /**
         * 天气查询
         */
        WEATHER_INFO(Constants.METHOD.GET, "https://restapi.amap.com/v3/weather/weatherInfo", new WeatherInfo()),

        /**
         * IP定位
         */
        IP(Constants.METHOD.GET, "https://restapi.amap.com/v3/ip", new Ip());

        /**
         * 请求方式：GET、POST
         */
        private final String method;

        /**
         * 请求URL
         */
        private final String url;

        /**
         * 请求参数
         */
        private final Param param;

    }

    /**
     * 请求参数
     */
    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Param {

        /**
         * 请求服务权限标识
         */
        private String key;
    }

    /**
     * 行政区域查询
     */
    @Data
    @NoArgsConstructor
    @EqualsAndHashCode(callSuper = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class District extends Param {

        /**
         * 含义：查询关键字
         * 规则：只支持单个关键词语搜索关键词支持：行政区名称、citycode、adcode
         * 例如，在subdistrict=2，搜索省份（例如山东），能够显示市（例如济南），区（例如历下区）
         */
        private String keywords;

        /**
         * 规则：设置显示下级行政区级数（行政区级别包括：国家、省/直辖市、市、区/县、乡镇/街道多级数据）
         * 可选值：0、1、2、3等数字，并以此类推
         * 0：不返回下级行政区；
         * 1：返回下一级行政区；
         * 2：返回下两级行政区；
         * 3：返回下三级行政区；
         * 需要在此特殊说明，目前部分城市和省直辖县因为没有区县的概念，故在市级下方直接显示街道。
         * 例如：广东-东莞、海南-文昌市
         */
        private String subdistrict = "1";

        /**
         * 需要第几页数据
         * 最外层的districts最多会返回20个数据，若超过限制，请用page请求下一页数据。
         * 例如page=2；page=3。默认page=1
         */
        private String page = "1";

        /**
         * 最外层返回数据个数
         */
        private String offset = "20";

        /**
         * 返回结果控制
         * 此项控制行政区信息中返回行政区边界坐标点； 可选值：base、all;
         * base:不返回行政区边界坐标点；
         * all:只返回当前查询district的边界值，不返回子节点的边界值；
         * 目前不能返回乡镇/街道级别的边界值
         */
        private String extensions = "base";

        /**
         * 根据区划过滤
         * 按照指定行政区划进行过滤，填入后则只返回该省/直辖市信息
         * 需填入adcode，为了保证数据的正确，强烈建议填入此参数
         */
        private String filter;

        /**
         * 回调函数
         * callback值是用户定义的函数名称，此参数只在output=JSON时有效
         */
        private String callback;

        /**
         * 可选值：JSON，XML
         */
        private String output = "JSON";

    }


    @Data
    @NoArgsConstructor
    @EqualsAndHashCode(callSuper = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class WeatherInfo extends Param {

        /**
         * 城市编码
         * 输入城市的adcode
         */
        private String city;

        /**
         * 气象类型
         * 可选值：base/all
         * base:返回实况天气
         * all:返回预报天气
         */
        private String extensions;

        /**
         * 可选值：JSON，XML
         */
        private String output = "JSON";
    }

    /**
     * IP定位
     */
    @Data
    @NoArgsConstructor
    @EqualsAndHashCode(callSuper = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Ip extends Param {

        /**
         * ip地址
         * 需要搜索的IP地址（仅支持国内）
         * 若用户不填写IP，则取客户http之中的请求来进行定位
         */
        private String ip;
    }

}
