package top.horsemuzi.system.core.schedule.quartz.handle;

import cn.hutool.core.annotation.AnnotationUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobDTO;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description Quartz调度反射实现
 * @date 2022/8/21 17:23
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class QuartzJobInvokeHandle {

    /**
     * 执行目标方法
     *
     * @param job
     */
    public static void invokeTargetMethod(QuartzJobDTO job) throws Exception {
        String targetMethod = job.getInvokeTarget();
        log.info("quartz job invoke target method: {}", targetMethod);
        Object clazz;
        String className = StrUtil.subBefore(StrUtil.subBefore(targetMethod, "(", false), StrUtil.DOT, true);
        Class<?> aClass = Class.forName(className);
        // 如果invoke反射执行的任务实例被Spring管理, 获取到的对象需要手动重新托管到Spring容器中
        if (AnnotationUtil.hasAnnotation(aClass, Component.class) || AnnotationUtil.hasAnnotation(aClass, Service.class)
                || AnnotationUtil.hasAnnotation(aClass, Controller.class)) {
            clazz = SpringUtil.getBean(aClass);
            SpringUtil.getApplicationContext().getAutowireCapableBeanFactory().autowireBean(clazz);
        } else {
            clazz = aClass.newInstance();
        }
        String methodName = getMethodName(targetMethod);
        List<Object[]> methodParams = getMethodParams(targetMethod);
        if (CollectionUtil.isNotEmpty(methodParams)) {
            Method method = clazz.getClass().getDeclaredMethod(methodName, getMethodParamsType(methodParams));
            method.invoke(clazz, getMethodParamsValue(methodParams));
        } else {
            Method method = clazz.getClass().getDeclaredMethod(methodName);
            method.invoke(clazz);
        }
    }

    /**
     * 获取目标方法名
     *
     * @param targetMethod
     * @return
     */
    private static String getMethodName(String targetMethod) {
        return StrUtil.subAfter(StrUtil.subBefore(targetMethod, "(", false), StrUtil.DOT, true);
    }

    /**
     * 获取method方法参数相关列表
     *
     * @param invokeTarget 目标字符串
     * @return method方法相关参数列表
     */
    private static List<Object[]> getMethodParams(String invokeTarget) {
        String paramsStr = StrUtil.subBetween(invokeTarget, "(", ")");
        if (StrUtil.isBlank(paramsStr)) {
            return Collections.emptyList();
        }
        List<String> params = StrUtil.split(paramsStr, StrUtil.COMMA);
        List<Object[]> classList = new LinkedList<>();
        for (String param : params) {
            param = StrUtil.trimToEmpty(param);
            if (StrUtil.contains(param, "'")) {
                classList.add(new Object[]{StrUtil.replace(param, "'", StrUtil.EMPTY), String.class});
            }
            // boolean布尔类型，等于true、false、Boolean.TRUE、Boolean.FALSE
            else if (StrUtil.equalsAnyIgnoreCase(param, "true", "false", "Boolean.TRUE", "Boolean.FALSE")) {
                classList.add(new Object[]{Convert.toBool(param), Boolean.class});
            }
            // long长整形，包含L
            else if (StrUtil.containsIgnoreCase(param, "L")) {
                classList.add(new Object[]{Convert.toLong(StrUtil.replaceIgnoreCase(param, "L", StrUtil.EMPTY)),
                        Long.class});
            }
            // double浮点类型，包含D、d
            else if (StrUtil.containsIgnoreCase(param, "D")) {
                classList.add(new Object[]{Convert.toDouble(StrUtil.replaceIgnoreCase(param, "D", StrUtil.EMPTY)),
                        Double.class});
            }
            // 其他类型归类为整形
            else {
                classList.add(new Object[]{Convert.toInt(param), Integer.class});
            }
        }
        return classList;
    }

    private static Class<?>[] getMethodParamsType(List<Object[]> methodParams) {
        Class<?>[] classArray = new Class<?>[methodParams.size()];
        int index = 0;
        for (Object[] os : methodParams) {
            classArray[index++] = (Class<?>) os[1];
        }
        return classArray;
    }

    /**
     * 获取参数值
     *
     * @param methodParams 参数相关列表
     * @return 参数值列表
     */
    private static Object[] getMethodParamsValue(List<Object[]> methodParams) {
        Object[] classArray = new Object[methodParams.size()];
        int index = 0;
        for (Object[] os : methodParams) {
            classArray[index++] = os[0];
        }
        return classArray;
    }

}
