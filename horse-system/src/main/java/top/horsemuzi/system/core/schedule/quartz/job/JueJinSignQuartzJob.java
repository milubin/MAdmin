package top.horsemuzi.system.core.schedule.quartz.job;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import top.horsemuzi.system.core.sign.juejin.service.JueJinService;

/**
 * 掘金平台自动签到任务调度
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/03/09 15:04
 **/
@Slf4j
@Component
public class JueJinSignQuartzJob {

    @Autowired
    private JueJinService jueJinService;

    public void sign() {
        jueJinService.autoSign();
    }

}
