package top.horsemuzi.system.core.sensitive;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * 敏感词过滤处理
 *
 * @author mabin
 * @date 2022/11/04 10:13
 **/
@Slf4j
@Component
public class SensitiveService implements InitializingBean {

    /**
     * 根节点
     */
    private TrieNode rootNode = new TrieNode();

    /**
     * 初始化敏感词词库
     */
    @Override
    public void afterPropertiesSet() {
        StopWatch watch = new StopWatch();
        watch.start();
        try {
            log.info("init sensitive file start...");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("sensitive-words.txt")));
            String word;
            while ((word = bufferedReader.readLine()) != null) {
                addKeyWord(word);
            }
            watch.stop();
            log.info("init sensitive file end, custom {}ms", watch.getTotalTimeMillis());
        } catch (Exception e) {
            watch.stop();
            log.info("init sensitive file exception", e);
        }
    }


    /**
     * 将一个敏感词添加到前缀树节点中
     *
     * @param keyWord
     */
    private void addKeyWord(String keyWord) {
        TrieNode tempNode = rootNode;
        int length = keyWord.length();
        for (int i = 0; i < length; i++) {
            char ch = keyWord.charAt(i);
            TrieNode subNode = tempNode.getSubNode(ch);
            if (subNode == null) {
                // 初始化节点: 设置子节点
                subNode = new TrieNode();
                tempNode.addSubNode(ch, subNode);
            }
            // 设置subNode指向下一个子节点,进入下一轮循环
            tempNode = subNode;
            // 设置结束标识
            if (i == length - 1) {
                tempNode.setKeyWordEnd(true);
            }
        }
    }

    /**
     * 判断是否为符号
     *
     * @param c
     * @return
     */
    private boolean isSymbol(Character c) {
        // 0x2E80~0x9FFF 是东亚文字范围
        return !CharUtils.isAsciiAlphanumeric(c) && (c < 0x2E80 || c > 0x9FFF);
    }

    /**
     * 过滤敏感词
     *
     * @param text
     * @return
     */
    public String filter(String text) {
        if (StringUtils.isBlank(text)) {
            return null;
        }
        //指针1
        TrieNode tempNode = rootNode;
        //指针2
        int begin = 0;
        //指针3
        int position = 0;
        //结果
        StringBuilder sb = new StringBuilder();
        while (position < text.length()) {
            char c = text.charAt(position);
            /*
            	跳过符号
            	情况一：符号在敏感词前面，将符号写入结果，如 ☆敏感词
            	情况二：符号在敏感词中间，则将符号与敏感词一起替换，如敏☆感☆词
            */
            if (isSymbol(c)) {
                //若指针1处于根节点，对应情况一，将符号计入结果，让指针2向下走一步
                if (tempNode == rootNode) {
                    sb.append(c);
                    begin++;
                }
                //无论符号在开头还是敏感词中间，指针3都向下走一步
                position++;
                continue;
            }
            //检查下级节点
            tempNode = tempNode.getSubNode(c);
            if (tempNode == null) {
                //以begin开头的的字符串不是敏感词
                sb.append(text.charAt(begin));
                //指针2和指针3共同指向指针2的下一个位置
                position = ++begin;
                //指针1重新指向根节点
                tempNode = rootNode;
            } else if (tempNode.isKeyWordEnd()) {
                //发现敏感词，将begin~position字符串替换: ***
                sb.append("*");
                //进入下一个位置
                begin = ++position;
                //指针1重新指向根节点
                tempNode = rootNode;
            } else {
                //检查下一个字符
                position++;
            }
        }
        //将最后一批字符计入结果
        sb.append(text.substring(begin));
        return sb.toString();
    }

}


