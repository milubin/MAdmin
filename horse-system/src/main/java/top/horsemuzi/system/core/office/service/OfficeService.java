package top.horsemuzi.system.core.office.service;

import cn.afterturn.easypoi.csv.CsvExportUtil;
import cn.afterturn.easypoi.csv.CsvImportUtil;
import cn.afterturn.easypoi.csv.entity.CsvExportParams;
import cn.afterturn.easypoi.csv.entity.CsvImportParams;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.excel.write.builder.ExcelWriterBuilder;
import com.alibaba.excel.write.builder.ExcelWriterSheetBuilder;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.metadata.fill.FillConfig;
import com.alibaba.excel.write.style.column.LongestMatchColumnWidthStyleStrategy;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StopWatch;
import top.horsemuzi.common.util.CommonCodeUtil;
import top.horsemuzi.system.core.office.excel.convert.BigNumberConvert;
import top.horsemuzi.system.core.office.excel.listener.DefaultExcelListener;
import top.horsemuzi.system.core.office.excel.listener.ExcelListener;
import top.horsemuzi.system.core.office.excel.listener.ExcelResult;
import wiki.xsx.core.pdf.doc.XEasyPdfDocument;
import wiki.xsx.core.pdf.handler.XEasyPdfHandler;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;

/**
 * Office相关数据处理
 *
 * @author mabin
 * @date 2022/06/15 09:49
 **/
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class OfficeService {

    /**
     * 基于EasyExcel的excel的相关处理 (本系统支持自定义导入/出、模板导出)
     */
    public static class EXCEL {

        /**
         * 同步导入(适用于小数据量)
         *
         * @param file
         * @param clazz
         * @return java.util.List<T>
         * @author mabin
         * @date 2023/3/11 14:58
         **/
        public static <T> List<T> importExcel(File file, Class<T> clazz) throws Exception {
            return importExcel(new FileInputStream(file), clazz);
        }

        /**
         * 同步导入(适用于小数据量)
         *
         * @param inputStream
         * @param clazz
         * @param <T>
         * @return
         */
        public static <T> List<T> importExcel(InputStream inputStream, Class<T> clazz) {
            return EasyExcel.read(inputStream).head(clazz).autoCloseStream(Boolean.FALSE).sheet().doReadSync();
        }

        /**
         * 使用默认校验监听器 异步导入 同步返回
         *
         * @param file
         * @param clazz
         * @param isValidate
         * @return top.horsemuzi.system.core.office.excel.listener.ExcelResult<T>
         * @author mabin
         * @date 2023/3/11 15:02
         **/
        public static <T> ExcelResult<T> importExcel(File file, Class<T> clazz, Boolean isValidate) throws Exception {
            return importExcel(new FileInputStream(file), clazz, isValidate);
        }

        /**
         * 使用默认校验监听器 异步导入 同步返回
         *
         * @param inputStream
         * @param clazz
         * @param isValidate
         * @param <T>
         * @return
         */
        public static <T> ExcelResult<T> importExcel(InputStream inputStream, Class<T> clazz, Boolean isValidate) {
            DefaultExcelListener<T> listener = new DefaultExcelListener<>(isValidate);
            EasyExcel.read(inputStream, clazz, listener).sheet().doRead();
            return listener.getExcelResult();
        }

        /**
         * 使用自定义监听器 异步导入 自定义返回
         *
         * @param file
         * @param clazz
         * @param listener
         * @return top.horsemuzi.system.core.office.excel.listener.ExcelResult<T>
         * @author mabin
         * @date 2023/3/11 15:03
         **/
        public static <T> ExcelResult<T> importExcel(File file, Class<T> clazz, ExcelListener<T> listener) throws Exception {
            return importExcel(new FileInputStream(file), clazz, listener);
        }

        /**
         * 使用自定义监听器 异步导入 自定义返回
         *
         * @param inputStream
         * @param clazz
         * @param listener
         * @param <T>
         * @return
         */
        public static <T> ExcelResult<T> importExcel(InputStream inputStream, Class<T> clazz, ExcelListener<T> listener) {
            EasyExcel.read(inputStream, clazz, listener).sheet().doRead();
            return listener.getExcelResult();
        }

        public static void export(List<?> data, String fileName, String sheetName, ExcelTypeEnum type, HttpServletResponse response) throws Exception {
            export(data, fileName, sheetName, type, null, response, null);
        }

        public static void export(List<?> data, String fileName, String sheetName, ExcelTypeEnum type, Set<String> excludeColumnFiledNames, HttpServletResponse response) throws Exception {
            export(data, fileName, sheetName, type, excludeColumnFiledNames, response, null);
        }

        public static void export(List<?> data, String fileName, String sheetName, ExcelTypeEnum type, String password, HttpServletResponse response) throws Exception {
            export(data, fileName, sheetName, type, null, response, password);
        }

        public static void export(List<?> data, String fileName, String sheetName, ExcelTypeEnum type, Set<String> excludeColumnFiledNames, String password, HttpServletResponse response) throws Exception {
            export(data, fileName, sheetName, type, excludeColumnFiledNames, response, password);
        }

        /**
         * 写入执行文件流(下载)
         *
         * @param data                    写入数据
         * @param fileName                文件名称(全路径, 但不包含文件类型后缀名)
         * @param sheetName               工作簿名
         * @param type                    文件类型(后缀名类型)
         * @param excludeColumnFiledNames 指定不需要导出的列名集合(字段名)
         * @param response
         * @param password                密码(注意: 文件加密时, 需要将全部数据读取到内存中, 非常占用内存, 谨慎使用)
         * @throws Exception
         */
        public static void export(List<?> data,
                                  String fileName,
                                  String sheetName,
                                  ExcelTypeEnum type,
                                  Set<String> excludeColumnFiledNames,
                                  HttpServletResponse response,
                                  String password) throws Exception {
            StopWatch watch = new StopWatch();
            watch.start();
            if (CollectionUtils.isEmpty(data) || StringUtils.isAnyBlank(fileName, sheetName)) {
                return;
            }
            CommonCodeUtil.generateAttachmentResponseHeader(response, fileName + type.getValue());
            ExcelWriterBuilder excelWriterBuilder = EasyExcel.write(response.getOutputStream(), data.get(0).getClass())
                    .excelType(type)
                    .autoCloseStream(Boolean.FALSE)
                    .registerWriteHandler(new LongestMatchColumnWidthStyleStrategy())
                    .registerConverter(new BigNumberConvert());
            if (StrUtil.isNotBlank(password)) {
                excelWriterBuilder.password(password);
            }
            ExcelWriterSheetBuilder excelWriterSheetBuilder = excelWriterBuilder.sheet(sheetName);
            if (!CollectionUtils.isEmpty(excludeColumnFiledNames)) {
                excelWriterSheetBuilder.excludeColumnFieldNames(excludeColumnFiledNames);
            }
            excelWriterSheetBuilder.doWrite(data);
            watch.stop();
            log.info("EasyExcel export file consuming: {}ms", watch.getTotalTimeMillis());
        }

        /**
         * 写入具体文件(非下载, 直接写入本地文件)
         *
         * @param data
         * @param fileName
         * @param sheetName
         * @param type
         * @param excludeColumnFiledNames
         * @param file
         * @param password
         * @return void
         * @author mabin
         * @date 2023/3/11 15:08
         **/
        public static void export(List<?> data,
                                  String fileName,
                                  String sheetName,
                                  ExcelTypeEnum type,
                                  Set<String> excludeColumnFiledNames,
                                  File file,
                                  String password) {
            StopWatch watch = new StopWatch();
            watch.start();
            if (CollectionUtils.isEmpty(data) || StringUtils.isAnyBlank(fileName, sheetName)) {
                return;
            }
            ExcelWriterBuilder excelWriterBuilder = EasyExcel.write(file, data.get(0).getClass())
                    .excelType(type)
                    .autoCloseStream(Boolean.FALSE)
                    .registerWriteHandler(new LongestMatchColumnWidthStyleStrategy())
                    .registerConverter(new BigNumberConvert());
            if (StrUtil.isNotBlank(password)) {
                excelWriterBuilder.password(password);
            }
            ExcelWriterSheetBuilder excelWriterSheetBuilder = excelWriterBuilder.sheet(sheetName);
            if (!CollectionUtils.isEmpty(excludeColumnFiledNames)) {
                excelWriterSheetBuilder.excludeColumnFieldNames(excludeColumnFiledNames);
            }
            excelWriterSheetBuilder.doWrite(data);
            watch.stop();
            log.info("EasyExcel export file consuming: {}ms", watch.getTotalTimeMillis());
        }

        /**
         * 根据本地模板文件导出(下载)
         *
         * @param data         写入数据
         * @param fileName     文件名称(全路径, 但不包含文件类型后缀名)
         * @param sheetName    工作簿名
         * @param type         文件类型(后缀名类型)
         * @param fillingMap   填充之定义单元格数据
         * @param templatePath 模板文件resource路径(模板文件必须在resource资源目录下)
         * @param response
         * @throws Exception
         */
        public static void export(List<?> data,
                                  String fileName,
                                  String sheetName,
                                  ExcelTypeEnum type,
                                  Map<String, Object> fillingMap,
                                  String templatePath,
                                  HttpServletResponse response) throws Exception {
            if (StringUtils.isBlank(templatePath)) {
                return;
            }
            export(data, fileName, sheetName, type, fillingMap, new ClassPathResource(templatePath).getInputStream(), response);
        }

        /**
         * 根据模板文件流导出(下载)
         *
         * @param data                写入数据
         * @param fileName            文件名称(全路径, 但不包含文件类型后缀名)
         * @param sheetName           工作簿名
         * @param type                文件类型(后缀名类型)
         * @param fillingMap          填充之定义单元格数据
         * @param templateInputStream 模板文件输入流
         * @param response
         * @throws Exception
         */
        public static void export(List<?> data, String fileName, String sheetName, ExcelTypeEnum type, Map<String, Object> fillingMap, InputStream templateInputStream, HttpServletResponse response) throws Exception {
            StopWatch watch = new StopWatch();
            watch.start();
            if (CollectionUtils.isEmpty(data) || StringUtils.isAnyBlank(fileName, sheetName) || Objects.isNull(templateInputStream)) {
                return;
            }
            CommonCodeUtil.generateAttachmentResponseHeader(response, fileName + type.getValue());
            // 指定导出文件和模板文件
            ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream(), data.get(0).getClass())
                    .excelType(type)
                    .autoCloseStream(Boolean.FALSE)
                    .registerConverter(new BigNumberConvert())
                    .withTemplate(templateInputStream)
                    .build();
            WriteSheet writeSheet = EasyExcel.writerSheet(sheetName).build();
            FillConfig config = FillConfig.builder().forceNewRow(Boolean.TRUE).build();
            // 填充遍历数据
            excelWriter.fill(data, config, writeSheet);
            // 填充自定义单元格数据
            if (!CollectionUtils.isEmpty(fillingMap)) {
                excelWriter.fill(fillingMap, writeSheet);
            }
            excelWriter.finish();
            watch.stop();
            log.info("EasyExcel export file consuming: {}ms", watch.getTotalTimeMillis());
        }

        /**
         * 解析扩展表达式 (0=女,1=男,2=外星人) 需严格遵守书写格式
         *
         * @param convertExp  扩展表达式
         * @param sourceValue 解析寻找标识值
         * @param reverse     是否反向解析, 导出=>正向(false), 导入=>反向(true)
         * @return
         */
        public static String convertExp(String convertExp, String sourceValue, Boolean reverse) {
            String parseValue = StrUtil.EMPTY;
            if (StrUtil.isAllNotBlank(convertExp, sourceValue)) {
                List<String> convertList = new ArrayList<>(StrUtil.split(convertExp, StrUtil.COMMA));
                for (String convertItem : convertList) {
                    List<String> convertItemList = StrUtil.split(convertItem, "=");
                    if (reverse) {
                        if (StrUtil.equals(convertItemList.get(1), sourceValue)) {
                            parseValue = convertItemList.get(0);
                            break;
                        }
                    } else {
                        if (StrUtil.equals(convertItemList.get(0), sourceValue)) {
                            parseValue = convertItemList.get(1);
                            break;
                        }
                    }
                }
            }
            return parseValue;
        }

    }

    /**
     * 基于EasyPoi的excel相关处理 (复杂表头excel导出)
     */
    public static class EXCEL_POI {

        /**
         * excel导出
         *
         * @param response   输出流
         * @param clazz      Class实例
         * @param collection 数据集合
         * @param fileName   文件名
         * @param title      excel文件合并第一行显示的标题
         * @param sheetName  工作簿名
         * @param type       文件类型
         * @throws Exception
         */
        public static void export(HttpServletResponse response,
                                  Class<?> clazz,
                                  Collection<?> collection,
                                  String fileName,
                                  String title,
                                  String sheetName,
                                  ExcelType type
        ) throws Exception {
            ExportParams exportParams = new ExportParams();
            if (StrUtil.isNotBlank(title)) {
                exportParams.setTitle(title);
            }
            if (StrUtil.isNotBlank(sheetName)) {
                exportParams.setSheetName(sheetName);
            }
            exportParams.setType(ObjectUtil.isNotNull(type) ? type : ExcelType.XSSF);
            export(response, clazz, collection, fileName, exportParams);
        }

        /**
         * excel导出
         *
         * @param response
         * @param clazz
         * @param collection
         * @param fileName
         * @param exportParams
         * @throws Exception
         */
        public static void export(HttpServletResponse response,
                                  Class<?> clazz,
                                  Collection<?> collection,
                                  String fileName,
                                  ExportParams exportParams
        ) throws Exception {
            StopWatch watch = new StopWatch();
            watch.start();
            Workbook workbook = ExcelExportUtil.exportExcel(exportParams, clazz, collection);
            CommonCodeUtil.generateAttachmentResponseHeader(response, fileName);
            ServletOutputStream outputStream = response.getOutputStream();
            workbook.write(outputStream);
            outputStream.flush();
            watch.stop();
            log.info("ExcelPoi export file consuming: {}ms", watch.getTotalTimeMillis());
        }

    }

    /**
     * 基于EasyPoi的csv的相关处理
     */
    public static class CSV {

        /**
         * CSV文件在线导出 (默认GBK编码)
         *
         * @param response
         * @param clazz
         * @param collection
         * @param fileName
         * @throws Exception
         */
        public static void export(HttpServletResponse response, Class<?> clazz, Collection<?> collection, String fileName) throws Exception {
            CommonCodeUtil.generateAttachmentResponseHeader(response, fileName);
            CsvExportUtil.exportCsv(new CsvExportParams(CsvExportParams.GBK), clazz, collection, response.getOutputStream());
        }

        /**
         * CSV文件在线导出 (自定义文件编码格式)
         *
         * @param response
         * @param clazz
         * @param collection
         * @param fileName
         * @param encoding
         * @throws Exception
         */
        public static void export(HttpServletResponse response, Class<?> clazz, Collection<?> collection, String fileName, String encoding) throws Exception {
            CommonCodeUtil.generateAttachmentResponseHeader(response, fileName);
            CsvExportUtil.exportCsv(new CsvExportParams(encoding), clazz, collection, response.getOutputStream());
        }

        /**
         * CSV文件在线导入 (默认GBK编码)
         *
         * @param inputStream
         * @param clazz
         * @param <T>
         * @return
         */
        public static <T> List<T> importCsv(InputStream inputStream, Class<T> clazz) {
            return CsvImportUtil.importCsv(inputStream, clazz, new CsvImportParams(CsvImportParams.GBK));
        }

        /**
         * CSV文件在线导入 (自定义文件编码格式, 文件编码格式不一致, 无法读取数据)
         *
         * @param inputStream
         * @param clazz
         * @param encoding
         * @param <T>
         * @return
         */
        public static <T> List<T> importCsv(InputStream inputStream, Class<T> clazz, String encoding) {
            return CsvImportUtil.importCsv(inputStream, clazz, new CsvImportParams(encoding));
        }

    }

    /**
     * 基于x-EasyPdf对pdf的相关处理 (本系统支持自定义导出、模板导出)
     */
    public static class PDF {

        public static void export() {
            String path = "E:\\horse_project\\x-easypdf.pdf";
            XEasyPdfDocument document = XEasyPdfHandler.Document.build(XEasyPdfHandler.Page.build(XEasyPdfHandler.Text.build("使用x-easypdf导出PDF文件测试")));
            document.setGlobalWatermark(XEasyPdfHandler.Watermark.build("Horse Blog V3")).setVersion(1.0f).save(path).close();
        }

    }

    /**
     * word相关处理 (本系统仅需要模板导出功能即可, word文档导出不太常用)
     */
    public static class WORD {

    }


    public static void main(String[] args) {
        PDF.export();
    }


}
