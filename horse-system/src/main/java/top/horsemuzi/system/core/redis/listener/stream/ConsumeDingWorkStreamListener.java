package top.horsemuzi.system.core.redis.listener.stream;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiGettokenRequest;
import com.dingtalk.api.response.OapiGettokenResponse;
import com.taobao.api.ApiException;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.stream.MapRecord;
import org.springframework.data.redis.stream.StreamListener;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.MessageStatusEnum;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.ExceptionUtil;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.core.message.common.MessageConstants;
import top.horsemuzi.core.message.common.MessageInfo;
import top.horsemuzi.core.message.common.channel.DingWorkChannelConfig;
import top.horsemuzi.core.message.common.model.DingWorkContentModel;
import top.horsemuzi.core.message.enums.SendEnum;
import top.horsemuzi.system.core.redis.base.RedisConstants;
import top.horsemuzi.system.core.redis.service.RedisService;
import top.horsemuzi.system.manager.admin.MessageManager;
import top.horsemuzi.system.pojo.dto.message.MessageChannelDTO;
import top.horsemuzi.system.pojo.dto.message.MessageDTO;

import java.util.Collections;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;

/**
 * 钉钉工作消息Stream流消费者监听
 *
 * @author mabin
 * @date 2022/09/21 17:42
 **/
@Slf4j
@Component
@RequiredArgsConstructor
public class ConsumeDingWorkStreamListener extends AbstractConsumeStreamListener {

    private static final String DING_WORK_STREAM_CONSUMER = "【钉钉工作通知stream消费监听】";

    private final RedisService redisService;
    private final MessageManager messageManager;

    @SneakyThrows
    @Override
    public void onMessage(MapRecord<String, String, String> message) {
        log.info("{} 消费者监听消息内容 message={}", DING_WORK_STREAM_CONSUMER, message);
        // 参数校验
        String recordId = message.getId().toString();
        MessageDTO messageDTO = messageManager.getMessage(recordId);
        ValidatorUtil.notEmpty(messageDTO, ResultCodeEnum.MESSAGE_NOT_EXIST_ERROR);
        ValidatorUtil.notEmpty(messageDTO.getChannel(), ResultCodeEnum.MESSAGE_CHANNEL_NOT_EXIST_ERROR);
        log.info("{} 消息 recordId={} 的库信息 messageDTO={}", DING_WORK_STREAM_CONSUMER, message.getId(), JsonUtil.toJson(messageDTO));

        // 组装并发送消息
        if (MapUtil.isNotEmpty(message.getValue())) {
            // 获取消息内容, 默认Map结构中的key为对应Stream的字符串名称
            String content = message.getValue().get(RedisConstants.STREAM.DING_WORK.STREAM);
            if (StrUtil.isNotBlank(content)) {
                MessageInfo messageInfo = new MessageInfo();
                // 设置消息接收者
                String receiver = messageDTO.getReceiver();
                if (!StrUtil.equals(MessageConstants.SEND_ALL, receiver)) {
                    messageInfo.setReceiver(new HashSet<>(StrUtil.split(receiver, StrUtil.COMMA)));
                } else {
                    messageInfo.setReceiver(Collections.singleton(receiver));
                }
                SendEnum sendEnum = SendEnum.getSendEnum(messageDTO.getChannel());
                ValidatorUtil.notEmpty(sendEnum, ResultCodeEnum.MESSAGE_HANDLER_NOT_EXIST_ERROR);
                MessageChannelDTO messageChannelDTO = messageManager.getMessageChannel(messageDTO.getChannel());
                ValidatorUtil.notEmpty(messageChannelDTO, ResultCodeEnum.MESSAGE_CHANNEL_NOT_EXIST_ERROR);
                DingWorkChannelConfig dingWorkChannelConfig = (DingWorkChannelConfig) JsonUtil.fromJson(messageChannelDTO.getConfig(), sendEnum.getChannelConfig().getClass());
                DingWorkContentModel dingWorkContentModel = (DingWorkContentModel) JsonUtil.fromJson(content, sendEnum.getContentModel().getClass());
                ValidatorUtil.allNotEmpty(dingWorkChannelConfig, dingWorkContentModel, ResultCodeEnum.MESSAGE_PARAMS_ERROR);
                // 设置token信息
                dingWorkContentModel.setToken(getDingWorkToken(dingWorkChannelConfig));
                messageInfo.setChannelConfig(dingWorkChannelConfig);
                messageInfo.setContentModel(dingWorkContentModel);
                messageDTO.setMessageInfo(JsonUtil.toJson(messageInfo));
                if (sendEnum.getSendHandler().send(messageInfo)) {
                    log.info("{} 消息 recordId={} 消费成功", DING_WORK_STREAM_CONSUMER, recordId);
                    messageDTO.setStatus(MessageStatusEnum.SUCCESS.getCode());
                    messageDTO.setRemark(MessageStatusEnum.SUCCESS.getText());
                    Long ackCount = redisService.acknowledge(message.getStream(), messageDTO.getStreamGroup(), recordId);
                    if (ackCount > Constants.COMMON_CODE.LONG_ZONE) {
                        log.info("{} 消息 recordId={} 消费且ACK应答成功", DING_WORK_STREAM_CONSUMER, recordId);
                        messageDTO.setStatus(MessageStatusEnum.ACK_SUCCESS.getCode());
                        messageDTO.setRemark(MessageStatusEnum.ACK_SUCCESS.getText());
                    }
                }
                messageManager.updateMessage(messageDTO);
            }
        }
    }

    /**
     * 获取token信息
     *
     * @param dingWorkChannelConfig
     * @return
     */
    private String getDingWorkToken(DingWorkChannelConfig dingWorkChannelConfig) {
        try {
            // 缓存key
            String key = Constants.REDIS_PREFIX.MESSAGE_DING_WORK_TOKEN + StrUtil.COLON + dingWorkChannelConfig.getAgentId();
            // 缓存是否存在
            String token = redisService.get(key);
            if (StrUtil.isNotBlank(token)) {
                return token;
            }
            // 请求生成token并缓存(失效时间: 1小时30分钟)
            DingTalkClient client = new DefaultDingTalkClient(dingWorkChannelConfig.getTokenUrl());
            OapiGettokenRequest request = new OapiGettokenRequest();
            request.setAppkey(dingWorkChannelConfig.getAppKey());
            request.setAppsecret(dingWorkChannelConfig.getAppSecret());
            request.setHttpMethod(Constants.METHOD.GET);
            OapiGettokenResponse response = client.execute(request);
            if (ObjectUtil.isNotNull(response) && ObjectUtil.equals(Constants.COMMON_CODE.LONG_ZONE, response.getErrcode())) {
                String accessToken = response.getAccessToken();
                redisService.set(key, accessToken, 90L, TimeUnit.MINUTES);
                return accessToken;
            }
            return StrUtil.EMPTY;
        } catch (ApiException e) {
            log.info("{} 请求生成token操作异常: {}", DING_WORK_STREAM_CONSUMER, ExceptionUtil.getMessage(e));
            return StrUtil.EMPTY;
        }
    }

}
