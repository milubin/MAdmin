package top.horsemuzi.system.core.rocketmq.consumer;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import top.horsemuzi.system.core.rocketmq.base.RocketConstants;

/**
 * MQ消息消费者
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/06/03 16:57
 **/
@Slf4j
@Component
@ConditionalOnProperty(prefix = "rocketmq", name = "enable", havingValue = "true")
public class ConsumerTest {

    private String tpic;

    @Service
    @RocketMQMessageListener(topic = RocketConstants.TOPIC.TEST_TOPIC, selectorExpression = RocketConstants.TAG.TEST_TAG, consumerGroup = RocketConstants.CONSUMER_GROUP.TEST_CONSUMER_GROUP)
    public class Consumer implements RocketMQListener<MessageExt> {
        @Override
        public void onMessage(MessageExt message) {
            String msg = new String(message.getBody());
            log.info("消费消息 msg={}", msg);
        }
    }


}
