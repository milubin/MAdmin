package top.horsemuzi.system.core.redis.base;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

/**
 * Redis相关常量
 *
 * @author mabin
 * @date 2022/09/20 09:13
 **/

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RedisConstants {

    /**
     * redis的stream通道初始化信息
     */
    public static Map<String, String> STREAM_MAP = new HashMap<>(16);

    static {
        STREAM_MAP.put(STREAM.DING_BOT.STREAM, STREAM.DING_BOT.GROUP);
        STREAM_MAP.put(STREAM.DING_WORK.STREAM, STREAM.DING_WORK.GROUP);
        STREAM_MAP.put(STREAM.EMAIL.STREAM, STREAM.EMAIL.GROUP);
        STREAM_MAP.put(STREAM.SMS.STREAM, STREAM.SMS.GROUP);
        STREAM_MAP.put(STREAM.FLYING_BOT.STREAM, STREAM.FLYING_BOT.GROUP);
    }

    /**
     * redis发布/订阅通道标识
     */
    public interface TOPIC {

        /**
         * 钉钉机器人
         */
        String DING_BOT = "ding_bot";

        /**
         * 钉钉工作消息
         */
        String DING_WORK = "ding_work";

        /**
         * 邮件消息
         */
        String EMAIL = "email";

        /**
         * 短信
         */
        String SMS = "sms";

        /**
         * 飞书机器人
         */
        String FLYING_BOT = "flying_bot";
    }

    /**
     * redis的stream发布/订阅通道标识
     */
    public interface STREAM {

        /**
         * 钉钉机器人Stream流配置
         */
        class DING_BOT {
            public static final String STREAM = "ding_bot_stream";
            public static final String GROUP = "ding_bot_group";
            public static final String CONSUMER = "ding_bot_consumer";
        }

        /**
         * 钉钉工作消息Stream流配置
         */
        class DING_WORK {
            public static final String STREAM = "ding_work_stream";
            public static final String GROUP = "ding_work_group";
            public static final String CONSUMER = "ding_work_consumer";
        }

        /**
         * 邮件消息Stream流配置
         */
        class EMAIL {
            public static final String STREAM = "email_stream";
            public static final String GROUP = "email_group";
            public static final String CONSUMER = "email_consumer";
        }

        /**
         * 短信消息Stream流配置
         */
        class SMS {
            public static final String STREAM = "sms_stream";
            public static final String GROUP = "sms_group";
            public static final String CONSUMER = "sms_consumer";
        }

        /**
         * 飞书机器人Stream流配置
         */
        class FLYING_BOT {
            public static final String STREAM = "flying_bot_stream";
            public static final String GROUP = "flying_bot_group";
            public static final String CONSUMER = "flying_bot_consumer";
        }

    }

}
