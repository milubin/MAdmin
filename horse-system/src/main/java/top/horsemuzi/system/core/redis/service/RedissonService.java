package top.horsemuzi.system.core.redis.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RBlockingDeque;
import org.redisson.api.RDelayedQueue;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * Redisson服务实现
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/05/13 09:51
 **/
@Slf4j
@Component
@RequiredArgsConstructor
public class RedissonService {

    private final RedissonClient redissonClient;

    /**
     * 添加延时队列
     *
     * @param t        延时队列值
     * @param delay    延时时间
     * @param timeUnit 单位
     * @param queue    延时队列键
     * @return void
     * @author mabin
     * @date 2023/5/13 9:54
     **/
    public <T> void addDelayQueue(T t, Long delay, TimeUnit timeUnit, String queue) {
        RBlockingDeque<Object> blockingDeque = redissonClient.getBlockingDeque(queue);
        RDelayedQueue<Object> delayedQueue = redissonClient.getDelayedQueue(blockingDeque);
        delayedQueue.offer(t, delay, timeUnit);
        log.info("延时消息新增成功 msg={}", t);
    }

    /**
     * 获取延时队列
     *
     * @param queue
     * @return T
     * @author mabin
     * @date 2023/5/13 10:10
     **/
    @SuppressWarnings("unchecked")
    public <T> T getDelayQueue(String queue) throws InterruptedException {
        RBlockingDeque<Object> blockingDeque = redissonClient.getBlockingDeque(queue);
        return (T) blockingDeque.take();
    }


}
