package top.horsemuzi.system.core.schedule.task.base;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Spring自定义调度常量
 *
 * @author mabin
 * @date 2022/09/06 09:06
 **/
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TaskConstants {

    public static final String TASK_HEADER = "【Spring调度任务】";

}
