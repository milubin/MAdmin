package top.horsemuzi.system.core.sign.gladoc.base;

import cn.hutool.core.date.DateUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Gladoc VPN 签到服务
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/06/09 11:13
 **/
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class GladocConstants {

    public static final String HEADER = "【Gladoc VPN】" + DateUtil.formatDateTime(new Date());

    public static final String COOKIE = "cookie";


}
