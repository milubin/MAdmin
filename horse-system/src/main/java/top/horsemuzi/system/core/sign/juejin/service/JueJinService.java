package top.horsemuzi.system.core.sign.juejin.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.ContentType;
import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.core.message.common.base.ding.DingBotParam;
import top.horsemuzi.core.message.common.model.DingBotContentModel;
import top.horsemuzi.core.message.enums.MessageTypeEnum;
import top.horsemuzi.system.config.properties.JueJinProperties;
import top.horsemuzi.system.config.properties.SystemProperties;
import top.horsemuzi.system.core.sign.juejin.base.JueJinConstants;
import top.horsemuzi.system.service.MessageService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * 掘金平台相关处理
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/03/09 09:30
 **/
@Slf4j
@Service
@RequiredArgsConstructor
public class JueJinService {

    private final JueJinProperties properties;
    private final MessageService messageService;

    /**
     * 自动签到
     *
     * @param
     * @return void
     * @author mabin
     * @date 2023/3/9 9:41
     **/
    public void autoSign() {
        StopWatch watch = new StopWatch();
        watch.start();
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("startTime", DateUtil.format(new Date(), DatePattern.NORM_DATETIME_MS_FORMAT));
        log.info("{} 开始进行自动签到处理", JueJinConstants.HEADER);
        // 签到
        if (checkAndSignIn(resultMap)) {
            // 抽奖
            draw(resultMap);
            // 沾喜气
            dipLucky(resultMap);
            // 签到总天数, 连续签到天数
            signDays(resultMap);
            // 尝试收集BUG
            collectBug(resultMap);
            // 我的道具
            displayMyTool(resultMap);
        }
        // 发送钉钉消息
        watch.stop();
        resultMap.put("endTime", DateUtil.format(new Date(), DatePattern.NORM_DATETIME_MS_FORMAT));
        sendDingMsg(resultMap, watch.getTotalTimeMillis());
    }

    /**
     * 签到处理
     *
     * @param
     * @param resultMap
     * @return void
     * @author mabin
     * @date 2023/3/9 9:50
     **/
    private boolean checkAndSignIn(Map<String, Object> resultMap) {
        // [{"err_no":0,"err_msg":"success","data":false}]
        // [{"err_no":403,"err_msg":"must login","data":null}]
        String checkResponse = getRequest(properties.getTodayStatus());
        log.info("{} 签到状态信息 checkResponse={}", JueJinConstants.HEADER, checkResponse);
        JSONObject checkObject = JSONUtil.parseObj(checkResponse);
        Integer errNo = checkObject.getInt(JueJinConstants.ERR_NO);
        if (ObjectUtil.notEqual(Constants.COMMON_CODE.ZONE, errNo)) {
            resultMap.put("cookie", false);
            return false;
        } else if (!checkObject.getBool(JueJinConstants.DATA)) {
            // {"err_no":0,"err_msg":"success","data":{"incr_point":700,"sum_point":734}}
            String signResponse = postRequest(properties.getCheck(), null);
            log.info("{} 签到信息 signResponse={}", JueJinConstants.HEADER, signResponse);
            JSONObject signObject = JSONUtil.parseObj(signResponse);
            if (ObjectUtil.equal(Constants.COMMON_CODE.ZONE, signObject.getInt(JueJinConstants.ERR_NO))) {
                JSONObject dataObject = JSONUtil.parseObj(JSONUtil.toJsonStr(signObject.get(JueJinConstants.DATA)));
                Long incrPoint = dataObject.getLong("incr_point");
                Long sumPoint = dataObject.getLong("sum_point");
                log.info("{} 本次签到获取积分:{}，总积分:{}", JueJinConstants.HEADER, incrPoint, sumPoint);
                resultMap.put("incrPoint", incrPoint);
                resultMap.put("sumPoint", sumPoint);
            }
        }
        return true;
    }

    /**
     * 抽奖
     *
     * @param
     * @param resultMap
     * @return void
     * @author mabin
     * @date 2023/3/9 10:29
     **/
    private void draw(Map<String, Object> resultMap) {
        List<String> lotteryNameList = new ArrayList<>();
        // 免费抽奖
        String lotteryConfigResponse = getRequest(properties.getLotteryConfig());
        log.info("{} 免费抽奖次数信息 lotteryConfigResponse={}", JueJinConstants.HEADER, lotteryConfigResponse);
        JSONObject lotteryConfigObject = JSONUtil.parseObj(lotteryConfigResponse);
        int freeDrawCount = Constants.COMMON_CODE.BASE_INT_ZONE;
        if (ObjectUtil.equal(Constants.COMMON_CODE.ZONE, lotteryConfigObject.getInt(JueJinConstants.ERR_NO))) {
            Integer freeCount = JSONUtil.parseObj(lotteryConfigObject.get(JueJinConstants.DATA)).getInt("free_count");
            if (freeCount > Constants.COMMON_CODE.BASE_INT_ZONE) {
                // 免费抽奖一次
                String drawResponse = postRequest(properties.getDraw(), null);
                log.info("{} 免费抽奖一次信息 drawResponse={}", JueJinConstants.HEADER, drawResponse);
                JSONObject drawObject = JSONUtil.parseObj(drawResponse);
                if (ObjectUtil.equal(Constants.COMMON_CODE.ZONE, drawObject.getInt(JueJinConstants.ERR_NO))) {
                    String lotteryName = JSONUtil.parseObj(drawObject.get(JueJinConstants.DATA)).getStr("lottery_name");
                    lotteryNameList.add(lotteryName);
                    freeDrawCount++;
                }
            }
        }
        // 无限积分抽奖
        String curPointResponse = getRequest(properties.getCurPoint());
        log.info("{} 当前账户积分信息 curPointResponse={}", JueJinConstants.HEADER, curPointResponse);
        JSONObject curPointObject = JSONUtil.parseObj(curPointResponse);
        // 抽奖之前的矿石数量
        Integer originPoint = curPointObject.getInt(JueJinConstants.DATA);
        // 抽奖过程中统计的矿石数量
        Integer summaryPoint = originPoint;
        int rouletteDrawCount = Constants.COMMON_CODE.BASE_INT_ZONE;
        while (summaryPoint >= Constants.COMMON_CODE.TWO_HUNDRED) {
            if (rouletteDrawCount > Constants.COMMON_CODE.BASE_INT_ZONE) {
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            String drawResponse = postRequest(properties.getDraw(), null);
            log.info("{} 无限抽奖第{}次信息 drawResponse={}", JueJinConstants.HEADER, rouletteDrawCount, drawResponse);
            JSONObject drawObject = JSONUtil.parseObj(drawResponse);
            if (ObjectUtil.equal(Constants.COMMON_CODE.ZONE, drawObject.getInt(JueJinConstants.ERR_NO))) {
                // 抽奖成功, 积分减200
                summaryPoint -= Constants.COMMON_CODE.TWO_HUNDRED;
                String lotteryName = JSONUtil.parseObj(drawObject.get(JueJinConstants.DATA)).getStr("lottery_name");
                // 如果奖品是积分, 累加到总积分
                if (StrUtil.contains(lotteryName, "矿石")) {
                    summaryPoint += Convert.toInt(StrUtil.replace(lotteryName, "矿石", StrUtil.EMPTY));
                }
                log.info("{} 第{}抽的奖品: {}, 积分剩余: {}", JueJinConstants.HEADER, rouletteDrawCount, lotteryName, summaryPoint);
                lotteryNameList.add(lotteryName);
                rouletteDrawCount++;
            } else {
                try {
                    // 抽奖请求异常时, 睡眠2s后重试
                    Thread.sleep(2000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        resultMap.put("surplusPoint", summaryPoint);
        resultMap.put("lotteryNameList", lotteryNameList);
        resultMap.put("drawCount", freeDrawCount + rouletteDrawCount);
        resultMap.put("originPoint", originPoint);
    }

    /**
     * 沾喜气
     *
     * @param resultMap
     * @return void
     * @author mabin
     * @date 2023/3/9 11:06
     **/
    private void dipLucky(Map<String, Object> resultMap) {
        String bigResponse = postRequest(properties.getGlobalBig(), null);
        log.info("{} 可沾喜气的用户信息 bigResponse={}", JueJinConstants.HEADER, bigResponse);
        JSONArray array = JSONUtil.parseArray(JSONUtil.parseObj(JSONUtil.parseObj(bigResponse).get(JueJinConstants.DATA)).get("lotteries"));
        if (array.size() > Constants.COMMON_CODE.BASE_INT_ZONE) {
            JSONObject object = JSONUtil.parseObj(array.get(RandomUtil.randomInt(array.size() - 1)));
            String lotteryHistoryId = object.getStr("history_id");
            if (StrUtil.isNotBlank(lotteryHistoryId)) {
                // 沾喜气
                // {"err_no":0,"err_msg":"success","data":{"dip_action":1,"has_dip":false,"total_value":3055,"dip_value":10}}
                String digResponse = postRequest(properties.getDipLucky(), Collections.singletonMap("lottery_history_id", lotteryHistoryId));
                log.info("{} 沾喜气信息 digResponse={}", JueJinConstants.HEADER, digResponse);
                JSONObject digObject = JSONUtil.parseObj(JSONUtil.parseObj(digResponse).get(JueJinConstants.DATA));
                Integer totalValue = digObject.getInt("total_value");
                resultMap.put("totalValue", totalValue);
                resultMap.put("luckyRate", BigDecimal.valueOf(totalValue).divide(BigDecimal.valueOf(6000L), 3, RoundingMode.HALF_UP)
                        .multiply(BigDecimal.valueOf(Constants.COMMON_CODE.ONE_HUNDRED)).stripTrailingZeros().toPlainString() + "%");
            }
        }
    }

    /**
     * 签到总天数, 连续签到天数
     *
     * @param resultMap
     * @return void
     * @author mabin
     * @date 2023/3/9 11:14
     **/
    private void signDays(Map<String, Object> resultMap) {
        String daysResponse = getRequest(properties.getCounts());
        log.info("{} 签到信息 daysResponse={}", JueJinConstants.HEADER, daysResponse);
        JSONObject resObject = JSONUtil.parseObj(JSONUtil.parseObj(daysResponse).get(JueJinConstants.DATA));
        resultMap.put("contCount", resObject.getInt("cont_count"));
        resultMap.put("sumCount", resObject.getInt("sum_count"));
    }

    /**
     * 尝试收集BUG
     *
     * @param resultMap
     * @return void
     * @author mabin
     * @date 2023/3/10 14:33
     **/
    private void collectBug(Map<String, Object> resultMap) {
        String notResponse = postRequest(properties.getNotCollectBug(), null);
        log.info("{} 待收集BUG信息 notResponse={}", JueJinConstants.HEADER, notResponse);
        JSONObject notObject = JSONUtil.parseObj(notResponse);
        if (ObjectUtil.equal(Constants.COMMON_CODE.ZONE, notObject.getInt(JueJinConstants.ERR_NO))) {
            int bugCount = 0;
            JSONArray bugArray = notObject.getJSONArray(JueJinConstants.DATA);
            if (ArrayUtil.isNotEmpty(bugArray)) {
                for (Object bug : bugArray) {
                    JSONObject bugObject = JSONUtil.parseObj(bug);
                    bugObject.remove("bug_show_type");
                    bugObject.remove("is_first");
                    String response = postRequest(properties.getCollectBug(), bugObject);
                    log.info("{} 收集BUG信息 response={}", JueJinConstants.HEADER, response);
                    if (ObjectUtil.equal(Constants.COMMON_CODE.ZONE, JSONUtil.parseObj(response).getInt(JueJinConstants.ERR_NO))) {
                        bugCount++;
                    }
                }
            }
            resultMap.put("collectBug", bugCount);
        }
    }

    /**
     * 获取我的道具
     *
     * @param resultMap
     * @return void
     * @author mabin
     * @date 2023/3/10 14:59
     **/
    private void displayMyTool(Map<String, Object> resultMap) {
        String response = postRequest(properties.getMyTool(), null);
        log.info("{} 我的道具信息 response={}", JueJinConstants.HEADER, response);
        JSONObject resObject = JSONUtil.parseObj(response);
        if (ObjectUtil.equal(Constants.COMMON_CODE.ZONE, resObject.getInt(JueJinConstants.ERR_NO))) {
            JSONObject dataObject = resObject.getJSONObject(JueJinConstants.DATA);
            if (ObjectUtil.isNotNull(dataObject)) {
                JSONArray lotteryArray = dataObject.getJSONArray("lottery_histories");
                if (ArrayUtil.isNotEmpty(lotteryArray)) {
                    List<String> lotteryList = new ArrayList<>();
                    for (Object lottery : lotteryArray) {
                        JSONObject lotteryObject = JSONUtil.parseObj(lottery);
                        lotteryList.add(lotteryObject.getStr("lottery_name") + " x " + lotteryObject.getInt("lottery_count"));
                        resultMap.put("myTool", CollUtil.isNotEmpty(lotteryList) ? CollUtil.join(lotteryList, "，") : "暂无道具");
                    }
                }
            }
        }
    }

    /**
     * 发送钉钉消息
     *
     * @param resultMap
     * @param totalTimeMillis
     * @return void
     * @author mabin
     * @date 2023/3/9 14:21
     **/
    private void sendDingMsg(Map<String, Object> resultMap, long totalTimeMillis) {
        if (MapUtil.isEmpty(resultMap)) {
            return;
        }
        DingBotContentModel model = new DingBotContentModel();
        model.setTitle("掘金推送");
        model.setSendType(MessageTypeEnum.ACTION_CARD.getCode());
        model.setBtnOrientation(Constants.COMMON_CODE.STR_ONE);
        List<DingBotParam.ActionCardVO.BtnsVO> btnVOList = new ArrayList<>();
        DingBotParam.ActionCardVO.BtnsVO btnVO = new DingBotParam.ActionCardVO.BtnsVO();
        btnVO.setTitle(SystemProperties.name);
        btnVO.setActionURL("http://192.168.85.139:9527/admin/");
        btnVOList.add(btnVO);
        model.setBtns(JsonUtil.toJson(btnVOList));
        // 账号是否正常
        if (ObjectUtil.isNotNull(resultMap.get("cookie"))) {
            model.setContent("![1](https://horse-blog.oss-cn-hangzhou.aliyuncs.com/202303/NMZlf0piTA.png) \n" +
                    "#### **掘金** \n" +
                    "---  \n" +
                    "**Cookie状态：** Cookie已失效，请重新配置！");
        } else {
            String msgTemplate = "![1](https://horse-blog.oss-cn-hangzhou.aliyuncs.com/202303/NMZlf0piTA.png) \n" +
                    "#### **掘金** \n" +
                    "---  \n" +
                    "**Cookie状态：** Cookie正常 \n\n" +
                    "**签到：** 总签到 {} 天，连续签到 {} 天 \n\n" +
                    "**沾喜气：** 沾喜气成功，当前幸运值：{}，幸运值比例：{} \n\n" +
                    "**轮盘抽奖：** 抽奖 {} 次，矿石 x {}，BUG x {}，其他 x {} \n\n" +
                    "**收集BUG：** BUG x {} \n\n" +
                    "**矿石变化：** 初始 {} 矿石，剩余 {} 矿石 \n\n" +
                    "**我的道具：** {} \n\n" +
                    "**签到耗时：** 起始 {}-{}，共耗时 {}ms";
            JSONObject resultObject = JSONUtil.parseObj(JSONUtil.toJsonStr(resultMap));
            JSONArray lotteryNameList = resultObject.getJSONArray("lotteryNameList");
            long mineralCount = 0;
            long bugCount = 0;
            long otherCount = 0;
            if (ArrayUtil.isNotEmpty(lotteryNameList)) {
                mineralCount = lotteryNameList.stream().map(Convert::toStr)
                        .filter(lottery -> StrUtil.containsIgnoreCase(lottery, JueJinConstants.MINERAL))
                        .map(lottery -> StrUtil.replace(lottery, JueJinConstants.MINERAL, StrUtil.EMPTY))
                        .mapToLong(Long::parseLong)
                        .sum();
                bugCount = lotteryNameList.stream().filter(lottery -> StrUtil.containsIgnoreCase(Convert.toStr(lottery), "Bug")).count();
                otherCount = lotteryNameList.size() - lotteryNameList.stream().map(Convert::toStr)
                        .filter(lottery -> StrUtil.containsIgnoreCase(lottery, JueJinConstants.MINERAL)).count() - bugCount;
            }
            model.setContent(StrUtil.format(msgTemplate, resultObject.getInt("sumCount"), resultObject.getInt("contCount"),
                    resultObject.getInt("totalValue"), resultObject.getStr("luckyRate"), resultObject.getInt("drawCount"),
                    mineralCount, bugCount, otherCount, resultObject.getInt("collectBug"), resultObject.getInt("originPoint"),
                    resultObject.getInt("surplusPoint"), resultObject.getStr("myTool"), resultObject.getStr("startTime"),
                    resultObject.getStr("endTime"), totalTimeMillis));
        }
        messageService.sendDingBotMessage(model, null);
    }

    /**
     * GET请求
     *
     * @param url
     * @return java.lang.String
     * @author mabin
     * @date 2023/3/9 15:27
     **/
    private String getRequest(String url) {
        return HttpRequest.get(url)
                .timeout(10000)
                .setFollowRedirects(false)
                .cookie(properties.getCookie())
                .header(Header.CONTENT_TYPE, ContentType.JSON.getValue())
                .header(Header.USER_AGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36 Edg/95.0.1020.53")
                .execute()
                .body();
    }

    /**
     * POST请求
     *
     * @param url
     * @param paramMap
     * @return java.lang.String
     * @author mabin
     * @date 2023/3/9 15:27
     **/
    private String postRequest(String url, Map<String, Object> paramMap) {
        HttpRequest httpRequest = HttpRequest.post(url)
                .timeout(10000)
                .setFollowRedirects(false)
                .cookie(properties.getCookie());
        if (MapUtil.isNotEmpty(paramMap)) {
            httpRequest.form(paramMap);
        }
        return httpRequest.execute().body();
    }

}
