package top.horsemuzi.system.core.rocketmq.base;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * Rocket基础信息
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/06/03 16:53
 **/

public class RocketConstants {

    public interface TOPIC {
        String TEST_TOPIC = "madmin_test_topic";
    }

    public interface TAG {
        String TEST_TAG = "madmin_test_tag";
    }

    public interface CONSUMER_GROUP {
        String TEST_CONSUMER_GROUP = "madmin_test_consumer_group";
    }

    /**
     * 缺省延迟等级枚举
     */
    @Getter
    @ToString
    @AllArgsConstructor
    public enum DELAY {

        ONE_LEVEL(1),
        TWO_LEVEL(2),
        THREE_LEVEL(3),
        FOUR_LEVEL(4),
        FIVE_LEVEL(5),
        SIX_LEVEL(6),
        SEVEN_LEVEL(7),
        EIGHT_LEVEL(8),
        NINE_LEVEL(9),
        TEN_LEVEL(10),
        ELEVEN_LEVEL(11),
        TWELVE_LEVEL(12),
        THIRTEEN_LEVEL(13),
        FOURTEEN_LEVEL(14),
        FIFTEEN_LEVEL(15),
        SIXTEEN_LEVEL(16),
        SEVENTEEN_LEVEL(17),
        EIGHTEEN_LEVEL(18);

        private final Integer level;
    }

}
