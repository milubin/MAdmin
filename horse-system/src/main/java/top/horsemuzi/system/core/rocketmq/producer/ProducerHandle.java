package top.horsemuzi.system.core.rocketmq.producer;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import top.horsemuzi.system.core.rocketmq.base.RocketConstants;
import top.horsemuzi.system.core.rocketmq.base.SendMessageResult;

import java.util.Objects;

/**
 * 消息生产者MQ处理
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/06/07 15:55
 **/
@Slf4j
@Component
@RequiredArgsConstructor
@ConditionalOnProperty(prefix = "rocketmq", name = "enable", havingValue = "true")
public class ProducerHandle implements ProducerProxy {

    private final RocketMQTemplate rocketMQTemplate;


    /**
     * 同步发送消息
     *
     * @param topic       主题
     * @param tag         标签
     * @param messageBody 消息
     * @param delayLevel  延时等级
     * @return top.horsemuzi.system.core.rocketmq.base.SendMessageResult
     * @author mabin
     * @date 2023/6/3 17:54
     **/
    @Override
    public SendMessageResult sendMessage(String topic, String tag, String messageBody, RocketConstants.DELAY delayLevel) {
        log.info("RocketMQ producer, sync message, topic: {}, tag: {}, message: {}", topic, tag, messageBody);
        SendResult sendResult;
        SendMessageResult result = null;
        try {
            if (Objects.nonNull(delayLevel)) {
                sendResult = rocketMQTemplate.syncSend(topic + StrUtil.COLON + tag, MessageBuilder.withPayload(messageBody).build(), 10000L, delayLevel.getLevel());
            } else {
                sendResult = rocketMQTemplate.syncSend(topic + StrUtil.COLON + tag, MessageBuilder.withPayload(messageBody).build());
            }
            log.info("RocketMQ producer, sync message send success");
            log.info("RocketMQ producer, sync message send info, topic: {}, tag: {}, message: {}, result: {}", topic, tag, messageBody, sendResult);
            result = new SendMessageResult();
            result.setMessageId(sendResult.getMsgId());
            result.setOriginResult(JSON.toJSONString(sendResult));
            result.setSendStatus(Boolean.TRUE);
        } catch (Exception e) {
            log.info("RocketMQ producer, sync message send fail");
            log.info("RocketMQ producer, sync message send info, topic: {}, tag: {}, message: {}, error: {}", topic, tag, messageBody, e.getMessage());
            result.setSendStatus(Boolean.FALSE);
            result.setExceptionMessage(e.getMessage());
        }
        return result;
    }

    /**
     * 异步发送消息
     *
     * @param topic       主题
     * @param tag         标签
     * @param messageBody 消息
     * @return top.horsemuzi.system.core.rocketmq.base.SendMessageResult
     * @author mabin
     * @date 2023/6/3 17:55
     **/
    @Override
    public SendMessageResult sendAsyncMessage(String topic, String tag, String messageBody) {
        log.info("RocketMQ producer, async message, topic: {}, tag: {}, message: {}", topic, tag, messageBody);
        SendMessageResult result = new SendMessageResult();
        rocketMQTemplate.asyncSend(topic + StrUtil.COLON + tag, MessageBuilder.withPayload(messageBody).build(), new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                log.info("RocketMQ producer, async message send success");
                log.info("RocketMQ producer, async message send info, topic: {}, tag: {}, message: {}, result: {}", topic, tag, messageBody, sendResult);
                result.setMessageId(sendResult.getMsgId());
                result.setOriginResult(JSON.toJSONString(sendResult));
                result.setSendStatus(Boolean.TRUE);
            }

            @Override
            public void onException(Throwable throwable) {
                log.info("RocketMQ producer, async message send fail");
                log.info("RocketMQ producer, async message send info, topic: {}, tag: {}, message: {}, error: {}", topic, tag, messageBody, throwable.getMessage());
                result.setSendStatus(Boolean.FALSE);
                result.setExceptionMessage(throwable.getMessage());
            }
        });
        return result;
    }

}
