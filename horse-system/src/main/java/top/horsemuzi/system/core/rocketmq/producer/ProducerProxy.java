package top.horsemuzi.system.core.rocketmq.producer;

import top.horsemuzi.system.core.rocketmq.base.RocketConstants;
import top.horsemuzi.system.core.rocketmq.base.SendMessageResult;

/**
 * 消息发送者
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/06/03 17:52
 **/
public interface ProducerProxy {

    /**
     * 同步发送消息
     *
     * @param topic       主题
     * @param tag         标签
     * @param messageBody 消息
     * @param delayLevel  延时等级
     * @return top.horsemuzi.system.core.rocketmq.base.SendMessageResult
     * @author mabin
     * @date 2023/6/3 17:54
     **/
    SendMessageResult sendMessage(String topic, String tag, String messageBody, RocketConstants.DELAY delayLevel);

    /**
     * 异步发送消息
     *
     * @param topic       主题
     * @param tag         标签
     * @param messageBody 消息
     * @return top.horsemuzi.system.core.rocketmq.base.SendMessageResult
     * @author mabin
     * @date 2023/6/3 17:55
     **/
    SendMessageResult sendAsyncMessage(String topic, String tag, String messageBody);

}
