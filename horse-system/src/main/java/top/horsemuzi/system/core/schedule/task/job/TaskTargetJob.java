package top.horsemuzi.system.core.schedule.task.job;

import cn.hutool.core.util.RandomUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import top.horsemuzi.system.core.schedule.task.base.TaskConstants;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description {description}
 * @date 2022/8/18 21:24
 */
@Slf4j
@Component
public class TaskTargetJob {

    public void testOne() {
        log.info("{} 无参测试任务开始执行", TaskConstants.TASK_HEADER);
        if (RandomUtil.randomInt(100) % 2 == 0) {
            log.error("{} 无参测试任务执行失败", TaskConstants.TASK_HEADER);
            int i = 1 / 0;
        } else {
            log.info("{} 无参测试任务执行成功", TaskConstants.TASK_HEADER);
        }
    }

    public void testTwo(String taskParams) {
        log.info("{} 有参测试任务开始执行, taskParams={}", TaskConstants.TASK_HEADER, taskParams);
        if (RandomUtil.randomInt(100) % 2 != 0) {
            log.error("{} 有参测试任务执行失败", TaskConstants.TASK_HEADER);
            int i = 1 / 0;
        } else {
            log.info("{} 有参测试任务执行成功", TaskConstants.TASK_HEADER);
        }
    }

}
