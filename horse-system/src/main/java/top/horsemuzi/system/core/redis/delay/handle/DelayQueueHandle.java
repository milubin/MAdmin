package top.horsemuzi.system.core.redis.delay.handle;

/**
 * 延时队列执行器
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/05/13 10:21
 **/

public interface DelayQueueHandle<T> {

    /**
     * 延时队列执行器
     *
     * @param t
     * @return void
     * @author mabin
     * @date 2023/5/13 10:21
     **/
    void execute(T t);
}
