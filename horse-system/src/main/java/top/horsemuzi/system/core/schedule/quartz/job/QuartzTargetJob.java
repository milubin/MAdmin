package top.horsemuzi.system.core.schedule.quartz.job;

import cn.hutool.core.util.RandomUtil;
import lombok.extern.slf4j.Slf4j;
import top.horsemuzi.system.core.schedule.quartz.base.QuartzConstants;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description Quartz调度任务的执行目标实例
 * @date 2022/8/21 17:59
 */
@Slf4j
public class QuartzTargetJob {

    public void testOne(String s, Boolean b, Long l, Double d, Integer i) {
        log.info("{} 方法参数信息, String: {}, Boolean: {}, Long: {}, Double: {}, Integer: {}",
                QuartzConstants.QUARTZ_HEADER, s, b, l, d, i);
        int value = RandomUtil.randomInt(100);
        if (value % 2 == 0) {
            log.error("{} 多参数调度任务执行失败", QuartzConstants.QUARTZ_HEADER);
            int j = 1 / 0;
        }
        log.info("{} 多参数调度任务执行成功", QuartzConstants.QUARTZ_HEADER);
    }

    public void testTwo() {
        // 随机异常
        int value = RandomUtil.randomInt(100);
        if (value % 2 == 0) {
            int i = 1 / 0;
            log.error("{} 无参调度任务执行失败", QuartzConstants.QUARTZ_HEADER);
        }
        log.info("{} 无参调度任务执行成功", QuartzConstants.QUARTZ_HEADER);
    }

}
