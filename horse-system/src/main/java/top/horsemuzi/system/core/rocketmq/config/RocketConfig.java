package top.horsemuzi.system.core.rocketmq.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;

/**
 * RocketMQ配置文件
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/06/03 16:51
 **/
@Configuration
@ConditionalOnProperty(prefix = "rocketmq", name = "enable", havingValue = "true")
public class RocketConfig {

}
