package top.horsemuzi.system.core.redis.listener.consume;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.stereotype.Component;

/**
 * 邮件消费者监听
 *
 * @author mabin
 * @date 2022/09/20 17:05
 **/
@Slf4j
@Component
public class ConsumeEmailMessageListener extends AbstractConsumeMessageListener {

    @Override
    public void onMessage(Message message, byte[] pattern) {
        log.info("pattern={}, message={}", new String(pattern), message.toString());
    }

}
