package top.horsemuzi.system.core.rocketmq.base;

import lombok.Data;

import java.io.Serializable;

/**
 * MQ消息发送返回结果
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/06/03 17:50
 **/
@Data
public class SendMessageResult implements Serializable {
    private static final long serialVersionUID = -3105848990672761720L;

    /**
     * 消息发送状态 true: 成功, false: 失败
     */
    private Boolean sendStatus;

    /**
     * 异常原因
     */
    private String exceptionMessage;

    /**
     * 消息ID
     */
    private String messageId;

    /**
     * 原始消息发送结果JSON
     */
    private String originResult;
}
