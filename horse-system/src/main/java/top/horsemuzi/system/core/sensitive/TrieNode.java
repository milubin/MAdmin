package top.horsemuzi.system.core.sensitive;

import java.util.HashMap;
import java.util.Map;

/**
 * 敏感词过滤前缀数实例
 *
 * @author mabin
 * @date 2022/11/04 10:13
 **/

public class TrieNode {

    /**
     * 过滤的关键词结束标识
     */
    private boolean isKeyWordEnd = false;

    /**
     * 子节点
     */
    private Map<Character, TrieNode> subNodes = new HashMap<>(16);


    public boolean isKeyWordEnd() {
        return isKeyWordEnd;
    }

    public void setKeyWordEnd(boolean keyWordEnd) {
        isKeyWordEnd = keyWordEnd;
    }

    /**
     * 添加子节点
     *
     * @param character
     * @param node
     */
    public void addSubNode(Character character, TrieNode node) {
        subNodes.put(character, node);
    }

    /**
     * 获取子节点
     *
     * @param character
     * @return
     */
    public TrieNode getSubNode(Character character) {
        return subNodes.get(character);
    }

}
