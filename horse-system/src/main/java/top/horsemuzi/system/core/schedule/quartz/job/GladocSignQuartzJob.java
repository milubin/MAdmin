package top.horsemuzi.system.core.schedule.quartz.job;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import top.horsemuzi.system.core.sign.gladoc.service.GladocService;

/**
 * GladocVPN自动签到
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/06/09 15:00
 **/
@Component
public class GladocSignQuartzJob {

    @Autowired
    private GladocService gladocService;

    public void sign() {
        gladocService.autoSign();
    }

}
