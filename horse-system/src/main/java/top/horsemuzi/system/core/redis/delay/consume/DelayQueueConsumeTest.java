package top.horsemuzi.system.core.redis.delay.consume;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import top.horsemuzi.system.core.redis.delay.handle.DelayQueueHandle;

/**
 * 延时队列具体业务消费者
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/05/13 10:22
 **/
@Slf4j
@Component("delayQueueConsumeTest")
public class DelayQueueConsumeTest implements DelayQueueHandle<Object> {


    /**
     * 延时队列执行器
     *
     * @param o
     * @return void
     * @author mabin
     * @date 2023/5/13 10:21
     **/
    @Override
    public void execute(Object o) {
        log.info("延时消息消费成功 msg={}", o);
    }

}
