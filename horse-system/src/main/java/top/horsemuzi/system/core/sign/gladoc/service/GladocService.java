package top.horsemuzi.system.core.sign.gladoc.service;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.core.message.common.base.ding.DingBotParam;
import top.horsemuzi.core.message.common.model.DingBotContentModel;
import top.horsemuzi.core.message.enums.MessageTypeEnum;
import top.horsemuzi.system.config.properties.SystemProperties;
import top.horsemuzi.system.core.sign.gladoc.base.GladocConstants;
import top.horsemuzi.system.service.MessageService;

import java.util.*;

/**
 * Gladoc VPN 服务
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/06/09 11:16
 **/
@Slf4j
@Service
@RequiredArgsConstructor
public class GladocService {

    @Value("${custom.sign.gladoc.cookie}")
    private String cookie;

    @Value("${custom.sign.gladoc.check_url}")
    private String checkUrl;

    @Value("${custom.sign.gladoc.state_url}")
    private String stateUrl;

    @Value("${custom.sign.gladoc.token}")
    private String token;

    private final MessageService messageService;

    public void autoSign() {
        StopWatch watch = new StopWatch();
        watch.start();
        log.info("{} 开始进行自动签到处理", GladocConstants.HEADER);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("startTime", DateUtil.format(new Date(), DatePattern.NORM_DATETIME_MS_FORMAT));
        checkIn(resultMap);
        if (Convert.toBool(resultMap.get(GladocConstants.COOKIE), Boolean.FALSE)) {
            getState(resultMap);
        }
        // 发送钉钉消息
        watch.stop();
        resultMap.put("endTime", DateUtil.format(new Date(), DatePattern.NORM_DATETIME_MS_FORMAT));
        sendMessage(resultMap, watch.getTotalTimeMillis());
    }

    /**
     * 发送钉钉消息
     *
     * @param resultMap
     * @param totalTimeMillis
     * @return void
     * @author mabin
     * @date 2023/6/9 14:45
     **/
    private void sendMessage(Map<String, Object> resultMap, long totalTimeMillis) {
        DingBotContentModel model = new DingBotContentModel();
        model.setTitle("Gladoc VPN推送");
        model.setSendType(MessageTypeEnum.ACTION_CARD.getCode());
        model.setBtnOrientation(Constants.COMMON_CODE.STR_ONE);
        List<DingBotParam.ActionCardVO.BtnsVO> btnVOList = new ArrayList<>();
        DingBotParam.ActionCardVO.BtnsVO btnVO = new DingBotParam.ActionCardVO.BtnsVO();
        btnVO.setTitle(SystemProperties.name);
        btnVO.setActionURL("http://192.168.85.139:9527/admin/");
        btnVOList.add(btnVO);
        model.setBtns(JsonUtil.toJson(btnVOList));
        // 账号是否正常
        if (!Convert.toBool(resultMap.get(GladocConstants.COOKIE), Boolean.FALSE)) {
            model.setContent("![1](https://horse-blog.oss-cn-hangzhou.aliyuncs.com/202303/NMZlf0piTA.png) \n" +
                    "#### **Gladoc VPN** \n" +
                    "---  \n" +
                    "**Cookie状态：** Cookie已失效，请重新配置！");
        } else {
            String msgTemplate = "![1](https://horse-blog.oss-cn-hangzhou.aliyuncs.com/202303/NMZlf0piTA.png) \n" +
                    "#### **Gladoc VPN** \n" +
                    "---  \n" +
                    "**Cookie状态：** Cookie正常 \n\n" +
                    "**签到账号：** {} \n\n" +
                    "**签到信息：** {} \n\n" +
                    "**VPN时长：** {} 天 \n\n" +
                    "**签到耗时：** 起始 {}-{}，共耗时 {}ms";
            model.setContent(StrUtil.format(msgTemplate, resultMap.get("email"), resultMap.get("message"), resultMap.get("days"), resultMap.get("startTime"),
                    resultMap.get("endTime"), totalTimeMillis));
        }
        messageService.sendDingBotMessage(model, null);
    }

    /**
     * 获取签到状态信息
     *
     * @param resultMap
     * @return void
     * @author mabin
     * @date 2023/6/9 14:41
     **/
    private void getState(Map<String, Object> resultMap) {
        String body = HttpRequest.get(stateUrl)
                .header(Header.COOKIE, cookie)
                .execute().body();
        log.info("{} 获取签到状态信息 response={}", GladocConstants.HEADER, body);
        if (StrUtil.isNotBlank(body)) {
            JSONObject entries = JSONUtil.parseObj(body);
            JSONObject data = entries.getJSONObject("data");
            resultMap.put("days", data.getInt("leftDays"));
            resultMap.put("email", data.getStr("email"));
        }
    }

    /**
     * 签到处理
     *
     * @param resultMap
     * @return void
     * @author mabin
     * @date 2023/6/9 14:33
     **/
    private void checkIn(Map<String, Object> resultMap) {
        Map<String, Object> paramsMap = new HashMap<>(3);
        paramsMap.put("token", token);
        String response = HttpRequest.post(checkUrl)
                .header(Header.COOKIE, cookie)
                .body(JSON.toJSONString(paramsMap))
                .execute().body();
        if (StrUtil.isBlank(response)) {
            log.error("{} 自动签到响应结果为空", GladocConstants.HEADER);
            resultMap.put(GladocConstants.COOKIE, Boolean.FALSE);
            return;
        }
        JSONObject entries = JSONUtil.parseObj(response);
        if (Objects.nonNull(entries.get("list"))) {
            log.info("{} 自动签到成功 response={}", GladocConstants.HEADER, response);
            resultMap.put(GladocConstants.COOKIE, Boolean.TRUE);
            resultMap.put("message", entries.getStr("message"));
        } else {
            log.error("{} 自动签到失败 response={}", GladocConstants.HEADER, response);
            resultMap.put(GladocConstants.COOKIE, Boolean.FALSE);
        }
    }

}
