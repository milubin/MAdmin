package top.horsemuzi.system.core.schedule.quartz.job;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.ObjectUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Range;
import org.springframework.data.redis.connection.stream.MapRecord;
import org.springframework.data.redis.connection.stream.PendingMessage;
import org.springframework.data.redis.connection.stream.PendingMessages;
import org.springframework.data.redis.connection.stream.PendingMessagesSummary;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.MessageStatusEnum;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.core.message.common.MessageInfo;
import top.horsemuzi.core.message.enums.SendEnum;
import top.horsemuzi.system.core.redis.base.RedisConstants;
import top.horsemuzi.system.core.redis.service.RedisService;
import top.horsemuzi.system.manager.admin.MessageManager;
import top.horsemuzi.system.pojo.dto.message.MessageDTO;

import java.util.List;
import java.util.Map;

/**
 * 处理Redis的stream消息调度任务
 *
 * @author mabin
 * @date 2022/09/24 15:09
 **/
@Slf4j
@Component
public class RedisStreamQuartzJob {

    private static final String PENDING_HEAD = "【消费pending列表消息】";

    @Autowired
    private RedisService redisService;

    @Autowired
    private MessageManager messageManager;

    /**
     * 调度任务处理 stream流中的未ACK的消息,
     * 处理逻辑: 遍历消费者组的pending消息情况, 再遍历组中每个消费者的pending消息id列表, 根据id直接读取消息并进行重新消费, ACK应答
     */
    @SneakyThrows
    public void handleStreamMessage() {
        Map<String, String> streamMap = RedisConstants.STREAM_MAP;
        if (MapUtil.isNotEmpty(streamMap)) {
            for (Map.Entry<String, String> entry : streamMap.entrySet()) {
                String streamName = entry.getKey();
                String streamGroup = entry.getValue();
                PendingMessagesSummary pendingMessagesSummary = redisService.pending(streamName, streamGroup);
                // 所有pending消息数量
                // 消费者组名称 (和 entry.getValue 值相同)
                Range<String> idRange = pendingMessagesSummary.getIdRange();
                String minMessageId = idRange.getLowerBound().getValue().orElse(Constants.COMMON_CODE.STR_ZONE);
                String maxMessageId = idRange.getUpperBound().getValue().orElse(Constants.COMMON_CODE.STR_ZONE);
                log.info("{} 消费者组: {} 共有{}条pending消息, 最小ID={}, 最大ID={}", PENDING_HEAD, streamGroup, pendingMessagesSummary.getTotalPendingMessages(), minMessageId, maxMessageId);
                // 组中每个消费者和对应的pending消息数量
                Map<String, Long> pendingMessagesPerConsumer = pendingMessagesSummary.getPendingMessagesPerConsumer();
                if (MapUtil.isNotEmpty(pendingMessagesPerConsumer)) {
                    log.info("{} 消费者组: {} 共有{}个消费者", PENDING_HEAD, streamGroup, pendingMessagesPerConsumer.size());
                    for (Map.Entry<String, Long> consumerEntity : pendingMessagesPerConsumer.entrySet()) {
                        // 消费者名称
                        String consumerName = consumerEntity.getKey();
                        // 消费者的pending消息数量
                        Long consumeTotalPendingMessages = consumerEntity.getValue();
                        log.info("{} 开始处理消费者 {}=>{} pending列表的{}条消息", PENDING_HEAD, streamGroup, consumerName, consumeTotalPendingMessages);
                        if (consumeTotalPendingMessages > Constants.COMMON_CODE.ZONE) {
                            PendingMessages pendingMessages = redisService.pending(streamName, streamGroup, consumerName);
                            log.info("{} stream流: {}, 消费者组: {}, 消费者: {} 的pending列表消息详情 pendingMessages={}", PENDING_HEAD, streamName, streamGroup, consumerName, pendingMessages);
                            if (CollUtil.isNotEmpty(pendingMessages)) {
                                for (PendingMessage message : pendingMessages) {
                                    List<MapRecord<String, String, String>> recordList = redisService.range(streamName, message.getId().toString());
                                    if (CollUtil.isEmpty(recordList)) {
                                        continue;
                                    }
                                    // 消息详情
                                    MapRecord<String, String, String> record = recordList.get(Constants.COMMON_CODE.ZONE);
                                    log.info("{} 消费者: {}=>{}, 消息ID: {} 的消息详情 record={}", PENDING_HEAD, streamGroup, consumerName, message.getId(), record);
                                    // 执行消费消息逻辑
                                    String recordId = record.getId().toString();
                                    log.info("{} 消费者: {}=>{}, 消息ID: {} 的消息开始重新消费", PENDING_HEAD, streamGroup, consumerName, recordId);
                                    MessageDTO messageDTO = messageManager.getMessage(recordId);
                                    log.info("{} 消费者: {}=>{}, 消息ID: {} 的数据库消息信息 messageDTO={}", PENDING_HEAD, streamGroup, consumerName, recordId, messageDTO);
                                    if (ObjectUtil.isNull(messageDTO)) {
                                        continue;
                                    }
                                    // 是否已达到最大消费次数（默认3次，若仍消费失败，则直接置为消费失败，标明备注）
                                    if (CharSequenceUtil.equals(messageDTO.getStatus(), MessageStatusEnum.FAIL.getCode()) || messageDTO.getRetryCount() >= Constants.COMMON_CODE.THREE) {
                                        log.warn("{} 消费者: {}=>{}, 消息ID: {} 的消息已达最大重试次数, 直接跳过", PENDING_HEAD, streamGroup, consumerName, recordId);
                                        continue;
                                    }
                                    if (CharSequenceUtil.equals(messageDTO.getStatus(), MessageStatusEnum.INIT.getCode()) && CharSequenceUtil.isNotBlank(messageDTO.getMessageInfo())) {
                                        // 未消费
                                        log.info("{} 消费者: {}=>{}, 消息ID: {} 的消息内容 messageInfo={}", PENDING_HEAD, streamGroup, consumerName, recordId, messageDTO.getMessageInfo());
                                        MessageInfo messageInfo = JsonUtil.fromJson(messageDTO.getMessageInfo(), MessageInfo.class);
                                        SendEnum sendEnum = SendEnum.getSendEnum(messageDTO.getChannel());
                                        if (ObjectUtil.isNotNull(sendEnum)) {
                                            if (sendEnum.getSendHandler().send(messageInfo)) {
                                                messageDTO.setStatus(MessageStatusEnum.SUCCESS.getCode());
                                                messageDTO.setRemark("pending列表消费成功");
                                                log.info("{} 消费者: {}=>{}, 消息ID: {} 的消息消费成功", PENDING_HEAD, streamGroup, consumerName, recordId);
                                                if (redisService.acknowledge(streamGroup, record) > 0) {
                                                    messageDTO.setStatus(MessageStatusEnum.ACK_SUCCESS.getCode());
                                                    messageDTO.setRemark("pending列表消费且ACK成功");
                                                    log.info("{} 消费者: {}=>{}, 消息ID: {} 的消息消费且ACK应答成功", PENDING_HEAD, streamGroup, consumerName, recordId);
                                                }
                                            }
                                            messageDTO.setRetryCount(messageDTO.getRetryCount() + Constants.COMMON_CODE.ONE);
                                            if (!CharSequenceUtil.equalsAny(messageDTO.getStatus(), MessageStatusEnum.SUCCESS.getCode(), MessageStatusEnum.ACK_SUCCESS.getCode())
                                                    && messageDTO.getRetryCount() >= Constants.COMMON_CODE.THREE) {
                                                messageDTO.setStatus(MessageStatusEnum.FAIL.getCode());
                                                messageDTO.setRemark(CharSequenceUtil.format("已达最大重试次数({})", messageDTO.getRetryCount()));
                                            }
                                        }
                                    } else if (CharSequenceUtil.equals(messageDTO.getStatus(), MessageStatusEnum.SUCCESS.getCode()) && (redisService.acknowledge(streamGroup, record) > 0)) {
                                        messageDTO.setStatus(MessageStatusEnum.ACK_SUCCESS.getCode());
                                        messageDTO.setRemark("pending列表ACK成功");
                                        log.info("{} 消费者: {}=>{}, 消息ID: {} 的消息ACK应答成功", PENDING_HEAD, streamGroup, consumerName, recordId);
                                    }
                                    messageManager.updateMessage(messageDTO);
                                    log.info("{} 消费者: {}=>{}, 消息ID: {} 的消息处理完毕", PENDING_HEAD, streamGroup, consumerName, recordId);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}
