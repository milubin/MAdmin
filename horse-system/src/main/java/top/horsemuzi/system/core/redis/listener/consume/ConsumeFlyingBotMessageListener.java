package top.horsemuzi.system.core.redis.listener.consume;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.stereotype.Component;

/**
 * 飞书机器人消费者监听
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/04/11 16:39
 **/
@Slf4j
@Component
public class ConsumeFlyingBotMessageListener extends AbstractConsumeMessageListener {

    @Override
    public void onMessage(Message message, byte[] pattern) {
        log.info("pattern={}, message={}", new String(pattern), message.toString());
    }

}
