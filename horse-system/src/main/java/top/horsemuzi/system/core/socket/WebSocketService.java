package top.horsemuzi.system.core.socket;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.util.ExceptionUtil;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.system.manager.admin.NoticeManager;
import top.horsemuzi.system.manager.common.CommonManager;
import top.horsemuzi.system.pojo.dto.notice.NoticeSocketDTO;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * WebSocket服务
 *
 * @author mabin
 * @date 2022/10/10 11:48
 **/

@Slf4j
@Component
@RequiredArgsConstructor
@ServerEndpoint("/websocket/{token}")
public class WebSocketService {

    /**
     * 自动注入spring管理的单例bean为null的原因及解决办法
     * 原因:
     * spring维护的是单实例，websocket为多实例。每次请求建立链接的时候，都会生成一个新的bean。
     * 解决办法:
     * 让WebSocketService持有一个静态的bean对象，在创建时通过@Atuowire标注方法，自动注入值
     */
    private static NoticeManager noticeManager;
    private static CommonManager commonManager;

    @Autowired
    public void setNoticeManager(NoticeManager noticeManager, CommonManager commonManager) {
        WebSocketService.noticeManager = noticeManager;
        WebSocketService.commonManager = commonManager;
    }

    /**
     * concurrent包的线程安全Set，用来存放每个客户端对应的WebSocket对象。
     */
    private static CopyOnWriteArraySet<WebSocketService> webSocketSet = new CopyOnWriteArraySet<>();

    /**
     * 与客户端的连接会话，需要通过它来给客户端发送数据
     */
    private Session session;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 连接建立成功调用的方法
     *
     * @param session
     * @param token   token令牌
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("token") String token) {
        this.session = session;
        this.userId = getUserId(token);
        if (ObjectUtil.isNotNull(userId)) {
            webSocketSet.add(this);
            log.info("客户端【{}】建立WebSocket连接成功, 当前连接客户端总数: {}", userId, webSocketSet.size());

            // 推送当前用户的未读通知公告记录的数量
            List<NoticeSocketDTO> noticeSocketDTOList = noticeManager.getNoticeSocketList(userId);
            Long notReadCount = 0L;
            if (CollectionUtil.isNotEmpty(noticeSocketDTOList)) {
                notReadCount = noticeSocketDTOList.get(0).getNotReadCount();
            }
            sendTo(Convert.toStr(notReadCount), userId);
        }
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose() {
        webSocketSet.remove(this);
        log.info("客户端【{}】关闭WebSocket连接成功, 当前连接客户端总数: {}", this.userId, webSocketSet.size());
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String message, Session session, @PathParam("token") String token) {

    }


    /**
     * 发生错误时调用的方法
     *
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {
        log.error("websocket连接状态异常: {}", ExceptionUtil.getMessage(error));
    }

    /**
     * 新通知公告推送在线客户端
     *
     * @param message
     */
    public void sendNotice(String message) {
        if (StrUtil.isBlank(message)) {
            return;
        }
        List<NoticeSocketDTO> noticeSocketDTOList = JsonUtil.fromJson(message, new TypeReference<List<NoticeSocketDTO>>() {
        });
        if (CollectionUtil.isNotEmpty(noticeSocketDTOList)) {
            for (NoticeSocketDTO noticeSocketDTO : noticeSocketDTOList) {
                sendTo(Convert.toStr(noticeSocketDTO.getNotReadCount()), noticeSocketDTO.getUserId());
            }
        }
    }

    /**
     * 定点消息推送(推送给当前用户所在的客户端)
     */
    public void sendTo(String message, Long userId) {
        if (StrUtil.isBlank(message)) {
            message = StrUtil.EMPTY;
        }
        for (WebSocketService item : webSocketSet) {
            if (ObjectUtil.equal(item.userId, userId)) {
                log.info("服务端向客户端【{}】推送消息内容: {}", userId, message);
                try {
                    item.session.getBasicRemote().sendText(message);
                } catch (Exception e) {
                    log.error("服务器向客户端【{}】推送消息异常: {}", userId, e);
                }
                break;
            }
        }
    }


    /**
     * 获取当前连接客户端唯一标识(用户id)
     *
     * @param token
     * @return
     */
    private Long getUserId(String token) {
        // 解析token信息
        JwtUser cacheJwtUser = commonManager.getCacheJwtUser(token);
        if (ObjectUtil.isNotNull(cacheJwtUser)) {
            return cacheJwtUser.getId();
        }
        log.error("WebSocket连接解析用户信息失败, token={}", token);
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WebSocketService that = (WebSocketService) o;
        return Objects.equals(session, that.session) && Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(session, userId);
    }

}
