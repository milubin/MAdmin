package top.horsemuzi.system.annotation.aspect;

import com.plumelog.core.TraceId;
import com.plumelog.trace.aspect.AbstractAspect;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * plumeLog全局日志链路处理
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/04/14 15:00
 **/
@Aspect
@Component
public class PlumeLogAspect extends AbstractAspect {

    /**
     * plumeLog日志链路切面（全局添加traceId标记）
     *
     * @param joinPoint
     * @return java.lang.Object
     * @author mabin
     * @date 2023/4/14 14:29
     **/
    @Around("within(top.horsemuzi.system.controller..*)")
    public Object plumeLogHandle(ProceedingJoinPoint joinPoint) throws Throwable {
        // 手动添加traceId，但是好像会被MDC中的traceId覆盖
        TraceId.logTraceID.set(String.valueOf(TraceId.worker.nextId()));
        return aroundExecute(joinPoint);
    }

}
