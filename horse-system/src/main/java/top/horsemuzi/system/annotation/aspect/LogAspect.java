package top.horsemuzi.system.annotation.aspect;

import cn.hutool.core.annotation.AnnotationUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.Header;
import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartFile;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.common.util.*;
import top.horsemuzi.system.annotation.Log;
import top.horsemuzi.system.config.properties.SystemProperties;
import top.horsemuzi.system.core.redis.service.RedisService;
import top.horsemuzi.system.handle.event.spring.event.AsyncEvent;
import top.horsemuzi.system.manager.common.CommonManager;
import top.horsemuzi.system.pojo.dto.log.LoginLogDTO;
import top.horsemuzi.system.pojo.dto.log.SystemLogDTO;
import top.horsemuzi.system.pojo.dto.login.AdminLoginDTO;
import top.horsemuzi.system.pojo.dto.open.api.ApiParams;
import top.horsemuzi.system.pojo.dto.open.log.ApiLogDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 系统日志记录AOP处理实现
 *
 * @author mabin
 * @date 2022/06/30 20:19
 **/
@Aspect
@Component
@RequiredArgsConstructor
public class LogAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogAspect.class);

    private final CommonManager commonManager;
    private final ApplicationContext applicationContext;
    private final RedisService redisService;

    /**
     * 操作日志切点
     *
     * @param log
     */
    @Pointcut("@annotation(log)")
    public void logPoint(Log log) {
    }

    /**
     * 异常日志切点 (排除登录API、开放API接口)
     */
    @Pointcut("execution(* top.horsemuzi.system.controller.*..*.*(..)) "
            + "&& !execution(* top.horsemuzi.system.controller.admin.AdminLoginController.login(..)) "
            + "&& !execution(* top.horsemuzi.system.controller.api.ApiController.*(..)) ")
    public void exceptionPoint() {
    }

    /**
     * 登录日志切点
     */
    @Pointcut("execution(* top.horsemuzi.system.controller.admin.AdminLoginController.login(..))")
    public void loginLogPoint() {
    }

    /**
     * 开放API请求日志切点
     */
    @Pointcut("execution(* top.horsemuzi.system.controller.api.ApiController.*(..))")
    public void apiLogPoint() {
    }

    /**
     * 系统操作日志
     *
     * @param joinPoint
     * @param log
     * @return java.lang.Object
     * @author mabin
     * @date 2023/3/6 19:13
     **/
    @Around(value = "logPoint(log)", argNames = "joinPoint, log")
    public Object logHandle(ProceedingJoinPoint joinPoint, Log log) throws Throwable {
        HttpServletRequest request = NetUtil.getGlobalRequest();
        SystemLogDTO logDTO = new SystemLogDTO();
        logDTO.setTraceId(MDC.get(Constants.TRACE_ID));

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        Class<?> clazz = joinPoint.getTarget().getClass();

        String[] tags = AnnotationUtil.getAnnotationValue(clazz, Api.class, "tags");
        if (ArrayUtil.isNotEmpty(tags)) {
            logDTO.setModule(tags[0]);
        }
        if (StrUtil.isBlank(log.type())) {
            logDTO.setType(request.getMethod().toUpperCase());
        } else {
            logDTO.setType(log.type());
        }
        logDTO.setHttpType(request.getMethod());
        // 设置当前线程名称
        logDTO.setThreadName(Thread.currentThread().getName());
        ApiOperation operation = method.getAnnotation(ApiOperation.class);
        if (Objects.nonNull(operation) && StrUtil.isNotBlank(operation.notes())) {
            logDTO.setDescription(operation.notes());
        } else {
            logDTO.setDescription(operation.value());
        }
        JwtUser currentUser = commonManager.getCacheJwtUser(request);
        if (Objects.nonNull(currentUser)) {
            logDTO.setUserId(currentUser.getId());
            logDTO.setUsername(currentUser.getUsername());
        } else {
            logDTO.setUserId(SystemProperties.userId);
            logDTO.setUsername(SystemProperties.username);
        }
        logDTO.setMethod(StrUtil.concat(true, clazz.getName(), StrUtil.DOT, method.getName()));

        // 是否保存请求参数信息
        if (log.saveReqData()) {
            Map<String, String[]> parameterMap = request.getParameterMap();
            if (MapUtil.isNotEmpty(parameterMap)) {
                // 排除指定的请求参数信息
                if (ArrayUtil.isNotEmpty(log.excludeParamNames())) {
                    MapUtil.removeAny(parameterMap, log.excludeParamNames());
                }
                logDTO.setParams(JsonUtil.toJson(parameterMap));
            } else {
                logDTO.setParams(StrUtil.EMPTY_JSON);
            }
            Object[] args = joinPoint.getArgs();
            if (ArrayUtil.isNotEmpty(args)) {
                List<String> argList = new ArrayList<>();
                for (Object arg : args) {
                    if (ObjectUtil.isNotEmpty(arg) && !isFilterObject(arg)) {
                        argList.add(Convert.toStr(arg));
                    }
                }
                if (CollectionUtil.isNotEmpty(argList)) {
                    logDTO.setBody(JsonUtil.toJson(argList));
                }
            } else {
                logDTO.setBody(StrUtil.EMPTY_JSON);
            }
        }

        String ip = NetUtil.getRequestIp(request);
        logDTO.setIp(ip);
        logDTO.setArea(AddressUtil.offLineByIp(ip));
        UserAgent agent = getUserAgent(request);
        logDTO.setOs(agent.getOs().getName());
        logDTO.setBrowser(agent.getBrowser().getName() + " " + agent.getVersion());
        logDTO.setMobile(agent.isMobile() ? Constants.COMMON_JUDGMENT.ZONE_STR : Constants.COMMON_JUDGMENT.ONE_STR);
        String url = request.getRequestURL().toString();
        logDTO.setUrl(url);
        logDTO.setOrigin(StrUtil.contains(url, "/admin") ? Constants.COMMON_JUDGMENT.ZONE_STR : Constants.COMMON_JUDGMENT.ONE_STR);
        Object proceed = null;
        StopWatch watch = new StopWatch();
        watch.start();
        proceed = joinPoint.proceed();
        watch.stop();
        // 如果执行成功, 进行响应结果分析处理
        logDTO.setConsume(watch.getTotalTimeMillis());
        logDTO.setCode(ResultCodeEnum.SUCCESS.getCode());
        logDTO.setLogType(Constants.COMMON_CODE.ZONE);
        if (!log.binaryResData() && ObjectUtil.isNotNull(proceed)) {
            Result<?> result = (Result<?>) proceed;
            logDTO.setCode(result.getCode());
            // 是否保存响应信息
            if (log.saveResData() && ObjectUtil.isNotEmpty(result.getData())) {
                logDTO.setData(JsonUtil.toJson(result.getData()));
            }
            logDTO.setStatus(Constants.COMMON_JUDGMENT.ZONE_STR);
        }
        publishLogEvent(logDTO);
        return proceed;
    }

    /**
     * 系统操作异常日志
     *
     * @param joinPoint
     * @param throwable
     * @return void
     * @author mabin
     * @date 2023/3/6 19:13
     **/
    @AfterThrowing(pointcut = "exceptionPoint()", throwing = "throwable")
    public void exceptionLogHandle(JoinPoint joinPoint, Throwable throwable) {
        HttpServletRequest request = NetUtil.getGlobalRequest();
        SystemLogDTO logDTO = new SystemLogDTO();
        try {
            logDTO.setTraceId(MDC.get(Constants.TRACE_ID));
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();
            Method method = signature.getMethod();
            Class<?> clazz = joinPoint.getTarget().getClass();
            String[] tags = AnnotationUtil.getAnnotationValue(clazz, Api.class, "tags");
            if (ArrayUtil.isNotEmpty(tags)) {
                logDTO.setModule(tags[0]);
            }
            logDTO.setMethod(StrUtil.concat(true, clazz.getName(), StrUtil.DOT, method.getName()));
            logDTO.setType(Constants.METHOD.EXCEPTION);
            logDTO.setLogType(Constants.COMMON_JUDGMENT.ONE);
            logDTO.setHttpType(request.getMethod());
            logDTO.setThreadName(Thread.currentThread().getName());
            ApiOperation operation = method.getAnnotation(ApiOperation.class);
            if (Objects.nonNull(operation)) {
                logDTO.setDescription(operation.value());
            }
            JwtUser currentUser = commonManager.getCacheJwtUser(request);
            if (Objects.nonNull(currentUser)) {
                logDTO.setUserId(currentUser.getId());
                logDTO.setUsername(currentUser.getUsername());
            } else {
                logDTO.setUserId(SystemProperties.userId);
                logDTO.setUsername(SystemProperties.username);
            }

            Map<String, String[]> parameterMap = request.getParameterMap();
            if (MapUtil.isNotEmpty(parameterMap)) {
                if (method.isAnnotationPresent(Log.class)) {
                    Log log = method.getAnnotation(Log.class);
                    if (ArrayUtil.isNotEmpty(log.excludeParamNames())) {
                        MapUtil.removeAny(parameterMap, log.excludeParamNames());
                    }
                }
                logDTO.setParams(JsonUtil.toJson(parameterMap));
            } else {
                logDTO.setParams(StrUtil.EMPTY_JSON);
            }
            Object[] args = joinPoint.getArgs();
            if (ArrayUtil.isNotEmpty(args)) {
                List<String> argList = new ArrayList<>();
                for (Object arg : args) {
                    if (ObjectUtil.isNotEmpty(arg) && !isFilterObject(arg)) {
                        argList.add(Convert.toStr(arg));
                    }
                }
                if (CollectionUtil.isNotEmpty(argList)) {
                    logDTO.setBody(JsonUtil.toJson(argList));
                }
            } else {
                logDTO.setBody(StrUtil.EMPTY_JSON);
            }
            String ip = NetUtil.getRequestIp(request);
            logDTO.setIp(ip);
            logDTO.setArea(AddressUtil.offLineByIp(ip));
            UserAgent agent = getUserAgent(request);
            logDTO.setOs(agent.getOs().getName());
            logDTO.setBrowser(agent.getBrowser().getName() + " " + agent.getVersion());
            logDTO.setMobile(agent.isMobile() ? Constants.COMMON_JUDGMENT.ZONE_STR : Constants.COMMON_JUDGMENT.ONE_STR);
            String url = request.getRequestURL().toString();
            logDTO.setUrl(url);
            logDTO.setOrigin(StrUtil.contains(url, "/admin") ? Constants.COMMON_JUDGMENT.ZONE_STR : Constants.COMMON_JUDGMENT.ONE_STR);
            // 如果执行异常, 直接设置响应结果字段
            if (throwable instanceof BusinessException) {
                BusinessException businessException = (BusinessException) throwable;
                logDTO.setCode(businessException.getCode());
            } else {
                logDTO.setCode(ResultCodeEnum.INTERNAL_ERROR.getCode());
            }
            logDTO.setStatus(Constants.COMMON_JUDGMENT.ONE_STR);
            logDTO.setExcName(throwable.getClass().getCanonicalName());
            logDTO.setExcMessage(StrUtil.sub(ExceptionUtil.getMessage(throwable), Constants.COMMON_CODE.ZONE, Constants.COMMON_CODE.FIVE_THOUSAND));
            publishLogEvent(logDTO);
        } catch (Exception e) {
            LOGGER.error(ExceptionUtil.getMessage(e));
        }
    }

    /**
     * 系统登录日志
     *
     * @param joinPoint
     * @return java.lang.Object
     * @author mabin
     * @date 2023/3/6 19:14
     **/
    @Around(value = "loginLogPoint()")
    public Object loginLogHandle(ProceedingJoinPoint joinPoint) throws Throwable {
        LoginLogDTO loginLogDTO = new LoginLogDTO();
        loginLogDTO.setTraceId(MDC.get(Constants.TRACE_ID));
        loginLogDTO.setThreadName(Thread.currentThread().getName());

        Object[] args = joinPoint.getArgs();
        if (ArrayUtil.isNotEmpty(args)) {
            for (Object arg : args) {
                if (ObjectUtil.isNotEmpty(arg) && !isFilterObject(arg)) {
                    if (arg instanceof AdminLoginDTO) {
                        AdminLoginDTO adminLoginDTO = (AdminLoginDTO) arg;
                        loginLogDTO.setUsername(adminLoginDTO.getUsername());
                        // 设置登录方式
                        loginLogDTO.setLoginWay(adminLoginDTO.getType());
                    }
                }
            }
            loginLogDTO.setBody(JsonUtil.toJson(args));
        } else {
            loginLogDTO.setBody(StrUtil.EMPTY_JSON);
        }

        HttpServletRequest request = NetUtil.getGlobalRequest();
        String ip = NetUtil.getRequestIp(request);
        loginLogDTO.setIp(ip);
        loginLogDTO.setArea(AddressUtil.offLineByIp(ip));
        UserAgent agent = getUserAgent(request);
        loginLogDTO.setOs(agent.getOs().getName());
        loginLogDTO.setBrowser(agent.getBrowser().getName() + " " + agent.getVersion());
        loginLogDTO.setMobile(agent.isMobile() ? Constants.COMMON_JUDGMENT.ZONE_STR : Constants.COMMON_JUDGMENT.ONE_STR);
        loginLogDTO.setOrigin(StrUtil.contains(request.getRequestURI(), "/admin") ? Constants.COMMON_JUDGMENT.ZONE_STR : Constants.COMMON_JUDGMENT.ONE_STR);
        loginLogDTO.setRemark("登录成功");
        Object proceed = null;
        try {
            proceed = joinPoint.proceed();
        } catch (Throwable throwable) {
            if (throwable instanceof BusinessException) {
                BusinessException businessException = (BusinessException) throwable;
                loginLogDTO.setRemark(businessException.getMessage());
            }
            loginLogDTO.setStatus(Constants.COMMON_JUDGMENT.ONE_STR);
            loginLogDTO.setExcName(throwable.getClass().getCanonicalName());
            loginLogDTO.setExcMessage(StrUtil.sub(ExceptionUtil.getMessage(throwable), Constants.COMMON_CODE.ZONE, Constants.COMMON_CODE.FIVE_THOUSAND));
            publishLogEvent(loginLogDTO);
            // 重新抛出异常
            throw throwable;
        }
        publishLogEvent(loginLogDTO);
        return proceed;
    }

    /**
     * 系统API平台访问日志
     *
     * @param joinPoint
     * @return java.lang.Object
     * @author mabin
     * @date 2023/3/6 19:14
     **/
    @Around(value = "apiLogPoint()")
    public Object apiLogHandle(ProceedingJoinPoint joinPoint) throws Throwable {
        StopWatch watch = new StopWatch();
        ApiLogDTO apiLogDTO = new ApiLogDTO();
        try {
            apiLogDTO.setTraceId(MDC.get(Constants.TRACE_ID));
            Object[] args = joinPoint.getArgs();
            if (ArrayUtil.isNotEmpty(args)) {
                for (Object arg : args) {
                    if (arg instanceof ApiParams) {
                        ApiParams apiParams = (ApiParams) arg;
                        apiLogDTO.setAppKey(apiParams.getAppKey());
                        apiLogDTO.setApiCode(apiParams.getApiCode());
                        apiLogDTO.setBody(JsonUtil.toJson(apiParams));
                    }
                }
            }

            // 只要AOP拦截到开放平台的请求, 只要存在appKey和apiCode两个值, 无论成功/异常, 均视为一次请求
            if (StrUtil.isAllNotBlank(apiLogDTO.getAppKey(), apiLogDTO.getApiCode())) {
                String cacheKey = Constants.REDIS_PREFIX.OPEN_API_DAILY_LIMIT + StrUtil.COLON +
                        EncryptUtil.md5(apiLogDTO.getAppKey() + apiLogDTO.getApiCode() + DateUtil.formatDate(new Date()));
                // 初始化请求次数为0, 有效期: 25h
                if (!redisService.hasKey(cacheKey)) {
                    redisService.set(cacheKey, Constants.COMMON_CODE.STR_ZONE, 25L, TimeUnit.HOURS);
                }
                redisService.increment(cacheKey);
            }

            HttpServletRequest request = NetUtil.getGlobalRequest();
            String ip = NetUtil.getRequestIp(request);
            apiLogDTO.setIp(ip);
            apiLogDTO.setArea(AddressUtil.offLineByIp(ip));
            UserAgent agent = getUserAgent(request);
            apiLogDTO.setOs(agent.getOs().getName());
            apiLogDTO.setBrowser(agent.getBrowser().getName() + " " + agent.getVersion());
            watch.start();
            Object proceed = joinPoint.proceed();
            watch.stop();
            apiLogDTO.setConsume(watch.getTotalTimeMillis());
            if (ObjectUtil.isNotNull(proceed)) {
                Result<?> result = (Result<?>) proceed;
                apiLogDTO.setCode(result.getCode());
                if (ObjectUtil.isNotEmpty(result.getData())) {
                    apiLogDTO.setData(JsonUtil.toJson(result.getData()));
                }
                apiLogDTO.setStatus(Constants.COMMON_JUDGMENT.ZONE_STR);
            }
            apiLogDTO.setRemark("请求成功");
            publishLogEvent(apiLogDTO);
            return proceed;
        } catch (Exception e) {
            watch.stop();
            apiLogDTO.setConsume(watch.getTotalTimeMillis());
            apiLogDTO.setStatus(Constants.COMMON_JUDGMENT.ONE_STR);
            apiLogDTO.setRemark("请求失败");
            if (e instanceof BusinessException) {
                BusinessException businessException = (BusinessException) e;
                apiLogDTO.setCode(businessException.getCode());
                apiLogDTO.setExcName(businessException.getClass().getCanonicalName());
                apiLogDTO.setExcMessage(ExceptionUtil.getMessage(e));
                publishLogEvent(apiLogDTO);
                throw businessException;
            } else {
                apiLogDTO.setCode(ResultCodeEnum.API_SERVER_ERROR.getCode());
                apiLogDTO.setExcName(e.getClass().getCanonicalName());
                apiLogDTO.setExcMessage(StrUtil.sub(ExceptionUtil.getMessage(e), Constants.COMMON_CODE.ZONE, Constants.COMMON_CODE.FIVE_THOUSAND));
                publishLogEvent(apiLogDTO);
                throw e;
            }
        }
    }

    /**
     * 发布日志监听事件
     *
     * @param logDTO
     * @return void
     * @author mabin
     * @date 2023/3/6 19:15
     **/
    private void publishLogEvent(Object logDTO) {
        NetUtil.setRequestInheritable();
        applicationContext.publishEvent(new AsyncEvent(logDTO));
    }

    /**
     * 判断请求参数是否是需要被过滤的对象
     *
     * @param object
     * @return
     */
    @SuppressWarnings("rawtypes")
    private boolean isFilterObject(final Object object) {
        Class<?> clazz = object.getClass();
        if (clazz.isArray()) {
            return clazz.getComponentType().isAssignableFrom(MultipartFile.class);
        } else if (Collection.class.isAssignableFrom(MultipartFile.class)) {
            Collection collection = (Collection) object;
            for (Object value : collection) {
                return value instanceof MultipartFile;
            }
        } else if (Map.class.isAssignableFrom(clazz)) {
            Map map = (Map) object;
            for (Object value : map.entrySet()) {
                Map.Entry entry = (Map.Entry) value;
                return entry.getValue() instanceof MultipartFile;
            }
        }
        return object instanceof MultipartFile || object instanceof HttpServletRequest || object instanceof HttpServletResponse || object instanceof BindingResult;
    }

    /**
     * 获取请求代理对象实例
     *
     * @param request
     * @return cn.hutool.http.useragent.UserAgent
     * @author mabin
     * @date 2023/3/8 16:00
     **/
    private UserAgent getUserAgent(HttpServletRequest request) {
        return UserAgentUtil.parse(request.getHeader(Header.USER_AGENT.getValue()));
    }

}
