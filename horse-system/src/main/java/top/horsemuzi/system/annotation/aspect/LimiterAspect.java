package top.horsemuzi.system.annotation.aspect;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.redisson.api.RRateLimiter;
import org.redisson.api.RateType;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.NetUtil;
import top.horsemuzi.common.util.SpelUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.annotation.Limiter;
import top.horsemuzi.system.manager.common.CommonManager;

import java.lang.reflect.Method;

/**
 * 基于Redisson的分布式限流器
 *
 * @author mabin
 * @date 2022/06/18 14:35
 **/
@Slf4j
@Component
@Aspect
@RequiredArgsConstructor
public class LimiterAspect {

    private final RedissonClient redissonClient;
    private final CommonManager commonManager;

    @Pointcut("@annotation(limiter)")
    public void limiterPoint(Limiter limiter) {
    }

    @Around(value = "limiterPoint(limiter)", argNames = "joinPoint, limiter")
    public Object limiterHandle(ProceedingJoinPoint joinPoint, Limiter limiter) throws Throwable {
        // 获取限流key
        String limitKey = buildLimitKey(joinPoint, limiter);
        // 限流处理
        RRateLimiter rateLimiter = redissonClient.getRateLimiter(limitKey);
        rateLimiter.trySetRate(RateType.OVERALL, limiter.rate(), limiter.interval(), limiter.unit());
        if (!rateLimiter.tryAcquire()) {
            if (StrUtil.isNotBlank(limiter.message())) {
                // 响应自定义错误消息
                throw new BusinessException(ResultCodeEnum.BEYOND_LIMIT_ERROR.getCode(), limiter.message());
            }
            throw new BusinessException(ResultCodeEnum.BEYOND_LIMIT_ERROR);
        }
        return joinPoint.proceed();
    }

    /**
     * 构建限流key
     *
     * @param joinPoint
     * @param limiter
     * @return
     */
    private String buildLimitKey(ProceedingJoinPoint joinPoint, Limiter limiter) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        String classMethod = method + joinPoint.getTarget().getClass().getName();
        String tempKey = StrUtil.EMPTY;
        switch (limiter.type()) {
            case IP:
                tempKey = NetUtil.getRequestIp();
                break;
            case UID:
                JwtUser jwtUser = commonManager.getCacheJwtUser();
                ValidatorUtil.notEmpty(jwtUser, ResultCodeEnum.NOT_LOGIN);
                tempKey = Convert.toStr(jwtUser.getId());
                break;
            case SPEL:
                tempKey = SpelUtil.parse(method, joinPoint.getArgs(), limiter.key(), String.class);
                break;
            case KEY:
                tempKey = limiter.key();
                break;
            default:
        }
        return SecureUtil.md5(classMethod + tempKey);
    }

}
