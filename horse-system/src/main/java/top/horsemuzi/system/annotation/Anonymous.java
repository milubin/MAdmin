package top.horsemuzi.system.annotation;

import java.lang.annotation.*;

/**
 * 匿名访问不鉴权注解 (主要用于拦截器鉴权处理)
 *
 * @author mabin
 * @date 2022/09/30 09:55
 **/
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Anonymous {

    /**
     * 是否需要签名校验，默认需要
     *
     * @return
     */
    boolean sign() default true;

}
