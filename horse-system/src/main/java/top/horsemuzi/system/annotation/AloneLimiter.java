package top.horsemuzi.system.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description 基于AOP+Guava的单机限流器
 * @date 2022/5/15 16:43
 */
@Target(ElementType.METHOD)  // 注解放置的目标位置,METHOD是可注解在方法级别上
@Retention(RetentionPolicy.RUNTIME)  // 注解在哪个阶段执行
@Documented
public @interface AloneLimiter {

    /**
     * 每秒向桶中放入的令牌数, 默认最大值(即: 默认不限流)
     * 实际测试中，每秒能够放入桶中的令牌数是 perSecond 值的二倍
     *
     * @return
     */
    double perSecond() default Double.MAX_VALUE;

    /**
     * 获取令牌的等待时间, 默认: 0
     *
     * @return
     */
    int timeOut() default 0;

    /**
     * 超时时间单位: 毫秒
     *
     * @return
     */
    TimeUnit unit() default TimeUnit.MILLISECONDS;
}
