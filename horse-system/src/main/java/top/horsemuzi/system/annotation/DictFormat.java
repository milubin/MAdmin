package top.horsemuzi.system.annotation;

import java.lang.annotation.*;

/**
 * 系统数据字典格式化
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface DictFormat {

    /**
     * 字典类型，例如：sys_sex
     *
     * @return
     */
    String type() default "";

    /**
     * 自定义扩展表达式，例如：1=男,0=女,2=外星人 (需严格控制扩展表达式格式)
     *
     * @return
     */
    String convertExp() default "";

}
