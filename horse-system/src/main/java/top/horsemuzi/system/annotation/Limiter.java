package top.horsemuzi.system.annotation;

import cn.hutool.core.util.StrUtil;
import org.redisson.api.RateIntervalUnit;
import top.horsemuzi.common.enums.LimiterTypeEnum;

import java.lang.annotation.*;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description 基于AOP+Redisson的分布式限流器(默认限流逻辑: 每秒内不超过10次)
 * @date 2022/5/15 16:43
 */
@Target(ElementType.METHOD)  // 注解放置的目标位置,METHOD是可注解在方法级别上
@Retention(RetentionPolicy.RUNTIME)  // 注解在哪个阶段执行
@Documented
public @interface Limiter {

    /**
     * 限流的枚举类型, 默认针对指定的key进行限流
     *
     * @return
     */
    LimiterTypeEnum type() default LimiterTypeEnum.KEY;

    /**
     * key值支持SPEL动态参数表达式, 如果动态表达式解析的值为空, 则默认为全局限流器
     * 全局限流或者自定义限流由key值动态决定
     *
     * @return
     */
    String key() default "redisson";

    /**
     * 限制次数(默认: 10次)
     *
     * @return
     */
    long rate() default 10;

    /**
     * 限制时间间隔(默认: 1s)
     *
     * @return
     */
    long interval() default 1;

    /**
     * 时间单位(默认: s)
     *
     * @return
     */
    RateIntervalUnit unit() default RateIntervalUnit.SECONDS;

    /**
     * 限流异常响应信息
     *
     * @return
     */
    String message() default StrUtil.EMPTY;
}
