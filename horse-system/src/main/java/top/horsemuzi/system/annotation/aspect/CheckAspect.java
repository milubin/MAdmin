package top.horsemuzi.system.annotation.aspect;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.CheckModeEnum;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.annotation.Anonymous;
import top.horsemuzi.system.annotation.Check;
import top.horsemuzi.system.manager.common.CommonManager;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description 权限校验AOP切面处理
 * @date 2022/5/15 16:53
 */
@Aspect
@Slf4j
@Component
@RequiredArgsConstructor
public class CheckAspect {

    private final CommonManager commonManager;

    @Pointcut(value = "@annotation(top.horsemuzi.system.annotation.Check)")
    private void check() {
    }

    /**
     * 注解优先级: @Anonymous > @Check
     *
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    @Around(value = "check()")
    public Object checkHandle(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();

        // 如果存在匿名访问注解但是仍需要请求验签，则默认用户已登录，自动进行 JwtUser 参数赋值
        JwtUser jwtUser = null;
        Anonymous anonymous = method.getAnnotation(Anonymous.class);
        if (ObjectUtil.isNotNull(anonymous)) {
            Object[] args = joinPoint.getArgs();
            if (anonymous.sign()) {
                // 为JwtUser jwtUser参数赋值
                if (ArrayUtil.isNotEmpty(args)) {
                    for (int i = 0; i < args.length; i++) {
                        if (ObjectUtil.isNotNull(args[i]) && ObjectUtil.equals(JwtUser.class, args[i].getClass())) {
                            if (ObjectUtil.isNull(jwtUser)) {
                                jwtUser = commonManager.getCacheJwtUser();
                                ValidatorUtil.notEmpty(jwtUser, ResultCodeEnum.NOT_LOGIN);
                            }
                            args[i] = jwtUser;
                        }
                    }
                }
            }
            return joinPoint.proceed(args);
        } else {
            String className = joinPoint.getTarget().getClass().getName();
            String methodName = method.getName();
            log.info("[@Around] check start processing [{}] annotation", className.concat(StrUtil.DOT).concat(methodName));
            Check check = method.getAnnotation(Check.class);

            // 校验role属性和orRole属性不能同时出现
            ValidatorUtil.isTrue(!(ArrayUtil.isNotEmpty(check.role()) && ArrayUtil.isNotEmpty(check.orRole())),
                    ResultCodeEnum.CHECK_ANNOTATION_SETTING_ERROR);
            // 校验是否登录: 查询redis中是否存在用户的token缓存数据
            if (check.login()) {
                jwtUser = commonManager.getCacheJwtUser();
                ValidatorUtil.notEmpty(jwtUser, ResultCodeEnum.NOT_LOGIN);
            }
            // 校验用户角色
            String[] roles = check.role();
            if (ArrayUtil.isNotEmpty(roles)) {
                if (ObjectUtil.isNull(jwtUser)) {
                    jwtUser = commonManager.getCacheJwtUser();
                    ValidatorUtil.notEmpty(jwtUser, ResultCodeEnum.NOT_LOGIN);
                }
                Set<String> cacheRoles = jwtUser.getRoles();
                ValidatorUtil.notEmpty(cacheRoles, ResultCodeEnum.NOT_ROLE_ERROR);
                // 角色模式(管理员角色标识直接放行)
                if (!cacheRoles.contains(Constants.ROLE_CODE.ADMIN)) {
                    Set<String> settingRoles = new HashSet<>(Arrays.asList(roles));
                    ValidatorUtil.isTrue(ObjectUtil.equal(CheckModeEnum.OR, check.roleMode())
                            ? CollectionUtil.containsAny(jwtUser.getRoles(), settingRoles)
                            : CollectionUtil.containsAll(jwtUser.getRoles(), settingRoles), ResultCodeEnum.NOT_ROLE_ERROR);
                }
            }
            // 校验用户权限
            String[] permissions = check.permissions();
            if (ArrayUtil.isNotEmpty(permissions)) {
                if (ObjectUtil.isNull(jwtUser)) {
                    jwtUser = commonManager.getCacheJwtUser();
                    ValidatorUtil.notEmpty(jwtUser, ResultCodeEnum.NOT_LOGIN);
                }
                Set<String> cachePermissions = jwtUser.getPermissions();
                ValidatorUtil.notEmpty(cachePermissions, ResultCodeEnum.NOT_PERMISSION_ERROR);
                // 权限模式(管理员权限标识直接放行)
                if (!cachePermissions.contains(Constants.ADMIN_PERMISSION)) {
                    try {
                        Set<String> settingPermissions = new HashSet<>(Arrays.asList(permissions));
                        ValidatorUtil.isTrue(ObjectUtil.equals(CheckModeEnum.OR, check.permissionMode())
                                        ? CollectionUtil.containsAny(cachePermissions, settingPermissions)
                                        : CollectionUtil.containsAll(cachePermissions, settingPermissions),
                                ResultCodeEnum.NOT_PERMISSION_ERROR);
                    } catch (Exception e) {
                        // 权限校验未通过后, 继续判断是否配置 orRole 属性
                        if (e instanceof BusinessException) {
                            String[] orRoles = check.orRole();
                            if (ArrayUtil.isNotEmpty(orRoles)) {
                                HashSet<String> settingOrRoles = new HashSet<>(Arrays.asList(orRoles));
                                ValidatorUtil.isTrue(ObjectUtil.equal(CheckModeEnum.OR, check.roleMode())
                                                ? CollectionUtil.containsAny(jwtUser.getRoles(), settingOrRoles)
                                                : CollectionUtil.containsAll(jwtUser.getRoles(), settingOrRoles),
                                        ResultCodeEnum.NOT_ROLE_ERROR);
                            } else {
                                throw e;
                            }
                        } else {
                            throw e;
                        }
                    }
                }
            }
            // 为JwtUser jwtUser参数赋值
            Object[] args = joinPoint.getArgs();
            if (ArrayUtil.isNotEmpty(args)) {
                for (int i = 0; i < args.length; i++) {
                    if (ObjectUtil.isNotNull(args[i]) && ObjectUtil.equals(JwtUser.class, args[i].getClass())) {
                        if (ObjectUtil.isNull(jwtUser)) {
                            jwtUser = commonManager.getCacheJwtUser();
                            ValidatorUtil.notEmpty(jwtUser, ResultCodeEnum.NOT_LOGIN);
                        }
                        args[i] = jwtUser;
                    }
                }
            }
            return joinPoint.proceed(args);
        }
    }

}
