package top.horsemuzi.system.annotation;

import cn.hutool.core.util.StrUtil;

import java.lang.annotation.*;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description 基于AOP+Redisson的分布式锁
 * @date 2022/5/15 16:43
 */
@Target(ElementType.METHOD)  // 注解放置的目标位置,METHOD是可注解在方法级别上
@Retention(RetentionPolicy.RUNTIME)  // 注解在哪个阶段执行
@Documented
public @interface Idempotent {


    /**
     * 加锁时间(默认5s)
     *
     * @return
     */
    int lockTime() default 5;

    /**
     * 自定义响应信息
     *
     * @return
     */
    String message() default StrUtil.EMPTY;

}
