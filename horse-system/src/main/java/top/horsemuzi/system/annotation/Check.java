package top.horsemuzi.system.annotation;

import top.horsemuzi.common.enums.CheckModeEnum;

import java.lang.annotation.*;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description 权限校验注解
 * @date 2022/5/15 16:43
 */
@Target(ElementType.METHOD)  // 注解放置的目标位置,METHOD是可注解在方法级别上
@Retention(RetentionPolicy.RUNTIME)  // 注解在哪个阶段执行
@Documented
public @interface Check {

    /**
     * 校验是否登录, 默认 login=true 校验
     *
     * @return
     */
    boolean login() default true;

    /**
     * 权限校验模式, 只针对于permissions有效 默认: CheckModeEnum.ADD
     *
     * @return
     */
    CheckModeEnum permissionMode() default CheckModeEnum.ADD;

    /**
     * 校验用户是否有指定权限, 默认不校验
     *
     * @return
     */
    String[] permissions() default {};

    /**
     * 校验用户是否有指定角色, 默认不校验
     *
     * @return
     */
    String[] role() default {};

    /**
     * 配置permissions权限使用, 表示: 如果没有配置的permissions权限, 但是具有此角色标识也可通过校验
     *
     * @return
     */
    String[] orRole() default {};

    /**
     * 角色校验模式, 只针对于role有效, 默认: CheckModeEnum.ADD
     * @return
     */
    CheckModeEnum roleMode() default CheckModeEnum.ADD;

}
