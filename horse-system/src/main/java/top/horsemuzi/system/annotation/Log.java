package top.horsemuzi.system.annotation;

import java.lang.annotation.*;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description TYPE - 类，接口（包括注解类型）或枚举
 * FIELD - 字段（包括枚举常量）
 * METHOD - 方法
 * PARAMETER - 参数
 * CONSTRUCTOR - 构造函数
 * LOCAL_VARIABLE - 局部变量
 * ANNOTATION_TYPE -注解类型
 * PACKAGE - 包
 * TYPE_PARAMETER - 类型参数
 * TYPE_USE - 使用类型
 * @date 2022/5/15 16:43
 */
@Target({ElementType.METHOD})  // 注解放置的目标位置
@Retention(RetentionPolicy.RUNTIME)  // 注解在哪个阶段执行
@Documented
public @interface Log {

    /**
     * 操作类型
     *
     * @return
     */
    String type() default "";

    /**
     * 是否二进制响应数据（例如：文件预览、下载、导出等操作，便于标记是否记录响应code等相关信息）
     *
     * @return
     */
    boolean binaryResData() default false;

    /**
     * 是否保存请求参数数据 (包括params和body)
     *
     * @return
     */
    boolean saveReqData() default true;

    /**
     * 是否保存响应数据(Result.data)
     *
     * @return
     */
    boolean saveResData() default true;

    /**
     * 排除指定的请求参数
     */
    String[] excludeParamNames() default {};

}
