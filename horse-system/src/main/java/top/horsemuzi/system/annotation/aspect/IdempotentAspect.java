package top.horsemuzi.system.annotation.aspect;

import cn.hutool.core.util.StrUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.EncryptUtil;
import top.horsemuzi.common.util.NetUtil;
import top.horsemuzi.system.annotation.Idempotent;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.TimeUnit;

/**
 * 基于AOP+Redisson的分布式锁
 *
 * @author mabin
 * @date 2022/06/17 17:42
 **/
@Slf4j
@Aspect
@Component
@RequiredArgsConstructor
public class IdempotentAspect {

    private final RedissonClient redissonClient;

    @Pointcut("@annotation(idempotent)")
    public void idempotentPoint(Idempotent idempotent) {
    }

    @Around(value = "idempotentPoint(idempotent)", argNames = "joinPoint, idempotent")
    public Object idempotentHandle(ProceedingJoinPoint joinPoint, Idempotent idempotent) throws Throwable {

        // 设置加锁的唯一标识(默认: MD5(全路径方法+token+ip))
        String className = joinPoint.getTarget().getClass().getName();
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        String methodName = signature.getMethod().getName();

        HttpServletRequest request = NetUtil.getGlobalRequest();
        String token = request.getHeader(Constants.ACCESS_TOKEN);
        String ip = NetUtil.getRequestIp(request);
        String key = EncryptUtil.md5(StrUtil.concat(true, className, methodName, token, ip));

        // 请求加解锁
        RLock lock = redissonClient.getLock(key);
        if (lock.tryLock(Constants.COMMON_CODE.BASE_LONG_ONE, idempotent.lockTime(), TimeUnit.SECONDS)) {
            log.info("redisson lock success! key: {}", key);
            try {
                return joinPoint.proceed();
            } finally {
                lock.unlock();
                log.info("redisson unlock success! key: {}", key);
            }
        } else {
            log.error("redisson lock fail! key: {}", key);
            String message = idempotent.message();
            if (StrUtil.isNotBlank(message)) {
                throw new BusinessException(ResultCodeEnum.REPEATED_SUBMIT_ERROR.getCode(), message);
            }
            throw new BusinessException(ResultCodeEnum.REPEATED_SUBMIT_ERROR);
        }
    }

}
