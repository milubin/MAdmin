package top.horsemuzi.system.annotation.aspect;

import cn.hutool.core.util.StrUtil;
import com.google.common.util.concurrent.RateLimiter;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.system.annotation.AloneLimiter;

import java.lang.reflect.Method;

/**
 * 单机限流AOP实现
 *
 * @author mabin
 * @date 2022/07/12 09:15
 **/
@Slf4j
@Component
@Aspect
public class AloneLimiterAspect {

    private RateLimiter rateLimiter = RateLimiter.create(Double.MAX_VALUE);

    @Pointcut("@annotation(aloneLimiter)")
    public void aloneLimiterPoint(AloneLimiter aloneLimiter) {
    }

    @Around(value = "aloneLimiterPoint(aloneLimiter)", argNames = "joinPoint, aloneLimiter")
    public Object limiterHandle(ProceedingJoinPoint joinPoint, AloneLimiter aloneLimiter) throws Throwable {
        rateLimiter.setRate(aloneLimiter.perSecond());
        if (!rateLimiter.tryAcquire(aloneLimiter.timeOut(), aloneLimiter.unit())) {
            throw new BusinessException(ResultCodeEnum.BEYOND_LIMIT_ERROR);
        }
        return joinPoint.proceed();
    }

}
