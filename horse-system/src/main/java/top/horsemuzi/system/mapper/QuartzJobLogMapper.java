package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.dto.statistics.LineChartDTO;
import top.horsemuzi.system.pojo.dto.statistics.PieChartDTO;
import top.horsemuzi.system.pojo.dto.statistics.SourceLineData;
import top.horsemuzi.system.pojo.entity.QuartzJobLog;

import java.util.List;

/**
 * <p>
 * Quartz任务调度日志信息表 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-08-21 16:05:01
 */
@Mapper
public interface QuartzJobLogMapper extends ExpandMapper<QuartzJobLog> {

    /**
     * 获取Quartz任务调度日志饼图统计数据
     *
     * @return
     */
    List<PieChartDTO> statisticsPieChartData();

    /**
     * 获取Quartz任务调度日志折线图统计数据
     *
     * @return
     */
    List<SourceLineData> statisticsLineChartData();
}
