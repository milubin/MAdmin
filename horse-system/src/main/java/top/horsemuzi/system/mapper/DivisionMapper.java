package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.division.DivisionDTO;
import top.horsemuzi.system.pojo.dto.division.DivisionQuery;
import top.horsemuzi.system.pojo.entity.Division;

import java.util.List;

/**
 * <p>
 * 国家行政区划信息表 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-06 10:31:24
 */
@Mapper
public interface DivisionMapper extends ExpandMapper<Division> {

    /**
     * 区划名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> fetch(@Param("search") String search);
}
