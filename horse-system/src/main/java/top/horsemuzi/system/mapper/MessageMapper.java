package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.dto.statistics.PieChartDTO;
import top.horsemuzi.system.pojo.entity.Message;

import java.util.List;

/**
 * <p>
 * 消息信息表 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-09-30 10:59:09
 */
@Mapper
public interface MessageMapper extends ExpandMapper<Message> {

    /**
     * 根据消息状态统计饼图
     *
     * @return
     */
    List<PieChartDTO> statisticsMessageByStatus();

    /**
     * 根据消息渠道统计饼图
     *
     * @return
     */
    List<PieChartDTO> statisticsMessageByChannel();
}
