package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.statistics.PieChartDTO;
import top.horsemuzi.system.pojo.entity.LoginLog;

import java.util.List;

/**
 * <p>
 * 系统登录日志记录表 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-09-26 21:50:38
 */
@Mapper
public interface LoginLogMapper extends ExpandMapper<LoginLog> {

    /**
     * 获取登录日志成功失败饼图
     *
     * @return
     */
    List<PieChartDTO> statisticsBasePieChartData();

    /**
     * 获取登录日志隐藏原因饼图
     *
     * @return
     */
    List<PieChartDTO> statisticsPieChartData();

    /**
     * 登录日志链路ID实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> loginFetch(@Param("search") String search);
}
