package top.horsemuzi.system.mapper;

import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.entity.DictData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 字典数据表 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-06-20 11:04:36
 */
@Mapper
public interface DictDataMapper extends ExpandMapper<DictData> {

}
