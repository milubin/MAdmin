package top.horsemuzi.system.mapper;

import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.entity.AppCategoryRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 开放API应用和接口类型关联信息 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-18 22:09:28
 */
@Mapper
public interface AppCategoryRelationMapper extends ExpandMapper<AppCategoryRelation> {

}
