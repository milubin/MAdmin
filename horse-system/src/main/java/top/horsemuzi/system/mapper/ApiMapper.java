package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.open.api.ApiExportDTO;
import top.horsemuzi.system.pojo.dto.open.api.ApiQuery;
import top.horsemuzi.system.pojo.entity.Api;

import java.util.List;

/**
 * <p>
 * 开放API应用接口信息 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-18 22:09:28
 */
@Mapper
public interface ApiMapper extends ExpandMapper<Api> {

    /**
     * 接口名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> fetch(@Param("search") String search);

    /**
     * API接口数据导出
     *
     * @param query
     * @return
     */
    List<ApiExportDTO> getApiExportDTOList(@Param("query") ApiQuery query);

}
