package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.log.EchartDTO;
import top.horsemuzi.system.pojo.entity.SystemLog;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统操作日志 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-06-30 20:35:31
 */
@Mapper
public interface SystemLogMapper extends ExpandMapper<SystemLog> {

    /**
     * 按日期(7天)统计分析展示生成日志的条数
     *
     * @return
     */
    List<EchartDTO> statisticsLogGroupDateCount();

    /**
     * 按日志类型（操作、异常）统计分析各自的日志条数
     *
     * @return
     */
    List<Map<String, Object>> statisticsLogGroupTypeCount();

    /**
     * 操作日志链路ID实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> logFetch(@Param("search") String search);
}
