package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Param;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.permission.PermissionDTO;
import top.horsemuzi.system.pojo.dto.permission.PermissionQuery;
import top.horsemuzi.system.pojo.entity.Permission;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 菜单权限表 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-07 22:44:27
 */
@Mapper
public interface PermissionMapper extends ExpandMapper<Permission> {

    /**
     * 根据用户id获取用户菜单权限信息
     *
     * @param userId
     * @return
     */
    Set<String> getPerms(@Param("userId") Long userId);

    /**
     * 根据用户信息获取菜单权限路由
     *
     * @param jwtUser
     * @return
     */
    List<PermissionDTO> getPermissionTree(@Param("jwtUser") JwtUser jwtUser);

    /**
     * 获取条件下权限菜单列表(权限菜单管理)
     *
     * @param query
     * @return
     */
    List<PermissionDTO> getPermissionMenus(@Param("query") PermissionQuery query);

    /**
     * 根据角色id获取权限菜单数据集合
     *
     * @param roleId
     * @return
     */
    List<PermissionDTO> getPermissionMenusByRoleId(@Param("roleId") Long roleId);

    /**
     * 菜单名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> fetch(@Param("search") String search);
}
