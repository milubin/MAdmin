package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.entity.User;

import java.util.List;

/**
 * <p>
 * 管理用户表 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-07 22:44:27
 */
@Mapper
public interface UserMapper extends ExpandMapper<User> {

    /**
     * 用户名输入框实时检索
     *
     * @param search
     * @param filedType
     * @return
     */
    List<InputRemoteFetchDTO> fetch(@Param("search") String search, @Param("filedType") String filedType);
}
