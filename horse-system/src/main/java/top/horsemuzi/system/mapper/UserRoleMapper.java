package top.horsemuzi.system.mapper;

import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-07 22:44:28
 */
@Mapper
public interface UserRoleMapper extends ExpandMapper<UserRole> {

}
