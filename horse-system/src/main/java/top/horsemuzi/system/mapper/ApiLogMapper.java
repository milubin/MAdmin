package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.entity.ApiLog;

import java.util.List;

/**
 * <p>
 * 开放API请求日志 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-21 22:26:38
 */
@Mapper
public interface ApiLogMapper extends ExpandMapper<ApiLog> {

    /**
     * API日志链路ID实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> logFetch(@Param("search") String search);
}
