package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.entity.TableColumn;

/**
 * <p>
 * 业务字段表 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-12-19 13:48:53
 */
@Mapper
public interface TableColumnMapper extends ExpandMapper<TableColumn> {

}
