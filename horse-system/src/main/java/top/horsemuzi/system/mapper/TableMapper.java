package top.horsemuzi.system.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.generate.TableDTO;
import top.horsemuzi.system.pojo.dto.generate.TableQuery;
import top.horsemuzi.system.pojo.entity.Table;

import java.util.List;

/**
 * <p>
 * 代码生成业务表 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-12-19 13:48:53
 */
@Mapper
public interface TableMapper extends ExpandMapper<Table> {

    /**
     * 分页获取业务表列表
     *
     * @param page
     * @param query
     * @return
     */
    Page<TableDTO> getPageTable(@Param("page") Page<TableDTO> page, @Param("query") TableQuery query);

    /**
     * 业务表名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> fetchTable(@Param("search") String search);
}
