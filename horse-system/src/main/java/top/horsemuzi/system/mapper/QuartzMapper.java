package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobDTO;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobQuery;

import java.util.List;

/**
 * <p>
 * 系统配置表 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-09 17:36:32
 */
@Mapper
public interface QuartzMapper {

    /**
     * 获取调度任务数量
     *
     * @param quartzQuery
     * @return
     */
    Long getQuartzCount(@Param("query") QuartzJobQuery quartzQuery);

    /**
     * 获取调度任务信息
     *
     * @param quartzQuery
     * @return
     */
    List<QuartzJobDTO> getQuartzList(@Param("query") QuartzJobQuery quartzQuery);

    /**
     * Quartz任务名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> quartzFetch(@Param("search") String search);
}
