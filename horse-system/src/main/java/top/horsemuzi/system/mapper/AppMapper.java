package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.open.app.AppExportDTO;
import top.horsemuzi.system.pojo.dto.open.app.AppQuery;
import top.horsemuzi.system.pojo.entity.App;

import java.util.List;

/**
 * <p>
 * 开放API应用信息 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-18 22:09:28
 */
@Mapper
public interface AppMapper extends ExpandMapper<App> {

    /**
     * 应用名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> fetch(@Param("search") String search);

    /**
     * API应用数据导出
     *
     * @param query
     * @return
     */
    List<AppExportDTO> getAppExportData(@Param("query") AppQuery query);
}
