package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.dto.notice.NoticeDTO;
import top.horsemuzi.system.pojo.dto.notice.NoticeExportDTO;
import top.horsemuzi.system.pojo.dto.notice.NoticeQuery;
import top.horsemuzi.system.pojo.entity.Notice;

import java.util.List;

/**
 * <p>
 * 系统通知公告信息表 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-10-06 16:00:05
 */
@Mapper
public interface NoticeMapper extends ExpandMapper<Notice> {

    /**
     * 获取导出通知公告和对应记录数据
     *
     * @param query
     * @return
     */
    List<NoticeExportDTO> getNoticeExportList(@Param("query") NoticeQuery query);

    /**
     * 分页通知公告信息列表
     *
     * @param query
     * @return
     */
    List<NoticeDTO> getPageNoticeList(@Param("query") NoticeQuery query);

    /**
     * 分页通知公告信息列表数量
     *
     * @param query
     * @return
     */
    Long getPageNoticeCount(@Param("query") NoticeQuery query);
}
