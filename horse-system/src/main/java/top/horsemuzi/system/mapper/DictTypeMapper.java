package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.dict.DictTypeExportDTO;
import top.horsemuzi.system.pojo.dto.dict.DictTypeQuery;
import top.horsemuzi.system.pojo.entity.DictType;

import java.util.List;

/**
 * <p>
 * 字典类型表 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-06-20 11:04:36
 */
@Mapper
public interface DictTypeMapper extends ExpandMapper<DictType> {

    /**
     * 获取导出字典数据信息
     *
     * @param query
     * @return
     */
    List<DictTypeExportDTO> getDictExportList(@Param("query") DictTypeQuery query);

    /**
     * 字典类型实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> fetch(@Param("search") String search);
}
