package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Param;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.dto.open.category.ApiCategoryDTO;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.entity.ApiCategory;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 开放API类型信息 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-18 22:09:28
 */
@Mapper
public interface ApiCategoryMapper extends ExpandMapper<ApiCategory> {

    /**
     * 获取开放API接口分类列表
     *
     * @param appId
     * @return
     */
    List<ApiCategoryDTO> listApiCategory(@Param("appId") Long appId);

    /**
     * 接口类型名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> fetch(@Param("search") String search);
}
