package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.entity.Config;

import java.util.List;

/**
 * <p>
 * 系统配置表 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-09 17:36:32
 */
@Mapper
public interface ConfigMapper extends ExpandMapper<Config> {

    /**
     * 配置参数名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> fetch(@Param("search") String search);
}
