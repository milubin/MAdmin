package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.entity.Datasource;

import java.util.List;

/**
 * <p>
 * 数据源信息 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-12-19 13:48:53
 */
@Mapper
public interface DatasourceMapper extends ExpandMapper<Datasource> {

    /**
     * 数据源名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> fetchDatasource(@Param("search") String search);
}
