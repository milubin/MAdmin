package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Param;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.role.RoleExportDTO;
import top.horsemuzi.system.pojo.dto.role.RoleQuery;
import top.horsemuzi.system.pojo.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-07 22:44:27
 */
@Mapper
public interface RoleMapper extends ExpandMapper<Role> {

    /**
     * 角色名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> fetch(@Param("search") String search);

    /**
     * 导出角色和菜单权限信息
     *
     * @param query
     * @return
     */
    List<RoleExportDTO> getRoleExportList(@Param("query") RoleQuery query);
}
