package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.entity.Oss;

import java.util.List;

/**
 * <p>
 * 对象存储文件信息表 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-08-01 21:50:47
 */
@Mapper
public interface OssMapper extends ExpandMapper<Oss> {

    /**
     * 文件名称实时检索
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> fetch(@Param("search") String search);
}
