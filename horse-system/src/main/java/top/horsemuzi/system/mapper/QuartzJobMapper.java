package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.entity.QuartzJob;

/**
 * <p>
 * Quartz任务调度信息表 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-08-21 16:05:01
 */
@Mapper
public interface QuartzJobMapper extends ExpandMapper<QuartzJob> {

}
