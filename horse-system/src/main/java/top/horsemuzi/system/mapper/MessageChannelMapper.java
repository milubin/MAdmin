package top.horsemuzi.system.mapper;

import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.entity.MessageChannel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 消息渠道配置表 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-09-25 18:20:21
 */
@Mapper
public interface MessageChannelMapper extends ExpandMapper<MessageChannel> {

}
