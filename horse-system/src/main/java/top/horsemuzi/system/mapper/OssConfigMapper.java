package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import org.apache.ibatis.annotations.Param;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import java.util.List;
import top.horsemuzi.system.pojo.entity.OssConfig;

/**
 * @author Mr.Horse
 * @description OSS对象存储配置业务对象tb_oss_config的Mapper实例
 * @date 2023-03-26 11:32:55
 * @version 1.0.0
 */
@Mapper
public interface OssConfigMapper extends ExpandMapper<OssConfig> {

    /**
     * 实时检索
     *
     * @param search 检索关键词
     * @return
     */
    List<InputRemoteFetchDTO> fetchConfig(@Param("search") String search);

}
