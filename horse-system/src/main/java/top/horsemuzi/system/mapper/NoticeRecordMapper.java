package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.dto.notice.NoticeRecordDTO;
import top.horsemuzi.system.pojo.dto.notice.NoticeRecordQuery;
import top.horsemuzi.system.pojo.dto.notice.NoticeSocketDTO;
import top.horsemuzi.system.pojo.entity.NoticeRecord;

import java.util.List;

/**
 * <p>
 * 通知公告发布记录表 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-10-06 16:00:06
 */
@Mapper
public interface NoticeRecordMapper extends ExpandMapper<NoticeRecord> {

    /**
     * 分页获取通知公告记录列表总数
     *
     * @param query
     * @return
     */
    Long getPageNoticeRecordCount(@Param("query") NoticeRecordQuery query);

    /**
     * 分页获取通知公告记录列表数据
     *
     * @param query
     * @return
     */
    List<NoticeRecordDTO> getPageNoticeRecordList(@Param("query") NoticeRecordQuery query);

    /**
     * 获取用户通知公告记录列表
     *
     * @param userId
     * @return
     */
    List<NoticeRecordDTO> getNoticeRecords(@Param("userId") Long userId);

    /**
     * 获取用户通知公告记录未读数量
     *
     * @param userIds
     * @return
     */
    List<NoticeSocketDTO> getNoticeSocketList(@Param("userIds") List<Long> userIds);

    /**
     * 获取发布记录内容信息
     *
     * @param noticeRecordId
     * @return
     */
    NoticeRecordDTO getNoticeRecord(@Param("noticeRecordId") Long noticeRecordId);
}
