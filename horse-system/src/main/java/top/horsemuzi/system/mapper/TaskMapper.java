package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.entity.Task;

import java.util.List;

/**
 * <p>
 * 系统任务调度信息表 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-08-20 17:56:39
 */
@Mapper
public interface TaskMapper extends ExpandMapper<Task> {

    /**
     * Spring任务名称实时检索
     *
     * @param search
     * @return
     */
    List<InputRemoteFetchDTO> taskFetch(@Param("search") String search);
}
