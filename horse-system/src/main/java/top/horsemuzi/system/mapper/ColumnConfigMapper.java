package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.entity.ColumnConfig;

/**
 * <p>
 * 字段类型关联信息 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-12-19 13:48:53
 */
@Mapper
public interface ColumnConfigMapper extends ExpandMapper<ColumnConfig> {

}
