package top.horsemuzi.system.mapper;

import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.entity.ShortcutMenu;

/**
 * <p>
 * 用户快捷菜单信息表 Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2023-01-07 15:26:14
 */
public interface ShortcutMenuMapper extends ExpandMapper<ShortcutMenu> {

}
