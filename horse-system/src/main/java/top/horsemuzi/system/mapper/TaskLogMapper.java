package top.horsemuzi.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.horsemuzi.system.config.mp.expand.ExpandMapper;
import top.horsemuzi.system.pojo.dto.statistics.LineChartDTO;
import top.horsemuzi.system.pojo.dto.statistics.PieChartDTO;
import top.horsemuzi.system.pojo.dto.statistics.SourceLineData;
import top.horsemuzi.system.pojo.entity.TaskLog;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-08-20 17:56:39
 */
@Mapper
public interface TaskLogMapper extends ExpandMapper<TaskLog> {

    /**
     * 获取Spring调度任务日志的饼图统计数据
     *
     * @return
     */
    List<PieChartDTO> statisticsPieChartData();

    /**
     * 获取Spring调度任务日志的折线图统计数据
     *
     * @return
     */
    List<SourceLineData> statisticsLineChartData();

}
