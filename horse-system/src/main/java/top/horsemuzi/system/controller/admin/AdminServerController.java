package top.horsemuzi.system.controller.admin;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.system.annotation.AloneLimiter;
import top.horsemuzi.system.annotation.Check;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.admin.MonitorManager;

import java.util.Map;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description {description}
 * @date 2022/8/13 21:15
 */
@Api(tags = "系统监控服务")
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/admin/server")
public class AdminServerController extends BaseController {

    private final MonitorManager monitorManager;

    @AloneLimiter(perSecond = 3)
    @Check(permissions = {"monitor:server:list"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取系统服务监控信息", notes = "获取系统服务监控信息")
    @GetMapping("/list")
    public Result<Map<String, Object>> getServerInfo() {
        return success(monitorManager.getServerInfo());
    }

}
