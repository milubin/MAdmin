package top.horsemuzi.system.controller.admin;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;
import top.horsemuzi.system.annotation.*;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.admin.NoticeManager;
import top.horsemuzi.system.pojo.dto.notice.NoticeDTO;
import top.horsemuzi.system.pojo.dto.notice.NoticeQuery;
import top.horsemuzi.system.pojo.dto.notice.NoticeRecordDTO;
import top.horsemuzi.system.pojo.dto.notice.NoticeRecordQuery;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * <p>
 * 系统通知公告信息表 前端控制器
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-10-06 14:36:00
 */
@Api(tags = "通知公告管理")
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/admin/notice")
public class AdminNoticeController extends BaseController {

    private final NoticeManager noticeManager;

    @Check(permissions = {"system:notice:list"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("/page")
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取通知公告信息列表")
    public Result<Page<NoticeDTO>> getPageNotices(NoticeQuery query) {
        return success(noticeManager.getPageNotices(query));
    }

    @Check(permissions = {"system:notice:query"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取通知公告信息详情")
    @ApiImplicitParam(name = "id", value = "通知公告ID", required = true, dataTypeClass = Long.class)
    @GetMapping("/{id}")
    public Result<NoticeDTO> getNotice(@PathVariable("id") @NotNull(message = "通知公告ID不能为空") Long noticeId) {
        return success(noticeManager.getNotice(noticeId));
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:notice:add"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "新增通知公告")
    @PostMapping
    public Result<?> addNotice(@RequestBody @Validated({ValidOne.class}) NoticeDTO noticeDTO) {
        noticeManager.addNotice(noticeDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:notice:update"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新通知公告")
    @PutMapping
    public Result<?> updateNotice(@RequestBody @Validated({ValidTwo.class}) NoticeDTO noticeDTO) {
        noticeManager.updateNotice(noticeDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:notice:publish"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "发布通知公告")
    @ApiImplicitParam(name = "id", value = "通知公告ID", required = true, dataTypeClass = Long.class)
    @PutMapping("/publish")
    public Result<?> publishNotice(@RequestParam @NotNull(message = "通知公告主键ID不能为空") Long id) {
        noticeManager.publishNotice(id);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:notice:revoke"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "撤销通知公告", notes = "允许已发布状态的通知公告撤销操作")
    @ApiImplicitParam(name = "id", value = "通知公告ID", required = true, dataTypeClass = Long.class)
    @PutMapping("/revoke")
    public Result<?> revokeNotice(@RequestParam @NotNull(message = "通知公告主键ID不能为空") Long id) {
        noticeManager.revokeNotice(id);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:notice:delete"}, role = Constants.ROLE_CODE.ADMIN)
    @DeleteMapping
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "删除通知公告")
    public Result<?> deleteNotice(@RequestBody @NotNull(message = "通知公告ID集合不能为空")
                                  @Size(min = 1, message = "通知公告ID集合元素必须大于1") List<Long> noticeIds) {
        noticeManager.deleteNotice(noticeIds);
        return success();
    }

    @Log(type = Constants.METHOD.EXPORT, binaryResData = true)
    @Idempotent
    @AloneLimiter(perSecond = 3)
    @Check(permissions = {"system:notice:export"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "导出通知公告数据", notes = "导出通知公告和对应记录数据（EXCEL）")
    @PostMapping("/export")
    public void export(NoticeQuery query, HttpServletResponse response) {
        noticeManager.export(query, response);
    }

    @Check(permissions = {"system:noticeRecord:list"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("/record/page")
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取通知公告记录列表")
    public Result<Page<NoticeRecordDTO>> getPageNoticeRecords(NoticeRecordQuery query) {
        return success(noticeManager.getPageNoticeRecords(query));
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:noticeRecord:revoke"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "撤销通知公告发布记录信息(细粒度)", notes = "管理员可针对发布记录进行撤销操作")
    @PutMapping("/record/revoke")
    public Result<?> revokeNoticeRecord(@RequestBody @NotNull(message = "通知公告发布记录ID集合不能为空")
                                        @Size(min = 1, message = "通知公告发布记录ID集合元素必须大于1") List<Long> noticeRecordIds) {
        noticeManager.revokeNoticeRecord(noticeRecordIds);
        return success();
    }

    @Anonymous
    @Check
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取用户通知公告记录列表")
    @GetMapping("/record/list")
    public Result<List<NoticeRecordDTO>> getNoticeRecords(@ApiIgnore JwtUser jwtUser) {
        return success(noticeManager.getNoticeRecords(jwtUser));
    }

    @Anonymous
    @Check
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取通知公告记录详情")
    @ApiImplicitParam(name = "id", value = "通知公告记录ID", required = true, dataTypeClass = Long.class)
    @GetMapping("/record/{id}")
    public Result<NoticeRecordDTO> getNoticeRecord(@PathVariable("id") @NotNull(message = "通知公告记录ID不能为空") Long noticeRecordId) {
        return success(noticeManager.getNoticeRecord(noticeRecordId));
    }

    @Anonymous
    @Check
    @Log
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "读取通知公告记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "noticeRecordId", value = "记录id", dataTypeClass = Long.class),
            @ApiImplicitParam(name = "type", value = "类型: 1-单条, 2-批量", dataTypeClass = String.class, required = true)
    })
    @PutMapping("/record/read")
    public Result<?> readNoticeRecord(@RequestParam(value = "noticeRecordId", required = false) Long noticeRecordId, @RequestParam("type") String type, @ApiIgnore JwtUser jwtUser) {
        noticeManager.readNoticeRecord(noticeRecordId, type, jwtUser);
        return success();
    }

    @Anonymous
    @Check
    @Log
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "删除通知公告记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "noticeRecordId", value = "记录id", dataTypeClass = Long.class),
            @ApiImplicitParam(name = "type", value = "类型: 1-单条, 2-批量", dataTypeClass = String.class, required = true)
    })
    @DeleteMapping("/record/delete")
    public Result<?> deleteNoticeRecord(@RequestParam(value = "noticeRecordId", required = false) Long noticeRecordId, @RequestParam("type") String type, @ApiIgnore JwtUser jwtUser) {
        noticeManager.deleteNoticeRecord(noticeRecordId, type, jwtUser);
        return success();
    }

}
