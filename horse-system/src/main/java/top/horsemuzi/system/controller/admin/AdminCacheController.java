package top.horsemuzi.system.controller.admin;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.system.annotation.AloneLimiter;
import top.horsemuzi.system.annotation.Check;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.admin.MonitorManager;

import java.util.Map;

/**
 * <p>
 * 文件信息表 前端控制器
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-07 22:44:27
 */
@Api(tags = "系统缓存管理服务")
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/admin/cache")
public class AdminCacheController extends BaseController {

    private final MonitorManager monitorManager;

    @AloneLimiter(perSecond = 3)
    @Check(permissions = {"monitor:cache:list"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取缓存监控信息", notes = "获取缓存监控信息(redis)")
    @GetMapping("/list")
    public Result<Map<String, Object>> getCacheInfo() {
        return success(monitorManager.getCacheInfo());
    }

}
