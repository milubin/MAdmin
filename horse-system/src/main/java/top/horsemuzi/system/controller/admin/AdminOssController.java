package top.horsemuzi.system.controller.admin;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;
import top.horsemuzi.system.annotation.AloneLimiter;
import top.horsemuzi.system.annotation.Check;
import top.horsemuzi.system.annotation.Idempotent;
import top.horsemuzi.system.annotation.Log;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.admin.OssManager;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.oss.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * <p>
 * 对象存储文件信息表 前端控制器
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-08-01 21:50:47
 */
@Api(tags = "文件管理服务")
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/admin/oss")
public class AdminOssController extends BaseController {

    private final OssManager ossManager;

    @Check(permissions = {"system:oss:list"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取文件列表")
    @GetMapping
    public Result<Page<OssDTO>> getPageOss(OssQuery ossQuery) {
        return success(ossManager.getPageOss(ossQuery));
    }

    /**
     * @param ossUploadDTO
     * @return
     */
    @Log
    @Idempotent
    @AloneLimiter(perSecond = 3)
    @Check(permissions = {"system:oss:upload"})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "文件上传")
    @PostMapping("/upload")
    public Result<OssDTO> upload(OssUploadDTO ossUploadDTO) {
        return success(ossManager.upload(ossUploadDTO));
    }

    @Log(type = Constants.METHOD.DOWNLOAD, binaryResData = true)
    @Idempotent
    @AloneLimiter(perSecond = 5)
    @Check(permissions = {"system:oss:download"})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "文件下载")
    @ApiImplicitParam(name = "id", value = "文件ID", required = true, dataTypeClass = Long.class)
    @PostMapping("/download")
    public void download(@RequestParam("id") Long id, HttpServletResponse response) {
        ossManager.download(id, response);
    }

    @Log(type = Constants.METHOD.PREVIEW, binaryResData = true)
    @Idempotent
    @Check(permissions = {"system:oss:preview"})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "文件预览", notes = "不接收图片类型文件")
    @ApiImplicitParam(name = "id", value = "文件ID", required = true, dataTypeClass = Long.class)
    @GetMapping("/preview")
    public void preview(@RequestParam("id") Long id, HttpServletResponse response) {
        ossManager.preview(id, response);
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:oss:delete"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "文件删除")
    @ApiImplicitParam(name = "ossIds", value = "文件ID集合", required = true, dataTypeClass = Long.class)
    @DeleteMapping
    public Result<?> delete(@RequestBody @NotNull(message = "文件ID集合不能为空")
                            @Size(min = 1, message = "文件ID集合元素必须大于1") List<Long> ossIds) {
        ossManager.delete(ossIds);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:oss:update"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "文件更新")
    @PutMapping
    public Result<?> update(@RequestBody @Validated({ValidOne.class}) OssDTO ossDTO) {
        ossManager.update(ossDTO);
        return success();
    }

    @Check(permissions = {"system:oss:fetch"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("/fetch")
    @ApiImplicitParam(name = "search", value = "实时检索关键字", dataTypeClass = String.class)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "文件名称实时检索", notes = "输入框实时检索文件名称")
    public Result<List<InputRemoteFetchDTO>> fetch(@RequestParam("search") String search) {
        return success(ossManager.fetch(search));
    }

    @Check(permissions = {"system:ossConfig:list"}, role = {Constants.ROLE_CODE.ADMIN})
    @GetMapping("/config")
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取OSS对象存储配置列表")
    @ApiOperationSupport(order = 0, author = "Mr.Horse")
    public Result<Page<OssConfigDTO>> getPageOssConfig(OssConfigQuery query) {
        return success(ossManager.getPageOssConfig(query));
    }

    @Check(permissions = {"system:ossConfig:query"}, role = {Constants.ROLE_CODE.ADMIN})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取OSS对象存储配置详情")
    @ApiOperationSupport(order = 1, author = "Mr.Horse")
    @ApiImplicitParam(name = "id", value = "OSS对象存储配置主键ID", required = true, dataTypeClass = Long.class)
    @GetMapping("/config/{id}")
    public Result<OssConfigDTO> getOssConfig(@PathVariable("id") @NotNull(message = "OSS对象存储配置主键ID不能为空") Long id) {
        return success(ossManager.getOssConfig(id));
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:ossConfig:add"}, role = {Constants.ROLE_CODE.ADMIN})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "新增OSS对象存储配置")
    @ApiOperationSupport(order = 2, author = "Mr.Horse")
    @PostMapping("/config")
    public Result<?> addOssConfig(@RequestBody @Validated({ValidOne.class}) OssConfigDTO ossConfigDTO) {
        ossManager.addOssConfig(ossConfigDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:ossConfig:update"}, role = {Constants.ROLE_CODE.ADMIN})
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新OSS对象存储配置")
    @ApiOperationSupport(order = 3, author = "Mr.Horse")
    @PutMapping("/config")
    public Result<?> updateOssConfig(@RequestBody @Validated({ValidTwo.class}) OssConfigDTO ossConfigDTO) {
        ossManager.updateOssConfig(ossConfigDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:ossConfig:delete"}, role = {Constants.ROLE_CODE.ADMIN})
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "删除OSS对象存储配置")
    @ApiOperationSupport(order = 4, author = "Mr.Horse")
    @ApiImplicitParam(name = "ossConfigIds", value = "OSS对象存储配置ID集合", required = true, dataTypeClass = Long.class)
    @DeleteMapping("/config")
    public Result<?> deleteOssConfig(@RequestBody @NotNull(message = "OSS对象存储配置ID集合不能为空")
                                     @Size(min = 1, message = "OSS对象存储配置ID集合元素必须大于1") List<Long> ossConfigIds) {
        ossManager.deleteOssConfig(ossConfigIds);
        return success();
    }

    @Log(type = Constants.METHOD.EXPORT, binaryResData = true)
    @Idempotent
    @AloneLimiter(perSecond = 3)
    @Check(permissions = {"system:ossConfig:export"}, role = {Constants.ROLE_CODE.ADMIN})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "导出OSS对象存储配置 EXCEL 数据")
    @ApiOperationSupport(order = 5, author = "Mr.Horse")
    @PostMapping("/config/export")
    public void export(OssConfigQuery query, HttpServletResponse response) {
        ossManager.export(query, response);
    }

    @Check(permissions = {"system:ossConfig:fetch"}, role = {Constants.ROLE_CODE.ADMIN})
    @ApiImplicitParam(name = "search", value = "实时检索关键字", dataTypeClass = String.class, required = true)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "实时检索")
    @ApiOperationSupport(order = 6, author = "Mr.Horse")
    @GetMapping("/config/fetch")
    public Result<List<InputRemoteFetchDTO>> fetchConfig(@RequestParam("search") String search) {
        return success(ossManager.fetchConfig(search));
    }

}
