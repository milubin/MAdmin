package top.horsemuzi.system.controller.admin;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;
import top.horsemuzi.system.annotation.Check;
import top.horsemuzi.system.annotation.Idempotent;
import top.horsemuzi.system.annotation.Log;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.admin.ApiManager;
import top.horsemuzi.system.manager.admin.AppManager;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.open.api.ApiDTO;
import top.horsemuzi.system.pojo.dto.open.category.ApiCategoryDTO;
import top.horsemuzi.system.pojo.dto.open.category.ApiCategoryQuery;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * <p>
 * 开放API应用接口信息 前端控制器
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-18 22:09:28
 */
@Api(tags = "开放API接口类型管理服务")
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/admin/api/category")
public class AdminApiCategoryController extends BaseController {

    private final AppManager appManager;
    private final ApiManager apiManager;

    @Check(permissions = {"open:category:list"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取开放API接口类型列表")
    @GetMapping("/page")
    public Result<Page<ApiCategoryDTO>> getPageApiCategory(ApiCategoryQuery query) {
        return success(appManager.getPageApiCategory(query));
    }

    @Check(permissions = {"open:category:query"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取开放API类型详情")
    @ApiImplicitParam(name = "id", value = "API类型id", required = true, dataTypeClass = Long.class)
    @GetMapping("/{id}")
    public Result<ApiCategoryDTO> getApiCategory(@PathVariable("id") @NotNull(message = "API类型id不能为空") Long id) {
        return success(appManager.getApiCategory(id));
    }

    @Check(permissions = {"open:category:list"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取开放API接口类型列表", notes = "用于页面下拉框选择")
    @GetMapping("/list")
    public Result<List<ApiCategoryDTO>> getApiCategoryList() {
        return success(appManager.getApiCategoryList());
    }

    @Log
    @Idempotent
    @Check(permissions = {"open:category:add"})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "新增开放API类型")
    @PostMapping
    public Result<?> addApiCategory(@RequestBody @Validated(ValidOne.class) ApiCategoryDTO apiCategoryDTO) {
        appManager.addApiCategory(apiCategoryDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"open:category:update"})
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新开放API类型")
    @PutMapping
    public Result<?> updateApiCategory(@RequestBody @Validated(ValidTwo.class) ApiCategoryDTO apiCategoryDTO) {
        appManager.updateApiCategory(apiCategoryDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"open:category:delete"})
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "删除开放API类型")
    @ApiImplicitParam(name = "apiCategoryIds", value = "API类型ID集合", required = true, dataTypeClass = Long.class)
    @DeleteMapping
    public Result<?> deleteApiCategory(@RequestBody @NotNull(message = "API类型ID集合不能为空")
                                       @Size(min = 1, message = "API类型ID集合元素必须大于1") List<Long> apiCategoryIds) {
        appManager.deleteApiCategory(apiCategoryIds);
        return success();
    }

    @Check(permissions = {"open:category:fetch"})
    @GetMapping("/fetch")
    @ApiImplicitParam(name = "search", value = "实时检索关键字", dataTypeClass = String.class)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "接口类型名称实时检索", notes = "输入框实时检索接口类型名称")
    public Result<List<InputRemoteFetchDTO>> fetchApiCategory(@RequestParam("search") String search) {
        return success(appManager.fetchApiCategory(search));
    }

    @Check(permissions = {"open:category:api"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取开放API接口列表")
    @ApiImplicitParam(name = "apiCategoryId", value = "开放API接口类型ID", readOnly = true, dataTypeClass = Long.class)
    @GetMapping("/api/{apiCategoryId}")
    public Result<List<ApiDTO>> listApi(@PathVariable("apiCategoryId") @NotNull(message = "开放API接口类型ID不能为空") Long apiCategoryId) {
        return success(apiManager.listApi(apiCategoryId));
    }
}
