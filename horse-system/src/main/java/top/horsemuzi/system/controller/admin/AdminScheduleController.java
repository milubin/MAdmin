package top.horsemuzi.system.controller.admin;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;
import top.horsemuzi.system.annotation.AloneLimiter;
import top.horsemuzi.system.annotation.Check;
import top.horsemuzi.system.annotation.Idempotent;
import top.horsemuzi.system.annotation.Log;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.admin.ScheduleManager;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobDTO;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobLogDTO;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobLogQuery;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobQuery;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskDTO;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskLogDTO;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskLogQuery;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskQuery;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Map;

/**
 * Quartz任务调度接口服务
 *
 * @author mabin
 * @date 2022/06/30 16:14
 **/
@Api(tags = "任务调度")
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/admin/schedule")
public class AdminScheduleController extends BaseController {

    private final ScheduleManager scheduleManager;

    @Check(permissions = {"system:quartz:list"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("/quartz/page")
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取Quartz任务调度列表")
    public Result<Page<QuartzJobDTO>> getPageQuartzs(QuartzJobQuery quartzQuery) {
        return success(scheduleManager.getPageQuartzs(quartzQuery));
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:quartz:add"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "创建Quartz任务调度")
    @PostMapping("/quartz/add")
    public Result<?> addQuartzJob(@RequestBody @Validated({ValidOne.class}) QuartzJobDTO quartzDTO) {
        scheduleManager.addQuartzJob(quartzDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:quartz:pause"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "暂停Quartz任务调度")
    @ApiImplicitParam(name = "id", value = "任务id", required = true, dataTypeClass = Long.class)
    @PutMapping("/quartz/pause")
    public Result<?> pauseQuartzJob(@RequestParam("id") @NotNull(message = "任务id不能为空") Long id) {
        scheduleManager.pauseQuartzJob(id);
        return success();
    }

    @Log()
    @Idempotent
    @Check(permissions = {"system:quartz:reboot"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "重启Quartz任务调度")
    @ApiImplicitParam(name = "id", value = "任务id", required = true, dataTypeClass = Long.class)
    @PutMapping("/quartz/reboot")
    public Result<?> rebootQuartzJob(@RequestParam("id") @NotNull(message = "任务id不能为空") Long id) {
        scheduleManager.rebootQuartzJob(id);
        return success();
    }

    @Log()
    @Idempotent
    @Check(permissions = {"system:quartz:update"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新Quartz任务调度")
    @PutMapping("/quartz/update")
    public Result<?> updateQuartzJob(@RequestBody @Validated({ValidTwo.class}) QuartzJobDTO quartzDTO) {
        scheduleManager.updateQuartzJob(quartzDTO);
        return success();
    }

    @Check(permissions = {"system:quartz:query"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "查询Quartz任务调度信息")
    @ApiImplicitParam(name = "id", value = "任务id", required = true, dataTypeClass = Long.class)
    @GetMapping("/quartz/{id}")
    public Result<QuartzJobDTO> getQuartzJob(@PathVariable("id") @NotNull(message = "任务id不能为空") Long id) {
        return success(scheduleManager.getQuartzJob(id));
    }

    @Log()
    @Idempotent
    @Check(permissions = {"system:quartz:delete"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "删除Quartz任务调度")
    @ApiImplicitParam(name = "id", value = "任务id列表", required = true, dataTypeClass = Long.class)
    @DeleteMapping("/quartz/delete")
    public Result<?> deleteQuartzJob(@RequestBody @NotNull(message = "任务集合不能为空")
                                     @Size(min = 1, message = "任务集合元素必须大于1") List<Long> jobIds) {
        scheduleManager.deleteQuartzJob(jobIds);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:quartz:once"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "Quartz任务立刻运行一次")
    @ApiImplicitParam(name = "id", value = "任务id", required = true, dataTypeClass = Long.class)
    @GetMapping("/quartz/once")
    public Result<?> runOnceQuartzJob(@RequestParam("id") @NotNull(message = "任务id不能为空") Long id) {
        scheduleManager.runOnceQuartzJob(id);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:quartz:all"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "Quartz任务全部暂停/启动")
    @ApiImplicitParam(name = "execType", value = "操作类型,0-全部暂停, 1-全部启动", required = true, dataTypeClass = Integer.class, example = "0")
    @PutMapping("/quartz/all")
    public Result<?> allPauseOrRunQuartzJob(@RequestParam("execType") Integer execType) {
        scheduleManager.allPauseOrRunQuartzJob(execType);
        return success();
    }

    @Check(permissions = {"system:quartz:log"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("/quartz/log")
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取Quartz任务调度日志列表")
    public Result<Page<QuartzJobLogDTO>> getPageQuartzLog(QuartzJobLogQuery query) {
        return success(scheduleManager.getPageQuartzLog(query));
    }

    @Log()
    @Idempotent
    @Check(permissions = {"system:quartz:clear"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "清理Quartz任务调度日志信息")
    @ApiImplicitParam(name = "id", value = "任务id列表", required = true, dataTypeClass = Long.class)
    @DeleteMapping("/quartz/log")
    public Result<?> clearQuartzLogs(@RequestBody @NotNull(message = "日志ID集合不能为空")
                                     @Size(min = 1, message = "日志ID集合元素必须大于1") List<Long> quartzLogIds) {
        scheduleManager.clearQuartzLogs(quartzLogIds);
        return success();
    }

    @Log(type = Constants.METHOD.EXPORT, binaryResData = true)
    @Idempotent
    @AloneLimiter(perSecond = 3)
    @Check(permissions = {"system:quartz:export"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "导出Quartz调度任务日志", notes = "导出Quartz调度任务日志(CSV)")
    @PostMapping("/quartz/export")
    public void exportQuartzLog(QuartzJobLogQuery query, HttpServletResponse response) {
        scheduleManager.exportQuartzLog(query, response);
    }

    @Check(permissions = {"system:quartz:statistics"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取Quartz调度任务日志统计数据", notes = "折线图和饼图相关数据")
    @GetMapping("/quartz/statistics")
    public Result<Map<String, Object>> statisticsQuartzLog() {
        return success(scheduleManager.statisticsQuartzLog());
    }

    @Check(permissions = {"system:quartz:fetch"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("/quartz/fetch")
    @ApiImplicitParam(name = "search", value = "实时检索关键字", dataTypeClass = String.class)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "Quartz任务名称实时检索", notes = "输入框实时检索Quartz任务名称")
    public Result<List<InputRemoteFetchDTO>> quartzFetch(@RequestParam("search") String search) {
        return success(scheduleManager.quartzFetch(search));
    }

    // ======================================Spring调度任务相关===========================================

    @Check(permissions = {"system:task:list"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取Spring任务调度列表")
    @GetMapping("/task/page")
    public Result<Page<TaskDTO>> getPageTasks(TaskQuery taskQuery) {
        return success(scheduleManager.getPageTasks(taskQuery));
    }

    @Check(permissions = {"system:task:query"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取Spring调度任务")
    @ApiImplicitParam(name = "id", value = "任务id", required = true, dataTypeClass = Long.class)
    @GetMapping("/task/{id}")
    public Result<TaskDTO> getTask(@PathVariable("id") @NotNull(message = "任务id不能为空") Long id) {
        return success(scheduleManager.getTask(id));
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:task:add"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "新增Spring调度任务")
    @PostMapping("/task/add")
    public Result<?> addTask(@RequestBody @Validated({ValidOne.class}) TaskDTO taskDTO) {
        scheduleManager.addTask(taskDTO);
        return success();
    }

    @Log()
    @Idempotent
    @Check(permissions = {"system:task:update"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新Spring调度任务", notes = "更新操作只能修改cron表达式")
    @PutMapping("/task/update")
    public Result<?> updateTask(@RequestBody @Validated({ValidTwo.class}) TaskDTO taskDTO) {
        scheduleManager.updateTask(taskDTO);
        return success();
    }

    @Log()
    @Idempotent
    @Check(permissions = {"system:task:switch"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "切换Spring调度任务状态")
    @PutMapping("/task/switch")
    public Result<?> switchTaskStatus(@RequestBody @Validated({ValidTwo.class}) TaskDTO taskDTO) {
        scheduleManager.switchTaskStatus(taskDTO);
        return success();
    }


    @Log
    @Idempotent
    @Check(permissions = {"system:task:all"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "Spring调度任务全部暂停/启动")
    @ApiImplicitParam(name = "execType", value = "操作类型,0-全部暂停, 1-全部启动", required = true, dataTypeClass = Integer.class, example = "0")
    @PutMapping("/task/all")
    public Result<?> allPauseOrRunTask(@RequestParam("execType") Integer execType) {
        scheduleManager.allPauseOrRunTask(execType);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:task:delete"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "删除Spring调度任务")
    @ApiImplicitParam(name = "taskIds", value = "任务id集合", required = true, dataTypeClass = Long.class, example = "1,2,3")
    @DeleteMapping("/task/delete")
    public Result<?> deleteTask(@RequestBody @NotNull(message = "任务集合不能为空")
                                @Size(min = 1, message = "任务集合元素必须大于1") List<Long> taskIds) {
        scheduleManager.deleteTask(taskIds);
        return success();
    }

    @Check(permissions = {"system:task:log"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("/task/log")
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取Spring任务调度日志列表")
    public Result<Page<TaskLogDTO>> getPageTaskLog(TaskLogQuery query) {
        return success(scheduleManager.getPageTaskLog(query));
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:task:clear"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "清理Spring任务调度日志信息")
    @ApiImplicitParam(name = "taskLogIds", value = "日志ID集合", required = true, dataTypeClass = Long.class, example = "1,2,3")
    @DeleteMapping("/task/log")
    public Result<?> clearTaskLogs(@RequestBody @NotNull(message = "日志ID集合不能为空")
                                   @Size(min = 1, message = "日志ID集合元素必须大于1") List<Long> taskLogIds) {
        scheduleManager.clearTaskLogs(taskLogIds);
        return success();
    }

    @Log(type = Constants.METHOD.EXPORT, binaryResData = true)
    @Idempotent
    @AloneLimiter(perSecond = 5)
    @Check(permissions = {"system:task:export"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "导出Spring调度任务日志", notes = "导出Spring调度任务日志(CSV)")
    @PostMapping("/task/export")
    public void exportTaskLog(TaskLogQuery query, HttpServletResponse response) {
        scheduleManager.exportTaskLog(query, response);
    }

    @Check(permissions = {"system:task:statistics"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取Spring调度任务日志统计数据", notes = "折线图和饼图相关数据")
    @GetMapping("/task/statistics")
    public Result<Map<String, Object>> statisticsTaskLog() {
        return success(scheduleManager.statisticsTaskLog());
    }

    @Check(permissions = {"system:task:fetch"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("/task/fetch")
    @ApiImplicitParam(name = "search", value = "实时检索关键字", dataTypeClass = String.class)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "Spring任务名称实时检索", notes = "输入框实时检索Spring任务名称")
    public Result<List<InputRemoteFetchDTO>> taskFetch(@RequestParam("search") String search) {
        return success(scheduleManager.taskFetch(search));
    }

}
