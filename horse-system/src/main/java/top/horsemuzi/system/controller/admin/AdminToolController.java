package top.horsemuzi.system.controller.admin;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.system.annotation.AloneLimiter;
import top.horsemuzi.system.annotation.Check;
import top.horsemuzi.system.annotation.Idempotent;
import top.horsemuzi.system.annotation.Log;
import top.horsemuzi.system.manager.admin.ToolManager;

import java.util.List;
import java.util.Map;

/**
 * 系统工具接口服务
 *
 * @author mabin
 * @date 2022/09/30 09:35
 **/
@Api(tags = "系统工具管理")
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/admin/tools")
public class AdminToolController {

    private final ToolManager toolManager;

    @Log
    @AloneLimiter(perSecond = 2)
    @Idempotent
    @Check(permissions = {"tools:inner:mysqlBackup"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "MySQL远程备份", notes = "执行备份、拉取、解压操作")
    @PostMapping("/mysql/backup")
    public Result<?> mysqlBackup() {
        toolManager.execMysqlBackup();
        return Result.success();
    }

    @Log
    @Check(permissions = {"tools:inner:permission"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "比对本地权限信息")
    @GetMapping("/diff/permission")
    public Result<List<Map<String, Object>>> diffPermissions() {
        return Result.success(toolManager.diffPermissions());
    }

}
