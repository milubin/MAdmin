package top.horsemuzi.system.controller.admin;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;
import top.horsemuzi.system.annotation.AloneLimiter;
import top.horsemuzi.system.annotation.Check;
import top.horsemuzi.system.annotation.Idempotent;
import top.horsemuzi.system.annotation.Log;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.admin.ConfigManager;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.config.ConfigDTO;
import top.horsemuzi.system.pojo.dto.config.ConfigQuery;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * <p>
 * 系统配置表 前端控制器
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-09 17:36:32
 */
@Api(tags = "系统配置管理")
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/admin/configs")
public class AdminConfigController extends BaseController {

    private final ConfigManager configManager;

    @Check(permissions = {"system:config:list"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取系统配置列表", notes = "分页获取系统配置列表")
    public Result<Page<ConfigDTO>> getPageConfigs(ConfigQuery configQuery) {
        return success(configManager.getPageConfigs(configQuery));
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:config:add"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "新增系统配置", notes = "新增系统配置信息")
    @PostMapping
    public Result<?> addConfig(@RequestBody @Validated(ValidOne.class) ConfigDTO configDTO) {
        configManager.addConfig(configDTO);
        return success();
    }

    @Check(permissions = {"system:config:query"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取系统配置详情", notes = "根据组别和key获取系统配置详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "group", value = "系统配置组别", required = true, dataTypeClass = String.class),
            @ApiImplicitParam(name = "key", value = "配置键名key", required = true, dataTypeClass = String.class),
    })
    @GetMapping("/{group}/{key}")
    public Result<ConfigDTO> getConfig(@PathVariable("group") @NotBlank(message = "系统配置组别不能为空") String group,
                                       @PathVariable("key") @NotBlank(message = "配置键名key不能为空") String key) {
        return success(configManager.getConfig(group, key));
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:config:update"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新系统配置", notes = "更新系统配置信息")
    @PutMapping
    public Result<?> updateConfig(@RequestBody @Validated(ValidTwo.class) ConfigDTO configDTO) {
        configManager.updateConfig(configDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:config:delete"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "批量删除系统配置信息", notes = "批量删除系统配置信息")
    @ApiImplicitParam(name = "configIds", value = "系统配置ID集合", required = true, dataTypeClass = Long.class)
    @DeleteMapping
    public Result<?> deleteConfigs(@RequestBody @NotNull(message = "系统配置ID集合不能为空")
                                   @Size(min = 1, message = "系统配置ID集合元素必须大于1") List<Long> configIds) {
        configManager.deleteConfigs(configIds);
        return success();
    }

    @Log(type = Constants.METHOD.EXPORT, binaryResData = true)
    @Idempotent
    @AloneLimiter(perSecond = 3)
    @Check(permissions = {"system:config:export"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "导出数据", notes = "系统配置信息导出")
    @PostMapping("/export")
    public void export(ConfigQuery configQuery) {
        configManager.export(configQuery);
    }

    @Log(type = Constants.METHOD.REFRESH)
    @Check(permissions = {"system:config:refresh"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "刷新缓存", notes = "刷新系统配置缓存信息")
    @PutMapping("/refresh")
    public Result<?> refresh() {
        configManager.refresh();
        return success();
    }

    @Check(permissions = {"system:config:fetch"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("/fetch")
    @ApiImplicitParam(name = "search", value = "实时检索关键字", dataTypeClass = String.class)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "配置参数名称实时检索", notes = "输入框实时检索配置参数名称")
    public Result<List<InputRemoteFetchDTO>> fetch(@RequestParam("search") String search) {
        return success(configManager.fetch(search));
    }
}
