package top.horsemuzi.system.controller.admin;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;
import top.horsemuzi.system.annotation.Check;
import top.horsemuzi.system.annotation.Idempotent;
import top.horsemuzi.system.annotation.Log;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.admin.DivisionManager;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.division.DivisionDTO;
import top.horsemuzi.system.pojo.dto.division.DivisionQuery;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 * 国家行政区划信息表 前端控制器
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-06 10:31:24
 */
@Api(tags = "国家行政区划服务")
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/admin/division")
public class AdminDivisionController extends BaseController {

    private final DivisionManager divisionManager;

    @Check(permissions = {"system:division:list"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取行政区划信息树形列表")
    @GetMapping
    public Result<List<DivisionDTO>> getTreeDivisions(DivisionQuery query) {
        return success(divisionManager.getTreeDivisions(query));
    }

    @Check(permissions = {"system:division:query"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取行政区划信息")
    @ApiImplicitParam(name = "id", value = "行政区划id", required = true, dataTypeClass = Long.class)
    @GetMapping("/{id}")
    public Result<DivisionDTO> getDivision(@PathVariable("id") @NotNull(message = "区域id不能为空") Long id) {
        return success(divisionManager.getDivision(id));
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:division:add"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "新增行政区划")
    @PostMapping
    public Result<?> addDivision(@RequestBody @Validated({ValidOne.class}) DivisionDTO divisionDTO) {
        divisionManager.addDivision(divisionDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:division:update"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新行政区划")
    @PutMapping
    public Result<?> updateDivision(@RequestBody @Validated({ValidTwo.class}) DivisionDTO divisionDTO) {
        divisionManager.updateDivision(divisionDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:division:delete"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "删除行政区划")
    @ApiImplicitParam(name = "id", value = "行政区划id", required = true, dataTypeClass = Long.class)
    @DeleteMapping("/{id}")
    public Result<?> deleteDivision(@PathVariable("id") @NotNull(message = "区划id不能为空") Long id) {
        divisionManager.deleteDivision(id);
        return success();
    }

    @Check(permissions = {"system:division:fetch"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("/fetch")
    @ApiImplicitParam(name = "search", value = "实时检索关键字", dataTypeClass = String.class)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "区划名称实时检索", notes = "输入框实时检索区划名称")
    public Result<List<InputRemoteFetchDTO>> fetch(@RequestParam("search") String search) {
        return success(divisionManager.fetch(search));
    }

}
