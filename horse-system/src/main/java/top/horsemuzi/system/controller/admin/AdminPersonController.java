package top.horsemuzi.system.controller.admin;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.common.validator.ValidFour;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidThree;
import top.horsemuzi.common.validator.ValidTwo;
import top.horsemuzi.system.annotation.AloneLimiter;
import top.horsemuzi.system.annotation.Check;
import top.horsemuzi.system.annotation.Idempotent;
import top.horsemuzi.system.annotation.Log;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.admin.UserManager;
import top.horsemuzi.system.pojo.dto.user.PersonDTO;
import top.horsemuzi.system.pojo.dto.user.ShortcutMenuDTO;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * 个人中心服务
 *
 * @author 马滨
 * @date 2022/09/12 16:08
 **/
@Api(tags = "个人中心服务管理")
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/admin/person")
public class AdminPersonController extends BaseController {

    private final UserManager userManager;

    @Check
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取个人信息")
    @GetMapping
    public Result<PersonDTO> getPerson(@ApiIgnore JwtUser jwtUser) {
        return success(userManager.getPerson(jwtUser));
    }

    @Log
    @Idempotent
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新个人信息")
    @PutMapping
    public Result<?> updatePerson(@RequestBody @Validated({ValidOne.class}) PersonDTO personDTO) {
        userManager.updatePerson(personDTO);
        return success();
    }

    @Log
    @Idempotent
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "修改个人密码")
    @PutMapping("/password")
    public Result<?> updatePassword(@RequestBody @Validated({ValidTwo.class}) PersonDTO personDTO) {
        userManager.updatePassword(personDTO);
        return success();
    }

    @Check
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "校验用户是否需要强制更新密码")
    @GetMapping("/force")
    public Result<Boolean> verifyForceUpdatePassword(@ApiIgnore JwtUser jwtUser) {
        return success(userManager.verifyForceUpdatePassword(jwtUser));
    }

    @Log
    @Check
    @Idempotent
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "强制更新用户密码")
    @PutMapping("/force")
    public Result<?> forceUpdatePassword(@RequestBody @Validated({ValidThree.class}) PersonDTO personDTO, @ApiIgnore JwtUser jwtUser) {
        userManager.forceUpdatePassword(personDTO, jwtUser);
        return success();
    }

    @Log
    @Idempotent
    @AloneLimiter(perSecond = 3)
    @Check
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "修改个人头像")
    @PostMapping("/avatar")
    public Result<String> updateAvatar(@RequestPart("file") MultipartFile file, @ApiIgnore JwtUser jwtUser) {
        return success(userManager.updateAvatar(file, jwtUser));
    }

    @Log
    @Idempotent
    @AloneLimiter(perSecond = 3)
    @Check
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "用户实名认证")
    @PostMapping("/authentication")
    public Result<?> authentication(@RequestBody @Validated({ValidFour.class}) PersonDTO personDTO) {
        userManager.authentication(personDTO);
        return success();
    }


    @Check
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取个人快捷菜单列表")
    @GetMapping("/shortcut")
    public Result<List<ShortcutMenuDTO>> getShortcutMenus(@ApiIgnore JwtUser jwtUser) {
        return success(userManager.getShortcutMenus(jwtUser));
    }

    @Check
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取个人快捷菜单详情")
    @ApiImplicitParam(name = "id", value = "快捷菜单主键id", required = true, dataTypeClass = Long.class)
    @GetMapping("/shortcut/{id}")
    public Result<ShortcutMenuDTO> getShortcutMenu(@PathVariable("id") @NotNull(message = "快捷菜单主键id不能为空") Long id) {
        return success(userManager.getShortcutMenu(id));
    }

    @Log
    @Idempotent
    @Check
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "新增个人快捷菜单")
    @PostMapping("/shortcut")
    public Result<?> addShortcutMenu(@RequestBody ShortcutMenuDTO shortcutMenuDTO, @ApiIgnore JwtUser jwtUser) {
        userManager.addShortcutMenu(shortcutMenuDTO, jwtUser);
        return success();
    }

    @Log
    @Idempotent
    @Check
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新个人快捷菜单")
    @PutMapping("/shortcut")
    public Result<?> updateShortcutMenu(@RequestBody ShortcutMenuDTO shortcutMenuDTO, @ApiIgnore JwtUser jwtUser) {
        userManager.updateShortcutMenu(shortcutMenuDTO, jwtUser);
        return success();
    }

    @Log
    @Idempotent
    @Check
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "删除个人快捷菜单")
    @ApiImplicitParam(name = "shortcutMenuIds", value = "快捷菜单主键id集合", required = true, dataTypeClass = Long.class)
    @DeleteMapping("/shortcut")
    public Result<?> deleteShortcutMenu(@RequestBody @NotNull(message = "快捷菜单主键id集合不能为空")
                                        @Size(min = 1, message = "快捷菜单主键id集合元素必须大于1") List<Long> shortcutMenuIds,
                                        @ApiIgnore JwtUser jwtUser) {
        userManager.deleteShortcutMenu(shortcutMenuIds, jwtUser);
        return success();
    }


}
