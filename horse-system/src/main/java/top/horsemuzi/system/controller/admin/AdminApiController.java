package top.horsemuzi.system.controller.admin;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidThree;
import top.horsemuzi.common.validator.ValidTwo;
import top.horsemuzi.system.annotation.AloneLimiter;
import top.horsemuzi.system.annotation.Check;
import top.horsemuzi.system.annotation.Idempotent;
import top.horsemuzi.system.annotation.Log;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.admin.ApiManager;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.open.api.ApiDTO;
import top.horsemuzi.system.pojo.dto.open.api.ApiQuery;
import top.horsemuzi.system.pojo.dto.open.log.ApiLogDTO;
import top.horsemuzi.system.pojo.dto.open.log.ApiLogQuery;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * <p>
 * 开放API接口接口信息 前端控制器
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-18 22:09:28
 */
@Api(tags = "开放API接口管理服务")
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/admin/api")
public class AdminApiController extends BaseController {

    private final ApiManager apiManager;

    @Check(permissions = {"open:api:list"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取开放API接口列表")
    @GetMapping("/page")
    public Result<Page<ApiDTO>> getPageApis(ApiQuery query) {
        return success(apiManager.getPageApis(query));
    }

    @Check(permissions = {"open:api:query"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取开放API接口详情")
    @ApiImplicitParam(name = "id", value = "接口id", required = true, dataTypeClass = Long.class)
    @GetMapping("/{id}")
    public Result<ApiDTO> getApi(@PathVariable("id") @NotNull(message = "开放API接口id不能为空") Long id) {
        return success(apiManager.getApi(id));
    }

    @Log
    @Idempotent
    @Check(permissions = {"open:api:add"})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "新增开放API接口")
    @PostMapping
    public Result<?> addApi(@RequestBody @Validated(ValidOne.class) ApiDTO apiDTO) {
        apiManager.addApi(apiDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"open:api:update"})
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新开放API接口")
    @PutMapping
    public Result<?> updateApi(@RequestBody @Validated(ValidTwo.class) ApiDTO apiDTO) {
        apiManager.updateApi(apiDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"open:api:delete"})
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "删除开放API接口")
    @ApiImplicitParam(name = "apiIds", value = "API接口ID集合", required = true, dataTypeClass = Long.class)
    @DeleteMapping
    public Result<?> deleteApi(@RequestBody @NotNull(message = "API接口ID集合不能为空")
                               @Size(min = 1, message = "API接口ID集合元素必须大于1") List<Long> apiIds) {
        apiManager.deleteApi(apiIds);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"open:api:switch"})
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新API接口状态")
    @PutMapping("/status")
    public Result<?> updateApiStatus(@RequestBody @Validated({ValidThree.class}) ApiDTO apiDTO) {
        apiManager.updateApiStatus(apiDTO);
        return success();
    }

    @Log(type = Constants.METHOD.EXPORT, binaryResData = true)
    @Idempotent
    @AloneLimiter(perSecond = 3)
    @Check(permissions = {"open:api:export"})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "API接口数据导出")
    @PostMapping("/export")
    public void export(ApiQuery query, HttpServletResponse response) {
        apiManager.export(query, response);
    }

    @Log(type = Constants.METHOD.IMPORT, saveReqData = false)
    @Idempotent
    @Check(permissions = {"open:api:import"})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "批量导入API接口数据")
    @ApiImplicitParam(name = "file", value = "文件信息", required = true, dataTypeClass = MultipartFile.class)
    @PostMapping("/import")
    public Result<?> imports(@RequestParam("file") MultipartFile file, @ApiIgnore JwtUser jwtUser) {
        apiManager.imports(file, jwtUser);
        return success();
    }

    @Check(permissions = {"open:api:fetch"})
    @GetMapping("/fetch")
    @ApiImplicitParam(name = "search", value = "实时检索关键字", dataTypeClass = String.class)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "接口名称实时检索", notes = "输入框实时检索接口名称")
    public Result<List<InputRemoteFetchDTO>> fetch(@RequestParam("search") String search) {
        return success(apiManager.fetch(search));
    }

    @Check(permissions = {"open:apiLog:list"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取开放API请求日志列表")
    @GetMapping("/log/page")
    public Result<Page<ApiLogDTO>> getPageApiLogs(ApiLogQuery query) {
        return success(apiManager.getPageApiLogs(query));
    }

    @Check(permissions = {"open:apiLog:query"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取开放API请求日志详情")
    @ApiImplicitParam(name = "id", value = "日志id", required = true, dataTypeClass = Long.class)
    @GetMapping("/log/{id}")
    public Result<ApiLogDTO> getApiLog(@PathVariable("id") @NotNull(message = "开放API请求日志id不能为空") Long id) {
        return success(apiManager.getApiLog(id));
    }

    @Log
    @Idempotent
    @Check(permissions = {"open:apiLog:delete"})
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "删除开放API请求日志")
    @ApiImplicitParam(name = "apiIds", value = "API请求日志ID集合", required = true, dataTypeClass = Long.class)
    @DeleteMapping("/log")
    public Result<?> deleteApiLog(@RequestBody @NotNull(message = "API请求日志ID集合不能为空")
                                  @Size(min = 1, message = "API请求日志ID集合元素必须大于1") List<Long> apiLogIds) {
        apiManager.deleteApiLog(apiLogIds);
        return success();
    }

    @Log(type = Constants.METHOD.EXPORT, binaryResData = true)
    @Idempotent
    @AloneLimiter(perSecond = 3)
    @Check(permissions = {"open:apiLog:export"})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "API接口请求日志数据导出")
    @PostMapping("/log/export")
    public void exportApiLog(ApiLogQuery query, HttpServletResponse response) {
        apiManager.exportApiLog(query, response);
    }

    @Check(permissions = {"open:apiLog:fetch"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("log/fetch")
    @ApiImplicitParam(name = "search", value = "实时检索关键字", dataTypeClass = String.class)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "API日志链路ID实时检索", notes = "输入框实时检索链路ID")
    public Result<List<InputRemoteFetchDTO>> logFetch(@RequestParam("search") String search) {
        return success(apiManager.logFetch(search));
    }

}
