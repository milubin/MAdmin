package top.horsemuzi.system.controller.admin;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.system.annotation.Check;
import top.horsemuzi.system.annotation.Idempotent;
import top.horsemuzi.system.annotation.Log;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.admin.MonitorManager;
import top.horsemuzi.system.pojo.dto.user.OnlineUserDTO;
import top.horsemuzi.system.pojo.dto.user.OnlineUserQuery;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * 系统在线用户服务
 *
 * @author mabin
 * @date 2022/08/16 09:08
 **/
@Api(tags = "系统在线用户服务")
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/admin/online")
public class AdminOnlineUserController extends BaseController {

    private final MonitorManager monitorManager;

    @Check(permissions = {"monitor:online:list"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页系统在线用户列表")
    @GetMapping
    public Result<Page<OnlineUserDTO>> getPageOnlineUsers(OnlineUserQuery query) {
        return success(monitorManager.getPageOnlineUsers(query));
    }

    @Log
    @Idempotent
    @Check(permissions = {"monitor:online:quit"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "强制用户退出")
    @PostMapping("/quit")
    public Result<Page<OnlineUserDTO>> forceUserQuit(@RequestBody @NotNull(message = "会话标识集合不能为空")
                                                     @Size(min = 1, message = "会话标识集合元素必须大于1") List<String> tokens) {
        monitorManager.forceUserQuit(tokens);
        return success();
    }

}
