package top.horsemuzi.system.controller.admin;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.system.annotation.AloneLimiter;
import top.horsemuzi.system.annotation.Check;
import top.horsemuzi.system.annotation.Idempotent;
import top.horsemuzi.system.annotation.Log;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.admin.LogManager;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.log.LoginLogDTO;
import top.horsemuzi.system.pojo.dto.log.LoginLogQuery;
import top.horsemuzi.system.pojo.dto.log.SystemLogDTO;
import top.horsemuzi.system.pojo.dto.log.SystemLogQuery;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统操作日志 前端控制器
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-06-30 20:35:31
 */
@Api(tags = "系统日志", description = "操作日志、登录日志")
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/admin/logs")
public class AdminSystemLogController extends BaseController {

    private final LogManager logManager;

    @Check(permissions = {"system:log:list"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取系统日志列表")
    @GetMapping
    public Result<Page<SystemLogDTO>> getPageLogs(SystemLogQuery query) {
        return success(logManager.getPageLogs(query));
    }

    @Check(permissions = {"system:log:query"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取系统日志详情")
    @ApiImplicitParam(name = "id", value = "日志ID", required = true, dataTypeClass = Long.class)
    @GetMapping("/{id}")
    public Result<SystemLogDTO> getLog(@PathVariable("id") @NotNull(message = "日志ID不能为空") Long id) {
        return success(logManager.getLog(id));
    }

    @Idempotent
    @Check(permissions = {"system:log:delete"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "删除系统日志")
    @ApiImplicitParam(name = "logIds", value = "日志ID集合", required = true, dataTypeClass = Long.class)
    @DeleteMapping
    public Result<?> deleteLogs(@RequestBody @NotNull(message = "日志ID集合不能为空")
                                @Size(min = 1, message = "日志ID集合元素必须大于1") List<Long> logIds) {
        logManager.deleteLogs(logIds);
        return success();
    }

    @Log(type = Constants.METHOD.EXPORT, binaryResData = true)
    @Idempotent
    @AloneLimiter(perSecond = 3)
    @Check(permissions = {"system:log:export"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "导出数据", notes = "导出系统操作日志(CSV)")
    @PostMapping("/export")
    public void export(SystemLogQuery query, HttpServletResponse response) {
        logManager.export(query, response);
    }

    @Check(permissions = {"system:log:fetch"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("/fetch")
    @ApiImplicitParam(name = "search", value = "实时检索关键字", dataTypeClass = String.class)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "操作日志链路ID实时检索", notes = "输入框实时检索链路ID")
    public Result<List<InputRemoteFetchDTO>> logFetch(@RequestParam("search") String search) {
        return success(logManager.logFetch(search));
    }

    @Check(permissions = {"system:log:statistics"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取系统日志统计数据")
    @GetMapping("/statistics")
    public Result<Map<String, Object>> statisticsLog() {
        return success(logManager.statisticsLog());
    }

    @Check(permissions = {"system:login:list"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("/login/page")
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取登录日志列表")
    public Result<Page<LoginLogDTO>> getPageLoginLogs(LoginLogQuery query) {
        return success(logManager.getPageLoginLogs(query));
    }

    @Check(permissions = {"system:login:query"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取登录日志详情")
    @ApiImplicitParam(name = "id", value = "登录日志ID", required = true, dataTypeClass = Long.class)
    @GetMapping("/login/{id}")
    public Result<LoginLogDTO> getLoginLog(@PathVariable("id") @NotNull(message = "登录日志ID不能为空") Long id) {
        return success(logManager.getLoginLog(id));
    }

    @Idempotent
    @Check(permissions = {"system:login:delete"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "删除登录日志")
    @ApiImplicitParam(name = "loginLogIds", value = "登录日志ID集合", required = true, dataTypeClass = Long.class)
    @DeleteMapping("/login")
    public Result<?> deleteLoginLogs(@RequestBody @NotNull(message = "日志ID集合不能为空")
                                     @Size(min = 1, message = "日志ID集合元素必须大于1") List<Long> loginLogIds) {
        logManager.deleteLoginLogs(loginLogIds);
        return success();
    }

    @Log(type = Constants.METHOD.EXPORT, binaryResData = true)
    @Idempotent
    @AloneLimiter(perSecond = 3)
    @Check(permissions = {"system:login:export"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "导出登录日志数据", notes = "导出系统登录日志(CSV)")
    @PostMapping("/login/export")
    public void loginExport(LoginLogQuery query, HttpServletResponse response) {
        logManager.loginExport(query, response);
    }

    @Check(permissions = {"system:login:statistics"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取登录日志统计数据")
    @GetMapping("/login/statistics")
    public Result<Map<String, Object>> statisticsLoginLog() {
        return success(logManager.statisticsLoginLog());
    }

    @Check(permissions = {"system:login:fetch"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("/login/fetch")
    @ApiImplicitParam(name = "search", value = "实时检索关键字", dataTypeClass = String.class)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "登录日志链路ID实时检索", notes = "输入框实时检索链路ID")
    public Result<List<InputRemoteFetchDTO>> loginFetch(@RequestParam("search") String search) {
        return success(logManager.loginFetch(search));
    }

}
