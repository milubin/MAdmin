package top.horsemuzi.system.controller.admin;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.common.validator.*;
import top.horsemuzi.system.annotation.AloneLimiter;
import top.horsemuzi.system.annotation.Check;
import top.horsemuzi.system.annotation.Idempotent;
import top.horsemuzi.system.annotation.Log;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.admin.RoleManager;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.role.RoleDTO;
import top.horsemuzi.system.pojo.dto.role.RoleQuery;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-07 22:44:27
 */
@Api(tags = "角色管理")
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/admin/roles")
public class AdminRoleController extends BaseController {

    private final RoleManager roleManager;

    @Check(permissions = {"system:role:list"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("/page")
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取角色列表", notes = "分页获取角色列表")
    public Result<Page<RoleDTO>> getPageRoles(RoleQuery roleQuery) {
        return success(roleManager.getPageRoles(roleQuery));
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:role:add"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "新增角色", notes = "新增角色信息")
    @PostMapping
    public Result<?> addRole(@RequestBody @Validated(ValidOne.class) RoleDTO roleDTO) {
        roleManager.addRole(roleDTO);
        return success();
    }

    @Log()
    @Idempotent
    @Check(permissions = {"system:role:update"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新角色", notes = "更新角色信息")
    @PutMapping
    public Result<?> updateRole(@RequestBody @Validated(ValidTwo.class) RoleDTO roleDTO) {
        roleManager.updateRole(roleDTO);
        return success();
    }

    @Idempotent
    @Log()
    @Check(permissions = {"system:role:switch"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新角色状态", notes = "切换角色状态")
    @PutMapping("/status")
    public Result<?> updateRoleStatus(@RequestBody @Validated({ValidFive.class}) RoleDTO roleDTO) {
        roleManager.updateRoleStatus(roleDTO);
        return success();
    }

    @Check(permissions = {"system:role:query"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取角色详情", notes = "根据id获取角色详情")
    @ApiImplicitParam(name = "id", value = "角色ID", required = true, dataTypeClass = Long.class)
    @GetMapping("/{id}")
    public Result<RoleDTO> getRole(@PathVariable("id") @NotNull(message = "角色ID不能为空") Long id) {
        return success(roleManager.getRole(id));
    }

    @Check(permissions = {"system:role:list"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取角色信息列表", notes = "角色下拉框/用户分配角色列表")
    @GetMapping("/list")
    public Result<List<RoleDTO>> getRoles() {
        return success(roleManager.getRoles());
    }

    @Idempotent
    @Log()
    @Check(permissions = {"system:role:delete"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "批量删除角色信息", notes = "批量删除角色信息")
    @ApiImplicitParam(name = "roleIds", value = "角色ID集合", required = true, dataTypeClass = Long.class)
    @DeleteMapping
    public Result<RoleDTO> deleteRoles(@RequestBody @NotNull(message = "角色ID集合不能为空")
                                       @Size(min = 1, message = "角色ID集合元素必须大于1") List<Long> roleIds) {
        roleManager.deleteRoles(roleIds);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:role:deploy"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "分配用户", notes = "为角色分配用户")
    @PostMapping("/deploy/user")
    public Result<?> deployRoleToUser(@RequestBody @Validated(ValidThree.class) RoleDTO roleDTO) {
        roleManager.deployRoleToUser(roleDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:role:deploy"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "分配权限", notes = "为角色分配权限")
    @PostMapping("/deploy/permission")
    public Result<?> deployPermissionToRole(@RequestBody @Validated(ValidFour.class) RoleDTO roleDTO) {
        roleManager.deployPermissionToRole(roleDTO);
        return success();
    }

    @Log()
    @Idempotent
    @Check(permissions = {"system:role:cancel"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "取消用户的角色授权", notes = "取消用户的角色授权(兼容批量)")
    @PutMapping("/cancel")
    public Result<?> cancelRoleUserAuth(@RequestBody RoleDTO roleDTO) {
        roleManager.cancelRoleUserAuth(roleDTO);
        return success();
    }

    @Check(permissions = {"system:role:fetch"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("/fetch")
    @ApiImplicitParam(name = "search", value = "实时检索关键字", dataTypeClass = String.class)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "角色名称实时检索", notes = "输入框实时检索角色名称")
    public Result<List<InputRemoteFetchDTO>> fetch(@RequestParam("search") String search) {
        return success(roleManager.fetch(search));
    }

    @Log(type = Constants.METHOD.EXPORT, binaryResData = true)
    @Idempotent
    @AloneLimiter(perSecond = 3)
    @Check(permissions = {"system:role:export"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "导出角色信息", notes = "角色和菜单权限信息")
    @PostMapping("/export")
    public void export(RoleQuery query, HttpServletResponse response) {
        roleManager.export(query, response);
    }

}
