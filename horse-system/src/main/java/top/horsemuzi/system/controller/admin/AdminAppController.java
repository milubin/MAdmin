package top.horsemuzi.system.controller.admin;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.common.validator.ValidFour;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidThree;
import top.horsemuzi.common.validator.ValidTwo;
import top.horsemuzi.system.annotation.AloneLimiter;
import top.horsemuzi.system.annotation.Check;
import top.horsemuzi.system.annotation.Idempotent;
import top.horsemuzi.system.annotation.Log;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.admin.AppManager;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.open.app.AppDTO;
import top.horsemuzi.system.pojo.dto.open.app.AppQuery;
import top.horsemuzi.system.pojo.dto.open.category.ApiCategoryDTO;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * <p>
 * 开放API应用信息 前端控制器
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-18 22:09:28
 */
@Api(tags = "开放API应用管理服务")
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/admin/app")
public class AdminAppController extends BaseController {

    private final AppManager appManager;

    @Check(permissions = {"open:app:list"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取开放API应用列表")
    @GetMapping("/page")
    public Result<Page<AppDTO>> getPageApps(AppQuery query) {
        return success(appManager.getPageApps(query));
    }

    @Check(permissions = {"open:app:query"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取开放API应用详情")
    @ApiImplicitParam(name = "id", value = "应用id", required = true, dataTypeClass = Long.class)
    @GetMapping("/{id}")
    public Result<AppDTO> getApp(@PathVariable("id") @NotNull(message = "开放API应用id不能为空") Long id) {
        return success(appManager.getApp(id));
    }

    @Log
    @Idempotent
    @Check(permissions = {"open:app:add"})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "新增开放API应用")
    @PostMapping
    public Result<?> addApp(@RequestBody @Validated(ValidOne.class) AppDTO appDTO) {
        appManager.addApp(appDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"open:app:update"})
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新开放API应用")
    @PutMapping
    public Result<?> updateApp(@RequestBody @Validated(ValidTwo.class) AppDTO appDTO) {
        appManager.updateApp(appDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"open:app:delete"})
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "删除开放API应用")
    @ApiImplicitParam(name = "appIds", value = "API应用ID集合", required = true, dataTypeClass = Long.class)
    @DeleteMapping
    public Result<?> deleteApp(@RequestBody @NotNull(message = "API应用ID集合不能为空")
                               @Size(min = 1, message = "API应用ID集合元素必须大于1") List<Long> appIds) {
        appManager.deleteApp(appIds);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"open:app:approval"})
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新API应用审批状态")
    @PutMapping("/approval")
    public Result<?> updateAppStatus(@RequestBody @Validated({ValidThree.class}) AppDTO appDTO) {
        appManager.updateAppStatus(appDTO);
        return success();
    }

    @Log(type = Constants.METHOD.EXPORT, binaryResData = true)
    @Idempotent
    @AloneLimiter(perSecond = 3)
    @Check(permissions = {"open:app:export"})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "API应用数据导出")
    @PostMapping("/export")
    public void export(AppQuery query, HttpServletResponse response) {
        appManager.export(query, response);
    }

    @Check(permissions = {"open:app:fetch"})
    @GetMapping("/fetch")
    @ApiImplicitParam(name = "search", value = "实时检索关键字", dataTypeClass = String.class)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "应用名称实时检索", notes = "输入框实时检索应用名称")
    public Result<List<InputRemoteFetchDTO>> fetch(@RequestParam("search") String search) {
        return success(appManager.fetch(search));
    }

    @Check(permissions = {"open:app:category"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取开放API接口分类列表")
    @ApiImplicitParam(name = "appId", value = "开放API应用ID", readOnly = true, dataTypeClass = Long.class)
    @GetMapping("/category/{appId}")
    public Result<List<ApiCategoryDTO>> listApiCategory(@PathVariable("appId") @NotNull(message = "开放API应用ID不能为空") Long appId) {
        return success(appManager.listApiCategory(appId));
    }

    @Check(permissions = {"open:app:secret"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取API应用密钥AppSecret")
    @ApiImplicitParam(name = "id", value = "应用id", required = true, dataTypeClass = Long.class)
    @GetMapping("/secret/{id}")
    public Result<String> getAppSecret(@PathVariable("id") @NotNull(message = "开放API应用id不能为空") Long id) {
        return success(appManager.getAppSecret(id));
    }

    @Log
    @Idempotent
    @Check(permissions = {"open:app:secret"})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "API应用密钥AppSecret一键发送至邮箱", notes = "默认为申请应用时的邮箱,支持自定义")
    @PostMapping("/secret/send/email")
    public Result<?> sendSecret(@RequestBody @Validated({ValidFour.class}) AppDTO appDTO) {
        appManager.sendSecret(appDTO);
        return success();
    }

}
