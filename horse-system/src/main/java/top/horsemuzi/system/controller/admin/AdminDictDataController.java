package top.horsemuzi.system.controller.admin;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;
import top.horsemuzi.system.annotation.Check;
import top.horsemuzi.system.annotation.Idempotent;
import top.horsemuzi.system.annotation.Log;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.admin.DictManager;
import top.horsemuzi.system.pojo.dto.dict.DictDataDTO;
import top.horsemuzi.system.pojo.dto.dict.DictDataQuery;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * <p>
 * 字典数据表 前端控制器
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-06-20 11:04:36
 */
@Api(tags = "字典数据服务")
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/admin/dict/data")
public class AdminDictDataController extends BaseController {

    private final DictManager dictManager;

    @Check(permissions = "system:dict:list", role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取字典数据列表", notes = "分页获取字典数据列表")
    @GetMapping
    public Result<Page<DictDataDTO>> getPageDictDatas(DictDataQuery dictDataQuery) {
        return success(dictManager.getPageDictDatas(dictDataQuery));
    }

    @Log
    @Idempotent
    @Check(permissions = "system:dict:add", role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "新增字典数据", notes = "新增对应类型的字典数据")
    @PostMapping
    public Result<?> addDictData(@RequestBody @Validated(ValidOne.class) DictDataDTO dictDataDTO) {
        dictManager.addDictData(dictDataDTO);
        return success();
    }

    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取字典数据列表", notes = "获取对应类型的字典数据列表")
    @ApiImplicitParam(name = "type", value = "字典类型", required = true, dataTypeClass = String.class)
    @GetMapping("/list/{type}")
    public Result<List<DictDataDTO>> getDictDatas(@PathVariable("type") @Validated @NotBlank(message = "字典类型不能为空") String type) {
        return success(dictManager.getDictDatas(type));
    }

    @Check(permissions = "system:dict:query", role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取字典数据详情", notes = "获取对应类型的具体字典数据详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "字典数据ID", required = true, dataTypeClass = Long.class),
            @ApiImplicitParam(name = "type", value = "字典类型", required = true, dataTypeClass = String.class)
    })
    @GetMapping("/{id}")
    public Result<DictDataDTO> getDictData(@PathVariable("id") @NotNull(message = "字典数据ID不能为空") Long id,
                                           @RequestParam("type") @NotBlank(message = "字典类型不能为空") String type) {
        return success(dictManager.getDictData(id, type));
    }

    @Log()
    @Idempotent
    @Check(permissions = "system:dict:update", role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新字典数据", notes = "更新对应类型的字典数据")
    @PutMapping
    public Result<?> updateDictData(@RequestBody @Validated(ValidTwo.class) DictDataDTO dictDataDTO) {
        dictManager.updateDictData(dictDataDTO);
        return success();
    }

    @Log()
    @Idempotent
    @Check(permissions = "system:dict:delete", role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "批量删除字典数据", notes = "批量删除对应类型的字典数据")
    @ApiImplicitParam(name = "dictDataIds", value = "字典数据ID集合", required = true, dataTypeClass = Long.class)
    @DeleteMapping
    public Result<?> deleteDictDatas(@RequestBody @NotNull(message = "字典数据ID集合不能为空")
                                     @Size(min = 1, message = "字典数据ID集合元素必须大于1") List<Long> dictDataIds) {
        dictManager.deleteDictDatas(dictDataIds);
        return success();
    }

}
