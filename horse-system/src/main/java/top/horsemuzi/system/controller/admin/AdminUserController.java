package top.horsemuzi.system.controller.admin;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.common.validator.*;
import top.horsemuzi.system.annotation.AloneLimiter;
import top.horsemuzi.system.annotation.Check;
import top.horsemuzi.system.annotation.Idempotent;
import top.horsemuzi.system.annotation.Log;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.admin.UserManager;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.user.UserDTO;
import top.horsemuzi.system.pojo.dto.user.UserQuery;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * <p>
 * 管理用户表 前端控制器
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-07 22:44:27
 */
@Api(tags = "用户管理")
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/admin/users")
public class AdminUserController extends BaseController {

    private final UserManager userManager;

    @Check(permissions = {"system:user:list"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取用户列表")
    @GetMapping("/page")
    public Result<Page<UserDTO>> getPageUsers(UserQuery userQuery) {
        return success(userManager.getPageUsers(userQuery));
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:user:add"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "新增用户")
    @PostMapping
    public Result<?> addUser(@RequestBody @Validated({ValidOne.class}) UserDTO userDTO) {
        userManager.addUser(userDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:user:update"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新用户")
    @PutMapping
    public Result<?> updateUser(@RequestBody @Validated({ValidTwo.class}) UserDTO userDTO) {
        userManager.updateUser(userDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:user:switch"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新账号状态")
    @PutMapping("/status")
    public Result<?> updateUserStatus(@RequestBody @Validated({ValidFour.class}) UserDTO userDTO) {
        userManager.updateUserStatus(userDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:user:unlock"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "解除账号锁定")
    @PutMapping("/unlock")
    public Result<?> unlockUser(@RequestBody @Validated({ValidFive.class}) UserDTO userDTO) {
        userManager.unlockUser(userDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:user:delete"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "批量删除用户")
    @ApiImplicitParam(name = "userIds", value = "用户ID集合", required = true, dataTypeClass = Long.class)
    @DeleteMapping
    public Result<?> deleteUsers(@RequestBody @NotNull(message = "用户ID集合不能为空")
                                 @Size(min = 1, message = "用户ID集合元素必须大于1") List<Long> userIds) {
        userManager.deleteUsers(userIds);
        return success();
    }

    @Check(permissions = {"system:user:query"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取用户信息")
    @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataTypeClass = Long.class)
    @GetMapping("/{id}")
    public Result<UserDTO> getUser(@PathVariable("id") @NotNull(message = "用户ID不能为空") Long userId) {
        return success(userManager.getUser(userId));
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:user:reset"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "重置用户密码")
    @PutMapping("/reset")
    public Result<?> resetPassword(@RequestBody @Validated({ValidThree.class}) UserDTO userDTO) {
        userManager.resetPassword(userDTO);
        return success();
    }

    @Log(type = Constants.METHOD.EXPORT, binaryResData = true)
    @Idempotent
    @AloneLimiter(perSecond = 3)
    @Check(permissions = {"system:user:export"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "导出用户信息数据")
    @PostMapping("/export")
    public void export(UserQuery userQuery, HttpServletResponse response) {
        userManager.export(userQuery, response);
    }

    @Log(type = Constants.METHOD.IMPORT, saveReqData = false)
    @Idempotent
    @Check(permissions = {"system:user:import"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "导入数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", value = "表单文件", required = true, dataTypeClass = MultipartFile.class),
            @ApiImplicitParam(name = "flag", value = "是否同步更新已存在用户信息标识: 0-不更新, 1-更新", required = true, dataTypeClass =
                    Integer.class)
    })
    @PostMapping("/import")
    public Result<?> imports(@RequestParam("file") MultipartFile file, @RequestParam("flag") Integer flag,
                             @ApiIgnore JwtUser jwtUser) {
        userManager.imports(file, flag, jwtUser);
        return success();
    }

    @Check(permissions = {"system:user:list"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("/list")
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取全部用户列表")
    public Result<List<UserDTO>> getUsers(UserQuery userQuery) {
        return success(userManager.getUsers(userQuery));
    }

    @Check(permissions = {"system:user:fetch"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("/fetch")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "search", value = "实时检索关键字", dataTypeClass = String.class),
            @ApiImplicitParam(name = "filedType", value = "实时检索字段类型, username: 1, phone: 2, email: 3", required = true, dataTypeClass = String.class)
    })
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "实时检索", notes = "用户名输入框实时检索")
    public Result<List<InputRemoteFetchDTO>> fetch(String search, String filedType) {
        return success(userManager.fetch(search, filedType));
    }

}
