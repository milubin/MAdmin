package top.horsemuzi.system.controller.admin;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;
import top.horsemuzi.system.annotation.Check;
import top.horsemuzi.system.annotation.Idempotent;
import top.horsemuzi.system.annotation.Log;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.admin.PermissionManager;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.permission.PermissionDTO;
import top.horsemuzi.system.pojo.dto.permission.PermissionQuery;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 菜单权限表 前端控制器
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-07 22:44:27
 */
@Api(tags = "权限菜单管理服务")
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/admin/permissions")
public class AdminPermissionController extends BaseController {

    private final PermissionManager permissionManager;

    @Check(permissions = {"system:permission:list"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取权限菜单列表", notes = "获取权限菜单列表")
    @GetMapping
    public Result<List<PermissionDTO>> getPermissionMenus(PermissionQuery permissionQuery) {
        return success(permissionManager.getPermissionMenus(permissionQuery));
    }

    @Check(permissions = {"system:permission:query"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取权限菜单树", notes = "角色新增/更新时获取权限菜单树")
    @ApiImplicitParam(name = "roleId", value = "角色id", required = true, dataTypeClass = Long.class)
    @GetMapping("/tree")
    public Result<Map<String, Object>> getPermissionTreeMenus(@RequestParam(value = "roleId", required = false) Long roleId) {
        return success(permissionManager.getPermissionTreeMenus(roleId));
    }

    @Check(permissions = {"system:permission:query"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取权限菜单详情", notes = "根据权限菜单id获取权限菜单详情")
    @ApiImplicitParam(name = "id", value = "权限菜单id", required = true, dataTypeClass = Long.class)
    @GetMapping("/{id}")
    public Result<PermissionDTO> getPermission(@PathVariable("id") @NotNull(message = "权限菜单id不能为空") Long id) {
        return success(permissionManager.getPermission(id));
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:permission:add"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "新增权限菜单")
    @PostMapping
    public Result<?> addPermission(@RequestBody @Validated({ValidOne.class}) PermissionDTO permissionDTO) {
        permissionManager.addPermission(permissionDTO);
        return success();
    }

    @Log()
    @Idempotent
    @Check(permissions = {"system:permission:update"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新权限菜单")
    @PutMapping
    public Result<?> updatePermission(@RequestBody @Validated({ValidTwo.class}) PermissionDTO permissionDTO) {
        permissionManager.updatePermission(permissionDTO);
        return success();
    }

    @Log()
    @Idempotent
    @Check(permissions = {"system:permission:delete"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "删除权限菜单")
    @ApiImplicitParam(name = "id", value = "权限菜单id", required = true, dataTypeClass = Long.class)
    @DeleteMapping("/{id}")
    public Result<?> deletePermission(@PathVariable("id") @NotNull(message = "权限菜单id不能为空") Long id) {
        permissionManager.deletePermission(id);
        return success();
    }

    @Check(permissions = {"system:permission:fetch"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("/fetch")
    @ApiImplicitParam(name = "search", value = "实时检索关键字", dataTypeClass = String.class)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "菜单名称实时检索", notes = "输入框实时检索菜单名称")
    public Result<List<InputRemoteFetchDTO>> fetch(@RequestParam("search") String search) {
        return success(permissionManager.fetch(search));
    }
}
