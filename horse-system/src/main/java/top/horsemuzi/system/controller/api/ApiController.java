package top.horsemuzi.system.controller.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.LimiterTypeEnum;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;
import top.horsemuzi.system.annotation.Anonymous;
import top.horsemuzi.system.annotation.Limiter;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.open.OpenManager;
import top.horsemuzi.system.pojo.dto.open.api.ApiParams;

/**
 * <p>
 * 开放API控制 前端控制器
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-07 22:44:27
 */
@Api(tags = "开放API接口服务")
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class ApiController extends BaseController {

    private final OpenManager openManager;

    @Anonymous(sign = false)
    @Limiter(type = LimiterTypeEnum.SPEL, key = "#apiParams.appKey")
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "生成access_token入口", notes = "每个应用每秒限制10次")
    @PostMapping("/token")
    public Result<String> apiToken(@RequestBody @Validated({ValidOne.class}) ApiParams apiParams) {
        return success(openManager.apiToken(apiParams));
    }

    @Anonymous(sign = false)
    @Limiter(type = LimiterTypeEnum.SPEL, key = "#apiParams.appKey", rate = 5)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "外部API统一入口", notes = "每个应用每秒限制5次")
    @PostMapping("/core")
    public Result<?> apiCore(@RequestBody @Validated({ValidTwo.class}) ApiParams apiParams) {
        return success(openManager.apiCore(apiParams));
    }

}
