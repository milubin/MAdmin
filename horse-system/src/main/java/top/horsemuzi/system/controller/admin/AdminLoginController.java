package top.horsemuzi.system.controller.admin;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.system.annotation.Anonymous;
import top.horsemuzi.system.annotation.Check;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.admin.LoginManager;
import top.horsemuzi.system.pojo.dto.login.AdminLoginDTO;
import top.horsemuzi.system.pojo.dto.login.ResetDTO;
import top.horsemuzi.system.pojo.dto.login.RouterDTO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 登录控制 前端控制器
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-07 22:44:27
 */
@Api(tags = "系统登录服务")
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/admin")
public class AdminLoginController extends BaseController {

    private final LoginManager loginManager;

    @Anonymous
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "用户登录")
    @PostMapping("/login")
    public Result<Map<String, String>> login(@Validated @RequestBody AdminLoginDTO adminLoginDTO, HttpServletRequest request) {
        return success(loginManager.login(adminLoginDTO, request));
    }

    @Anonymous
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "安全退出")
    @GetMapping("/logout")
    public Result<?> logout(HttpServletRequest request) {
        loginManager.logout(request);
        return success();
    }

    @Check
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取用户角色权限信息")
    @GetMapping("/getInfo")
    public Result<Map<String, Object>> getInfo(@ApiIgnore JwtUser jwtUser) {
        return success(loginManager.getInfo(jwtUser));
    }

    @Check
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取用户菜单权限路由信息")
    @GetMapping("/getRouters")
    public Result<List<RouterDTO>> getRouters(@ApiIgnore JwtUser jwtUser) {
        return success(loginManager.getRouters(jwtUser));
    }

    @Anonymous
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "用户忘记密码重置")
    @PutMapping("/reset")
    public Result<?> reset(@RequestBody @Validated({ValidOne.class}) ResetDTO resetDTO) {
        loginManager.reset(resetDTO);
        return success();
    }


}
