package top.horsemuzi.system.controller.common;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.LimiterTypeEnum;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.common.validator.custom.annotation.Phone;
import top.horsemuzi.system.annotation.AloneLimiter;
import top.horsemuzi.system.annotation.Anonymous;
import top.horsemuzi.system.annotation.Limiter;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.common.CommonManager;
import top.horsemuzi.system.pojo.dto.division.DivisionDTO;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;

/**
 * 公共控制处理
 *
 * @author mabin
 * @date 2022/05/09 21:36
 **/
@Api(tags = "系统公共服务")
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/common")
public class CommonController extends BaseController {

    private final CommonManager commonManager;


    @Anonymous
    @AloneLimiter(perSecond = 3)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "生成验证码", notes = "基于HuTool动态生成验证码")
    @GetMapping("/kaptcha/image")
    public Result<Map<String, Object>> generateKaptcha() {
        return success(commonManager.generateKaptcha());
    }

    @Anonymous
    @AloneLimiter(perSecond = 3)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取Gitee提交信息", notes = "基于Jsoup网页解析处理")
    @GetMapping("/gitee/commits")
    public Result<Map<String, Object>> getGiteeCommits() {
        return success(commonManager.getGiteeCommits());
    }

    @Anonymous
    @AloneLimiter(perSecond = 3)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取区划级联数据", notes = "懒加载模式")
    @ApiImplicitParam(name = "id", value = "区划数据父id, 默认: 0", dataTypeClass = Long.class, defaultValue = "0")
    @GetMapping("/division/{id}")
    public Result<List<DivisionDTO>> getDivisionCascader(@PathVariable("id") Long id) {
        return success(commonManager.getDivisionCascader(id));
    }

    @Anonymous
    @AloneLimiter(perSecond = 2)
    @Limiter(type = LimiterTypeEnum.SPEL, key = "#phone", rate = 1L)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "发送短信验证码")
    @ApiImplicitParam(name = "phone", value = "手机号", dataTypeClass = String.class, required = true)
    @GetMapping("/send/sms")
    public Result<List<DivisionDTO>> sendSms(@RequestParam("phone") @NotBlank(message = "手机号不能为空") @Phone(message = "手机号不合法") String phone) {
        commonManager.sendSms(phone);
        return success();
    }

    @Anonymous
    @AloneLimiter(perSecond = 3)
    @Limiter(type = LimiterTypeEnum.SPEL, key = "#email", rate = 2L)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "发送邮箱验证码")
    @ApiImplicitParam(name = "email", value = "邮箱号", dataTypeClass = String.class, required = true)
    @GetMapping("/send/email")
    public Result<List<DivisionDTO>> sendEmail(@RequestParam("email") @NotBlank(message = "邮箱号不能为空") @Email(message = "邮箱不合法") String email) {
        commonManager.sendEmail(email);
        return success();
    }

    @Anonymous
    @AloneLimiter(perSecond = 3)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "系统模板文件下载")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "group", value = "系统配置组别group", required = true, dataTypeClass = String.class),
            @ApiImplicitParam(name = "key", value = "配置键名key", required = true, dataTypeClass = String.class),
    })
    @PostMapping("/download/template")
    public Result<?> template(@RequestParam("group") @NotBlank(message = "系统配置组别group不能为空") String group,
                              @RequestParam("key") @NotBlank(message = "配置键名key不能为空") String key,
                              HttpServletResponse response) {
        commonManager.template(group, key, response);
        return success();
    }

}
