package top.horsemuzi.system.controller.admin;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidThree;
import top.horsemuzi.common.validator.ValidTwo;
import top.horsemuzi.system.annotation.AloneLimiter;
import top.horsemuzi.system.annotation.Check;
import top.horsemuzi.system.annotation.Idempotent;
import top.horsemuzi.system.annotation.Log;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.admin.DictManager;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.dict.DictTypeDTO;
import top.horsemuzi.system.pojo.dto.dict.DictTypeQuery;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * <p>
 * 字典类型表 前端控制器
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-06-20 11:04:36
 */
@Api(tags = "字典类型服务")
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/admin/dict/type")
public class AdminDictTypeController extends BaseController {

    private final DictManager dictManager;

    @Check(permissions = {"system:dict:list"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取字典类型列表")
    public Result<Page<DictTypeDTO>> getPageDictTypes(DictTypeQuery dictTypeQuery) {
        return success(dictManager.getPageDictTypes(dictTypeQuery));
    }

    @Check(permissions = {"system:dict:query"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("/{type}")
    @ApiImplicitParam(name = "type", value = "字典类型", required = true, dataTypeClass = Long.class)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取字典类型")
    public Result<DictTypeDTO> getDictType(@PathVariable("type") @NotBlank(message = "字典类型不能为空") String type) {
        return success(dictManager.getDictType(type));
    }

    @Check(permissions = {"system:dict:list"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("/list")
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取字典类型列表")
    public Result<List<DictTypeDTO>> getDictTypeList() {
        return success(dictManager.getDictTypeList());
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:dict:add"}, role = Constants.ROLE_CODE.ADMIN)
    @PostMapping
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "新增字典类型", notes = "新增字典类型")
    public Result<?> addDictType(@RequestBody @Validated(ValidOne.class) DictTypeDTO dictTypeDTO) {
        dictManager.addDictType(dictTypeDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:dict:update"}, role = Constants.ROLE_CODE.ADMIN)
    @PutMapping
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新字典类型")
    public Result<?> updateDictType(@RequestBody @Validated(ValidTwo.class) DictTypeDTO dictTypeDTO) {
        dictManager.updateDictType(dictTypeDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:dict:delete"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "批量删除字典类型")
    @ApiImplicitParam(name = "dictTypeIds", value = "字典类型ID集合", required = true, dataTypeClass = Long.class)
    @DeleteMapping
    public Result<Boolean> deleteDictTypes(@RequestBody @NotNull(message = "字典类型ID集合不能为空")
                                           @Size(min = 1, message = "字典类型ID集合元素必须大于1") List<Long> dictTypeIds) {
        dictManager.deleteDictTypes(dictTypeIds);
        return success();
    }

    @Log(type = Constants.METHOD.EXPORT, binaryResData = true)
    @Idempotent
    @AloneLimiter(perSecond = 3)
    @Check(permissions = {"system:dict:export"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "导出数据", notes = "字典类型数据导出数据")
    @PostMapping("/export")
    public void export(DictTypeQuery dictTypeQuery, HttpServletResponse response) {
        dictManager.export(dictTypeQuery, response);
    }

    @Idempotent
    @Log()
    @Check(permissions = "system:dict:switch", role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新字典类型状态")
    @PutMapping("/status")
    public Result<?> switchDictTypeStatus(@RequestBody @Validated({ValidThree.class}) DictTypeDTO dictTypeDTO) {
        dictManager.switchDictTypeStatus(dictTypeDTO);
        return success();
    }

    @Log(type = Constants.METHOD.REFRESH)
    @Check(permissions = {"system:dict:refresh"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "刷新缓存", notes = "刷新字典数据缓存信息")
    @PutMapping("/refresh")
    public Result<?> refresh() {
        dictManager.refresh();
        return success();
    }

    @Check(permissions = {"system:dict:fetch"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("/fetch")
    @ApiImplicitParam(name = "search", value = "实时检索关键字", dataTypeClass = String.class)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "字典类型实时检索", notes = "输入框实时检索字典类型")
    public Result<List<InputRemoteFetchDTO>> fetch(@RequestParam("search") String search) {
        return success(dictManager.fetch(search));
    }

}
