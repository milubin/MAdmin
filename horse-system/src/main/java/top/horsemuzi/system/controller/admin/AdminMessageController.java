package top.horsemuzi.system.controller.admin;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.system.annotation.AloneLimiter;
import top.horsemuzi.system.annotation.Check;
import top.horsemuzi.system.annotation.Idempotent;
import top.horsemuzi.system.annotation.Log;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.admin.MessageManager;
import top.horsemuzi.system.pojo.dto.message.MessageChannelDTO;
import top.horsemuzi.system.pojo.dto.message.MessageDTO;
import top.horsemuzi.system.pojo.dto.message.MessageQuery;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 消息信息表 前端控制器
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-09-30 10:59:09
 */
@Api(tags = "消息管理")
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/admin/message")
public class AdminMessageController extends BaseController {

    private final MessageManager messageManager;

    @Check(permissions = {"system:message:list"}, role = Constants.ROLE_CODE.ADMIN)
    @GetMapping("/page")
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取消息记录列表")
    public Result<Page<MessageDTO>> getPageMessages(MessageQuery query) {
        return success(messageManager.getPageMessages(query));
    }

    @Check(permissions = {"system:message:query"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取消息记录详情")
    @ApiImplicitParam(name = "id", value = "消息记录ID", required = true, dataTypeClass = Long.class)
    @GetMapping("/{id}")
    public Result<MessageDTO> getMessage(@PathVariable("id") @NotNull(message = "消息记录ID不能为空") Long messageId) {
        return success(messageManager.getMessage(messageId));
    }

    @Log()
    @Idempotent
    @Check(permissions = {"system:message:delete"}, role = Constants.ROLE_CODE.ADMIN)
    @DeleteMapping
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "删除消息日志")
    public Result<?> deleteMessage(@RequestBody @NotNull(message = "消息记录ID集合不能为空")
                                   @Size(min = 1, message = "消息记录ID集合元素必须大于1") List<Long> messageIds) {
        messageManager.deleteMessage(messageIds);
        return success();
    }

    @Log(type = Constants.METHOD.EXPORT, binaryResData = true)
    @Idempotent
    @AloneLimiter(perSecond = 3)
    @Check(permissions = {"system:message:export"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "导出消息记录数据", notes = "导出消息记录数据(CSV)")
    @PostMapping("/export")
    public void export(MessageQuery query, HttpServletResponse response) {
        messageManager.export(query, response);
    }

    @Check(permissions = {"system:message:statistics"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取消息记录统计数据")
    @GetMapping("/statistics")
    public Result<Map<String, Object>> statisticsMessage() {
        return success(messageManager.statisticsMessage());
    }

    @Check(permissions = {"system:message:setting"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取消息渠道列表")
    @GetMapping("/channel")
    public Result<List<MessageChannelDTO>> getMessageChannels() {
        return success(messageManager.getMessageChannels());
    }

    @Log
    @Idempotent
    @Check(permissions = {"system:message:setting"}, role = Constants.ROLE_CODE.ADMIN)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "保存消息渠道")
    @PostMapping("/channel")
    public Result<?> addMessageChannel(@RequestBody @Validated({ValidOne.class}) MessageChannelDTO messageChannelDTO) {
        messageManager.addMessageChannel(messageChannelDTO);
        return success();
    }


}
