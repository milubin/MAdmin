package top.horsemuzi.system.controller.admin;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidThree;
import top.horsemuzi.common.validator.ValidTwo;
import top.horsemuzi.system.annotation.*;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.manager.admin.GenerateManager;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.generate.*;
import top.horsemuzi.system.pojo.dto.permission.PermissionDTO;
import top.horsemuzi.system.pojo.dto.role.RoleDTO;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 代码生成业务 前端控制器
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-08 15:12:56
 */
@Api(tags = "代码生成服务")
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/admin/generate")
public class AdminGenerateController extends BaseController {

    private final GenerateManager generateManager;

    @Check(permissions = {"generate:datasource:list"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取数据源列表")
    @GetMapping("/datasource")
    public Result<Page<DatasourceDTO>> getPageDatasource(DataSourceQuery query) {
        return success(generateManager.getPageDatasource(query));
    }

    @Check(permissions = {"generate:datasource:query"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取数据源详情")
    @ApiImplicitParam(name = "datasourceId", value = "数据源表id", required = true, dataTypeClass = Long.class)
    @GetMapping("/datasource/{datasourceId}")
    public Result<DatasourceDTO> getDatasource(@PathVariable("datasourceId") @NotNull(message = "数据源表id不能为空") Long datasourceId) {
        return success(generateManager.getDatasource(datasourceId));
    }

    @Check(permissions = {"generate:datasource:list"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取数据源列表")
    @GetMapping("/datasource/list")
    public Result<List<DatasourceDTO>> getDatasourceList() {
        return success(generateManager.getDatasourceList());
    }

    @Log
    @Idempotent
    @Check(permissions = {"generate:datasource:add"})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "新增数据源")
    @PostMapping("/datasource")
    public Result<?> addDataSource(@RequestBody @Validated({ValidOne.class}) DatasourceDTO datasourceDTO) {
        generateManager.addDataSource(datasourceDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"generate:datasource:update"})
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新数据源")
    @PutMapping("/datasource")
    public Result<?> updateDataSource(@RequestBody @Validated({ValidTwo.class}) DatasourceDTO datasourceDTO) {
        generateManager.updateDataSource(datasourceDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"generate:datasource:delete"})
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "删除数据源")
    @ApiImplicitParam(name = "datasourceIds", value = "数据源ID集合", required = true, dataTypeClass = Long.class)
    @DeleteMapping("/datasource")
    public Result<?> deleteDataSource(@RequestBody @NotNull(message = "数据源ID集合不能为空")
                                      @Size(min = 1, message = "数据源ID集合元素必须大于1") List<Long> datasourceIds) {
        generateManager.deleteDataSource(datasourceIds);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"generate:datasource:connect"})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "测试连接数据源")
    @PostMapping("/datasource/connect")
    public Result<?> connectDataSource(@RequestBody @Validated({ValidThree.class}) DatasourceDTO datasourceDTO) {
        generateManager.connectDataSource(datasourceDTO);
        return success();
    }

    @Check(permissions = {"generate:datasource:fetch"})
    @GetMapping("/datasource/fetch")
    @ApiImplicitParam(name = "search", value = "实时检索关键字", dataTypeClass = String.class)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "数据源名称实时检索", notes = "输入框实时检索数据源名称")
    public Result<List<InputRemoteFetchDTO>> fetchDatasource(@RequestParam("search") String search) {
        return success(generateManager.fetchDatasource(search));
    }

    @Idempotent
    @Check(permissions = {"generate:datasource:databases"})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "获取数据源的所有数据库名称")
    @PostMapping("/datasource/databases")
    public Result<List<String>> getDatabases(@RequestBody @Validated({ValidThree.class}) DatasourceDTO datasourceDTO) {
        return success(generateManager.getDatabases(datasourceDTO));
    }

    @Idempotent
    @Check(permissions = {"generate:datasource:tables"})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "获取数据库所有表信息")
    @PostMapping("/datasource/tables")
    public Result<List<SchemaTableDTO>> getSchemaTables(@RequestParam("database") @NotBlank(message = "数据库名称不能为空") String database,
                                                        @RequestParam("datasourceId") @NotNull(message = "数据源主键id不能为空") Long datasourceId) {
        return success(generateManager.getSchemaTables(database, datasourceId));
    }

    @Check(permissions = {"generate:columnConfig:list"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取字段类型关联列表")
    @GetMapping("/columnConfig")
    public Result<Page<ColumnConfigDTO>> getPageColumnConfig(ColumnConfigQuery query) {
        return success(generateManager.getPageColumnConfig(query));
    }

    @Check(permissions = {"generate:columnConfig:query"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取字段类型关联详情")
    @ApiImplicitParam(name = "columnConfigId", value = "字段类型关联id", required = true, dataTypeClass = Long.class)
    @GetMapping("/columnConfig/{columnConfigId}")
    public Result<ColumnConfigDTO> getColumnConfig(@PathVariable("columnConfigId") @NotNull(message = "字段类型关联id不能为空") Long columnConfigId) {
        return success(generateManager.getColumnConfig(columnConfigId));
    }

    @Check(permissions = {"generate:columnConfig:list"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取字段列表", notes = "针对每种类型组装字符串列表, type=0返回DB字段类型列表, type=1返回JAVA字段类型列表")
    @ApiImplicitParam(name = "type", value = "字段类型", required = true, dataTypeClass = String.class, example = "0")
    @GetMapping("/columnConfig/type")
    public Result<List<String>> getColumnConfigType(@RequestParam("type") @NotBlank(message = "字段类型不能为空") String type) {
        return success(generateManager.getColumnConfigType(type));
    }

    @Log
    @Idempotent
    @Check(permissions = {"generate:columnConfig:add"})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "新增字段类型关联")
    @PostMapping("/columnConfig")
    public Result<?> addColumnConfig(@RequestBody @Validated({ValidOne.class}) ColumnConfigDTO columnConfigDTO) {
        generateManager.addColumnConfig(columnConfigDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"generate:columnConfig:update"})
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新字段类型关联")
    @PutMapping("/columnConfig")
    public Result<?> updateColumnConfig(@RequestBody @Validated({ValidTwo.class}) ColumnConfigDTO columnConfigDTO) {
        generateManager.updateColumnConfig(columnConfigDTO);
        return success();
    }

    @Log()
    @Idempotent
    @Check(permissions = {"generate:columnConfig:delete"})
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "删除字段类型关联")
    @ApiImplicitParam(name = "columnConfigIds", value = "字段类型关联ID集合", required = true, dataTypeClass = Long.class)
    @DeleteMapping("/columnConfig")
    public Result<?> deleteColumnConfig(@RequestBody @NotNull(message = "字段类型关联ID集合不能为空")
                                        @Size(min = 1, message = "字段类型关联ID集合元素必须大于1") List<Long> columnConfigIds) {
        generateManager.deleteColumnConfig(columnConfigIds);
        return success();
    }

    @Check(permissions = {"generate:tableConfig:list"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取业务表配置列表")
    @GetMapping("/tableConfig")
    public Result<Page<TableConfigDTO>> getPageTableConfig(TableConfigQuery query) {
        return success(generateManager.getPageTableConfig(query));
    }

    @Check(permissions = {"generate:tableConfig:query"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取业务表配置详情")
    @ApiImplicitParam(name = "tableConfigId", value = "配置id", required = true, dataTypeClass = Long.class)
    @GetMapping("/tableConfig/{tableConfigId}")
    public Result<TableConfigDTO> getTableConfig(@PathVariable("tableConfigId") @NotNull(message = "配置id不能为空") Long tableConfigId) {
        return success(generateManager.getTableConfig(tableConfigId));
    }

    @Check(permissions = {"generate:tableConfig:list"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取业务表配置列表")
    @GetMapping("/tableConfig/list")
    public Result<List<TableConfigDTO>> getTableConfigList() {
        return success(generateManager.getTableConfigList());
    }

    @Log
    @Idempotent
    @Check(permissions = {"generate:tableConfig:add"})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "新增业务表配置")
    @PostMapping("/tableConfig")
    public Result<?> addTableConfig(@RequestBody @Validated({ValidOne.class}) TableConfigDTO tableConfigDTO) {
        generateManager.addTableConfig(tableConfigDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"generate:tableConfig:update"})
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新业务表配置")
    @PutMapping("/tableConfig")
    public Result<?> updateTableConfig(@RequestBody @Validated({ValidTwo.class}) TableConfigDTO tableConfigDTO) {
        generateManager.updateTableConfig(tableConfigDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"generate:tableConfig:delete"})
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "删除业务表配置")
    @ApiImplicitParam(name = "tableConfigIds", value = "配置id集合", required = true, dataTypeClass = Long.class)
    @DeleteMapping("/tableConfig")
    public Result<?> deleteTableConfig(@RequestBody @NotNull(message = "配置id集合不能为空")
                                       @Size(min = 1, message = "配置id集合元素必须大于1") List<Long> tableConfigIds) {
        generateManager.deleteTableConfig(tableConfigIds);
        return success();
    }

    @Check(permissions = {"generate:tableConfig:check"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "业务表配置基类路径校验")
    @ApiImplicitParam(name = "baseClass", value = "基类路径", required = true, dataTypeClass = String.class, example = "top.horsemuzi.system.base.BaseEntity")
    @GetMapping("/tableConfig/check")
    public Result<Boolean> checkTableConfigBaseClass(@RequestParam("baseClass") @NotNull(message = "基类路径不能为空") String baseClass) {
        return success(generateManager.checkTableConfigBaseClass(baseClass));
    }

    @Check(permissions = {"generate:table:list"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "分页获取业务表列表")
    @GetMapping("/table")
    public Result<Page<TableDTO>> getPageTable(TableQuery query) {
        return success(generateManager.getPageTable(query));
    }

    @Check(permissions = {"generate:table:query"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取业务表详情")
    @ApiImplicitParam(name = "tableId", value = "业务表id", required = true, dataTypeClass = Long.class)
    @GetMapping("/table/{tableId}")
    public Result<TableDTO> getTable(@PathVariable("tableId") @NotNull(message = "业务表id不能为空") Long tableId) {
        return success(generateManager.getTable(tableId));
    }

    @Log
    @Idempotent
    @Check(permissions = {"generate:table:add"})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "新增业务表")
    @PostMapping("/table")
    public Result<?> addTable(@RequestBody @Validated({ValidOne.class}) TableDTO tableDTO) {
        generateManager.addTable(tableDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"generate:table:update"})
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新业务表")
    @PutMapping("/table")
    public Result<?> updateTable(@RequestBody @Validated({ValidTwo.class}) TableDTO tableDTO) {
        generateManager.updateTable(tableDTO);
        return success();
    }

    @Log
    @Idempotent
    @Check(permissions = {"generate:table:delete"})
    @ApiOperation(httpMethod = Constants.METHOD.DELETE, value = "删除业务表")
    @ApiImplicitParam(name = "tableIds", value = "业务表id集合", required = true, dataTypeClass = Long.class)
    @DeleteMapping("/table")
    public Result<?> deleteTable(@RequestBody @NotNull(message = "业务表id集合不能为空")
                                 @Size(min = 1, message = "业务表id集合元素必须大于1") List<Long> tableIds) {
        generateManager.deleteTable(tableIds);
        return success();
    }

    @Log(type = Constants.METHOD.PREVIEW)
    @Idempotent
    @Check(permissions = {"generate:table:preview"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "代码生成预览")
    @ApiImplicitParam(name = "tableId", value = "业务表id", required = true, dataTypeClass = Long.class)
    @GetMapping("/table/preview/{tableId}")
    public Result<Map<String, Object>> preview(@PathVariable("tableId") @NotNull(message = "业务表id不能为空") Long tableId) {
        return success(generateManager.preview(tableId));
    }

    @Log
    @Idempotent
    @Check(permissions = {"generate:table:sync"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "代码生成同步表结构")
    @ApiImplicitParam(name = "tableId", value = "业务表id", required = true, dataTypeClass = Long.class)
    @GetMapping("/table/sync/{tableId}")
    public Result<?> sync(@PathVariable("tableId") @NotNull(message = "业务表id不能为空") Long tableId) {
        generateManager.sync(tableId);
        return success();
    }

    @Log(type = Constants.METHOD.DOWNLOAD, binaryResData = true)
    @Idempotent
    @AloneLimiter(perSecond = 2)
    @Check(permissions = {"generate:table:download"})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "代码生成下载", notes = "下载业务表所有模板文件(zip压缩包形式)")
    @ApiImplicitParam(name = "tableIds", value = "业务表id集合", required = true, dataTypeClass = Long.class)
    @PostMapping("/table/download")
    public void download(@RequestBody @NotNull(message = "业务表id集合不能为空")
                         @Size(min = 1, message = "业务表id集合元素必须大于1") List<Long> tableIds,
                         HttpServletResponse response) {
        generateManager.download(tableIds, response);
    }

    @Log(type = Constants.METHOD.DOWNLOAD, binaryResData = true)
    @Idempotent
    @AloneLimiter(perSecond = 3)
    @Check(permissions = {"generate:table:downloadSingle"})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "代码文件下载", notes = "下载业务表单个文件下载")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tableId", value = "业务表id", required = true, dataTypeClass = Long.class),
            @ApiImplicitParam(name = "vmName", value = "模板文件名", required = true, dataTypeClass = String.class, example = "controller.java")
    })
    @PostMapping("/table/downloadSingle")
    public void downloadSingle(@RequestParam("tableId") @NotNull(message = "业务表Id不能为空") Long tableId,
                               @RequestParam("vmName") @NotBlank(message = "模板文件名不能为空") String vmName,
                               HttpServletResponse response) {
        generateManager.downloadSingle(tableId, vmName, response);
    }

    @Idempotent
    @Check(permissions = {"generate:table:permission"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取父权限菜单列表(目录、菜单)")
    @GetMapping("/table/permission")
    public Result<List<PermissionDTO>> getPermissions() {
        return success(generateManager.getPermissions());
    }

    @Idempotent
    @Check(permissions = {"generate:table:role"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取系统角色权限标识列表")
    @GetMapping("/table/role")
    public Result<List<RoleDTO>> getRoles() {
        return success(generateManager.getRoles());
    }

    @Idempotent
    @Check(permissions = {"generate:table:fetch"})
    @GetMapping("/table/fetch")
    @ApiImplicitParam(name = "search", value = "实时检索关键字", dataTypeClass = String.class)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "业务表名称实时检索", notes = "输入框实时检索业务表名称")
    public Result<List<InputRemoteFetchDTO>> fetchTable(@RequestParam("search") String search) {
        return success(generateManager.fetchTable(search));
    }

    @Idempotent
    @Check(permissions = {"generate:table:ddl"})
    @GetMapping("/table/ddl")
    @ApiImplicitParam(name = "tableId", value = "业务表id", required = true, dataTypeClass = Long.class)
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取业务表DDL信息")
    public Result<String> getTableDDL(@RequestParam("tableId") @NotNull(message = "业务表id不能为空") Long tableId) {
        return success(generateManager.getTableDDL(tableId));
    }

    @Check(permissions = {"generate:tableColumn:list"})
    @ApiOperation(httpMethod = Constants.METHOD.GET, value = "获取业务表字段列表")
    @ApiImplicitParam(name = "tableId", value = "业务表id", required = true, dataTypeClass = Long.class)
    @GetMapping("/tableColumn/{tableId}")
    public Result<List<TableColumnDTO>> getTableColumnList(@PathVariable("tableId") @NotNull(message = "业务表id不能为空") Long tableId) {
        return success(generateManager.getTableColumnList(tableId));
    }

    @Log
    @Idempotent
    @Check(permissions = {"generate:tableColumn:update"})
    @ApiOperation(httpMethod = Constants.METHOD.PUT, value = "更新业务表字段")
    @PutMapping("/tableColumn")
    public Result<?> updateTableColumn(@RequestBody @Validated({ValidTwo.class}) List<TableColumnDTO> tableColumnDTO) {
        generateManager.updateTableColumn(tableColumnDTO);
        return success();
    }

    @Log(type = Constants.METHOD.DOWNLOAD, binaryResData = true)
    @Idempotent
    @AloneLimiter(perSecond = 2)
    @Check(permissions = {"generate:screw:download"})
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "数据库设计文档下载", notes = "当前数据源数据库对应表设计文档下载")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "文档类型, 0-HTML, 1-DOC, 2-MD", required = true, dataTypeClass = Integer.class, example = "2"),
            @ApiImplicitParam(name = "tableId", value = "业务表id", required = true, dataTypeClass = Long.class)
    })
    @PostMapping("/table/screw/download")
    public void download(@RequestParam("type") Integer type, @RequestParam("tableId") Long tableId, HttpServletResponse response) {
        generateManager.screwDownload(type, tableId, response);
    }

}
