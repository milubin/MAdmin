package top.horsemuzi.system.controller.portal;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.horsemuzi.system.base.BaseController;

/**
 * <p>
 * 登录控制 前端控制器
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-07 22:44:27
 */
@RestController
@RequestMapping("/portal/login")
public class PortalLoginController extends BaseController {

}
