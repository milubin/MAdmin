package top.horsemuzi.system.controller.common;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.EnvironmentPBEConfig;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.system.annotation.Anonymous;
import top.horsemuzi.system.base.BaseController;
import top.horsemuzi.system.core.sign.aliyun.service.AliyunService;
import top.horsemuzi.system.core.sign.juejin.service.JueJinService;
import top.horsemuzi.system.core.slack.SlackService;
import top.horsemuzi.system.manager.admin.ToolManager;
import top.horsemuzi.system.manager.common.CommonManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 公共控制处理
 *
 * @author mabin
 * @date 2022/05/09 21:36
 **/
@Slf4j
@Api(tags = "系统测试服务")
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/test")
public class TestController extends BaseController {

    private final CommonManager commonManager;
    private final ToolManager toolManager;
    private final JueJinService jueJinService;
    private final SlackService slackService;
    private final AliyunService aliyunService;

    @Anonymous(sign = false)
    // @Log(type = Constants.METHOD.TEST)
    @ApiOperation(httpMethod = Constants.METHOD.POST, value = "系统测试接口")
    @PostMapping
    public Result<?> test(@RequestParam(value = "json", required = false) String json,
                          HttpServletRequest request, HttpServletResponse response) throws Exception {
        // toolManager.execMysqlBackup();

        // jueJinService.autoSign();

        // slackService.chatClaude(json);

        aliyunService.autoSign();

        return success();
    }

    public static void main(String[] args) throws Exception {
        StandardPBEStringEncryptor standardPBEStringEncryptor = new StandardPBEStringEncryptor();
        EnvironmentPBEConfig config = new EnvironmentPBEConfig();
        config.setAlgorithm("PBEWithMD5AndDES");
        config.setPassword("xxx");
        standardPBEStringEncryptor.setConfig(config);

        Map<String, String> plainMap = new LinkedHashMap<>(16);
        plainMap.put("check", "xxx");

        Map<String, String> cipherMap = new LinkedHashMap<>(16);
        for (Map.Entry<String, String> entry : plainMap.entrySet()) {
            cipherMap.put(entry.getKey(), standardPBEStringEncryptor.encrypt(entry.getValue()));
        }
        System.out.println(JsonUtil.toPrettyJson(cipherMap));


        // 组装请求参数
        // String appSecret = "Ptz01vHm2dp5iRn73xxwwuxrrt8I91Ha";
        // Map<String, String> paramsMap = new TreeMap<>();
        // paramsMap.put("appKey", "10086");
        // paramsMap.put("apiCode", "API_TEST");
        // paramsMap.put("accessToken", "bfe160a81b0811d61d781c0b2ed7231814f5b332237708055bbf74f8ae973ef150d9088b238c504f38b5f856054cc501");
        // paramsMap.put("timestamp", System.currentTimeMillis() + "");
        // paramsMap.put("version", "v1");
        //
        // HashMap<String, Object> dataMap = new HashMap<>(16);
        // dataMap.put("ip", "140.205.135.3");
        // paramsMap.put("data", EncryptUtil.desedeEncrypt(JsonUtil.toJson(dataMap), appSecret));
        //
        // // 签名
        // StringBuilder builder = new StringBuilder();
        // for (Map.Entry<String, String> entry : paramsMap.entrySet()) {
        //     builder.append(entry.getValue());
        // }
        // String plainText = builder.toString() + appSecret;
        // paramsMap.put("sign", EncryptUtil.md5(plainText));
        //
        // String post = HttpUtil.OK_HTTP.post("http://localhost:10086/api/core", JsonUtil.toJson(paramsMap));
        //
        // // 发起请求
        // log.info("post={}", post);

        System.exit(0);

    }

}
