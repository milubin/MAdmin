package top.horsemuzi.system.config.guava;

import com.google.common.eventbus.AsyncEventBus;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.Executor;

/**
 * Guava的EventBus事件配置
 *
 * @author mabin
 * @date 2022/08/06 17:26
 **/
@RequiredArgsConstructor
@Configuration
public class EventConfig {

    private final Executor async;

    @Bean
    public AsyncEventBus asyncEventBus() {
        return new AsyncEventBus(async);
    }

}
