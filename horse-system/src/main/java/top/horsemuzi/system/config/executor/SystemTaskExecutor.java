package top.horsemuzi.system.config.executor;

import cn.hutool.core.util.IdUtil;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.slf4j.MDC;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.util.ExceptionUtil;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

/**
 * 重写系统线程池（加强MDC的链路追踪能力）
 *
 * @author mabin
 * @date 2022/06/17 13:45
 **/

@Slf4j
public class SystemTaskExecutor extends ThreadPoolTaskExecutor {

    private static final long serialVersionUID = -3284512767076297372L;

    @Override
    public void execute(Runnable task) {
        super.execute(runnableTask(task));
    }

    @NotNull
    @Override
    public Future<?> submit(Runnable task) {
        return super.submit(runnableTask(task));
    }

    @NotNull
    @Override
    public <T> Future<T> submit(Callable<T> task) {
        Map<String, String> contentMap = MDC.getCopyOfContextMap();
        return super.submit(() -> {
            T result;
            if (Objects.nonNull(contentMap)) {
                MDC.setContextMap(contentMap);
            } else {
                MDC.put(Constants.TRACE_ID, IdUtil.nanoId(Constants.COMMON_CODE.BASE_INT_TEN));
            }
            try {
                result = task.call();
            } finally {
                // 任务完成, 移除当前MDC的内容
                try {
                    MDC.remove(Constants.TRACE_ID);
                } catch (Exception e) {
                    log.error(ExceptionUtil.getMessage(e));
                }
            }
            return result;
        });
    }

    private Runnable runnableTask(Runnable task) {
        Map<String, String> contentMap = MDC.getCopyOfContextMap();
        return () -> {
            if (Objects.nonNull(contentMap)) {
                MDC.setContextMap(contentMap);
            } else {
                MDC.put(Constants.TRACE_ID, IdUtil.nanoId(Constants.COMMON_CODE.BASE_INT_TEN));
            }
            try {
                task.run();
            } finally {
                // 任务完成, 移除当前MDC的内容
                try {
                    MDC.remove(Constants.TRACE_ID);
                } catch (Exception e) {
                    log.error(ExceptionUtil.getMessage(e));
                }
            }
        };
    }

}
