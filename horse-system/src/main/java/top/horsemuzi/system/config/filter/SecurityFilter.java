package top.horsemuzi.system.config.filter;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.extra.servlet.ServletUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StopWatch;
import top.horsemuzi.common.util.EncryptUtil;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.wrapper.RequestWrapper;
import top.horsemuzi.common.wrapper.ResponseWrapper;
import top.horsemuzi.system.config.properties.EncryptProperties;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * 数据传输自动加解密Filter处理
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/05/05 10:20
 **/
@Slf4j
public class SecurityFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("Initialization of encrypt filter succeeded!");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        byte[] body = null;
        if (httpServletRequest.getContentLength() > 0) {
            Map<String, String> cipherMap = JsonUtil.fromJson(ServletUtil.getBody(request), new TypeReference<Map<String, String>>() {
            });
            if (MapUtil.isNotEmpty(cipherMap)) {
                // 前后端对于数据格式的定义应保持一致
                // 前端在body中传输的数据格式应该是: {cipherText: data}, data为密文, 解密后是json字符串
                String cipherText = cipherMap.get("cipherText");
                if (CharSequenceUtil.isNotBlank(cipherText)) {
                    StopWatch watch = new StopWatch();
                    watch.start();
                    String plainText = EncryptUtil.sm2Decrypt(EncryptProperties.sm2PrivateKey, cipherText);
                    watch.stop();
                    log.info("decrypt time: {}ms", watch.getTotalTimeMillis());
                    body = plainText.getBytes(StandardCharsets.UTF_8);
                }
            }
        } else {
            body = IoUtil.readBytes(request.getInputStream(), false);
        }
        RequestWrapper requestWrapper = new RequestWrapper(httpServletRequest, body);
        ResponseWrapper responseWrapper = new ResponseWrapper(httpServletResponse);
        if (ObjectUtil.hasNull(requestWrapper, responseWrapper)) {
            chain.doFilter(request, response);
        } else {
            chain.doFilter(requestWrapper, responseWrapper);
            String plainText = new String(responseWrapper.getResponseData(), StandardCharsets.UTF_8);
            if (CharSequenceUtil.isNotBlank(plainText)) {
                StopWatch watch = new StopWatch();
                watch.start();
                String cipherText = EncryptUtil.sm2Encrypt(EncryptProperties.sm2PublicKey, plainText);
                watch.stop();
                log.info("encrypt time: {}ms", watch.getTotalTimeMillis());
                PrintWriter writer = response.getWriter();
                writer.print(cipherText);
                writer.flush();
                writer.close();
            }
        }
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }

}
