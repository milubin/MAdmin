package top.horsemuzi.system.config.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description {description}
 * @date 2022/5/15 18:29
 */
@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "custom.interceptor")
public class InterceptorProperties {

    private String include;

    private List<String> exclude;
}
