package top.horsemuzi.system.config.validator;

import lombok.RequiredArgsConstructor;
import org.hibernate.validator.HibernateValidator;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.Validator;
import java.util.Properties;

/**
 * Validator配置修改
 *
 * @author mabin
 * @date 2022/06/30 09:31
 **/
@Configuration
@RequiredArgsConstructor
public class ValidatorConfig {

    private final MessageSource messageSource;

    /**
     * Validator 默认会校验完所有字段，然后返回所有的验证失败信息。
     * 可以通过一些简单的配置，开启Fail Fast模式，只要有一个验证失败就立即返回
     */
    @Bean
    public Validator validator() {
        LocalValidatorFactoryBean factory = new LocalValidatorFactoryBean();
        factory.setValidationMessageSource(messageSource);
        factory.setProviderClass(HibernateValidator.class);
        Properties properties = new Properties();
        properties.setProperty("hibernate.validator.fail_fast", "true");
        factory.setValidationProperties(properties);
        factory.afterPropertiesSet();
        return factory.getValidator();
    }

}
