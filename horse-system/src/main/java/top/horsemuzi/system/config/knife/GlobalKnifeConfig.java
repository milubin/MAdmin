package top.horsemuzi.system.config.knife;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import top.horsemuzi.system.config.properties.SystemProperties;

/**
 * 配置全局接口文档
 *
 * @author mabin
 * @date 2022/05/06 15:47
 **/
@Configuration
@EnableOpenApi
@EnableKnife4j
public class GlobalKnifeConfig {

    @Bean
    public Docket adminApiConfig() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("adminApi")
                .apiInfo(getApiInfo("后台管理"))
                .select()
                .paths(PathSelectors.regex("/admin/.*"))
                .build();
    }

    @Bean
    public Docket portalApiConfig() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("portalApi")
                .apiInfo(getApiInfo("网站门户"))
                .select()
                .paths(PathSelectors.regex("/portal/.*"))
                .build();
    }

    @Bean
    public Docket commonApiConfig() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("commonApi")
                .apiInfo(getApiInfo("公共服务"))
                .select()
                .paths(PathSelectors.regex("/common/.*"))
                .build();
    }

    @Bean
    public Docket webApiConfig() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("openApi")
                .apiInfo(getApiInfo("API开放平台"))
                .select()
                .paths(PathSelectors.regex("/api/.*"))
                .build();
    }

    @Bean
    public Docket linkApiConfig() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("linkApi")
                .apiInfo(getApiInfo("短链平台"))
                .select()
                .paths(PathSelectors.regex("/short/.*"))
                .build();
    }

    /**
     * 获取API基础定义信息
     *
     * @param title
     * @return springfox.documentation.service.ApiInfo
     * @author mabin
     * @date 2023/3/8 10:49
     **/
    private ApiInfo getApiInfo(String title) {
        return new ApiInfoBuilder()
                .title(SystemProperties.name + title + "-API文档")
                .description(title + "API平台接口描述")
                .version(SystemProperties.version)
                .contact(new Contact(SystemProperties.author, SystemProperties.email, SystemProperties.email))
                .build();
    }

}
