package top.horsemuzi.system.config.interceptor;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ConfigEnum;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.EncryptUtil;
import top.horsemuzi.common.util.NetUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.annotation.Anonymous;
import top.horsemuzi.system.config.properties.EncryptProperties;
import top.horsemuzi.system.manager.common.CommonManager;
import top.horsemuzi.system.service.ConfigService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Duration;
import java.util.Date;
import java.util.Objects;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description 全局拦截器处理
 * @date 2022/5/15 16:30
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class GlobalInterceptor implements HandlerInterceptor {

    private final CommonManager commonManager;
    private final ConfigService configService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 非系统预置请求方式, 直接放行
        if (!CharSequenceUtil.equalsAnyIgnoreCase(request.getMethod(), Constants.METHOD.GET, Constants.METHOD.POST, Constants.METHOD.PUT, Constants.METHOD.DELETE)) {
            return true;
        }
        log.info("this request: {} <-> {}", request.getMethod(), request.getRequestURI());
        // ip白名单校验
        verifyIp(request);
        // 匿名访问注解 @Anonymous
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Anonymous anonymous = handlerMethod.getMethodAnnotation(Anonymous.class);
        if (Objects.nonNull(anonymous)) {
            if (anonymous.sign()) {
                verifySign(request);
            }
            return true;
        } else {
            verifySign(request);
        }
        // token鉴权
        ValidatorUtil.isTrue(commonManager.verifyToken(request), ResultCodeEnum.AUTHENTICATION_FAILED);
        return true;
    }

    /**
     * ip白名单校验
     *
     * @param request
     * @return void
     * @author mabin
     * @date 2023/7/8 10:16
     **/
    private void verifyIp(HttpServletRequest request) {
        String ipWhiteList = configService.getConfigValue(ConfigEnum.SERVER_WHITE_LIST);
        if (StrUtil.isBlank(ipWhiteList)) {
            return;
        }
        String requestIp = NetUtil.getRequestIp(request);
        log.info("this requestIp: {}", requestIp);
        ValidatorUtil.isTrue(ArrayUtil.contains(ipWhiteList.split(StrUtil.COMMA), requestIp), ResultCodeEnum.REQUEST_IP_LIMIT_ERROR);
    }

    /**
     * 校验签名
     *
     * @param request
     */
    private void verifySign(HttpServletRequest request) {
        /*
         * 签名校验规则:
         * 1、从header中获取时间戳(timestamp)、随机数(random(nacoId) RSA加密字符串)、签名(sign)等3个参数
         * 2、根据timestamp计算请求时差, 超过5分钟默认为非法请求
         * 3、将timestamp参数进行MD5加密生成一个key参数, 将三个参数按照 timestamp + key + random(RSA解密之后的字符串) 的顺序拼接成字符串
         * 4、将拼接的字符串进行md5加密, 最后转换成大写形式生成sign签名
         * 5、校验header中的sign和生成的sign是否相等, 进行签名校验
         * */
        String timestamp = request.getHeader("timestamp");
        String random = request.getHeader("random");
        String sign = request.getHeader("sign");
        ValidatorUtil.allNotBlank(ResultCodeEnum.SIGN_ERROR, timestamp, random, sign);
        long between = DateUtil.between(new Date(Convert.toLong(timestamp)), new Date(), DateUnit.MINUTE, true);
        ValidatorUtil.isTrue(between <= Duration.ofMinutes(5).toMinutes(), ResultCodeEnum.SIGN_ERROR);
        String generateSign = EncryptUtil.md5(CharSequenceUtil.concat(true, timestamp, EncryptUtil.md5(timestamp),
                EncryptUtil.rsaDecrypt(EncryptProperties.rsaPrivateKey, random))).toUpperCase();
        ValidatorUtil.isTrue(CharSequenceUtil.equals(sign, generateSign), ResultCodeEnum.SIGN_ERROR);
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
