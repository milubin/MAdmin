package top.horsemuzi.system.config.i18n;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.text.StrPool;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * 国际化信息配置
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/04/08 10:50
 **/
@Configuration
public class I18nConfig {

    /**
     * 获取请求国际化信息
     *
     * @return
     */
    @Bean
    public LocaleResolver localeResolver() {
        return new LocaleResolver() {
            @Override
            public Locale resolveLocale(HttpServletRequest request) {
                String language = request.getHeader("content-language");
                Locale locale = Locale.getDefault();
                if (CharSequenceUtil.isNotBlank(language)) {
                    String[] split = language.split(StrPool.UNDERLINE);
                    locale = new Locale(split[0], split[1]);
                }
                return locale;
            }

            @Override
            public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {

            }
        };
    }

}
