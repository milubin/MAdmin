package top.horsemuzi.system.config.mp.expand;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * MP扩展操作
 *
 * @author mabin
 * @date 2022/07/12 13:40
 **/

public interface ExpandMapper<T> extends BaseMapper<T> {

    /**
     * 批量插入 仅使用于mysql,方法名必须是这个,(基于MP的源码中的规定)
     * 注意事项: 这是自选字段 insert !!!, 如果个别字段在 entity 里为 null 但是数据库中有配置默认值, insert 后数据库字段是为 null 而不是默认值.
     * 所以这里entity中的所有字段值都不能为空(除了自动填充的字段), 需谨慎使用!
     *
     * @param entityList 实例列表
     * @return 影响行数
     */
    Integer insertBatchSomeColumn(List<T> entityList);

}
