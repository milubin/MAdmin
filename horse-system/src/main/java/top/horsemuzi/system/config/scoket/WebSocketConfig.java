package top.horsemuzi.system.config.scoket;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * WebSocket配置服务支持（因为集成了plumeLog轻量级日志工具，会重复初始化此Bean而报错，所以整个系统此处不再需要进行初始化）
 *
 * @author mabin
 * @date 2022/10/10 11:46
 **/
@Configuration
@Slf4j
public class WebSocketConfig {

    /**
     * 注入ServerEndpointExporter，
     * 这个bean会自动注册使用了@ServerEndpoint注解声明的Websocket endpoint
     * 注意: 如果独立使用Servlet容器,而不是直接使用Spring Boot的内置容器就不需要注入ServerEndpointExporter, 因为它将由容器自己提供和管理。
     */
    // @Bean
    // public ServerEndpointExporter serverEndpointExporter() {
    //     return new ServerEndpointExporter();
    // }

}
