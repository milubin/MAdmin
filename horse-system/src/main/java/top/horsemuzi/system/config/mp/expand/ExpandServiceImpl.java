package top.horsemuzi.system.config.mp.expand;

import com.baomidou.mybatisplus.core.conditions.AbstractWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import top.horsemuzi.common.util.OrikaUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * MP扩展操作服务
 *
 * @author mabin
 * @date 2022/07/12 16:04
 **/
@Slf4j
public class ExpandServiceImpl<M extends ExpandMapper<T>, T> extends ServiceImpl<M, T> implements IService<T> {

//    /**
//     * 重写saveBatch方法, 实现数据真正的批量插入(暂时放弃重写, 不太适用于本系统)
//     *
//     * @param entityList ignore
//     * @param batchSize  ignore
//     * @return ignore
//     */
//    @Transactional(rollbackFor = Exception.class)
//    @Override
//    public boolean saveBatch(Collection<T> entityList, int batchSize) {
//        try {
//            if (CollectionUtil.isEmpty(entityList)) {
//                log.warn("expand saveBatch method params is not null");
//                return false;
//            }
//            int size = entityList.size();
//            if (Math.min(size, batchSize) == size) {
//                baseMapper.insertBatchSomeColumn(new ArrayList<>(entityList));
//            } else {
//                List<List<T>> splitList = Lists.partition(new ArrayList<>(entityList), batchSize);
//                for (List<T> entities : splitList) {
//                    baseMapper.insertBatchSomeColumn(entities);
//                }
//            }
//            log.info("expand saveBatch method execute success!");
//            return true;
//        } catch (Exception e) {
//            log.error("expand saveBatch method execute fail! {}", ExceptionUtil.getMessage(e));
//            return false;
//        }
//    }


    /**
     * 重写getOne方法，加上"LIMIT 1"，防止出现异常
     * Wrapper默认做过处理，添加多个last("LIMIT 1")，最后只会有一个生效，拼接到执行的sql当中，多写也没有关系
     *
     * @param queryWrapper
     * @return
     */
    @Override
    public T getOne(Wrapper<T> queryWrapper) {
        AbstractWrapper<T, SFunction<T, ?>, ?> abstractWrapper = (AbstractWrapper<T, SFunction<T, ?>, ?>) queryWrapper;
        abstractWrapper.last("LIMIT 1");
        return this.getOne(abstractWrapper, true);
    }

    public <C> C getOne(Wrapper<T> queryWrapper, Class<C> clazz) {
        AbstractWrapper<T, SFunction<T, ?>, ?> abstractWrapper = (AbstractWrapper<T, SFunction<T, ?>, ?>) queryWrapper;
        abstractWrapper.last("LIMIT 1");
        return OrikaUtil.convert(this.getOne(abstractWrapper, true), clazz);
    }

    public <C> C getById(Serializable id, Class<C> clazz) {
        return OrikaUtil.convert(this.getById(id), clazz);
    }

    public <C> List<C> listByIds(Collection<? extends Serializable> idList, Class<C> clazz) {
        return OrikaUtil.convertList(this.listByIds(idList), clazz);
    }

    public <C> List<C> list(Class<C> clazz) {
        return OrikaUtil.convertList(list(), clazz);
    }

    public <C> List<C> list(Wrapper<T> queryWrapper, Class<C> clazz) {
        return OrikaUtil.convertList(list(queryWrapper), clazz);
    }

    public <C> boolean updateById(C entityDTO, Class<T> clazz) {
        return updateById(OrikaUtil.convert(entityDTO, clazz));
    }

    public <C> boolean updateBatchById(Collection<C> entityDTOList, Class<T> clazz) {
        List<C> convertEntityList = new ArrayList<>(entityDTOList);
        return updateBatchById(OrikaUtil.convertList(convertEntityList, clazz));
    }

    public <C> boolean save(C entity, Class<T> clazz) {
        return save(OrikaUtil.convert(entity, clazz));
    }

    public <C> boolean saveBatch(Collection<C> entityDTOList, Class<T> clazz) {
        List<C> convertEntityList = new ArrayList<>(entityDTOList);
        return saveBatch(OrikaUtil.convertList(convertEntityList, clazz));
    }

}
