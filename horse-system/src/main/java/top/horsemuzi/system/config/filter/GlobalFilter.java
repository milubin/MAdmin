package top.horsemuzi.system.config.filter;

import cn.hutool.core.util.IdUtil;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.wrapper.RequestWrapper;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description 使用过滤器完成日志链路ID, 此次使用spring boot提供的FilterRegistrationBean注册Filter
 * 此链路日志目前不适用于微服务,因为微服务之间存在跨服务调用,MDC的值需要进行做传递操作,当前并未实现.
 * @date 2021/7/12 20:50
 */
@Slf4j
public class GlobalFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("Initialization of global filter succeeded!");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {
        // 设置链路日志ID唯一标识(缺点: 只局限于当前线程,若产生子线程,这子线程的日志将缺失唯一标识)
        MDC.put(Constants.TRACE_ID, IdUtil.nanoId(Constants.COMMON_CODE.BASE_INT_TEN));
        // 替换request为requestWrapper包装类
        ServletRequest requestWrapper = null;
        if (request instanceof HttpServletRequest) {
            requestWrapper = new RequestWrapper((HttpServletRequest) request);
        }
        if (requestWrapper == null) {
            chain.doFilter(request, response);
        } else {
            chain.doFilter(requestWrapper, response);
        }
        // 响应返回页面前,清楚表示,避免内存资源浪费
        MDC.remove(Constants.TRACE_ID);
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }

}
