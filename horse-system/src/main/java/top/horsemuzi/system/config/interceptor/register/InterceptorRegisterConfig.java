package top.horsemuzi.system.config.interceptor.register;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import top.horsemuzi.system.config.interceptor.GlobalInterceptor;
import top.horsemuzi.system.config.properties.InterceptorProperties;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description {description}
 * @date 2022/5/15 16:23
 */
@Slf4j
@RequiredArgsConstructor
@Configuration
public class InterceptorRegisterConfig implements WebMvcConfigurer {


    private final InterceptorProperties interceptorProperties;
    private final GlobalInterceptor globalInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(globalInterceptor)
                .addPathPatterns(interceptorProperties.getInclude())
                .excludePathPatterns(interceptorProperties.getExclude());
    }

    /**
     * 跨域配置
     */
    @Bean
    public CorsFilter corsFilter() {
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(Boolean.TRUE);
        // 设置访问源地址
        config.addAllowedOrigin("*");
        // 是否发送Cookie信息
        config.setAllowCredentials(Boolean.TRUE);
        // 设置访问源请求头
        config.addAllowedHeader("*");
        // 设置访问源请求方法
        config.addAllowedMethod("*");
        // 设置暴露头部信息(跨域访问默认不能获取全部头部信息)
        config.addExposedHeader("*");
        // 对接口配置跨域设置
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }
}
