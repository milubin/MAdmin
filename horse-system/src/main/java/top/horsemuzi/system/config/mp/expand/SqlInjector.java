package top.horsemuzi.system.config.mp.expand;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.extension.injector.methods.InsertBatchSomeColumn;

import java.util.List;
import java.util.Objects;

/**
 * MP方法扩展配置注射器(支持自定义数据方法注入)
 *
 * @author mabin
 * @date 2022/07/12 13:32
 **/

public class SqlInjector extends DefaultSqlInjector {

    @Override
    public List<AbstractMethod> getMethodList(Class<?> mapperClass, TableInfo tableInfo) {
        // 防止父类方法不可使用,所有要返回父类方法并添加自定义扩展方法
        List<AbstractMethod> methodList = super.getMethodList(mapperClass, tableInfo);
        // 增加扩展的方法(存在于源码,但作者没有内置,有一定的bug)
        methodList.add(new InsertBatchSomeColumn(t -> !Objects.equals("version", t.getProperty())));
        return methodList;
    }

}
