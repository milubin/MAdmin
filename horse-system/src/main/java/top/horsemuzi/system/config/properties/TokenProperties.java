package top.horsemuzi.system.config.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Token访问令牌属性配置
 *
 * @author mabin
 * @date 2022/05/12 09:32
 **/
@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "custom.token")
public class TokenProperties {

    private Long expire;

    private Long refresh;
}
