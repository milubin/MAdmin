package top.horsemuzi.system.config.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Xss过滤自定义配置
 *
 * @author mabin
 * @date 2022/11/28 11:55
 **/

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "custom.xss")
public class XssProperties {

    /**
     * 是否开启
     */
    private Boolean enable;

    /**
     * 排除链接（多个用逗号分隔）
     */
    private String excludes;

    /**
     * 匹配链接
     */
    private String urlPatterns;

}
