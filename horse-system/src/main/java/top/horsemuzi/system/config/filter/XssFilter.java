package top.horsemuzi.system.config.filter;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.text.StrPool;
import cn.hutool.core.util.ObjectUtil;
import lombok.extern.slf4j.Slf4j;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.wrapper.XssWrapper;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Xss过滤器配置
 *
 * @author mabin
 * @date 2022/11/28 11:35
 **/

@Slf4j
public class XssFilter implements Filter {

    /**
     * 排除链接
     */
    private String[] excludes = new String[]{};

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        String tempExcludes = filterConfig.getInitParameter("excludes");
        if (CharSequenceUtil.isNotBlank(tempExcludes)) {
            excludes = tempExcludes.split(StrPool.COMMA);
        }
        log.info("Initialization of xss filter succeeded!");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        if (handleExcludeURL(req)) {
            chain.doFilter(request, response);
            return;
        }
        XssWrapper xssRequest = new XssWrapper((HttpServletRequest) request);
        chain.doFilter(xssRequest, response);
    }

    /**
     * 判断请求是否需要Xss过滤处理
     *
     * @param request
     * @return
     */
    private boolean handleExcludeURL(HttpServletRequest request) {
        String url = request.getServletPath();
        String method = request.getMethod();
        // GET DELETE 不过滤
        if (ObjectUtil.isNull(method) || CharSequenceUtil.equalsAnyIgnoreCase(method, Constants.METHOD.GET, Constants.METHOD.DELETE)) {
            return true;
        }
        return CharSequenceUtil.startWithAny(url, excludes);
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }

}
