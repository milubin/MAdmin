package top.horsemuzi.system.config.executor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 系统线程池配置
 *
 * @author mabin
 * @date 2022/06/14 17:59
 **/
@Configuration
public class ExecutorConfig {

    private final int core = Runtime.getRuntime().availableProcessors() + 1;

    @Bean("async")
    public Executor executor() {
        SystemTaskExecutor executor = new SystemTaskExecutor();
        // 核心线程数: 线程池创建时候初始化的线程数
        executor.setCorePoolSize(core);
        // 线程池最大的线程数
        executor.setMaxPoolSize(core * 2);
        // 缓冲队列
        executor.setQueueCapacity(15);
        // 允许线程的空闲时间60秒：当超过了核心线程出之外的线程在空闲时间到达之后会被销毁
        executor.setKeepAliveSeconds(60);
        // 线程池的前缀
        executor.setThreadNamePrefix("async-");
        // 线程池对拒绝任务的处理策略：这里采用了CallerRunsPolicy策略，当线程池没有处理能力的时候，该策略会直接在 execute 方法的调用线程中运行被拒绝的任务；如果执行程序已关闭，则会丢弃该任务
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.initialize();
        return executor;
    }

    @Bean("scheduler")
    public TaskScheduler taskScheduler() {
        // 配置任务线程池
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        // 核心线程数: 线程池创建时候初始化的线程数
        scheduler.setPoolSize(core);
        // 删除策略
        scheduler.setRemoveOnCancelPolicy(true);
        // 线程池名称前缀
        scheduler.setThreadNamePrefix("scheduler-");
        return scheduler;
    }

}
