package top.horsemuzi.system.config.quartz;

import org.springframework.boot.autoconfigure.quartz.SchedulerFactoryBeanCustomizer;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import top.horsemuzi.common.constants.Constants;

/**
 * Quartz任务调度全局配置
 *
 * @author mabin
 * @date 2022/07/12 14:43
 **/
@Configuration
public class QuartzConfig implements SchedulerFactoryBeanCustomizer {

    @Override
    public void customize(SchedulerFactoryBean schedulerFactoryBean) {
        // 在服务启动10s才进行任务调度作业
        schedulerFactoryBean.setStartupDelay(Constants.COMMON_CODE.BASE_INT_TEN);
        // 服务器加载自启动
        schedulerFactoryBean.setAutoStartup(true);
        // 覆盖已存在的任务(确保任务更新及时生效)
        schedulerFactoryBean.setOverwriteExistingJobs(true);
    }

}
