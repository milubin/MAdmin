package top.horsemuzi.system.config.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 掘金平台属性配置
 *
 * @author mabin
 **/
@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "custom.sign.juejin")
public class JueJinProperties {


    /**
     * cookie信息
     */
    private String cookie;

    /**
     * 查询本日签到状态
     */
    private String todayStatus;

    /**
     * 查询免费抽奖次数
     */
    private String lotteryConfig;

    /**
     * 签到
     */
    private String check;

    /**
     * 轮盘抽奖
     */
    private String draw;

    /**
     * 查询剩余积分
     */
    private String curPoint;

    /**
     * 查询签到天数
     */
    private String counts;

    /**
     * 沾喜气
     */
    private String dipLucky;

    /**
     * 查询可以沾喜气的用户列表
     */
    private String globalBig;

    /**
     * 查询未收集的BUG
     */
    private String notCollectBug;

    /**
     * 开始收集BUG
     */
    private String collectBug;

    /**
     * 查询我的道具
     */
    private String myTool;

}
