package top.horsemuzi.system.config.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 项目密钥配置
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/03/18 17:54
 **/
@Component
public class EncryptProperties {

    public static String rsaPrivateKey;
    public static String rsaPublicKey;
    public static String aesKey;
    public static String desedeKey;
    public static String sm2PrivateKey;
    public static String sm2PublicKey;
    public static String sm4Key;

    @Value("${custom.secure.rsa.privateKey}")
    public void setRsaPrivateKey(String rsaPrivateKey) {
        EncryptProperties.rsaPrivateKey = rsaPrivateKey;
    }

    @Value("${custom.secure.rsa.publicKey}")
    public void setRsaPublicKey(String rsaPublicKey) {
        EncryptProperties.rsaPublicKey = rsaPublicKey;
    }

    @Value("${custom.secure.aes.key}")
    public void setAesKey(String aesKey) {
        EncryptProperties.aesKey = aesKey;
    }

    @Value("${custom.secure.3des.key}")
    public void setDesedeKey(String desedeKey) {
        EncryptProperties.desedeKey = desedeKey;
    }

    @Value("${custom.secure.sm2.privateKey}")
    public void setSm2PrivateKey(String sm2PrivateKey) {
        EncryptProperties.sm2PrivateKey = sm2PrivateKey;
    }

    @Value("${custom.secure.sm2.publicKey}")
    public void setSm2PublicKey(String sm2PublicKey) {
        EncryptProperties.sm2PublicKey = sm2PublicKey;
    }

    @Value("${custom.secure.sm4.key}")
    public void setSm4Key(String sm4Key) {
        EncryptProperties.sm4Key = sm4Key;
    }

}
