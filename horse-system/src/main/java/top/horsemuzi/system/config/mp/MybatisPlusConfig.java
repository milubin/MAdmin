package top.horsemuzi.system.config.mp;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.text.StrPool;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.core.metadata.TableFieldInfo;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.BlockAttackInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.util.JwtUtil;
import top.horsemuzi.common.util.NetUtil;
import top.horsemuzi.system.config.mp.expand.SqlInjector;
import top.horsemuzi.system.config.properties.SystemProperties;
import top.horsemuzi.system.core.redis.service.RedisService;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * MP全局配置
 *
 * @author mabin
 * @date 2022/05/06 18:07
 **/
@Slf4j
@RequiredArgsConstructor
@Configuration
public class MybatisPlusConfig implements MetaObjectHandler {

    private final RedisService redisService;

    private static final String GMT_CREATE = "gmtCreate";
    private static final String CREATE_ID = "createId";
    private static final String CREATE_USER = "createUser";
    private static final String GMT_MODIFIED = "gmtModified";
    private static final String MODIFIED_ID = "modifiedId";
    private static final String MODIFIED_USER = "modifiedUser";


    private static final String DEFAULT_ID = "id";
    private static final String DEFAULT_USER = "username";


    @Bean
    public MybatisPlusInterceptor optimisticLockerInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        // 乐观锁
        interceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());
        // 防止全表更新或删除
        interceptor.addInnerInterceptor(new BlockAttackInnerInterceptor());
        // 分页插件
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }

    @Bean
    public SqlInjector sqlInjector() {
        return new SqlInjector();
    }

    /**
     * 自动插入处理(强制插入处理create_id、create_user、gmt_create、modified_id、modified_user、gmt_modified字段)
     *
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        Map<String, Object> userMap = getCurrentUser();
        Long id = Convert.toLong(userMap.get(DEFAULT_ID));
        String username = Convert.toStr(userMap.get(DEFAULT_USER));
        List<TableFieldInfo> fieldList = findTableInfo(metaObject).getFieldList();
        for (TableFieldInfo field : fieldList) {
            String property = field.getProperty();
            if (ObjectUtil.isNotNull(id) && CharSequenceUtil.equalsAny(property, CREATE_ID, MODIFIED_ID)) {
                metaObject.setValue(property, id);
            } else if (CharSequenceUtil.isNotBlank(username) && CharSequenceUtil.equalsAny(property, CREATE_USER, MODIFIED_USER)) {
                metaObject.setValue(property, username);
            } else if (CharSequenceUtil.equalsAny(property, GMT_CREATE, GMT_MODIFIED)) {
                metaObject.setValue(property, now());
            }
        }
    }

    /**
     * 自动更新处理(强制更新处理modified_id、modified_user、gmt_modified字段)
     *
     * @param metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        Map<String, Object> userMap = getCurrentUser();
        Long id = Convert.toLong(userMap.get(DEFAULT_ID));
        String username = Convert.toStr(userMap.get(DEFAULT_USER));
        List<TableFieldInfo> fieldList = findTableInfo(metaObject).getFieldList();
        for (TableFieldInfo field : fieldList) {
            String property = field.getProperty();
            if (ObjectUtil.isNotNull(id) && CharSequenceUtil.equals(MODIFIED_ID, property)) {
                metaObject.setValue(property, id);
            } else if (CharSequenceUtil.isNotBlank(username) && CharSequenceUtil.equals(MODIFIED_USER, property)) {
                metaObject.setValue(property, username);
            } else if (CharSequenceUtil.equals(GMT_MODIFIED, property)) {
                metaObject.setValue(property, now());
            }
        }
    }

    /**
     * 获取当前时间
     *
     * @return
     */
    private static Date now() {
        return new Date();
    }

    /**
     * 获取当前用户信息(id、username)
     *
     * @return
     */
    private Map<String, Object> getCurrentUser() {
        Map<String, Object> userMap = new HashMap<>(4);
        userMap.put(DEFAULT_ID, SystemProperties.userId);
        userMap.put(DEFAULT_USER, SystemProperties.username);
        // 获取缓存中的用户信息, 如果获取全局request异常, 则直接返回默认信息(避免类似于任务调度日志记录等没有request对象参与的操作而运行报错)
        String token = null;
        try {
            HttpServletRequest request = NetUtil.getGlobalRequest();
            token = request.getHeader(Constants.ACCESS_TOKEN);
        } catch (Exception e) {
            return userMap;
        }
        if (CharSequenceUtil.isNotBlank(token)) {
            JwtUser jwtUser = JwtUtil.parseToken(token);
            if (ObjectUtil.isNotNull(jwtUser) && CharSequenceUtil.isNotBlank(jwtUser.getUuid())) {
                JwtUser cacheJwtUser = JwtUtil.parseToken(redisService.get(Constants.REDIS_PREFIX.TOKEN + StrPool.COLON + jwtUser.getUuid()));
                if (ObjectUtil.isNotNull(cacheJwtUser)) {
                    if (ObjectUtil.isNotNull(cacheJwtUser.getId())) {
                        userMap.put(DEFAULT_ID, cacheJwtUser.getId());
                    }
                    if (CharSequenceUtil.isNotBlank(cacheJwtUser.getUsername())) {
                        userMap.put(DEFAULT_USER, cacheJwtUser.getUsername());
                    }
                }
            }
        }
        return userMap;
    }

}
