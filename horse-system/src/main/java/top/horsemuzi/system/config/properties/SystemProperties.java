package top.horsemuzi.system.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * 系统全局属性配置
 *
 * @author mabin
 * @date 2022/05/12 09:32
 **/
@Component
@ConfigurationProperties(prefix = "custom.system")
public class SystemProperties implements Serializable {

    private static final long serialVersionUID = 6141859173223589245L;

    /**
     * 项目名称
     */
    public static String name;

    /**
     * 项目版本
     */
    public static String version;

    /**
     * 项目初始用户ID
     */
    public static Long userId;

    /**
     * 项目处理用户名
     */
    public static String username;

    /**
     * 作者
     */
    public static String author;

    /**
     * 网站
     */
    public static String webSite;

    /**
     * QQ号
     */
    public static String qq;

    /**
     * 邮箱
     */
    public static String email;

    /**
     * 微信号
     */
    public static String wx;

    public void setName(String name) {
        SystemProperties.name = name;
    }

    public void setVersion(String version) {
        SystemProperties.version = version;
    }

    public void setUserId(Long userId) {
        SystemProperties.userId = userId;
    }

    public void setUsername(String username) {
        SystemProperties.username = username;
    }

    public void setAuthor(String author) {
        SystemProperties.author = author;
    }

    public void setWebSite(String webSite) {
        SystemProperties.webSite = webSite;
    }

    public void setQq(String qq) {
        SystemProperties.qq = qq;
    }

    public void setEmail(String email) {
        SystemProperties.email = email;
    }

    public void setWx(String wx) {
        SystemProperties.wx = wx;
    }

}
