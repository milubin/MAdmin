package top.horsemuzi.system.config.filter.register;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import top.horsemuzi.system.config.filter.GlobalFilter;
import top.horsemuzi.system.config.filter.SecurityFilter;
import top.horsemuzi.system.config.filter.XssFilter;
import top.horsemuzi.system.config.properties.XssProperties;

import javax.servlet.DispatcherType;
import java.util.Collections;

/**
 * 注册全局过滤器
 *
 * @author mabin
 * @date 2022/05/06 15:47
 **/
@RequiredArgsConstructor
@Configuration
public class FilterRegisterConfig {

    private final XssProperties xssProperties;

    @Bean
    @ConditionalOnProperty(prefix = "custom.xss", name = "enable", havingValue = "true")
    public FilterRegistrationBean<XssFilter> xssFilterRegistration() {
        FilterRegistrationBean<XssFilter> filterRegistrationBean = new FilterRegistrationBean<>(new XssFilter());
        filterRegistrationBean.setDispatcherTypes(DispatcherType.REQUEST);
        filterRegistrationBean.setOrder(Ordered.HIGHEST_PRECEDENCE + 3);
        filterRegistrationBean.setInitParameters(Collections.singletonMap("excludes", xssProperties.getExcludes()));
        filterRegistrationBean.setName("xssFilter");
        filterRegistrationBean.addUrlPatterns(xssProperties.getUrlPatterns());
        return filterRegistrationBean;
    }

    @Bean
    @ConditionalOnProperty(prefix = "custom.security", name = "enable", havingValue = "true")
    public FilterRegistrationBean<SecurityFilter> securityFilterRegistration() {
        FilterRegistrationBean<SecurityFilter> filterRegistrationBean = new FilterRegistrationBean<>(new SecurityFilter());
        filterRegistrationBean.setDispatcherTypes(DispatcherType.REQUEST);
        filterRegistrationBean.setOrder(Ordered.HIGHEST_PRECEDENCE + 2);
        filterRegistrationBean.setName("securityFilter");
        filterRegistrationBean.addUrlPatterns("/admin/*", "/portal/*", "/common/*");
        return filterRegistrationBean;
    }

    @Bean
    public FilterRegistrationBean<GlobalFilter> registrationBean() {
        FilterRegistrationBean<GlobalFilter> filterRegistrationBean = new FilterRegistrationBean<>(new GlobalFilter());
        filterRegistrationBean.setDispatcherTypes(DispatcherType.REQUEST);
        // Filter执行顺序, 值越小优先级越高
        filterRegistrationBean.setOrder(Ordered.HIGHEST_PRECEDENCE + 1);
        filterRegistrationBean.setName("globalFilter");
        filterRegistrationBean.addUrlPatterns("/*");
        return filterRegistrationBean;
    }

}
