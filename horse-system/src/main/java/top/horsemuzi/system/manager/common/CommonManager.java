package top.horsemuzi.system.manager.common;

import cn.hutool.captcha.CircleCaptcha;
import cn.hutool.captcha.GifCaptcha;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.captcha.ShearCaptcha;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ConfigEnum;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.*;
import top.horsemuzi.core.message.common.model.AliyunSmsModel;
import top.horsemuzi.core.message.common.model.EmailContentModel;
import top.horsemuzi.system.config.properties.TokenProperties;
import top.horsemuzi.system.core.redis.service.RedisService;
import top.horsemuzi.system.pojo.dto.division.DivisionDTO;
import top.horsemuzi.system.service.ConfigService;
import top.horsemuzi.system.service.DivisionService;
import top.horsemuzi.system.service.MessageService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 系统公共处理层
 *
 * @author mabin
 * @date 2022/05/10 08:57
 **/
@Slf4j
@RequiredArgsConstructor
@Component
public class CommonManager {

    private final ConfigService configService;
    private final TokenProperties tokenProperties;
    private final RedisService redisService;
    private final DivisionService divisionService;
    private final MessageService messageService;

    // =============================================验证码相关========================================

    /**
     * 生成验证码
     *
     * @return java.util.Map 自定义验证码信息
     */
    public Map<String, Object> generateKaptcha() {
        Map<String, Object> kaptchaMap = MapUtil.newHashMap(16);
        // 获取验证码系统配置
        String kaptchaSwitchKey = ConfigEnum.KAPTCHA_SWITCH.getKey();
        String kaptchaTypeKey = ConfigEnum.KAPTCHA_TYPE.getKey();
        String generatorTypeKey = ConfigEnum.KAPTCHA_GENERATOR_TYPE.getKey();
        Map<String, String> configMap = configService.getConfigValueMap(ConfigEnum.GROUP.KAPTCHA, kaptchaSwitchKey,
                kaptchaTypeKey, generatorTypeKey);
        if (CollectionUtil.isEmpty(configMap)) {
            return kaptchaMap;
        }
        String kaptchaSwitch = configMap.get(kaptchaSwitchKey);
        String kaptchaType = configMap.get(kaptchaTypeKey);
        String kaptchaGeneratorType = configMap.get(generatorTypeKey);
        ValidatorUtil.isTrue(StrUtil.isNotBlank(kaptchaSwitch), ResultCodeEnum.KAPTCHA_NOT_SETTING_ERROR);
        kaptchaMap.put(kaptchaSwitchKey, kaptchaSwitch);
        if (!StrUtil.equals(Constants.COMMON_JUDGMENT.ON, kaptchaSwitch)) {
            return kaptchaMap;
        }
        // 生成验证码
        String code = null;
        String img = null;
        if (StrUtil.isBlank(kaptchaType) || StrUtil.equals(KaptchaUtil.KaptchaType.LINE, kaptchaType)) {
            // 线性干扰
            LineCaptcha lineCaptcha = KaptchaUtil.createLineCaptcha(kaptchaGeneratorType);
            img = lineCaptcha.getImageBase64();
            code = lineCaptcha.getCode();
            if (StrUtil.equals(kaptchaGeneratorType, KaptchaUtil.GeneratorType.RANDOM) || StrUtil.isBlank(kaptchaGeneratorType)) {
                // 生成器random || 未配置
                code = lineCaptcha.getCode();
            } else if (StrUtil.equals(kaptchaGeneratorType, KaptchaUtil.GeneratorType.MATH)) {
                // 四则运算
                code = KaptchaUtil.getKaptchaMathResult(lineCaptcha.getCode());
            }
        } else if (StrUtil.equals(KaptchaUtil.KaptchaType.CIRCLE, kaptchaType)) {
            // 圆圈干扰
            CircleCaptcha circleCaptcha = KaptchaUtil.createCircleCaptcha(kaptchaGeneratorType);
            img = circleCaptcha.getImageBase64();
            code = circleCaptcha.getCode();
            if (StrUtil.equals(kaptchaGeneratorType, KaptchaUtil.GeneratorType.RANDOM) || StrUtil.isBlank(kaptchaGeneratorType)) {
                code = circleCaptcha.getCode();
            } else if (StrUtil.equals(kaptchaGeneratorType, KaptchaUtil.GeneratorType.MATH)) {
                code = KaptchaUtil.getKaptchaMathResult(circleCaptcha.getCode());
            }
        } else if (StrUtil.equals(KaptchaUtil.KaptchaType.SHEAR, kaptchaType)) {
            // 干扰线
            ShearCaptcha shearCaptcha = KaptchaUtil.createShearCaptcha(kaptchaGeneratorType);
            img = shearCaptcha.getImageBase64();
            code = shearCaptcha.getCode();
            if (StrUtil.equals(kaptchaGeneratorType, KaptchaUtil.GeneratorType.RANDOM) || StrUtil.isBlank(kaptchaGeneratorType)) {
                code = shearCaptcha.getCode();
            } else if (StrUtil.equals(kaptchaGeneratorType, KaptchaUtil.GeneratorType.MATH)) {
                code = KaptchaUtil.getKaptchaMathResult(shearCaptcha.getCode());
            }
        } else if (StrUtil.equals(KaptchaUtil.KaptchaType.GIF, kaptchaType)) {
            // gif动态
            GifCaptcha gifCaptcha = KaptchaUtil.createGifCaptcha(kaptchaGeneratorType);
            img = gifCaptcha.getImageBase64();
            code = gifCaptcha.getCode();
            if (StrUtil.equals(kaptchaGeneratorType, KaptchaUtil.GeneratorType.RANDOM) || StrUtil.isBlank(kaptchaGeneratorType)) {
                code = gifCaptcha.getCode();
            } else if (StrUtil.equals(kaptchaGeneratorType, KaptchaUtil.GeneratorType.MATH)) {
                code = KaptchaUtil.getKaptchaMathResult(gifCaptcha.getCode());
            }
        }
        if (!StrUtil.isAllNotBlank(code, img)) {
            return kaptchaMap;
        }
        // 组装返回验证码信息
        String uuid = IdUtil.fastSimpleUUID();
        // 验证码存储, 过期时间: 默认5min
        redisService.set(Constants.REDIS_PREFIX.KAPTCHA + uuid, code,
                Constants.REDIS_DEFAULT_EXPIRE_TIME, TimeUnit.MINUTES);
        kaptchaMap.put("uuid", uuid);
        kaptchaMap.put("img", img);
        return kaptchaMap;
    }


    // =============================================token相关========================================

    /**
     * 创建双token
     * 虚拟token: 用于前后端传输的token, 载荷信息只存储uuid
     * 真实token: 用于鉴权以及获取用户信息等
     *
     * @param jwtUser
     * @return
     */
    public String createToken(@NonNull JwtUser jwtUser) {
        // 创建虚拟token和真实token
        String token = JwtUtil.createToken(new JwtUser()
                .setUuid(jwtUser.getUuid())
                .setId(jwtUser.getId())
                .setUsername(jwtUser.getUsername()));
        String cacheToken = JwtUtil.createToken(jwtUser);
        ValidatorUtil.isTrue(StrUtil.isAllNotBlank(token, cacheToken), ResultCodeEnum.TOKEN_CREATION_FAILED);
        // 存入redis
        redisService.set(getTokenKey(jwtUser.getUuid()), cacheToken, tokenProperties.getExpire(), TimeUnit.SECONDS);
        return token;
    }

    /**
     * 校验token的有效性并无感刷新token
     *
     * @return
     */
    public Boolean verifyToken() {
        return verifyToken(NetUtil.getGlobalRequest());
    }

    /**
     * 校验token的有效性并无感刷新token
     *
     * @param request
     * @return
     */
    public Boolean verifyToken(HttpServletRequest request) {
        // 获取token
        String token = request.getHeader(Constants.ACCESS_TOKEN);
        if (StrUtil.isBlank(token)) {
            return Boolean.FALSE;
        }
        // 校验token的有效性
        if (!JwtUtil.verifyToken(token)) {
            return Boolean.FALSE;
        }
        // 解析uuid
        JwtUser jwtUser = JwtUtil.parseToken(token);
        if (ObjectUtil.isNull(jwtUser)) {
            return Boolean.FALSE;
        }
        String uuid = jwtUser.getUuid();
        if (StrUtil.isBlank(uuid)) {
            return Boolean.FALSE;
        }
        // 获取真实token
        String cacheToken = redisService.get(getTokenKey(uuid));
        if (StrUtil.isBlank(cacheToken)) {
            return Boolean.FALSE;
        }
        // 校验真实token的有效性
        if (!JwtUtil.verifyToken(cacheToken)) {
            return Boolean.FALSE;
        }
        // 判断token是否将在10分钟内失效而进行无感刷新token失效时间

        // 从redis中获取key对应的过期时间;
        // 如果该值有过期时间，就返回相应的过期时间;
        // 如果该值没有设置过期时间，就返回-1;
        // 如果没有该值，就返回-2;
        Long expire = redisService.getExpire(getTokenKey(uuid), TimeUnit.SECONDS);
        if (expire == null || expire == -2) {
            return Boolean.FALSE;
        }
        Date currentDate = new Date();
        if (expire > 1 && DateUtil.between(DateUtil.offset(currentDate, DateField.SECOND, Convert.toInt(expire)),
                currentDate, DateUnit.SECOND, true) < tokenProperties.getRefresh()) {
            redisService.set(getTokenKey(uuid), cacheToken, tokenProperties.getExpire(), TimeUnit.SECONDS);
        }
        // 返回校验结果
        return Boolean.TRUE;
    }

    /**
     * 获取并解析缓存的token信息
     *
     * @param request
     * @return
     */
    public JwtUser getCacheJwtUser(HttpServletRequest request) {
        return getCacheJwtUserByUuid(getCurrentTokenUuid(request));
    }


    /**
     * 获取并解析缓存的token信息
     *
     * @return
     */
    public JwtUser getCacheJwtUser() {
        return getCacheJwtUserByUuid(getCurrentTokenUuid(NetUtil.getGlobalRequest()));
    }


    /**
     * 获取并解析缓存的token信息
     *
     * @param token
     * @return
     */
    public JwtUser getCacheJwtUser(String token) {
        if (StrUtil.isBlank(token)) {
            return null;
        }
        JwtUser jwtUser = JwtUtil.parseToken(token);
        if (ObjectUtil.isNull(jwtUser)) {
            return null;
        }
        return getCacheJwtUserByUuid(jwtUser.getUuid());
    }

    /**
     * 获取当前token中的uuid
     *
     * @param request
     * @return
     */
    public String getCurrentTokenUuid(HttpServletRequest request) {
        String token = request.getHeader(Constants.ACCESS_TOKEN);
        if (StrUtil.isBlank(token)) {
            return null;
        }
        JwtUser jwtUser = JwtUtil.parseToken(token);
        if (ObjectUtil.isNull(jwtUser)) {
            return null;
        }
        return jwtUser.getUuid();
    }

    /**
     * 获取并解析缓存的token信息
     *
     * @param uuid
     * @return
     */
    private JwtUser getCacheJwtUserByUuid(String uuid) {
        if (StrUtil.isBlank(uuid)) {
            return null;
        }
        String token = redisService.get(getTokenKey(uuid));
        if (StrUtil.isBlank(token)) {
            return null;
        }
        return JwtUtil.parseToken(token);
    }

    /**
     * 获取组装后的token的key
     *
     * @param uuid
     * @return
     */
    public String getTokenKey(String uuid) {
        if (StrUtil.isBlank(uuid)) {
            return StrUtil.EMPTY;
        }
        return Constants.REDIS_PREFIX.TOKEN + StrUtil.COLON + uuid;
    }

    /**
     * 获取Gitee仓库提交信息(Jsoup爬虫解析)
     */
    public Map<String, Object> getGiteeCommits() {
        // 若配置项不存在, 仓库个人主页默认为 milubin
        String giteeName = "milubin";
        String configGiteeName = configService.getConfigValue(ConfigEnum.GROUP.GITEE, ConfigEnum.GITEE_USER.getKey());
        if (StrUtil.isNotBlank(configGiteeName)) {
            giteeName = configGiteeName;
        }
        Map<String, Object> giteeMap = new HashMap<>(16);
        try {
            Document document = Jsoup.connect("https://gitee.com/" + giteeName)
                    .timeout(5000)
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Content-type", "application/json")
                    .userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) " +
                            "Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.62")
                    .get();
            Elements dateElements = document.getElementsByClass("right-side").select("div[data-content]");
            if (ObjectUtil.isNotNull(dateElements)) {
                // 开始/结束日期
                List<String> rangeData = new ArrayList<>();
                // 日期/贡献量
                List<List<Object>> dateData = new ArrayList<>();
                List<Object> dateAndCount = null;
                int size = dateElements.size();
                for (int i = 0; i < size; i++) {
                    dateAndCount = new ArrayList<>();
                    String attr = dateElements.get(i).attr("data-content");
                    Integer count = ReUtil.getFirstNumber(attr);
                    String date = StrUtil.split(attr, "：").get(1);
                    dateAndCount.add(date);
                    dateAndCount.add(count);
                    if (i == 0 || i == size - 1) {
                        rangeData.add(date);
                    }
                    dateData.add(dateAndCount);
                }
                giteeMap.put("rangeData", rangeData);
                giteeMap.put("dateData", dateData);
            }
            // 贡献量统计
            Elements contributionElements = document.select(".contribution-describ p");
            if (ObjectUtil.isNotNull(contributionElements)) {
                List<Integer> contributionData = new ArrayList<>();
                int sumSize = contributionElements.size();
                for (int i = 0; i < sumSize - 1; i++) {
                    contributionData.add(ReUtil.getFirstNumber(contributionElements.get(i).text()));
                }
                giteeMap.put("contributionData", contributionData);
            }
        } catch (IOException e) {
            log.error("获取Gitee贡献信息异常: {}", ExceptionUtil.getMessage(e));
            throw new BusinessException(e, ResultCodeEnum.INTERNAL_ERROR);
        }
        return giteeMap;
    }

    /**
     * 获取区划级联数据
     *
     * @param id
     * @return
     */
    public List<DivisionDTO> getDivisionCascader(Long id) {
        ValidatorUtil.notEmpty(id);
        return divisionService.getDivisionCascader(id);
    }

    /**
     * 发送邮箱验证码
     * 逻辑:
     * 1、邮箱发送次数限制(默认: 每个邮箱号每天限制100条, 无视功能模块)
     * 2、获取验证码配置信息(验证码位数、类型、有效期)
     * 3、生成验证码、组装数据发送邮箱验证码
     * 4、缓存验证码
     *
     * @param email
     */
    public void sendEmail(String email) {
        // 邮箱功能是否启用
        String smsEnabled = configService.getConfigValue(ConfigEnum.VERIFY_EMAIL_ENABLED);
        ValidatorUtil.isTrue(StrUtil.equals(Constants.COMMON_JUDGMENT.YES, smsEnabled), ResultCodeEnum.KAPTCHA_EMAIL_NOT_ENABLED_ERROR);
        // 邮箱发送次数是否超出天限制
        String cacheLimitKey = Constants.REDIS_PREFIX.VERIFY_CODE + StrUtil.COLON + DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN) + StrUtil.COLON + email;
        Integer sendCount = Convert.toInt(redisService.get(cacheLimitKey), Constants.COMMON_CODE.ZONE);

        Map<String, String> configValueMap = configService.getConfigValueMap(ConfigEnum.GROUP.VERIFY);
        // 次数限制
        Integer maxSendCount = Convert.toInt(configValueMap.get(ConfigEnum.VERIFY_EMAIL_LIMIT.getKey()), Constants.COMMON_CODE.ONE_HUNDRED);
        ValidatorUtil.isTrue(sendCount < maxSendCount, ResultCodeEnum.VERIFY_THAN_SEND_LIMIT_ERROR);
        // 长度
        Integer length = Convert.toInt(configValueMap.get(ConfigEnum.VERIFY_LENGTH.getKey()), Constants.COMMON_CODE.FOUR);
        // 1-纯数字/2-纯字母/3-混合码
        Integer type = Convert.toInt(configValueMap.get(ConfigEnum.VERIFY_TYPE.getKey()), Constants.COMMON_CODE.ONE);
        // 有效期, 默认5分钟
        Integer timeout = Convert.toInt(configValueMap.get(ConfigEnum.VERIFY_CACHE_TIME.getKey()), Constants.COMMON_CODE.FIVE);

        String code = null;
        if (ObjectUtil.equals(Constants.COMMON_CODE.TWO, type)) {
            code = RandomUtil.randomString(RandomUtil.BASE_CHAR, type);
        } else if (ObjectUtil.equals(Constants.COMMON_CODE.THREE, type)) {
            code = RandomUtil.randomString(length);
        } else {
            code = RandomUtil.randomNumbers(length);
        }

        EmailContentModel model = new EmailContentModel();
        model.setTitle("【MAdmin系统】");
        model.setTos(email);
        model.setContent(StrUtil.format("【MAdmin系统】验证码: {}, 有效期{}分钟, 如非本人操作, 请忽略", code, timeout));
        messageService.sendEmailMessage(model);

        String key = Constants.REDIS_PREFIX.VERIFY_CODE + StrUtil.COLON + email;
        redisService.set(key, code, Duration.ofMinutes(timeout));

        // 缓存发送次数
        Long expireTimeout;
        if (redisService.hasKey(cacheLimitKey)) {
            expireTimeout = redisService.getExpire(cacheLimitKey, TimeUnit.MINUTES);
        } else {
            // 默认key值一天后过期
            expireTimeout = Duration.ofHours(25).toMinutes();
        }
        redisService.set(cacheLimitKey, Convert.toStr(sendCount + Constants.COMMON_CODE.ONE), expireTimeout, TimeUnit.MINUTES);
    }

    /**
     * 发送短信验证码
     * 逻辑:
     * 1、短信发送次数限制(无视功能模块)
     * 2、生成并缓存验证码
     *
     * @param phone
     */
    public void sendSms(String phone) {
        // 短信功能是否启用
        String smsEnabled = configService.getConfigValue(ConfigEnum.VERIFY_SMS_ENABLED);
        ValidatorUtil.isTrue(StrUtil.equals(Constants.COMMON_JUDGMENT.YES, smsEnabled), ResultCodeEnum.KAPTCHA_SMS_NOT_ENABLED_ERROR);
        // 短信发送次数是否超天限制
        String cacheLimitKey = Constants.REDIS_PREFIX.VERIFY_CODE + StrUtil.COLON + DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN) + StrUtil.COLON + phone;
        Integer sendCount = Convert.toInt(redisService.get(cacheLimitKey), Constants.COMMON_CODE.ZONE);
        Integer maxSendCount = Convert.toInt(configService.getConfigValue(ConfigEnum.VERIFY_SMS_LIMIT), Constants.COMMON_CODE.FIVE);
        ValidatorUtil.isTrue(sendCount < maxSendCount, ResultCodeEnum.VERIFY_THAN_SEND_LIMIT_ERROR);
        // 组装参数, 发送短信
        AliyunSmsModel model = new AliyunSmsModel();
        model.setPhones(phone);
        // 生成6位纯数字验证码
        String code = RandomUtil.randomNumbers(6);
        model.setParamsMap(Collections.singletonMap("code", code));
        model.setTemplateId(configService.getConfigValue(ConfigEnum.VERIFY_SMS_TEMPLATE));
        messageService.sendSmsMessage(model);
        // 缓存验证码
        String key = Constants.REDIS_PREFIX.VERIFY_CODE + StrUtil.COLON + phone;
        redisService.set(key, code, Duration.ofMinutes(Convert.toInt(configService.getConfigValue(ConfigEnum.VERIFY_CACHE_TIME), Constants.COMMON_CODE.FIVE)));
        // 缓存发送次数
        Long expireTimeout;
        if (redisService.hasKey(cacheLimitKey)) {
            expireTimeout = redisService.getExpire(cacheLimitKey, TimeUnit.MINUTES);
        } else {
            // 默认key值一天后过期
            expireTimeout = Duration.ofHours(25).toMinutes();
        }
        redisService.set(cacheLimitKey, Convert.toStr(sendCount + Constants.COMMON_CODE.ONE), expireTimeout, TimeUnit.MINUTES);
    }

    /**
     * 系统模板文件下载
     *
     * @param group
     * @param key
     * @param response
     */
    public void template(String group, String key, HttpServletResponse response) {
        ValidatorUtil.allNotBlank(group, key);
        // 根据系统配置, 查询模板信息
        InputStream inputStream = null;
        try {
            String templateUrl = configService.getConfigValue(group, key);
            ValidatorUtil.notBlank(templateUrl, ResultCodeEnum.RESOURCE_NOT_EXIST);
            // 下载模板文件
            CommonCodeUtil.generateAttachmentResponseHeader(response, "信息导入模板.xlsx");
            inputStream = new URL(templateUrl).openStream();
            IoUtil.copy(inputStream, response.getOutputStream());
        } catch (Exception e) {
            throw new BusinessException(e, ResultCodeEnum.OSS_SERVICE_DOWNLOAD_ERROR);
        } finally {
            IoUtil.close(inputStream);
        }
    }

}
