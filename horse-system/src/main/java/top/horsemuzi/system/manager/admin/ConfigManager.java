package top.horsemuzi.system.manager.admin;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.core.redis.service.RedisService;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.config.ConfigDTO;
import top.horsemuzi.system.pojo.dto.config.ConfigQuery;
import top.horsemuzi.system.pojo.dto.dict.DictTypeDTO;
import top.horsemuzi.system.service.ConfigService;
import top.horsemuzi.system.service.DictTypeService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 系统配置模块
 *
 * @author mabin
 * @date 2022/06/24 14:48
 **/
@Slf4j
@RequiredArgsConstructor
@Component
public class ConfigManager {

    private final ConfigService configService;
    private final RedisService redisService;
    private final DictTypeService dictTypeService;

    public String getConfigKey(String group) {
        if (StrUtil.isBlank(group)) {
            return null;
        }
        return Constants.REDIS_PREFIX.CONFIG + StrUtil.COLON + group;
    }


    /**
     * 分页获取系统配置列表
     *
     * @param configQuery
     * @return
     */
    public Page<ConfigDTO> getPageConfigs(ConfigQuery configQuery) {
        ValidatorUtil.notEmpty(configQuery);
        return configService.getPageConfigs(configQuery);
    }


    /**
     * 新增系统配置信息
     *
     * @param configDTO
     */
    public void addConfig(ConfigDTO configDTO) {
        ValidatorUtil.notEmpty(configDTO);
        // 是否重复
        ConfigDTO queryConfigDTO = new ConfigDTO();
        queryConfigDTO.setConfigGroup(configDTO.getConfigGroup());
        queryConfigDTO.setConfigKey(configDTO.getConfigKey());
        ValidatorUtil.isEmpty(configService.checkConfigRepeat(queryConfigDTO), ResultCodeEnum.CONFIG_EXIST_ERROR);
        // 如果键值从字典类型选取，校验字典类型是否存在
        if (StrUtil.equals(Constants.COMMON_CODE.STR_ONE, configDTO.getConfigWay())) {
            DictTypeDTO query = new DictTypeDTO();
            query.setType(configDTO.getConfigValue());
            ValidatorUtil.notEmpty(dictTypeService.getDictType(query), ResultCodeEnum.DICT_TYPE_NOT_EXIST_ERROR);
        }
        // 新增系统配置
        configService.addConfig(configDTO);
        // 删除缓存信息(避免查询时, 无法获取最新的配置信息)
        redisService.delete(getConfigKey(configDTO.getConfigGroup()));
    }

    /**
     * 更新系统配置信息
     * 限制: 内置属性配置不允许修改配置组别和键值key的值
     *
     * @param configDTO
     */
    public void updateConfig(ConfigDTO configDTO) {
        ValidatorUtil.notEmpty(configDTO);
        ValidatorUtil.notEmpty(configDTO.getId());
        // 是否存在
        List<ConfigDTO> configDTOList = configService.getConfigs(Collections.singletonList(configDTO.getId()));
        ValidatorUtil.notEmpty(configDTOList, ResultCodeEnum.CONFIG_NOT_EXIST_ERROR);
        ConfigDTO currentConfigDTO = configDTOList.get(0);
        // 是否内置
        if (StrUtil.equals(Constants.COMMON_JUDGMENT.Y, configDTO.getInternal())) {
            if (!StrUtil.equals(currentConfigDTO.getConfigGroup(), configDTO.getConfigGroup()) || !StrUtil.equals(currentConfigDTO.getConfigKey(), configDTO.getConfigKey())) {
                throw new BusinessException(ResultCodeEnum.NOT_OPERATE_INTERNAL_ERROR);
            }
        }
        // 是否重复
        ConfigDTO queryConfigDTO = new ConfigDTO();
        queryConfigDTO.setId(configDTO.getId());
        queryConfigDTO.setConfigGroup(configDTO.getConfigGroup());
        queryConfigDTO.setConfigKey(configDTO.getConfigKey());
        ValidatorUtil.isEmpty(configService.checkConfigRepeat(queryConfigDTO), ResultCodeEnum.CONFIG_EXIST_ERROR);
        // 如果键值从字典类型选取，校验字典类型是否存在
        if (StrUtil.equals(Constants.COMMON_CODE.STR_ONE, configDTO.getConfigWay())) {
            DictTypeDTO query = new DictTypeDTO();
            query.setType(configDTO.getConfigValue());
            ValidatorUtil.notEmpty(dictTypeService.getDictType(query), ResultCodeEnum.DICT_TYPE_NOT_EXIST_ERROR);
        }
        // 更新DB
        configService.updateConfig(configDTO);
        // 删除缓存
        redisService.delete(getConfigKey(configDTO.getConfigGroup()));
    }

    /**
     * 批量删除系统配置信息
     *
     * @param configIds
     */
    public void deleteConfigs(List<Long> configIds) {
        ValidatorUtil.notEmpty(configIds);
        // 是否存在
        List<ConfigDTO> configDTOList = configService.getConfigs(configIds);
        ValidatorUtil.notEmpty(configDTOList, ResultCodeEnum.RESOURCE_NOT_EXIST);
        // 是否内置
        List<String> configKeys = new ArrayList<>();
        for (ConfigDTO configDTO : configDTOList) {
            ValidatorUtil.isTrue(!StrUtil.equals(Constants.COMMON_JUDGMENT.Y, configDTO.getInternal()),
                    ResultCodeEnum.NOT_OPERATE_INTERNAL_ERROR);
            configKeys.add(getConfigKey(configDTO.getConfigGroup()));
        }
        // 删除DB
        configService.deleteConfigs(configIds);
        // 删除缓存(对应的组别信息应全部删除)
        redisService.delete(configKeys);
    }

    /**
     * 根据组别和key获取系统配置详情
     *
     * @param group
     * @param key
     * @return
     */
    public ConfigDTO getConfig(String group, String key) {
        ValidatorUtil.allNotEmpty(group, key);
        // 查询缓存
        String configKey = getConfigKey(group);
        List<ConfigDTO> configDTOList = null;
        String cacheConfig = redisService.get(configKey);
        if (StrUtil.isNotBlank(cacheConfig)) {
            configDTOList = JsonUtil.fromJson(cacheConfig, new TypeReference<List<ConfigDTO>>() {
            });
        } else {
            ConfigDTO queryConfigDTO = new ConfigDTO();
            queryConfigDTO.setConfigGroup(group);
            configDTOList = configService.getConfigs(queryConfigDTO);
        }
        ValidatorUtil.notEmpty(configDTOList, ResultCodeEnum.RESOURCE_NOT_EXIST);
        ConfigDTO configDTO = null;
        for (ConfigDTO item : configDTOList) {
            if (StrUtil.equals(key, item.getConfigKey())) {
                configDTO = item;
            }
        }
        ValidatorUtil.notEmpty(configDTO, ResultCodeEnum.RESOURCE_NOT_EXIST);
        // 录入缓存
        redisService.set(configKey, JsonUtil.toJson(configDTOList));
        return configDTO;
    }

    /**
     * 系统配置信息导出
     *
     * @param configQuery
     */
    public void export(ConfigQuery configQuery) {
        ValidatorUtil.notEmpty(configQuery);
        configService.export(configQuery);
    }

    /**
     * 刷新系统配置缓存信息
     */
    public void refresh() {
        // 匹配删除缓存中的配置信息
        redisService.deletePatternByPrefix(Constants.REDIS_PREFIX.CONFIG);
        // 查询DB并组装, 批量缓存所有配置信息
        List<ConfigDTO> configDTOList = configService.getConfigs(new ConfigDTO());
        if (CollectionUtil.isNotEmpty(configDTOList)) {
            Map<String, List<ConfigDTO>> configGroupMap = configDTOList.stream().collect(Collectors.groupingBy(ConfigDTO::getConfigGroup));
            Map<String, String> configCacheMap = new HashMap<>(16);
            for (Map.Entry<String, List<ConfigDTO>> entity : configGroupMap.entrySet()) {
                configCacheMap.put(getConfigKey(entity.getKey()), JsonUtil.toJson(entity.getValue()));
            }
            redisService.multiSet(configCacheMap);
        }
    }

    /**
     * 配置参数名称实时检索
     *
     * @param search
     * @return
     */
    public List<InputRemoteFetchDTO> fetch(String search) {
        return configService.fetch(search);
    }
}
