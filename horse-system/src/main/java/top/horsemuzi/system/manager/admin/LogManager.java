package top.horsemuzi.system.manager.admin;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.log.*;
import top.horsemuzi.system.pojo.dto.statistics.PieChartDTO;
import top.horsemuzi.system.service.LoginLogService;
import top.horsemuzi.system.service.SystemLogService;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 系统日志模块
 *
 * @author mabin
 * @date 2022/08/06 11:50
 **/

@Slf4j
@RequiredArgsConstructor
@Component
public class LogManager {

    @Value("${spring.profiles.active}")
    private String env;

    private static String LOG_PATH;

    @Value("${custom.log.path}")
    public void setLogPath(String path) {
        LOG_PATH = path + "horse-system";
    }

    private final SystemLogService logService;
    private final LoginLogService loginLogService;

    /**
     * 分页获取系统日志列表
     *
     * @param query
     * @return
     */
    public Page<SystemLogDTO> getPageLogs(SystemLogQuery query) {
        return logService.getPageLogs(query);
    }

    /**
     * 获取系统日志详情
     *
     * @param id
     * @return
     */
    public SystemLogDTO getLog(Long id) {
        ValidatorUtil.notEmpty(id);
        return logService.getLog(id);
    }

    /**
     * 删除系统日志
     *
     * @param logIds
     */
    public void deleteLogs(List<Long> logIds) {
        ValidatorUtil.notEmpty(logIds);
        logService.deleteLogs(logIds);
    }

    /**
     * 获取系统日志统计数据
     *
     * @return
     */
    public Map<String, Object> statisticsLog() {
        return logService.statisticsLog();
    }

    /**
     * 导出系统操作日志
     *
     * @param query
     * @param response
     */
    public void export(SystemLogQuery query, HttpServletResponse response) {
        logService.export(query, response);
    }

    /**
     * 操作日志链路ID实时检索
     *
     * @param search
     * @return
     */
    public List<InputRemoteFetchDTO> logFetch(String search) {
        return logService.logFetch(search);
    }

    /**
     * 分页获取登录日志列表
     *
     * @param query
     * @return
     */
    public Page<LoginLogDTO> getPageLoginLogs(LoginLogQuery query) {
        return loginLogService.getPageLoginLogs(query);
    }

    /**
     * 导出系统登录日志(CSV)
     *
     * @param query
     * @param response
     */
    public void loginExport(LoginLogQuery query, HttpServletResponse response) {
        loginLogService.loginExport(query, response);
    }

    /**
     * 删除登录日志
     *
     * @param loginLogIds
     */
    public void deleteLoginLogs(List<Long> loginLogIds) {
        ValidatorUtil.notEmpty(loginLogIds);
        loginLogService.deleteLoginLog(loginLogIds);
    }

    /**
     * 获取登录日志详情
     *
     * @param id
     * @return
     */
    public LoginLogDTO getLoginLog(Long id) {
        ValidatorUtil.notEmpty(id);
        return loginLogService.getLoginLog(id);
    }

    /**
     * 获取登录日志统计数据
     * 1、成功失败饼图
     * 2、异常原因饼图
     *
     * @return
     */
    public Map<String, Object> statisticsLoginLog() {
        Map<String, Object> chartMap = new HashMap<>(4);
        // 成功失败分组饼图
        List<PieChartDTO> basePieChartDTOList = loginLogService.statisticsBasePieChartData();
        chartMap.put("pieOne", basePieChartDTOList);
        // 异常原因饼图
        List<PieChartDTO> pieChartDTOList = loginLogService.statisticsPieChartData();
        chartMap.put("pieTwo", pieChartDTOList);
        return chartMap;
    }

    /**
     * 登录日志链路ID实时检索
     *
     * @param search
     * @return
     */
    public List<InputRemoteFetchDTO> loginFetch(String search) {
        return loginLogService.loginFetch(search);
    }

}
