package top.horsemuzi.system.manager.admin;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.EncryptUtil;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.core.message.common.channel.*;
import top.horsemuzi.core.message.enums.SendEnum;
import top.horsemuzi.system.config.properties.EncryptProperties;
import top.horsemuzi.system.core.office.service.OfficeService;
import top.horsemuzi.system.core.redis.service.RedisService;
import top.horsemuzi.system.pojo.dto.message.ChannelConfigDTO;
import top.horsemuzi.system.pojo.dto.message.MessageChannelDTO;
import top.horsemuzi.system.pojo.dto.message.MessageDTO;
import top.horsemuzi.system.pojo.dto.message.MessageQuery;
import top.horsemuzi.system.pojo.dto.statistics.PieChartDTO;
import top.horsemuzi.system.pojo.entity.Message;
import top.horsemuzi.system.service.MessageChannelService;
import top.horsemuzi.system.service.MessageService;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 消息模块
 *
 * @author 马滨
 * @date 2022/09/25 18:26
 **/
@Slf4j
@RequiredArgsConstructor
@Component
public class MessageManager {

    private final MessageService messageService;
    private final RedisService redisService;
    private final MessageChannelService messageChannelService;


    /**
     * 分页获取消息记录列表
     *
     * @param query
     * @return
     */
    @Transactional(readOnly = true)
    public Page<MessageDTO> getPageMessages(MessageQuery query) {
        ValidatorUtil.notEmpty(query);
        Page<Message> page = new Page<>(query.getPage(), query.getSize());
        LambdaQueryWrapper<Message> wrapper = getMessageWrapper(query);
        wrapper.select(Message::getId, Message::getChannel, Message::getRecordId, Message::getStreamName, Message::getStreamGroup,
                Message::getStatus, Message::getRetryCount, Message::getDeleted, Message::getRemark, Message::getGmtCreate, Message::getCreateId,
                Message::getCreateUser, Message::getGmtModified, Message::getModifiedId, Message::getModifiedUser);
        return Message.convertPage(messageService.page(page, wrapper), MessageDTO.class);
    }

    /**
     * 导出消息记录数据(CSV)
     *
     * @param query
     * @param response
     */
    public void export(MessageQuery query, HttpServletResponse response) {
        LambdaQueryWrapper<Message> wrapper = getMessageWrapper(query);
        List<MessageDTO> messageDTOList = OrikaUtil.convertList(messageService.list(wrapper), MessageDTO.class);
        if (CollectionUtil.isNotEmpty(messageDTOList)) {
            try {
                messageDTOList.forEach(dto -> {
                    dto.setChannelName(SendEnum.getChannelName(dto.getChannel()));
                    // csv文件会根据json字符串中的双引号进行分割内容，需要去除双引号
                    dto.setContent(StrUtil.replace(dto.getContent(), "\"", StrUtil.EMPTY));
                });
                OfficeService.CSV.export(response, MessageDTO.class, messageDTOList, "消息记录信息.csv");
            } catch (Exception e) {
                throw new BusinessException(ResultCodeEnum.FILE_EXPORT_ERROR);
            }
        }
    }

    private LambdaQueryWrapper<Message> getMessageWrapper(MessageQuery query) {
        LambdaQueryWrapper<Message> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ObjectUtil.isNotNull(query.getChannel()), Message::getChannel, query.getChannel())
                .eq(StrUtil.isNotBlank(query.getStatus()), Message::getStatus, query.getStatus())
                .eq(StrUtil.isNotBlank(query.getRecordId()), Message::getRecordId, query.getRecordId())
                .like(StrUtil.isNotBlank(query.getStreamName()), Message::getStreamName, query.getStreamName())
                .likeRight(StrUtil.isNotBlank(query.getContent()), Message::getContent, query.getContent())
                .ge(ObjectUtil.isNotNull(query.getCreateStart()), Message::getGmtCreate, query.getCreateStart())
                .le(ObjectUtil.isNotNull(query.getCreateEnd()), Message::getGmtCreate, query.getCreateEnd())
                .orderByDesc(Message::getGmtCreate);
        return wrapper;
    }

    /**
     * 获取消息记录详情
     *
     * @param messageId 主键ID
     * @return
     */
    public MessageDTO getMessage(Long messageId) {
        ValidatorUtil.notEmpty(messageId);
        return messageService.getMessage(messageId);
    }

    /**
     * 获取消息记录详情
     *
     * @param recordId 消息记录id
     * @return
     */
    public MessageDTO getMessage(String recordId) {
        ValidatorUtil.notEmpty(recordId);
        return messageService.getMessage(recordId);
    }

    /**
     * 删除消息日志
     *
     * @param messageIds
     */
    public void deleteMessage(List<Long> messageIds) {
        ValidatorUtil.notEmpty(messageIds);
        messageService.deleteMessage(messageIds);
    }

    /**
     * 更新消息记录
     *
     * @param messageDTO
     */
    public void updateMessage(MessageDTO messageDTO) {
        ValidatorUtil.notEmpty(messageDTO);
        messageService.updateMessage(messageDTO);
    }

    /**
     * 获取消息记录统计数据
     * 1、根据消息渠道统计饼图
     * 2、根据消息状态统计饼图
     *
     * @return
     */
    public Map<String, Object> statisticsMessage() {
        Map<String, Object> chartMap = new HashMap<>(16);
        List<PieChartDTO> pieOneList = messageService.statisticsMessageByStatus();
        chartMap.put("pieOne", pieOneList);
        List<PieChartDTO> pieTwoList = messageService.statisticsMessageByChannel();
        chartMap.put("pieTwo", pieTwoList);
        return chartMap;
    }

    /**
     * 保存消息渠道
     *
     * @param messageChannelDTO
     */
    public void addMessageChannel(MessageChannelDTO messageChannelDTO) {
        ValidatorUtil.notEmpty(messageChannelDTO);
        ValidatorUtil.allNotEmpty(messageChannelDTO.getChannel(), messageChannelDTO.getConfig());
        // 渠道标识唯一
        ValidatorUtil.isTrue(messageChannelService.checkMessageChannelRepeat(messageChannelDTO), ResultCodeEnum.MESSAGE_CHANNEL_REPEAT_ERROR);
        // 解密渠道配置信息
        String channelConfigStr = EncryptUtil.sm4Decrypt(EncryptProperties.sm4Key, messageChannelDTO.getConfig());
        if (ObjectUtil.equals(messageChannelDTO.getChannel(), SendEnum.DING_BOT.getChannel())) {
            // 钉钉机器人
            ChannelConfigDTO.DingBotChannelConfigDTO dingBotChannelConfigDTO = JsonUtil.fromJson(channelConfigStr, ChannelConfigDTO.DingBotChannelConfigDTO.class);
            ValidatorUtil.notEmpty(dingBotChannelConfigDTO, ResultCodeEnum.MESSAGE_CHANNEL_PARAMS_ERROR);
            ValidatorUtil.allNotBlank(ResultCodeEnum.MESSAGE_CHANNEL_PARAMS_ERROR, dingBotChannelConfigDTO.getSecret(), dingBotChannelConfigDTO.getWebhook());
            messageChannelDTO.setConfig(JsonUtil.toJson(OrikaUtil.convert(dingBotChannelConfigDTO, DingBotChannelConfig.class)));
        } else if (ObjectUtil.equals(messageChannelDTO.getChannel(), SendEnum.DING_WORK.getChannel())) {
            // 钉钉工作通知
            ChannelConfigDTO.DingWorkChannelConfigDTO dingWorkChannelConfigDTO = JsonUtil.fromJson(channelConfigStr, ChannelConfigDTO.DingWorkChannelConfigDTO.class);
            ValidatorUtil.notEmpty(dingWorkChannelConfigDTO, ResultCodeEnum.MESSAGE_CHANNEL_PARAMS_ERROR);
            ValidatorUtil.allNotBlank(ResultCodeEnum.MESSAGE_CHANNEL_PARAMS_ERROR, dingWorkChannelConfigDTO.getAppKey(),
                    dingWorkChannelConfigDTO.getAppSecret(), dingWorkChannelConfigDTO.getAgentId());
            messageChannelDTO.setConfig(JsonUtil.toJson(OrikaUtil.convert(dingWorkChannelConfigDTO, DingWorkChannelConfig.class)));
        } else if (ObjectUtil.equals(messageChannelDTO.getChannel(), SendEnum.EMAIL.getChannel())) {
            // 邮箱
            ChannelConfigDTO.EmailChannelConfigDTO emailChannelConfigDTO = JsonUtil.fromJson(channelConfigStr, ChannelConfigDTO.EmailChannelConfigDTO.class);
            ValidatorUtil.notEmpty(emailChannelConfigDTO, ResultCodeEnum.MESSAGE_CHANNEL_PARAMS_ERROR);
            ValidatorUtil.allNotEmpty(ResultCodeEnum.MESSAGE_CHANNEL_PARAMS_ERROR, emailChannelConfigDTO.getHost(), emailChannelConfigDTO.getPort(),
                    emailChannelConfigDTO.getUser(), emailChannelConfigDTO.getPass(), emailChannelConfigDTO.getFrom(), emailChannelConfigDTO.getSslEnable(),
                    emailChannelConfigDTO.getAuth(), emailChannelConfigDTO.getStarttlsEnable());
            messageChannelDTO.setConfig(JsonUtil.toJson(OrikaUtil.convert(emailChannelConfigDTO, EmailChannelConfig.class)));
        } else if (ObjectUtil.equals(messageChannelDTO.getChannel(), SendEnum.SMS.getChannel())) {
            // 短信
            ChannelConfigDTO.AliyunSmsConfigDTO aliyunSmsConfigDTO = JsonUtil.fromJson(channelConfigStr, ChannelConfigDTO.AliyunSmsConfigDTO.class);
            ValidatorUtil.notEmpty(aliyunSmsConfigDTO, ResultCodeEnum.MESSAGE_CHANNEL_PARAMS_ERROR);
            ValidatorUtil.allNotBlank(ResultCodeEnum.MESSAGE_CHANNEL_PARAMS_ERROR, aliyunSmsConfigDTO.getEndpoint(), aliyunSmsConfigDTO.getAccessKeyId(),
                    aliyunSmsConfigDTO.getAccessKeySecret(), aliyunSmsConfigDTO.getSignName());
            messageChannelDTO.setConfig(JsonUtil.toJson(OrikaUtil.convert(aliyunSmsConfigDTO, AliyunSmsConfig.class)));
        } else if (ObjectUtil.equals(messageChannelDTO.getChannel(), SendEnum.FLYING_BOT.getChannel())) {
            // 飞书机器人
            ChannelConfigDTO.FlyingBotChannelConfigDTO flyingBotChannelConfigDTO = JsonUtil.fromJson(channelConfigStr, ChannelConfigDTO.FlyingBotChannelConfigDTO.class);
            ValidatorUtil.notEmpty(flyingBotChannelConfigDTO, ResultCodeEnum.MESSAGE_CHANNEL_PARAMS_ERROR);
            ValidatorUtil.allNotBlank(ResultCodeEnum.MESSAGE_CHANNEL_PARAMS_ERROR, flyingBotChannelConfigDTO.getSecret(), flyingBotChannelConfigDTO.getWebhook());
            messageChannelDTO.setConfig(JsonUtil.toJson(OrikaUtil.convert(flyingBotChannelConfigDTO, FlyingBotChannelConfig.class)));
        }
        // TODO 其他消息渠道依次新增即可

        if (ObjectUtil.isNotNull(messageChannelDTO.getId())) {
            // 更新渠道信息
            messageChannelService.updateMessageChannel(messageChannelDTO);
            // 移除缓存
            redisService.delete(Constants.REDIS_PREFIX.MESSAGE_CHANNEL + StrUtil.COLON + messageChannelDTO.getChannel());
        } else {
            messageChannelService.addMessageChannel(messageChannelDTO);
        }
    }

    /**
     * 获取消息渠道列表
     *
     * @return
     */
    public List<MessageChannelDTO> getMessageChannels() {
        List<MessageChannelDTO> messageChannelDTOList = messageChannelService.getMessageChannels();
        // 渠道配置 config 字段加密传输
        if (CollectionUtil.isNotEmpty(messageChannelDTOList)) {
            messageChannelDTOList.forEach(channel -> channel.setConfig(EncryptUtil.sm4Encrypt(EncryptProperties.sm4Key, channel.getConfig())));
        }
        return messageChannelDTOList;
    }

    /**
     * 获取消息渠道配置(先缓存后DB)
     *
     * @param channel 渠道标识
     * @return
     */
    public MessageChannelDTO getMessageChannel(Integer channel) {
        ValidatorUtil.notEmpty(channel);
        String channelKey = Constants.REDIS_PREFIX.MESSAGE_CHANNEL + StrUtil.COLON + channel;
        String channelJson = redisService.get(channelKey);
        if (StrUtil.isNotBlank(channelJson)) {
            return JsonUtil.fromJson(channelJson, MessageChannelDTO.class);
        }
        MessageChannelDTO messageChannelDTO = messageChannelService.getMessageChannel(channel);
        ValidatorUtil.notEmpty(messageChannelDTO, ResultCodeEnum.MESSAGE_CHANNEL_NOT_EXIST_ERROR);
        redisService.set(channelKey, JsonUtil.toJson(messageChannelDTO));
        return messageChannelDTO;
    }

}
