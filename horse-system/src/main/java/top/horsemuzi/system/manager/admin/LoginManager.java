package top.horsemuzi.system.manager.admin;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.Header;
import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ConfigEnum;
import top.horsemuzi.common.enums.LoginTypeEnum;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.EncryptUtil;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.NetUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.properties.EncryptProperties;
import top.horsemuzi.system.core.redis.service.RedisService;
import top.horsemuzi.system.manager.common.CommonManager;
import top.horsemuzi.system.pojo.dto.login.AdminLoginDTO;
import top.horsemuzi.system.pojo.dto.login.ResetDTO;
import top.horsemuzi.system.pojo.dto.login.RouterDTO;
import top.horsemuzi.system.pojo.dto.user.UserDTO;
import top.horsemuzi.system.service.ConfigService;
import top.horsemuzi.system.service.PermissionService;
import top.horsemuzi.system.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 用户登录模块
 *
 * @author mabin
 * @date 2022/05/10 21:17
 **/
@Slf4j
@RequiredArgsConstructor
@Component
public class LoginManager {


    private final ConfigService configService;
    private final UserService userService;
    private final CommonManager commonManager;
    private final PermissionService permissionService;
    private final RedisService redisService;

    /**
     * 管理后台-用户登录
     *
     * @param adminLoginDTO
     * @param request
     * @return
     */
    public Map<String, String> login(AdminLoginDTO adminLoginDTO, HttpServletRequest request) {
        // 校验参数
        ValidatorUtil.notEmpty(adminLoginDTO);
        String username = adminLoginDTO.getUsername();
        String password = adminLoginDTO.getPassword();
        ValidatorUtil.allNotBlank(username, password);
        Integer loginType = adminLoginDTO.getType();
        if (ObjectUtil.isNull(loginType) || ObjectUtil.equals(loginType, LoginTypeEnum.PASSWORD.getType())) {
            // 查询验证码配置
            Map<String, String> configMap = configService.getConfig(ConfigEnum.KAPTCHA_SWITCH);
            String kaptchaSwitch = configMap.get(ConfigEnum.KAPTCHA_SWITCH.getKey());
            if (StrUtil.equals(Constants.COMMON_JUDGMENT.ON, kaptchaSwitch)) {
                String code = adminLoginDTO.getCode();
                String uuid = adminLoginDTO.getUuid();
                ValidatorUtil.allNotBlank(code, uuid);
                // 校验验证码有效性和正确性(无论验证是否成功, 都删除缓存)
                String codeKey = Constants.REDIS_PREFIX.KAPTCHA + uuid;
                String cacheCode = redisService.get(codeKey);
                ValidatorUtil.notBlank(cacheCode, ResultCodeEnum.KAPTCHA_EXPIRE);
                redisService.delete(codeKey);
                ValidatorUtil.isTrue(StrUtil.equals(cacheCode, code), ResultCodeEnum.KAPTCHA_ERROR);
            }
        }

        // 校验用户是否存在(用户名/手机号/邮箱)
        UserDTO userDTO = userService.getUserByLogin(username);
        ValidatorUtil.notEmpty(userDTO, ResultCodeEnum.WRONG_USER_OR_PASSWORD);
        // 校验账户状态
        ValidatorUtil.isTrue(Constants.COMMON_JUDGMENT.ZONE_STR.equals(userDTO.getStatus()), ResultCodeEnum.ACCOUNT_IS_DISABLED);

        // 校验密码错误次数是否超过设置的最大限制
        // 获取用户登录错误次数 (缓存键策略: 自定义key + username + ip[ip值是否存在均可以,因为用户名在系统中已经是唯一值])
        String key = Constants.REDIS_PREFIX.WRONG_PWD + StrUtil.COLON + userDTO.getUsername();
        String errorCount = redisService.get(key);
        // 获取系统配置: 密码错误最大错误次数、锁定时间 (min)
        Map<String, String> configValueMap = configService.getConfigValueMap(ConfigEnum.GROUP.WRONG_PASSWORD, ConfigEnum.WRONG_PASSWORD_COUNT.getKey(), ConfigEnum.WRONG_PASSWORD_LOCK_TIME.getKey());
        String configMaxErrorCount = configValueMap.get(ConfigEnum.WRONG_PASSWORD_COUNT.getKey());
        String configLockTime = configValueMap.get(ConfigEnum.WRONG_PASSWORD_LOCK_TIME.getKey());
        ValidatorUtil.allNotEmpty(configMaxErrorCount, configLockTime, ResultCodeEnum.CONFIG_NOT_EXIST_ERROR);
        // 如果在锁定时间内登录, 则直接拒绝
        ValidatorUtil.notTrue(StrUtil.equals(errorCount, configMaxErrorCount), ResultCodeEnum.LOGIN_ACCOUNT_LOCK_ERROR);

        // 校验密码 (前后端密码加密方式:RSA, 约定添加用户的时候,密码加密方式: pbkdf2加盐加密)
        boolean isWrongPwd;
        if (ObjectUtil.isNull(loginType) || ObjectUtil.equals(loginType, LoginTypeEnum.PASSWORD.getType())) {
            isWrongPwd = StrUtil.equals(EncryptUtil.pbkdf2(EncryptUtil.rsaDecrypt(EncryptProperties.rsaPrivateKey, password), userDTO.getSalt()), userDTO.getPassword());
        } else {
            // 手机号/邮箱号 对应业务验证码的缓存key
            String captchaCacheKey = Constants.REDIS_PREFIX.VERIFY_CODE + StrUtil.COLON + username;
            isWrongPwd = StrUtil.equals(redisService.get(captchaCacheKey), password);
        }
        // 本次是否登录成功(三种登录方式的一种)
        if (!isWrongPwd) {
            int tempErrorCount = StrUtil.isBlank(errorCount) ? Constants.COMMON_CODE.BASE_INT_ONE : Convert.toInt(errorCount) + Constants.COMMON_CODE.BASE_INT_ONE;
            // 达到最大错误次数限制
            if (ObjectUtil.equals(tempErrorCount, Convert.toInt(configMaxErrorCount))) {
                redisService.set(key, Integer.toString(tempErrorCount), Long.parseLong(configLockTime), TimeUnit.MINUTES);
                throw new BusinessException(ResultCodeEnum.LOGIN_ACCOUNT_LOCK_ERROR);
            } else {
                // 未达到最大错误限制, 直接累加错误次数
                redisService.set(key, Integer.toString(tempErrorCount));
                throw new BusinessException(ResultCodeEnum.WRONG_USER_OR_PASSWORD);
            }
        }
        // 登录成功, 清除锁定缓存信息
        if (redisService.hasKey(key)) {
            redisService.delete(key);
        }

        // 组装token数据
        // 获取当前登录终端类型, 支持多端登录场景 (PC端和移动端可同时在线的场景)
        UserAgent agent = UserAgentUtil.parse(request.getHeader(Header.USER_AGENT.getValue()));
        String terminal = agent.isMobile() ? Constants.TERMINAL.Android : Constants.TERMINAL.PC;
        String uuidToken = EncryptUtil.md5(userDTO.getId() + terminal);

        JwtUser jwtUser = new JwtUser()
                .setTerminal(terminal)
                .setId(userDTO.getId())
                .setUuid(uuidToken)
                .setUsername(userDTO.getUsername())
                .setAvatar(userDTO.getAvatar());
        Set<String> roles = new HashSet<>(StrUtil.split(userDTO.getRoleCodes(), StrUtil.COMMA));
        jwtUser.setAdmin(CollectionUtil.contains(roles, Constants.ROLE_CODE.ADMIN)).setRoles(roles);
        jwtUser.setPermissions(getCachePerms(jwtUser));
        String token = commonManager.createToken(jwtUser);
        // 更新用户登录信息
        NetUtil.setRequestInheritable();
        userService.updateLoginUser(userDTO, request);
        // 返回token
        Map<String, String> tokenMap = new HashMap<>(3);
        tokenMap.put(Constants.ACCESS_TOKEN, token);
        return tokenMap;
    }

    /**
     * 获取当前用户的权限标识信息
     *
     * @param jwtUser
     * @return
     */
    private Set<String> getCachePerms(JwtUser jwtUser) {
        Set<String> permsSet = new LinkedHashSet<>(16);
        if (ObjectUtil.isNotNull(jwtUser) && jwtUser.getAdmin()) {
            permsSet.add(Constants.ADMIN_PERMISSION);
            return permsSet;
        }
        // 缓存是否存在
        String key = Constants.REDIS_PREFIX.MENU_PERMS + StrUtil.COLON + jwtUser.getId();
        String perms = redisService.get(key);
        if (StrUtil.isNotBlank(perms)) {
            permsSet = JsonUtil.fromJson(perms, new TypeReference<Set<String>>() {
            });
            return permsSet;
        }
        // 查询DB并放入缓存
        Set<String> permissions = permissionService.getPerms(jwtUser.getId());
        if (CollectionUtil.isNotEmpty(permissions)) {
            permsSet = permissions.stream().filter(StrUtil::isNotBlank).collect(Collectors.toSet());
            redisService.set(key, JsonUtil.toJson(permsSet));
        }
        return permsSet;
    }

    /**
     * 管理后台-安全退出
     *
     * @param request
     */
    public void logout(HttpServletRequest request) {
        JwtUser jwtUser = commonManager.getCacheJwtUser(request);
        // 如果token缓存已失效, 默认已退出, 无须后续操作
        if (ObjectUtil.isNotEmpty(jwtUser)) {
            // 获取当前用户缓存token对应的uuid
            String uuid = jwtUser.getUuid();
            ValidatorUtil.notBlank(uuid, ResultCodeEnum.FAILED_TO_EXIT);
            // 清除token缓存
            redisService.delete(commonManager.getTokenKey(uuid));
            // 更新用户在线时长(异步更新)
            NetUtil.setRequestInheritable();
            userService.updateUserOnlineTime(jwtUser.getId());
        }
    }

    /**
     * 管理后台-获取用户角色权限信息
     *
     * @param jwtUser
     * @return java.util.Map<java.lang.String, java.lang.Object>
     * @author mabin
     * @date 2023/4/25 14:48
     **/
    public Map<String, Object> getInfo(JwtUser jwtUser) {
        Map<String, Object> userMap = new LinkedHashMap<>(16);
        ValidatorUtil.notEmpty(jwtUser, ResultCodeEnum.GET_USER_INFO_FAIL);
        userMap.put("user", jwtUser.setUuid(StrUtil.EMPTY));
        // 获取用户角色信息
        userMap.put("roles", jwtUser.getRoles());
        // 获取用户菜单权限信息
        userMap.put("permissions", jwtUser.getPermissions());
        return userMap;
    }

    /**
     * 管理后台-获取用户路由信息
     *
     * @param jwtUser
     * @return java.util.List<top.horsemuzi.system.pojo.dto.login.RouterDTO>
     * @author mabin
     * @date 2023/4/25 14:47
     **/
    @Transactional(rollbackFor = Exception.class)
    public List<RouterDTO> getRouters(JwtUser jwtUser) {
        ValidatorUtil.notEmpty(jwtUser, ResultCodeEnum.GET_USER_INFO_FAIL);
        // 获取当前用户的菜单权限路由(Tree接口,先缓存再DB)
        String menuKey = Constants.REDIS_PREFIX.MENU_ROUTER + StrUtil.COLON + jwtUser.getId();
        String routerJson = redisService.get(menuKey);
        if (StrUtil.isNotBlank(routerJson)) {
            return JsonUtil.fromJson(routerJson, new TypeReference<List<RouterDTO>>() {
            });
        }
        List<RouterDTO> routers = permissionService.getUserRouters(jwtUser);
        if (CollectionUtil.isNotEmpty(routers)) {
            redisService.set(menuKey, JsonUtil.toJson(routers), Constants.COMMON_CODE.LONG_TWO, TimeUnit.HOURS);
        }
        return routers;
    }

    /**
     * 用户忘记密码重置
     *
     * @param resetDTO
     */
    public void reset(ResetDTO resetDTO) {
        // 校验参数
        ValidatorUtil.notEmpty(resetDTO);
        String bindAccount = resetDTO.getBindAccount();
        String password = resetDTO.getPassword();
        String code = resetDTO.getCode();
        ValidatorUtil.allNotBlank(bindAccount, password, code);

        // 用户是否存在
        UserDTO userDTO = userService.getUserByLogin(bindAccount);
        ValidatorUtil.notEmpty(userDTO, ResultCodeEnum.USER_NOT_EXIST);

        // 验证码是否一致
        String key = Constants.REDIS_PREFIX.VERIFY_CODE + StrUtil.COLON + bindAccount;
        String cacheCode = redisService.get(key);
        ValidatorUtil.isTrue(StrUtil.equals(code, cacheCode), ResultCodeEnum.KAPTCHA_ERROR);

        // 新旧密码是否相同
        String newPassword = EncryptUtil.pbkdf2(EncryptUtil.rsaDecrypt(EncryptProperties.rsaPrivateKey, password), userDTO.getSalt());
        ValidatorUtil.notTrue(StrUtil.equals(newPassword, userDTO.getPassword()), ResultCodeEnum.LOGIN_NEW_AND_OLD_PASSWORD_SAME_ERROR);

        // 重置用户密码
        userDTO.setPassword(newPassword);
        // 用户是否开启强制密码更新策略
        userDTO.setLastUpdatePwdTime(new Date());

        if (userService.updateUser(userDTO)) {
            // 移除验证码缓存
            if (redisService.hasKey(key)) {
                redisService.delete(key);
            }
        }
    }

}
