package top.horsemuzi.system.manager.admin;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Validator;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.core.redis.service.RedisService;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.permission.PermissionDTO;
import top.horsemuzi.system.pojo.dto.permission.PermissionQuery;
import top.horsemuzi.system.pojo.dto.role.RoleDTO;
import top.horsemuzi.system.pojo.entity.RolePermission;
import top.horsemuzi.system.service.PermissionService;
import top.horsemuzi.system.service.RolePermissionService;
import top.horsemuzi.system.service.RoleService;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 权限菜单模块
 *
 * @author mabin
 * @date 2022/07/07 17:34
 **/

@Slf4j
@RequiredArgsConstructor
@Component
public class PermissionManager {

    private final PermissionService permissionService;
    private final RolePermissionService rolePermissionService;
    private final RedisService redisService;
    private final RoleService roleService;


    /**
     * 获取权限菜单列表
     *
     * @param query
     * @return
     */
    public List<PermissionDTO> getPermissionMenus(PermissionQuery query) {
        return permissionService.getPermissionMenus(query);
    }

    /**
     * 根据权限菜单id获取权限菜单详情
     *
     * @param id
     * @return
     */
    public PermissionDTO getPermission(Long id) {
        return permissionService.getPermission(id);
    }

    /**
     * 新增权限菜单
     *
     * @param permissionDTO
     */
    @Transactional(rollbackFor = Exception.class)
    public void addPermission(PermissionDTO permissionDTO) {
        ValidatorUtil.notEmpty(permissionDTO);
        // 菜单名称是否重复
        ValidatorUtil.isTrue(permissionService.checkPermissionNameUnique(permissionDTO),
                ResultCodeEnum.PERMISSION_MENU_NAME_EXIST);
        // 菜单外链是否合法
        if (StrUtil.equals(permissionDTO.getFrame(), Constants.COMMON_JUDGMENT.ZONE_STR)) {
            ValidatorUtil.isTrue(Validator.isUrl(permissionDTO.getRoutePath()),
                    ResultCodeEnum.PERMISSION_MENU_FRAME_PATH_ERROR);
        }
        // 父级菜单是否存在
        ValidatorUtil.isTrue(checkParentPermissionExist(permissionDTO), ResultCodeEnum.PERMISSION_MENU_PARENT_NOT_EXIST);
        // 保存权限信息
        permissionService.addPermission(permissionDTO);
        // 管理员角色自动赋值菜单权限关联信息
        RoleDTO query = new RoleDTO();
        query.setRoleCode(Constants.ROLE_CODE.ADMIN);
        RoleDTO roleDTO = roleService.getRole(query);
        ValidatorUtil.notEmpty(roleDTO, ResultCodeEnum.ROLE_NOT_EXIST);
        roleDTO.setPermissionIds(Collections.singletonList(permissionDTO.getId()));
        rolePermissionService.addRolePermission(roleDTO);
        // 移除用户权限标识缓存、用户菜单路由缓存
        deleteCachePermissions();
    }

    /**
     * 父级菜单是否存在
     *
     * @param permissionDTO
     * @return
     */
    private boolean checkParentPermissionExist(PermissionDTO permissionDTO) {
        if (ObjectUtil.isNull(permissionDTO)) {
            return false;
        }
        // 顶级菜单默认通过
        if (permissionDTO.getParentId() != 0) {
            PermissionDTO parentPermissionDTO = permissionService.getPermission(permissionDTO.getParentId());
            return ObjectUtil.isNotNull(parentPermissionDTO);
        }
        return true;
    }

    /**
     * 根据当前登录用户信息获取权限菜单树
     *
     * @param roleId
     * @return
     */
    public Map<String, Object> getPermissionTreeMenus(Long roleId) {
        Map<String, Object> permissionMap = new HashMap<>(16);
        List<PermissionDTO> permissionDTOList = getPermissionMenus(new PermissionQuery());
        if (CollectionUtil.isEmpty(permissionDTOList)) {
            return permissionMap;
        }
        // 构建权限菜单树结构
        TreeNodeConfig config = new TreeNodeConfig();
        config.setNameKey("label");
        List<Tree<Long>> trees = TreeUtil.build(permissionDTOList, 0L, config, (permission, tree) -> {
            tree.setId(permission.getId())
                    .setParentId(permission.getParentId())
                    .setName(permission.getName())
                    .setWeight(permission.getSort());
            tree.putExtra("status", permission.getStatus());
            tree.putExtra("icon", permission.getIcon());
        });
        permissionMap.put("menus", trees);
        if (ObjectUtil.isNotEmpty(roleId)) {
            List<PermissionDTO> roleMenuList = permissionService.getPermissionMenusByRoleId(roleId);
            if (CollectionUtil.isNotEmpty(roleMenuList)) {
                List<Long> permissionIds = roleMenuList.stream().map(PermissionDTO::getId).collect(Collectors.toList());
                permissionMap.put("checkedMenuIds", permissionIds);
            }
        }
        return permissionMap;
    }

    /**
     * 更新权限菜单
     *
     * @param permissionDTO
     */
    public void updatePermission(PermissionDTO permissionDTO) {
        ValidatorUtil.notEmpty(permissionDTO);
        // 权限菜单是否存在
        ValidatorUtil.notEmpty(permissionService.getPermission(permissionDTO.getId()), ResultCodeEnum.PERMISSION_NOT_EXIST);
        // 菜单名称是否重复
        ValidatorUtil.isTrue(permissionService.checkPermissionNameUnique(permissionDTO), ResultCodeEnum.PERMISSION_MENU_NAME_EXIST);
        // 菜单外链是否合法
        if (StrUtil.equals(permissionDTO.getFrame(), Constants.COMMON_JUDGMENT.ZONE_STR)) {
            ValidatorUtil.isTrue(Validator.isUrl(permissionDTO.getRoutePath()), ResultCodeEnum.PERMISSION_MENU_FRAME_PATH_ERROR);
        }
        // 当前父级是否是自己
        ValidatorUtil.isTrue(!ObjectUtil.equal(permissionDTO.getParentId(), permissionDTO.getId()),
                ResultCodeEnum.PERMISSION_PARENT_NOT_SELF);
        // 父级菜单是否存在
        ValidatorUtil.isTrue(checkParentPermissionExist(permissionDTO), ResultCodeEnum.PERMISSION_MENU_PARENT_NOT_EXIST);
        if (permissionService.updatePermission(permissionDTO)) {
            // 移除用户权限标识缓存、用户菜单路由缓存
            deleteCachePermissions();
        }
    }

    /**
     * 删除权限菜单
     *
     * @param id
     */
    public void deletePermission(Long id) {
        ValidatorUtil.notEmpty(id);
        // 是否存在
        PermissionDTO permissionDTO = permissionService.getPermission(id);
        ValidatorUtil.notEmpty(permissionDTO, ResultCodeEnum.PERMISSION_NOT_EXIST);
        // 菜单是否存在已分配子菜单
        ValidatorUtil.isEmpty(permissionService.getPermissionByParentId(permissionDTO.getId()),
                ResultCodeEnum.PERMISSION_EXIST_RELATE_ERROR);
        // 菜单是否已有角色关联
        RolePermission rolePermission = new RolePermission();
        rolePermission.setPermissionId(permissionDTO.getId());
        ValidatorUtil.isEmpty(rolePermissionService.getRolePermissions(rolePermission),
                ResultCodeEnum.PERMISSION_ROLE_DEPLOY_ERROR);
        if (permissionService.deletePermission(id)) {
            // 移除用户权限标识缓存、用户菜单路由缓存
            deleteCachePermissions();
        }
    }

    /**
     * 移除菜单权限标识、菜单路由等缓存信息
     */
    private void deleteCachePermissions() {
        redisService.deletePatternByPrefix(Constants.REDIS_PREFIX.MENU_PERMS, Constants.REDIS_PREFIX.MENU_ROUTER);
    }

    /**
     * 菜单名称实时检索
     *
     * @param search
     * @return
     */
    public List<InputRemoteFetchDTO> fetch(String search) {
        return permissionService.fetch(search);
    }
}
