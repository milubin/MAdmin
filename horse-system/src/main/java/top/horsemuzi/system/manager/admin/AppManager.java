package top.horsemuzi.system.manager.admin;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ConfigEnum;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.CommonCodeUtil;
import top.horsemuzi.common.util.EncryptUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.core.message.common.model.EmailContentModel;
import top.horsemuzi.system.config.properties.EncryptProperties;
import top.horsemuzi.system.core.redis.service.RedisService;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.open.AppCategoryRelationDTO;
import top.horsemuzi.system.pojo.dto.open.api.ApiDTO;
import top.horsemuzi.system.pojo.dto.open.app.AppDTO;
import top.horsemuzi.system.pojo.dto.open.app.AppQuery;
import top.horsemuzi.system.pojo.dto.open.category.ApiCategoryDTO;
import top.horsemuzi.system.pojo.dto.open.category.ApiCategoryQuery;
import top.horsemuzi.system.service.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * 开放API应用模块
 *
 * @author 马滨
 * @date 2022/11/18 22:13
 **/

@Slf4j
@Component
@RequiredArgsConstructor
public class AppManager {

    private final AppService appService;
    private final AppCategoryRelationService appCategoryRelationService;
    private final ApiCategoryService apiCategoryService;
    private final ApiService apiService;
    private final RedisService redisService;
    private final MessageService messageService;
    private final ConfigService configService;


    /**
     * 分页获取开放API应用列表
     *
     * @param query
     * @return
     */
    public Page<AppDTO> getPageApps(AppQuery query) {
        return appService.getPageApps(query);
    }

    /**
     * 获取开放API应用详情
     *
     * @param id
     * @return
     */
    public AppDTO getApp(Long id) {
        ValidatorUtil.notEmpty(id);
        return appService.getApp(id);
    }

    /**
     * 新增开放API应用
     *
     * @param appDTO
     */
    @Transactional(rollbackFor = Exception.class)
    public void addApp(AppDTO appDTO) {
        ValidatorUtil.notEmpty(appDTO);
        // 应用名称是否重复
        ValidatorUtil.isTrue(appService.checkAppRepeat(appDTO), ResultCodeEnum.OPEN_APP_NAME_REPEAT_ERROR);
        // 生成 appKey, appSecret
        appDTO.setAppKey(IdUtil.nanoId(10));
        appDTO.setAppSecret(EncryptUtil.generate3DesKey());
        // 生成默认应用图标
        if (StrUtil.isBlank(appDTO.getAppIcon())) {
            appDTO.setAppIcon(CommonCodeUtil.generateImg(appDTO.getAppName()));
        }
        // 管理端添加应用，默认审批通过
        appDTO.setApprovalStatus(Constants.COMMON_CODE.STR_ONE);
        // 保存应用和接口类型关联信息
        if (CollectionUtil.isNotEmpty(appDTO.getApiCategoryIdList())) {
            appDTO.setApiCategoryIds(CollectionUtil.join(appDTO.getApiCategoryIdList(), StrUtil.COMMA));
        }
        if (appService.addApp(appDTO) && CollectionUtil.isNotEmpty(appDTO.getApiCategoryIdList())) {
            saveAppCategoryRelation(appDTO);
        }
    }


    /**
     * 更新开放API应用
     *
     * @param appDTO
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateApp(AppDTO appDTO) {
        ValidatorUtil.notEmpty(appDTO);
        ValidatorUtil.notEmpty(appDTO.getId());
        // 应用是否存在
        ValidatorUtil.notEmpty(appService.getApp(appDTO.getId()), ResultCodeEnum.OPEN_APP_NOT_EXIST_ERROR);
        // 应用名称是否重复
        ValidatorUtil.isTrue(appService.checkAppRepeat(appDTO), ResultCodeEnum.OPEN_APP_NAME_REPEAT_ERROR);
        // 更新应用和接口类型关联信息
        if (CollectionUtil.isNotEmpty(appDTO.getApiCategoryIdList())) {
            appDTO.setApiCategoryIds(CollectionUtil.join(appDTO.getApiCategoryIdList(), StrUtil.COMMA));
        }
        if (appService.updateApp(appDTO) && CollectionUtil.isNotEmpty(appDTO.getApiCategoryIdList())) {
            if (appCategoryRelationService.deleteAppCategoryRelation(appDTO.getId())) {
                saveAppCategoryRelation(appDTO);
            }
        }
        // 移除appKey对应缓存信息, 数据缓存一致性
        String cacheKey = Constants.REDIS_PREFIX.OPEN_APP + StrUtil.COLON + appDTO.getAppKey();
        if (redisService.hasKey(cacheKey)) {
            redisService.delete(cacheKey);
        }
    }

    /**
     * 保存应用类型关联信息
     *
     * @param appDTO
     */
    private void saveAppCategoryRelation(AppDTO appDTO) {
        List<AppCategoryRelationDTO> appCategoryRelationDTOList = new ArrayList<>();
        AppCategoryRelationDTO appCategoryRelationDTO = null;
        for (Long apiCategoryId : appDTO.getApiCategoryIdList()) {
            appCategoryRelationDTO = new AppCategoryRelationDTO();
            appCategoryRelationDTO.setAppId(appDTO.getId());
            appCategoryRelationDTO.setApiCategoryId(apiCategoryId);
            appCategoryRelationDTOList.add(appCategoryRelationDTO);
        }
        appCategoryRelationService.addAppCategoryRelation(appCategoryRelationDTOList);
    }

    /**
     * 删除开放API应用
     *
     * @param appIds
     */
    @Transactional(rollbackFor = Exception.class)
    public void deleteApp(List<Long> appIds) {
        ValidatorUtil.notEmpty(appIds);
        // API应用是否都存在
        List<AppDTO> appDTOList = appService.getApp(appIds);
        ValidatorUtil.isTrue(CollectionUtil.isNotEmpty(appDTOList) && appDTOList.size() == appIds.size(), ResultCodeEnum.OPEN_APP_NOT_EXIST_ERROR);
        if (appService.deleteApp(appIds)) {
            appCategoryRelationService.deleteAppCategoryRelation(appIds);
            // 移除缓存
            for (AppDTO appDTO : appDTOList) {
                String cacheKey = Constants.REDIS_PREFIX.OPEN_APP + StrUtil.COLON + appDTO.getAppKey();
                if (redisService.hasKey(cacheKey)) {
                    redisService.delete(cacheKey);
                }
            }
        }
    }

    /**
     * 更新API应用审批状态
     *
     * @param appDTO
     */
    public void updateAppStatus(AppDTO appDTO) {
        ValidatorUtil.notEmpty(appDTO);
        ValidatorUtil.allNotEmpty(appDTO.getId(), appDTO.getApprovalStatus());
        // 应用是否存在
        AppDTO currentAppDTO = appService.getApp(appDTO.getId());
        ValidatorUtil.notEmpty(currentAppDTO, ResultCodeEnum.OPEN_APP_NOT_EXIST_ERROR);
        // 应用是否已审批完成
        ValidatorUtil.isTrue(StrUtil.equals(Constants.COMMON_CODE.STR_ZONE, currentAppDTO.getApprovalStatus()), ResultCodeEnum.OPEN_APP_APPROVED_COMPLETE_ERROR);
        currentAppDTO.setApprovalStatus(appDTO.getApprovalStatus());
        if (StrUtil.equals(Constants.COMMON_CODE.STR_TWO, appDTO.getApprovalStatus())) {
            currentAppDTO.setRejectReason(appDTO.getRejectReason());
        }
        if (appService.updateApp(currentAppDTO)) {
            // 移除appKey对应缓存信息, 保证数据缓存一致性
            String cacheKey = Constants.REDIS_PREFIX.OPEN_APP + StrUtil.COLON + currentAppDTO.getAppKey();
            if (redisService.hasKey(cacheKey)) {
                redisService.delete(cacheKey);
            }
        }
    }

    /**
     * 获取开放API接口分类列表
     *
     * @param appId
     * @return
     */
    public List<ApiCategoryDTO> listApiCategory(Long appId) {
        ValidatorUtil.notEmpty(appId);
        return apiCategoryService.listApiCategory(appId);
    }

    /**
     * 应用名称实时检索
     *
     * @param search
     * @return
     */
    public List<InputRemoteFetchDTO> fetch(String search) {
        return appService.fetch(search);
    }

    /**
     * 分页获取开放API接口类型列表
     *
     * @param query
     * @return
     */
    public Page<ApiCategoryDTO> getPageApiCategory(ApiCategoryQuery query) {
        return apiCategoryService.getPageApiCategory(query);
    }

    /**
     * 获取开放API类型详情
     *
     * @param id
     * @return
     */
    public ApiCategoryDTO getApiCategory(Long id) {
        ValidatorUtil.notEmpty(id);
        return apiCategoryService.getApiCategory(id);
    }

    /**
     * 新增开放API类型
     *
     * @param apiCategoryDTO
     */
    public void addApiCategory(ApiCategoryDTO apiCategoryDTO) {
        ValidatorUtil.notEmpty(apiCategoryDTO);
        if (StrUtil.isBlank(apiCategoryDTO.getCategoryIcon())) {
            apiCategoryDTO.setCategoryIcon(CommonCodeUtil.generateImg(apiCategoryDTO.getCategoryName()));
        }
        apiCategoryService.addApiCategory(apiCategoryDTO);
    }

    /**
     * 更新开放API类型
     *
     * @param apiCategoryDTO
     */
    public void updateApiCategory(ApiCategoryDTO apiCategoryDTO) {
        ValidatorUtil.notEmpty(apiCategoryDTO);
        ValidatorUtil.notEmpty(apiCategoryDTO.getId());
        apiCategoryService.updateApiCategory(apiCategoryDTO);
    }

    /**
     * 删除开放API类型
     *
     * @param apiCategoryIds
     */
    public void deleteApiCategory(List<Long> apiCategoryIds) {
        ValidatorUtil.notEmpty(apiCategoryIds);
        // 类型资源是否存在
        List<ApiCategoryDTO> apiCategoryDTOList = apiCategoryService.getApiCategory(apiCategoryIds);
        ValidatorUtil.isTrue(CollectionUtil.isNotEmpty(apiCategoryDTOList) && apiCategoryDTOList.size() == apiCategoryIds.size(), ResultCodeEnum.OPEN_API_CATEGORY_NOT_EXIST_ERROR);
        // 是否存在接口关联
        List<ApiDTO> apiDTOList = apiService.getApiByCategory(apiCategoryIds);
        ValidatorUtil.isEmpty(apiDTOList, ResultCodeEnum.OPEN_API_CATEGORY_EXIST_RELATION_ERROR);
        // 是否存在应用关联
        List<AppCategoryRelationDTO> appCategoryRelationDTOList = appCategoryRelationService.getAppCategoryRelation(apiCategoryIds);
        ValidatorUtil.isEmpty(appCategoryRelationDTOList, ResultCodeEnum.OPEN_API_CATEGORY_EXIST_RELATION_ERROR);
        // 移除类型
        apiCategoryService.deleteApiCategory(apiCategoryIds);
    }

    /**
     * 接口类型名称实时检索
     *
     * @param search
     * @return
     */
    public List<InputRemoteFetchDTO> fetchApiCategory(String search) {
        return apiCategoryService.fetch(search);
    }

    /**
     * API应用数据导出
     *
     * @param query
     * @param response
     */
    public void export(AppQuery query, HttpServletResponse response) {
        appService.export(query, response);
    }

    /**
     * 获取开放API接口类型列表
     *
     * @return
     */
    public List<ApiCategoryDTO> getApiCategoryList() {
        return apiCategoryService.getApiCategoryList();
    }

    /**
     * 获取API应用密钥AppSecret
     *
     * @param id
     * @return
     */
    public String getAppSecret(Long id) {
        ValidatorUtil.notEmpty(id);
        AppDTO appDTO = getApp(id);
        ValidatorUtil.notEmpty(appDTO, ResultCodeEnum.OPEN_APP_NOT_EXIST_ERROR);
        return EncryptUtil.rsaEncrypt(EncryptProperties.rsaPublicKey, appDTO.getAppSecret());
    }

    /**
     * API应用密钥AppSecret一键发送至邮箱
     *
     * @param appDTO
     * @return void
     * @author mabin
     * @date 2023/3/21 9:23
     **/
    public void sendSecret(AppDTO appDTO) {
        // 应用是否存在
        AppDTO currentAppDTO = getApp(appDTO.getId());
        ValidatorUtil.notEmpty(currentAppDTO, ResultCodeEnum.OPEN_APP_NOT_EXIST_ERROR);
        // 应用密钥发送至指定邮箱
        EmailContentModel model = new EmailContentModel();
        model.setTitle("MAdmin开放平台");
        model.setTos(appDTO.getEmail());
        // 每次发送抄送一份给当前系统管理员, 支持多管理员邮箱账号
        String adminEmail = configService.getConfigValue(ConfigEnum.ADMIN_EMAIL_ACCOUNT);
        model.setCcs(adminEmail);
        model.setHtml(Boolean.FALSE);
        model.setContent(StrUtil.format("您申请的MAdmin系统平台开放应用: {} 的appKey: {}, secretKey: {}, 请妥善保管!",
                currentAppDTO.getAppName(), currentAppDTO.getAppKey(), currentAppDTO.getAppSecret()));
        messageService.sendEmailMessage(model);
    }

}
