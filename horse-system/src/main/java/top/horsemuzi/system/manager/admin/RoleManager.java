package top.horsemuzi.system.manager.admin;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.permission.PermissionDTO;
import top.horsemuzi.system.pojo.dto.role.RoleDTO;
import top.horsemuzi.system.pojo.dto.role.RoleQuery;
import top.horsemuzi.system.pojo.dto.user.UserDTO;
import top.horsemuzi.system.pojo.entity.UserRole;
import top.horsemuzi.system.service.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 角色管理模块
 *
 * @author mabin
 * @date 2022/06/23 08:59
 **/
@Slf4j
@RequiredArgsConstructor
@Component
public class RoleManager {

    private final RoleService roleService;
    private final RolePermissionService rolePermissionService;
    private final UserRoleService userRoleService;
    private final UserService userService;
    private final PermissionService permissionService;


    /**
     * 新增角色信息
     *
     * @param roleDTO
     */
    public void addRole(RoleDTO roleDTO) {
        ValidatorUtil.notEmpty(roleDTO);
        // 角色权限标识是否重复(与停用状态的角色也不能重复)
        ValidatorUtil.isTrue(roleService.checkRoleUnique(roleDTO), ResultCodeEnum.ROLE_EXIST_ERROR);
        // 新增角色信息
        roleService.addRole(roleDTO);
        // 新增角色权限关联信息
        if (CollectionUtil.isNotEmpty(roleDTO.getPermissionIds())) {
            rolePermissionService.addRolePermission(roleDTO);
        }
    }

    /**
     * 更新角色信息
     *
     * @param roleDTO
     */
    public void updateRole(RoleDTO roleDTO) {
        ValidatorUtil.notEmpty(roleDTO);
        // 角色权限标识是否重复(与停用状态的角色也不能重复)
        ValidatorUtil.isTrue(roleService.checkRoleUnique(roleDTO), ResultCodeEnum.ROLE_EXIST_ERROR);
        // 更新角色信息
        roleService.updateRole(roleDTO);
        // 角色权限关联信息: 先删除再新增
        rolePermissionService.deleteRolePermission(Collections.singletonList(roleDTO.getId()));
        if (CollectionUtil.isNotEmpty(roleDTO.getPermissionIds())) {
            rolePermissionService.addRolePermission(roleDTO);
        }
    }

    /**
     * 根据id获取角色详情
     *
     * @param id
     * @return
     */
    public RoleDTO getRole(Long id) {
        ValidatorUtil.notEmpty(id);
        RoleDTO roleDTO = roleService.getRole(id);
        ValidatorUtil.notEmpty(roleDTO, ResultCodeEnum.RESOURCE_NOT_EXIST);
        return roleDTO;
    }


    /**
     * 批量删除角色信息
     *
     * @param roleIds
     */
    public void deleteRoles(List<Long> roleIds) {
        ValidatorUtil.notEmpty(roleIds);
        // 是否存在已分配给用户的角色
        ValidatorUtil.isEmpty(userRoleService.getUserIdsByRoleIds(roleIds), ResultCodeEnum.ROLE_ASSIGNED_USER);
        // 删除角色
        roleService.deleteRoles(roleIds);
        // 删除关联信息
        rolePermissionService.deleteRolePermission(roleIds);
    }

    /**
     * 分页获取角色列表
     *
     * @param roleQuery
     * @return
     */
    public Page<RoleDTO> getPageRoles(RoleQuery roleQuery) {
        ValidatorUtil.notEmpty(roleQuery);
        return roleService.getPageRoles(roleQuery);
    }

    /**
     * 分配角色给用户
     *
     * @param roleDTO
     */
    public void deployRoleToUser(RoleDTO roleDTO) {
        ValidatorUtil.notEmpty(roleDTO);
        // 校验角色是否存在
        Long roleId = roleDTO.getId();
        RoleDTO currentRoleDTO = roleService.getRole(roleId);
        ValidatorUtil.notEmpty(currentRoleDTO, ResultCodeEnum.ROLE_NOT_EXIST);
        // 获取用户角色关联信息
        List<UserRole> userRoles = userRoleService.getUserIdsByRoleIds(Collections.singletonList(roleId));
        List<Long> currentUserIds = CollectionUtil.isEmpty(userRoles) ? Collections.emptyList() : userRoles.stream().map(UserRole::getUserId).collect(Collectors.toList());
        // 删除用户角色关联信息
        userRoleService.deleteUserRoleByRoleIds(Collections.singletonList(roleId));
        // 更新用户角色标识字段信息
        List<Long> checkUserIds = CollectionUtil.isEmpty(roleDTO.getUserIds()) ? Collections.emptyList() : roleDTO.getUserIds();
        List<UserDTO> userDTOList = userService.getUsers(new ArrayList<>(CollectionUtil.unionDistinct(checkUserIds, currentUserIds)));
        ValidatorUtil.notEmpty(userDTOList, ResultCodeEnum.USER_NOT_EXIST);
        for (UserDTO userDTO : userDTOList) {
            Long userId = userDTO.getId();
            if (!(checkUserIds.contains(userId) && currentUserIds.contains(userId))) {
                List<UserDTO> waitUpdateUserDTOList = new ArrayList<>();
                List<String> codes = new ArrayList<>(StrUtil.split(userDTO.getRoleCodes(), StrUtil.COMMA));
                if (checkUserIds.contains(userId) && !currentUserIds.contains(userId)) {
                    codes.add(currentRoleDTO.getRoleCode());
                } else if (!checkUserIds.contains(userId) && currentUserIds.contains(userId)) {
                    codes.remove(currentRoleDTO.getRoleCode());
                }
                userDTO.setRoleCodes(CollectionUtil.join(codes, StrUtil.COMMA));
                waitUpdateUserDTOList.add(userDTO);
                userService.updateBatchUser(waitUpdateUserDTOList);
            }
        }
        // 保存用户角色关联信息
        List<UserRole> insertUserRoles = new ArrayList<>();
        for (Long checkUserId : checkUserIds) {
            insertUserRoles.add(new UserRole().setRoleId(roleId).setUserId(checkUserId));
        }
        userRoleService.addBatchUserRole(insertUserRoles);
    }

    /**
     * 获取角色信息列表
     *
     * @return
     */
    public List<RoleDTO> getRoles() {
        return roleService.getRoles();
    }

    /**
     * 为角色分配权限
     *
     * @param roleDTO
     */
    public void deployPermissionToRole(RoleDTO roleDTO) {
        ValidatorUtil.notEmpty(roleDTO);
        ValidatorUtil.notEmpty(roleDTO.getId());
        // 角色是否存在
        Long roleId = roleDTO.getId();
        RoleDTO currentRoleDTO = roleService.getRole(roleId);
        ValidatorUtil.notEmpty(currentRoleDTO, ResultCodeEnum.ROLE_NOT_EXIST);
        // 删除角色权限关联
        rolePermissionService.deleteRolePermission(Collections.singletonList(roleId));
        List<Long> permissionIds = roleDTO.getPermissionIds();
        if (CollectionUtil.isNotEmpty(permissionIds)) {
            // 权限菜单是否均存在
            List<PermissionDTO> permissionDTOList = permissionService.getPermissions(permissionIds);
            ValidatorUtil.isTrue(CollectionUtil.isNotEmpty(permissionDTOList) && permissionDTOList.size() == permissionIds.size(), ResultCodeEnum.PERMISSION_NOT_EXIST);
            // 新增角色权限关联
            rolePermissionService.addRolePermission(roleDTO);
        }
    }

    /**
     * 取消用户的角色授权(兼容批量)
     *
     * @param roleDTO
     */
    public void cancelRoleUserAuth(RoleDTO roleDTO) {
        ValidatorUtil.notEmpty(roleDTO);
        ValidatorUtil.allNotEmpty(roleDTO.getId(), roleDTO.getUserIds());
        // 角色是否存在
        RoleDTO currentRoleDTO = roleService.getRole(roleDTO.getId());
        ValidatorUtil.notEmpty(currentRoleDTO, ResultCodeEnum.ROLE_NOT_EXIST);
        // 用户是否拥有此角色
        List<UserDTO> userDTOList = userService.getUsers(roleDTO.getUserIds());
        ValidatorUtil.notEmpty(userDTOList, ResultCodeEnum.USER_NOT_EXIST);
        // 组装待更新数据
        List<Long> waitUpdateUserIds = new ArrayList<>();
        List<UserDTO> waitUpdateUsers = new ArrayList<>();
        for (UserDTO userDTO : userDTOList) {
            if (StrUtil.isNotBlank(userDTO.getRoleCodes())) {
                List<String> codes = new ArrayList<>(StrUtil.split(userDTO.getRoleCodes(), StrUtil.COMMA));
                if (codes.contains(currentRoleDTO.getRoleCode())) {
                    // 移除角色标识
                    codes.remove(currentRoleDTO.getRoleCode());
                    userDTO.setRoleCodes(CollectionUtil.join(codes, StrUtil.COMMA));
                    waitUpdateUsers.add(userDTO);
                    waitUpdateUserIds.add(userDTO.getId());
                }
            }
        }
        // 更新用户角色标识
        if (CollectionUtil.isNotEmpty(waitUpdateUsers)) {
            userService.updateBatchUser(waitUpdateUsers);
        }
        // 取消用户角色关联
        if (CollectionUtil.isNotEmpty(waitUpdateUserIds)) {
            userRoleService.deleteUserRoleByUserIds(waitUpdateUserIds);
        }
    }

    /**
     * 更新角色状态
     *
     * @param roleDTO
     */
    public void updateRoleStatus(RoleDTO roleDTO) {
        ValidatorUtil.notEmpty(roleDTO);
        RoleDTO currentRoleDTO = roleService.getRole(roleDTO.getId());
        ValidatorUtil.notEmpty(currentRoleDTO, ResultCodeEnum.RESOURCE_NOT_EXIST);
        currentRoleDTO.setStatus(roleDTO.getStatus());
        roleService.updateRole(currentRoleDTO);
    }

    /**
     * 角色名称实时检索
     *
     * @param search
     * @return
     */
    public List<InputRemoteFetchDTO> fetch(String search) {
        return roleService.fetch(search);
    }

    /**
     * 导出角色和菜单权限信息
     *
     * @param query
     * @param response
     */
    public void export(RoleQuery query, HttpServletResponse response) {
        roleService.export(query, response);
    }
}
