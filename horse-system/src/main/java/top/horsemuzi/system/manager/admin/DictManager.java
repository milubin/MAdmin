package top.horsemuzi.system.manager.admin;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.core.redis.service.RedisService;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.dict.DictDataDTO;
import top.horsemuzi.system.pojo.dto.dict.DictDataQuery;
import top.horsemuzi.system.pojo.dto.dict.DictTypeDTO;
import top.horsemuzi.system.pojo.dto.dict.DictTypeQuery;
import top.horsemuzi.system.service.DictDataService;
import top.horsemuzi.system.service.DictTypeService;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 数据字典模块
 *
 * @author mabin
 * @date 2022/06/20 11:14
 **/
@Slf4j
@RequiredArgsConstructor
@Component
public class DictManager {

    private final DictTypeService dictTypeService;
    private final DictDataService dictDataService;
    private final RedisService redisService;

    /**
     * 新增字典类型
     *
     * @param dictTypeDTO
     * @return
     */
    public void addDictType(DictTypeDTO dictTypeDTO) {
        ValidatorUtil.notEmpty(dictTypeDTO);
        ValidatorUtil.notBlank(dictTypeDTO.getType());

        DictTypeDTO queryDictTypeDTO = new DictTypeDTO();
        queryDictTypeDTO.setType(dictTypeDTO.getType());
        DictTypeDTO existDictTypeDTO = dictTypeService.checkDictTypeRepeat(queryDictTypeDTO);
        ValidatorUtil.isEmpty(existDictTypeDTO, ResultCodeEnum.DICT_TYPE_REPEAT_ERROR);

        dictTypeService.addDictType(dictTypeDTO);
    }

    /**
     * 更新字典类型并删除缓存
     *
     * @param dictTypeDTO
     * @return
     */
    public void updateDictType(DictTypeDTO dictTypeDTO) {
        ValidatorUtil.notEmpty(dictTypeDTO);
        ValidatorUtil.notEmpty(dictTypeDTO.getId());
        // 是否重复
        DictTypeDTO queryDictTypeDTO = new DictTypeDTO();
        queryDictTypeDTO.setId(dictTypeDTO.getId());
        queryDictTypeDTO.setType(dictTypeDTO.getType());
        DictTypeDTO existDictTypeDTO = dictTypeService.checkDictTypeRepeat(queryDictTypeDTO);
        ValidatorUtil.isEmpty(existDictTypeDTO, ResultCodeEnum.DICT_TYPE_REPEAT_ERROR);
        dictTypeService.updateDictType(dictTypeDTO);
    }

    /**
     * 获取字典类型
     *
     * @param type 字典类型
     * @return
     */
    public DictTypeDTO getDictType(String type) {
        ValidatorUtil.notBlank(type);
        // 查询DB
        DictTypeDTO queryDictTypeDTO = new DictTypeDTO();
        queryDictTypeDTO.setType(type);
        DictTypeDTO dictTypeDTO = dictTypeService.getDictType(queryDictTypeDTO);
        ValidatorUtil.notEmpty(dictTypeDTO, ResultCodeEnum.DICT_TYPE_NOT_EXIST_ERROR);
        return dictTypeDTO;
    }

    /**
     * 分页获取字典类型列表(分页查询每次从DB获取最新的数据, 不走缓存)
     *
     * @param dictTypeQuery
     * @return
     */
    public Page<DictTypeDTO> getPageDictTypes(DictTypeQuery dictTypeQuery) {
        ValidatorUtil.notEmpty(dictTypeQuery);
        return dictTypeService.getPageDictTypes(dictTypeQuery);
    }

    /**
     * 批量删除字典类型并删除缓存
     *
     * @param dictTypeIds
     */
    public void deleteDictTypes(List<Long> dictTypeIds) {
        ValidatorUtil.notEmpty(dictTypeIds);
        // 批量查询字典类型数据
        List<DictTypeDTO> dictTypeDTOList = dictTypeService.getDictTypes(dictTypeIds);
        ValidatorUtil.isTrue(CollectionUtil.isNotEmpty(dictTypeIds), ResultCodeEnum.RESOURCE_NOT_EXIST);
        // 校验是否存在内置字典类型
        long internalCount = dictTypeDTOList.stream().filter(dict -> Objects.equals(dict.getInternal(),
                Constants.COMMON_JUDGMENT.Y)).count();
        ValidatorUtil.isTrue(internalCount <= 0, ResultCodeEnum.NOT_OPERATE_INTERNAL_DICT_TYPE);
        // 批量删除字典类型
        dictTypeService.deleteDictTypes(dictTypeIds);
    }

    /**
     * 字典类型数据导出数据(字典类型和对应字典数据全部导出)
     *
     * @param dictTypeQuery
     * @param response
     */
    public void export(DictTypeQuery dictTypeQuery, HttpServletResponse response) {
        ValidatorUtil.notEmpty(dictTypeQuery);
        dictTypeService.export(dictTypeQuery, response);
    }

    /**
     * 更新字典类型状态
     *
     * @param dictTypeDTO
     */
    public void switchDictTypeStatus(DictTypeDTO dictTypeDTO) {
        ValidatorUtil.notEmpty(dictTypeDTO);
        ValidatorUtil.allNotEmpty(dictTypeDTO.getId(), dictTypeDTO.getStatus());
        DictTypeDTO currentDictTypeDTO = dictTypeService.getDictType(dictTypeDTO.getId());
        ValidatorUtil.notEmpty(currentDictTypeDTO, ResultCodeEnum.DICT_TYPE_NOT_EXIST_ERROR);
        currentDictTypeDTO.setStatus(dictTypeDTO.getStatus());
        dictTypeService.updateDictType(currentDictTypeDTO);
    }

    // ============================================字典数据相关=================================================

    /**
     * 获取字典数据缓存键
     *
     * @param type 字典类型
     * @return
     */
    private String getDictDataCacheKey(String type) {
        ValidatorUtil.notBlank(type, ResultCodeEnum.CACHE_KEY_ERROR);
        return Constants.REDIS_PREFIX.DICT_DATA + StrUtil.COLON + type;
    }

    /**
     * 新增对应类型的字典数据
     *
     * @param dictDataDTO
     */
    public void addDictData(DictDataDTO dictDataDTO) {
        ValidatorUtil.notEmpty(dictDataDTO);
        ValidatorUtil.allNotBlank(dictDataDTO.getType(), dictDataDTO.getLabel(), dictDataDTO.getValue());
        // 校验字典类型是否存在
        DictTypeDTO dictTypeDTO = new DictTypeDTO();
        dictTypeDTO.setType(dictDataDTO.getType());
        DictTypeDTO currentDictTypeDTO = dictTypeService.getDictType(dictTypeDTO);
        ValidatorUtil.notEmpty(currentDictTypeDTO, ResultCodeEnum.DICT_TYPE_NOT_EXIST_ERROR);
        // 字典类型状态是否停用
        ValidatorUtil.isTrue(StrUtil.equals(currentDictTypeDTO.getStatus(), Constants.COMMON_JUDGMENT.ZONE_STR),
                ResultCodeEnum.DICT_TYPE_STATUS_ERROR);
        // 校验字典数据是否重复, 字典数据默认属性是否重复
        DictDataDTO queryDictDataDTO = new DictDataDTO();
        queryDictDataDTO.setType(dictDataDTO.getType());
        queryDictDataDTO.setLabel(dictDataDTO.getLabel());
        queryDictDataDTO.setDefaults(dictDataDTO.getDefaults());
        ValidatorUtil.isTrue(dictDataService.checkDictData(queryDictDataDTO), ResultCodeEnum.DICT_DATA_REPEAT_ERROR);
        // 保存数据
        dictDataService.addDictData(dictDataDTO);
        // 删除缓存(避免无法获取最新的字典数据信息)
        redisService.delete(getDictDataCacheKey(dictDataDTO.getType()));
    }

    /**
     * 获取对应类型的具体字典数据详情
     *
     * @param id
     * @param type
     * @return
     */
    public DictDataDTO getDictData(Long id, String type) {
        ValidatorUtil.notEmpty(id);
        ValidatorUtil.notBlank(type);
        List<DictDataDTO> dictDataDTOList = getDictDatas(type);
        DictDataDTO result = null;
        for (DictDataDTO dictDataDTO : dictDataDTOList) {
            if (ObjectUtil.equals(dictDataDTO.getId(), id)) {
                result = dictDataDTO;
            }
        }
        ValidatorUtil.notEmpty(result, ResultCodeEnum.RESOURCE_NOT_EXIST);
        return result;
    }

    /**
     * 更新对应类型的字典数据
     *
     * @param dictDataDTO
     */
    public void updateDictData(DictDataDTO dictDataDTO) {
        ValidatorUtil.notEmpty(dictDataDTO);
        ValidatorUtil.allNotEmpty(dictDataDTO.getId(), dictDataDTO.getType(), dictDataDTO.getValue());
        // 查询字典数据是否存在
        ValidatorUtil.notEmpty(dictDataService.getDictData(dictDataDTO.getId()), ResultCodeEnum.RESOURCE_NOT_EXIST);
        // 字典类型状态是否停用
        DictTypeDTO dictTypeDTO = new DictTypeDTO();
        dictTypeDTO.setType(dictDataDTO.getType());
        DictTypeDTO currentDictTypeDTO = dictTypeService.getDictType(dictTypeDTO);
        ValidatorUtil.notEmpty(currentDictTypeDTO, ResultCodeEnum.DICT_TYPE_NOT_EXIST_ERROR);
        ValidatorUtil.isTrue(StrUtil.equals(currentDictTypeDTO.getStatus(), Constants.COMMON_JUDGMENT.ZONE_STR),
                ResultCodeEnum.DICT_TYPE_STATUS_ERROR);
        // 校验字典数据是否重复, 字典数据默认属性是否重复
        DictDataDTO queryDictDataDTO = new DictDataDTO();
        queryDictDataDTO.setId(dictDataDTO.getId());
        queryDictDataDTO.setType(dictDataDTO.getType());
        queryDictDataDTO.setLabel(dictDataDTO.getLabel());
        queryDictDataDTO.setDefaults(dictDataDTO.getDefaults());
        ValidatorUtil.isTrue(dictDataService.checkDictData(queryDictDataDTO), ResultCodeEnum.DICT_DATA_REPEAT_ERROR);
        // 更新字典数据
        dictDataService.updateDictData(dictDataDTO);
        // 删除字典数据缓存
        redisService.delete(getDictDataCacheKey(dictDataDTO.getType()));
    }

    /**
     * 批量删除对应类型的字典数据
     *
     * @param dictDataIds
     */
    public void deleteDictDatas(List<Long> dictDataIds) {
        ValidatorUtil.notEmpty(dictDataIds);
        List<DictDataDTO> dictDataDTOList = dictDataService.getDictDatas(dictDataIds);
        ValidatorUtil.notEmpty(dictDataDTOList, ResultCodeEnum.RESOURCE_NOT_EXIST);
        // 字典类型是否系统内置(系统内置字典数据不允许删除)
        String type = dictDataDTOList.get(0).getType();
        DictTypeDTO dictTypeDTO = new DictTypeDTO();
        dictTypeDTO.setType(type);
        DictTypeDTO dictType = dictTypeService.getDictType(dictTypeDTO);
        ValidatorUtil.notEmpty(dictType, ResultCodeEnum.RESOURCE_NOT_EXIST);
        ValidatorUtil.isTrue(!StrUtil.equals(dictType.getInternal(), Constants.COMMON_JUDGMENT.Y),
                ResultCodeEnum.NOT_OPERATE_INTERNAL_DICT_TYPE);
        dictDataService.deleteDictDatas(dictDataIds);
        redisService.delete(getDictDataCacheKey(type));
    }

    /**
     * 获取对应类型的字典数据列表(非停用状态数据列表)
     *
     * @param type
     * @return
     */
    public List<DictDataDTO> getDictDatas(String type) {
        ValidatorUtil.notBlank(type);
        // 查询缓存
        List<DictDataDTO> dictDataDTOList = null;
        String cacheData = redisService.get(getDictDataCacheKey(type));
        if (StrUtil.isNotBlank(cacheData)) {
            dictDataDTOList = JsonUtil.fromJson(cacheData, new TypeReference<List<DictDataDTO>>() {
            });
        } else {
            // 查询DB
            DictDataDTO queryDictDataDTO = new DictDataDTO();
            queryDictDataDTO.setType(type);
            queryDictDataDTO.setStatus(Constants.COMMON_JUDGMENT.ZONE_STR);
            dictDataDTOList = dictDataService.getDictDatas(queryDictDataDTO);
        }
        ValidatorUtil.notEmpty(dictDataDTOList, ResultCodeEnum.RESOURCE_NOT_EXIST);
        // 录入缓存
        redisService.set(getDictDataCacheKey(type), JsonUtil.toJson(dictDataDTOList));
        return dictDataDTOList;
    }

    /**
     * 分页获取字典数据列表
     *
     * @param dictDataQuery
     * @return
     */
    public Page<DictDataDTO> getPageDictDatas(DictDataQuery dictDataQuery) {
        ValidatorUtil.notEmpty(dictDataQuery);
        return dictDataService.getPageDictDatas(dictDataQuery);
    }

    /**
     * 刷新字典数据缓存信息(只缓存状态正常的字典数据)
     */
    public void refresh() {
        redisService.deletePatternByPrefix(Constants.REDIS_PREFIX.DICT_DATA);
        DictTypeDTO queryDictTypeDTO = new DictTypeDTO();
        queryDictTypeDTO.setStatus(Constants.COMMON_JUDGMENT.ZONE_STR);
        List<DictTypeDTO> dictTypeDTOList = dictTypeService.getDictTypes(queryDictTypeDTO);
        if (CollectionUtil.isNotEmpty(dictTypeDTOList)) {
            List<String> types = dictTypeDTOList.stream().map(DictTypeDTO::getType).collect(Collectors.toList());
            DictDataDTO queryDictDataDTO = new DictDataDTO();
            queryDictDataDTO.setStatus(Constants.COMMON_JUDGMENT.ZONE_STR);
            queryDictDataDTO.setTypes(types);
            List<DictDataDTO> dictDataDTOList = dictDataService.getDictDatas(queryDictDataDTO);
            if (CollectionUtil.isNotEmpty(dictDataDTOList)) {
                Map<String, String> dictDataMap = new HashMap<>(16);
                Map<String, List<DictDataDTO>> dictDataTypeMap =
                        dictDataDTOList.stream().collect(Collectors.groupingBy(DictDataDTO::getType));
                for (Map.Entry<String, List<DictDataDTO>> entry : dictDataTypeMap.entrySet()) {
                    dictDataMap.put(getDictDataCacheKey(entry.getKey()), JsonUtil.toJson(entry.getValue()));
                }
                redisService.multiSet(dictDataMap);
            }
        }
    }

    /**
     * 字典类型实时检索
     *
     * @param search
     * @return
     */
    public List<InputRemoteFetchDTO> fetch(String search) {
        return dictTypeService.fetch(search);
    }

    /**
     * 获取字典类型列表
     *
     * @param
     * @return java.util.List<top.horsemuzi.system.pojo.dto.dict.DictTypeDTO>
     * @author mabin
     * @date 2023/4/5 16:23
     **/
    public List<DictTypeDTO> getDictTypeList() {
        return dictTypeService.getDictTypes();
    }

}
