package top.horsemuzi.system.manager.admin;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.core.redis.service.RedisService;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.open.api.ApiDTO;
import top.horsemuzi.system.pojo.dto.open.api.ApiQuery;
import top.horsemuzi.system.pojo.dto.open.log.ApiLogDTO;
import top.horsemuzi.system.pojo.dto.open.log.ApiLogQuery;
import top.horsemuzi.system.service.ApiCategoryService;
import top.horsemuzi.system.service.ApiLogService;
import top.horsemuzi.system.service.ApiService;

import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;

/**
 * 开放API接口模块
 *
 * @author 马滨
 * @date 2022/11/18 22:13
 **/

@Slf4j
@Component
@RequiredArgsConstructor
public class ApiManager {

    private final ApiService apiService;
    private final ApiCategoryService apiCategoryService;
    private final ApiLogService apiLogService;
    private final RedisService redisService;

    /**
     * 获取开放API接口列表
     *
     * @param apiCategoryId
     * @return
     */
    public List<ApiDTO> listApi(Long apiCategoryId) {
        ValidatorUtil.notEmpty(apiCategoryId);
        return apiService.getApiByCategory(Collections.singletonList(apiCategoryId));
    }

    /**
     * 分页获取开放API接口列表
     *
     * @param query
     * @return
     */
    public Page<ApiDTO> getPageApis(ApiQuery query) {
        return apiService.getPageApis(query);
    }

    /**
     * 获取开放API接口详情
     *
     * @param id
     * @return
     */
    public ApiDTO getApi(Long id) {
        ValidatorUtil.notEmpty(id);
        return apiService.getApi(id);
    }

    /**
     * 新增开放API接口
     *
     * @param apiDTO
     */
    public void addApi(ApiDTO apiDTO) {
        ValidatorUtil.notEmpty(apiDTO);
        // 接口是否重复
        ValidatorUtil.isTrue(apiService.checkApiRepeat(apiDTO), ResultCodeEnum.OPEN_API_NAME_REPEAT_ERROR);
        // 接口类型不为空情况: 类型信息是否存在
        if (ObjectUtil.isNotNull(apiDTO.getApiCategoryId())) {
            ValidatorUtil.notEmpty(apiCategoryService.getApiCategory(apiDTO.getApiCategoryId()), ResultCodeEnum.OPEN_API_CATEGORY_NOT_EXIST_ERROR);
        }
        // 保存API接口
        apiService.addApi(apiDTO);
    }

    /**
     * 更新开放API接口
     *
     * @param apiDTO
     */
    public void updateApi(ApiDTO apiDTO) {
        ValidatorUtil.notEmpty(apiDTO);
        ValidatorUtil.allNotEmpty(apiDTO.getId(), apiDTO.getApiCode());
        // 接口是否存在
        ValidatorUtil.notEmpty(apiService.getApi(apiDTO.getId()), ResultCodeEnum.OPEN_API_NOT_EXIST_ERROR);
        // 接口是否重复
        ValidatorUtil.isTrue(apiService.checkApiRepeat(apiDTO), ResultCodeEnum.OPEN_API_NAME_REPEAT_ERROR);
        // 接口类型不为空: 类型信息是否存在
        if (ObjectUtil.isNotNull(apiDTO.getApiCategoryId())) {
            ValidatorUtil.notEmpty(apiCategoryService.getApiCategory(apiDTO.getApiCategoryId()), ResultCodeEnum.OPEN_API_CATEGORY_NOT_EXIST_ERROR);
        }
        // 更新API接口
        if (apiService.updateApi(apiDTO)) {
            // 移除缓存
            String cacheKey = Constants.REDIS_PREFIX.OPEN_API + StrUtil.COLON + apiDTO.getApiCode();
            if (redisService.hasKey(cacheKey)) {
                redisService.delete(cacheKey);
            }
        }
    }

    /**
     * 删除开放API接口
     *
     * @param apiIds
     */
    public void deleteApi(List<Long> apiIds) {
        ValidatorUtil.notEmpty(apiIds);
        // API接口是否都存在
        List<ApiDTO> apiDTOList = apiService.getApi(apiIds);
        ValidatorUtil.isTrue(CollectionUtil.isNotEmpty(apiDTOList) && apiDTOList.size() == apiIds.size(), ResultCodeEnum.OPEN_API_NOT_EXIST_ERROR);
        if (apiService.deleteApi(apiIds)) {
            // 移除缓存
            for (ApiDTO apiDTO : apiDTOList) {
                String cacheKey = Constants.REDIS_PREFIX.OPEN_API + StrUtil.COLON + apiDTO.getApiCode();
                if (redisService.hasKey(cacheKey)) {
                    redisService.delete(cacheKey);
                }
            }
        }
    }

    /**
     * 更新API接口状态
     *
     * @param apiDTO
     */
    public void updateApiStatus(ApiDTO apiDTO) {
        ValidatorUtil.notEmpty(apiDTO);
        ValidatorUtil.allNotEmpty(apiDTO.getId(), apiDTO.getApiStatus());
        ApiDTO currentApiDTO = apiService.getApi(apiDTO.getId());
        ValidatorUtil.notEmpty(currentApiDTO, ResultCodeEnum.OPEN_API_NOT_EXIST_ERROR);
        currentApiDTO.setApiStatus(apiDTO.getApiStatus());
        if (apiService.updateApi(apiDTO)) {
            // 移除缓存
            String cacheKey = Constants.REDIS_PREFIX.OPEN_API + StrUtil.COLON + currentApiDTO.getApiCode();
            if (redisService.hasKey(cacheKey)) {
                redisService.delete(cacheKey);
            }
        }
    }

    /**
     * 接口名称实时检索
     *
     * @param search
     * @return
     */
    public List<InputRemoteFetchDTO> fetch(String search) {
        return apiService.fetch(search);
    }

    /**
     * API接口数据导出
     *
     * @param query
     * @param response
     */
    public void export(ApiQuery query, HttpServletResponse response) {
        apiService.export(query, response);
    }

    /**
     * 批量导入API接口数据
     *
     * @param file
     * @param jwtUser
     */
    public void imports(MultipartFile file, JwtUser jwtUser) {
        ValidatorUtil.notEmpty(file);
        apiService.imports(file, jwtUser);
    }

    /**
     * 分页获取开放API请求日志列表
     *
     * @param query
     * @return
     */
    public Page<ApiLogDTO> getPageApiLogs(ApiLogQuery query) {
        return apiLogService.getPageApiLogs(query);
    }

    /**
     * 获取开放API请求日志详情
     *
     * @param id
     * @return
     */
    public ApiLogDTO getApiLog(Long id) {
        ValidatorUtil.notEmpty(id);
        return apiLogService.getApiLog(id);
    }

    /**
     * 删除开放API请求日志
     *
     * @param apiLogIds
     */
    public void deleteApiLog(List<Long> apiLogIds) {
        ValidatorUtil.notEmpty(apiLogIds);
        apiLogService.deleteApiLog(apiLogIds);
    }

    /**
     * API接口请求日志数据导出
     *
     * @param query
     * @param response
     */
    public void exportApiLog(ApiLogQuery query, HttpServletResponse response) {
        apiLogService.exportApiLog(query, response);
    }

    /**
     * API日志链路ID实时检索
     *
     * @param search
     * @return
     */
    public List<InputRemoteFetchDTO> logFetch(String search) {
        return apiLogService.logFetch(search);
    }
}
