package top.horsemuzi.system.manager.open;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ApiSignEnum;
import top.horsemuzi.common.enums.ConfigEnum;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.EncryptUtil;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.NetUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.core.redis.service.RedisService;
import top.horsemuzi.system.pojo.dto.open.api.ApiDTO;
import top.horsemuzi.system.pojo.dto.open.api.ApiParams;
import top.horsemuzi.system.pojo.dto.open.app.AppDTO;
import top.horsemuzi.system.service.ApiService;
import top.horsemuzi.system.service.AppService;
import top.horsemuzi.system.service.ConfigService;

import java.lang.reflect.Method;
import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description 开放API统一处理模块
 * @date 2022/5/7 20:57
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class OpenManager {

    private final RedisService redisService;
    private final ApiService apiService;
    private final AppService appService;
    private final ConfigService configService;

    /**
     * 生成access_token入口
     *
     * @param apiParams
     * @return
     */
    public String apiToken(ApiParams apiParams) {
        ValidatorUtil.isTrue(checkTimeout(apiParams), ResultCodeEnum.API_REQUEST_TIMEOUT_ERROR);
        ValidatorUtil.isTrue(checkSign(apiParams), ResultCodeEnum.API_SIGN_ERROR);
        return createToken(apiParams);
    }


    /**
     * 外部API统一入口
     *
     * @param apiParams json结构参数
     * @return
     */
    public Object apiCore(ApiParams apiParams) {
        ValidatorUtil.isTrue(checkToken(apiParams), ResultCodeEnum.API_TOKEN_ERROR);
        ValidatorUtil.isTrue(checkTimeout(apiParams), ResultCodeEnum.API_REQUEST_TIMEOUT_ERROR);
        ValidatorUtil.isTrue(checkSign(apiParams), ResultCodeEnum.API_SIGN_ERROR);
        ValidatorUtil.isTrue(checkRequestLimit(apiParams), ResultCodeEnum.API_DAILY_REQUEST_LIMIT_ERROR);
        // 请求分发
        return executeApi(apiParams);
    }

    /**
     * 请求分发处理
     *
     * @param apiParams
     * @return
     */
    private Object executeApi(ApiParams apiParams) {
        try {
            ApiDTO apiDTO = getApi(apiParams.getApiCode());
            Object clazz = Class.forName(apiDTO.getApiClass()).newInstance();
            // 默认所有的服务方法参数都只有一个且为 String 类型
            Class<?>[] argsClass = new Class[]{String.class};
            Method method = clazz.getClass().getDeclaredMethod(apiDTO.getApiMethod(), argsClass);
            // 业务参数解密
            String data = apiParams.getData();
            if (StrUtil.isNotBlank(data)) {
                apiParams.setData(EncryptUtil.desedeDecrypt(data, getApp(apiParams.getAppKey()).getAppSecret()));
            }
            return method.invoke(clazz, apiParams.getData());
        } catch (Exception e) {
            throw new BusinessException(ResultCodeEnum.API_SERVER_ERROR);
        }
    }

    /**
     * 校验请求是否超时
     *
     * @param apiParams
     * @return
     */
    private boolean checkTimeout(ApiParams apiParams) {
        // 获取超时时间配置信息
        String configTimeout = configService.getConfigValue(ConfigEnum.OPEN_API_TIMEOUT);
        return Math.abs(System.currentTimeMillis() - apiParams.getTimestamp())
                < Duration.ofMinutes(Convert.toLong(configTimeout, Constants.COMMON_CODE.LONG_THREE)).toMillis();
    }

    /**
     * 校验token
     *
     * @param apiParams
     * @return
     */
    private boolean checkToken(ApiParams apiParams) {
        String cacheKey = Constants.REDIS_PREFIX.OPEN_TOKEN + StrUtil.COLON + apiParams.getAccessToken();
        String appKey = redisService.get(cacheKey);
        return StrUtil.isNotBlank(appKey) && StrUtil.equals(apiParams.getAppKey(), appKey);
    }

    /**
     * 生成token: 如果token已存在且有效期大于10分钟返回原token值, 否则重新生成token并返回
     *
     * @param apiParams
     * @return
     */
    private String createToken(ApiParams apiParams) {
        try {
            // 获取有效的应用信息
            AppDTO appDTO = getApp(apiParams.getAppKey());
            String accessToken = appDTO.getAccessToken();
            String newAccessToken = null;
            if (StrUtil.isNotBlank(accessToken)) {
                String cacheKey = Constants.REDIS_PREFIX.OPEN_TOKEN + StrUtil.COLON + accessToken;
                Long expire = redisService.getExpire(cacheKey, TimeUnit.MINUTES);
                // token有效期大于10分钟返回原token
                if (expire > Constants.COMMON_CODE.LONG_TEN) {
                    newAccessToken = redisService.get(cacheKey);
                } else {
                    // 重新生成token
                    newAccessToken = IdUtil.fastSimpleUUID();
                    // 更新应用的token值
                    AppDTO updateAppDTO = new AppDTO();
                    updateAppDTO.setId(appDTO.getId());
                    updateAppDTO.setAccessToken(newAccessToken);
                    appService.updateApp(updateAppDTO);
                    // 删除缓存中的token值
                    if (redisService.hasKey(cacheKey)) {
                        redisService.delete(cacheKey);
                    }
                    // 重新设置token缓存
                    String configTokenExpire = configService.getConfigValue(ConfigEnum.OPEN_TOKEN_EXPIRE);
                    redisService.set(cacheKey, appDTO.getAppKey(), Convert.toLong(configTokenExpire, Constants.COMMON_CODE.LONG_TWO), TimeUnit.HOURS);
                }
            } else {
                newAccessToken = IdUtil.fastSimpleUUID();
                // 更新应用的token值
                AppDTO updateAppDTO = new AppDTO();
                updateAppDTO.setId(appDTO.getId());
                updateAppDTO.setAccessToken(newAccessToken);
                appService.updateApp(updateAppDTO);
                // 重新设置token缓存
                String configTokenExpire = configService.getConfigValue(ConfigEnum.OPEN_TOKEN_EXPIRE);
                String cacheKey = Constants.REDIS_PREFIX.OPEN_TOKEN + StrUtil.COLON + newAccessToken;
                redisService.set(cacheKey, appDTO.getAppKey(), Convert.toLong(configTokenExpire, Constants.COMMON_CODE.LONG_TWO), TimeUnit.HOURS);
            }
            return newAccessToken;
        } catch (Exception e) {
            throw new BusinessException(ResultCodeEnum.API_SERVER_ERROR);
        }
    }

    /**
     * 签名校验
     *
     * @param apiParams
     * @return
     */
    private boolean checkSign(ApiParams apiParams) {
        Map<String, String> paramsMap = JsonUtil.fromJson(JsonUtil.toJson(apiParams), new TypeReference<TreeMap<String, String>>() {
        });
        if (MapUtil.isEmpty(paramsMap)) {
            return false;
        }
        paramsMap.remove("sign");
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<String, String> entry : paramsMap.entrySet()) {
            builder.append(entry.getValue());
        }
        String plainText = builder.toString() + getApp(apiParams.getAppKey()).getAppSecret();
        // 签名方法为空则默认为md5验签方式
        if (StrUtil.isBlank(apiParams.getSignMethod())) {
            apiParams.setSignMethod(ApiSignEnum.MD5.getSignMethod());
        }
        return StrUtil.equals(apiParams.getSign(), ApiSignEnum.sign(apiParams.getSignMethod(), plainText));
    }

    /**
     * 校验应用日请求是否超限
     *
     * @param apiParams
     * @return
     */
    private boolean checkRequestLimit(ApiParams apiParams) {
        // 获取应用日调用量缓存, 缓存无数据则默认为0 (一般不会出现无数据的情况)
        String cacheKey = Constants.REDIS_PREFIX.OPEN_API_DAILY_LIMIT + StrUtil.COLON + EncryptUtil.md5(apiParams.getAppKey() + apiParams.getApiCode() + DateUtil.formatDate(new Date()));
        Long cacheRequestCount = Constants.COMMON_CODE.LONG_ZONE;
        if (redisService.hasKey(cacheKey)) {
            cacheRequestCount = Convert.toLong(redisService.get(cacheKey));
        }
        // 获取接口的日调用限制数量
        return getApi(apiParams.getApiCode()).getDailyRequestLimit() >= cacheRequestCount;
    }

    /**
     * 获取API应用的 appSecret
     *
     * @param appKey
     * @return
     */
    private AppDTO getApp(String appKey) {
        String cacheKey = Constants.REDIS_PREFIX.OPEN_APP + StrUtil.COLON + appKey;
        String appJson = redisService.get(cacheKey);
        AppDTO appDTO = null;
        if (StrUtil.isNotBlank(appJson)) {
            appDTO = JsonUtil.fromJson(appJson, AppDTO.class);
        } else {
            appDTO = appService.getApp(appKey);
        }
        // 应用是否存在
        ValidatorUtil.notEmpty(appDTO, ResultCodeEnum.API_APP_NOT_EXIST_ERROR);
        // 应用是否审批通过状态
        ValidatorUtil.equals(appDTO.getApprovalStatus(), Constants.COMMON_CODE.STR_ONE, ResultCodeEnum.API_APP_STATUS_ERROR);
        // 访问ip是否在应用白名单内, 白名单值为空, 默认不校验
        if (StrUtil.isNotBlank(appDTO.getIpWhiteList())) {
            String ip = NetUtil.getRequestIp();
            List<String> ipWhiteList = StrUtil.split(appDTO.getIpWhiteList(), StrUtil.COMMA);
            ValidatorUtil.isTrue(ipWhiteList.contains(ip), ResultCodeEnum.API_APP_IP_WHITE_LIST_LIMIT_ERROR);
        }
        redisService.set(cacheKey, JsonUtil.toJson(appDTO));
        return appDTO;
    }

    /**
     * 获取API接口
     *
     * @param apiCode
     * @return
     */
    private ApiDTO getApi(String apiCode) {
        String cacheKey = Constants.REDIS_PREFIX.OPEN_API + StrUtil.COLON + apiCode;
        ApiDTO apiDTO = null;
        String apiJson = redisService.get(cacheKey);
        if (StrUtil.isNotBlank(apiJson)) {
            apiDTO = JsonUtil.fromJson(apiJson, ApiDTO.class);
        } else {
            apiDTO = apiService.getApi(apiCode);
        }
        ValidatorUtil.notEmpty(apiDTO, ResultCodeEnum.API_NOT_EXIST_ERROR);
        // 接口状态是否正常
        ValidatorUtil.equals(apiDTO.getApiStatus(), Constants.COMMON_CODE.STR_ZONE, ResultCodeEnum.API_STATUS_ERROR);
        redisService.set(cacheKey, JsonUtil.toJson(apiDTO));
        return apiDTO;
    }

}
