package top.horsemuzi.system.manager.admin;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.Validator;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.text.StrPool;
import cn.hutool.core.util.*;
import cn.hutool.extra.pinyin.PinyinUtil;
import cn.hutool.http.ContentType;
import cn.hutool.http.Header;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ConfigEnum;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.*;
import top.horsemuzi.common.util.http.HttpUtil;
import top.horsemuzi.system.config.properties.EncryptProperties;
import top.horsemuzi.system.core.redis.service.RedisService;
import top.horsemuzi.system.manager.common.CommonManager;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.division.DivisionDTO;
import top.horsemuzi.system.pojo.dto.role.RoleDTO;
import top.horsemuzi.system.pojo.dto.user.PersonDTO;
import top.horsemuzi.system.pojo.dto.user.ShortcutMenuDTO;
import top.horsemuzi.system.pojo.dto.user.UserDTO;
import top.horsemuzi.system.pojo.dto.user.UserQuery;
import top.horsemuzi.system.pojo.entity.UserRole;
import top.horsemuzi.system.service.*;

import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description 用户模块: 登录、用户角色权限管理相关等
 * @date 2022/5/7 20:56
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class UserManager {

    private final UserService userService;
    private final UserRoleService userRoleService;
    private final CommonManager commonManager;
    private final RoleService roleService;
    private final RedisService redisService;
    private final OssManager ossManager;
    private final DivisionService divisionService;
    private final ShortcutMenuService shortcutMenuService;
    private final ConfigService configService;

    @Value("${custom.aliyun_auth.url}")
    private String url;

    @Value("${custom.aliyun_auth.app_code}")
    private String appCode;


    /**
     * 获取用户列表-用户管理
     *
     * @param userQuery
     * @return
     */
    public Page<UserDTO> getPageUsers(UserQuery userQuery) {
        ValidatorUtil.notEmpty(userQuery);
        Page<UserDTO> pageUsers = userService.getPageUsers(userQuery);

        // 当前页用户是否被登录锁定属性赋值
        List<UserDTO> userDTOList = pageUsers.getRecords();
        if (CollectionUtil.isNotEmpty(userDTOList)) {
            List<String> usernameKeys = userDTOList.stream().map(user -> Constants.REDIS_PREFIX.WRONG_PWD + StrUtil.COLON + user.getUsername()).collect(Collectors.toList());
            Map<String, String> cacheLockKeyMap = redisService.multiGetMap(usernameKeys);
            if (MapUtil.isNotEmpty(cacheLockKeyMap)) {
                for (UserDTO userDTO : userDTOList) {
                    String cacheKey = Constants.REDIS_PREFIX.WRONG_PWD + StrUtil.COLON + userDTO.getUsername();
                    if (StrUtil.isNotBlank(cacheLockKeyMap.get(cacheKey))) {
                        // aes加密账号锁定的缓存key, 便于页面直接操作进行强制解除锁定
                        userDTO.setAccountCacheLockKey(EncryptUtil.aesEncrypt(cacheKey, EncryptProperties.aesKey));
                        break;
                    }
                }
            }
        }
        pageUsers.setRecords(userDTOList);
        return pageUsers;
    }

    /**
     * 新增用户-用户管理
     *
     * @param userDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public void addUser(UserDTO userDTO) {
        // 校验账号是否已存在
        verifyExistUser(userDTO);
        // 手机号、邮箱必须存在一个有效信息且和用户名不能相同
        verifyUserAccount(userDTO);
        // 组装密码: 密码RSA加密, 约定添加用户的时候, 密码加密方式: pbkdf2加盐加密, 盐值默认为: UUID
        String password = userDTO.getPassword();
        password = EncryptUtil.rsaDecrypt(EncryptProperties.rsaPrivateKey, password);
        String salt = IdUtil.fastSimpleUUID();
        userDTO.setSalt(salt);
        userDTO.setPassword(EncryptUtil.pbkdf2(password, salt));
        // 添加默认头像
        userDTO.setAvatar(CommonCodeUtil.generateImg(userDTO.getUsername()));
        // 添加用户名拼音相关字段
        setPinyinByUsername(userDTO);
        // 组装用户地址信息
        if (StrUtil.isNotBlank(userDTO.getAddress())) {
            String tempAddress = StrUtil.EMPTY;
            List<String> addressList = StrUtil.split(userDTO.getAddress(), StrUtil.COMMA);
            if (addressList.size() >= Constants.COMMON_CODE.BASE_INT_THREE) {
                List<Long> divisionIds = new ArrayList<>(Arrays.asList(Convert.toLong(addressList.get(0)),
                        Convert.toLong(addressList.get(1)), Convert.toLong(addressList.get(2))));
                List<DivisionDTO> divisionDTOList = divisionService.getDivision(divisionIds);
                if (CollectionUtil.isNotEmpty(divisionDTOList)) {
                    tempAddress = divisionDTOList.stream().sorted(Comparator.comparing(DivisionDTO::getLevel))
                            .map(DivisionDTO::getName)
                            .collect(Collectors.joining(StrUtil.EMPTY));
                }
                if (addressList.size() >= Constants.COMMON_CODE.BASE_INT_FOUR) {
                    tempAddress = tempAddress + addressList.get(3);
                }
            }
            userDTO.setAddress(tempAddress);
        }
        userService.addUser(userDTO);
        // 新增用户角色关联信息
        if (StrUtil.isNotBlank(userDTO.getRoleCodes())) {
            insertUserRoleInfo(userDTO);
        }
    }

    /**
     * 校验用户绑定手机号、邮箱号的有效性
     *
     * @param userDTO
     */
    private void verifyUserAccount(UserDTO userDTO) {
        String phone = userDTO.getPhone();
        String email = userDTO.getEmail();
        ValidatorUtil.notTrue(StrUtil.isAllBlank(phone, email), ResultCodeEnum.BING_ACCOUNT_NOT_ALL_EMPTY_ERROR);
        if (StrUtil.isNotBlank(phone)) {
            ValidatorUtil.isTrue(Validator.isMobile(phone), ResultCodeEnum.PHONE_NOT_ILLEGALITY_ERROR);
            ValidatorUtil.isTrue(!StrUtil.equals(phone, userDTO.getUsername()), ResultCodeEnum.USER_THREE_ACCOUNT_SAME_ERROR);
        }
        if (StrUtil.isNotBlank(email)) {
            ValidatorUtil.isTrue(Validator.isEmail(email), ResultCodeEnum.EMAIL_NOT_ILLEGALITY_ERROR);
            ValidatorUtil.isTrue(!StrUtil.equals(email, userDTO.getUsername()), ResultCodeEnum.USER_THREE_ACCOUNT_SAME_ERROR);
        }
    }

    /**
     * 根据用户名设置拼音相关信息
     *
     * @param userDTO
     */
    private void setPinyinByUsername(UserDTO userDTO) {
        ValidatorUtil.notEmpty(userDTO);
        String username = userDTO.getUsername();
        if (StrUtil.isNotBlank(username)) {
            // 设置用户名拼音值 (汉字解析成拼音, 其他值直接拼接)
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < username.length(); i++) {
                char c = username.charAt(i);
                if (PinyinUtil.isChinese(c)) {
                    builder.append(PinyinUtil.getPinyin(c));
                } else {
                    builder.append(c);
                }
            }
            if (!StrUtil.equals(userDTO.getPinyinUsername(), builder.toString())) {
                userDTO.setPinyinUsername(builder.toString());
            }
            // 设置用户名拼音缩写值(全中文解析成缩写, 其他值不进行设置缩写)
            boolean isChinese = true;
            for (int i = 0; i < username.length(); i++) {
                char c = username.charAt(i);
                if (!PinyinUtil.isChinese(c)) {
                    isChinese = false;
                    break;
                }
            }
            if (isChinese) {
                String firstLetter = PinyinUtil.getFirstLetter(username, StrUtil.EMPTY);
                if (!StrUtil.equals(userDTO.getPinyinAcronymUsername(), firstLetter)) {
                    userDTO.setPinyinAcronymUsername(firstLetter);
                }
            }
        }
    }

    /**
     * 插入用户角色关联信息
     *
     * @param userDTO
     */
    private void insertUserRoleInfo(UserDTO userDTO) {
        List<RoleDTO> roleDTOList = roleService.getRolesByCodes(StrUtil.split(userDTO.getRoleCodes(), StrUtil.COMMA));
        ValidatorUtil.notEmpty(roleDTOList, ResultCodeEnum.ROLE_NOT_EXIST);
        List<UserRole> userRoles = new ArrayList<>();
        UserRole userRole = null;
        for (RoleDTO roleDTO : roleDTOList) {
            userRole = new UserRole();
            userRole.setUserId(userDTO.getId()).setRoleId(roleDTO.getId());
            userRoles.add(userRole);
        }
        userRoleService.addBatchUserRole(userRoles);
    }

    /**
     * 更新用户-用户管理
     *
     * @param userDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateUser(UserDTO userDTO) {
        ValidatorUtil.notEmpty(userDTO);
        // 校验用户是否存在
        ValidatorUtil.notEmpty(userDTO.getId(), ResultCodeEnum.OBJECT_ID_IS_NULL);
        ValidatorUtil.notEmpty(userService.getUser(userDTO.getId()), ResultCodeEnum.USER_NOT_EXIST);
        // 校验账号是否已存在
        verifyExistUser(userDTO);
        // 手机号、邮箱必须存在一个有效信息
        verifyUserAccount(userDTO);
        // 添加用户名拼音相关字段
        setPinyinByUsername(userDTO);
        // 用户头像是否存在: 不存在着默认生成默认头像
        if (StrUtil.isBlank(userDTO.getAvatar())) {
            userDTO.setAvatar(CommonCodeUtil.generateImg(userDTO.getUsername()));
        }
        userService.updateUser(userDTO);
        // 更新用户角色关联信息(先删除, 再新增)
        userRoleService.deleteUserRoleByUserIds(Collections.singletonList(userDTO.getId()));
        if (StrUtil.isNotBlank(userDTO.getRoleCodes())) {
            insertUserRoleInfo(userDTO);
        }
    }

    /**
     * 校验账号是否已存在(用户名、手机号、邮箱号均唯一)
     *
     * @param userDTO
     */
    private void verifyExistUser(UserDTO userDTO) {
        UserDTO existUser = userService.getExistUser(userDTO);
        if (ObjectUtil.isNotNull(existUser)) {
            // 用户名已存在
            ValidatorUtil.isTrue(!StrUtil.equals(existUser.getUsername(), userDTO.getUsername()), ResultCodeEnum.USER_EXIST_ERROR);
            // 手机号已存在
            ValidatorUtil.isTrue(!StrUtil.equals(existUser.getPhone(), userDTO.getPhone()), ResultCodeEnum.PHONE_EXIST_ERROR);
            // 邮箱号已存在
            ValidatorUtil.isTrue(!StrUtil.equals(existUser.getEmail(), userDTO.getEmail()), ResultCodeEnum.EMAIL_EXIST_ERROR);
        }
    }

    /**
     * 获取用户信息-用户管理
     *
     * @param userId
     * @return
     */
    public UserDTO getUser(Long userId) {
        return userService.getUser(userId);
    }

    /**
     * 重置用户密码-用户管理
     *
     * @param userDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public void resetPassword(UserDTO userDTO) {
        ValidatorUtil.notEmpty(userDTO);
        Long id = userDTO.getId();
        ValidatorUtil.notEmpty(id, ResultCodeEnum.OBJECT_ID_IS_NULL);
        String password = userDTO.getPassword();
        ValidatorUtil.notEmpty(password);
        UserDTO currentUserDTO = userService.getUser(id);
        ValidatorUtil.notEmpty(currentUserDTO, ResultCodeEnum.USER_NOT_EXIST);
        password = EncryptUtil.rsaDecrypt(EncryptProperties.rsaPrivateKey, password);
        password = EncryptUtil.pbkdf2(password, currentUserDTO.getSalt());
        userDTO.setPassword(password);
        // 用户是否开启强制密码更新策略
        userDTO.setLastUpdatePwdTime(new Date());
        userService.updateUser(userDTO);
    }

    /**
     * 批量删除用户-用户管理(不允许删除自己的账号)
     *
     * @param userIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public void deleteUsers(List<Long> userIds) {
        ValidatorUtil.notEmpty(userIds);
        // 是否当前操作账号
        JwtUser cacheJwtUser = commonManager.getCacheJwtUser();
        ValidatorUtil.notEmpty(cacheJwtUser, ResultCodeEnum.USER_NOT_EXIST);
        ValidatorUtil.isTrue(!userIds.contains(cacheJwtUser.getId()), ResultCodeEnum.NOT_OPERATE_SELF_ACCOUNT);
        // 删除用户信息
        userService.deleteUsers(userIds);
        // 删除关联的用户角色信息
        userRoleService.deleteUserRoleByUserIds(userIds);
    }

    /**
     * 导出数据-用户管理
     *
     * @param userQuery
     * @param response
     */
    public void export(UserQuery userQuery, HttpServletResponse response) {
        userService.export(userQuery, response);
    }

    /**
     * 导入数据-用户管理
     *
     * @param file    表单文件
     * @param flag    是否同步更新已存在用户信息标识: 0-不更新, 1-更新
     * @param jwtUser 当前操作用户基础信息
     */
    public void imports(MultipartFile file, Integer flag, JwtUser jwtUser) {
        ValidatorUtil.allNotEmpty(file, flag);
        userService.imports(file, flag, jwtUser);
    }

    /**
     * 更新账号状态
     *
     * @param userDTO
     */
    public void updateUserStatus(UserDTO userDTO) {
        ValidatorUtil.notEmpty(userDTO);
        ValidatorUtil.allNotEmpty(userDTO.getId(), userDTO.getStatus());
        UserDTO currentUser = userService.getUser(userDTO.getId());
        ValidatorUtil.isTrue(ObjectUtil.isNotEmpty(currentUser), ResultCodeEnum.USER_NOT_EXIST);
        currentUser.setStatus(userDTO.getStatus());
        userService.updateUser(userDTO);
    }

    /**
     * 获取全部用户列表
     *
     * @param userQuery
     * @return
     */
    public List<UserDTO> getUsers(UserQuery userQuery) {
        return userService.getUsers(userQuery);
    }

    /**
     * 获取个人用户信息
     *
     * @param jwtUser
     * @return
     */
    public PersonDTO getPerson(JwtUser jwtUser) {
        ValidatorUtil.notEmpty(jwtUser);
        ValidatorUtil.notEmpty(jwtUser.getId());
        UserDTO userDTO = userService.getUser(jwtUser.getId());
        ValidatorUtil.notEmpty(userDTO, ResultCodeEnum.USER_NOT_EXIST);
        PersonDTO personDTO = OrikaUtil.convert(userDTO, PersonDTO.class);
        // 组装角色名称信息
        if (StrUtil.isNotBlank(personDTO.getRoleCodes())) {
            List<RoleDTO> roleDTOList = roleService.getRolesByCodes(StrUtil.split(personDTO.getRoleCodes(), StrUtil.COMMA));
            ValidatorUtil.notEmpty(roleDTOList, ResultCodeEnum.ROLE_NOT_EXIST);
            personDTO.setRoleNames(roleDTOList.stream().map(RoleDTO::getName).collect(Collectors.toList()));
        }
        return personDTO;
    }

    /**
     * 更新个人信息（用户名、手机号、邮箱、性别、地址、简介等个人信息）
     *
     * @param personDTO
     */
    @Transactional(rollbackFor = Exception.class)
    public void updatePerson(PersonDTO personDTO) {
        ValidatorUtil.notEmpty(personDTO);
        ValidatorUtil.notEmpty(personDTO.getId(), ResultCodeEnum.OBJECT_ID_IS_NULL);
        // 用户是否存在
        UserDTO userDTO = userService.getUser(personDTO.getId());
        ValidatorUtil.notEmpty(userDTO, ResultCodeEnum.USER_NOT_EXIST);
        // 用户名是否修改
        if (!StrUtil.equals(personDTO.getUsername(), userDTO.getUsername())) {
            userDTO.setUsername(personDTO.getUsername());
            // 设置用户名拼音字段值
            setPinyinByUsername(userDTO);
        }
        userDTO.setPhone(personDTO.getPhone());
        userDTO.setEmail(personDTO.getEmail());
        userDTO.setSex(personDTO.getSex());
        userDTO.setForceUpdatePaw(personDTO.getForceUpdatePaw());
        userDTO.setForceUpdateInterval(personDTO.getForceUpdateInterval());
        userDTO.setLastUpdatePwdTime(new Date());
        userDTO.setAddress(personDTO.getAddress());
        userDTO.setUserDesc(personDTO.getUserDesc());
        // 用户名/手机号/邮箱是否重复
        verifyExistUser(userDTO);
        // 更新个人信息
        userService.updateUser(userDTO);
    }

    /**
     * 修改个人密码
     *
     * @param personDTO
     */
    @Transactional(rollbackFor = Exception.class)
    public void updatePassword(PersonDTO personDTO) {
        ValidatorUtil.notEmpty(personDTO);
        ValidatorUtil.notEmpty(personDTO.getId(), ResultCodeEnum.OBJECT_ID_IS_NULL);
        UserDTO userDTO = userService.getUser(personDTO.getId());
        ValidatorUtil.notEmpty(userDTO, ResultCodeEnum.USER_NOT_EXIST);
        // 校验新旧密码是否相同（新密码必须不等于旧密码）
        String password = EncryptUtil.rsaDecrypt(EncryptProperties.rsaPrivateKey, personDTO.getPassword());
        String newPassword = EncryptUtil.rsaDecrypt(EncryptProperties.rsaPrivateKey, personDTO.getNewPassword());
        ValidatorUtil.notTrue(StrUtil.equals(password, newPassword), ResultCodeEnum.INTERNAL_ERROR);
        // 校验旧密码是否相同(旧密码和数据库密码是否相同)
        String salt = userDTO.getSalt();
        ValidatorUtil.isTrue(StrUtil.equals(EncryptUtil.pbkdf2(password, salt), userDTO.getPassword()), ResultCodeEnum.INTERNAL_ERROR);
        // 更新用户密码
        userDTO.setPassword(EncryptUtil.pbkdf2(newPassword, salt));
        // 用户是否开启强制密码更新策略
        userDTO.setLastUpdatePwdTime(new Date());
        userService.updateUser(userDTO);
    }

    /**
     * 修改个人头像
     *
     * @param file
     * @param jwtUser
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public String updateAvatar(MultipartFile file, JwtUser jwtUser) {
        ValidatorUtil.allNotEmpty(file, jwtUser);
        ValidatorUtil.notEmpty(jwtUser.getId());
        // 文件是否图片类型
        ValidatorUtil.isTrue(ArrayUtil.contains(Constants.MIME_TYPE.IMAGE_EXTENSION,
                FileUtil.extName(file.getOriginalFilename())), ResultCodeEnum.NOT_ACCEPTABLE);
        // 文件上传
        String url = ossManager.upload(file).getUrl();
        // 更新用户头像信息
        UserDTO userDTO = userService.getUser(jwtUser.getId());
        ValidatorUtil.notEmpty(userDTO, ResultCodeEnum.USER_NOT_EXIST);
        userDTO.setAvatar(url);
        userService.updateUser(userDTO);
        return url;
    }

    /**
     * 解除账号锁定
     *
     * @param userDTO
     */
    public void unlockUser(UserDTO userDTO) {
        ValidatorUtil.notEmpty(userDTO);
        ValidatorUtil.allNotEmpty(userDTO.getId(), userDTO.getAccountCacheLockKey());
        ValidatorUtil.notEmpty(userService.getUser(userDTO.getId()), ResultCodeEnum.USER_NOT_EXIST);
        String cacheKey = EncryptUtil.aesDecrypt(userDTO.getAccountCacheLockKey(), EncryptProperties.aesKey);
        if (StrUtil.isNotBlank(cacheKey) && redisService.hasKey(cacheKey)) {
            ValidatorUtil.isTrue(redisService.delete(cacheKey), ResultCodeEnum.LOGIN_ACCOUNT_UNLOCK_ERROR);
        }
    }

    /**
     * 用户名输入框实时检索
     *
     * @param search
     * @param filedType
     * @return
     */
    public List<InputRemoteFetchDTO> fetch(String search, String filedType) {
        return userService.fetch(search, filedType);
    }

    /**
     * 获取个人快捷菜单列表
     *
     * @param jwtUser
     * @return
     */
    public List<ShortcutMenuDTO> getShortcutMenus(JwtUser jwtUser) {
        String cacheKey = Constants.REDIS_PREFIX.SHORTCUT_MENU + StrUtil.COLON + jwtUser.getId();
        String shortcutMenuJson = redisService.get(cacheKey);
        if (StrUtil.isNotBlank(shortcutMenuJson)) {
            return JsonUtil.fromJson(shortcutMenuJson, new TypeReference<List<ShortcutMenuDTO>>() {
            });
        } else {
            List<ShortcutMenuDTO> shortcutMenuDTOList = shortcutMenuService.getShortcutMenuByUserId(jwtUser.getId());
            if (CollUtil.isNotEmpty(shortcutMenuDTOList)) {
                redisService.set(cacheKey, JsonUtil.toJson(shortcutMenuDTOList));
            }
            return shortcutMenuDTOList;
        }
    }

    /**
     * 获取个人快捷菜单详情
     *
     * @param id
     * @return
     */
    public ShortcutMenuDTO getShortcutMenu(Long id) {
        ValidatorUtil.notEmpty(id);
        return shortcutMenuService.getShortcutMenu(id);
    }


    /**
     * 新增个人快捷菜单
     *
     * @param shortcutMenuDTO
     * @param jwtUser
     */
    public void addShortcutMenu(ShortcutMenuDTO shortcutMenuDTO, JwtUser jwtUser) {
        ValidatorUtil.notEmpty(shortcutMenuDTO);
        shortcutMenuDTO.setUserId(jwtUser.getId());
        // 快捷菜单个数是否超出限制
        String limit = configService.getConfigValue(ConfigEnum.SHORTCUT_MENU_LIMIT_COUNT);
        List<ShortcutMenuDTO> shortcutMenuDTOList = shortcutMenuService.getShortcutMenuByUserId(jwtUser.getId());
        ValidatorUtil.isTrue(CollUtil.isEmpty(shortcutMenuDTOList)
                || shortcutMenuDTOList.size() < Convert.toInt(limit, Constants.COMMON_CODE.NINE), ResultCodeEnum.USER_SHORTCUT_MENU_LIMIT_ERROR);
        // 快捷菜单是否重复
        ValidatorUtil.isTrue(shortcutMenuService.checkShortcutMenuRepeat(shortcutMenuDTO), ResultCodeEnum.USER_SHORTCUT_MENU_REPEAT_ERROR);
        // 新增快捷菜单
        if (shortcutMenuService.addShortcutMenu(shortcutMenuDTO)) {
            // 移除缓存
            deleteCacheShortMenu(jwtUser.getId());
        }
    }

    /**
     * 更新个人快捷菜单
     *
     * @param shortcutMenuDTO
     * @param jwtUser
     */
    public void updateShortcutMenu(ShortcutMenuDTO shortcutMenuDTO, JwtUser jwtUser) {
        ValidatorUtil.notEmpty(shortcutMenuDTO);
        shortcutMenuDTO.setUserId(jwtUser.getId());
        // 快捷菜单是否存在
        ValidatorUtil.notEmpty(shortcutMenuService.getShortcutMenu(shortcutMenuDTO.getId()), ResultCodeEnum.USER_SHORTCUT_MENU_NOT_EXIST_ERROR);
        // 快捷菜单是否重复
        ValidatorUtil.isTrue(shortcutMenuService.checkShortcutMenuRepeat(shortcutMenuDTO), ResultCodeEnum.USER_SHORTCUT_MENU_REPEAT_ERROR);
        if (shortcutMenuService.updateShortcutMenu(shortcutMenuDTO)) {
            // 移除缓存
            deleteCacheShortMenu(jwtUser.getId());
        }
    }

    /**
     * 删除个人快捷菜单
     *
     * @param shortcutMenuIds
     * @param jwtUser
     */
    public void deleteShortcutMenu(List<Long> shortcutMenuIds, JwtUser jwtUser) {
        ValidatorUtil.notEmpty(shortcutMenuIds);
        shortcutMenuService.deleteShortcutMenu(shortcutMenuIds);
        // 移除缓存
        deleteCacheShortMenu(jwtUser.getId());
    }

    /**
     * 移除用户个人快捷菜单缓存
     *
     * @param userId
     */
    private void deleteCacheShortMenu(Long userId) {
        String cacheKey = Constants.REDIS_PREFIX.SHORTCUT_MENU + StrPool.COLON + userId;
        if (Boolean.TRUE.equals(redisService.hasKey(cacheKey))) {
            redisService.delete(cacheKey);
        }
    }

    /**
     * 校验用户是否需要强制更新密码
     *
     * @param jwtUser
     * @return java.lang.Boolean
     * @author mabin
     * @date 2023/4/25 14:25
     **/
    public Boolean verifyForceUpdatePassword(JwtUser jwtUser) {
        ValidatorUtil.notEmpty(jwtUser);
        UserDTO userDTO = userService.getUser(jwtUser.getId());
        ValidatorUtil.notEmpty(userDTO, ResultCodeEnum.USER_NOT_EXIST);
        if (CharSequenceUtil.equals(userDTO.getForceUpdatePaw(), Constants.COMMON_JUDGMENT.ZONE_STR)) {
            ValidatorUtil.isTrue(ObjectUtil.isAllNotEmpty(userDTO.getForceUpdateInterval(), userDTO.getLastUpdatePwdTime(),
                    ResultCodeEnum.USER_FORCE_UPDATE_PASSWORD_ERROR));
            if (DateUtil.betweenDay(userDTO.getLastUpdatePwdTime(), new Date(), true) > userDTO.getForceUpdateInterval()) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    /**
     * 强制更新用户密码
     *
     * @param personDTO
     * @param jwtUser
     * @return void
     * @author mabin
     * @date 2023/4/25 14:58
     **/
    public void forceUpdatePassword(PersonDTO personDTO, JwtUser jwtUser) {
        ValidatorUtil.allNotEmpty(personDTO, jwtUser);
        UserDTO userDTO = userService.getUser(jwtUser.getId());
        ValidatorUtil.notEmpty(userDTO, ResultCodeEnum.USER_NOT_EXIST);
        if (CharSequenceUtil.equals(userDTO.getForceUpdatePaw(), Constants.COMMON_JUDGMENT.ZONE_STR)) {
            String newPassword = EncryptUtil.rsaDecrypt(EncryptProperties.rsaPrivateKey, personDTO.getNewPassword());
            userDTO.setPassword(EncryptUtil.pbkdf2(newPassword, userDTO.getSalt()));
            userDTO.setLastUpdatePwdTime(new Date());
            userService.updateUser(userDTO);
        }
    }

    /**
     * 用户实名认证
     *
     * @param personDTO
     * @return void
     * @author Mr.Horse
     * @date 2023/5/1 16:13
     **/
    public void authentication(PersonDTO personDTO) {
        ValidatorUtil.notEmpty(personDTO);
        ValidatorUtil.notEmpty(personDTO.getId());
        // 功能是否开启
        String authEnabled = configService.getConfigValue(ConfigEnum.ADMIN_USER_AUTH_ENABLED);
        ValidatorUtil.isTrue(CharSequenceUtil.isNotBlank(authEnabled) && CharSequenceUtil.equals(Constants.COMMON_JUDGMENT.YES, authEnabled),
                ResultCodeEnum.USER_REAL_NAME_AUTH_NOT_ENABLED_ERROR);
        UserDTO userDTO = userService.getUser(personDTO.getId());
        ValidatorUtil.notEmpty(userDTO, ResultCodeEnum.USER_NOT_EXIST);
        String realName = EncryptUtil.sm4Decrypt(EncryptProperties.sm4Key, personDTO.getRealName());
        String idNumber = EncryptUtil.sm4Decrypt(EncryptProperties.sm4Key, personDTO.getIdNumber());
        Map<String, String> headersMap = new HashMap<>(4);
        headersMap.put(Header.AUTHORIZATION.getValue(), "APPCODE " + appCode);
        headersMap.put(Header.CONTENT_TYPE.getValue(), ContentType.FORM_URLENCODED.getValue());
        HashMap<String, String> paramsMap = new HashMap<>(4);
        paramsMap.put("idNo", idNumber);
        paramsMap.put("name", realName);
        String response = HttpUtil.HTTP_CLIENT.post(url, headersMap, paramsMap, Boolean.FALSE);
        ValidatorUtil.notEmpty(response, ResultCodeEnum.USER_REAL_NAME_AUTH_ERROR);
        JsonNode jsonNode = JsonUtil.fromJson(response);
        ValidatorUtil.notEmpty(jsonNode, ResultCodeEnum.USER_REAL_NAME_AUTH_ERROR);
        ValidatorUtil.isTrue(CharSequenceUtil.equals(JsonUtil.getNodeStr(jsonNode, "respCode"), "0000"), ResultCodeEnum.USER_REAL_NAME_AUTH_ERROR);
        userDTO.setRealNameAuth(Constants.COMMON_JUDGMENT.ZONE_STR);
        userDTO.setRealName(realName);
        userDTO.setIdNumber(idNumber);
        userService.updateUser(userDTO);
    }

}
