package top.horsemuzi.system.manager.admin;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.smallbun.screw.core.Configuration;
import cn.smallbun.screw.core.engine.EngineConfig;
import cn.smallbun.screw.core.engine.EngineFileType;
import cn.smallbun.screw.core.engine.EngineTemplateType;
import cn.smallbun.screw.core.execute.DocumentationExecute;
import cn.smallbun.screw.core.process.ProcessConfig;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.enums.generate.AutoFillEnum;
import top.horsemuzi.common.enums.generate.FormTypeEnum;
import top.horsemuzi.common.enums.generate.IdTypeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.CommonCodeUtil;
import top.horsemuzi.common.util.EncryptUtil;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.config.properties.EncryptProperties;
import top.horsemuzi.system.config.properties.SystemProperties;
import top.horsemuzi.system.core.generate.init.VelocityInitializer;
import top.horsemuzi.system.core.generate.util.VelocityUtil;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.dict.DictTypeDTO;
import top.horsemuzi.system.pojo.dto.generate.*;
import top.horsemuzi.system.pojo.dto.permission.PermissionDTO;
import top.horsemuzi.system.pojo.dto.role.RoleDTO;
import top.horsemuzi.system.service.*;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * 代码生成模块
 *
 * @author mabin
 * @date 2022/11/29 14:23
 **/
@Slf4j
@Component
@RequiredArgsConstructor
public class GenerateManager {


    private final TableService tableService;
    private final TableColumnService tableColumnService;
    private final TableConfigService tableConfigService;
    private final DatasourceService datasourceService;
    private final ColumnConfigService columnConfigService;
    private final PermissionService permissionService;
    private final RoleService roleService;
    private final DictTypeService dictTypeService;

    /**
     * 分页获取数据源列表
     *
     * @param query
     * @return
     */
    public Page<DatasourceDTO> getPageDatasource(DataSourceQuery query) {
        Page<DatasourceDTO> page = datasourceService.getPageDatasource(query);
        List<DatasourceDTO> datasourceDTOList = page.getRecords();
        datasourceDTOList.forEach(item -> item.setConnPassword(EncryptUtil.rsaEncrypt(EncryptProperties.rsaPublicKey, item.getConnPassword())));
        page.setRecords(datasourceDTOList);
        return page;
    }

    /**
     * 获取数据源详情
     *
     * @param datasourceId
     * @return
     */
    public DatasourceDTO getDatasource(Long datasourceId) {
        ValidatorUtil.notEmpty(datasourceId);
        DatasourceDTO datasourceDTO = datasourceService.getDatasource(datasourceId);
        datasourceDTO.setConnPassword(EncryptUtil.rsaEncrypt(EncryptProperties.rsaPublicKey, datasourceDTO.getConnPassword()));
        return datasourceDTO;
    }

    /**
     * 新增数据源
     *
     * @param datasourceDTO
     */
    public void addDataSource(DatasourceDTO datasourceDTO) {
        ValidatorUtil.notEmpty(datasourceDTO);
        ValidatorUtil.isTrue(datasourceService.checkDatasourceRepeat(datasourceDTO), ResultCodeEnum.GENERATE_DATASOURCE_REPEAT_ERROR);
        datasourceDTO.setConnPassword(EncryptUtil.rsaDecrypt(EncryptProperties.rsaPrivateKey, datasourceDTO.getConnPassword()));
        // 数据源连接测试
        try (Connection connection = datasourceService.connectDataSource(datasourceDTO)) {
        } catch (Exception e) {
            throw new BusinessException(e, ResultCodeEnum.GENERATE_DATASOURCE_CONNECT_ERROR);
        }
        // 保存数据源
        datasourceService.addDataSource(datasourceDTO);
    }

    /**
     * 更新数据源
     *
     * @param datasourceDTO
     */
    public void updateDataSource(DatasourceDTO datasourceDTO) {
        ValidatorUtil.notEmpty(datasourceDTO);
        ValidatorUtil.notEmpty(datasourceDTO.getId());
        ValidatorUtil.notEmpty(datasourceService.getDatasource(datasourceDTO.getId()), ResultCodeEnum.GENERATE_DATASOURCE_NOT_EXIST_ERROR);
        ValidatorUtil.isTrue(datasourceService.checkDatasourceRepeat(datasourceDTO), ResultCodeEnum.GENERATE_DATASOURCE_REPEAT_ERROR);
        datasourceDTO.setConnPassword(EncryptUtil.rsaDecrypt(EncryptProperties.rsaPrivateKey, datasourceDTO.getConnPassword()));
        // 数据源连接测试
        datasourceService.connectDataSource(datasourceDTO);
        // 更新数据源
        datasourceService.updateDataSource(datasourceDTO);
    }

    /**
     * 删除数据源
     *
     * @param datasourceIds
     */
    public void deleteDataSource(List<Long> datasourceIds) {
        ValidatorUtil.notEmpty(datasourceIds);
        List<DatasourceDTO> datasourceDTOList = datasourceService.getDatasource(datasourceIds);
        ValidatorUtil.isTrue(CollUtil.isNotEmpty(datasourceDTOList) && datasourceDTOList.size() == datasourceIds.size(), ResultCodeEnum.GENERATE_DATASOURCE_NOT_EXIST_ERROR);
        // 是否存在业务表关联
        ValidatorUtil.isEmpty(tableService.getTable(new TableQuery().setDatasourceIds(datasourceIds)), ResultCodeEnum.GENERATE_DATASOURCE_RELATION_TABLE_ERROR);
        datasourceService.deleteDataSource(datasourceIds);
    }

    /**
     * 获取数据源的所有数据库名称
     *
     * @param datasourceDTO
     * @return
     */
    public List<String> getDatabases(DatasourceDTO datasourceDTO) {
        ValidatorUtil.notEmpty(datasourceDTO);
        datasourceDTO.setConnPassword(EncryptUtil.rsaDecrypt(EncryptProperties.rsaPrivateKey, datasourceDTO.getConnPassword()));
        return datasourceService.getDatabases(datasourceDTO);
    }

    /**
     * 分页获取字段类型关联列表
     *
     * @param query
     * @return
     */
    public Page<ColumnConfigDTO> getPageColumnConfig(ColumnConfigQuery query) {
        return columnConfigService.getPageColumnConfig(query);
    }

    /**
     * 获取字段类型关联详情
     *
     * @param columnConfigId
     * @return
     */
    public ColumnConfigDTO getColumnConfig(Long columnConfigId) {
        ValidatorUtil.notEmpty(columnConfigId);
        return columnConfigService.getColumnConfig(columnConfigId);
    }

    /**
     * 新增字段类型关联
     *
     * @param columnConfigDTO
     */
    public void addColumnConfig(ColumnConfigDTO columnConfigDTO) {
        ValidatorUtil.notEmpty(columnConfigDTO);
        ValidatorUtil.isTrue(columnConfigService.checkColumnConfigRepeat(columnConfigDTO), ResultCodeEnum.GENERATE_COLUMN_CONFIG_REPEAT_ERROR);
        columnConfigService.addColumnConfig(columnConfigDTO);
    }

    /**
     * 更新字段类型关联
     *
     * @param columnConfigDTO
     */
    public void updateColumnConfig(ColumnConfigDTO columnConfigDTO) {
        ValidatorUtil.notEmpty(columnConfigDTO);
        ValidatorUtil.notEmpty(columnConfigDTO.getId());
        ValidatorUtil.notEmpty(columnConfigService.getColumnConfig(columnConfigDTO.getId()), ResultCodeEnum.GENERATE_COLUMN_CONFIG_NOT_EXIST_ERROR);
        ValidatorUtil.isTrue(columnConfigService.checkColumnConfigRepeat(columnConfigDTO), ResultCodeEnum.GENERATE_COLUMN_CONFIG_REPEAT_ERROR);
        columnConfigService.updateColumnConfig(columnConfigDTO);
    }

    /**
     * 删除字段类型关联
     *
     * @param columnConfigIds
     */
    public void deleteColumnConfig(List<Long> columnConfigIds) {
        ValidatorUtil.notEmpty(columnConfigIds);
        columnConfigService.deleteColumnConfig(columnConfigIds);
    }

    /**
     * 删除业务表配置
     *
     * @param tableConfigIds
     */
    public void deleteTableConfig(List<Long> tableConfigIds) {
        ValidatorUtil.notEmpty(tableConfigIds);
        List<TableConfigDTO> tableConfigDTOList = tableConfigService.getTableConfig(tableConfigIds);
        ValidatorUtil.isTrue(CollUtil.isNotEmpty(tableConfigDTOList) && tableConfigDTOList.size() == tableConfigIds.size(),
                ResultCodeEnum.GENERATE_TABLE_CONFIG_NOT_EXIST_ERROR);
        ValidatorUtil.isEmpty(tableService.getTable(new TableQuery().setTableConfigIds(tableConfigIds)),
                ResultCodeEnum.GENERATE_TABLE_CONFIG_RELATION_TABLE_ERROR);
        tableConfigService.deleteTableConfig(tableConfigIds);
    }

    /**
     * 更新业务表配置
     *
     * @param tableConfigDTO
     */
    public void updateTableConfig(TableConfigDTO tableConfigDTO) {
        ValidatorUtil.notEmpty(tableConfigDTO);
        ValidatorUtil.notEmpty(tableConfigDTO.getId());
        // Entity父类路径各baseField基类字段两个值不能通知存在
        ValidatorUtil.notTrue(StrUtil.isAllNotBlank(tableConfigDTO.getBaseField(), tableConfigDTO.getEntitySupperClass()));
        ValidatorUtil.notEmpty(tableConfigService.getTableConfig(tableConfigDTO.getId()),
                ResultCodeEnum.GENERATE_TABLE_CONFIG_NOT_EXIST_ERROR);
        ValidatorUtil.isTrue(tableConfigService.checkTableConfigRepeat(tableConfigDTO),
                ResultCodeEnum.GENERATE_COLUMN_CONFIG_REPEAT_ERROR);
        // 校验业务表自定义宽松配置
        checkTableConfig(tableConfigDTO);
        tableConfigService.updateTableConfig(tableConfigDTO);
    }

    /**
     * 新增业务表配置
     *
     * @param tableConfigDTO
     */
    public void addTableConfig(TableConfigDTO tableConfigDTO) {
        ValidatorUtil.notEmpty(tableConfigDTO);
        // Entity父类路径和baseField基类字段两个值不能同时存在
        ValidatorUtil.notTrue(StrUtil.isAllNotBlank(tableConfigDTO.getBaseField(), tableConfigDTO.getEntitySupperClass()));
        ValidatorUtil.isTrue(tableConfigService.checkTableConfigRepeat(tableConfigDTO), ResultCodeEnum.GENERATE_COLUMN_CONFIG_REPEAT_ERROR);
        // 校验业务表自定义宽松配置
        checkTableConfig(tableConfigDTO);
        tableConfigService.addTableConfig(tableConfigDTO);
    }

    /**
     * 校验业务表自定义宽松配置
     *
     * @param tableConfigDTO
     */
    private void checkTableConfig(TableConfigDTO tableConfigDTO) {
        // 设置自动去除表前缀
        if (StrUtil.equals(Constants.COMMON_JUDGMENT.ZONE_STR, tableConfigDTO.getRemovePrefix())) {
            ValidatorUtil.notEmpty(tableConfigDTO.getPrefix());
        }
        // 校验各父类路径是否存在
        try {
            if (StrUtil.isNotBlank(tableConfigDTO.getControllerSupperClass())) {
                Class.forName(tableConfigDTO.getControllerSupperClass());
            }
            if (StrUtil.isNotBlank(tableConfigDTO.getEntitySupperClass())) {
                Class.forName(tableConfigDTO.getEntitySupperClass());
                // 如果Entity基类存在则置空baseField字段
                tableConfigDTO.setBaseField(StrUtil.EMPTY);
            }
            if (StrUtil.isNotBlank(tableConfigDTO.getQuerySupperClass())) {
                Class.forName(tableConfigDTO.getQuerySupperClass());
            }
        } catch (ClassNotFoundException e) {
            throw new BusinessException(e, ResultCodeEnum.GENERATE_TABLE_CONFIG_SUPPLER_CLASS_NOT_EXIST_ERROR);
        }
        // 校验各文件格式化名称是否符合要求: 必须包含%s填充字符
        int fillCharLength = "%s".length();
        if (StrUtil.isNotBlank(tableConfigDTO.getFormatControllerFileName())) {
            ValidatorUtil.isTrue(StrUtil.contains(tableConfigDTO.getFormatControllerFileName(), "%s"), ResultCodeEnum.GENERATE_TABLE_CONFIG_FILE_FORMAT_NAME_ERROR);
            ValidatorUtil.isTrue(Constants.COMMON_CODE.BASE_INT_ONE == (tableConfigDTO.getFormatControllerFileName().length()
                            - tableConfigDTO.getFormatControllerFileName().replaceAll("%s", StrUtil.EMPTY).length()) / fillCharLength,
                    ResultCodeEnum.GENERATE_TABLE_CONFIG_FILE_FORMAT_NAME_ERROR);
        }
        if (StrUtil.isNotBlank(tableConfigDTO.getFormatServiceFileName())) {
            ValidatorUtil.isTrue(StrUtil.contains(tableConfigDTO.getFormatServiceFileName(), "%s"), ResultCodeEnum.GENERATE_TABLE_CONFIG_FILE_FORMAT_NAME_ERROR);
            ValidatorUtil.isTrue(Constants.COMMON_CODE.BASE_INT_ONE == (tableConfigDTO.getFormatServiceFileName().length()
                            - tableConfigDTO.getFormatServiceFileName().replaceAll("%s", StrUtil.EMPTY).length()) / fillCharLength,
                    ResultCodeEnum.GENERATE_TABLE_CONFIG_FILE_FORMAT_NAME_ERROR);
        }
        if (StrUtil.isNotBlank(tableConfigDTO.getFormatServiceImplFileName())) {
            ValidatorUtil.isTrue(StrUtil.contains(tableConfigDTO.getFormatServiceImplFileName(), "%s"), ResultCodeEnum.GENERATE_TABLE_CONFIG_FILE_FORMAT_NAME_ERROR);
            ValidatorUtil.isTrue(Constants.COMMON_CODE.BASE_INT_ONE == (tableConfigDTO.getFormatServiceImplFileName().length()
                            - tableConfigDTO.getFormatServiceImplFileName().replaceAll("%s", StrUtil.EMPTY).length()) / fillCharLength,
                    ResultCodeEnum.GENERATE_TABLE_CONFIG_FILE_FORMAT_NAME_ERROR);
        }
        if (StrUtil.isNotBlank(tableConfigDTO.getFormatMapperFileName())) {
            ValidatorUtil.isTrue(StrUtil.contains(tableConfigDTO.getFormatMapperFileName(), "%s"), ResultCodeEnum.GENERATE_TABLE_CONFIG_FILE_FORMAT_NAME_ERROR);
            ValidatorUtil.isTrue(Constants.COMMON_CODE.BASE_INT_ONE == (tableConfigDTO.getFormatMapperFileName().length()
                            - tableConfigDTO.getFormatMapperFileName().replaceAll("%s", StrUtil.EMPTY).length()) / fillCharLength,
                    ResultCodeEnum.GENERATE_TABLE_CONFIG_FILE_FORMAT_NAME_ERROR);
        }
        if (StrUtil.isNotBlank(tableConfigDTO.getFormatXmlFileName())) {
            ValidatorUtil.isTrue(StrUtil.contains(tableConfigDTO.getFormatXmlFileName(), "%s"), ResultCodeEnum.GENERATE_TABLE_CONFIG_FILE_FORMAT_NAME_ERROR);
            ValidatorUtil.isTrue(Constants.COMMON_CODE.BASE_INT_ONE == (tableConfigDTO.getFormatXmlFileName().length()
                            - tableConfigDTO.getFormatXmlFileName().replaceAll("%s", StrUtil.EMPTY).length()) / fillCharLength,
                    ResultCodeEnum.GENERATE_TABLE_CONFIG_FILE_FORMAT_NAME_ERROR);
        }
        // 日期格式化模板字符串是否有效
        if (StrUtil.isNotBlank(tableConfigDTO.getFormatCommentDate())) {
            try {
                new SimpleDateFormat(tableConfigDTO.getFormatCommentDate());
            } catch (IllegalArgumentException e) {
                throw new BusinessException(ResultCodeEnum.GENERATE_TABLE_CONFIG_DATE_FORMAT_INVALID_ERROR);
            }
        }
    }

    /**
     * 获取业务表配置详情
     *
     * @param tableConfigId
     * @return
     */
    public TableConfigDTO getTableConfig(Long tableConfigId) {
        ValidatorUtil.notEmpty(tableConfigId);
        return tableConfigService.getTableConfig(tableConfigId);
    }

    /**
     * 分页获取业务表配置列表
     *
     * @param query
     * @return
     */
    public Page<TableConfigDTO> getPageTableConfig(TableConfigQuery query) {
        return tableConfigService.getPageTableConfig(query);
    }

    /**
     * 连接数据源
     *
     * @param datasourceDTO
     */
    public void connectDataSource(DatasourceDTO datasourceDTO) {
        ValidatorUtil.notEmpty(datasourceDTO);
        datasourceDTO.setConnPassword(EncryptUtil.rsaDecrypt(EncryptProperties.rsaPrivateKey, datasourceDTO.getConnPassword()));
        try (Connection connection = datasourceService.connectDataSource(datasourceDTO)) {
        } catch (Exception e) {
            throw new BusinessException(e, ResultCodeEnum.GENERATE_DATASOURCE_CONNECT_ERROR);
        }
    }

    /**
     * 分页获取业务表列表
     *
     * @param query
     * @return
     */
    public Page<TableDTO> getPageTable(TableQuery query) {
        return tableService.getPageTable(query);
    }

    /**
     * 获取业务表详情
     *
     * @param tableId
     * @return
     */
    public TableDTO getTable(Long tableId) {
        ValidatorUtil.notEmpty(tableId);
        TableDTO tableDTO = tableService.getTable(tableId);
        ValidatorUtil.notEmpty(tableDTO, ResultCodeEnum.GENERATE_TABLE_NOT_EXIST_ERROR);
        // 数据源信息
        DatasourceDTO datasourceDTO = datasourceService.getDatasource(tableDTO.getDatasourceId());
        ValidatorUtil.notEmpty(datasourceDTO, ResultCodeEnum.GENERATE_DATASOURCE_NOT_EXIST_ERROR);
        tableDTO.setDatasourceDTO(datasourceDTO);
        // 业务表配置信息
        TableConfigDTO tableConfigDTO = tableConfigService.getTableConfig(tableDTO.getTableConfigId());
        ValidatorUtil.notEmpty(tableConfigDTO, ResultCodeEnum.GENERATE_TABLE_CONFIG_NOT_EXIST_ERROR);
        tableDTO.setTableConfigDTO(tableConfigDTO);
        return tableDTO;
    }

    /**
     * 新增业务表
     *
     * @param tableDTO
     */
    @Transactional(rollbackFor = Exception.class)
    public void addTable(TableDTO tableDTO) {
        // 校验业务表参数
        ValidatorUtil.notEmpty(tableDTO);
        ValidatorUtil.allNotEmpty(tableDTO.getDatasourceId(), tableDTO.getTableConfigId(), tableDTO.getParentMenuId(), tableDTO.getImportType());
        // 批量添加的业务表
        List<TableDTO> tableDTOList = new ArrayList<>();
        // 当前数据源、数据库的所有业务表信息
        List<SchemaTableDTO> schemaTables = new ArrayList<>();
        // 基于DB导入
        if (StrUtil.equals(Constants.COMMON_JUDGMENT.ZONE_STR, tableDTO.getImportType()) && StrUtil.isNotBlank(tableDTO.getTableName())) {
            if (StrUtil.contains(tableDTO.getTableName(), StrUtil.COMMA)) {
                List<String> tableNameList = new ArrayList<>(StrUtil.split(tableDTO.getTableName(), StrUtil.COMMA));
                TableDTO copyTableDTO = null;
                for (String tableName : tableNameList) {
                    copyTableDTO = OrikaUtil.convert(tableDTO, TableDTO.class);
                    // 重新设置表明、表描、功能描述
                    copyTableDTO.setTableName(tableName);
                    copyTableDTO.setTableComment(StrUtil.EMPTY);
                    copyTableDTO.setFunctionName(StrUtil.EMPTY);
                    tableDTOList.add(copyTableDTO);
                }
            } else {
                tableDTOList.add(tableDTO);
            }
            schemaTables = datasourceService.getSchemaTables(tableDTO.getDatabaseName(), tableDTO.getDatasourceId());
        } else {
            // 基于DDL导入
            ValidatorUtil.notBlank(tableDTO.getTableDdl());
            // 获取当前数据库的所有表名
            List<SchemaTableDTO> schemaTablesDdlBefore = datasourceService.getSchemaTables(tableDTO.getDatabaseName(), tableDTO.getDatasourceId());
            // 执行DDL建表语句
            if (datasourceService.execTableDDL(tableDTO.getDatasourceId(), tableDTO.getDatabaseName(), tableDTO.getTableDdl())) {
                // 再次获取当前数据库的所有表名
                schemaTables = datasourceService.getSchemaTables(tableDTO.getDatabaseName(), tableDTO.getDatasourceId());
                // 差集获取当前DDL语句添加的表名信息
                List<String> ddlBeforeTableNameList = schemaTablesDdlBefore.stream().map(SchemaTableDTO::getTableName).collect(Collectors.toList());
                TableDTO copyTableDTO = null;
                for (SchemaTableDTO schemaTable : schemaTables) {
                    if (!ddlBeforeTableNameList.contains(schemaTable.getTableName())) {
                        copyTableDTO = OrikaUtil.convert(tableDTO, TableDTO.class);
                        // 重新设置表明、表描、功能描述
                        copyTableDTO.setTableName(schemaTable.getTableName());
                        copyTableDTO.setTableComment(schemaTable.getTableComment());
                        copyTableDTO.setFunctionName(StrUtil.EMPTY);
                        tableDTOList.add(copyTableDTO);
                    }
                }
            }
        }
        // 遍历处理表
        if (CollUtil.isNotEmpty(tableDTOList)) {
            for (TableDTO copyTableDTO : tableDTOList) {
                TableConfigDTO tableConfigDTO = checkTable(copyTableDTO);
                // 组装业务表相关信息
                // 设置表描述和表功能名称
                if (StrUtil.isBlank(copyTableDTO.getTableComment())) {
                    List<SchemaTableDTO> schemaTableDTOList = schemaTables.stream()
                            .filter(item -> StrUtil.equals(copyTableDTO.getTableName(), item.getTableName()))
                            .collect(Collectors.toList());
                    ValidatorUtil.notEmpty(schemaTableDTOList, ResultCodeEnum.GENERATE_TABLE_NOT_EXIST_ERROR);
                    SchemaTableDTO schemaTableDTO = schemaTableDTOList.get(0);
                    copyTableDTO.setTableComment(schemaTableDTO.getTableComment());
                }
                if (StrUtil.isBlank(copyTableDTO.getFunctionName())) {
                    copyTableDTO.setFunctionName(StrUtil.subBefore(copyTableDTO.getTableComment(), "表", true));
                }
                // 设置类名(是否去除表前缀)
                String finalTableName = StrUtil.EMPTY;
                if (StrUtil.equals(Constants.COMMON_JUDGMENT.ZONE_STR, tableConfigDTO.getRemovePrefix()) && StrUtil.isNotBlank(tableConfigDTO.getPrefix())) {
                    List<String> prefixList = StrUtil.split(tableConfigDTO.getPrefix(), StrUtil.COMMA);
                    for (String prefix : prefixList) {
                        finalTableName = StrUtil.removePrefix(copyTableDTO.getTableName(), prefix);
                    }
                } else {
                    finalTableName = copyTableDTO.getTableName();
                }
                copyTableDTO.setClassName(StrUtil.upperFirst(StrUtil.toCamelCase(finalTableName)));
                // 保存业务表
                tableService.addTable(copyTableDTO);
                // 组装业务表字段信息
                generateTableColumn(copyTableDTO, tableConfigDTO);
            }
        }
    }

    /**
     * 代码生成同步表结构
     * 逻辑: 业务表结构如果有更新, 则可以同步表机构, 重新生成业务代码
     *
     * @param tableId
     */
    @Transactional(rollbackFor = Exception.class)
    public void sync(Long tableId) {
        ValidatorUtil.notEmpty(tableId);
        // 业务表
        TableDTO tableDTO = tableService.getTable(tableId);
        ValidatorUtil.notEmpty(tableDTO, ResultCodeEnum.GENERATE_TABLE_NOT_EXIST_ERROR);
        // 业务表配置
        TableConfigDTO tableConfigDTO = tableConfigService.getTableConfig(tableDTO.getTableConfigId());
        ValidatorUtil.notEmpty(tableConfigDTO, ResultCodeEnum.GENERATE_TABLE_CONFIG_NOT_EXIST_ERROR);
        // 组装业务表字段信息
        generateTableColumn(tableDTO, tableConfigDTO);
    }

    /**
     * 组装业务表字段信息
     *
     * @param tableDTO
     * @param tableConfigDTO
     */
    private void generateTableColumn(TableDTO tableDTO, TableConfigDTO tableConfigDTO) {
        List<SchemaColumnDTO> schemaColumns = datasourceService.getSchemaColumns(tableDTO.getDatasourceId(), tableDTO.getDatabaseName(), tableDTO.getTableName());
        ValidatorUtil.notEmpty(schemaColumns, ResultCodeEnum.GENERATE_TABLE_COLUMN_NOT_EXIST_ERROR);
        // 获取字段类型关联信息
        List<ColumnConfigDTO> columnConfigDTOList = columnConfigService.getColumnConfig();
        ValidatorUtil.notEmpty(columnConfigDTOList, ResultCodeEnum.GENERATE_COLUMN_CONFIG_NOT_EXIST_ERROR);
        Map<String, ColumnConfigDTO> columnConfigMap = columnConfigDTOList.stream().collect(Collectors.toMap(ColumnConfigDTO::getColumnType, item -> item, (v1, v2) -> v1));

        // 组装具体字段信息
        List<TableColumnDTO> tableColumnDTOList = new ArrayList<>();
        TableColumnDTO tableColumnDTO = null;
        // 基础字段信息
        List<String> baseFieldList = new ArrayList<>();
        if (StrUtil.isNotBlank(tableConfigDTO.getBaseField())) {
            baseFieldList = new ArrayList<>(StrUtil.split(tableConfigDTO.getBaseField(), StrUtil.COMMA));
        }
        // 基类字段列表
        List<String> baseEntityFieldList = new ArrayList<>();
        if (StrUtil.isNotBlank(tableConfigDTO.getEntitySupperClass())) {
            try {
                Arrays.stream(Class.forName(tableConfigDTO.getEntitySupperClass()).getDeclaredFields())
                        .forEach(field -> baseEntityFieldList.add(field.getName()));
            } catch (ClassNotFoundException e) {
                ValidatorUtil.exception(ResultCodeEnum.GENERATE_TABLE_CONFIG_SUPPLER_CLASS_NOT_EXIST_ERROR);
            }
        }
        for (SchemaColumnDTO schemaColumn : schemaColumns) {
            String columnName = schemaColumn.getColumnName();
            // 基类字段直接跳过
            if (CollUtil.isNotEmpty(baseEntityFieldList) && baseEntityFieldList.contains(columnName)) {
                continue;
            }
            tableColumnDTO = new TableColumnDTO();
            tableColumnDTO.setTableId(tableDTO.getId());
            tableColumnDTO.setColumnName(columnName);
            tableColumnDTO.setColumnType(schemaColumn.getColumnType());
            tableColumnDTO.setColumnComment(schemaColumn.getColumnComment());
            tableColumnDTO.setSort(schemaColumn.getOrdinalPosition());
            // Java字段名称是否自动去除is前缀(忽略大小写)
            if (StrUtil.startWithIgnoreCase(columnName, "is_") && StrUtil.equals(Constants.COMMON_JUDGMENT.ZONE_STR, tableConfigDTO.getRemoveIsPrefix())) {
                tableColumnDTO.setJavaName(StrUtil.toCamelCase(StrUtil.removePrefixIgnoreCase(columnName, "is_")));
            } else {
                tableColumnDTO.setJavaName(StrUtil.toCamelCase(columnName));
            }
            tableColumnDTO.setJavaNameUp(StrUtil.upperFirst(tableColumnDTO.getJavaName()));
            // 设置Java字段类型和属性包名
            ColumnConfigDTO columnConfigDTO = columnConfigMap.get(schemaColumn.getDataType());
            if (ObjectUtil.isNotNull(columnConfigDTO)) {
                tableColumnDTO.setJavaType(columnConfigDTO.getPropertyType());
                tableColumnDTO.setPackageName(columnConfigDTO.getPackageName());
            }
            // 是否主键字段及设置主键id为不导出字段
            if (StrUtil.equals("PRI", schemaColumn.getColumnKey())) {
                tableColumnDTO.setPkField(Constants.COMMON_JUDGMENT.ZONE_STR);
                tableColumnDTO.setWhetherExport(Constants.COMMON_JUDGMENT.ONE_STR);
            }
            // 设置字段是否导出
            if (StrUtil.equals(tableConfigDTO.getWhetherExport(), Constants.COMMON_JUDGMENT.ONE_STR)) {
                tableColumnDTO.setWhetherExport(Constants.COMMON_JUDGMENT.ONE_STR);
            }
            // 设置自动填充项的值和默认不导出字段
            if (CollUtil.isNotEmpty(baseFieldList) && baseFieldList.contains(schemaColumn.getColumnName())) {
                if (StrUtil.contains(schemaColumn.getColumnName(), "create")) {
                    tableColumnDTO.setAutoFill(AutoFillEnum.INSERT.getText());
                } else if (StrUtil.contains(schemaColumn.getColumnName(), "modified")) {
                    tableColumnDTO.setAutoFill(AutoFillEnum.INSERT_UPDATE.getText());
                }
                tableColumnDTO.setWhetherExport(Constants.COMMON_JUDGMENT.ONE_STR);
            }
            // 设置表单项
            if (StrUtil.equals(Constants.COMMON_JUDGMENT.NO, schemaColumn.getIsNullable())
                    && !StrUtil.equals("PRI", schemaColumn.getColumnKey())
                    && !StrUtil.endWith(schemaColumn.getColumnName(), "_id")
                    && !baseFieldList.contains(schemaColumn.getColumnName())
                    && !StrUtil.equalsAny(schemaColumn.getColumnName(), tableConfigDTO.getLogicDeletedName(), tableConfigDTO.getVersionName())) {
                // 设置表单是否必填、必填校验内容、是否可清理、占位符内容
                tableColumnDTO.setFormItem(Constants.COMMON_JUDGMENT.ZONE_STR);
                tableColumnDTO.setFormRequired(Constants.COMMON_JUDGMENT.ZONE_STR);
                tableColumnDTO.setFormValidator(schemaColumn.getColumnComment() + "不能为空");
                tableColumnDTO.setFormClearable(Constants.COMMON_JUDGMENT.ZONE_STR);
                tableColumnDTO.setFormPlaceholder("请输入" + schemaColumn.getColumnComment());
                // 设置表单类型
                if (StrUtil.equalsAnyIgnoreCase(schemaColumn.getDataType(), "tinytext", "text", "mediumtext", "longtext")
                        || StrUtil.equalsAny(schemaColumn.getColumnName(), "remark")) {
                    tableColumnDTO.setFormType(FormTypeEnum.TEXTAREA.getType());
                } else {
                    tableColumnDTO.setFormType(FormTypeEnum.INPUT.getType());
                }
                // 设置表单最大字符限制
                if (StrUtil.equalsAnyIgnoreCase(schemaColumn.getDataType(), "char", "varchar")) {
                    tableColumnDTO.setFormCharMaxLength(schemaColumn.getCharacterMaximumLength());
                }
            }
            // 设置列表项
            if (!StrUtil.equals("PRI", schemaColumn.getColumnKey())
                    && !StrUtil.endWith(schemaColumn.getColumnName(), "_id")
                    && !StrUtil.equalsAny(schemaColumn.getColumnName(), tableConfigDTO.getLogicDeletedName(), tableConfigDTO.getVersionName())) {
                tableColumnDTO.setGridItem(Constants.COMMON_JUDGMENT.ZONE_STR);
                // 是否创建时间(gmt_create)字段
                if (baseFieldList.contains(schemaColumn.getColumnName()) && StrUtil.equals(schemaColumn.getColumnName(), "gmt_create")) {
                    tableColumnDTO.setGridSort(Constants.COMMON_JUDGMENT.ZONE_STR);
                    tableColumnDTO.setGridWidth(160);
                }
            }
            tableColumnDTOList.add(tableColumnDTO);
        }
        // 保存业务表字段 (兼容同步表结构操作: 先删除再新增)
        List<TableColumnDTO> existTableColumnDTOList = tableColumnService.getTableColumn(tableDTO.getId());
        if (CollUtil.isNotEmpty(existTableColumnDTOList)) {
            List<Long> tableColumnIds = existTableColumnDTOList.stream().map(TableColumnDTO::getId).collect(Collectors.toList());
            tableColumnService.deleteTableColumn(tableColumnIds);
        }
        tableColumnService.addTableColumn(tableColumnDTOList);
    }


    /**
     * 校验业务表参数
     *
     * @param tableDTO
     * @return
     */
    private TableConfigDTO checkTable(TableDTO tableDTO) {
        // 业务表是否重复(表名)
        ValidatorUtil.isTrue(tableService.checkTableRepeat(tableDTO), ResultCodeEnum.GENERATE_TABLE_REPEAT_ERROR);
        // 数据源信息是否存在
        DatasourceDTO datasourceDTO = datasourceService.getDatasource(tableDTO.getDatasourceId());
        ValidatorUtil.notEmpty(datasourceDTO, ResultCodeEnum.GENERATE_DATASOURCE_NOT_EXIST_ERROR);
        // 业务表配置信息是否存在
        TableConfigDTO tableConfigDTO = tableConfigService.getTableConfig(tableDTO.getTableConfigId());
        ValidatorUtil.notEmpty(tableConfigDTO, ResultCodeEnum.GENERATE_TABLE_CONFIG_NOT_EXIST_ERROR);
        // 权限菜单是否存在
        ValidatorUtil.notEmpty(permissionService.getPermission(tableDTO.getParentMenuId()), ResultCodeEnum.PERMISSION_NOT_EXIST);
        // 数据库是否存在
        List<String> databases = datasourceService.getDatabases(datasourceDTO);
        ValidatorUtil.isTrue(CollUtil.isNotEmpty(databases) && databases.contains(tableDTO.getDatabaseName()), ResultCodeEnum.GENERATE_DATABASE_NOT_EXIST_ERROR);
        // 角色标识是否存在
        if (StrUtil.isNotBlank(tableDTO.getRoleCode())) {
            ValidatorUtil.notEmpty(roleService.getRolesByCodes(Collections.singletonList(tableDTO.getRoleCode())), ResultCodeEnum.ROLE_NOT_EXIST);
        }
        return tableConfigDTO;
    }

    /**
     * 更新业务表
     *
     * @param tableDTO
     */
    public void updateTable(TableDTO tableDTO) {
        // 校验业务表参数
        ValidatorUtil.notEmpty(tableDTO);
        ValidatorUtil.allNotEmpty(tableDTO.getId(), tableDTO.getDatasourceId(), tableDTO.getTableConfigId());
        TableConfigDTO tableConfigDTO = checkTable(tableDTO);
        // 组装业务表相关信息
        // 设置表描述和表功能名称
        if (StrUtil.isBlank(tableDTO.getTableComment())) {
            List<SchemaTableDTO> schemaTables = datasourceService.getSchemaTables(tableDTO.getDatabaseName(), tableDTO.getDatasourceId());
            ValidatorUtil.notEmpty(schemaTables, ResultCodeEnum.GENERATE_TABLE_NOT_EXIST_ERROR);
            List<SchemaTableDTO> schemaTableDTOList = schemaTables.stream().filter(item -> StrUtil.equals(tableDTO.getTableName(), item.getTableName())).collect(Collectors.toList());
            ValidatorUtil.isTrue(CollUtil.isNotEmpty(schemaTableDTOList) && schemaTableDTOList.size() == Constants.COMMON_CODE.BASE_INT_ONE, ResultCodeEnum.GENERATE_TABLE_NOT_EXIST_ERROR);
            SchemaTableDTO schemaTableDTO = schemaTableDTOList.get(0);
            tableDTO.setTableComment(schemaTableDTO.getTableComment());
        }
        if (StrUtil.isBlank(tableDTO.getFunctionName())) {
            tableDTO.setFunctionName(StrUtil.subBefore(tableDTO.getTableComment(), "表", true));
        }
        // 更新业务表
        tableService.updateTable(tableDTO);
    }

    /**
     * 删除业务表
     *
     * @param tableIds
     */
    @Transactional(rollbackFor = Exception.class)
    public void deleteTable(List<Long> tableIds) {
        ValidatorUtil.notEmpty(tableIds);
        tableService.deleteTable(tableIds);
        // 删除关联字段信息
        List<TableColumnDTO> tableColumnDTOList = tableColumnService.getTableColumn(tableIds);
        if (CollUtil.isNotEmpty(tableColumnDTOList)) {
            List<Long> tableColumnIds = tableColumnDTOList.stream().map(TableColumnDTO::getId).collect(Collectors.toList());
            tableColumnService.deleteTableColumn(tableColumnIds);
        }
    }

    /**
     * 代码生成预览
     *
     * @param tableId
     * @return
     */
    public Map<String, Object> preview(Long tableId) {
        ValidatorUtil.notEmpty(tableId);
        // 获取表信息(包含对应表字段信息)
        TableDTO tableDTO = tableService.getTable(tableId);
        ValidatorUtil.notEmpty(tableDTO, ResultCodeEnum.GENERATE_TABLE_NOT_EXIST_ERROR);
        // 组装业务表扩展信息
        generateTable(tableDTO);
        // 初始化模板
        VelocityInitializer.initVelocity();
        VelocityContext context = VelocityUtil.prepareContext(tableDTO);
        // 渲染模板文件
        List<String> templates = VelocityUtil.getTemplateList();
        StringWriter sw = null;
        Map<String, Object> codeMap = new LinkedHashMap<>(16);
        for (String template : templates) {
            sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, StandardCharsets.UTF_8.toString());
            tpl.merge(context, sw);
            codeMap.put(template, sw.toString());
        }
        return codeMap;
    }


    /**
     * 代码生成下载
     *
     * @param tableIds
     * @param response
     */
    public void download(List<Long> tableIds, HttpServletResponse response) {
        ValidatorUtil.notEmpty(tableIds);
        // 获取业务表信息
        List<TableDTO> tableDTOList = tableService.getTable(tableIds);
        ValidatorUtil.notEmpty(tableDTOList, ResultCodeEnum.GENERATE_TABLE_NOT_EXIST_ERROR);
        // 生成代码压缩包文件
        try (ZipOutputStream zipOutputStream = new ZipOutputStream(response.getOutputStream())) {
            CommonCodeUtil.generateAttachmentResponseHeader(response, "generate_code.zip");
            for (TableDTO tableDTO : tableDTOList) {
                // 生成代码
                generateCode(tableDTO, zipOutputStream);
            }
        } catch (Exception e) {
            throw new BusinessException(e, ResultCodeEnum.GENERATE_TABLE_GENERATE_CODE_ERROR);
        }
    }


    /**
     * 代码生成处理
     *
     * @param tableDTO
     * @param zipOutputStream
     */
    private void generateCode(TableDTO tableDTO, ZipOutputStream zipOutputStream) throws Exception {
        // 组装业务表扩展信息
        generateTable(tableDTO);
        // 初始化模板
        VelocityInitializer.initVelocity();
        VelocityContext context = VelocityUtil.prepareContext(tableDTO);
        // 渲染模板文件
        List<String> templates = VelocityUtil.getTemplateList();
        StringWriter sw = null;
        for (String template : templates) {
            sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, StandardCharsets.UTF_8.toString());
            tpl.merge(context, sw);
            try {
                // 添加到zip
                String templateFilePathName = VelocityUtil.getTemplateFilePathName(template, tableDTO);
                if (StrUtil.isBlank(templateFilePathName)) {
                    break;
                }
                zipOutputStream.putNextEntry(new ZipEntry(templateFilePathName));
                IoUtil.write(zipOutputStream, StandardCharsets.UTF_8, false, sw.toString());
                IoUtil.close(sw);
                zipOutputStream.flush();
                zipOutputStream.closeEntry();
            } catch (Exception e) {
                log.error("渲染模板失败，tableName={}", tableDTO.getTableName(), e);
                throw e;
            }
        }
    }

    /**
     * 组装业务表扩展信息
     *
     * @param tableDTO
     */
    private void generateTable(TableDTO tableDTO) {
        // 获取业务表字段列表
        List<TableColumnDTO> tableColumnDTOList = tableColumnService.getTableColumn(tableDTO.getId());
        ValidatorUtil.notEmpty(tableColumnDTOList, ResultCodeEnum.GENERATE_TABLE_COLUMN_NOT_EXIST_ERROR);

        // 设置业务表主键字段
        List<TableColumnDTO> pkColumnList = tableColumnDTOList.stream().filter(column -> StrUtil.equals(Constants.COMMON_JUDGMENT.ONE_STR, column.getPkField())).collect(Collectors.toList());
        if (CollUtil.isEmpty(pkColumnList) || pkColumnList.size() > Constants.COMMON_CODE.BASE_INT_ONE) {
            tableDTO.setPrimaryTableColumnDTO(tableColumnDTOList.get(0));
        } else {
            tableDTO.setPrimaryTableColumnDTO(pkColumnList.get(0));
        }

        // 设置引入基础依赖包信息
        Set<String> columnTypes = tableColumnDTOList.stream().map(TableColumnDTO::getColumnType).collect(Collectors.toSet());
        List<ColumnConfigDTO> columnConfigDTOList = columnConfigService.getColumnConfig(columnTypes);
        tableDTO.setImportPackages(columnConfigDTOList.stream().map(ColumnConfigDTO::getPackageName).collect(Collectors.toSet()));

        // 获取业务表配置信息
        TableConfigDTO tableConfigDTO = tableConfigService.getTableConfig(tableDTO.getTableConfigId());
        ValidatorUtil.notEmpty(tableConfigDTO, ResultCodeEnum.GENERATE_TABLE_CONFIG_NOT_EXIST_ERROR);
        // 设置基础父类包路径
        try {
            if (StrUtil.isNotBlank(tableConfigDTO.getControllerSupperClass())) {
                Class<?> controllerClazz = Class.forName(tableConfigDTO.getControllerSupperClass());
                tableConfigDTO.setControllerSupperClassPackage(controllerClazz.getName());
                tableConfigDTO.setControllerSupperClassName(controllerClazz.getSimpleName());
            }
            if (StrUtil.isNotBlank(tableConfigDTO.getEntitySupperClass())) {
                Class<?> entityClazz = Class.forName(tableConfigDTO.getEntitySupperClass());
                tableConfigDTO.setEntitySupperClassPackage(entityClazz.getName());
                tableConfigDTO.setEntitySupperClassName(entityClazz.getSimpleName());
                // 设置表字段的类型和名称是否属于Entity基类, 如果属于则不再entity文件中生成
                List<Field> fieldList = Arrays.stream(entityClazz.getDeclaredFields())
                        .filter(field -> !StrUtil.equals(field.getName(), "serialVersionUID"))
                        .collect(Collectors.toList());
                for (TableColumnDTO tableColumnDTO : tableColumnDTOList) {
                    for (Field field : fieldList) {
                        if (StrUtil.equals(tableColumnDTO.getJavaType(), field.getType().getSimpleName())
                                && StrUtil.equals(tableColumnDTO.getJavaName(), field.getName())) {
                            tableColumnDTO.setEntitySupperField(Boolean.TRUE);
                            break;
                        }
                    }
                }
            }
            if (StrUtil.isNotBlank(tableConfigDTO.getQuerySupperClass())) {
                Class<?> queryClazz = Class.forName(tableConfigDTO.getQuerySupperClass());
                tableConfigDTO.setQuerySupperClassPackage(queryClazz.getName());
                tableConfigDTO.setQuerySupperClassName(queryClazz.getSimpleName());
            }
        } catch (ClassNotFoundException e) {
            throw new BusinessException(e, ResultCodeEnum.GENERATE_TABLE_CONFIG_SUPPLER_CLASS_NOT_EXIST_ERROR);
        }
        tableDTO.setTableColumnDTOList(tableColumnDTOList);

        // 设置各文件格式化名称
        if (StrUtil.isNotBlank(tableConfigDTO.getFormatControllerFileName())) {
            tableConfigDTO.setControllerName(String.format(tableConfigDTO.getFormatControllerFileName(), tableDTO.getClassName()));
        }
        if (StrUtil.isNotBlank(tableConfigDTO.getFormatServiceFileName())) {
            String serviceName = String.format(tableConfigDTO.getFormatServiceFileName(), tableDTO.getClassName());
            tableConfigDTO.setServiceName(serviceName);
            tableConfigDTO.setServiceNameLowerFirst(StrUtil.lowerFirst(serviceName));
        }
        if (StrUtil.isNotBlank(tableConfigDTO.getFormatServiceImplFileName())) {
            tableConfigDTO.setServiceImplName(String.format(tableConfigDTO.getFormatServiceImplFileName(), tableDTO.getClassName()));
        }
        if (StrUtil.isNotBlank(tableConfigDTO.getFormatMapperFileName())) {
            tableConfigDTO.setMapperName(String.format(tableConfigDTO.getFormatMapperFileName(), tableDTO.getClassName()));
        }
        if (StrUtil.isNotBlank(tableConfigDTO.getFormatXmlFileName())) {
            tableConfigDTO.setXmlName(String.format(tableConfigDTO.getFormatXmlFileName(), tableDTO.getClassName()));
        }
        tableDTO.setTableConfigDTO(tableConfigDTO);

        // 设置权限菜单主键id列表 (根据主键类型设置)
        List<Object> menuIdList;
        if (ObjectUtil.equal(Constants.COMMON_CODE.ZONE, tableConfigDTO.getUseIdType())) {
            // 自增主键, 获取当前权限菜单最大主键值
            Long maxMenuId = permissionService.getMaxMenuId();
            menuIdList = IdTypeEnum.generateMenuIdList(tableConfigDTO.getUseIdType(), maxMenuId);
        } else {
            menuIdList = IdTypeEnum.generateMenuIdList(tableConfigDTO.getUseIdType(), null);
        }
        if (menuIdList.get(0) instanceof Long) {
            tableDTO.setLongMenuIds(menuIdList.stream().map(Convert::toLong).collect(Collectors.toList()));
        } else {
            tableDTO.setStrMenuIds(menuIdList.stream().map(Convert::toStr).collect(Collectors.toList()));
        }

        // 设置父权限菜单信息(组装SQL)
        PermissionDTO permissionDTO = permissionService.getPermission(tableDTO.getParentMenuId());
        ValidatorUtil.notEmpty(permissionDTO, ResultCodeEnum.PERMISSION_NOT_EXIST);
        tableDTO.setPermissionDTO(permissionDTO);
    }


    /**
     * 获取数据库所有表信息
     *
     * @param database
     * @param datasourceId
     * @return
     */
    public List<SchemaTableDTO> getSchemaTables(String database, Long datasourceId) {
        ValidatorUtil.allNotEmpty(database, datasourceId);
        return datasourceService.getSchemaTables(database, datasourceId);
    }

    /**
     * 获取父权限菜单列表(目录、菜单)
     *
     * @return
     */
    public List<PermissionDTO> getPermissions() {
        return permissionService.getParentPermissions();
    }

    /**
     * 获取字段列表: 针对每种类型组装字符串列表, type=0返回DB字段类型列表, type=1返回JAVA字段类型列表
     *
     * @param type
     * @return
     */
    public List<String> getColumnConfigType(String type) {
        ValidatorUtil.notEmpty(type);
        return columnConfigService.getColumnConfigType(type);
    }

    /**
     * 数据源名称实时检索
     *
     * @param search
     * @return
     */
    public List<InputRemoteFetchDTO> fetchDatasource(String search) {
        return datasourceService.fetchDatasource(search);
    }

    /**
     * 业务表名称实时检索
     *
     * @param search
     * @return
     */
    public List<InputRemoteFetchDTO> fetchTable(String search) {
        return tableService.fetchTable(search);
    }

    /**
     * 获取业务表配置列表
     *
     * @return
     */
    public List<TableConfigDTO> getTableConfigList() {
        return tableConfigService.getTableConfigList();
    }

    /**
     * 获取数据源列表
     *
     * @return
     */
    public List<DatasourceDTO> getDatasourceList() {
        List<DatasourceDTO> datasourceDTOList = datasourceService.getDatasourceList();
        if (CollUtil.isNotEmpty(datasourceDTOList)) {
            for (DatasourceDTO datasourceDTO : datasourceDTOList) {
                datasourceDTO.setConnPassword(EncryptUtil.rsaEncrypt(EncryptProperties.rsaPublicKey, datasourceDTO.getConnPassword()));
            }
        }
        return datasourceDTOList;
    }

    /**
     * 新增业务表字段
     *
     * @param tableColumnDTOList
     */
    public void updateTableColumn(List<TableColumnDTO> tableColumnDTOList) {
        // 字段参数校验
        int priFieldCount = 0;
        int fetchCount = 0;
        // 字典类型
        List<String> dictTypeNameList = new ArrayList<>();
        List<DictTypeDTO> dictTypeDTOList = dictTypeService.getDictTypes();
        if (CollUtil.isNotEmpty(dictTypeDTOList)) {
            dictTypeNameList = dictTypeDTOList.stream().map(DictTypeDTO::getType).collect(Collectors.toList());
        }
        for (TableColumnDTO tableColumnDTO : tableColumnDTOList) {
            // 是否只有一个主键字段
            if (StrUtil.equals(Constants.COMMON_JUDGMENT.ZONE_STR, tableColumnDTO.getPkField())) {
                priFieldCount++;
            }
            // 是否只有一个实时检索字段
            if (StrUtil.equals(Constants.COMMON_JUDGMENT.ZONE_STR, tableColumnDTO.getQueryFormFetch())) {
                fetchCount++;
            }
            // 查询表单项处理
            if (StrUtil.equals(Constants.COMMON_JUDGMENT.ZONE_STR, tableColumnDTO.getQueryItem())) {
                ValidatorUtil.allNotBlank(tableColumnDTO.getQueryType(), tableColumnDTO.getQueryFormType());
            }
            // 表单项处理
            if (StrUtil.equals(Constants.COMMON_JUDGMENT.ZONE_STR, tableColumnDTO.getFormItem())) {
                ValidatorUtil.notBlank(tableColumnDTO.getFormType());
                if (StrUtil.equals(Constants.COMMON_JUDGMENT.ZONE_STR, tableColumnDTO.getFormRequired())) {
                    ValidatorUtil.notBlank(tableColumnDTO.getFormValidator());
                }
            }
            // 表单字典校验
            if (StrUtil.isNotBlank(tableColumnDTO.getFormDict())) {
                ValidatorUtil.isTrue(dictTypeNameList.contains(tableColumnDTO.getFormDict()), ResultCodeEnum.DICT_TYPE_NOT_EXIST_ERROR);
            }
            if (StrUtil.isNotBlank(tableColumnDTO.getQueryFormDict())) {
                ValidatorUtil.isTrue(dictTypeNameList.contains(tableColumnDTO.getQueryFormDict()), ResultCodeEnum.DICT_TYPE_NOT_EXIST_ERROR);
            }
        }
        ValidatorUtil.isTrue(priFieldCount == Constants.COMMON_CODE.BASE_INT_ONE,
                ResultCodeEnum.GENERATE_TABLE_COLUMN_PRIMARY_REPEAT_ERROR);
        ValidatorUtil.isTrue(fetchCount <= Constants.COMMON_CODE.BASE_INT_ONE,
                ResultCodeEnum.GENERATE_TABLE_COLUMN_REPEAT_ERROR);
        // 更新字段信息
        tableColumnService.updateTableColumn(tableColumnDTOList);
    }

    /**
     * 获取业务表字段列表
     *
     * @param tableId
     * @return
     */
    public List<TableColumnDTO> getTableColumnList(Long tableId) {
        ValidatorUtil.notEmpty(tableId);
        return tableColumnService.getTableColumn(tableId);
    }

    /**
     * 获取建表的DDL语句
     *
     * @param tableId
     * @return
     */
    public String getTableDDL(Long tableId) {
        ValidatorUtil.notEmpty(tableId);
        return datasourceService.getTableDDL(tableService.getTable(tableId));
    }

    /**
     * 获取系统角色权限标识列表
     *
     * @return
     */
    public List<RoleDTO> getRoles() {
        return roleService.getRoles();
    }

    /**
     * 代码文件下载
     *
     * @param tableId
     * @param vmName
     * @param response
     */
    public void downloadSingle(Long tableId, String vmName, HttpServletResponse response) {
        // 业务表是否存在
        TableDTO tableDTO = tableService.getTable(tableId);
        ValidatorUtil.notEmpty(tableDTO, ResultCodeEnum.GENERATE_TABLE_NOT_EXIST_ERROR);
        // 模板文件是否存在
        List<String> templateList = VelocityUtil.getTemplateList();
        List<String> vmFileList = templateList.stream().filter(template -> StrUtil.contains(template, vmName))
                .collect(Collectors.toList());
        ValidatorUtil.isTrue(CollUtil.isNotEmpty(vmFileList) && vmFileList.size() == Constants.COMMON_CODE.BASE_INT_ONE,
                ResultCodeEnum.GENERATE_TABLE_NOT_EXIST_ERROR);
        try {
            CommonCodeUtil.generateAttachmentResponseHeader(response, vmName);
            // 组装业务表扩展信息
            generateTable(tableDTO);
            // 初始化模板
            VelocityInitializer.initVelocity();
            VelocityContext context = VelocityUtil.prepareContext(tableDTO);
            // 渲染代码文件并同步下载
            StringWriter sw = new StringWriter();
            Template template = Velocity.getTemplate(vmFileList.get(0), StandardCharsets.UTF_8.toString());
            template.merge(context, sw);
            IoUtil.write(response.getOutputStream(), StandardCharsets.UTF_8, false, sw.toString());
            IoUtil.close(sw);
        } catch (Exception e) {
            throw new BusinessException(e, ResultCodeEnum.OSS_SERVICE_DOWNLOAD_ERROR);
        }
    }

    /**
     * 业务表配置基类路径校验
     *
     * @param baseClass
     * @return
     */
    public Boolean checkTableConfigBaseClass(String baseClass) {
        ValidatorUtil.notBlank(baseClass);
        try {
            Class.forName(baseClass);
            return Boolean.TRUE;
        } catch (ClassNotFoundException e) {
            throw new BusinessException(ResultCodeEnum.GENERATE_BASE_CLASS_NOT_EXIST_ERROR);
        }
    }

    /**
     * 数据库设计文档下载
     *
     * @param type
     * @param tableId
     * @param response
     */
    public void screwDownload(Integer type, Long tableId, HttpServletResponse response) {
        ValidatorUtil.allNotEmpty(type, tableId);
        // 业务表是否存在
        TableDTO tableDTO = tableService.getTable(tableId);
        ValidatorUtil.notEmpty(tableDTO, ResultCodeEnum.GENERATE_TABLE_NOT_EXIST_ERROR);
        // 数据源信息是否存在
        DatasourceDTO datasourceDTO = datasourceService.getDatasource(tableDTO.getDatasourceId());
        ValidatorUtil.notEmpty(datasourceDTO, ResultCodeEnum.GENERATE_DATASOURCE_NOT_EXIST_ERROR);
        // 组装数据源连接参数
        if (!StrUtil.contains(datasourceDTO.getConnUrl(), tableDTO.getDatabaseName())) {
            datasourceDTO.setConnUrl(datasourceDTO.getConnUrl() + StrUtil.SLASH + tableDTO.getDatabaseName());
        }
        if (StrUtil.isNotBlank(datasourceDTO.getUrlConfig())) {
            datasourceDTO.setConnUrl(datasourceDTO.getConnUrl() + (StrUtil.startWith(datasourceDTO.getUrlConfig(), "?")
                    ? datasourceDTO.getUrlConfig() : "?" + datasourceDTO.getUrlConfig()));
        }

        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(datasourceDTO.getDbDriver());
        hikariConfig.setJdbcUrl(datasourceDTO.getConnUrl());
        hikariConfig.setUsername(datasourceDTO.getConnUser());
        hikariConfig.setPassword(datasourceDTO.getConnPassword());
        //设置可以获取tables remarks信息
        hikariConfig.addDataSourceProperty("useInformationSchema", "true");
        hikariConfig.setMinimumIdle(2);
        hikariConfig.setMaximumPoolSize(5);

        // 临时文件名称
        String tempFilePath = StrUtil.EMPTY;
        InputStream inputStream = null;
        try (HikariDataSource hikariDataSource = new HikariDataSource(hikariConfig)) {
            // 设置文档类型
            EngineFileType fileType = EngineFileType.HTML;
            if (ObjectUtil.equals(Constants.COMMON_CODE.ONE, type)) {
                fileType = EngineFileType.WORD;
            } else if (ObjectUtil.equals(Constants.COMMON_CODE.TWO, type)) {
                fileType = EngineFileType.MD;
            }
            // 设置文件路径和名称
            String fileName = IdUtil.fastSimpleUUID();
            tempFilePath = (StrUtil.endWith(Constants.TEMP_DOC, StrUtil.SLASH) ? Constants.TEMP_DOC : Constants.TEMP_DOC + StrUtil.SLASH) + fileName + fileType.getFileSuffix();
            // 初始化生成配置
            Configuration config = Configuration.builder()
                    .version(SystemProperties.version)
                    .description("MAdmin数据库文档描述")
                    .dataSource(hikariDataSource)
                    .engineConfig(EngineConfig.builder()
                            .openOutputDir(false)
                            .fileType(fileType)
                            .fileOutputDir(Constants.TEMP_DOC)
                            .fileName(fileName)
                            .produceType(EngineTemplateType.velocity)
                            .build())
                    .produceConfig(ProcessConfig.builder()
                            .ignoreTablePrefix(Arrays.asList("QRTZ_", "gen_"))
                            .build())
                    .build();
            // 生成文档
            new DocumentationExecute(config).execute();
            // 读取文件到输出流中并删除
            if (FileUtil.exist(tempFilePath)) {
                CommonCodeUtil.generateAttachmentResponseHeader(response, "MAdmin_Database_Doc" + fileType.getFileSuffix());
                inputStream = Files.newInputStream(Paths.get(tempFilePath));
                IoUtil.copy(inputStream, response.getOutputStream());
            }
        } catch (Exception e) {
            throw new BusinessException(ResultCodeEnum.OSS_SERVICE_DOWNLOAD_ERROR);
        } finally {
            // 删除临时文件
            IoUtil.close(inputStream);
            if (StrUtil.isNotBlank(tempFilePath) && FileUtil.exist(tempFilePath)) {
                FileUtil.del(tempFilePath);
            }
        }
    }

}
