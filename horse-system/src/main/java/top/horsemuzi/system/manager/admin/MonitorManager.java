package top.horsemuzi.system.manager.admin;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.BetweenFormatter;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import oshi.SystemInfo;
import oshi.hardware.*;
import oshi.software.os.FileSystem;
import oshi.software.os.OSFileStore;
import oshi.software.os.OperatingSystem;
import oshi.util.FormatUtil;
import oshi.util.Util;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.*;
import top.horsemuzi.system.config.properties.EncryptProperties;
import top.horsemuzi.system.core.redis.service.RedisService;
import top.horsemuzi.system.pojo.dto.user.OnlineUserDTO;
import top.horsemuzi.system.pojo.dto.user.OnlineUserQuery;
import top.horsemuzi.system.pojo.dto.user.UserDTO;
import top.horsemuzi.system.pojo.dto.user.UserQuery;
import top.horsemuzi.system.service.UserService;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description 缓存监控服务模块
 * @date 2022/7/3 16:35
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MonitorManager {

    private final RedisService redisService;
    private final UserService userService;

    /**
     * 获取缓存监控信息(redis)
     *
     * @return
     */
    public Map<String, Object> getCacheInfo() {
        Map<String, Object> cacheMap = new HashMap<>(16);
        Properties info = redisService.getRedisInfo(RedisService.SECTION_TYPE.INFO);
        Properties dbSize = redisService.getRedisInfo(RedisService.SECTION_TYPE.DB_SIZE);
        cacheMap.put(RedisService.SECTION_TYPE.INFO.getSection(), info);
        String section = RedisService.SECTION_TYPE.DB_SIZE.getSection();
        cacheMap.put(section, dbSize.get(section));
        Properties commands = redisService.getRedisInfo(RedisService.SECTION_TYPE.COMMANDSTATS);
        List<Map<String, String>> pieList = new ArrayList<>();
        if (ObjectUtil.isNotEmpty(commands)) {
            Map<String, String> data = null;
            for (String key : commands.stringPropertyNames()) {
                data = new HashMap<>(4);
                data.put("name", StrUtil.removePrefix(key, "cmdstat_"));
                String property = commands.getProperty(key);
                data.put("value", StrUtil.subBetween(property, "calls=", ",usec"));
                pieList.add(data);
            }
        }
        cacheMap.put(RedisService.SECTION_TYPE.COMMANDSTATS.getSection(), pieList);
        return cacheMap;
    }

    /**
     * 获取系统服务监控信息
     *
     * @return
     */
    public Map<String, Object> getServerInfo() {
        Map<String, Object> serverMap = new HashMap<>(16);
        try {
            SystemInfo si = new SystemInfo();
            OperatingSystem os = si.getOperatingSystem();
            HardwareAbstractionLayer hal = si.getHardware();
            // 系统信息
            serverMap.put("sys", getSystemInfo(os));
            // CPU信息
            serverMap.put("cpu", getCpuInfo(hal.getProcessor()));
            // 内存信息
            serverMap.put("memory", getMemoryInfo(hal.getMemory()));
            // 交换区信息
            serverMap.put("swap", getSwapInfo(hal.getMemory()));
            // 磁盘
            serverMap.put("disk", getDiskInfo(os));
            // 实时网速
            serverMap.put("speed", getInternetSpeed(hal));
            // JDK信息
            serverMap.put("jdk", getJdkInfo());
            // 当前时间
            serverMap.put("time", DateUtil.formatTime(new Date()));
        } catch (Exception e) {
            log.info("获取系统服务监控信息异常: {}", ExceptionUtil.getMessage(e));
            throw new BusinessException(ResultCodeEnum.GET_SERVER_INFO_ERROR);
        }
        return serverMap;
    }

    /**
     * 获取JDK信息
     *
     * @return
     */
    private Map<String, Object> getJdkInfo() {
        Map<String, Object> jdkMap = new HashMap<>(16);
        RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
        jdkMap.put("jvmName", runtimeMXBean.getVmName());
        jdkMap.put("startTime", DateUtil.formatDateTime(DateUtil.date(runtimeMXBean.getStartTime())));
        jdkMap.put("jvmVendor", runtimeMXBean.getVmVendor());
        jdkMap.put("jdkVersion", System.getProperty("java.version"));
        jdkMap.put("javaHome", System.getProperty("java.home"));
        return jdkMap;
    }

    /**
     * 获取实时网速
     *
     * @param hardware
     * @return
     */
    private Map<String, Object> getInternetSpeed(HardwareAbstractionLayer hardware) {
        List<NetworkIF> networkIFList = hardware.getNetworkIFs();
        NetworkIF net = networkIFList.get(networkIFList.size() - 1);
        // 获取时间段内实时网速
        long downloadStart = net.getBytesRecv();
        long uploadStart = net.getBytesSent();
        long timestampStart = net.getTimeStamp();
        // 睡眠300ms, 避免页面请求超时
        Util.sleep(300);
        net.updateAttributes();
        long downloadEnd = net.getBytesRecv();
        long uploadEnd = net.getBytesSent();
        long timestampEnd = net.getTimeStamp();

        // 计算实时网速
        BigDecimal downloadDiff = new BigDecimal(Long.toString(downloadEnd - downloadStart));
        BigDecimal uploadDiff = new BigDecimal(Long.toString(uploadEnd - uploadStart));
        BigDecimal timestampDiff = new BigDecimal(Long.toString(timestampEnd - timestampStart));
        BigDecimal downloadPerSecond = downloadDiff.multiply(BigDecimal.valueOf(1000)).divide(timestampDiff, 2, RoundingMode.HALF_UP);
        BigDecimal uploadPerSecond = uploadDiff.multiply(BigDecimal.valueOf(1000)).divide(timestampDiff, 2, RoundingMode.HALF_UP);
        BigDecimal downKb = downloadPerSecond.divide(BigDecimal.valueOf(1024), 0, RoundingMode.HALF_UP);
        BigDecimal downMb = downKb.divide(BigDecimal.valueOf(1024), 1, RoundingMode.HALF_UP);
        BigDecimal uploadKb = uploadPerSecond.divide(BigDecimal.valueOf(1024), 0, RoundingMode.HALF_UP);
        BigDecimal uploadMb = uploadKb.divide(BigDecimal.valueOf(1024), 1, RoundingMode.HALF_UP);
        Map<String, Object> speedMap = new HashMap<>(4);
        speedMap.put("upload", uploadMb.compareTo(BigDecimal.ONE) > 0 ? uploadMb + " MB/s" : uploadKb + " KB/s");
        speedMap.put("down", downMb.compareTo(BigDecimal.ONE) > 0 ? downMb + " MB/s" : downKb + " KB/s");
        return speedMap;
    }

    /**
     * 获取磁盘信息
     *
     * @return
     */
    private Map<String, Object> getDiskInfo(OperatingSystem os) {
        Map<String, Object> diskInfo = new LinkedHashMap<>();
        FileSystem fileSystem = os.getFileSystem();
        List<OSFileStore> fsArray = fileSystem.getFileStores();
        String osName = System.getProperty("os.name");
        long available = 0, total = 0;
        for (OSFileStore fs : fsArray) {
            // windows 需要将所有磁盘分区累加，linux 和 mac 直接累加会出现磁盘重复的问题，待修复
            if (osName.toLowerCase().startsWith("win")) {
                available += fs.getUsableSpace();
                total += fs.getTotalSpace();
            } else {
                available = fs.getUsableSpace();
                total = fs.getTotalSpace();
                break;
            }
        }
        long used = total - available;
        diskInfo.put("total", total > 0 ? FormatUtil.formatBytesDecimal(total) : "?");
        diskInfo.put("available", FormatUtil.formatBytesDecimal(available));
        diskInfo.put("used", FormatUtil.formatBytesDecimal(used));
        if (total != 0) {
            BigDecimal usageRate = BigDecimal.valueOf(used).divide(BigDecimal.valueOf(total), 2, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100));
            diskInfo.put("usageRate", usageRate);
        } else {
            diskInfo.put("usageRate", BigDecimal.ZERO);
        }
        return diskInfo;
    }

    /**
     * 获取交换区信息
     *
     * @param memory
     * @return
     */
    private Map<String, Object> getSwapInfo(GlobalMemory memory) {
        Map<String, Object> swapInfo = new LinkedHashMap<>();
        VirtualMemory virtualMemory = memory.getVirtualMemory();
        long total = virtualMemory.getSwapTotal();
        long used = virtualMemory.getSwapUsed();
        swapInfo.put("total", FormatUtil.formatBytesDecimal(total));
        swapInfo.put("used", FormatUtil.formatBytesDecimal(used));
        swapInfo.put("available", FormatUtil.formatBytesDecimal(total - used));
        if (used == 0) {
            swapInfo.put("usageRate", BigDecimal.ZERO);
        } else {
            BigDecimal usageRate = BigDecimal.valueOf(used).divide(BigDecimal.valueOf(total), 2, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100));
            swapInfo.put("usageRate", usageRate);
        }
        return swapInfo;
    }

    /**
     * 获取内存信息
     *
     * @param memory
     * @return
     */
    private Map<String, Object> getMemoryInfo(GlobalMemory memory) {
        Map<String, Object> memoryInfo = new LinkedHashMap<>();
        memoryInfo.put("total", FormatUtil.formatBytesDecimal(memory.getTotal()));
        memoryInfo.put("available", FormatUtil.formatBytesDecimal(memory.getAvailable()));
        memoryInfo.put("used", FormatUtil.formatBytesDecimal(memory.getTotal() - memory.getAvailable()));
        BigDecimal usageRate = BigDecimal.valueOf(memory.getTotal() - memory.getAvailable()).divide(BigDecimal.valueOf(memory.getTotal()), 2, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100));
        memoryInfo.put("usageRate", usageRate);
        return memoryInfo;
    }

    /**
     * 获取Cpu相关信息
     *
     * @param processor
     * @return
     */
    private Map<String, Object> getCpuInfo(CentralProcessor processor) {
        Map<String, Object> cpuInfo = new LinkedHashMap<>();
        cpuInfo.put("name", processor.getProcessorIdentifier().getName());
        cpuInfo.put("package", processor.getPhysicalPackageCount() + "个物理CPU");
        cpuInfo.put("core", processor.getPhysicalProcessorCount() + "个物理核心");
        cpuInfo.put("coreNumber", processor.getPhysicalProcessorCount());
        cpuInfo.put("logic", processor.getLogicalProcessorCount() + "个逻辑CPU");
        // CPU信息
        long[] prevTicks = processor.getSystemCpuLoadTicks();
        // 默认等待300毫秒...
        long time = 300;
        Util.sleep(time);
        long[] ticks = processor.getSystemCpuLoadTicks();
        while (Arrays.toString(prevTicks).equals(Arrays.toString(ticks)) && time < 1000) {
            time += 25;
            Util.sleep(25);
            ticks = processor.getSystemCpuLoadTicks();
        }
        long user = ticks[CentralProcessor.TickType.USER.getIndex()] - prevTicks[CentralProcessor.TickType.USER.getIndex()];
        long nice = ticks[CentralProcessor.TickType.NICE.getIndex()] - prevTicks[CentralProcessor.TickType.NICE.getIndex()];
        long sys = ticks[CentralProcessor.TickType.SYSTEM.getIndex()] - prevTicks[CentralProcessor.TickType.SYSTEM.getIndex()];
        long idle = ticks[CentralProcessor.TickType.IDLE.getIndex()] - prevTicks[CentralProcessor.TickType.IDLE.getIndex()];
        long iowait = ticks[CentralProcessor.TickType.IOWAIT.getIndex()] - prevTicks[CentralProcessor.TickType.IOWAIT.getIndex()];
        long irq = ticks[CentralProcessor.TickType.IRQ.getIndex()] - prevTicks[CentralProcessor.TickType.IRQ.getIndex()];
        long softirq = ticks[CentralProcessor.TickType.SOFTIRQ.getIndex()] - prevTicks[CentralProcessor.TickType.SOFTIRQ.getIndex()];
        long steal = ticks[CentralProcessor.TickType.STEAL.getIndex()] - prevTicks[CentralProcessor.TickType.STEAL.getIndex()];
        long totalCpu = user + nice + sys + idle + iowait + irq + softirq + steal;
        BigDecimal used = BigDecimal.valueOf(100).multiply(BigDecimal.valueOf(user).divide(BigDecimal.valueOf(totalCpu), 2, RoundingMode.HALF_UP)).add(BigDecimal.valueOf(100).multiply(BigDecimal.valueOf(sys).divide(BigDecimal.valueOf(totalCpu), 2, RoundingMode.HALF_UP)));
        cpuInfo.put("used", used);
        cpuInfo.put("idle", BigDecimal.valueOf(100).multiply(BigDecimal.valueOf(idle).divide(BigDecimal.valueOf(totalCpu), 2, RoundingMode.HALF_UP)));
        return cpuInfo;
    }

    /**
     * 获取系统相关信息,系统、运行天数、系统IP
     *
     * @param os
     * @return
     */
    private Map<String, Object> getSystemInfo(OperatingSystem os) {
        Map<String, Object> systemInfo = new LinkedHashMap<>();
        // jvm 开始运行时间
        Date date = new Date(ManagementFactory.getRuntimeMXBean().getStartTime());
        // 计算项目运行时间
        String formatBetween = DateUtil.formatBetween(date, new Date(), BetweenFormatter.Level.HOUR);
        // 系统信息
        systemInfo.put("os", os.toString());
        systemInfo.put("day", formatBetween);
        systemInfo.put("ip", NetUtil.getLocalIp());
        return systemInfo;
    }

    /**
     * 分页系统在线用户列表
     *
     * @param query
     * @return
     */
    public Page<OnlineUserDTO> getPageOnlineUsers(OnlineUserQuery query) {
        Page<OnlineUserDTO> page = new Page<>(query.getPage(), query.getSize());
        // 获取缓存中登录用户信息
        Set<String> keys = redisService.getKeysByPrefix(Constants.REDIS_PREFIX.TOKEN);
        if (CollectionUtil.isEmpty(keys)) {
            return page;
        }
        Map<String, String> cacheOnlineTokenMap = redisService.multiGetMap(keys);
        Map<Long, String> tokenMap = new HashMap<>(16);
        HashMap<Long, String> terminalMap = new HashMap<>(16);
        List<Long> onlineUserIds = new ArrayList<>();
        for (Map.Entry<String, String> onlineToken : cacheOnlineTokenMap.entrySet()) {
            JwtUser jwtUser = JwtUtil.parseToken(onlineToken.getValue());
            if (ObjectUtil.isEmpty(jwtUser)) {
                continue;
            }
            tokenMap.put(jwtUser.getId(), onlineToken.getKey());
            terminalMap.put(jwtUser.getId(), jwtUser.getTerminal());
            onlineUserIds.add(jwtUser.getId());
        }
        if (CollectionUtil.isEmpty(onlineUserIds)) {
            return page;
        }
        // 分页获取用户信息
        UserQuery userQuery = new UserQuery();
        userQuery.setUserIds(onlineUserIds);
        userQuery.setPage(query.getPage());
        userQuery.setSize(query.getSize());
        if (StrUtil.isNotBlank(query.getUsername())) {
            userQuery.setUsername(query.getUsername());
        }
        if (StrUtil.isNotBlank(query.getIp())) {
            userQuery.setIp(query.getIp());
        }
        if (StrUtil.isNotBlank(query.getArea())) {
            userQuery.setIp(query.getArea());
        }
        if (ObjectUtil.isNotEmpty(query.getCreateStart())) {
            userQuery.setCreateStart(query.getCreateStart());
        }
        if (ObjectUtil.isNotEmpty(query.getCreateEnd())) {
            userQuery.setCreateEnd(query.getCreateEnd());
        }
        Page<UserDTO> userDTOPage = userService.getPageUsers(userQuery);
        page.setTotal(userDTOPage.getTotal());
        List<UserDTO> userDTOList = userDTOPage.getRecords();
        if (CollectionUtil.isEmpty(userDTOList)) {
            return page;
        }
        List<OnlineUserDTO> onlineUserDTOList = new ArrayList<>();
        OnlineUserDTO onlineUserDTO = null;
        for (UserDTO userDTO : userDTOList) {
            onlineUserDTO = new OnlineUserDTO();
            String redisKey = tokenMap.get(userDTO.getId());
            if (StrUtil.isNotBlank(redisKey)) {
                // 获取当前在线用户的token有效期
                Long expire = redisService.getExpire(redisKey, TimeUnit.SECONDS);
                if (expire > Constants.COMMON_CODE.LONG_ZONE) {
                    onlineUserDTO.setExpireTime(expire);
                } else {
                    continue;
                }
                String key = StrUtil.replace(redisKey, Constants.REDIS_PREFIX.TOKEN + StrUtil.COLON, StrUtil.EMPTY);
                onlineUserDTO.setToken(EncryptUtil.aesEncrypt(key, EncryptProperties.aesKey));
            }
            onlineUserDTO.setUsername(userDTO.getUsername());
            onlineUserDTO.setPinyinUsername(userDTO.getPinyinUsername());
            onlineUserDTO.setPinyinAcronymUsername(userDTO.getPinyinAcronymUsername());
            onlineUserDTO.setIp(userDTO.getLastLoginIp());
            onlineUserDTO.setArea(userDTO.getLastLoginArea());
            onlineUserDTO.setLoginTime(userDTO.getLastLoginTime());
            onlineUserDTO.setOs(userDTO.getLastLoginOs());
            onlineUserDTO.setBrowser(userDTO.getLastLoginClient());
            onlineUserDTO.setTotalCount(userDTO.getLoginCount());
            onlineUserDTO.setTotalOnlineTime(userDTO.getTotalOnlineTime());
            onlineUserDTO.setTerminal(terminalMap.get(userDTO.getId()));
            onlineUserDTOList.add(onlineUserDTO);
        }
        // 根据最后登录时间、过期时间倒序
        page.setRecords(onlineUserDTOList.stream().sorted(Comparator.comparing(OnlineUserDTO::getLoginTime, Comparator.reverseOrder()).thenComparing(OnlineUserDTO::getExpireTime, Comparator.reverseOrder())).collect(Collectors.toList()));
        return page;
    }

    /**
     * 强制用户退出(不允许强制退出当前登录账号)
     *
     * @param tokens
     */
    public void forceUserQuit(List<String> tokens) {
        ValidatorUtil.notEmpty(tokens);
        // 获取当前登录账号的token相关信息
        JwtUser currentJwtUser = JwtUtil.parseToken(NetUtil.getGlobalRequest().getHeader(Constants.ACCESS_TOKEN));
        ValidatorUtil.notEmpty(currentJwtUser, ResultCodeEnum.NOT_LOGIN);
        List<String> keys = new ArrayList<>();
        for (String token : tokens) {
            token = EncryptUtil.aesDecrypt(token, EncryptProperties.aesKey);
            ValidatorUtil.isTrue(!StrUtil.equals(token, currentJwtUser.getUuid()), ResultCodeEnum.NOT_ALLOWED_QUIT_SELF_ACCOUNT);
            keys.add(Constants.REDIS_PREFIX.TOKEN + StrUtil.COLON + token);
        }
        redisService.delete(keys);
    }

}
