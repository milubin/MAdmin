package top.horsemuzi.system.manager.admin;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobDTO;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobLogDTO;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobLogQuery;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobQuery;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskDTO;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskLogDTO;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskLogQuery;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskQuery;
import top.horsemuzi.system.service.ScheduleService;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 任务调度模块
 *
 * @author mabin
 * @date 2022/06/30 16:25
 **/
@Slf4j
@RequiredArgsConstructor
@Component
public class ScheduleManager {

    private final ScheduleService scheduleService;


    /**
     * 创建Quartz任务调度
     *
     * @param quartzDTO
     */
    public void addQuartzJob(QuartzJobDTO quartzDTO) {
        ValidatorUtil.notEmpty(quartzDTO);
        scheduleService.addQuartzJob(quartzDTO);
    }

    /**
     * 暂停Quartz任务调度
     *
     * @param id
     */
    public void pauseQuartzJob(Long id) {
        scheduleService.pauseQuartzJob(id);
    }

    /**
     * 重启Quartz任务调度
     *
     * @param id
     */
    public void rebootQuartzJob(Long id) {
        scheduleService.rebootQuartzJob(id);
    }

    /**
     * 更新Quartz任务调度
     *
     * @param quartzDTO
     */
    public void updateQuartzJob(QuartzJobDTO quartzDTO) {
        ValidatorUtil.notEmpty(quartzDTO);
        scheduleService.updateQuartzJob(quartzDTO);
    }

    /**
     * 删除Quartz任务调度
     *
     * @param jobIds
     */
    public void deleteQuartzJob(List<Long> jobIds) {
        ValidatorUtil.notEmpty(jobIds);
        scheduleService.deleteQuartzJob(jobIds);
    }

    /**
     * 分页获取Quartz任务调度列表
     *
     * @param quartzQuery
     * @return
     */
    public Page<QuartzJobDTO> getPageQuartzs(QuartzJobQuery quartzQuery) {
        Page<QuartzJobDTO> page = new Page<>(quartzQuery.getPage(), quartzQuery.getSize());
        Long total = scheduleService.getQuartzCount(quartzQuery);
        if (total <= Constants.COMMON_CODE.LONG_ZONE) {
            return page;
        }
        List<QuartzJobDTO> quartzDTOList = scheduleService.getQuartzList(quartzQuery);
        return page.setTotal(total).setRecords(quartzDTOList);
    }

    /**
     * 查询Quartz任务调度信息
     *
     * @param id
     * @return
     */
    public QuartzJobDTO getQuartzJob(Long id) {
        ValidatorUtil.notEmpty(id);
        return scheduleService.getQuartzJob(id);
    }

    /**
     * 任务立刻运行一次
     *
     * @param id
     */
    public void runOnceQuartzJob(Long id) {
        scheduleService.runOnceQuartzJob(id);
    }

    /**
     * 分页获取Quartz任务调度日志列表
     *
     * @param query
     * @return
     */
    public Page<QuartzJobLogDTO> getPageQuartzLog(QuartzJobLogQuery query) {
        ValidatorUtil.notEmpty(query);
        ValidatorUtil.notEmpty(query.getJobId());
        return scheduleService.getPageQuartzLog(query);
    }

    /**
     * 清理Quartz任务调度日志信息
     *
     * @param quartzLogIds
     */
    public void clearQuartzLogs(List<Long> quartzLogIds) {
        ValidatorUtil.notEmpty(quartzLogIds);
        scheduleService.clearQuartzLogs(quartzLogIds);
    }

    /**
     * 分页获取Spring任务调度列表
     *
     * @param taskQuery
     * @return
     */
    public Page<TaskDTO> getPageTasks(TaskQuery taskQuery) {
        return scheduleService.getPageTasks(taskQuery);
    }

    /**
     * 新增Spring调度任务
     *
     * @param taskDTO
     */
    public void addTask(TaskDTO taskDTO) {
        ValidatorUtil.notEmpty(taskDTO);
        taskDTO.setMethodParams(null);
        scheduleService.addTask(taskDTO);
    }

    /**
     * 更新Spring调度任务
     *
     * @param taskDTO
     */
    public void updateTask(TaskDTO taskDTO) {
        ValidatorUtil.notEmpty(taskDTO);
        taskDTO.setMethodParams(null);
        scheduleService.updateTask(taskDTO);
    }

    /**
     * 获取Spring调度任务
     *
     * @param id
     * @return
     */
    public TaskDTO getTask(Long id) {
        return scheduleService.getTask(id);
    }

    /**
     * 切换Spring调度任务状态
     *
     * @param taskDTO
     */
    public void switchTaskStatus(TaskDTO taskDTO) {
        scheduleService.switchTaskStatus(taskDTO);
    }

    /**
     * 删除Spring调度任务
     *
     * @param taskIds
     */
    public void deleteTask(List<Long> taskIds) {
        ValidatorUtil.notEmpty(taskIds);
        scheduleService.deleteTask(taskIds);
    }

    /**
     * 分页获取Spring任务调度日志列表
     *
     * @param query
     * @return
     */
    public Page<TaskLogDTO> getPageTaskLog(TaskLogQuery query) {
        return scheduleService.getPageTaskLog(query);
    }

    /**
     * 清理Spring任务调度日志信息
     *
     * @param taskLogIds
     */
    public void clearTaskLogs(List<Long> taskLogIds) {
        scheduleService.clearTaskLogs(taskLogIds);
    }

    /**
     * 获取Quartz调度任务日志统计数据
     *
     * @return
     */
    public Map<String, Object> statisticsQuartzLog() {
        return scheduleService.statisticsQuartzLog();
    }

    /**
     * 获取Spring调度任务日志统计数据
     *
     * @return
     */
    public Map<String, Object> statisticsTaskLog() {
        return scheduleService.statisticsTaskLog();
    }

    /**
     * 导出Quartz调度任务日志
     *
     * @param query
     * @param response
     */
    public void exportQuartzLog(QuartzJobLogQuery query, HttpServletResponse response) {
        scheduleService.exportQuartzLog(query, response);
    }

    /**
     * 导出Spring调度任务日志
     *
     * @param query
     * @param response
     */
    public void exportTaskLog(TaskLogQuery query, HttpServletResponse response) {
        scheduleService.exportTaskLog(query, response);
    }

    /**
     * Spring任务名称实时检索
     *
     * @param search
     * @return
     */
    public List<InputRemoteFetchDTO> taskFetch(String search) {
        return scheduleService.taskFetch(search);
    }

    /**
     * Quartz任务名称实时检索
     *
     * @param search
     * @return
     */
    public List<InputRemoteFetchDTO> quartzFetch(String search) {
        return scheduleService.quartzFetch(search);
    }

    /**
     * Quartz任务全部暂停/启动
     *
     * @param execType
     * @return void
     * @author mabin
     * @date 2023/3/1 11:01
     **/
    public void allPauseOrRunQuartzJob(Integer execType) {
        ValidatorUtil.notEmpty(execType);
        scheduleService.allPauseOrRunQuartzJob(execType);
    }

    /**
     * Spring调度任务全部暂停/启动
     *
     * @param execType
     * @return void
     * @author mabin
     * @date 2023/3/1 13:42
     **/
    public void allPauseOrRunTask(Integer execType) {
        ValidatorUtil.notEmpty(execType);
        scheduleService.allPauseOrRunTask(execType);
    }
}
