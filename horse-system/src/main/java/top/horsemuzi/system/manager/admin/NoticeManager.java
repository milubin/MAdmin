package top.horsemuzi.system.manager.admin;

import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.eventbus.AsyncEventBus;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.framework.AopContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.PublishTypeEnum;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.core.office.service.OfficeService;
import top.horsemuzi.system.handle.event.guava.event.NoticeSocketEvent;
import top.horsemuzi.system.pojo.dto.notice.*;
import top.horsemuzi.system.pojo.dto.role.RoleDTO;
import top.horsemuzi.system.pojo.dto.user.UserDTO;
import top.horsemuzi.system.pojo.dto.user.UserQuery;
import top.horsemuzi.system.pojo.entity.UserRole;
import top.horsemuzi.system.service.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 通知公告模块
 *
 * @author 马滨
 **/
@Slf4j
@RequiredArgsConstructor
@Component
public class NoticeManager {

    private final NoticeService noticeService;
    private final NoticeRecordService noticeRecordService;
    private final RoleService roleService;
    private final UserService userService;
    private final UserRoleService userRoleService;
    private final AsyncEventBus asyncEventBus;

    /**
     * 分页通知公告信息列表
     *
     * @param query
     * @return
     */
    @Transactional(readOnly = true)
    public Page<NoticeDTO> getPageNotices(NoticeQuery query) {
        Page<NoticeDTO> page = new Page<>(query.getPage(), query.getSize());
        query.setPage((query.getPage() - 1) * query.getSize());
        Long noticeCount = noticeService.getPageNoticeCount(query);
        if (noticeCount <= 0) {
            return page;
        }
        List<NoticeDTO> noticeDTOList = noticeService.getPageNoticeList(query);
        page.setTotal(noticeCount);
        page.setRecords(noticeDTOList);
        return page;
    }


    /**
     * 导出通知公告和对应记录数据（EXCEL）
     *
     * @param query
     * @param response
     */
    public void export(NoticeQuery query, HttpServletResponse response) {
        List<NoticeExportDTO> noticeExportDTOList = noticeService.getNoticeExportList(query);
        if (CollUtil.isNotEmpty(noticeExportDTOList)) {
            try {
                OfficeService.EXCEL_POI.export(response, NoticeExportDTO.class, noticeExportDTOList, "通知公告发布信息.xlsx",
                        "通知公告发布详情", null, ExcelType.XSSF);
            } catch (Exception e) {
                throw new BusinessException(e, ResultCodeEnum.FILE_EXPORT_ERROR);
            }
        }
    }

    /**
     * 获取通知公告信息详情
     *
     * @param noticeId
     * @return
     */
    public NoticeDTO getNotice(Long noticeId) {
        ValidatorUtil.notEmpty(noticeId);
        return noticeService.getNotice(noticeId);
    }

    /**
     * 新增通知公告
     *
     * @param noticeDTO
     */
    @Transactional(rollbackFor = Exception.class)
    public void addNotice(NoticeDTO noticeDTO) {
        ValidatorUtil.notEmpty(noticeDTO);
        ValidatorUtil.notEmpty(noticeDTO.getStatus());
        // 新增通知公告(额外添加备注信息)
        if (StrUtil.isBlank(noticeDTO.getRemark()) && ObjectUtil.isNotNull(noticeDTO.getPublishType())) {
            noticeDTO.setRemark(PublishTypeEnum.getByCode(noticeDTO.getPublishType()));
        }
        noticeService.addNotice(noticeDTO);
        // 是否同步发布通知公告
        NoticeManager proxy = (NoticeManager) AopContext.currentProxy();
        proxy.syncPublishNotice(noticeDTO);
    }

    /**
     * 更新通知公告
     *
     * @param noticeDTO
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateNotice(NoticeDTO noticeDTO) {
        ValidatorUtil.notEmpty(noticeDTO);
        ValidatorUtil.notEmpty(noticeDTO.getId());
        // 校验状态
        NoticeDTO currentNoticeDTO = noticeService.getNotice(noticeDTO.getId());
        ValidatorUtil.notEmpty(currentNoticeDTO, ResultCodeEnum.NOTICE_NOT_EXIST_ERROR);
        ValidatorUtil.isTrue(StrUtil.equals(Constants.COMMON_JUDGMENT.ZONE_STR, currentNoticeDTO.getStatus()), ResultCodeEnum.NOTICE_STATUS_ERROR);
        // 更新通知公告
        noticeService.updateNotice(noticeDTO);
        // 是否同步发布通知公告
        NoticeManager proxy = (NoticeManager) AopContext.currentProxy();
        proxy.syncPublishNotice(noticeDTO);
    }

    /**
     * 发布通知公告
     *
     * @param id
     */
    @Transactional(rollbackFor = Exception.class)
    public void publishNotice(Long id) {
        ValidatorUtil.notEmpty(id);
        NoticeDTO noticeDTO = noticeService.getNotice(id);
        ValidatorUtil.notEmpty(noticeDTO, ResultCodeEnum.NOTICE_NOT_EXIST_ERROR);
        // 已发布状态不允许重复发布
        ValidatorUtil.isTrue(StrUtil.equals(Constants.COMMON_JUDGMENT.ZONE_STR, noticeDTO.getStatus()), ResultCodeEnum.NOTICE_STATUS_ERROR);
        // 发布对象类型和相关对象信息是否存在
        if (StrUtil.isBlank(noticeDTO.getPublishInfo()) || ObjectUtil.isNull(noticeDTO.getPublishType())) {
            throw new BusinessException(ResultCodeEnum.NOTICE_PUBLISH_STATUS_ERROR);
        }
        // 更新通知公告状态
        noticeDTO.setStatus(Constants.COMMON_JUDGMENT.ONE_STR);
        noticeDTO.setRemark("一键发布");
        noticeService.updateNotice(noticeDTO);
        // 同步发布通知公告
        NoticeManager proxy = (NoticeManager) AopContext.currentProxy();
        proxy.syncPublishNotice(noticeDTO);
    }

    /**
     * 是否同步发布通知公告
     *
     * @param noticeDTO
     */
    @Async("async")
    public void syncPublishNotice(NoticeDTO noticeDTO) {
        // 是否同步发布
        if (StrUtil.equals(noticeDTO.getStatus(), Constants.COMMON_JUDGMENT.ONE_STR)) {
            List<UserDTO> userDTOList = null;
            if (ObjectUtil.equals(PublishTypeEnum.CUSTOM_ROLE.getCode(), noticeDTO.getPublishType()) && StrUtil.isNotBlank(noticeDTO.getPublishInfo())) {
                // 自定义角色
                List<Long> roleIds = new ArrayList<>(StrUtil.split(noticeDTO.getPublishInfo(), StrUtil.COMMA))
                        .stream().map(Convert::toLong)
                        .collect(Collectors.toList());
                List<RoleDTO> roleDTOList = roleService.getRoles(roleIds);
                ValidatorUtil.notEmpty(roleDTOList, ResultCodeEnum.ROLE_NOT_EXIST);
                long roleCount = roleDTOList.stream().filter(item -> StrUtil.equals(Constants.COMMON_JUDGMENT.ZONE_STR, item.getStatus())).count();
                ValidatorUtil.isTrue(roleCount == Convert.toLong(roleIds.size()), ResultCodeEnum.NOTICE_PUBLISH_STATUS_ERROR);

                List<Long> userIds = userRoleService.getUserIdsByRoleIds(roleIds).stream().map(UserRole::getUserId).collect(Collectors.toList());
                userDTOList = userService.getUsers(userIds);
                ValidatorUtil.notEmpty(userDTOList, ResultCodeEnum.USER_NOT_EXIST);
                userDTOList.forEach(item -> ValidatorUtil.isTrue(StrUtil.equals(Constants.COMMON_JUDGMENT.ZONE_STR, item.getStatus()), ResultCodeEnum.NOTICE_PUBLISH_STATUS_ERROR));
            } else if (ObjectUtil.equals(PublishTypeEnum.CUSTOM_USER.getCode(), noticeDTO.getPublishType())) {
                // 自定义用户
                List<Long> userIds = new ArrayList<>(StrUtil.split(noticeDTO.getPublishInfo(), StrUtil.COMMA))
                        .stream().map(Convert::toLong)
                        .collect(Collectors.toList());
                userDTOList = userService.getUsers(userIds);
                ValidatorUtil.notEmpty(userDTOList, ResultCodeEnum.USER_NOT_EXIST);
                userDTOList.forEach(item -> ValidatorUtil.isTrue(StrUtil.equals(Constants.COMMON_JUDGMENT.ZONE_STR, item.getStatus()), ResultCodeEnum.NOTICE_PUBLISH_STATUS_ERROR));
            } else {
                // 全部用户
                userDTOList = userService.getUsers(new UserQuery());
                ValidatorUtil.notEmpty(userDTOList, ResultCodeEnum.USER_NOT_EXIST);
            }
            // 新增通知公告发布记录
            if (CollectionUtil.isNotEmpty(userDTOList)) {
                List<NoticeRecordDTO> noticeRecordDTOList = new ArrayList<>(userDTOList.size());
                NoticeRecordDTO noticeRecordDTO = null;
                List<Long> userIdList = new ArrayList<>(userDTOList.size());
                for (UserDTO userDTO : userDTOList) {
                    noticeRecordDTO = new NoticeRecordDTO();
                    noticeRecordDTO.setNoticeId(noticeDTO.getId());
                    noticeRecordDTO.setUserId(userDTO.getId());
                    noticeRecordDTO.setStatus(Constants.COMMON_JUDGMENT.ZONE_STR);
                    noticeRecordDTO.setRemark("同步发布通知公告(" + PublishTypeEnum.getByCode(noticeDTO.getPublishType()) + ")");
                    noticeRecordDTOList.add(noticeRecordDTO);
                    userIdList.add(userDTO.getId());
                }
                if (CollectionUtil.isNotEmpty(noticeRecordDTOList)) {
                    noticeRecordService.addNoticeRecord(noticeRecordDTOList);
                }
                // WebSocket实时推送用户未读状态消息数量
                asyncEventBus.post(new NoticeSocketEvent(JsonUtil.toJson(noticeRecordService.getNoticeSocketList(userIdList))));
            }
        }
    }

    /**
     * 删除通知公告
     *
     * @param noticeIds
     */
    public void deleteNotice(List<Long> noticeIds) {
        ValidatorUtil.notEmpty(noticeIds);
        // 已发布状态不允许删除
        List<NoticeDTO> noticeDTOList = noticeService.getNotice(noticeIds);
        ValidatorUtil.notEmpty(noticeDTOList, ResultCodeEnum.NOTICE_NOT_EXIST_ERROR);
        noticeDTOList.forEach(item -> ValidatorUtil.isTrue(StrUtil.equals(Constants.COMMON_JUDGMENT.ZONE_STR, item.getStatus()), ResultCodeEnum.NOTICE_STATUS_ERROR));
        noticeService.deleteNotice(noticeIds);
    }

    /**
     * 撤销通知公告
     *
     * @param id
     */
    @Transactional(rollbackFor = Exception.class)
    public void revokeNotice(Long id) {
        ValidatorUtil.notEmpty(id);
        // 是否已发布状态
        NoticeDTO noticeDTO = noticeService.getNotice(id);
        ValidatorUtil.notEmpty(noticeDTO, ResultCodeEnum.NOTICE_NOT_EXIST_ERROR);
        ValidatorUtil.isTrue(StrUtil.equals(noticeDTO.getStatus(), Constants.COMMON_JUDGMENT.ONE_STR), ResultCodeEnum.NOTICE_REVOKE_STATUS_ERROR);
        // 状态置为 未发布
        noticeDTO.setStatus(Constants.COMMON_JUDGMENT.ZONE_STR);
        noticeDTO.setRemark("一键撤销");
        noticeService.updateNotice(noticeDTO);
        // 删除发布记录信息
        List<NoticeRecordDTO> noticeRecordDTOList = noticeRecordService.getNoticeRecord(Collections.singletonList(id));
        if (CollectionUtil.isNotEmpty(noticeRecordDTOList)) {
            List<Long> noticeRecordIds = noticeRecordDTOList.stream().map(NoticeRecordDTO::getId).collect(Collectors.toList());
            noticeRecordService.deleteNoticeRecord(noticeRecordIds);

            // WebSocket实时推送用户未读状态消息数量
            List<Long> userIds = noticeRecordDTOList.stream().map(NoticeRecordDTO::getUserId).collect(Collectors.toList());
            List<NoticeSocketDTO> noticeSocketList = noticeRecordService.getNoticeSocketList(userIds);
            List<NoticeSocketDTO> finalNoticeSocketList = new ArrayList<>();
            NoticeSocketDTO noticeSocketDTO = null;
            if (CollectionUtil.isEmpty(noticeSocketList)) {
                for (Long userId : userIds) {
                    noticeSocketDTO = new NoticeSocketDTO();
                    noticeSocketDTO.setUserId(userId);
                    noticeSocketDTO.setNotReadCount(Constants.COMMON_CODE.LONG_ZONE);
                    finalNoticeSocketList.add(noticeSocketDTO);
                }
            } else {
                Map<Long, NoticeSocketDTO> noticeSocketDTOMap = noticeSocketList.stream()
                        .collect(Collectors.toMap(NoticeSocketDTO::getUserId, item -> item, (v1, v2) -> v1));
                for (Long userId : userIds) {
                    NoticeSocketDTO item = noticeSocketDTOMap.get(userId);
                    if (ObjectUtil.isNotNull(item)) {
                        finalNoticeSocketList.add(item);
                    } else {
                        noticeSocketDTO = new NoticeSocketDTO();
                        noticeSocketDTO.setUserId(userId);
                        noticeSocketDTO.setNotReadCount(Constants.COMMON_CODE.LONG_ZONE);
                        finalNoticeSocketList.add(noticeSocketDTO);
                    }
                }
            }
            asyncEventBus.post(new NoticeSocketEvent(JsonUtil.toJson(finalNoticeSocketList)));
        }
    }

    /**
     * 分页获取通知公告记录列表
     *
     * @param query
     * @return
     */
    public Page<NoticeRecordDTO> getPageNoticeRecords(NoticeRecordQuery query) {
        ValidatorUtil.notEmpty(query);
        ValidatorUtil.notEmpty(query.getNoticeId());
        return noticeRecordService.getPageNoticeRecords(query);
    }

    /**
     * 撤销通知公告发布记录信息(细粒度)
     * 如果发布记录全部被撤销，自动更新通知公告的状态为：未发布
     *
     * @param noticeRecordIds
     */
    @Transactional(rollbackFor = Exception.class)
    public void revokeNoticeRecord(List<Long> noticeRecordIds) {
        ValidatorUtil.notEmpty(noticeRecordIds);
        // 获取通知公告id信息 noticeId
        NoticeRecordDTO noticeRecordDTO = noticeRecordService.getNoticeRecord(noticeRecordIds.get(0));
        ValidatorUtil.notEmpty(noticeRecordDTO, ResultCodeEnum.NOTICE_RECORD_NOT_EXIST_ERROR);
        Long noticeId = noticeRecordDTO.getNoticeId();
        // 撤销发布记录
        noticeRecordService.deleteNoticeRecord(noticeRecordIds);
        // 检查发布记录是否全部被撤销
        List<NoticeRecordDTO> noticeRecordDTOList = noticeRecordService.getNoticeRecord(Collections.singletonList(noticeId));
        if (CollectionUtil.isEmpty(noticeRecordDTOList)) {
            NoticeDTO noticeDTO = noticeService.getNotice(noticeId);
            ValidatorUtil.notEmpty(noticeDTO, ResultCodeEnum.NOTICE_NOT_EXIST_ERROR);
            noticeDTO.setStatus(Constants.COMMON_JUDGMENT.ZONE_STR);
            noticeDTO.setRemark("发布记录已全部撤销，自动更新为未发布状态");
            noticeService.updateNotice(noticeDTO);
        }
    }

    /**
     * 读取通知公告记录
     *
     * @param noticeRecordId
     * @param type           类型: 1-单条, 2-批量
     * @param jwtUser
     */
    public void readNoticeRecord(Long noticeRecordId, String type, JwtUser jwtUser) {
        ValidatorUtil.notEmpty(type);
        // 单条读取
        if (StrUtil.equals(Constants.COMMON_CODE.STR_ONE, type)) {
            ValidatorUtil.notEmpty(noticeRecordId);
            noticeRecordService.readNoticeRecord(noticeRecordId, jwtUser.getId());
        } else if (StrUtil.equals(Constants.COMMON_CODE.STR_TWO, type)) {
            // 全部读取
            noticeRecordService.readAllNoticeRecord(jwtUser.getId());
        }
    }

    /**
     * 删除通知公告记录
     *
     * @param noticeRecordId
     * @param type           类型: 1-单条, 2-批量
     * @param jwtUser
     */
    public void deleteNoticeRecord(Long noticeRecordId, String type, JwtUser jwtUser) {
        ValidatorUtil.notEmpty(type);
        if (StrUtil.equals(Constants.COMMON_CODE.STR_ONE, type)) {
            ValidatorUtil.notEmpty(noticeRecordId);
            noticeRecordService.deleteNoticeRecord(noticeRecordId, jwtUser.getId());
        } else if (StrUtil.equals(Constants.COMMON_CODE.STR_TWO, type)) {
            noticeRecordService.deleteAllNoticeRecord(jwtUser.getId());
        }
    }

    /**
     * 获取用户通知公告记录列表
     *
     * @param jwtUser
     * @return
     */
    public List<NoticeRecordDTO> getNoticeRecords(JwtUser jwtUser) {
        return noticeRecordService.getNoticeRecords(jwtUser.getId());
    }

    /**
     * 获取通知公告记录详情
     *
     * @param noticeRecordId
     * @return
     */
    public NoticeRecordDTO getNoticeRecord(Long noticeRecordId) {
        ValidatorUtil.notEmpty(noticeRecordId);
        return noticeRecordService.getNoticeRecord(noticeRecordId);
    }

    /**
     * 获取用户通知公告记录未读数量
     *
     * @param userId
     * @return
     */
    public List<NoticeSocketDTO> getNoticeSocketList(Long userId) {
        ValidatorUtil.notEmpty(userId);
        return noticeRecordService.getNoticeSocketList(Collections.singletonList(userId));
    }

}
