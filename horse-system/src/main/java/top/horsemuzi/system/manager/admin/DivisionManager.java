package top.horsemuzi.system.manager.admin;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.division.DivisionDTO;
import top.horsemuzi.system.pojo.dto.division.DivisionQuery;
import top.horsemuzi.system.pojo.entity.Division;
import top.horsemuzi.system.service.DivisionService;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 国家区划管理模块
 *
 * @author 马滨
 * @date 2022/11/06 10:38
 **/

@Slf4j
@RequiredArgsConstructor
@Component
public class DivisionManager {

    private final DivisionService divisionService;


    /**
     * 初始化区划信息（将区划信息导入数据库）
     */
    public void initDivision() {
        // 加载json文件
        String jsonFilePath = System.getProperty("user.dir") + "/static/json/中国行政区划.json";
        try (FileInputStream inputStream = new FileInputStream(jsonFilePath)) {
            String json = IoUtil.readUtf8(inputStream);
            ArrayNode jsonNode = JsonUtil.getArrayNode(JsonUtil.fromJson(json), "districts");
            // 解析json文件、组装父子关系结构数据
            ArrayNode districtsOne = (ArrayNode) jsonNode.get(0).get("districts");
            if (ObjectUtil.isNotNull(districtsOne)) {
                for (JsonNode node : districtsOne) {
                    // 获取省信息
                    DivisionDTO divisionDTO = new DivisionDTO();
                    divisionDTO.setParentId(Constants.COMMON_CODE.LONG_ZONE);
                    divisionDTO.setCode(JsonUtil.getNodeStr(node, "adcode"));
                    divisionDTO.setName(JsonUtil.getNodeStr(node, "name"));
                    divisionDTO.setLevel(Constants.COMMON_CODE.ONE);
                    divisionDTO.setRemark(JsonUtil.getNodeStr(node, "center"));
                    Division divisionOne = OrikaUtil.convert(divisionDTO, Division.class);
                    boolean save = divisionService.save(divisionOne);
                    // 获取市信息
                    ArrayNode districtsTwo = JsonUtil.getArrayNode(node, "districts");
                    if (ObjectUtil.isNotNull(districtsTwo) && save) {
                        for (JsonNode twoNode : districtsTwo) {
                            DivisionDTO divisionDTOTwo = new DivisionDTO();
                            divisionDTOTwo.setParentId(divisionOne.getId());
                            divisionDTOTwo.setCode(JsonUtil.getNodeStr(twoNode, "adcode"));
                            divisionDTOTwo.setName(JsonUtil.getNodeStr(twoNode, "name"));
                            divisionDTOTwo.setLevel(Constants.COMMON_CODE.TWO);
                            divisionDTOTwo.setRemark(JsonUtil.getNodeStr(twoNode, "center"));
                            Division divisionTwo = OrikaUtil.convert(divisionDTOTwo, Division.class);
                            boolean saveTwo = divisionService.save(divisionTwo);
                            // 获取区县信息
                            ArrayNode districtsThree = JsonUtil.getArrayNode(twoNode, "districts");
                            if (ObjectUtil.isNotEmpty(districtsThree) && saveTwo) {
                                List<DivisionDTO> districtList = new ArrayList<>();
                                for (JsonNode threeNode : districtsThree) {
                                    DivisionDTO divisionDTOThree = new DivisionDTO();
                                    divisionDTOThree.setParentId(divisionTwo.getId());
                                    divisionDTOThree.setCode(JsonUtil.getNodeStr(threeNode, "adcode"));
                                    divisionDTOThree.setName(JsonUtil.getNodeStr(threeNode, "name"));
                                    divisionDTOThree.setLevel(Constants.COMMON_CODE.THREE);
                                    divisionDTOThree.setRemark(JsonUtil.getNodeStr(threeNode, "center"));
                                    districtList.add(divisionDTOThree);
                                }
                                if (CollectionUtil.isNotEmpty(districtList)) {
                                    divisionService.saveBatch(OrikaUtil.convertList(districtList, Division.class));
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("初始化区划数据异常", e);
        }

    }

    /**
     * 获取行政区划信息树形列表
     *
     * @param query
     * @return
     */
    public List<DivisionDTO> getTreeDivisions(DivisionQuery query) {
        List<DivisionDTO> divisionDTOList = divisionService.getDivisions(query);
        if (CollectionUtil.isEmpty(divisionDTOList)) {
            return Collections.emptyList();
        }
        return divisionDTOList.stream()
                .filter(divisionDTO -> ObjectUtil.equals(Constants.COMMON_CODE.LONG_ZONE, divisionDTO.getParentId()))
                .peek(divisionDTO -> divisionDTO.setChildren(getChildren(divisionDTO, divisionDTOList)))
                .sorted(Comparator.comparing(DivisionDTO::getCode))
                .collect(Collectors.toList());
    }

    /**
     * 组装区划树形结构信息
     *
     * @param divisionParentDTO
     * @param divisionDTOList
     * @return
     */
    private List<DivisionDTO> getChildren(DivisionDTO divisionParentDTO, List<DivisionDTO> divisionDTOList) {
        return divisionDTOList.stream().filter(divisionDTO -> ObjectUtil.equals(divisionDTO.getParentId(), divisionParentDTO.getId()))
                .peek(divisionDTO -> divisionDTO.setChildren(getChildren(divisionDTO, divisionDTOList)))
                .sorted(Comparator.comparing(DivisionDTO::getCode))
                .collect(Collectors.toList());
    }

    /**
     * 获取行政区划信息
     *
     * @param id
     * @return
     */
    public DivisionDTO getDivision(Long id) {
        ValidatorUtil.notEmpty(id);
        return divisionService.getDivision(id);
    }

    /**
     * 新增行政区划
     *
     * @param divisionDTO
     */
    public void addDivision(DivisionDTO divisionDTO) {
        ValidatorUtil.notEmpty(divisionDTO);
        // 区划信息校验
        checkDivision(divisionDTO);
        // 保存区划信息
        divisionService.addDivision(divisionDTO);
    }

    /**
     * 更新行政区划
     *
     * @param divisionDTO
     */
    public void updateDivision(DivisionDTO divisionDTO) {
        ValidatorUtil.notEmpty(divisionDTO);
        ValidatorUtil.notEmpty(divisionDTO.getId());
        // 区划是否存在
        ValidatorUtil.notEmpty(getDivision(divisionDTO.getId()), ResultCodeEnum.DIVISION_NOT_EXIST_ERROR);
        // 区划信息校验
        checkDivision(divisionDTO);
        // 更新区划信息
        divisionService.updateDivision(divisionDTO);
    }

    /**
     * 区划相关信息校验
     *
     * @param divisionDTO
     */
    private void checkDivision(DivisionDTO divisionDTO) {
        // 区划信息是否重复(code, name)
        ValidatorUtil.isTrue(divisionService.checkDivisionUnique(divisionDTO), ResultCodeEnum.DIVISION_NOT_REPEAT_ERROR);
        // 父级区划是否存在
        ValidatorUtil.isTrue(divisionService.checkParentDivisionExist(divisionDTO), ResultCodeEnum.DIVISION_PARENT_NOT_EXIST_ERROR);
        // 地区编码长度是否合法(编码长度都是6)
        ValidatorUtil.isTrue(StrUtil.isNotBlank(divisionDTO.getCode()) && divisionDTO.getCode().length() == Constants.COMMON_CODE.SIX, ResultCodeEnum.DIVISION_CODE_ERROR);
        // 非顶级区划不能有地区简称
        ValidatorUtil.isTrue(!(ObjectUtil.notEqual(divisionDTO.getLevel(), Constants.COMMON_CODE.ONE) && StrUtil.isNotBlank(divisionDTO.getShortName())), ResultCodeEnum.DIVISION_SHORT_NAME_ERROR);
    }

    /**
     * 区划名称实时检索
     *
     * @param search
     * @return
     */
    public List<InputRemoteFetchDTO> fetch(String search) {
        return divisionService.fetch(search);
    }

    /**
     * 删除行政区划
     *
     * @param id
     */
    public void deleteDivision(Long id) {
        ValidatorUtil.notEmpty(id);
        // 区划是否存在
        DivisionDTO divisionDTO = getDivision(id);
        ValidatorUtil.notEmpty(divisionDTO, ResultCodeEnum.DIVISION_NOT_EXIST_ERROR);
        // 区划是否存在子级区划信息
        ValidatorUtil.isTrue(divisionService.checkSonDivisionExist(divisionDTO), ResultCodeEnum.DIVISION_SON_EXIST_ERROR);
        // 删除区划信息
        divisionService.deleteDivision(Collections.singletonList(id));
    }

}
