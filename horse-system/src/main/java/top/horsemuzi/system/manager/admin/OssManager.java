package top.horsemuzi.system.manager.admin;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ConfigEnum;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.EncryptUtil;
import top.horsemuzi.common.util.ExceptionUtil;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.core.aspose.enums.ConvertEnum;
import top.horsemuzi.core.oss.enums.StrategyEnum;
import top.horsemuzi.core.oss.service.StrategyAbstract;
import top.horsemuzi.system.config.properties.EncryptProperties;
import top.horsemuzi.system.core.oss.OssFactory;
import top.horsemuzi.system.core.redis.service.RedisService;
import top.horsemuzi.system.pojo.dto.common.InputRemoteFetchDTO;
import top.horsemuzi.system.pojo.dto.oss.*;
import top.horsemuzi.system.service.ConfigService;
import top.horsemuzi.system.service.OssConfigService;
import top.horsemuzi.system.service.OssService;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description 系统文件信息模块
 * @date 2022/7/3 16:35
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class OssManager {

    private final OssService ossService;
    private final ConfigService configService;
    private final OssConfigService ossConfigService;
    private final RedisService redisService;
    private final OssFactory ossFactory;

    /**
     * 系统文件上传通用接口(其他额外参数均为默认值)
     *
     * @param file
     * @return
     */
    public OssDTO upload(MultipartFile file) {
        OssUploadDTO ossUploadDTO = new OssUploadDTO();
        ossUploadDTO.setFile(file);
        return upload(ossUploadDTO);
    }

    /**
     * 系统文件上传通用接口(用于文件管理界面上传文件, 可扩展配置额外参数: 是否系统内置、是否压缩、是否缩放、是否添加水印...)
     *
     * @param ossUploadDTO
     * @return
     */
    public OssDTO upload(OssUploadDTO ossUploadDTO) {
        MultipartFile file = ossUploadDTO.getFile();
        OssDTO ossDTO = new OssDTO();
        try {
            // 上传前置处理: 压缩、添加水印等
            ossDTO.setInternal(ossUploadDTO.getInternal());

            // 上传文件到OSS
            ValidatorUtil.notEmpty(file);
            String originalFilename = file.getOriginalFilename();
            ossDTO.setOriginalName(originalFilename);
            ossDTO.setSuffix(FileUtil.extName(originalFilename));
            InputStream inputStream = file.getInputStream();
            ossDTO.setSize(Convert.toLong(inputStream.available() >> Constants.COMMON_CODE.BASE_INT_TEN));
            StrategyAbstract strategy = ossFactory.instance();
            String url = strategy.upload(inputStream, originalFilename, Boolean.TRUE, Boolean.FALSE);
            ValidatorUtil.notBlank(url, ResultCodeEnum.OSS_SERVICE_UPLOAD_ERROR);
            // 如果服务商是github, 额外存储标识字段: sha
            if (StrUtil.equals(StrategyEnum.GITHUB.getConfigKey(), strategy.properties.getConfigKey())) {
                List<String> githubResult = StrUtil.split(url, StrUtil.COMMA);
                url = githubResult.get(0);
                ossDTO.setSha(githubResult.get(1));
            }
            ossDTO.setUrl(url);
            ossDTO.setService(strategy.properties.getConfigKey());
            ossDTO.setRemark(strategy.properties.getConfigKey());
            ossService.saveOss(ossDTO);
        } catch (Exception e) {
            throw new BusinessException(ResultCodeEnum.OSS_SERVICE_UPLOAD_ERROR);
        }
        return ossDTO;
    }

    /**
     * 分页获取文件列表
     *
     * @param ossQuery
     * @return
     */
    public Page<OssDTO> getPageOss(OssQuery ossQuery) {
        return ossService.getPageOss(ossQuery);
    }

    /**
     * 文件下载
     *
     * @param id
     * @param response
     */
    public void download(Long id, HttpServletResponse response) {
        OssDTO ossDTO = ossService.getOss(id);
        ValidatorUtil.notEmpty(ossDTO, ResultCodeEnum.RESOURCE_NOT_EXIST);
        try {
            ossFactory.instance(ossDTO.getService()).download(response, ossDTO.getUrl(), ossDTO.getOriginalName());
        } catch (Exception e) {
            throw new BusinessException(e, ResultCodeEnum.OSS_SERVICE_DOWNLOAD_ERROR);
        }
    }

    /**
     * 文件删除
     *
     * @param ossIds
     */
    public void delete(List<Long> ossIds) {
        List<OssDTO> ossDTOList = ossService.getOss(ossIds);
        ValidatorUtil.notEmpty(ossDTOList, ResultCodeEnum.RESOURCE_NOT_EXIST);
        // 是否存在内置属性文件（内置属性文件不允许删除）
        for (OssDTO ossDTO : ossDTOList) {
            ValidatorUtil.isTrue(StrUtil.equals(ossDTO.getInternal(), Constants.COMMON_JUDGMENT.N), ResultCodeEnum.OSS_SERVICE_INTERNAL_ERROR);
        }
        try {
            ossService.delete(ossIds);
            // 系统配置：是否同步删除远程资源
            String syncDelete = configService.getConfig(ConfigEnum.OSS_SYNC_DELETE).get(ConfigEnum.OSS_SYNC_DELETE.getKey());
            ValidatorUtil.notBlank(syncDelete, ResultCodeEnum.CONFIG_NOT_EXIST_ERROR);
            if (StrUtil.equals(Constants.COMMON_JUDGMENT.YES, syncDelete)) {
                for (OssDTO ossDTO : ossDTOList) {
                    if (StrUtil.equals(ossDTO.getService(), StrategyEnum.GITHUB.getConfigKey())) {
                        ossFactory.instance(ossDTO.getService()).delete(ossDTO.getUrl(), ossDTO.getSha());
                    }
                    ossFactory.instance(ossDTO.getService()).delete(ossDTO.getUrl(), null);
                }
            }
        } catch (Exception e) {
            throw new BusinessException(ResultCodeEnum.OSS_SERVICE_DELETE_ERROR);
        }
    }

    /**
     * 文件预览 (非图片文件)
     *
     * @param id
     * @param response
     */
    public void preview(Long id, HttpServletResponse response) {
        ValidatorUtil.notEmpty(id);
        OssDTO ossDTO = ossService.getOss(id);
        ValidatorUtil.notEmpty(ossDTO, ResultCodeEnum.RESOURCE_NOT_EXIST);
        InputStream previewStream = null;
        String previewFilePath = null;
        try (InputStream inputStream = new URL(ossDTO.getUrl()).openStream()) {
            if (StrUtil.equalsAny(ossDTO.getSuffix(), "doc", "docx", "txt")) {
                previewFilePath = ConvertEnum.WORD.getConvert().convertPdf(inputStream);
            } else if (StrUtil.equalsAny(ossDTO.getSuffix(), "xls", "xlsx", "csv")) {
                previewFilePath = ConvertEnum.EXCEL.getConvert().convertPdf(inputStream);
            } else if (StrUtil.equalsAny(ossDTO.getSuffix(), "ppt", "pptx")) {
                previewFilePath = ConvertEnum.PPT.getConvert().convertPdf(inputStream);
            } else {
                throw new Exception();
            }
            previewStream = Files.newInputStream(Paths.get(previewFilePath));
            response.setContentType(MediaType.APPLICATION_PDF_VALUE);
            IoUtil.copy(previewStream, response.getOutputStream());
        } catch (Exception e) {
            throw new BusinessException(ResultCodeEnum.OSS_SERVICE_PREVIEW_ERROR);
        } finally {
            IoUtil.close(previewStream);
            // 删除临时文件
            if (StrUtil.isNotBlank(previewFilePath) && FileUtil.exist(previewFilePath)) {
                FileUtil.del(previewFilePath);
            }
        }
    }

    /**
     * 文件更新
     *
     * @param ossDTO
     */
    public void update(OssDTO ossDTO) {
        ValidatorUtil.notEmpty(ossDTO);
        ValidatorUtil.notEmpty(ossDTO.getId());
        OssDTO currentOssDTO = ossService.getOss(ossDTO.getId());
        ValidatorUtil.notEmpty(currentOssDTO, ResultCodeEnum.RESOURCE_NOT_EXIST);
        // 更新文件信息：原始文件名、备注、是否内置属性
        currentOssDTO.setOriginalName(ossDTO.getOriginalName());
        currentOssDTO.setRemark(ossDTO.getRemark());
        currentOssDTO.setInternal(ossDTO.getInternal());
        ossService.updateOss(currentOssDTO);
    }

    /**
     * 文件名称实时检索
     *
     * @param search
     * @return
     */
    public List<InputRemoteFetchDTO> fetch(String search) {
        return ossService.fetch(search);
    }

    /**
     * 分页获取OSS对象存储配置列表
     *
     * @param query
     * @return
     */
    public Page<OssConfigDTO> getPageOssConfig(OssConfigQuery query) {
        Page<OssConfigDTO> page = ossConfigService.getPageOssConfig(query);
        List<OssConfigDTO> ossConfigDTOList = page.getRecords();
        if (CollUtil.isNotEmpty(ossConfigDTOList)) {
            for (OssConfigDTO ossConfigDTO : ossConfigDTOList) {
                ossConfigDTO.setSecretKey(EncryptUtil.sm4Encrypt(EncryptProperties.sm4Key, ossConfigDTO.getSecretKey()));
            }
        }
        return page;
    }


    /**
     * 获取OSS对象存储配置详情
     *
     * @param id
     * @return
     */
    public OssConfigDTO getOssConfig(Long id) {
        ValidatorUtil.notEmpty(id);
        // 配置是否存在
        OssConfigDTO configDTO = ossConfigService.getOssConfig(id);
        ValidatorUtil.notEmpty(configDTO, ResultCodeEnum.OSS_CONFIG_NOT_EXIST_ERROR);
        // 先缓存后DB
        String cacheKey = Constants.REDIS_PREFIX.OSS_CONFIG + StrUtil.COLON + configDTO.getConfigKey();
        if (redisService.hasKey(cacheKey)) {
            String configJson = redisService.get(cacheKey);
            return JsonUtil.fromJson(configJson, OssConfigDTO.class);
        }
        OssConfigDTO ossConfigDTO = ossConfigService.getOssConfig(id);
        ValidatorUtil.notEmpty(ossConfigDTO, ResultCodeEnum.OSS_CONFIG_NOT_EXIST_ERROR);
        ossConfigDTO.setSecretKey(EncryptUtil.sm4Encrypt(EncryptProperties.sm4Key, ossConfigDTO.getSecretKey()));
        redisService.set(cacheKey, JsonUtil.toJson(ossConfigDTO));
        return ossConfigDTO;
    }

    /**
     * 新增OSS对象存储配置
     *
     * @param ossConfigDTO
     */
    public void addOssConfig(OssConfigDTO ossConfigDTO) {
        ValidatorUtil.notEmpty(ossConfigDTO);
        ossConfigDTO.setSecretKey(EncryptUtil.sm4Decrypt(EncryptProperties.sm4Key, ossConfigDTO.getSecretKey()));
        ossConfigService.addOssConfig(ossConfigDTO);
    }

    /**
     * 更新OSS对象存储配置
     *
     * @param ossConfigDTO
     */
    public void updateOssConfig(OssConfigDTO ossConfigDTO) {
        ValidatorUtil.notEmpty(ossConfigDTO);
        ValidatorUtil.notEmpty(ossConfigDTO.getId());
        ValidatorUtil.notEmpty(ossConfigService.getOssConfig(ossConfigDTO.getId()), ResultCodeEnum.OSS_CONFIG_NOT_EXIST_ERROR);
        ossConfigDTO.setSecretKey(EncryptUtil.sm4Decrypt(EncryptProperties.sm4Key, ossConfigDTO.getSecretKey()));
        if (ossConfigService.updateOssConfig(ossConfigDTO)) {
            // 移除缓存
            String cacheKey = Constants.REDIS_PREFIX.OSS_CONFIG + StrUtil.COLON + ossConfigDTO.getConfigKey();
            if (redisService.hasKey(cacheKey)) {
                redisService.delete(cacheKey);
            }
        }
    }

    /**
     * 删除OSS对象存储配置
     *
     * @param ossConfigIds
     */
    public void deleteOssConfig(List<Long> ossConfigIds) {
        ValidatorUtil.notEmpty(ossConfigIds);
        List<OssConfigDTO> ossConfigDTOList = ossConfigService.getOssConfig(ossConfigIds);
        ValidatorUtil.isTrue(CollUtil.isNotEmpty(ossConfigDTOList) && ossConfigDTOList.size() == ossConfigIds.size(),
                ResultCodeEnum.OSS_CONFIG_NOT_EXIST_ERROR);
        if (ossConfigService.deleteOssConfig(ossConfigIds)) {
            // 移除缓存
            List<String> cacheKeys = ossConfigDTOList.stream()
                    .map(configDTO -> Constants.REDIS_PREFIX.OSS_CONFIG + StrUtil.COLON + configDTO.getConfigKey())
                    .collect(Collectors.toList());
            redisService.delete(cacheKeys);
        }
    }

    /**
     * 导出OSS对象存储配置 EXCEL 数据
     *
     * @param query
     * @param response
     */
    public void export(OssConfigQuery query, HttpServletResponse response) {
        ossConfigService.export(query, response);
    }

    /**
     * 实时检索
     *
     * @param search 检索关键词
     * @return
     */
    public List<InputRemoteFetchDTO> fetchConfig(String search) {
        return ossConfigService.fetchConfig(search);
    }

}
