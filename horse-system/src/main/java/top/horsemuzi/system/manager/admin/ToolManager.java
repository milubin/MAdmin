package top.horsemuzi.system.manager.admin;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.compress.CompressUtil;
import cn.hutool.extra.compress.extractor.Extractor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;
import org.reflections.util.ConfigurationBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.RestController;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.GanymedSSH2Util;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.annotation.Anonymous;
import top.horsemuzi.system.annotation.Check;
import top.horsemuzi.system.pojo.dto.permission.PermissionDTO;
import top.horsemuzi.system.pojo.dto.permission.PermissionQuery;
import top.horsemuzi.system.service.PermissionService;

import java.io.File;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 系统工具模块
 *
 * @author mabin
 * @date 2022/09/30 09:36
 **/
@Slf4j
@RequiredArgsConstructor
@Component
public class ToolManager {

    @Value("{spring.profiles.active}")
    private String env;

    @Value("${custom.server.mysql.backup.host}")
    private String host;

    @Value("${custom.server.mysql.backup.username}")
    private String username;

    @Value("${custom.server.mysql.backup.password}")
    private String password;

    @Value("${custom.server.mysql.backup.command}")
    private String command;

    @Value("${custom.server.mysql.backup.sourceDir}")
    private String sourceDir;

    @Value("${custom.server.mysql.backup.targetDir}")
    private String targetDir;

    private final PermissionService permissionService;


    /**
     * 远程执行mysql备份(dev环境才允许执行远程备份)
     */
    public void execMysqlBackup() {
        // 是否dev环境
        // ValidatorUtil.isTrue(StrUtil.isNotBlank(env) && StrUtil.equals("dev", env), ResultCodeEnum.BACKUP_NOT_DEV_ENVIRONMENT_ERROR);
        log.info("开始远程备份、下载、解压数据库文件");
        StopWatch watch = new StopWatch("远程操作");
        watch.start("远程备份");
        // 远程备份
        String fileName = GanymedSSH2Util.exec(GanymedSSH2Util.connection(host, null, username, password), command);
        watch.stop();
        watch.start("远程下载");
        // 拉取远程文件
        GanymedSSH2Util.download(GanymedSSH2Util.connection(host, null, username, password), sourceDir + fileName, targetDir);
        watch.stop();
        watch.start("本地解压");
        // 解压远程文件
        File targetFile = new File(targetDir + fileName);
        if (targetFile.exists()) {
            Extractor extractor = CompressUtil.createExtractor(CharsetUtil.CHARSET_UTF_8, "tar.gz", targetFile);
            extractor.extract(new File(targetDir));
            // 删除当前压缩包
            FileUtil.del(targetFile);
        }
        watch.stop();
        // 保留最近2次的备份文件
        List<File> files = FileUtil.loopFiles(targetDir);
        if (CollectionUtil.isNotEmpty(files) && files.size() > Constants.COMMON_CODE.BASE_INT_TWO) {
            watch.start("移除历史备份");
            files = files.stream().sorted().collect(Collectors.toList());
            for (int i = 0; i < files.size() - Constants.COMMON_CODE.BASE_INT_TWO; i++) {
                File file = files.get(i);
                log.info("移除备份SQL脚本 fileName={}", file.getName());
                FileUtil.del(file);
            }
            watch.stop();
        }
        log.info("远程备份、下载、解压数据库文件耗时预览: \n{}", watch.prettyPrint());
    }


    /**
     * 比对本地权限数据
     *
     * @param
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     * @author mabin
     * @date 2023/3/7 9:02
     **/
    public List<Map<String, Object>> diffPermissions() {
        List<Map<String, Object>> result = new ArrayList<>();
        List<PermissionDTO> permissionDTOList = permissionService.getPermissionMenus(new PermissionQuery());
        Set<String> permSet = permissionDTOList.stream().filter(permission -> StrUtil.equalsAny(permission.getType(), Constants.MENU.TYPE_MENU, Constants.MENU.TYPE_BUTTON)
                && StrUtil.isNotBlank(permission.getPerms()))
                .map(PermissionDTO::getPerms)
                .collect(Collectors.toSet());
        Reflections ref = new Reflections(new ConfigurationBuilder().forPackage("top.horsemuzi.system.controller"));
        Set<Class<?>> classSet = ref.getTypesAnnotatedWith(RestController.class);
        if (CollUtil.isEmpty(classSet)) {
            return result;
        }
        for (Class<?> clazz : classSet) {
            Method[] methods = clazz.getDeclaredMethods();
            if (ArrayUtil.isEmpty(methods)) {
                continue;
            }
            for (Method method : methods) {
                if (!method.isAnnotationPresent(Check.class) || method.isAnnotationPresent(Anonymous.class)) {
                    continue;
                }
                Check check = method.getAnnotation(Check.class);
                String[] permissions = check.permissions();
                if (ArrayUtil.isNotEmpty(permissions) && !permSet.contains(permissions[Constants.COMMON_CODE.BASE_INT_ZONE])) {
                    Map<String, Object> map = new HashMap<>(6);
                    map.put("className", clazz.getName());
                    map.put("methodName", method.getName());
                    map.put("permission", permissions[Constants.COMMON_CODE.BASE_INT_ZONE]);
                    result.add(map);
                }
            }
        }
        return result;
    }
}
