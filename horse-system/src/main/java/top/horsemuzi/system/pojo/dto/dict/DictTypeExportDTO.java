package top.horsemuzi.system.pojo.dto.dict;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 数据字典导出相关实例
 *
 * @author mabin
 * @date 2022/09/27 13:54
 **/

@Data
public class DictTypeExportDTO implements Serializable {

    private static final long serialVersionUID = -3263468289575097500L;

    @Excel(name = "字典名称", width = 20, needMerge = true)
    @ApiModelProperty("字典名称")
    private String name;

    @Excel(name = "字典类型", width = 20, needMerge = true)
    @ApiModelProperty("字典类型")
    private String type;

    @Excel(name = "状态", replace = {"正常_0", "停用_1"}, needMerge = true)
    @ApiModelProperty("状态: 0-正常 1-停用")
    private String status;

    @Excel(name = "内置", replace = {"是_Y", "否_N"}, needMerge = true)
    @ApiModelProperty("是否内置: Y-是, N-否")
    private String internal;

    @Excel(name = "备注", width = 40, needMerge = true)
    @ApiModelProperty("备注")
    private String remark;

    @ExcelCollection(name = "字典数据信息")
    @ApiModelProperty("字典数据列表")
    private List<DictDataExportDTO> dictDataExportDTOList;

}
