package top.horsemuzi.system.pojo.dto.open.api;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseQuery;

/**
 * 开放Api接口查询
 *
 * @author mabin
 * @date 2022/11/23 14:26
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class ApiQuery extends BaseQuery {

    private static final long serialVersionUID = 7889747328027970642L;

    @ApiModelProperty("API标识code")
    private String apiCode;

    @ApiModelProperty("API功能")
    private String apiFunction;

    @ApiModelProperty("API状态: 0-正常, 1-已下线")
    private String apiStatus;

}
