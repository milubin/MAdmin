package top.horsemuzi.system.pojo.dto.notice;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.hutool.core.date.DatePattern;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 通知公告发布记录导出DTO
 *
 * @author 马滨
 * @date 2022/10/06 16:03
 **/
@Data
public class NoticeRecordExportDTO implements Serializable {

    private static final long serialVersionUID = -8029683754985117525L;

    @Excel(name = "接收用户", width = 20)
    @ApiModelProperty("接收用户")
    private String username;

    @Excel(name = "读取状态", replace = {"未读_0", "已读_1"})
    @ApiModelProperty("是否已读：0-未读，1-已读")
    private String status;

    @Excel(name = "读取时间", width = 20, format = DatePattern.NORM_DATETIME_PATTERN)
    @ApiModelProperty("读取时间")
    private Date readTime;

    @Excel(name = "备注", width = 40, needMerge = true)
    @ApiModelProperty("备注")
    private String remark;

    @Excel(name = "创建时间", width = 20, format = DatePattern.NORM_DATETIME_PATTERN)
    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @Excel(name = "是否已删除", replace = {"正常_0", "已删除_1"}, width = 20)
    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    private Boolean deleted;

}
