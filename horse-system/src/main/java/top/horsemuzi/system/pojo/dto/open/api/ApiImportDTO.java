package top.horsemuzi.system.pojo.dto.open.api;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import top.horsemuzi.system.annotation.DictFormat;
import top.horsemuzi.system.core.office.excel.convert.DictConvert;

import java.io.Serializable;

/**
 * Api接口导入DTO
 *
 * @author mabin
 * @date 2022/11/29 09:48
 **/
@Data
@NoArgsConstructor
public class ApiImportDTO implements Serializable {
    private static final long serialVersionUID = -500753057840687103L;

    @ExcelProperty(value = {"API类型名称"})
    @ApiModelProperty(value = "API类型名称")
    private String categoryName;

    @ExcelProperty(value = {"API标识"})
    @ApiModelProperty("API标识")
    private String apiCode;

    @ExcelProperty(value = {"API类路径"})
    @ApiModelProperty("API类路径")
    private String apiClass;

    @ExcelProperty(value = {"API方法名"})
    @ApiModelProperty("API方法名")
    private String apiMethod;

    @ExcelProperty(value = {"API功能名称"})
    @ApiModelProperty("API功能名称")
    private String apiFunction;

    @ExcelProperty(value = {"API描述"})
    @ApiModelProperty("API描述")
    private String apiDescription;

    @ExcelProperty(value = {"日调用阈值（APP粒度）"})
    @ApiModelProperty("日请求阈值(APP粒度)")
    private Long dailyRequestLimit;

    @DictFormat(type = "sys_status")
    @ExcelProperty(value = {"API状态"}, converter = DictConvert.class)
    @ApiModelProperty("API状态: 0-正常, 1-停用")
    private String apiStatus;

    @ExcelProperty(value = {"备注"})
    @ApiModelProperty("备注")
    private String remark;

}
