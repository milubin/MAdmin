package top.horsemuzi.system.pojo.dto.open.api;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.hutool.core.date.DatePattern;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 开放API接口导出DTO
 *
 * @author mabin
 * @date 2022/11/23 18:37
 **/
@Data
public class ApiExportDTO implements Serializable {
    private static final long serialVersionUID = -4496378750990380308L;

    @Excel(name = "API功能名称", width = 30)
    @ApiModelProperty("API功能名称")
    private String apiFunction;

    @Excel(name = "API类型名称", width = 20)
    @ApiModelProperty("API类型名称")
    private String categoryName;

    @Excel(name = "API标识", width = 20)
    @ApiModelProperty("API标识code")
    private String apiCode;

    @Excel(name = "API类路径", width = 50)
    @ApiModelProperty("API类路径")
    private String apiClass;

    @Excel(name = "API方法名", width = 20)
    @ApiModelProperty("API方法名")
    private String apiMethod;

    @Excel(name = "API描述", width = 50)
    @ApiModelProperty("API描述")
    private String apiDescription;

    @Excel(name = "API版本")
    @ApiModelProperty("API版本，默认v1")
    private String apiVersion;

    @Excel(name = "API状态", replace = {"正常_0", "停用_1"})
    @ApiModelProperty("API状态: 0-正常, 1-停用")
    private String apiStatus;

    @Excel(name = "请求阈值", suffix = "次/日")
    @ApiModelProperty("日请求阈值(APP粒度)")
    private Long dailyRequestLimit;

    @Excel(name = "备注", width = 50)
    @ApiModelProperty("备注")
    private String remark;

    @Excel(name = "创建时间", width = 20, format = DatePattern.NORM_DATETIME_PATTERN)
    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @Excel(name = "创建人", width = 20)
    @ApiModelProperty("创建人")
    private String createUser;

    @Excel(name = "更新时间", width = 20, format = DatePattern.NORM_DATETIME_PATTERN)
    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @Excel(name = "更新人", width = 20)
    @ApiModelProperty("更新人")
    private String modifiedUser;

}
