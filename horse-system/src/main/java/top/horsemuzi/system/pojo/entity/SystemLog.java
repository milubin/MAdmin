package top.horsemuzi.system.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseEntity;

import java.util.Date;

/**
 * <p>
 * 系统操作日志
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-06-30 20:35:31
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("tb_system_log")
@ApiModel(value = "SystemLog对象", description = "系统操作日志")
public class SystemLog extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("链路ID")
    @TableField("trace_id")
    private String traceId;

    @ApiModelProperty("日志类型: 0-操作日志, 1-异常日志")
    @TableField("log_type")
    private Integer logType;

    @ApiModelProperty("线程名称")
    @TableField("thread_name")
    private String threadName;

    @ApiModelProperty("操作模块")
    @TableField("module")
    private String module;

    @ApiModelProperty("操作类型")
    @TableField("type")
    private String type;

    @ApiModelProperty("操作描述")
    @TableField("description")
    private String description;

    @ApiModelProperty("用户id")
    @TableField("user_id")
    private Long userId;

    @ApiModelProperty("用户名")
    @TableField("username")
    private String username;

    @ApiModelProperty("操作方法")
    @TableField("method")
    private String method;

    @ApiModelProperty("param参数")
    @TableField("params")
    private String params;

    @ApiModelProperty("body参数")
    @TableField("body")
    private String body;

    @ApiModelProperty("异常名称")
    @TableField("exc_name")
    private String excName;

    @ApiModelProperty("异常详情")
    @TableField("exc_message")
    private String excMessage;

    @ApiModelProperty("访问IP")
    @TableField("ip")
    private String ip;

    @ApiModelProperty("ip归属地")
    @TableField("area")
    private String area;

    @ApiModelProperty("终端系统")
    @TableField("os")
    private String os;

    @ApiModelProperty("浏览器")
    @TableField("browser")
    private String browser;

    @ApiModelProperty("是否移动端: 0-是, 1-否")
    @TableField("is_mobile")
    private String mobile;

    @ApiModelProperty("请求方法类型")
    @TableField("http_type")
    private String httpType;

    @ApiModelProperty("访问URL地址")
    @TableField("url")
    private String url;

    @ApiModelProperty("访问来源: 0-管理系统, 1-门户网站")
    @TableField("origin")
    private String origin;

    @ApiModelProperty("操作状态: 0-成功, 1-异常")
    @TableField("status")
    private String status;

    @ApiModelProperty("响应结果")
    @TableField("data")
    private String data;

    @ApiModelProperty("状态码")
    @TableField("code")
    private Integer code;

    @ApiModelProperty("操作耗时, 单位:ms")
    @TableField("consume")
    private Long consume;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    @TableField("is_deleted")
    @TableLogic
    private Boolean deleted;

    @ApiModelProperty("创建时间")
    @TableField(value = "gmt_create", fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    @TableField(value = "create_id", fill = FieldFill.INSERT)
    private Long createId;

    @ApiModelProperty("创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty("更新时间")
    @TableField(value = "gmt_modified", fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    @TableField(value = "modified_id", fill = FieldFill.INSERT_UPDATE)
    private Long modifiedId;

    @ApiModelProperty("更新人")
    @TableField(value = "modified_user", fill = FieldFill.INSERT_UPDATE)
    private String modifiedUser;


}
