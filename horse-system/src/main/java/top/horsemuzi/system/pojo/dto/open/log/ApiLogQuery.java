package top.horsemuzi.system.pojo.dto.open.log;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseQuery;

/**
 * 开放API请求日志查询实例
 *
 * @author 马滨
 * @date 2022/11/21 22:29
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class ApiLogQuery extends BaseQuery {
    private static final long serialVersionUID = -5425676343692750461L;

    @ApiModelProperty("链路ID")
    private String traceId;

    @ApiModelProperty("应用名称")
    private String appName;

    @ApiModelProperty("API功能名称")
    private String apiFunction;

    @ApiModelProperty("API标识code")
    private String apiCode;

    @ApiModelProperty("请求ip")
    private String ip;

}
