package top.horsemuzi.system.pojo.dto.generate;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;
import top.horsemuzi.system.pojo.dto.permission.PermissionDTO;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * 业务表DTO
 *
 * @author mabin
 * @date 2022/12/19 13:58
 **/
@Data
public class TableDTO implements Serializable {
    private static final long serialVersionUID = 7829598598470175966L;

    @NotNull(message = "业务表主键id不能为为空", groups = {ValidTwo.class})
    @ApiModelProperty("主键id")
    private Long id;

    @NotBlank(message = "业务表导入类型", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("业务表导入类型: 0-基于DB导入, 1-基于DDL导入")
    private String importType;

    @NotNull(message = "业务表配置id不能为为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("业务表配置id")
    private Long tableConfigId;

    @NotNull(message = "数据源id不能为为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("数据源id")
    private Long datasourceId;

    @NotNull(message = "父权限菜单id不能为为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("父权限菜单id")
    private Long parentMenuId;

    @ApiModelProperty("角色权限标识")
    private String roleCode;

    @ApiModelProperty("菜单图标")
    private String menuIcon;

    @NotBlank(message = "数据库名不能为为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("数据库名")
    private String databaseName;

    @ApiModelProperty("表名")
    private String tableName;

    @ApiModelProperty("表描述")
    private String tableComment;

    @ApiModelProperty("实体类名")
    private String className;

    @ApiModelProperty("功能描述")
    private String functionName;

    @ApiModelProperty(value = "DDL语句", notes = "只有importType=1时, 此值才不为空")
    private String tableDdl;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    private Boolean deleted;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;

    @ApiModelProperty("数据源名称")
    private String dbName;

    @ApiModelProperty("业务表配置功能作者")
    private String author;

    @ApiModelProperty("业务表字段列表")
    private List<TableColumnDTO> tableColumnDTOList;

    @ApiModelProperty("业务表主键字段")
    private TableColumnDTO primaryTableColumnDTO;

    @ApiModelProperty("数据源信息")
    private DatasourceDTO datasourceDTO;

    @ApiModelProperty("业务表配置信息")
    private TableConfigDTO tableConfigDTO;

    @ApiModelProperty(value = "权限菜单主键id列表(Long)", notes = "主要包含自增id、雪花算法等")
    private List<Long> longMenuIds;

    @ApiModelProperty(value = "权限菜单主键id列表(String)", notes = "主要包含UUID")
    private List<String> strMenuIds;

    @ApiModelProperty("引入基础依赖包路径")
    private Set<String> importPackages;

    @ApiModelProperty("父权限菜单信息")
    private PermissionDTO permissionDTO;

}
