package top.horsemuzi.system.pojo.dto.schedule.quartz;

import cn.hutool.core.date.DatePattern;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * Quartz任务调度实例
 *
 * @author mabin
 * @date 2022/07/02 09:57
 **/

@NoArgsConstructor
@Data
public class QuartzJobDTO implements Serializable {

    private static final long serialVersionUID = -3265256667223060142L;

    @NotNull(message = "主键id不能为空", groups = {ValidTwo.class})
    @ApiModelProperty("主键id")
    private Long id;

    @NotNull(message = "任务模式不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("任务模式: 0-cron表达式执行模式, 1-间隔执行模式")
    private Integer jobModel;

    @NotNull(message = "任务状态不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("任务状态：0-正常， 1-暂停")
    private Integer jobStatus;

    @NotBlank(message = "任务名称不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("任务名称")
    private String jobName;

    @NotBlank(message = "任务组别不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("任务组别")
    private String jobGroup;

    @NotBlank(message = "调度任务目标方法不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("调度任务目标方法")
    private String invokeTarget;

    @NotNull(message = "是否允许并发不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("是否允许并发执行：0-允许，1-禁止")
    private Integer concurrent;

    @NotNull(message = "计划执行错误策略不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("计划执行错误策略：1-立即执行，2-执行一次，3-放弃执行")
    private Integer misfirePolicy;

    @NotBlank(message = "任务备注不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("任务备注")
    private String remark;

    @NotBlank(message = "触发器名称不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("触发器名称")
    private String triggerName;

    @NotBlank(message = "触发器组别不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("触发器组别")
    private String triggerGroup;

    @ApiModelProperty("cron表达式")
    private String cronExpression;

    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @ApiModelProperty("开始时间")
    private Date startTime;

    @ApiModelProperty("任务执行间隔(每隔多少秒执行一次任务)")
    private Integer jobInterval;

    @ApiModelProperty("任务间隔模式运行的次数 （<0:表示不限次数）")
    private Integer repeatCount;

    @ApiModelProperty("触发器状态")
    private String triggerState;

    @ApiModelProperty("触发器类型")
    private String triggerType;

    @ApiModelProperty("上次执行时间")
    private String prevFireTime;

    @ApiModelProperty("下次执行时间")
    private String nextFireTime;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;

}
