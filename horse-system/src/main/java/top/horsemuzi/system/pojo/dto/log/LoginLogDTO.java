package top.horsemuzi.system.pojo.dto.log;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelIgnore;
import cn.hutool.core.date.DatePattern;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 登录日志DTO
 *
 * @author 马滨
 * @date 2022/09/26 21:53
 **/

@Data
public class LoginLogDTO implements Serializable {

    private static final long serialVersionUID = -6661884738135617257L;

    @ExcelIgnore
    @ApiModelProperty("主键id")
    private Long id;

    @Excel(name = "链路ID")
    @ApiModelProperty("链路ID")
    private String traceId;

    @Excel(name = "线程名称")
    @ApiModelProperty("线程名称")
    private String threadName;

    @Excel(name = "用户名")
    @ApiModelProperty("用户名")
    private String username;

    @Excel(name = "登录方式", replace = {"账号密码_0", "邮箱登录_1", "手机号登录_2"})
    @ApiModelProperty("登录方式：0-账号密码，1-邮箱登录，2-手机号登录")
    private Integer loginWay;

    @Excel(name = "body参数", width = 150)
    @ApiModelProperty("body参数")
    private String body;

    @Excel(name = "异常名称")
    @ApiModelProperty("异常名称")
    private String excName;

    @Excel(name = "异常详情", width = 200)
    @ApiModelProperty("异常详情")
    private String excMessage;

    @Excel(name = "访问IP")
    @ApiModelProperty("访问IP")
    private String ip;

    @Excel(name = "归属地")
    @ApiModelProperty("ip归属地")
    private String area;

    @Excel(name = "终端系统")
    @ApiModelProperty("终端系统")
    private String os;

    @Excel(name = "浏览器")
    @ApiModelProperty("浏览器")
    private String browser;

    @Excel(name = "移动端", replace = {"是_0", "否_1"})
    @ApiModelProperty("是否移动端: 0-是, 1-否")
    private String mobile;

    @Excel(name = "登录状态", replace = {"成功_0", "异常_1"})
    @ApiModelProperty("登录状态: 0-成功, 1-异常")
    private String status;

    @Excel(name = "用户来源", replace = {"MAdmin_0", "MBlog_1"})
    @ApiModelProperty("访问来源: 0-管理系统, 1-门户网站")
    private String origin;

    @ExcelIgnore
    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    private Boolean deleted;

    @Excel(name = "备注")
    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;

    @Excel(name = "登录时间", format = DatePattern.NORM_DATETIME_PATTERN)
    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ExcelIgnore
    @ApiModelProperty("创建人id")
    private Long createId;

    @ExcelIgnore
    @ApiModelProperty("创建人")
    private String createUser;

    @ExcelIgnore
    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ExcelIgnore
    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ExcelIgnore
    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ExcelIgnore
    @ApiModelProperty("乐观锁")
    private Long version;
}
