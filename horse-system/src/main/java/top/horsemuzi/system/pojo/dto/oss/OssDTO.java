package top.horsemuzi.system.pojo.dto.oss;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.horsemuzi.common.validator.ValidOne;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description {description}
 * @date 2022/8/1 21:56
 */
@Data
public class OssDTO implements Serializable {

    private static final long serialVersionUID = 1955002039506544555L;

    @NotNull(message = "主键ID不能为空", groups = {ValidOne.class})
    @ApiModelProperty("主键id")
    private Long id;

    @NotBlank(message = "原始文件名不能为空", groups = {ValidOne.class})
    @ApiModelProperty("原始文件名")
    private String originalName;

    @ApiModelProperty("文件大小，单位：KB")
    private Long size;

    @ApiModelProperty("文件后缀")
    private String suffix;

    @ApiModelProperty("文件URL访问链接")
    private String url;

    @ApiModelProperty("对象存储服务商")
    private String service;

    @ApiModelProperty("文件备注")
    private String remark;

    @ApiModelProperty("GITHUB服务商特有扩展字段（用于文件删除）")
    private String sha;

    @NotBlank(message = "是否内置属性不能为空", groups = {ValidOne.class})
    @ApiModelProperty("是否内置: Y-是, N-否")
    private String internal;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    private Boolean deleted;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;
}
