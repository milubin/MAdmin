package top.horsemuzi.system.pojo.dto.generate;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseQuery;

/**
 * 字段类型关联Query
 *
 * @author mabin
 * @date 2022/12/19 15:28
 **/

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class ColumnConfigQuery extends BaseQuery {
    private static final long serialVersionUID = -8120066567184919169L;

    @ApiModelProperty("DB字段类型")
    private String columnType;

    @ApiModelProperty("JAVA字段类型")
    private String propertyType;

}
