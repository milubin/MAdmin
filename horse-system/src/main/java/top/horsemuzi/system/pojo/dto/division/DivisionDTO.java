package top.horsemuzi.system.pojo.dto.division;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 国家区划信息实例
 *
 * @author 马滨
 * @date 2022/11/06 10:35
 **/
@Data
public class DivisionDTO implements Serializable {

    private static final long serialVersionUID = 5248422408110787787L;

    @NotNull(message = "主键id不能为空", groups = {ValidTwo.class})
    @ApiModelProperty("主键id")
    private Long id;

    @NotNull(message = "地区关联父id不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("地区关联父id，默认0")
    private Long parentId;

    @NotBlank(message = "地区编号不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("地区编号")
    private String code;

    @NotBlank(message = "地区名称不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("地区名称")
    private String name;

    @ApiModelProperty("地区名称缩写")
    private String shortName;

    @ApiModelProperty("经纬度坐标")
    private String coordinate;

    @NotNull(message = "地区级别不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("地区级别 1-省、自治区、直辖市 2-地级市、地区、自治州、盟 3-市辖区、县级市、县")
    private Integer level;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    private Boolean deleted;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;

    @ApiModelProperty("子类实例集合")
    private List<DivisionDTO> children;

}
