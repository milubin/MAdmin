package top.horsemuzi.system.pojo.dto.oss;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import top.horsemuzi.system.base.BaseQuery;

/**
 * OSS文件查询条件实例
 *
 * @author mabin
 * @date 2022/08/02 09:37
 **/
@EqualsAndHashCode(callSuper = true)
@Data
public class OssQuery extends BaseQuery {
    private static final long serialVersionUID = -7282553653057950160L;

    @ApiModelProperty("文件名")
    private String name;

    @ApiModelProperty("原始文件名")
    private String originalName;

    @ApiModelProperty("文件后缀")
    private String suffix;

    @ApiModelProperty("对象存储服务商")
    private String service;

    @ApiModelProperty("是否内置: Y-是, N-否")
    private String internal;

}
