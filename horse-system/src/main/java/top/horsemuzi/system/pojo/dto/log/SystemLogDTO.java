package top.horsemuzi.system.pojo.dto.log;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelIgnore;
import cn.hutool.core.date.DatePattern;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统操作日志
 *
 * @author mabin
 * @date 2022/06/30 20:37
 **/

@Data
public class SystemLogDTO implements Serializable {

    private static final long serialVersionUID = -3887138242972172034L;

    @ExcelIgnore
    @ApiModelProperty("主键ID")
    private Long id;

    @ExcelIgnore
    @ApiModelProperty("日志类型: 0-操作日志, 1-异常日志")
    private Integer logType;

    @Excel(name = "链路ID")
    @ApiModelProperty("链路ID")
    private String traceId;

    @Excel(name = "线程名称")
    @ApiModelProperty("线程名称")
    private String threadName;

    @Excel(name = "操作模块")
    @ApiModelProperty("操作模块")
    private String module;

    @ExcelIgnore
    @ApiModelProperty("操作类型")
    private String type;

    @Excel(name = "操作描述")
    @ApiModelProperty("操作描述")
    private String description;

    @ExcelIgnore
    @ApiModelProperty("用户id")
    private Long userId;

    @Excel(name = "操作方法")
    @ApiModelProperty("操作方法")
    private String method;

    @ExcelIgnore
    @ApiModelProperty("param参数")
    private String params;

    @ExcelIgnore
    @ApiModelProperty("body参数")
    private String body;

    @ExcelIgnore
    @ApiModelProperty("异常名称")
    private String excName;

    @ExcelIgnore
    @ApiModelProperty("异常详情")
    private String excMessage;

    @Excel(name = "访问IP")
    @ApiModelProperty("访问IP")
    private String ip;

    @Excel(name = "归属地")
    @ApiModelProperty("ip归属地")
    private String area;

    @Excel(name = "终端系统")
    @ApiModelProperty("终端系统")
    private String os;

    @Excel(name = "浏览器")
    @ApiModelProperty("浏览器")
    private String browser;

    @ExcelIgnore
    @ApiModelProperty("是否移动端: 0-是, 1-否")
    private String mobile;

    @Excel(name = "请求方法类型")
    @ApiModelProperty("请求方法类型")
    private String httpType;

    @Excel(name = "访问URL地址")
    @ApiModelProperty("访问URL地址")
    private String url;

    @ExcelIgnore
    @ApiModelProperty("访问来源: 0-管理系统, 1-门户网站")
    private String origin;

    @Excel(name = "操作状态", replace = {"成功_0", "异常_1"})
    @ApiModelProperty("操作状态: 0-成功, 1-异常")
    private String status;

    @ExcelIgnore
    @ApiModelProperty("响应结果")
    private String data;

    @Excel(name = "状态码", type = 10)
    @ApiModelProperty("状态码")
    private Integer code;

    @Excel(name = "操作耗时", type = 10, suffix = "ms")
    @ApiModelProperty("操作耗时, 单位:ms")
    private Long consume;

    @Excel(name = "用户名")
    @ApiModelProperty("用户名")
    private String username;

    @Excel(name = "创建时间", format = DatePattern.NORM_DATETIME_PATTERN)
    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ExcelIgnore
    @ApiModelProperty("创建人id")
    private Long createId;

    @ExcelIgnore
    @ApiModelProperty("创建人")
    private String createUser;

    @ExcelIgnore
    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ExcelIgnore
    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ExcelIgnore
    @ApiModelProperty("更新人")
    private String modifiedUser;
}
