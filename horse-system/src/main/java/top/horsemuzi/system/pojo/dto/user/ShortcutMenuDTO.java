package top.horsemuzi.system.pojo.dto.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 用户快捷菜单DTO
 *
 * @author mabin
 * @date 2023/01/07 15:38
 **/

@Data
public class ShortcutMenuDTO implements Serializable {

    private static final long serialVersionUID = 1803038816587389906L;

    @NotNull(message = "主键id不能为空", groups = {ValidTwo.class})
    @ApiModelProperty("主键id")
    private Long id;

    @ApiModelProperty("用户id")
    private Long userId;

    @NotBlank(message = "快捷菜单名称不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("快捷菜单名称")
    private String menuName;

    @NotBlank(message = "快捷菜单组件路径不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("快捷菜单组件路径")
    private String menuComponent;

    @ApiModelProperty("快捷菜单图标")
    private String menuIcon;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    private Boolean deleted;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;
}
