package top.horsemuzi.system.pojo.dto.permission;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;
import top.horsemuzi.system.pojo.entity.Permission;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 权限菜单实例DTO
 *
 * @author mabin
 * @date 2022/07/07 17:36
 **/

@NoArgsConstructor
@Data
public class PermissionDTO implements Serializable {
    private static final long serialVersionUID = -4435382057173532261L;

    @NotNull(message = "主键id不能为空", groups = {ValidTwo.class})
    @ApiModelProperty("主键id")
    private Long id;

    @NotBlank(message = "菜单名称不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("菜单名称")
    private String name;

    @NotNull(message = "父主键id不能为空", groups = {ValidTwo.class})
    @ApiModelProperty("父主键id")
    private Long parentId;

    @ApiModelProperty("父菜单名称")
    private String parentName;

    @NotNull(message = "菜单显示顺序不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("显示顺序")
    private Integer sort;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    private Boolean deleted;

    @ApiModelProperty("路由地址")
    private String routePath;

    @ApiModelProperty("组件路径")
    private String component;

    @ApiModelProperty("路由参数")
    private String routeQuery;

    @ApiModelProperty("是否外链：0-是，1-否")
    private String frame;

    @ApiModelProperty("是否缓存：0-是，1-否")
    private String cache;

    @ApiModelProperty("菜单类型：M-目录，C-菜单，F-按钮")
    private String type;

    @ApiModelProperty("菜单是否隐藏：0-显示，1-隐藏")
    private String visible;

    @ApiModelProperty("菜单状态：0-正常，1-停用")
    private String status;

    @ApiModelProperty("权限标识")
    private String perms;

    @ApiModelProperty("菜单图标")
    private String icon;

    @ApiModelProperty("菜单权限备注")
    private String remark;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;

    @ApiModelProperty("子类实例集合")
    private List<PermissionDTO> children;

}
