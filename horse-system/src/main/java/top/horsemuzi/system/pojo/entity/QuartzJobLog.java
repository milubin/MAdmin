package top.horsemuzi.system.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseEntity;

import java.util.Date;

/**
 * <p>
 * Quartz任务调度日志信息表
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-08-21 16:05:01
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("tb_quartz_job_log")
@ApiModel(value = "QuartzJobLog对象", description = "Quartz任务调度日志信息表")
public class QuartzJobLog extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("链路ID")
    @TableField("trace_id")
    private String traceId;

    @ApiModelProperty("线程名称")
    @TableField("thread_name")
    private String threadName;

    @ApiModelProperty("quartz任务id")
    @TableField("job_id")
    private Long jobId;

    @ApiModelProperty("任务模式: 0-cron表达式执行模式, 1-间隔执行模式")
    @TableField("job_model")
    private Integer jobModel;

    @ApiModelProperty("任务名称")
    @TableField("job_name")
    private String jobName;

    @ApiModelProperty("任务组别")
    @TableField("job_group")
    private String jobGroup;

    @ApiModelProperty("执行目标方法")
    @TableField("invoke_target")
    private String invokeTarget;

    @ApiModelProperty("执行状态：0-失败，1-成功")
    @TableField("exec_status")
    private Integer execStatus;

    @ApiModelProperty("异常名称")
    @TableField("exec_name")
    private String execName;

    @ApiModelProperty("异常信息")
    @TableField("exec_message")
    private String execMessage;

    @ApiModelProperty("任务执行耗时（单位：ms）")
    @TableField("consume")
    private Long consume;

    @ApiModelProperty("创建时间")
    @TableField(value = "gmt_create", fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    @TableField(value = "create_id", fill = FieldFill.INSERT)
    private Long createId;

    @ApiModelProperty("创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

}
