package top.horsemuzi.system.pojo.dto.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseQuery;

import java.io.Serializable;
import java.util.List;

/**
 * 查询参数-用户相关
 *
 * @author mabin
 * @date 2022/06/06 10:26
 **/
@EqualsAndHashCode(callSuper = true)
@ApiModel("用户管理查询相关")
@Data
@Accessors(chain = true)
public class UserQuery extends BaseQuery implements Serializable {
    private static final long serialVersionUID = -3635148418718682718L;

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("用户手机号")
    private String phone;

    @ApiModelProperty("用户邮箱")
    private String email;

    @ApiModelProperty("账号状态：0-正常，1-停用")
    private String status;

    @ApiModelProperty("登录IP")
    private String ip;

    @ApiModelProperty("登录区域")
    private String area;

    @ApiModelProperty("用户id列表")
    private List<Long> userIds;

}
