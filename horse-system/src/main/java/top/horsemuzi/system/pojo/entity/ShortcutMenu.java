package top.horsemuzi.system.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseEntity;

import java.util.Date;

/**
 * <p>
 * 用户快捷菜单信息表
 * </p>
 *
 * @author Mr.Horse
 * @since 2023-01-07 15:26:14
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("tb_shortcut_menu")
@ApiModel(value = "ShortcutMenu对象", description = "用户快捷菜单信息表")
public class ShortcutMenu extends BaseEntity {

    private static final long serialVersionUID = -3360848501503537179L;

    @ApiModelProperty("主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("用户id")
    @TableField("user_id")
    private Long userId;

    @ApiModelProperty("快捷菜单名称")
    @TableField("menu_name")
    private String menuName;

    @ApiModelProperty("快捷菜单组件路径")
    @TableField("menu_component")
    private String menuComponent;

    @ApiModelProperty("快捷菜单图标")
    @TableField("menu_icon")
    private String menuIcon;

    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    @TableField("is_deleted")
    @TableLogic
    private Boolean deleted;

    @ApiModelProperty("创建时间")
    @TableField(value = "gmt_create", fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    @TableField(value = "create_id", fill = FieldFill.INSERT)
    private Long createId;

    @ApiModelProperty("创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty("更新时间")
    @TableField(value = "gmt_modified", fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    @TableField(value = "modified_id", fill = FieldFill.INSERT_UPDATE)
    private Long modifiedId;

    @ApiModelProperty("更新人")
    @TableField(value = "modified_user", fill = FieldFill.INSERT_UPDATE)
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    @TableField("version")
    @Version
    private Long version;
}
