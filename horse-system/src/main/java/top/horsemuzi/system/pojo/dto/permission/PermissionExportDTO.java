package top.horsemuzi.system.pojo.dto.permission;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import cn.hutool.core.date.DatePattern;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 权限菜单导出DTO
 *
 * @author 马滨
 * @date 2022/12/03 14:26
 **/
@ExcelTarget("RoleExportDTO")
@Data
public class PermissionExportDTO implements Serializable {
    private static final long serialVersionUID = 4546155314869870119L;

    @Excel(name = "菜单名称", width = 20)
    @ApiModelProperty("菜单名称")
    private String name;

    @Excel(name = "父菜单名称", width = 20)
    @ApiModelProperty("父菜单名称")
    private String parentName;

    @Excel(name = "顺序", width = 20)
    @ApiModelProperty("显示顺序")
    private Integer sort;

    @Excel(name = "路由地址", width = 20)
    @ApiModelProperty("路由地址")
    private String routePath;

    @Excel(name = "组件路径", width = 20)
    @ApiModelProperty("组件路径")
    private String component;

    @Excel(name = "路由参数", width = 20)
    @ApiModelProperty("路由参数")
    private String routeQuery;

    @Excel(name = "是否外链", replace = {"是_0", "否_1"})
    @ApiModelProperty("是否外链：0-是，1-否")
    private String frame;

    @Excel(name = "是否缓存", replace = {"是_0", "否_1"})
    @ApiModelProperty("是否缓存：0-是，1-否")
    private String cache;

    @Excel(name = "菜单类型", replace = {"目录_M", "菜单_C", "按钮_F"})
    @ApiModelProperty("菜单类型：M-目录，C-菜单，F-按钮")
    private String type;

    @Excel(name = "菜单是否隐藏", width = 20, replace = {"显示_0", "隐藏_1"})
    @ApiModelProperty("菜单是否隐藏：0-显示，1-隐藏")
    private String visible;

    @Excel(name = "状态", replace = {"正常_0", "停用_1"})
    @ApiModelProperty("菜单状态：0-正常，1-停用")
    private String status;

    @Excel(name = "权限标识", width = 20)
    @ApiModelProperty("权限标识")
    private String perms;

    @Excel(name = "菜单图标", width = 20)
    @ApiModelProperty("菜单图标")
    private String icon;

    @Excel(name = "创建人", width = 20)
    @ApiModelProperty("创建人")
    private String createUser;

    @Excel(name = "创建时间", width = 20, format = DatePattern.NORM_DATETIME_PATTERN)
    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @Excel(name = "备注", width = 50)
    @ApiModelProperty("菜单权限备注")
    private String remark;

}
