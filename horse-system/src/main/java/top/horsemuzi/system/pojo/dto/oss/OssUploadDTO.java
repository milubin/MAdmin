package top.horsemuzi.system.pojo.dto.oss;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;
import top.horsemuzi.common.constants.Constants;

import java.io.Serializable;

/**
 * 文件上传DTO
 *
 * @author mabin
 * @date 2022/11/29 09:16
 **/
@Data
public class OssUploadDTO implements Serializable {
    private static final long serialVersionUID = -9074445319382065669L;

    @ApiModelProperty("二进制文件信息")
    private MultipartFile file;

    @ApiModelProperty("是否内置: Y-是, N-否")
    private String internal = Constants.COMMON_JUDGMENT.N;

    @ApiModelProperty("是否压缩: Y-是, N-否")
    private String compress = Constants.COMMON_JUDGMENT.N;

    @ApiModelProperty("是否添加水印: Y-是, N-否")
    private String watermark = Constants.COMMON_JUDGMENT.N;

}
