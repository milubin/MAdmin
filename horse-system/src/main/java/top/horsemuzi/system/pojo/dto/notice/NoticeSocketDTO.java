package top.horsemuzi.system.pojo.dto.notice;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 通知公告Socket推送DTO
 *
 * @author mabin
 * @date 2022/11/11 14:40
 **/
@Data
public class NoticeSocketDTO implements Serializable {

    private static final long serialVersionUID = -4729974333906673508L;

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty("通知公告未读数量")
    private Long notReadCount;

}
