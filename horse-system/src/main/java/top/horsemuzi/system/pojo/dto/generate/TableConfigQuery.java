package top.horsemuzi.system.pojo.dto.generate;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseQuery;

/**
 * 业务表配置Query
 *
 * @author mabin
 * @date 2022/12/19 15:53
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class TableConfigQuery extends BaseQuery {
    private static final long serialVersionUID = -2180247988162671836L;

    @ApiModelProperty("功能作者")
    private String author;

}
