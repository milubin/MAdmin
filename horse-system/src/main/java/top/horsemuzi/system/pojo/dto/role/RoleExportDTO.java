package top.horsemuzi.system.pojo.dto.role;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import cn.hutool.core.date.DatePattern;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.horsemuzi.system.pojo.dto.permission.PermissionExportDTO;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 角色导出DTO
 *
 * @author 马滨
 * @date 2022/12/03 14:21
 **/
@Data
public class RoleExportDTO implements Serializable {
    private static final long serialVersionUID = 5695245448881721003L;

    @Excel(name = "角色名称", width = 20, needMerge = true)
    @ApiModelProperty("角色名称")
    private String name;

    @Excel(name = "角色标识", width = 20, needMerge = true)
    @ApiModelProperty("角色权限标识")
    private String roleCode;

    @Excel(name = "状态", replace = {"正常_0", "停用_1"}, needMerge = true)
    @ApiModelProperty("角色状态：0-正常，1-停用")
    private String status;

    @Excel(name = "创建人", width = 20, needMerge = true)
    @ApiModelProperty("创建人")
    private String createUser;

    @Excel(name = "创建时间", width = 20, format = DatePattern.NORM_DATETIME_PATTERN, needMerge = true)
    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @Excel(name = "备注", width = 50, needMerge = true)
    @ApiModelProperty("备注")
    private String remark;

    @ExcelCollection(name = "权限菜单信息")
    private List<PermissionExportDTO> permissionExportDTOList;

}
