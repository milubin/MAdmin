package top.horsemuzi.system.pojo.dto.notice;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import cn.hutool.core.date.DatePattern;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 通知公告导出DTO实例(只导出发布状态的通知公告信息)
 *
 * @author 马滨
 * @date 2022/10/06 14:39
 **/

@Data
public class NoticeExportDTO implements Serializable {

    private static final long serialVersionUID = -2005072786506876723L;

    @Excel(name = "标题", width = 30, needMerge = true)
    @ApiModelProperty("通知公告标题")
    private String title;

    @Excel(name = "类型", replace = {"通知_0", "公告_1"}, needMerge = true)
    @ApiModelProperty("类型：0-通知，1-公告")
    private Integer type;

    @Excel(name = "内容", width = 100, needMerge = true)
    @ApiModelProperty("通知公告内容")
    private String content;

    @Excel(name = "发布类型", replace = {"所有用户_0", "自定义角色_1", "自定义用户_2"}, needMerge = true)
    @ApiModelProperty("发布对象类型：0-所有用户，1-自定义角色，2-自定义用户")
    private Integer publishType;

    @Excel(name = "备注", width = 50, needMerge = true)
    @ApiModelProperty("备注")
    private String remark;

    @Excel(name = "发布时间", width = 20, format = DatePattern.NORM_DATETIME_PATTERN, needMerge = true)
    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @Excel(name = "创建人", width = 15, needMerge = true)
    @ApiModelProperty("创建人")
    private String createUser;

    @ExcelCollection(name = "发布记录信息")
    @ApiModelProperty("发布记录列表")
    private List<NoticeRecordExportDTO> noticeRecordExportDTOList;

}
