package top.horsemuzi.system.pojo.dto.generate;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 数据库表结构信息
 *
 * @author 马滨
 * @date 2022/12/21 21:54
 **/
@Data
public class SchemaTableDTO implements Serializable {

    private static final long serialVersionUID = 542953331154059966L;

    @ApiModelProperty("表名称")
    private String tableName;

    @ApiModelProperty("表描述")
    private String tableComment;

}
