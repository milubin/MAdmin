package top.horsemuzi.system.pojo.dto.open.category;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;
import top.horsemuzi.common.validator.custom.annotation.Xss;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 开放API类型DTO
 *
 * @author 马滨
 * @date 2022/11/18 22:18
 **/
@Data
public class ApiCategoryDTO implements Serializable {
    private static final long serialVersionUID = 4324255647200408492L;

    @NotNull(message = "API类型主键id不能为空", groups = {ValidTwo.class})
    @ApiModelProperty("主键id")
    private Long id;

    @Xss(message = "API类型名称不能包含脚本字符", groups = {ValidOne.class, ValidTwo.class})
    @NotBlank(message = "API类型名称不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("API类型名称")
    private String categoryName;

    @NotBlank(message = "API类型描述不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("API类型描述")
    private String categoryDescription;

    @ApiModelProperty("API类型图标")
    private String categoryIcon;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    private Boolean deleted;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;
}
