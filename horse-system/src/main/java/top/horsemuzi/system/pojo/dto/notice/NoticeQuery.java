package top.horsemuzi.system.pojo.dto.notice;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseQuery;

/**
 * 通知公告查询实例
 *
 * @author 马滨
 * @date 2022/10/06 14:43
 **/
@Getter
@Setter
@ToString
@Accessors(chain = true)
@NoArgsConstructor
public class NoticeQuery extends BaseQuery {
    private static final long serialVersionUID = -4717238152655591402L;

    @ApiModelProperty("通知公告标题")
    private String title;

}
