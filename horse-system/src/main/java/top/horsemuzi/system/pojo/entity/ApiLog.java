package top.horsemuzi.system.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseEntity;

import java.util.Date;

/**
 * <p>
 * 开放API请求日志
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-21 22:26:38
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("open_api_log")
@ApiModel(value = "ApiLog对象", description = "开放API请求日志")
public class ApiLog extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("链路ID")
    @TableField("trace_id")
    private String traceId;

    @ApiModelProperty("应用id")
    @TableField("app_id")
    private Long appId;

    @ApiModelProperty("应用appKey")
    @TableField("app_key")
    private String appKey;

    @ApiModelProperty("应用名称")
    @TableField("app_name")
    private String appName;

    @ApiModelProperty("API标识code")
    @TableField("api_code")
    private String apiCode;

    @ApiModelProperty("API功能")
    @TableField("api_function")
    private String apiFunction;

    @ApiModelProperty("body请求参数（API默认POST请求）")
    @TableField("body")
    private String body;

    @ApiModelProperty("异常名称")
    @TableField("exc_name")
    private String excName;

    @ApiModelProperty("异常详情")
    @TableField("exc_message")
    private String excMessage;

    @ApiModelProperty("请求ip")
    @TableField("ip")
    private String ip;

    @ApiModelProperty("ip归属地")
    @TableField("area")
    private String area;

    @ApiModelProperty("终端系统")
    @TableField("os")
    private String os;

    @ApiModelProperty("浏览器")
    @TableField("browser")
    private String browser;

    @ApiModelProperty("操作状态: 0-成功, 1-异常")
    @TableField("status")
    private String status;

    @ApiModelProperty("响应信息")
    @TableField("data")
    private String data;

    @ApiModelProperty("状态码")
    @TableField("code")
    private Integer code;

    @ApiModelProperty("操作耗时, 单位:ms")
    @TableField("consume")
    private Long consume;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    @TableField("is_deleted")
    @TableLogic
    private Boolean deleted;

    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("创建时间")
    @TableField(value = "gmt_create", fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    @TableField(value = "create_id", fill = FieldFill.INSERT)
    private Long createId;

    @ApiModelProperty("创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty("更新时间")
    @TableField(value = "gmt_modified", fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    @TableField(value = "modified_id", fill = FieldFill.INSERT_UPDATE)
    private Long modifiedId;

    @ApiModelProperty("更新人")
    @TableField(value = "modified_user", fill = FieldFill.INSERT_UPDATE)
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    @TableField("version")
    @Version
    private Long version;


}
