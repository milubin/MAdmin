package top.horsemuzi.system.pojo.dto.message;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseQuery;

/**
 * 消息记录查询实例
 *
 * @author mabin
 * @date 2022/09/30 11:14
 **/

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class MessageQuery extends BaseQuery {

    private static final long serialVersionUID = 4572612895350456452L;

    @ApiModelProperty("渠道标识：10-钉钉机器人，20-钉钉工作通知，30-飞书机器人，40-飞书工作通知，50-邮箱，60-短信，70-微信公众号")
    private Integer channel;

    @ApiModelProperty("消息记录id")
    private String recordId;

    @ApiModelProperty("stream流名称")
    private String streamName;

    @ApiModelProperty("消息内容")
    private String content;

    @ApiModelProperty("消息状态: 0-初始化, 1-消费成功, 2-消费且ACK成功")
    private String status;

}
