package top.horsemuzi.system.pojo.dto.role;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import top.horsemuzi.common.validator.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 角色实例DTO
 *
 * @author mabin
 * @date 2022/06/23 09:00
 **/

@NoArgsConstructor
@Data
public class RoleDTO implements Serializable {

    private static final long serialVersionUID = 1820074548509680514L;

    @NotNull(message = "角色id不能为空", groups = {ValidTwo.class, ValidThree.class, ValidFour.class, ValidFive.class})
    @ApiModelProperty("主键id")
    private Long id;

    @NotBlank(message = "角色名称不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("角色名称")
    private String name;

    @NotBlank(message = "角色权限标识不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("角色权限标识")
    private String roleCode;

    @NotNull(message = "显示顺序不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("显示顺序")
    private Integer sort;

    @NotBlank(message = "角色状态不能为空", groups = {ValidOne.class, ValidTwo.class, ValidFive.class})
    @ApiModelProperty("角色状态：0-正常，1-停用")
    private String status;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;

    @ApiModelProperty("关联的菜单权限id集合")
    private List<Long> permissionIds;

    @NotNull(message = "用户ID集合不能为空", groups = {ValidThree.class})
    @Size(min = 1, message = "用户ID集合元素至少大于1", groups = {ValidThree.class})
    @ApiModelProperty("用户ID集合")
    private List<Long> userIds;

}
