package top.horsemuzi.system.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseEntity;

import java.util.Date;

/**
 * <p>
 * 开放API应用信息
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-18 22:09:28
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("open_app")
@ApiModel(value = "App对象", description = "开放API应用信息")
public class App extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("应用名称")
    @TableField("app_name")
    private String appName;

    @ApiModelProperty("应用图标")
    @TableField("app_icon")
    private String appIcon;

    @ApiModelProperty("应用appKey")
    @TableField("app_key")
    private String appKey;

    @ApiModelProperty("应用appSecret")
    @TableField("app_secret")
    private String appSecret;

    @ApiModelProperty("访问令牌token")
    @TableField("access_token")
    private String accessToken;

    @ApiModelProperty("应用白名单, 空-不校验")
    @TableField("ip_white_list")
    private String ipWhiteList;

    @ApiModelProperty("应用描述")
    @TableField("app_description")
    private String appDescription;

    @ApiModelProperty("审批状态： 0-审批中， 1-通过，2-拒绝")
    @TableField("approval_status")
    private String approvalStatus;

    @ApiModelProperty("应用审批拒绝原因")
    @TableField("reject_reason")
    private String rejectReason;

    @ApiModelProperty("申请主体：0-个人开发者，1-企业开发")
    @TableField("approval_subject")
    private String approvalSubject;

    @ApiModelProperty("身份证号(个人主体需要提供身份证号)")
    @TableField("id_card")
    private String idCard;

    @ApiModelProperty("社会信用码(公司主体需提供社会统一信用码)")
    @TableField("credit_code")
    private String creditCode;

    @ApiModelProperty("详细地址(申请主体对应的地址信息)")
    @TableField("address")
    private String address;

    @ApiModelProperty("手机号")
    @TableField("phone")
    private String phone;

    @ApiModelProperty("邮箱")
    @TableField("email")
    private String email;

    @ApiModelProperty("API类型id列表，英文逗号分隔")
    @TableField("api_category_ids")
    private String apiCategoryIds;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    @TableField("is_deleted")
    @TableLogic
    private Boolean deleted;

    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("创建时间")
    @TableField(value = "gmt_create", fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    @TableField(value = "create_id", fill = FieldFill.INSERT)
    private Long createId;

    @ApiModelProperty("创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty("更新时间")
    @TableField(value = "gmt_modified", fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    @TableField(value = "modified_id", fill = FieldFill.INSERT_UPDATE)
    private Long modifiedId;

    @ApiModelProperty("更新人")
    @TableField(value = "modified_user", fill = FieldFill.INSERT_UPDATE)
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    @TableField("version")
    @Version
    private Long version;


}
