package top.horsemuzi.system.pojo.dto.open.category;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseQuery;

/**
 * 开放API类型Query
 *
 * @author mabin
 * @date 2022/11/22 15:45
 **/

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class ApiCategoryQuery extends BaseQuery {

    private static final long serialVersionUID = -404053620948788972L;

    @ApiModelProperty("API类型名称")
    private String categoryName;

}
