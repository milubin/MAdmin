package top.horsemuzi.system.pojo.dto.oss;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelIgnore;
import cn.hutool.core.date.DatePattern;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Mr.Horse
 * @version 1.0.0
 * @description OSS对象存储配置业务对象tb_oss_config的DTO实例
 * @date 2023-03-26 11:32:55
 */
@Data
public class OssConfigDTO implements Serializable {

    @ExcelIgnore
    private static final long serialVersionUID = -8644496180482151592L;

    @ApiModelProperty("主键id")
    @ExcelIgnore
    @NotNull(message = "主键id不能为空", groups = {ValidTwo.class})
    private Long id;

    @ApiModelProperty("系统配置key")
    @Excel(name = "系统配置key")
    @NotBlank(message = "系统配置key不能为空", groups = {ValidOne.class, ValidTwo.class})
    private String configKey;

    @ApiModelProperty("accessKey")
    @Excel(name = "accessKey")
    @NotBlank(message = "accessKey不能为空", groups = {ValidOne.class, ValidTwo.class})
    private String accessKey;

    @ApiModelProperty("秘钥")
    @Excel(name = "秘钥")
    @NotBlank(message = "秘钥不能为空", groups = {ValidOne.class, ValidTwo.class})
    private String secretKey;

    @ApiModelProperty("桶名称")
    @Excel(name = "桶名称")
    @NotBlank(message = "桶名称不能为空", groups = {ValidOne.class, ValidTwo.class})
    private String bucketName;

    @ApiModelProperty("前缀")
    @Excel(name = "前缀")
    private String prefix;

    @ApiModelProperty("访问站点")
    @Excel(name = "访问站点")
    private String endpoint;

    @ApiModelProperty("自定义域名")
    @Excel(name = "自定义域名")
    private String domain;

    @ApiModelProperty("域")
    @Excel(name = "域")
    private String region;

    @ApiModelProperty("是否https（0=是,1=否）")
    @Excel(name = "是否https", replace = {"是_0", "否_1"})
    @NotBlank(message = "是否https（0=是,1=否）不能为空", groups = {ValidOne.class, ValidTwo.class})
    private String https;

    @ApiModelProperty("桶权限类型(0=private 1=public 2=custom)")
    @Excel(name = "桶权限类型", replace = {"私有_0", "公共读_1", "自定义_2"})
    @NotBlank(message = "桶权限类型不能为空", groups = {ValidOne.class, ValidTwo.class})
    private String accessPolicy;

    @ApiModelProperty("是否默认（0=是,1=否）")
    @Excel(name = "是否默认（0=是,1=否）", replace = {"是_0", "否_1"})
    @NotBlank(message = "是否默认（0=是,1=否）不能为空", groups = {ValidOne.class, ValidTwo.class})
    private String status;

    @ApiModelProperty("扩展字段")
    @Excel(name = "扩展字段")
    private String ext1;

    @ApiModelProperty("扩展字段")
    @Excel(name = "扩展字段")
    private String ext2;

    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remark;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    @ExcelIgnore
    private Boolean deleted;

    @ApiModelProperty("创建时间")
    @Excel(name = "创建时间", format = DatePattern.NORM_DATETIME_PATTERN)
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    @ExcelIgnore
    private Long createId;

    @ApiModelProperty("创建人")
    @Excel(name = "创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    @Excel(name = "更新时间", format = DatePattern.NORM_DATETIME_PATTERN)
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    @ExcelIgnore
    private Long modifiedId;

    @ApiModelProperty("更新人")
    @Excel(name = "更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    @ExcelIgnore
    private Long version;

}
