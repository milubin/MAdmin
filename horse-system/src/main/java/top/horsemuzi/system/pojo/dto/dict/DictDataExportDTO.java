package top.horsemuzi.system.pojo.dto.dict;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import cn.hutool.core.date.DatePattern;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 字典数据导出DTO
 *
 * @author mabin
 * @date 2022/09/27 13:58
 **/
@ExcelTarget(value = "DictTypeExportDTO")
@Data
public class DictDataExportDTO implements Serializable {

    private static final long serialVersionUID = 1188833970784454531L;

    @Excel(name = "数据标签", width = 15)
    @ApiModelProperty("字典标签")
    private String label;

    @Excel(name = "数据键值", width = 15)
    @ApiModelProperty("字典键值")
    private String value;

    @Excel(name = "状态", replace = {"正常_0", "停用_1"})
    @ApiModelProperty("状态: 0-正常 1-停用")
    private String status;

    @Excel(name = "默认", replace = {"是_Y", "否_N"})
    @ApiModelProperty("是否默认: Y-是, N-否")
    private String defaults;

    @Excel(name = "回显样式", width = 15)
    @ApiModelProperty("表格回显样式")
    private String listClass;

    @Excel(name = "备注", width = 40)
    @ApiModelProperty("备注")
    private String remark;

    @Excel(name = "创建人", width = 15)
    @ApiModelProperty("创建人")
    private String createUser;

    @Excel(name = "创建时间", width = 20, format = DatePattern.NORM_DATETIME_PATTERN)
    @ApiModelProperty("创建时间")
    private Date gmtCreate;

}
