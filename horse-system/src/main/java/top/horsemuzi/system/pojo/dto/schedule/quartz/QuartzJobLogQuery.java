package top.horsemuzi.system.pojo.dto.schedule.quartz;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import top.horsemuzi.system.base.BaseQuery;

/**
 * Quartz任务调度执行日志查询实例
 *
 * @author mabin
 * @date 2022/09/01 13:34
 **/

@EqualsAndHashCode(callSuper = true)
@Data
public class QuartzJobLogQuery extends BaseQuery {
    private static final long serialVersionUID = -4932890477400672155L;

    @ApiModelProperty("quartz任务id")
    private Long jobId;

}
