package top.horsemuzi.system.pojo.dto.statistics;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 饼图数据相关实例
 *
 * @author mabin
 * @date 2022/09/08 19:04
 **/
@Data
public class PieChartDTO implements Serializable {

    private static final long serialVersionUID = 1578666577218085008L;

    @ApiModelProperty("x轴")
    private String name;

    @ApiModelProperty("y轴")
    private String value;
}
