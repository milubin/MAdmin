package top.horsemuzi.system.pojo.dto.login;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 用户登录相关实例
 *
 * @author mabin
 * @date 2022/05/10 21:19
 **/

@NoArgsConstructor
@ApiModel(value = "后台用户登录信息实例")
@Data
public class AdminLoginDTO implements Serializable {

    private static final long serialVersionUID = 8982347254825031706L;

    @NotBlank(message = "用户名不能为空!")
    @ApiModelProperty(value = "用户名", notes = "短信登陆时为手机号, 邮箱登陆时为邮箱号")
    private String username;

    @NotBlank(message = "密码不能为空!")
    @ApiModelProperty(value = "密码", notes = "短信和邮箱登陆时为对应验证码")
    private String password;

    @ApiModelProperty(value = "验证码")
    private String code;

    @ApiModelProperty(value = "uuid验证码唯一标识")
    private String uuid;

    @ApiModelProperty("用户登录方式: 1-密码、2-短信、3-邮箱")
    private Integer type;

}
