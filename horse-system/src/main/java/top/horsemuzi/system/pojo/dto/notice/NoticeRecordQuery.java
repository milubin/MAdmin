package top.horsemuzi.system.pojo.dto.notice;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseQuery;

import java.util.Date;

/**
 * 通知公告发布记录查询相关实例
 *
 * @author 马滨
 * @date 2022/10/06 16:04
 **/
@Getter
@Setter
@ToString
@Accessors(chain = true)
@NoArgsConstructor
public class NoticeRecordQuery extends BaseQuery {

    private static final long serialVersionUID = 5014623483760461759L;

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("通知公告ID, tb_notice.id")
    private Long noticeId;

    @ApiModelProperty("是否已读：0-未读，1-已读")
    private String status;

}
