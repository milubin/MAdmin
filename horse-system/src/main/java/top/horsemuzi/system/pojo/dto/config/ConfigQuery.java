package top.horsemuzi.system.pojo.dto.config;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseQuery;

/**
 * 系统配置查询相关
 *
 * @author mabin
 * @date 2022/06/24 14:53
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class ConfigQuery extends BaseQuery {

    private static final long serialVersionUID = 298900957219581210L;

    @ApiModelProperty("参数配置组别")
    private String configGroup;

    @ApiModelProperty("参数名称")
    private String configName;

    @ApiModelProperty("参数键名")
    private String configKey;

    @ApiModelProperty("是否系统内置（N-否 ，Y-是）,系统内置参数不能删除")
    private String internal;

}
