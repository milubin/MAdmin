package top.horsemuzi.system.pojo.dto.user;

import cn.hutool.core.date.DatePattern;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import top.horsemuzi.common.enums.SensitiveStrategyEnum;
import top.horsemuzi.common.validator.*;
import top.horsemuzi.system.annotation.DictFormat;
import top.horsemuzi.system.annotation.Sensitive;
import top.horsemuzi.system.core.office.excel.convert.DateTimeFormatConvert;
import top.horsemuzi.system.core.office.excel.convert.DictConvert;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description 注意: 实体类中如果使用@Accessory（chain=true）,那么导入的数据无法填充到实例中, 导出数据不受影响
 * @date 2022/5/7 20:53
 */
@ExcelIgnoreUnannotated
@NoArgsConstructor
@Data
public class UserDTO implements Serializable {

    private static final long serialVersionUID = 6807956300562840494L;
    private static final String TITLE = "用户信息";

    @ApiModelProperty("主键id")
    @NotNull(message = "用户ID不能为空!", groups = {ValidTwo.class, ValidThree.class, ValidFour.class, ValidFive.class})
    private Long id;

    @ExcelProperty(value = {TITLE, "用户名"})
    @ApiModelProperty("用户名")
    @NotBlank(message = "用户名不能为空!", groups = {ValidOne.class, ValidTwo.class})
    private String username;

    @ApiModelProperty("用户名拼音")
    private String pinyinUsername;

    @ApiModelProperty("用户名拼音首字母缩写")
    private String pinyinAcronymUsername;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotBlank(message = "密码不能为空!", groups = {ValidOne.class, ValidThree.class})
    @ApiModelProperty("密码")
    private String password;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("密码加密盐值")
    private String salt;

    @ExcelProperty(value = {TITLE, "角色标识"})
    @ApiModelProperty("角色权限标识集合, 逗号分割")
    private String roleCodes;

    @DictFormat(type = "sys_number_reverse_whether")
    @ExcelProperty(value = {TITLE, "实名认证"}, converter = DictConvert.class)
    @ApiModelProperty("是否已实名认证:0-是, 1-否")
    private String realNameAuth;

    @ExcelProperty(value = {TITLE, "真实姓名"})
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("真实姓名")
    private String realName;

    @ExcelProperty(value = {TITLE, "身份证号"})
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("身份证号")
    private String idNumber;

    @ExcelProperty(value = {TITLE, "用户简介"})
    @ApiModelProperty("用户简介")
    private String userDesc;

    @ApiModelProperty("用户头像")
    private String avatar;

    @DictFormat(type = "sys_sex")
    @ExcelProperty(value = {TITLE, "性别"}, converter = DictConvert.class)
    @ApiModelProperty("用户性别：0-女，1-男，2-未知")
    private String sex;

    @Sensitive(strategy = SensitiveStrategyEnum.PHONE)
    @ExcelProperty(value = {TITLE, "手机号"})
    @ApiModelProperty("用户手机号")
    private String phone;

    @Sensitive(strategy = SensitiveStrategyEnum.EMAIL)
    @ExcelProperty(value = {TITLE, "邮箱"})
    @ApiModelProperty("用户邮箱")
    private String email;

    @DictFormat(type = "sys_status")
    @ExcelProperty(value = {TITLE, "账号状态"}, converter = DictConvert.class)
    @NotBlank(message = "账号状态不能为空", groups = {ValidFour.class})
    @ApiModelProperty("账号状态：0-正常，1-停用")
    private String status;

    @ApiModelProperty("是否开启强制更新密码，0-是, 1-否")
    private String forceUpdatePaw;

    @ApiModelProperty("强制更新间隔,单位:天")
    private Integer forceUpdateInterval;

    @ApiModelProperty("最后更新密码时间")
    private Date lastUpdatePwdTime;

    @ExcelProperty(value = {TITLE, "地址"})
    @ApiModelProperty("地址")
    private String address;

    @ExcelProperty(value = {TITLE, "最后登录ip"})
    @ApiModelProperty("最后登录ip")
    private String lastLoginIp;

    @ExcelProperty(value = {TITLE, "最后登录时间"}, converter = DateTimeFormatConvert.class)
    @ApiModelProperty("最后登录时间")
    private Date lastLoginTime;

    @ExcelProperty(value = {TITLE, "最后登录区域地址"})
    @ApiModelProperty("最后登录区域地址")
    private String lastLoginArea;

    @ExcelProperty(value = {TITLE, "区域地址经度"})
    @ApiModelProperty("区域地址经度")
    private BigDecimal longitude;

    @ExcelProperty(value = {TITLE, "区域地址纬度"})
    @ApiModelProperty("区域地址纬度")
    private BigDecimal latitude;

    @ExcelProperty(value = {TITLE, "最后登录系统"})
    @ApiModelProperty("最后登录系统")
    private String lastLoginOs;

    @ExcelProperty(value = {TITLE, "最后登录客户端"})
    @ApiModelProperty("最后登录客户端")
    private String lastLoginClient;

    @DictFormat(type = "sys_number_whether")
    @ExcelProperty(value = {TITLE, "是否移动端"}, converter = DictConvert.class)
    @ApiModelProperty("是否移动端：0-是，1-否")
    private String mobile;

    @ExcelProperty(value = {TITLE, "总在线时长"})
    @ApiModelProperty("总在线时长")
    private Long totalOnlineTime;

    @ExcelProperty(value = {TITLE, "总登录次数"})
    @ApiModelProperty("总登录次数")
    private Long loginCount;

    @ExcelProperty(value = {TITLE, "备注"})
    @ApiModelProperty("备注")
    private String remark;

    @DateTimeFormat(DatePattern.NORM_DATETIME_PATTERN)
    @ExcelProperty(value = {TITLE, "创建时间"}, converter = DateTimeFormatConvert.class)
    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ExcelProperty(value = {TITLE, "创建人"})
    @ApiModelProperty("创建人")
    private String createUser;

    @DateTimeFormat(DatePattern.NORM_DATETIME_PATTERN)
    @ExcelProperty(value = {TITLE, "更新时间"}, converter = DateTimeFormatConvert.class)
    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ExcelProperty(value = {TITLE, "更新人"})
    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;

    @NotBlank(message = "账户锁定缓存key不能为空", groups = {ValidFive.class})
    @ApiModelProperty("账户锁定缓存key(此值不为空, 说明当前账号在错误登录锁定时间内)")
    private String accountCacheLockKey;

}
