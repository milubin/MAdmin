package top.horsemuzi.system.pojo.dto.socket;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * WebSocket通信实例
 *
 * @author mabin
 * @date 2022/10/21 10:02
 **/
@Data
public class SocketDataDTO implements Serializable {

    private static final long serialVersionUID = -6702082311644438283L;

    @ApiModelProperty(value = "客户端行为操作", notes = "read: 读取, all_read: 全部已读, delete: 删除, clear: 清理全部")
    private String action;

    /**
     * 站内信信息记录主键id(只有读取、删除时有值)
     */
    private Long noticeRecordId;

}
