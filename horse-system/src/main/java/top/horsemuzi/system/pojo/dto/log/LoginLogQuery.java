package top.horsemuzi.system.pojo.dto.log;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseQuery;

import java.io.Serializable;

/**
 * 登录日志查询相关DTO
 *
 * @author 马滨
 * @date 2022/09/26 21:55
 **/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class LoginLogQuery extends BaseQuery implements Serializable {
    private static final long serialVersionUID = -3340371532903692654L;

    @ApiModelProperty("链路ID")
    private String traceId;

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("访问IP")
    private String ip;

    @ApiModelProperty("ip归属地")
    private String area;

    @ApiModelProperty("是否移动端: 0-是, 1-否")
    private String mobile;

    @ApiModelProperty("登录状态: 0-成功, 1-异常")
    private String status;

    @ApiModelProperty("登录方式：0-账号密码，1-邮箱登录，2-手机号登录")
    private Integer loginWay;

}
