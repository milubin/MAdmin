package top.horsemuzi.system.pojo.dto.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 页面输入框关键词实时检索
 *
 * @author mabin
 * @date 2022/10/29 14:43
 **/
@Data
public class InputRemoteFetchDTO implements Serializable {

    private static final long serialVersionUID = -3239330860386872009L;

    @ApiModelProperty("实时检索显式在页面的结果")
    private String value;

    @ApiModelProperty(value = "实时检索结果的扩展值")
    private String ext;

}
