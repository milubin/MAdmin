package top.horsemuzi.system.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import top.horsemuzi.system.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 数据源信息
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-12-19 13:48:53
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("gen_datasource")
@ApiModel(value = "Datasource对象", description = "数据源信息")
public class Datasource extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("DB类型: 0-MySQL, 1-Oracle")
    @TableField("db_type")
    private Integer dbType;

    @ApiModelProperty("DB别名")
    @TableField("db_name")
    private String dbName;

    @ApiModelProperty("DB描述")
    @TableField("db_comment")
    private String dbComment;

    @ApiModelProperty("DB版本")
    @TableField("db_version")
    private String dbVersion;

    @ApiModelProperty("DB驱动")
    @TableField("db_driver")
    private String dbDriver;

    @ApiModelProperty("DB链接")
    @TableField("conn_url")
    private String connUrl;

    @ApiModelProperty("DB链接配置")
    @TableField("url_config")
    private String urlConfig;

    @ApiModelProperty("DB用户名")
    @TableField("conn_user")
    private String connUser;

    @ApiModelProperty("DB密码")
    @TableField("conn_password")
    private String connPassword;

    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    @TableField("is_deleted")
    @TableLogic
    private Boolean deleted;

    @ApiModelProperty("创建时间")
    @TableField(value = "gmt_create", fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    @TableField(value = "create_id", fill = FieldFill.INSERT)
    private Long createId;

    @ApiModelProperty("创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty("更新时间")
    @TableField(value = "gmt_modified", fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    @TableField(value = "modified_id", fill = FieldFill.INSERT_UPDATE)
    private Long modifiedId;

    @ApiModelProperty("更新人")
    @TableField(value = "modified_user", fill = FieldFill.INSERT_UPDATE)
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    @TableField("version")
    @Version
    private Long version;


}
