package top.horsemuzi.system.pojo.dto.generate;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 业务表配置DTO
 *
 * @author mabin
 * @date 2022/12/19 14:00
 **/
@Data
public class TableConfigDTO implements Serializable {
    private static final long serialVersionUID = 7559452749976805196L;

    @NotNull(message = "业务表配置id不能为空", groups = {ValidTwo.class})
    @ApiModelProperty("主键id")
    private Long id;

    @NotBlank(message = "功能作者不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("功能作者")
    private String author;

    @NotBlank(message = "包路径不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("包路径")
    private String packageName;

    @ApiModelProperty("模块名(默认取包路径的最后一部分)")
    private String moduleName;

    @ApiModelProperty("自动去除表前缀: 0-去除, 1-保留")
    private String removePrefix;

    @ApiModelProperty("表前缀(多个英文逗号分隔)")
    private String prefix;

    @ApiModelProperty("自动去除字段的is前缀: 0-是, 1-否")
    private String removeIsPrefix;

    @ApiModelProperty("项目版本号")
    private String projectVersion;

    @ApiModelProperty("表单是否添加自适应滚动效果, 默认:500px 0：是  1：否")
    private String fromRoll;

    @ApiModelProperty("主键类型: 0-自增, 1-雪花算法、2-UUID")
    private Integer useIdType;

    @ApiModelProperty("逻辑删除字段名, 默认: is_deleted")
    private String logicDeletedName;

    @ApiModelProperty("乐观送字段名, 默认: version")
    private String versionName;

    @ApiModelProperty("是否添加swagger注解, 0-是, 1-否")
    private String swaggerAnnotation;

    @ApiModelProperty("列工具刷新按钮 0：是  1：否")
    private String gridToolRefresh;

    @ApiModelProperty("列工具显式搜索行表单按钮 0：是  1：否")
    private String gridToolSearch;

    @ApiModelProperty("列工具字段过滤按钮 0：是  1：否")
    private String gridToolFilter;

    @ApiModelProperty("列工具打印按钮 0：是  1：否")
    private String gridToolPrint;

    @ApiModelProperty("是否开启@Mapper注解, 0-是, 1-否")
    private String mapperAnnotation;

    @ApiModelProperty("是否开启@RestController注解, 0-是, 1-否")
    private String restControllerAnnotation;

    @ApiModelProperty("Controller父类路径")
    private String controllerSupperClass;

    @ApiModelProperty("Entity父类路径")
    private String entitySupperClass;

    @ApiModelProperty("Query父类路径")
    private String querySupperClass;

    @ApiModelProperty(value = "Controller格式化名称", example = "%sController")
    private String formatControllerFileName;

    @ApiModelProperty(value = "Service格式化名称", example = "%sService")
    private String formatServiceFileName;

    @ApiModelProperty(value = "ServiceImpl格式化名称", example = "%sServiceImpl")
    @TableField("format_service_impl_file_name")
    private String formatServiceImplFileName;

    @ApiModelProperty(value = "Mapper格式化名称", example = "%sMapper")
    @TableField("format_mapper_file_name")
    private String formatMapperFileName;

    @ApiModelProperty(value = "XML格式化名称", example = "%sMapper")
    private String formatXmlFileName;

    @ApiModelProperty("是否生成xml文件的baseResultMap")
    private String enableBaseResultMap;

    @ApiModelProperty("是否生成xml文件的baseColumnList")
    private String enableBaseColumnList;

    @ApiModelProperty("是否开启@Lombok注解, 0-是, 1-否")
    private String lombokAnnotation;

    @ApiModelProperty("是否生成lombok的链式调用注解")
    private String enableChainModel;

    @ApiModelProperty(value = "注释日期格式化", example = "yyyy-MM-dd HH:mm:ss")
    private String formatCommentDate;

    @ApiModelProperty(value = "是否融合MAdmin系统, 0-是, 1-否", notes = "融合情况下,讲使用MAdmin自定义MP扩展处理")
    private String fuseMadminSystem;

    @ApiModelProperty(value = "基础字段（多个英文逗号分隔）", notes = "此字段会进行自动填充处理")
    private String baseField;

    @ApiModelProperty("是否导出, 0-是, 1-否")
    private String whetherExport;

    @ApiModelProperty("导出类型, 0-EXCEL, 1-CSV")
    private Integer exportType;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    private Boolean deleted;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;

    @ApiModelProperty("Controller父类名称")
    private String controllerSupperClassName;

    @ApiModelProperty("Controller父类包路径")
    private String controllerSupperClassPackage;

    @ApiModelProperty("Entity父类名称")
    private String entitySupperClassName;

    @ApiModelProperty("Entity父类包路径")
    private String entitySupperClassPackage;

    @ApiModelProperty("Query父类名称")
    private String querySupperClassName;

    @ApiModelProperty("Query父类包路径")
    private String querySupperClassPackage;

    @ApiModelProperty("Controller类名")
    private String controllerName;

    @ApiModelProperty("Service类名")
    private String serviceName;

    @ApiModelProperty("Service首字母小写类名")
    private String serviceNameLowerFirst;

    @ApiModelProperty("ServiceImpl类名")
    private String serviceImplName;

    @ApiModelProperty("Mapper类名")
    private String mapperName;

    @ApiModelProperty("XML类名")
    private String xmlName;

}
