package top.horsemuzi.system.pojo.dto.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统在线用户
 *
 * @author mabin
 * @date 2022/08/16 09:02
 **/
@Data
public class OnlineUserDTO implements Serializable {

    private static final long serialVersionUID = 1479022884519641859L;

    @ApiModelProperty(value = "用户会话token", notes = "AES加密")
    private String token;

    @ApiModelProperty("用户名称")
    private String username;

    @ApiModelProperty("用户名拼音")
    private String pinyinUsername;

    @ApiModelProperty("用户名拼音首字母缩写")
    private String pinyinAcronymUsername;

    @ApiModelProperty("登录IP")
    private String ip;

    @ApiModelProperty("登录区域")
    private String area;

    @ApiModelProperty("登录时间")
    private Date loginTime;

    @ApiModelProperty("系统终端")
    private String os;

    @ApiModelProperty("浏览器")
    private String browser;

    @ApiModelProperty("总在线时长")
    private Long totalOnlineTime;

    @ApiModelProperty("总登录次数")
    private Long totalCount;

    @ApiModelProperty("token有效时间")
    private Long expireTime;

    @ApiModelProperty(value = "终端平台类型", notes = "PC/Android")
    private String terminal;

}
