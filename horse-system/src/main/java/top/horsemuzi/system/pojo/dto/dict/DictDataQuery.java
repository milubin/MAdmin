package top.horsemuzi.system.pojo.dto.dict;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseQuery;

import java.io.Serializable;
import java.util.Date;

/**
 * 字典数据查询实例
 *
 * @author mabin
 * @date 2022/06/20 23:33
 **/

@EqualsAndHashCode(callSuper = true)
@ApiModel("字典数据查询相关")
@Data
@Accessors(chain = true)
public class DictDataQuery extends BaseQuery implements Serializable {

    private static final long serialVersionUID = -9068269567222015249L;

    @ApiModelProperty("字典标签")
    private String label;

    @ApiModelProperty("字典类型")
    private String type;

    @ApiModelProperty("状态: 0-正常 1-停用")
    private String status;

    @ApiModelProperty("是否默认: Y-是, N-否")
    private String defaults;

}
