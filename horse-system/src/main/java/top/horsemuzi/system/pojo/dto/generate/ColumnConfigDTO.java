package top.horsemuzi.system.pojo.dto.generate;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 字段类型关联DTO
 *
 * @author mabin
 * @date 2022/12/19 13:53
 **/

@Data
public class ColumnConfigDTO implements Serializable {
    private static final long serialVersionUID = -7598697932630389535L;

    @NotNull(message = "字段类型关联id不能为空", groups = {ValidTwo.class})
    @ApiModelProperty("主键id")
    private Long id;

    @NotBlank(message = "DB字段类型不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("DB字段类型")
    private String columnType;

    @NotBlank(message = "JAVA字段类型不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("JAVA字段类型")
    private String propertyType;

    @NotBlank(message = "JAVA字段属性包名不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("JAVA字段属性包名")
    private String packageName;

    @NotNull(message = "DB类型不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("DB类型: 0-MySQL, 1-Oracle")
    private Integer dbType;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    private Boolean deleted;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;

}
