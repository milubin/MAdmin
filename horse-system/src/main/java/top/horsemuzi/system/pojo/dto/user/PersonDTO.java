package top.horsemuzi.system.pojo.dto.user;

import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.horsemuzi.common.validator.ValidFour;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidThree;
import top.horsemuzi.common.validator.ValidTwo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description
 * @date 2022/5/7 20:53
 */

@Data
public class PersonDTO implements Serializable {

    private static final long serialVersionUID = -3656573331569274670L;

    @NotNull(message = "用户id不能为空", groups = {ValidOne.class, ValidTwo.class, ValidFour.class})
    @ApiModelProperty("主键id")
    private Long id;

    @NotBlank(message = "用户名不能为空", groups = {ValidOne.class})
    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("用户名拼音")
    private String pinyinUsername;

    @ApiModelProperty("用户名拼音首字母缩写")
    private String pinyinAcronymUsername;

    @NotBlank(message = "密码不能为空", groups = {ValidTwo.class})
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("密码")
    private String password;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("密码加密盐值")
    private String salt;

    @ApiModelProperty("角色权限标识集合, 逗号分割")
    private String roleCodes;

    @ApiModelProperty("角色名称集合")
    private List<String> roleNames;

    @ApiModelProperty("是否已实名认证:0-是, 1-否")
    private String realNameAuth;

    @NotBlank(message = "真实姓名不能为空", groups = {ValidFour.class})
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("真实姓名")
    private String realName;

    @NotBlank(message = "身份证号不能为空", groups = {ValidFour.class})
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("身份证号")
    private String idNumber;

    @ApiModelProperty("用户简介")
    private String userDesc;

    @ApiModelProperty("用户头像")
    private String avatar;

    @NotBlank(message = "用户性别不能为空", groups = {ValidOne.class})
    @ApiModelProperty("用户性别：0-女，1-男，2-未知")
    private String sex;

    @NotBlank(message = "用户手机号不能为空", groups = {ValidOne.class})
    @ApiModelProperty("用户手机号")
    private String phone;

    @NotBlank(message = "用户邮箱不能为空", groups = {ValidOne.class})
    @ApiModelProperty("用户邮箱")
    private String email;

    @ApiModelProperty("账号状态：0-正常，1-停用")
    private String status;

    @NotBlank(message = "用户密码更新策略配置不能为空", groups = {ValidOne.class})
    @ApiModelProperty("是否开启强制更新密码，0-是, 1-否")
    private String forceUpdatePaw;

    @NotNull(message = "强制更新间隔配置不能为空", groups = {ValidOne.class})
    @ApiModelProperty("强制更新间隔,单位:天")
    private Integer forceUpdateInterval;

    @ApiModelProperty("地址")
    private String address;

    @ApiModelProperty("最后登录ip")
    private String lastLoginIp;

    @ApiModelProperty("最后登录时间")
    private Date lastLoginTime;

    @ApiModelProperty("最后登录区域地址")
    private String lastLoginArea;

    @ApiModelProperty("区域地址经度")
    private BigDecimal longitude;

    @ApiModelProperty("区域地址纬度")
    private BigDecimal latitude;

    @ApiModelProperty("最后登录系统")
    private String lastLoginOs;

    @ApiModelProperty("最后登录客户端")
    private String lastLoginClient;

    @ApiModelProperty("是否移动端：0-是，1-否")
    private String mobile;

    @ApiModelProperty("总在线时长")
    private Long totalOnlineTime;

    @ApiModelProperty("总登录次数")
    private Long loginCount;

    @ApiModelProperty("备注")
    private String remark;

    @DateTimeFormat("yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;

    @NotBlank(message = "新密码不能为空", groups = {ValidTwo.class, ValidThree.class})
    @ApiModelProperty("新密码")
    private String newPassword;

}
