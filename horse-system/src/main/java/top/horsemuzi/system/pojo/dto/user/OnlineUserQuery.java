package top.horsemuzi.system.pojo.dto.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import top.horsemuzi.system.base.BaseQuery;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统在线用户
 *
 * @author mabin
 * @date 2022/08/16 09:02
 **/

@EqualsAndHashCode(callSuper = true)
@Data
public class OnlineUserQuery extends BaseQuery implements Serializable {

    private static final long serialVersionUID = 7711673589338501561L;

    @ApiModelProperty("用户名称")
    private String username;

    @ApiModelProperty("登录IP")
    private String ip;

    @ApiModelProperty("登录区域")
    private String area;

}
