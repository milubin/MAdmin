package top.horsemuzi.system.pojo.dto.schedule.quartz;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelIgnore;
import cn.hutool.core.date.DatePattern;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Quartz热调度任务日志记录
 *
 * @author mabin
 * @date 2022/08/22 08:54
 **/
@Data
public class QuartzJobLogDTO implements Serializable {
    private static final long serialVersionUID = 8449593511813059877L;

    @ExcelIgnore
    @ApiModelProperty("主键id")
    private Long id;

    @Excel(name = "链路ID")
    @ApiModelProperty("链路ID")
    private String traceId;

    @Excel(name = "线程名称")
    @ApiModelProperty("线程名称")
    private String threadName;

    @ExcelIgnore
    @ApiModelProperty("quartz任务id")
    private Long jobId;

    @Excel(name = "线程名称", replace = {"cron模式_0", "间隔模式_1"}, type = 10)
    @ApiModelProperty("任务模式: 0-cron表达式执行模式, 1-间隔执行模式")
    private Integer jobModel;

    @Excel(name = "任务名称")
    @ApiModelProperty("任务名称")
    private String jobName;

    @Excel(name = "任务组别")
    @ApiModelProperty("任务组别")
    private String jobGroup;

    @Excel(name = "执行目标方法")
    @ApiModelProperty("执行目标方法")
    private String invokeTarget;

    @Excel(name = "执行状态", replace = {"成功_1", "失败_0"}, type = 10)
    @ApiModelProperty("执行状态：0-失败，1-成功")
    private Integer execStatus;

    @Excel(name = "异常名称")
    @ApiModelProperty("异常名称")
    private String execName;

    @Excel(name = "异常信息")
    @ApiModelProperty("异常信息")
    private String execMessage;

    @Excel(name = "任务耗时", suffix = "ms", type = 10)
    @ApiModelProperty("任务执行耗时（单位：ms）")
    private Long consume;

    @Excel(name = "创建时间", format = DatePattern.NORM_DATETIME_PATTERN)
    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ExcelIgnore
    @ApiModelProperty("创建人id")
    private Long createId;

    @Excel(name = "创建人")
    @ApiModelProperty("创建人")
    private String createUser;

}
