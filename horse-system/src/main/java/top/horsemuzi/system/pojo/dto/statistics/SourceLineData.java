package top.horsemuzi.system.pojo.dto.statistics;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Line图源数据实例
 *
 * @author mabin
 * @date 2022/09/09 09:04
 **/
@Data
public class SourceLineData implements Serializable {
    private static final long serialVersionUID = 7030831946808613379L;

    @ApiModelProperty("x轴源数据")
    private String name;

    @ApiModelProperty("y轴源数据")
    private Long value;

    @ApiModelProperty("区别统计类型的标识")
    private String flag;
}
