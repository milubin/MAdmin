package top.horsemuzi.system.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseEntity;

import java.util.Date;

/**
 * <p>
 * Quartz任务调度信息表
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-08-21 16:05:01
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("tb_quartz_job")
@ApiModel(value = "QuartzJob对象", description = "Quartz任务调度信息表")
public class QuartzJob extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("任务模式: 0-cron表达式执行模式, 1-间隔执行模式")
    @TableField("job_model")
    private Integer jobModel;

    @ApiModelProperty("任务状态：0-正常， 1-暂停")
    @TableField("job_status")
    private Integer jobStatus;

    @ApiModelProperty("任务名称")
    @TableField("job_name")
    private String jobName;

    @ApiModelProperty("任务组别")
    @TableField("job_group")
    private String jobGroup;

    @ApiModelProperty("执行目标方法")
    @TableField("invoke_target")
    private String invokeTarget;

    @ApiModelProperty("是否允许并发执行：0-允许，1-禁止")
    @TableField("concurrent")
    private Integer concurrent;

    @ApiModelProperty("计划执行错误策略：0-默认策略，1-立即执行，2-执行一次，3-放弃执行")
    @TableField("misfire_policy")
    private Integer misfirePolicy;

    @ApiModelProperty("触发器名称")
    @TableField("trigger_name")
    private String triggerName;

    @ApiModelProperty("触发器组别")
    @TableField("trigger_group")
    private String triggerGroup;

    @ApiModelProperty("cron表达式")
    @TableField("cron_expression")
    private String cronExpression;

    @ApiModelProperty("开始时间")
    @TableField("start_time")
    private Date startTime;

    @ApiModelProperty("任务执行间隔(每隔多少秒执行一次任务)")
    @TableField("job_interval")
    private Integer jobInterval;

    @ApiModelProperty("任务间隔模式运行的次数 （<0:表示不限次数）")
    @TableField("repeat_count")
    private Integer repeatCount;

    @ApiModelProperty("任务备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    @TableField("is_deleted")
    @TableLogic
    private Boolean deleted;

    @ApiModelProperty("创建时间")
    @TableField(value = "gmt_create", fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    @TableField(value = "create_id", fill = FieldFill.INSERT)
    private Long createId;

    @ApiModelProperty("创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty("更新时间")
    @TableField(value = "gmt_modified", fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    @TableField(value = "modified_id", fill = FieldFill.INSERT_UPDATE)
    private Long modifiedId;

    @ApiModelProperty("更新人")
    @TableField(value = "modified_user", fill = FieldFill.INSERT_UPDATE)
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    @TableField("version")
    @Version
    private Long version;


}
