package top.horsemuzi.system.pojo.dto.schedule.task;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import top.horsemuzi.system.base.BaseQuery;

/**
 * Spring调度任务查询实例
 *
 * @author 马滨
 * @date 2022/09/06 22:03
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public class TaskQuery extends BaseQuery {

    private static final long serialVersionUID = -3621281262637928256L;

    @ApiModelProperty("任务名称")
    private String taskName;

    @ApiModelProperty("任务实例")
    private String className;

    @ApiModelProperty("任务方法")
    private String methodName;

    @ApiModelProperty("任务参数")
    private String methodParams;

    @ApiModelProperty("任务状态： 0-暂停，1-正常")
    private Integer status;

}
