package top.horsemuzi.system.pojo.dto.generate;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 业务表字段结构信息
 *
 * @author mabin
 * @date 2022/12/22 11:19
 **/
@Data
public class SchemaColumnDTO implements Serializable {
    private static final long serialVersionUID = -1826137435508981528L;

    @ApiModelProperty("数据库名称")
    private String tableSchema;

    @ApiModelProperty("表名称")
    private String tableName;

    @ApiModelProperty("列名称")
    private String columnName;

    @ApiModelProperty("顺序")
    private Integer ordinalPosition;

    @ApiModelProperty("默认值")
    private String columnDefault;

    @ApiModelProperty("是否允许为空: YES-是, NO-否")
    private String isNullable;

    @ApiModelProperty("数据类型")
    private String dataType;

    @ApiModelProperty("字符最大长度")
    private Integer characterMaximumLength;

    @ApiModelProperty("列类型")
    private String columnType;

    @ApiModelProperty("列关键字,主键字段显示: PRI")
    private String columnKey;

    @ApiModelProperty("列注释")
    private String columnComment;

}
