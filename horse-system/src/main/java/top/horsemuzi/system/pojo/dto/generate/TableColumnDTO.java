package top.horsemuzi.system.pojo.dto.generate;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.horsemuzi.common.validator.ValidTwo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 业务表字段DTO
 *
 * @author mabin
 * @date 2022/12/19 14:01
 **/
@Data
public class TableColumnDTO implements Serializable {
    private static final long serialVersionUID = -5953142001068680984L;

    @NotNull(message = "主键id不能为空", groups = {ValidTwo.class})
    @ApiModelProperty("主键id")
    private Long id;

    @NotNull(message = "业务表id不能为空", groups = {ValidTwo.class})
    @ApiModelProperty("业务表id")
    private Long tableId;

    @NotBlank(message = "列名称不能为空", groups = {ValidTwo.class})
    @ApiModelProperty("列名称")
    private String columnName;

    @NotBlank(message = "列类型不能为空", groups = {ValidTwo.class})
    @ApiModelProperty("列类型")
    private String columnType;

    @NotBlank(message = "列说明不能为空", groups = {ValidTwo.class})
    @ApiModelProperty("列说明")
    private String columnComment;

    @NotBlank(message = "Java字段名不能为空", groups = {ValidTwo.class})
    @ApiModelProperty("Java字段名")
    private String javaName;

    @NotBlank(message = "Java字段类型不能为空", groups = {ValidTwo.class})
    @ApiModelProperty("Java字段类型")
    private String javaType;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("Java字段名（首字母大写）")
    private String javaNameUp;

    @NotBlank(message = "字段属性包名不能为空", groups = {ValidTwo.class})
    @ApiModelProperty("Java字段属性包名")
    private String packageName;

    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("自动填充  DEFAULT、INSERT、UPDATE、INSERT_UPDATE")
    private String autoFill;

    @ApiModelProperty("主键 0：是  1：否")
    private String pkField;

    @ApiModelProperty("表单项 0：是  1：否")
    private String formItem;

    @ApiModelProperty("表单项必填 0：是  1：否")
    private String formRequired;

    @ApiModelProperty("表单类型")
    private String formType;

    @ApiModelProperty("表单字典类型")
    private String formDict;

    @ApiModelProperty("表单项清空 0：是  1：否")
    private String formClearable;

    @ApiModelProperty("表单占位符")
    private String formPlaceholder;

    @ApiModelProperty("表单项说明")
    private String formExplain;

    @ApiModelProperty("表单效验显式内容")
    private String formValidator;

    @ApiModelProperty("表单项最大字符数")
    private Integer formCharMaxLength;

    @ApiModelProperty("列表项 0：是  1：否")
    private String gridItem;

    @ApiModelProperty("列表排序 0：是  1：否")
    private String gridSort;

    @ApiModelProperty("列表单击复制 0：是  1：否")
    private String gridCopy;

    @ApiModelProperty("列表项说明 0：是  1：否")
    private String gridExplain;

    @ApiModelProperty("列表项宽度, 默认:120px")
    private Integer gridWidth;

    @ApiModelProperty("查询项 0：是  1：否")
    private String queryItem;

    @ApiModelProperty("查询方式")
    private String queryType;

    @ApiModelProperty("查询表单类型")
    private String queryFormType;

    @ApiModelProperty("查询表单字典类型")
    private String queryFormDict;

    @ApiModelProperty("查询表单项清空 0：是  1：否")
    private String queryFormClearable;

    @ApiModelProperty("查询表单占位符")
    private String queryFormPlaceholder;

    @ApiModelProperty("查询表单项宽度, 默认:240px")
    private Integer queryFormWidth;

    @ApiModelProperty("查询表单实时检索项, 只允许一个表单项 0：是  1：否")
    private String queryFormFetch;

    @ApiModelProperty("是否导出, 0-是, 1-否")
    private String whetherExport;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    private Boolean deleted;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;

    @ApiModelProperty(value = "是否entity父类字段", example = "false")
    private Boolean entitySupperField;

}
