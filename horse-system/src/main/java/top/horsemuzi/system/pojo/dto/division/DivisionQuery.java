package top.horsemuzi.system.pojo.dto.division;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import top.horsemuzi.system.base.BaseQuery;

/**
 * 国家区划查询实例
 *
 * @author 马滨
 * @date 2022/11/06 10:37
 **/

@Data
@EqualsAndHashCode(callSuper = true)
public class DivisionQuery extends BaseQuery {
    private static final long serialVersionUID = -7836230397109828853L;

    @ApiModelProperty(value = "地区名称", notes = "页面下拉框控制只允许查询省级地区名称")
    private String name;

}
