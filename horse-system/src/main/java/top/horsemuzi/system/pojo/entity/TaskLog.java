package top.horsemuzi.system.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import top.horsemuzi.system.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-08-20 17:56:39
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("tb_task_log")
@ApiModel(value = "TaskLog对象", description = "")
public class TaskLog extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("任务id")
    @TableField("task_id")
    private Long taskId;

    @ApiModelProperty("链路ID")
    @TableField("trace_id")
    private String traceId;

    @ApiModelProperty("线程名称")
    @TableField("thread_name")
    private String threadName;

    @ApiModelProperty("任务实例")
    @TableField("class_name")
    private String className;

    @ApiModelProperty("任务方法")
    @TableField("method_name")
    private String methodName;

    @ApiModelProperty("任务参数")
    @TableField("method_params")
    private String methodParams;

    @ApiModelProperty("执行状态：0-失败，1-成功")
    @TableField("exec_status")
    private Integer execStatus;

    @ApiModelProperty("异常名称")
    @TableField("exec_name")
    private String execName;

    @ApiModelProperty("异常信息")
    @TableField("exec_message")
    private String execMessage;

    @ApiModelProperty("任务执行耗时（单位：ms）")
    @TableField("consume")
    private Long consume;

    @ApiModelProperty("创建时间")
    @TableField(value = "gmt_create", fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    @TableField(value = "create_id", fill = FieldFill.INSERT)
    private Long createId;

    @ApiModelProperty("创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty("更新时间")
    @TableField(value = "gmt_modified", fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    @TableField(value = "modified_id", fill = FieldFill.INSERT_UPDATE)
    private Long modifiedId;

    @ApiModelProperty("更新人")
    @TableField(value = "modified_user", fill = FieldFill.INSERT_UPDATE)
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    @TableField("version")
    @Version
    private Long version;


}
