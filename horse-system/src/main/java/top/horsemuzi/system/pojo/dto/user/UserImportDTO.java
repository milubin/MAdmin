package top.horsemuzi.system.pojo.dto.user;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import top.horsemuzi.system.annotation.DictFormat;
import top.horsemuzi.system.core.office.excel.convert.DictConvert;

import java.io.Serializable;

/**
 * 用户信息excel导入DTO实例
 *
 * @author mabin
 * @date 2022/06/16 16:43
 **/

@NoArgsConstructor
@Data
public class UserImportDTO implements Serializable {

    private static final long serialVersionUID = -7544564642058079464L;

    @ExcelProperty(value = {"用户名"})
    @ApiModelProperty("用户名")
    private String username;

    @DictFormat(type = "sys_sex")
    @ExcelProperty(value = {"性别"}, converter = DictConvert.class)
    @ApiModelProperty("用户性别：0-女，1-男，2-未知")
    private String sex;

    @DictFormat(type = "sys_status")
    @ExcelProperty(value = {"账号状态"}, converter = DictConvert.class)
    @ApiModelProperty("账号状态：0-正常，1-停用")
    private String status;

    @ExcelProperty(value = {"手机号"})
    @ApiModelProperty("手机号")
    private String phone;

    @ExcelProperty(value = {"邮箱号"})
    @ApiModelProperty("邮箱号")
    private String email;

}
