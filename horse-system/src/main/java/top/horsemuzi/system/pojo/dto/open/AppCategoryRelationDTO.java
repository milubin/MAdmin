package top.horsemuzi.system.pojo.dto.open;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 开放API应用和接口类型关联DTO
 *
 * @author 马滨
 * @date 2022/11/18 22:20
 **/
@Data
public class AppCategoryRelationDTO implements Serializable {
    private static final long serialVersionUID = -1190704843473340965L;

    @ApiModelProperty("主键id")
    private Long id;

    @ApiModelProperty("API应用id")
    private Long appId;

    @ApiModelProperty("API接口类型id")
    private Long apiCategoryId;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;
}
