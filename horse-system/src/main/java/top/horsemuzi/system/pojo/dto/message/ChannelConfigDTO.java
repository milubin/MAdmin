package top.horsemuzi.system.pojo.dto.message;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 渠道配置属性DTO
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/04/29 11:20
 **/
@Data
public class ChannelConfigDTO implements Serializable {
    private static final long serialVersionUID = 8703655617975603738L;

    /**
     * 钉钉机器人
     * {@link top.horsemuzi.core.message.common.channel.DingBotChannelConfig}
     */
    @Data
    public static class DingBotChannelConfigDTO {
        @ApiModelProperty("密钥（签名验证）")
        private String secret;

        @ApiModelProperty("自定义机器人的 webhook")
        private String webhook;
    }

    /**
     * 钉钉工作通知
     * {@link top.horsemuzi.core.message.common.channel.DingWorkChannelConfig}
     */
    @Data
    public static class DingWorkChannelConfigDTO {
        @ApiModelProperty("应用的唯一标识key")
        private String appKey;

        @ApiModelProperty("应用的密钥")
        private String appSecret;

        @ApiModelProperty("发送消息时使用的微应用的AgentID")
        private String agentId;

        @ApiModelProperty("发送工作消息URL地址")
        private String sendUrl;

        @ApiModelProperty("生成token的URL地址")
        private String tokenUrl;
    }

    /**
     * 邮箱服务
     * {@link top.horsemuzi.core.message.common.channel.EmailChannelConfig}
     */
    @Data
    public static class EmailChannelConfigDTO {
        /**
         * SMTP服务器域名
         */
        @ApiModelProperty("SMTP服务器域名")
        private String host;

        /**
         * SMTP服务端口
         */
        @ApiModelProperty("SMTP服务端口")
        private Integer port;

        /**
         * 用户名
         */
        @ApiModelProperty("用户名")
        private String user;

        /**
         * 密码
         */
        @ApiModelProperty("密码")
        private String pass;

        /**
         * 发送方
         */
        @ApiModelProperty("发送方")
        private String from;

        /**
         * 使用 SSL安全连接: SSL加密方式发送邮件 在使用QQ或Gmail邮箱时，需要强制开启SSL支持
         */
        @ApiModelProperty("是否使用SSL安全连接")
        private Boolean sslEnable;

        /**
         * 是否使用用户名密码认证
         */
        @ApiModelProperty("是否使用用户名密码认证")
        private Boolean auth;

        /**
         * 是否使用STARTTLS安全连接
         */
        @ApiModelProperty("是否使用STARTTLS安全连接")
        private Boolean starttlsEnable;
    }

    /**
     * 阿里云sms
     * {@link top.horsemuzi.core.message.common.channel.AliyunSmsConfig}
     */
    @Data
    public static class AliyunSmsConfigDTO {
        @ApiModelProperty("配置节点")
        private String endpoint;

        @ApiModelProperty("AccessKey ID")
        private String accessKeyId;

        @ApiModelProperty("AccessKey Secret")
        private String accessKeySecret;

        @ApiModelProperty("短信签名")
        private String signName;
    }

    /**
     * 飞书机器人
     * {@link top.horsemuzi.core.message.common.channel.FlyingBotChannelConfig}
     */
    @Data
    public static class FlyingBotChannelConfigDTO {
        @ApiModelProperty("自定义机器人的 webhook")
        private String webhook;

        @ApiModelProperty("密钥（签名验证）")
        private String secret;
    }


}
