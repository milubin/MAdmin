package top.horsemuzi.system.pojo.dto.schedule.task;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 任务日志相关
 *
 * @author 马滨
 * @date 2022/09/06 21:33
 **/
@Data
public class TaskLogDTO implements Serializable {
    private static final long serialVersionUID = -6700046621897179902L;

    @ApiModelProperty("主键id")
    private Long id;

    @ApiModelProperty("任务id")
    private Long taskId;

    @ApiModelProperty("链路ID")
    private String traceId;

    @ApiModelProperty("线程名称")
    private String threadName;

    @ApiModelProperty("任务实例")
    private String className;

    @ApiModelProperty("任务方法")
    private String methodName;

    @ApiModelProperty("任务参数")
    private String methodParams;

    @ApiModelProperty("执行状态：0-失败，1-成功")
    private Integer execStatus;

    @ApiModelProperty("异常名称")
    private String execName;

    @ApiModelProperty("异常信息")
    private String execMessage;

    @ApiModelProperty("任务执行耗时（单位：ms）")
    private Long consume;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;
}
