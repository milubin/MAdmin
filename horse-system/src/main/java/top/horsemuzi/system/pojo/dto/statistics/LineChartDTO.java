package top.horsemuzi.system.pojo.dto.statistics;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 折线图数据实例
 *
 * @author mabin
 * @date 2022/09/08 19:13
 **/
@Data
public class LineChartDTO implements Serializable {
    private static final long serialVersionUID = 3739977728655785675L;

    @ApiModelProperty(name = "x轴", notes = "日期列表")
    private List<String> dateList;

    @ApiModelProperty(name = "y轴", notes = "数据集合列表")
    private List<List<Long>> dataList;

}
