package top.horsemuzi.system.pojo.dto.dict;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 字段数据DTO
 *
 * @author mabin
 * @date 2022/06/20 11:11
 **/

@NoArgsConstructor
@Data
public class DictDataDTO implements Serializable {

    private static final long serialVersionUID = -6794410555180512353L;

    @NotNull(message = "字典数据ID不能为空", groups = {ValidTwo.class})
    @ApiModelProperty("主键id")
    private Long id;

    @ApiModelProperty("字典排序")
    private Integer sort;

    @NotBlank(message = "字典标签不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("字典标签")
    private String label;

    @NotBlank(message = "字典键值不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("字典键值")
    private String value;

    @NotBlank(message = "字典类型不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("字典类型")
    private String type;

    @ApiModelProperty("样式属性（其他样式扩展）")
    private String cssClass;

    @ApiModelProperty("表格回显样式")
    private String listClass;

    @ApiModelProperty("是否默认: Y-是, N-否")
    private String defaults;

    @NotBlank(message = "字典数据状态不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("状态: 0-正常 1-停用")
    private String status;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;

    @ApiModelProperty("字典类型列表")
    private List<String> types;

}
