package top.horsemuzi.system.pojo.dto.generate;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseQuery;

import java.util.List;

/**
 * 业务表Query
 *
 * @author mabin
 * @date 2022/12/19 15:01
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class TableQuery extends BaseQuery {

    private static final long serialVersionUID = 4212550880843092944L;

    @ApiModelProperty("表名")
    private String tableName;

    @ApiModelProperty("业务表配置id")
    private List<Long> tableConfigIds;

    @ApiModelProperty("数据源id")
    private List<Long> datasourceIds;

}
