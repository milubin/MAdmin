package top.horsemuzi.system.pojo.dto.oss;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;
import top.horsemuzi.system.base.BaseQuery;

import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.io.Serializable;

/**
 * @author Mr.Horse
 * @description OSS对象存储配置业务对象tb_oss_config的Query实例
 * @date 2023-03-26 11:32:55
 * @version 1.0.0
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class OssConfigQuery extends BaseQuery implements Serializable {

    private static final long serialVersionUID = -1002065165133503070L;

    @ApiModelProperty("系统配置key")
    private String configKey;

    @ApiModelProperty("是否默认（0=是,1=否）")
    private String status;


}
