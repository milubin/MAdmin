package top.horsemuzi.system.pojo.dto.dict;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidThree;
import top.horsemuzi.common.validator.ValidTwo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * 字典类型DTO
 *
 * @author mabin
 * @date 2022/06/20 11:11
 **/

@NoArgsConstructor
@Data
public class DictTypeDTO implements Serializable {

    private static final long serialVersionUID = 7838329527009855989L;

    @NotNull(message = "主键id不能为空", groups = {ValidTwo.class, ValidThree.class})
    @ApiModelProperty("主键id")
    private Long id;

    @NotBlank(message = "字典名称不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("字典名称")
    private String name;

    @NotBlank(message = "字典类型不能为空", groups = {ValidOne.class, ValidTwo.class})
    @Pattern(regexp = "^[a-z][a-z0-9_]*$", message = "字典类型必须以字母开头, 且字典类型只能由小写字母或加下划线还有数字组成", groups = {ValidOne.class, ValidTwo.class})
    @Size(min = 2, max = 200, message = "字典类型长度必须在2-200个字符之间", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("字典类型")
    private String type;

    @NotBlank(message = "字典状态不能为空", groups = {ValidOne.class, ValidTwo.class, ValidThree.class})
    @ApiModelProperty("状态: 0-正常 1-停用")
    private String status;

    @NotBlank(message = "字典属性不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("是否内置: Y-是, N-否")
    private String internal;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;

}
