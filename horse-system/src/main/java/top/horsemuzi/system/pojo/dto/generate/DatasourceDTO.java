package top.horsemuzi.system.pojo.dto.generate;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidThree;
import top.horsemuzi.common.validator.ValidTwo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 数据源DTO
 *
 * @author mabin
 * @date 2022/12/19 13:54
 **/
@Data
public class DatasourceDTO implements Serializable {
    private static final long serialVersionUID = -1853047858421125351L;

    @NotNull(message = "DB主键id不能为空", groups = {ValidTwo.class})
    @ApiModelProperty("主键id")
    private Long id;

    @NotNull(message = "DB类型不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("DB类型: 0-MySQL, 1-Oracle")
    private Integer dbType;

    @NotBlank(message = "DB别名不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("DB别名")
    private String dbName;

    @ApiModelProperty("DB描述")
    private String dbComment;

    @ApiModelProperty("DB版本")
    private String dbVersion;

    @NotBlank(message = "DB驱动不能为空", groups = {ValidOne.class, ValidTwo.class, ValidThree.class})
    @ApiModelProperty("DB驱动")
    private String dbDriver;

    @NotBlank(message = "DB链接不能为空", groups = {ValidOne.class, ValidTwo.class, ValidThree.class})
    @ApiModelProperty("DB链接")
    private String connUrl;

    @ApiModelProperty("DB链接配置")
    private String urlConfig;

    @NotBlank(message = "DB用户名不能为空", groups = {ValidOne.class, ValidTwo.class, ValidThree.class})
    @ApiModelProperty("DB用户名")
    private String connUser;

    @NotBlank(message = "DB密码不能为空", groups = {ValidOne.class, ValidTwo.class, ValidThree.class})
    @ApiModelProperty("DB密码")
    private String connPassword;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    private Boolean deleted;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;
}
