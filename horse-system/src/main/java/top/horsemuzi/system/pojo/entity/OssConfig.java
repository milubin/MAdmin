package top.horsemuzi.system.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseEntity;
import top.horsemuzi.system.handle.encrypt.AESTypeHandler;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Mr.Horse
 * @version 1.0.0
 * @description OSS对象存储配置业务对象tb_oss_config的Entity实例
 * @date 2023-03-26 11:32:55
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "OssConfig对象", description = "OSS对象存储配置信息")
@TableName(value = "tb_oss_config", autoResultMap = true)
public class OssConfig extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("系统配置key")
    @TableField("config_key")
    private String configKey;

    @ApiModelProperty("accessKey")
    @TableField(value = "access_key", typeHandler = AESTypeHandler.class)
    private String accessKey;

    @ApiModelProperty("秘钥")
    @TableField(value = "secret_key", typeHandler = AESTypeHandler.class)
    private String secretKey;

    @ApiModelProperty("桶名称")
    @TableField("bucket_name")
    private String bucketName;

    @ApiModelProperty("前缀")
    @TableField("prefix")
    private String prefix;

    @ApiModelProperty("访问站点")
    @TableField("endpoint")
    private String endpoint;

    @ApiModelProperty("自定义域名")
    @TableField("domain")
    private String domain;

    @ApiModelProperty("域")
    @TableField("region")
    private String region;

    @ApiModelProperty("是否https（0=是,1=否）")
    @TableField("is_https")
    private String https;

    @ApiModelProperty("桶权限类型(0=private 1=public 2=custom)")
    @TableField("access_policy")
    private String accessPolicy;

    @ApiModelProperty("是否默认（0=是,1=否）")
    @TableField("status")
    private String status;

    @ApiModelProperty("扩展字段")
    @TableField("ext1")
    private String ext1;

    @ApiModelProperty("扩展字段")
    @TableField("ext2")
    private String ext2;

    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    @TableLogic
    @TableField("is_deleted")
    private Boolean deleted;

    @ApiModelProperty("创建时间")
    @TableField(value = "gmt_create", fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    @TableField(value = "create_id", fill = FieldFill.INSERT)
    private Long createId;

    @ApiModelProperty("创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty("更新时间")
    @TableField(value = "gmt_modified", fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    @TableField(value = "modified_id", fill = FieldFill.INSERT_UPDATE)
    private Long modifiedId;

    @ApiModelProperty("更新人")
    @TableField(value = "modified_user", fill = FieldFill.INSERT_UPDATE)
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    @Version
    @TableField("version")
    private Long version;


}
