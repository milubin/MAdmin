package top.horsemuzi.system.pojo.dto.open.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * API外部接口公共参数实例
 *
 * @author mabin
 * @date 2022/11/24 10:31
 **/
@ApiModel(value = "API外部接口公共参数实例")
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiParams implements Serializable {
    private static final long serialVersionUID = -6794665056750163152L;

    @NotBlank(message = "参数错误", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("应用创建完成后被分配的key")
    private String appKey;

    @NotBlank(message = "参数错误", groups = {ValidTwo.class})
    @ApiModelProperty("用于调用API的access_token访问令牌")
    private String accessToken;

    @NotBlank(message = "参数错误", groups = {ValidTwo.class})
    @ApiModelProperty(value = "API接口code标识", notes = "每个接口有一个唯一的接口标识码")
    private String apiCode;

    @NotNull(message = "参数错误", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty(value = "时间戳", notes = "时区: GMT+8, 接口请求超时时间默认: 3分钟")
    private Long timestamp;

    @NotBlank(message = "参数错误", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty(value = "API协议版本, 当前版本: v1", example = "v1")
    private String version;

    @NotBlank(message = "参数错误", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("签名")
    private String sign;

    @ApiModelProperty(value = "签名算法类型, 为空则默认为: md5", notes = "签名算法类型: md5, sha1, sha256", example = "md5")
    private String signMethod;

    @ApiModelProperty("业务参数")
    private String data;

}
