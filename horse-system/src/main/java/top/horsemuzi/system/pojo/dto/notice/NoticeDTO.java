package top.horsemuzi.system.pojo.dto.notice;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 通知公告DTO实例
 *
 * @author 马滨
 * @date 2022/10/06 14:39
 **/

@Data
public class NoticeDTO implements Serializable {

    private static final long serialVersionUID = 4011409597104837423L;

    @NotNull(message = "通知公告主键id不能为空", groups = {ValidTwo.class})
    @ApiModelProperty("主键id")
    private Long id;

    @NotBlank(message = "通知公告标题不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("通知公告标题")
    private String title;

    @NotNull(message = "通知公告类型不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("类型：0-通知，1-公告")
    private Integer type;

    @NotBlank(message = "通知公告摘要不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("通知公告摘要信息")
    private String summary;

    @NotBlank(message = "通知公告内容不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("通知公告内容")
    private String content;

    @NotBlank(message = "通知公告状态不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("状态：0-草稿，1-已发布")
    private String status;

    @ApiModelProperty("发布对象类型：0-所有用户，1-自定义角色，2-自定义用户")
    private Integer publishType;

    @ApiModelProperty("发布对象信息：表示自定义 角色/用户 的主键id")
    private String publishInfo;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    private Boolean deleted;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;

    @ApiModelProperty("通知公告发布记录总数量")
    private Integer totalNoticeRecordCount;

    @ApiModelProperty("通知公告发布记录已读数量")
    private Integer readNoticeRecordCount;
}
