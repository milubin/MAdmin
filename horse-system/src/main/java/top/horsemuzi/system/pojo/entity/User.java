package top.horsemuzi.system.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseEntity;
import top.horsemuzi.system.handle.encrypt.SM4TypeHandler;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 管理用户表
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-05-07 22:44:27
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@TableName(value = "tb_user", autoResultMap = true)
@ApiModel(value = "User对象", description = "管理用户表(用户N-1角色)")
public class User extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("用户名")
    @TableField("username")
    private String username;

    @ApiModelProperty("用户名拼音")
    @TableField("pinyin_username")
    private String pinyinUsername;

    @ApiModelProperty("用户名拼音首字母缩写")
    @TableField("pinyin_acronym_username")
    private String pinyinAcronymUsername;

    @ApiModelProperty("密码")
    @TableField("password")
    private String password;

    @ApiModelProperty("密码加密盐值")
    @TableField("salt")
    private String salt;

    @ApiModelProperty("所有角色权限标识, 逗号分割")
    @TableField("role_codes")
    private String roleCodes;

    @ApiModelProperty("是否已实名认证:0-是, 1-否")
    @TableField("real_name_auth")
    private String realNameAuth;

    @ApiModelProperty("真实姓名")
    @TableField(value = "real_name", typeHandler = SM4TypeHandler.class)
    private String realName;

    @ApiModelProperty("身份证号")
    @TableField(value = "id_number", typeHandler = SM4TypeHandler.class)
    private String idNumber;

    @ApiModelProperty("用户简介")
    @TableField("user_desc")
    private String userDesc;

    @ApiModelProperty("用户头像")
    @TableField("avatar")
    private String avatar;

    @ApiModelProperty("用户性别：0-女，1-男，2-未知")
    @TableField("sex")
    private String sex;

    @ApiModelProperty("用户手机号")
    @TableField(value = "phone")
    private String phone;

    @ApiModelProperty("用户邮箱")
    @TableField(value = "email")
    private String email;

    @ApiModelProperty("账号状态：0-正常，1-停用")
    @TableField("status")
    private String status;

    @ApiModelProperty("是否开启强制更新密码，0-是, 1-否")
    @TableField("force_update_paw")
    private String forceUpdatePaw;

    @ApiModelProperty("强制更新间隔,单位:天")
    @TableField("force_update_interval")
    private Integer forceUpdateInterval;

    @ApiModelProperty("最后更新密码时间")
    @TableField("last_update_pwd_time")
    private Date lastUpdatePwdTime;

    @ApiModelProperty("地址")
    @TableField("address")
    private String address;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    @TableField("is_deleted")
    @TableLogic
    private Boolean deleted;

    @ApiModelProperty("最后登录ip")
    @TableField("last_login_ip")
    private String lastLoginIp;

    @ApiModelProperty("最后登录时间")
    @TableField("last_login_time")
    private Date lastLoginTime;

    @ApiModelProperty("最后登录区域地址")
    @TableField("last_login_area")
    private String lastLoginArea;

    @ApiModelProperty("区域地址经度")
    @TableField("longitude")
    private BigDecimal longitude;

    @ApiModelProperty("区域地址纬度")
    @TableField("latitude")
    private BigDecimal latitude;

    @ApiModelProperty("最后登录系统")
    @TableField("last_login_os")
    private String lastLoginOs;

    @ApiModelProperty("最后登录客户端")
    @TableField("last_login_client")
    private String lastLoginClient;

    @ApiModelProperty("是否移动端：0-是，1-否")
    @TableField("mobile")
    private String mobile;

    @ApiModelProperty("总在线时长")
    @TableField("total_online_time")
    private Long totalOnlineTime;

    @ApiModelProperty("总登录次数")
    @TableField("login_count")
    private Long loginCount;

    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("创建时间")
    @TableField(value = "gmt_create", fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    @TableField(value = "create_id", fill = FieldFill.INSERT)
    private Long createId;

    @ApiModelProperty("创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty("更新时间")
    @TableField(value = "gmt_modified", fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    @TableField(value = "modified_id", fill = FieldFill.INSERT_UPDATE)
    private Long modifiedId;

    @ApiModelProperty("更新人")
    @TableField(value = "modified_user", fill = FieldFill.INSERT_UPDATE)
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    @TableField("version")
    @Version
    private Long version;


}
