package top.horsemuzi.system.pojo.dto.schedule.quartz;

import lombok.Data;
import lombok.EqualsAndHashCode;
import top.horsemuzi.system.base.BaseQuery;

import java.io.Serializable;

/**
 * Quartz任务调度查询实例
 *
 * @author mabin
 * @date 2022/07/02 14:06
 **/

@EqualsAndHashCode(callSuper = true)
@Data
public class QuartzJobQuery extends BaseQuery implements Serializable {

    private static final long serialVersionUID = -3776913502014708703L;

    private String jobName;

    private String jobGroup;

    private String invokeTarget;

    private Integer concurrent;

    private Integer misfirePolicy;

    private String triggerName;

    private String triggerGroup;

    private String createUser;

}
