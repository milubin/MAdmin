package top.horsemuzi.system.pojo.dto.open.log;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelIgnore;
import cn.hutool.core.date.DatePattern;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 开放API请求日志DTO
 *
 * @author 马滨
 * @date 2022/11/21 22:28
 **/
@Data
public class ApiLogDTO implements Serializable {

    @ExcelIgnore
    private static final long serialVersionUID = 3065316622089227064L;

    @ExcelIgnore
    @ApiModelProperty("主键id")
    private Long id;

    @Excel(name = "链路ID", width = 20)
    @ApiModelProperty("链路ID")
    private String traceId;

    @ExcelIgnore
    @ApiModelProperty("应用id")
    private Long appId;

    @Excel(name = "应用appKey", width = 25)
    @ApiModelProperty("应用appKey")
    private String appKey;

    @Excel(name = "应用名称", width = 25)
    @ApiModelProperty("应用名称")
    private String appName;

    @Excel(name = "API标识", width = 25)
    @ApiModelProperty("API标识code")
    private String apiCode;

    @Excel(name = "API功能", width = 25)
    @ApiModelProperty("API功能")
    private String apiFunction;

    @ExcelIgnore
    @ApiModelProperty("body请求参数（API默认POST请求）")
    private String body;

    @ExcelIgnore
    @ApiModelProperty("异常名称")
    private String excName;

    @ExcelIgnore
    @ApiModelProperty("异常详情")
    private String excMessage;

    @Excel(name = "请求ip", width = 25)
    @ApiModelProperty("请求ip")
    private String ip;

    @Excel(name = "ip归属地", width = 25)
    @ApiModelProperty("ip归属地")
    private String area;

    @Excel(name = "终端系统", width = 25)
    @ApiModelProperty("终端系统")
    private String os;

    @Excel(name = "浏览器", width = 25)
    @ApiModelProperty("浏览器")
    private String browser;

    @Excel(name = "请求状态", replace = {"成功_0", "正常_1"})
    @ApiModelProperty("操作状态: 0-成功, 1-异常")
    private String status;

    @ExcelIgnore
    @ApiModelProperty("响应信息")
    private String data;

    @Excel(name = "响应状态码")
    @ApiModelProperty("状态码")
    private Integer code;

    @Excel(name = "请求耗时", suffix = "ms")
    @ApiModelProperty("操作耗时, 单位:ms")
    private Long consume;

    @ExcelIgnore
    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    private Boolean deleted;

    @ExcelIgnore
    @ApiModelProperty("备注")
    private String remark;

    @Excel(name = "请求时间", width = 20, format = DatePattern.NORM_DATETIME_PATTERN)
    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ExcelIgnore
    @ApiModelProperty("创建人id")
    private Long createId;

    @ExcelIgnore
    @ApiModelProperty("创建人")
    private String createUser;

    @ExcelIgnore
    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ExcelIgnore
    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ExcelIgnore
    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ExcelIgnore
    @ApiModelProperty("乐观锁")
    private Long version;

}
