package top.horsemuzi.system.pojo.dto.log;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import top.horsemuzi.system.base.BaseQuery;

import java.io.Serializable;

/**
 * 系统操作日志
 *
 * @author mabin
 * @date 2022/06/30 20:37
 **/
@EqualsAndHashCode(callSuper = true)
@Data
public class SystemLogQuery extends BaseQuery implements Serializable {

    private static final long serialVersionUID = 5555528130664360592L;

    @ApiModelProperty("链路ID")
    private String traceId;

    @ApiModelProperty("日志类型: 0-操作日志, 1-异常日志")
    private Integer logType;

    @ApiModelProperty("操作模块")
    private String module;

    @ApiModelProperty("操作类型")
    private String type;

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("操作方法")
    private String method;

    @ApiModelProperty("param参数")
    private String params;

    @ApiModelProperty("body参数")
    private String body;

    @ApiModelProperty("异常名称")
    private String excName;

    @ApiModelProperty("异常详情")
    private String excMessage;

    @ApiModelProperty("访问IP")
    private String ip;

    @ApiModelProperty("ip归属地")
    private String area;

    @ApiModelProperty("响应结果")
    private String data;

    @ApiModelProperty("状态码")
    private Integer code;

}
