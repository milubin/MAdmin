package top.horsemuzi.system.pojo.dto.open.app;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import cn.hutool.core.date.DatePattern;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 开放API接口类型导出DTO
 *
 * @author mabin
 * @date 2022/11/23 13:53
 **/
@ExcelTarget(value = "AppExportDTO")
@Data
public class AppCategoryExportDTO implements Serializable {
    private static final long serialVersionUID = -8553192799840212425L;

    @Excel(name = "类型名称", width = 30)
    @ApiModelProperty("API类型名称")
    private String categoryName;

    @Excel(name = "类型描述", width = 60)
    @ApiModelProperty("API类型描述")
    private String categoryDescription;

    @Excel(name = "备注", width = 60)
    @ApiModelProperty("备注")
    private String remark;

    @Excel(name = "创建时间", width = 20, format = DatePattern.NORM_DATETIME_PATTERN)
    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @Excel(name = "创建人")
    @ApiModelProperty("创建人")
    private String createUser;

}
