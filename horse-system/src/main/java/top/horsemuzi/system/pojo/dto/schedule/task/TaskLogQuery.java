package top.horsemuzi.system.pojo.dto.schedule.task;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import top.horsemuzi.system.base.BaseQuery;

/**
 * Spring调度任务日志查询实例
 *
 * @author mabin
 * @date 2022/09/07 09:08
 **/
@EqualsAndHashCode(callSuper = true)
@Data
public class TaskLogQuery extends BaseQuery {
    private static final long serialVersionUID = -1185505881169374950L;

    @ApiModelProperty("任务id")
    private String taskId;

}

