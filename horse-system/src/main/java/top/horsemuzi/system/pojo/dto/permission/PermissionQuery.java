package top.horsemuzi.system.pojo.dto.permission;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import top.horsemuzi.system.base.BaseQuery;

import java.io.Serializable;

/**
 * 权限菜单查询实例
 *
 * @author mabin
 * @date 2022/07/07 18:00
 **/
@EqualsAndHashCode(callSuper = true)
@Data
public class PermissionQuery extends BaseQuery implements Serializable {
    private static final long serialVersionUID = -4236743818419014518L;

    @ApiModelProperty("菜单名称")
    private String name;

    @ApiModelProperty("菜单是否隐藏：0-显示，1-隐藏")
    private String visible;

    @ApiModelProperty("菜单状态：0-正常，1-停用")
    private String status;
}
