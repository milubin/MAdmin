package top.horsemuzi.system.pojo.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import java.io.Serializable;
import java.util.Date;
import top.horsemuzi.system.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统通知公告信息表
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-10-06 16:00:05
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("tb_notice")
@ApiModel(value = "Notice对象", description = "系统通知公告信息表")
public class Notice extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("通知公告标题")
    @TableField("title")
    private String title;

    @ApiModelProperty("类型：0-通知，1-公告")
    @TableField("type")
    private Integer type;

    @ApiModelProperty("通知公告摘要信息")
    @TableField("summary")
    private String summary;

    @ApiModelProperty("通知公告内容")
    @TableField("content")
    private String content;

    @ApiModelProperty("状态：0-草稿，1-已发布")
    @TableField("status")
    private String status;

    @ApiModelProperty("发布对象类型：0-所有用户，1-自定义角色，2-自定义用户")
    @TableField("publish_type")
    private Integer publishType;

    @ApiModelProperty("发布对象信息：表示自定义 角色/用户 的主键id")
    @TableField("publish_info")
    private String publishInfo;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    @TableField("is_deleted")
    @TableLogic
    private Boolean deleted;

    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("创建时间")
    @TableField(value = "gmt_create", fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    @TableField(value = "create_id", fill = FieldFill.INSERT)
    private Long createId;

    @ApiModelProperty("创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty("更新时间")
    @TableField(value = "gmt_modified", fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    @TableField(value = "modified_id", fill = FieldFill.INSERT_UPDATE)
    private Long modifiedId;

    @ApiModelProperty("更新人")
    @TableField(value = "modified_user", fill = FieldFill.INSERT_UPDATE)
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    @TableField("version")
    @Version
    private Long version;


}
