package top.horsemuzi.system.pojo.dto.config;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;
import top.horsemuzi.system.core.office.excel.convert.DateTimeFormatConvert;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 系统配置相关DTO
 *
 * @author mabin
 * @date 2022/06/24 14:52
 **/

@ExcelIgnoreUnannotated
@NoArgsConstructor
@Data
public class ConfigDTO implements Serializable {

    private static final long serialVersionUID = -3434236433437444677L;
    private static final String TITLE = "系统配置信息";

    @NotNull(message = "系统配置ID不能为空", groups = {ValidTwo.class})
    @ApiModelProperty("主键id")
    private Long id;

    @ExcelProperty(value = {TITLE, "组别"})
    @NotNull(message = "参数配置组别不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("参数配置组别")
    private String configGroup;

    @ExcelProperty(value = {TITLE, "配置名称"})
    @NotNull(message = "参数名称不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("参数名称")
    private String configName;

    @ExcelProperty(value = {TITLE, "配置键名"})
    @NotNull(message = "参数键名不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("参数键名")
    private String configKey;

    @ExcelProperty(value = {TITLE, "配置键值"})
    @NotNull(message = "参数键值不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("参数键值")
    private String configValue;

    @ApiModelProperty("配置键值定义方式，0-input自定义，1-select字典类型")
    @NotNull(message = "配置键值定义方式不能为空", groups = {ValidOne.class, ValidTwo.class})
    private String configWay;

    @ExcelProperty(value = {TITLE, "是否内置"})
    @NotNull(message = "是否系统内置属性不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("是否系统内置（N-否 ，Y-是）,系统内置参数不能删除")
    private String internal;

    @ExcelProperty(value = {TITLE, "备注"})
    @ApiModelProperty("备注")
    private String remark;

    @ExcelProperty(value = {TITLE, "创建时间"}, converter = DateTimeFormatConvert.class)
    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ExcelProperty(value = {TITLE, "创建人"})
    @ApiModelProperty("创建人")
    private String createUser;

    @ExcelProperty(value = {TITLE, "更新时间"}, converter = DateTimeFormatConvert.class)
    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ExcelProperty(value = {TITLE, "更新人"})
    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;
}
