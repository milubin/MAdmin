package top.horsemuzi.system.pojo.dto.login;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.horsemuzi.common.validator.ValidOne;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 用户重置密码相关实例
 *
 * @author mabin
 * @date 2022/11/12 13:38
 **/
@Data
public class ResetDTO implements Serializable {

    private static final long serialVersionUID = 5270997720915919947L;

    @NotBlank(message = "绑定账号不能为空", groups = {ValidOne.class})
    @ApiModelProperty(value = "绑定账号", notes = "手机号/邮箱")
    private String bindAccount;

    @NotBlank(message = "新密码不能为空", groups = {ValidOne.class})
    @ApiModelProperty("新密码")
    private String password;

    @NotBlank(message = "验证码不能为空", groups = {ValidOne.class})
    @ApiModelProperty("验证码")
    private String code;

}
