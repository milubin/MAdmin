package top.horsemuzi.system.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseEntity;

import java.util.Date;

/**
 * <p>
 * 业务表配置
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-12-19 13:48:53
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("gen_table_config")
@ApiModel(value = "TableConfig对象", description = "业务表配置")
public class TableConfig extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("功能作者")
    @TableField("author")
    private String author;

    @ApiModelProperty("包路径")
    @TableField("package_name")
    private String packageName;

    @ApiModelProperty("模块名(默认取包路径的最后一部分)")
    @TableField("module_name")
    private String moduleName;

    @ApiModelProperty("自动去除表前缀: 0-是, 1-否")
    @TableField("remove_prefix")
    private String removePrefix;

    @ApiModelProperty("自动去除字段的is前缀: 0-是, 1-否")
    @TableField("remove_is_prefix")
    private String removeIsPrefix;

    @ApiModelProperty("表前缀(多个英文逗号分隔)")
    @TableField("prefix")
    private String prefix;

    @ApiModelProperty("表单是否添加自适应滚动效果, 默认:500px 0：是  1：否")
    @TableField("from_roll")
    private String fromRoll;

    @ApiModelProperty("项目版本号")
    @TableField("project_version")
    private String projectVersion;

    @ApiModelProperty("主键类型: 0-自增, 1-雪花算法、2-UUID")
    @TableField("use_id_type")
    private Integer useIdType;

    @ApiModelProperty("逻辑删除字段名, 默认: is_deleted")
    @TableField("logic_deleted_name")
    private String logicDeletedName;

    @ApiModelProperty("乐观锁字段名, 默认: version")
    @TableField("version_name")
    private String versionName;

    @ApiModelProperty("是否添加swagger注解, 0-是, 1-否")
    @TableField("swagger_annotation")
    private String swaggerAnnotation;

    @ApiModelProperty("列工具刷新按钮 0：是  1：否")
    @TableField("grid_tool_refresh")
    private String gridToolRefresh;

    @ApiModelProperty("列工具显式搜索行表单按钮 0：是  1：否")
    @TableField("grid_tool_search")
    private String gridToolSearch;

    @ApiModelProperty("列工具字段过滤按钮 0：是  1：否")
    @TableField("grid_tool_filter")
    private String gridToolFilter;

    @ApiModelProperty("列工具打印按钮 0：是  1：否")
    @TableField("grid_tool_print")
    private String gridToolPrint;

    @ApiModelProperty("是否开启@Mapper注解, 0-是, 1-否")
    @TableField("mapper_annotation")
    private String mapperAnnotation;

    @ApiModelProperty("是否开启@RestController注解, 0-是, 1-否")
    @TableField("rest_controller_annotation")
    private String restControllerAnnotation;

    @ApiModelProperty("Controller父类路径")
    @TableField("controller_supper_class")
    private String controllerSupperClass;

    @ApiModelProperty("Entity父类路径")
    @TableField("entity_supper_class")
    private String entitySupperClass;

    @ApiModelProperty("Query父类路径")
    @TableField("query_supper_class")
    private String querySupperClass;

    @ApiModelProperty(value = "Controller格式化名称", example = "%sController")
    @TableField("format_controller_file_name")
    private String formatControllerFileName;

    @ApiModelProperty(value = "Service格式化名称", example = "%sService")
    @TableField("format_service_file_name")
    private String formatServiceFileName;

    @ApiModelProperty(value = "ServiceImpl格式化名称", example = "%sServiceImpl")
    @TableField("format_service_impl_file_name")
    private String formatServiceImplFileName;

    @ApiModelProperty(value = "Mapper格式化名称", example = "%sMapper")
    @TableField("format_mapper_file_name")
    private String formatMapperFileName;

    @ApiModelProperty(value = "XML格式化名称", example = "%sMapper")
    @TableField("format_xml_file_name")
    private String formatXmlFileName;

    @ApiModelProperty("是否生成xml文件的baseResultMap")
    @TableField("enable_base_result_map")
    private String enableBaseResultMap;

    @ApiModelProperty("是否生成xml文件的baseColumnList")
    @TableField("enable_base_column_list")
    private String enableBaseColumnList;

    @ApiModelProperty("是否开启@Lombok注解, 0-是, 1-否")
    @TableField("lombok_annotation")
    private String lombokAnnotation;

    @ApiModelProperty("是否生成lombok的链式调用注解")
    @TableField("enable_chain_model")
    private String enableChainModel;

    @ApiModelProperty(value = "注释日期格式化", example = "yyyy-MM-dd HH:mm:ss")
    @TableField("format_comment_date")
    private String formatCommentDate;

    @ApiModelProperty(value = "是否融合MAdmin系统, 0-是, 1-否", notes = "融合情况下,讲使用MAdmin自定义MP扩展处理")
    @TableField("fuse_madmin_system")
    private String fuseMadminSystem;

    @ApiModelProperty("基础字段（多个英文逗号分隔）")
    @TableField("base_field")
    private String baseField;

    @ApiModelProperty("是否导出, 0-是, 1-否")
    @TableField("whether_export")
    private String whetherExport;

    @ApiModelProperty("导出类型, 0-EXCEL, 1-CSV")
    @TableField("export_type")
    private Integer exportType;

    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    @TableField("is_deleted")
    @TableLogic
    private Boolean deleted;

    @ApiModelProperty("创建时间")
    @TableField(value = "gmt_create", fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    @TableField(value = "create_id", fill = FieldFill.INSERT)
    private Long createId;

    @ApiModelProperty("创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty("更新时间")
    @TableField(value = "gmt_modified", fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    @TableField(value = "modified_id", fill = FieldFill.INSERT_UPDATE)
    private Long modifiedId;

    @ApiModelProperty("更新人")
    @TableField(value = "modified_user", fill = FieldFill.INSERT_UPDATE)
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    @TableField("version")
    @Version
    private Long version;


}
