package top.horsemuzi.system.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseEntity;

import java.util.Date;

/**
 * <p>
 * 开放API应用接口信息
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-11-18 22:09:28
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("open_api")
@ApiModel(value = "Api对象", description = "开放API应用接口信息")
public class Api extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("API类型id")
    @TableField("api_category_id")
    private Long apiCategoryId;

    @ApiModelProperty("API标识code")
    @TableField("api_code")
    private String apiCode;

    @ApiModelProperty("API类路径")
    @TableField("api_class")
    private String apiClass;

    @ApiModelProperty("API方法名")
    @TableField("api_method")
    private String apiMethod;

    @ApiModelProperty("API功能")
    @TableField("api_function")
    private String apiFunction;

    @ApiModelProperty("API描述")
    @TableField("api_description")
    private String apiDescription;

    @ApiModelProperty("API版本，默认v1")
    @TableField("api_version")
    private String apiVersion;

    @ApiModelProperty("API状态: 0-正常, 1-已下线")
    @TableField("api_status")
    private String apiStatus;

    @ApiModelProperty("日请求阈值(APP粒度)")
    @TableField("daily_request_limit")
    private Long dailyRequestLimit;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    @TableField("is_deleted")
    @TableLogic
    private Boolean deleted;

    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("创建时间")
    @TableField(value = "gmt_create", fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    @TableField(value = "create_id", fill = FieldFill.INSERT)
    private Long createId;

    @ApiModelProperty("创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty("更新时间")
    @TableField(value = "gmt_modified", fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    @TableField(value = "modified_id", fill = FieldFill.INSERT_UPDATE)
    private Long modifiedId;

    @ApiModelProperty("更新人")
    @TableField(value = "modified_user", fill = FieldFill.INSERT_UPDATE)
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    @TableField("version")
    @Version
    private Long version;


}
