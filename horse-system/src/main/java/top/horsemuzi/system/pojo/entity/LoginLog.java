package top.horsemuzi.system.pojo.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import java.io.Serializable;
import java.util.Date;
import top.horsemuzi.system.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统登录日志记录表
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-09-26 21:50:38
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("tb_login_log")
@ApiModel(value = "LoginLog对象", description = "系统登录日志记录表")
public class LoginLog extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("链路ID")
    @TableField("trace_id")
    private String traceId;

    @ApiModelProperty("线程名称")
    @TableField("thread_name")
    private String threadName;

    @ApiModelProperty("用户名")
    @TableField("username")
    private String username;

    @ApiModelProperty("body参数")
    @TableField("body")
    private String body;

    @ApiModelProperty("异常名称")
    @TableField("exc_name")
    private String excName;

    @ApiModelProperty("异常详情")
    @TableField("exc_message")
    private String excMessage;

    @ApiModelProperty("访问IP")
    @TableField("ip")
    private String ip;

    @ApiModelProperty("ip归属地")
    @TableField("area")
    private String area;

    @ApiModelProperty("终端系统")
    @TableField("os")
    private String os;

    @ApiModelProperty("浏览器")
    @TableField("browser")
    private String browser;

    @ApiModelProperty("是否移动端: 0-是, 1-否")
    @TableField("is_mobile")
    private String mobile;

    @ApiModelProperty("登录方式：0-账号密码，1-邮箱登录，2-手机号登录")
    @TableField("login_way")
    private Integer loginWay;

    @ApiModelProperty("操作状态: 0-成功, 1-异常")
    @TableField("status")
    private String status;

    @ApiModelProperty("访问来源: 0-管理系统, 1-门户网站")
    @TableField("origin")
    private String origin;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    @TableField("is_deleted")
    @TableLogic
    private Boolean deleted;

    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("创建时间")
    @TableField(value = "gmt_create", fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    @TableField(value = "create_id", fill = FieldFill.INSERT)
    private Long createId;

    @ApiModelProperty("创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty("更新时间")
    @TableField(value = "gmt_modified", fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    @TableField(value = "modified_id", fill = FieldFill.INSERT_UPDATE)
    private Long modifiedId;

    @ApiModelProperty("更新人")
    @TableField(value = "modified_user", fill = FieldFill.INSERT_UPDATE)
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    @TableField("version")
    @Version
    private Long version;


}
