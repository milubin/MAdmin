package top.horsemuzi.system.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseEntity;

import java.util.Date;

/**
 * <p>
 * 代码生成业务表
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-12-19 13:48:53
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("gen_table")
@ApiModel(value = "Table对象", description = "代码生成业务表")
public class Table extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("业务表导入类型: 0-基于DB导入, 1-基于DDL导入")
    @TableField("import_type")
    private String importType;

    @ApiModelProperty("业务表配置id")
    @TableField("table_config_id")
    private Long tableConfigId;

    @ApiModelProperty("数据源id")
    @TableField("datasource_id")
    private Long datasourceId;

    @ApiModelProperty("父权限菜单id")
    @TableField("parent_menu_id")
    private Long parentMenuId;

    @ApiModelProperty("角色权限标识")
    @TableField("role_code")
    private String roleCode;

    @ApiModelProperty("菜单图标")
    @TableField("menu_icon")
    private String menuIcon;

    @ApiModelProperty("数据库名")
    @TableField("database_name")
    private String databaseName;

    @ApiModelProperty("表名")
    @TableField("table_name")
    private String tableName;

    @ApiModelProperty("表描述")
    @TableField("table_comment")
    private String tableComment;

    @ApiModelProperty("实体类名")
    @TableField("class_name")
    private String className;

    @ApiModelProperty("功能描述")
    @TableField("function_name")
    private String functionName;

    @ApiModelProperty(value = "DDL语句", notes = "只有importType=1时, 此值才不为空")
    @TableField("table_ddl")
    private String tableDdl;

    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    @TableField("is_deleted")
    @TableLogic
    private Boolean deleted;

    @ApiModelProperty("创建时间")
    @TableField(value = "gmt_create", fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    @TableField(value = "create_id", fill = FieldFill.INSERT)
    private Long createId;

    @ApiModelProperty("创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty("更新时间")
    @TableField(value = "gmt_modified", fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    @TableField(value = "modified_id", fill = FieldFill.INSERT_UPDATE)
    private Long modifiedId;

    @ApiModelProperty("更新人")
    @TableField(value = "modified_user", fill = FieldFill.INSERT_UPDATE)
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    @TableField("version")
    @Version
    private Long version;


}
