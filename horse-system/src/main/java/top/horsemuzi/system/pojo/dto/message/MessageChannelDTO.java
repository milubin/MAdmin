package top.horsemuzi.system.pojo.dto.message;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.horsemuzi.common.validator.ValidOne;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 消息渠道DTO
 *
 * @author 马滨
 * @date 2022/09/25 18:23
 **/
@Data
public class MessageChannelDTO implements Serializable {

    private static final long serialVersionUID = 5329822550728454214L;

    @ApiModelProperty("主键id")
    private Long id;

    @NotBlank(message = "渠道名称不能为空", groups = {ValidOne.class})
    @ApiModelProperty("渠道名称")
    private String name;

    @NotNull(message = "渠道标识不能为空", groups = {ValidOne.class})
    @ApiModelProperty("渠道标识：10-钉钉机器人，20-钉钉工作通知，30-飞书机器人，40-飞书工作通知")
    private Integer channel;

    @NotBlank(message = "渠道配置不能为空", groups = {ValidOne.class})
    @ApiModelProperty("渠道配置")
    private String config;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    private Boolean deleted;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;
}
