package top.horsemuzi.system.pojo.dto.open.api;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidThree;
import top.horsemuzi.common.validator.ValidTwo;
import top.horsemuzi.common.validator.custom.annotation.Xss;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 开放API接口DTO实例
 *
 * @author 马滨
 * @date 2022/11/18 22:16
 **/

@Data
public class ApiDTO implements Serializable {

    private static final long serialVersionUID = -4573917437521034168L;

    @NotNull(message = "API接口主键id不能为空", groups = {ValidTwo.class, ValidThree.class})
    @ApiModelProperty("主键id")
    private Long id;

    @ApiModelProperty("API类型id")
    private Long apiCategoryId;

    @NotBlank(message = "API标识code不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("API标识code")
    private String apiCode;

    @NotBlank(message = "API类路径不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("API类路径")
    private String apiClass;

    @NotBlank(message = "API方法名不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("API方法名")
    private String apiMethod;

    @Xss(message = "API功能名称不能包含脚本字符", groups = {ValidOne.class, ValidTwo.class})
    @NotBlank(message = "API功能名称不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("API功能名称")
    private String apiFunction;

    @ApiModelProperty("API描述")
    private String apiDescription;

    @ApiModelProperty("API版本，默认v1")
    private String apiVersion;

    @NotBlank(message = "API状态不能为空", groups = {ValidOne.class, ValidTwo.class, ValidThree.class})
    @ApiModelProperty("API状态: 0-正常, 1-已下线")
    private String apiStatus;

    @NotNull(message = "API接口日请求限制阈值不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("日请求阈值(APP粒度)")
    private Long dailyRequestLimit;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    private Boolean deleted;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;
}
