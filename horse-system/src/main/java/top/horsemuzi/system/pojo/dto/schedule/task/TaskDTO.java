package top.horsemuzi.system.pojo.dto.schedule.task;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description {description}
 * @date 2022/8/21 10:50
 */
@Data
public class TaskDTO implements Serializable {
    private static final long serialVersionUID = 1476429173022430831L;

    @NotNull(message = "任务主键id不能为空", groups = {ValidTwo.class})
    @ApiModelProperty("主键id")
    private Long id;

    @NotNull(message = "任务名称不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("任务名称")
    private String taskName;

    @NotBlank(message = "任务实例不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("任务实例")
    private String className;

    @NotBlank(message = "任务方法不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("任务方法")
    private String methodName;

    @ApiModelProperty("任务参数")
    private String methodParams;

    @NotBlank(message = "cron任务表达式不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("cron任务表达式")
    private String cron;

    @NotNull(message = "任务状态不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("任务状态： 0-暂停，1-正常")
    private Integer status;

    @ApiModelProperty("任务备注")
    private String remark;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    private Boolean deleted;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;

}
