package top.horsemuzi.system.pojo.dto.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.horsemuzi.system.pojo.dto.role.RoleDTO;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户角色关联信息
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/04/18 09:33
 **/
@Data
public class UserRoleDTO implements Serializable {
    private static final long serialVersionUID = 6226990935804891853L;

    @ApiModelProperty("主键id")
    private Long id;

    /**
     * {@link UserDTO#getId()}
     */
    @ApiModelProperty("用户id tb_user.id")
    private Long userId;

    /**
     * {@link RoleDTO#getId()}
     */
    @ApiModelProperty("角色id tb_role.id")
    private Long roleId;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;
}
