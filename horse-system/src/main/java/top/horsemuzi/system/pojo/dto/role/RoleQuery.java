package top.horsemuzi.system.pojo.dto.role;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseQuery;

import java.io.Serializable;
import java.util.Date;

/**
 * 角色管理查询相关
 *
 * @author mabin
 * @date 2022/06/06 10:26
 **/
@EqualsAndHashCode(callSuper = true)
@ApiModel("角色管理查询相关")
@Data
@Accessors(chain = true)
public class RoleQuery extends BaseQuery implements Serializable {
    private static final long serialVersionUID = -3635148418718682718L;

    @ApiModelProperty("用户名")
    private String name;

    @ApiModelProperty("角色权限标识")
    private String roleCode;

    @ApiModelProperty("角色状态：0-正常，1-停用")
    private String status;

}
