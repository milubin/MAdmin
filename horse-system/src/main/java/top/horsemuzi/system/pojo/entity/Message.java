package top.horsemuzi.system.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseEntity;

import java.util.Date;

/**
 * <p>
 * 消息信息表
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-09-30 10:59:09
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("tb_message")
@ApiModel(value = "Message对象", description = "消息信息表")
public class Message extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("渠道标识：10-钉钉机器人，20-钉钉工作通知，30-飞书机器人，40-飞书工作通知，50-邮箱，60-短信，70-微信公众号\"")
    @TableField("channel")
    private Integer channel;

    @ApiModelProperty("消息记录id")
    @TableField("record_id")
    private String recordId;

    @ApiModelProperty("消息接收者(英文逗号分隔)")
    @TableField("receiver")
    private String receiver;

    @ApiModelProperty("stream流名称")
    @TableField("stream_name")
    private String streamName;

    @ApiModelProperty("stream消费者组")
    @TableField("stream_group")
    private String streamGroup;

    @ApiModelProperty("消息内容")
    @TableField("content")
    private String content;

    @ApiModelProperty("消息发送内容")
    @TableField("message_info")
    private String messageInfo;

    @ApiModelProperty("消息状态: 0-初始化, 1-消费成功, 2-消费且ACK成功, 3-消费失败")
    @TableField("status")
    private String status;

    @ApiModelProperty("消息最大重试次数（系统默认重试3次）")
    @TableField("retry_count")
    private Integer retryCount;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    @TableField("is_deleted")
    @TableLogic
    private Boolean deleted;

    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("创建时间")
    @TableField(value = "gmt_create", fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    @TableField(value = "create_id", fill = FieldFill.INSERT)
    private Long createId;

    @ApiModelProperty("创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty("更新时间")
    @TableField(value = "gmt_modified", fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    @TableField(value = "modified_id", fill = FieldFill.INSERT_UPDATE)
    private Long modifiedId;

    @ApiModelProperty("更新人")
    @TableField(value = "modified_user", fill = FieldFill.INSERT_UPDATE)
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    @TableField("version")
    @Version
    private Long version;


}
