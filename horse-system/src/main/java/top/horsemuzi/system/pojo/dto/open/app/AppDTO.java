package top.horsemuzi.system.pojo.dto.open.app;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.horsemuzi.common.validator.ValidFour;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidThree;
import top.horsemuzi.common.validator.ValidTwo;
import top.horsemuzi.common.validator.custom.annotation.CreditCode;
import top.horsemuzi.common.validator.custom.annotation.IdCard;
import top.horsemuzi.common.validator.custom.annotation.Phone;
import top.horsemuzi.common.validator.custom.annotation.Xss;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 开放API应用DTO
 *
 * @author 马滨
 * @date 2022/11/18 22:19
 **/
@Data
public class AppDTO implements Serializable {
    private static final long serialVersionUID = -8504039626393340569L;

    @NotNull(message = "应用主键id不能为空", groups = {ValidTwo.class, ValidThree.class, ValidFour.class})
    @ApiModelProperty("主键id")
    private Long id;

    @Xss(message = "应用名称不能包含脚本字符", groups = {ValidOne.class, ValidTwo.class})
    @NotBlank(message = "应用名称不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("应用名称")
    private String appName;

    @ApiModelProperty("应用图标")
    private String appIcon;

    @NotBlank(message = "应用appKey不能为空", groups = {ValidTwo.class})
    @ApiModelProperty("应用appKey")
    private String appKey;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("应用appSecret")
    private String appSecret;

    @ApiModelProperty("访问令牌token")
    private String accessToken;

    @ApiModelProperty("应用白名单, 空-不校验")
    private String ipWhiteList;

    @ApiModelProperty("应用描述")
    private String appDescription;

    @NotBlank(message = "审批状态不能为空", groups = {ValidThree.class})
    @ApiModelProperty("审批状态： 0-审批中， 1-通过，2-拒绝")
    private String approvalStatus;

    @ApiModelProperty("应用审批拒绝原因")
    private String rejectReason;

    @NotBlank(message = "申请主体不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("申请主体：0-个人开发者，1-企业开发")
    private String approvalSubject;

    @IdCard(groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("身份证号(个人主体需要提供身份证号)")
    private String idCard;

    @CreditCode(groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("社会信用码(公司主体需提供社会统一信用码)")
    private String creditCode;

    @ApiModelProperty("详细地址(申请主体对应的地址信息)")
    private String address;

    @Phone(groups = {ValidOne.class, ValidTwo.class})
    @NotBlank(message = "手机号不能为空", groups = {ValidOne.class, ValidTwo.class})
    @ApiModelProperty("手机号")
    private String phone;

    @Email(message = "邮箱格式错误", groups = {ValidOne.class, ValidTwo.class, ValidFour.class})
    @NotBlank(message = "邮箱不能为空", groups = {ValidOne.class, ValidTwo.class, ValidFour.class})
    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("API类型id列表，英文逗号分隔")
    private String apiCategoryIds;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    private Boolean deleted;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;

    @ApiModelProperty("api接口类型id列表")
    private List<Long> apiCategoryIdList;

}
