package top.horsemuzi.system.pojo.dto.open.app;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import cn.hutool.core.date.DatePattern;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 开放API应用导出DTO
 *
 * @author mabin
 * @date 2022/11/23 13:51
 **/

@Data
public class AppExportDTO implements Serializable {
    private static final long serialVersionUID = -1154403139037866128L;

    @Excel(name = "应用名称", width = 20, needMerge = true)
    @ApiModelProperty("应用名称")
    private String appName;

    @Excel(name = "应用appKey", width = 25, needMerge = true)
    @ApiModelProperty("应用appKey")
    private String appKey;

    @Excel(name = "应用描述", width = 50, needMerge = true)
    @ApiModelProperty("应用描述")
    private String appDescription;

    @Excel(name = "应用白名单", width = 80, needMerge = true)
    @ApiModelProperty("应用白名单, 空-不校验")
    private String ipWhiteList;

    @Excel(name = "审批状态", replace = {"审批中_0", "通过_1", "拒绝_2"}, needMerge = true)
    @ApiModelProperty("审批状态： 0-审批中， 1-通过，2-拒绝")
    private String approvalStatus;

    @Excel(name = "拒绝原因", width = 50, needMerge = true)
    @ApiModelProperty("应用审批拒绝原因")
    private String rejectReason;

    @Excel(name = "申请主体", replace = {"个人开发者_0", "企业开发_1"}, needMerge = true)
    @ApiModelProperty("申请主体：0-个人开发者，1-企业开发")
    private String approvalSubject;

    @Excel(name = "身份证号", width = 50, needMerge = true)
    @ApiModelProperty("身份证号(个人主体需要提供身份证号)")
    private String idCard;

    @Excel(name = "社会信用码", width = 50, needMerge = true)
    @ApiModelProperty("社会信用码(公司主体需提供社会统一信用码)")
    private String creditCode;

    @Excel(name = "详细地址", width = 60, needMerge = true)
    @ApiModelProperty("详细地址(申请主体对应的地址信息)")
    private String address;

    @Excel(name = "手机号", width = 20, needMerge = true)
    @ApiModelProperty("手机号")
    private String phone;

    @Excel(name = "邮箱", width = 20, needMerge = true)
    @ApiModelProperty("邮箱")
    private String email;

    @Excel(name = "备注", width = 60, needMerge = true)
    @ApiModelProperty("备注")
    private String remark;

    @Excel(name = "创建时间", width = 20, format = DatePattern.NORM_DATETIME_PATTERN, needMerge = true)
    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @Excel(name = "创建人", needMerge = true)
    @ApiModelProperty("创建人")
    private String createUser;

    @ExcelCollection(name = "应用类型信息")
    @ApiModelProperty("应用类型信息")
    private List<AppCategoryExportDTO> appCategoryExportDTOList;

}
