package top.horsemuzi.system.pojo.dto.log;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description {description}
 * @date 2022/8/6 22:37
 */
@Data
public class SearchQuery {

    @ApiModelProperty("grep检索关键词")
    private String keyOne;

    @ApiModelProperty("grep检索关键词")
    private String keyTwo;

    @ApiModelProperty("grep检索关键词")
    private String keyThree;

    @ApiModelProperty(value = "grep检索日志等级", example = "INFO、ERROR")
    private String level;

    @ApiModelProperty("检索日志日期")
    private Date date;

    @ApiModelProperty("tail检索行数")
    private Integer tailLastCount = 1;

    @ApiModelProperty(value = "grep -C 命令命中行上下文行数",example = "10、20、30...")
    private Integer contextLineCount;

}
