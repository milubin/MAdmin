package top.horsemuzi.system.pojo.dto.notice;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.horsemuzi.common.validator.ValidOne;
import top.horsemuzi.common.validator.ValidTwo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 通知公告发布记录DTO
 *
 * @author 马滨
 * @date 2022/10/06 16:03
 **/
@Data
public class NoticeRecordDTO implements Serializable {

    private static final long serialVersionUID = 6906048897952232817L;

    @ApiModelProperty("主键id")
    private Long id;

    @ApiModelProperty("通知公告ID, tb_notice.id")
    private Long noticeId;

    @ApiModelProperty("用户ID, tb_user.id")
    private Long userId;

    @ApiModelProperty("是否已读：0-未读，1-已读")
    private String status;

    @ApiModelProperty("读取时间")
    private Date readTime;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    private Boolean deleted;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    private Long createId;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    private Long version;

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("通知公告标题")
    private String title;

    @ApiModelProperty("类型：0-通知，1-公告")
    private Integer type;

    @ApiModelProperty("通知公告摘要信息")
    private String summary;

    @ApiModelProperty("通知公告内容")
    private String content;
}
