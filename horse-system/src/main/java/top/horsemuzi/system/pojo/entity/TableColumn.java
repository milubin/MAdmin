package top.horsemuzi.system.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import top.horsemuzi.system.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 业务字段表
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-12-19 13:48:53
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("gen_table_column")
@ApiModel(value = "TableColumn对象", description = "业务字段表")
public class TableColumn extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("业务表id")
    @TableField("table_id")
    private Long tableId;

    @ApiModelProperty("列名称")
    @TableField("column_name")
    private String columnName;

    @ApiModelProperty("列类型")
    @TableField("column_type")
    private String columnType;

    @ApiModelProperty("列说明")
    @TableField("column_comment")
    private String columnComment;

    @ApiModelProperty("Java字段名")
    @TableField("java_name")
    private String javaName;

    @ApiModelProperty("Java字段类型")
    @TableField("java_type")
    private String javaType;

    @ApiModelProperty("Java字段名（首字母大写）")
    @TableField("java_name_up")
    private String javaNameUp;

    @ApiModelProperty("Java字段属性包名")
    @TableField("package_name")
    private String packageName;

    @ApiModelProperty("排序")
    @TableField("sort")
    private Integer sort;

    @ApiModelProperty("自动填充  DEFAULT、INSERT、UPDATE、INSERT_UPDATE")
    @TableField("auto_fill")
    private String autoFill;

    @ApiModelProperty("主键 0：是  1：否")
    @TableField("pk_field")
    private String pkField;

    @ApiModelProperty("表单项 0：是  1：否")
    @TableField("form_item")
    private String formItem;

    @ApiModelProperty("表单项必填 0：是  1：否")
    @TableField("form_required")
    private String formRequired;

    @ApiModelProperty("表单类型")
    @TableField("form_type")
    private String formType;

    @ApiModelProperty("表单字典类型")
    @TableField("form_dict")
    private String formDict;

    @ApiModelProperty("表单项清空 0：是  1：否")
    @TableField("form_clearable")
    private String formClearable;

    @ApiModelProperty("表单占位符")
    @TableField("form_placeholder")
    private String formPlaceholder;

    @ApiModelProperty("表单项说明")
    @TableField("form_explain")
    private String formExplain;

    @ApiModelProperty("表单效验显式内容")
    @TableField("form_validator")
    private String formValidator;

    @ApiModelProperty("表单项最大字符数")
    @TableField("form_char_max_length")
    private Integer formCharMaxLength;

    @ApiModelProperty("列表项 0：是  1：否")
    @TableField("grid_item")
    private String gridItem;

    @ApiModelProperty("列表排序 0：是  1：否")
    @TableField("grid_sort")
    private String gridSort;

    @ApiModelProperty("列表单击复制 0：是  1：否")
    @TableField("grid_copy")
    private String gridCopy;

    @ApiModelProperty("列表项说明 0：是  1：否")
    @TableField("grid_explain")
    private String gridExplain;

    @ApiModelProperty("列表项宽度, 默认:120px")
    @TableField("grid_width")
    private Integer gridWidth;

    @ApiModelProperty("查询项 0：是  1：否")
    @TableField("query_item")
    private String queryItem;

    @ApiModelProperty("查询方式")
    @TableField("query_type")
    private String queryType;

    @ApiModelProperty("查询表单类型")
    @TableField("query_form_type")
    private String queryFormType;

    @ApiModelProperty("查询表单字典类型")
    @TableField("query_form_dict")
    private String queryFormDict;

    @ApiModelProperty("查询表单项清空 0：是  1：否")
    @TableField("query_form_clearable")
    private String queryFormClearable;

    @ApiModelProperty("查询表单占位符")
    @TableField("query_form_placeholder")
    private String queryFormPlaceholder;

    @ApiModelProperty("查询表单项宽度, 默认:240px")
    @TableField("query_form_width")
    private Integer queryFormWidth;

    @ApiModelProperty("查询表单实时检索项, 只允许一个表单项 0：是  1：否")
    @TableField("query_form_fetch")
    private String queryFormFetch;

    @ApiModelProperty("是否导出, 0-是, 1-否")
    @TableField("whether_export")
    private String whetherExport;

    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    @TableField("is_deleted")
    @TableLogic
    private Boolean deleted;

    @ApiModelProperty("创建时间")
    @TableField(value = "gmt_create", fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    @TableField(value = "create_id", fill = FieldFill.INSERT)
    private Long createId;

    @ApiModelProperty("创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty("更新时间")
    @TableField(value = "gmt_modified", fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    @TableField(value = "modified_id", fill = FieldFill.INSERT_UPDATE)
    private Long modifiedId;

    @ApiModelProperty("更新人")
    @TableField(value = "modified_user", fill = FieldFill.INSERT_UPDATE)
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    @TableField("version")
    @Version
    private Long version;


}
