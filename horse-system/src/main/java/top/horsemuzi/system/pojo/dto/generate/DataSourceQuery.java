package top.horsemuzi.system.pojo.dto.generate;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseQuery;

/**
 * 数据源Query
 *
 * @author mabin
 * @date 2022/12/19 14:11
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class DataSourceQuery extends BaseQuery {
    private static final long serialVersionUID = -5874718286431790176L;

    @ApiModelProperty("DB别名")
    private String dbName;


}
