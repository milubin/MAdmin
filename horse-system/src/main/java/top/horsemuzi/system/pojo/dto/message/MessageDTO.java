package top.horsemuzi.system.pojo.dto.message;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelIgnore;
import cn.hutool.core.date.DatePattern;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 消息信息DTO
 *
 * @author mabin
 * @date 2022/09/30 11:09
 **/
@Data
public class MessageDTO implements Serializable {

    private static final long serialVersionUID = 3654222571736118976L;

    @ExcelIgnore
    @ApiModelProperty("主键id")
    private Long id;

    @ExcelIgnore
    @ApiModelProperty("渠道标识：10-钉钉机器人，20-钉钉工作通知，30-飞书机器人，40-飞书工作通知，50-邮箱，60-短信，70-微信公众号")
    private Integer channel;

    @Excel(name = "渠道名称")
    @ApiModelProperty("渠道名称：文件导出时使用")
    private String channelName;

    @Excel(name = "消息记录id")
    @ApiModelProperty("消息记录id")
    private String recordId;

    @ApiModelProperty("消息接收者(英文逗号分隔)")
    private String receiver;

    @Excel(name = "stream流名称")
    @ApiModelProperty("stream流名称")
    private String streamName;

    @Excel(name = "stream消费者组")
    @ApiModelProperty("stream消费者组")
    private String streamGroup;

    @Excel(name = "消息内容")
    @ApiModelProperty("消息内容")
    private String content;

    @Excel(name = "消息状态", replace = {"初始_0", "消费成功_1", "消费且ACK成功_2", "消费失败_3"})
    @ApiModelProperty("消息状态: 0-初始化, 1-消费成功, 2-消费且ACK成功, 3-消费失败")
    private String status;

    @Excel(name = "发送次数",type = 10, suffix = "次")
    @ApiModelProperty("消息最大重试次数（系统默认重试3次）")
    private Integer retryCount;

    @ExcelIgnore
    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    private Boolean deleted;

    @Excel(name = "备注")
    @ApiModelProperty("备注")
    private String remark;

    @ExcelIgnore
    @ApiModelProperty("消息发送内容")
    private String messageInfo;

    @Excel(name = "创建时间", format = DatePattern.NORM_DATETIME_PATTERN)
    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ExcelIgnore
    @ApiModelProperty("创建人id")
    private Long createId;

    @ExcelIgnore
    @ApiModelProperty("创建人")
    private String createUser;

    @ExcelIgnore
    @ApiModelProperty("更新时间")
    private Date gmtModified;

    @ExcelIgnore
    @ApiModelProperty("更新人id")
    private Long modifiedId;

    @ExcelIgnore
    @ApiModelProperty("更新人")
    private String modifiedUser;

    @ExcelIgnore
    @ApiModelProperty("乐观锁")
    private Long version;

}
