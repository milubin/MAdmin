package top.horsemuzi.system.pojo.dto.open.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import top.horsemuzi.system.base.BaseQuery;

/**
 * 开放API应用查询实例
 *
 * @author 马滨
 **/

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class AppQuery extends BaseQuery {
    private static final long serialVersionUID = 8292805594733812972L;


    @ApiModelProperty("应用名称")
    private String appName;

    @ApiModelProperty("审批状态：0-审批中， 1-通过，2-拒绝")
    private String approvalStatus;


}
