package top.horsemuzi.system.pojo.dto.log;

import lombok.Data;

import java.io.Serializable;

/**
 * Echarts图表相关实例
 *
 * @author mabin
 * @date 2022/08/11 16:12
 **/
@Data
public class EchartDTO implements Serializable {
    private static final long serialVersionUID = 12799401824417861L;

    private String name;

    private Long value;

    private Integer logType;

}
