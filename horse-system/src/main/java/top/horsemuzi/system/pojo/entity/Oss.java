package top.horsemuzi.system.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import top.horsemuzi.system.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 对象存储文件信息表
 * </p>
 *
 * @author Mr.Horse
 * @since 2022-08-01 21:50:47
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("tb_oss")
@ApiModel(value = "Oss对象", description = "对象存储文件信息表")
public class Oss extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("原始文件名")
    @TableField("original_name")
    private String originalName;

    @ApiModelProperty("文件大小，单位：KB")
    @TableField("size")
    private Long size;

    @ApiModelProperty("文件后缀")
    @TableField("suffix")
    private String suffix;

    @ApiModelProperty("文件URL访问链接")
    @TableField("url")
    private String url;

    @ApiModelProperty("对象存储服务商")
    @TableField("service")
    private String service;

    @ApiModelProperty("文件备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("GITHUB服务商特有扩展字段（用于文件删除）")
    @TableField("sha")
    private String sha;

    @ApiModelProperty("是否内置: Y-是, N-否")
    @TableField("internal")
    private String internal;

    @ApiModelProperty("逻辑删除：0-正常，1-已删除")
    @TableField("is_deleted")
    @TableLogic
    private Boolean deleted;

    @ApiModelProperty("创建时间")
    @TableField(value = "gmt_create", fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty("创建人id")
    @TableField(value = "create_id", fill = FieldFill.INSERT)
    private Long createId;

    @ApiModelProperty("创建人")
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private String createUser;

    @ApiModelProperty("更新时间")
    @TableField(value = "gmt_modified", fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @ApiModelProperty("更新人id")
    @TableField(value = "modified_id", fill = FieldFill.INSERT_UPDATE)
    private Long modifiedId;

    @ApiModelProperty("更新人")
    @TableField(value = "modified_user", fill = FieldFill.INSERT_UPDATE)
    private String modifiedUser;

    @ApiModelProperty("乐观锁")
    @TableField("version")
    @Version
    private Long version;


}
