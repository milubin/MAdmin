package top.horsemuzi.system;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import org.apache.rocketmq.spring.autoconfigure.RocketMQAutoConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * MAdmin主启动类
 *
 * @author mabin
 * @date 2022/05/05 14:09
 **/

@EnableRetry
@EnableCaching
@EnableAsync
@EnableAspectJAutoProxy(exposeProxy = true)
@EnableTransactionManagement
@MapperScan(basePackages = "top.horsemuzi.system.mapper")
@ComponentScan(basePackages = {"com.plumelog", "top.horsemuzi"})
@SpringBootApplication(exclude = {DruidDataSourceAutoConfigure.class, RocketMQAutoConfiguration.class})
public class HorseSystemApplication {
    public static void main(String[] args) {
        SpringApplication.run(HorseSystemApplication.class, args);
    }
}
