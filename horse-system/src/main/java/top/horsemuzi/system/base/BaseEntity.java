package top.horsemuzi.system.base;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import top.horsemuzi.common.util.OrikaUtil;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;


/**
 * entity实体父类
 *
 * @author mabin
 * @date 2022/05/06 11:00
 **/
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = 4998758273222781240L;

    public static <A, B> Page<B> convertPage(Page<A> source, Class<B> clazz) {
        return convertPage(source, clazz, null);
    }

    public static <A, B> Page<B> convertPage(Page<A> source, Class<B> clazz, Map<String, String> refMap) {
        Page<B> page = new Page<>();
        if (Objects.nonNull(source)) {
            page.setTotal(source.getTotal())
                    .setCurrent(source.getCurrent())
                    .setSize(source.getSize())
                    .setPages(source.getPages())
                    .setRecords(OrikaUtil.convertList(source.getRecords(), clazz, refMap));
        }
        return page;
    }

}
