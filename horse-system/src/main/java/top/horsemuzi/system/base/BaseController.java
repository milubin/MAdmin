package top.horsemuzi.system.base;

import top.horsemuzi.common.result.Result;

/**
 * Controller控制层基础类
 *
 * @author mabin
 * @date 2022/05/06 10:26
 **/
public class BaseController {

    protected <T> Result<T> success() {
        return Result.success();
    }

    protected <T> Result<T> success(T data) {
        return Result.success(data);
    }

    protected <T> Result<T> success(String message, T data) {
        return Result.success(message, data);
    }

    protected <T> Result<T> error() {
        return Result.error();
    }

    protected <T> Result<T> error(T data) {
        return Result.error(data);
    }

    protected <T> Result<T> error(String message, T data) {
        return Result.error(message, data);
    }

}
