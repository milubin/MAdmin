package top.horsemuzi.system.base;

import cn.hutool.core.date.DatePattern;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;
import top.horsemuzi.common.convert.DateConvert;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;


/**
 * 基础查询参数实体父类
 *
 * @author mabin
 * @date 2022/05/06 11:00
 **/
public class BaseQuery implements Serializable {

    private static final long serialVersionUID = 4998758273222781240L;

    @ApiModelProperty("当前页,默认page=0")
    private Long page = 0L;

    @ApiModelProperty("每页数量,默认size=20")
    private Long size = 20L;

    @ApiModelProperty("排序字段")
    private String sortField;

    @ApiModelProperty("排序方式: ASC-正序, DESC-倒叙")
    private String sortWay;

    @ApiModelProperty("创建时间-开始时间(yyyy-MM-dd)")
    @DateTimeFormat(pattern = DatePattern.NORM_DATE_PATTERN)
    private Date createStart;

    @ApiModelProperty("创建时间-结束时间(yyyy-MM-dd)")
    @DateTimeFormat(pattern = DatePattern.NORM_DATE_PATTERN)
    private Date createEnd;

    public Long getPage() {
        return page;
    }

    public void setPage(Long page) {
        this.page = page;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public String getSortWay() {
        return sortWay;
    }

    public void setSortWay(String sortWay) {
        this.sortWay = sortWay;
    }

    public Date getCreateStart() {
        return createStart;
    }

    public void setCreateStart(Date createStart) {
        if (Objects.nonNull(createStart)) {
            this.createStart = DateConvert.getStartTimeOfDay(createStart);
        } else {
            this.createStart = null;
        }
    }

    public Date getCreateEnd() {
        return createEnd;
    }

    public void setCreateEnd(Date createEnd) {
        if (Objects.nonNull(createEnd)) {
            this.createEnd = DateConvert.getEndTimeOfDay(createEnd);
        } else {
            this.createEnd = null;
        }
    }

    @Override
    public String toString() {
        return "BaseQuery{" + "page=" + page + ", size=" + size + ", sortField='" + sortField + '\'' + ", sortWay='" + sortWay + '\'' + ", createStart=" + createStart + ", createEnd=" + createEnd + '}';
    }

}
