package top.horsemuzi.system.handle.excel;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.pinyin.PinyinUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.exception.ExcelDataConvertException;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.CommonCodeUtil;
import top.horsemuzi.common.util.EncryptUtil;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.core.office.excel.listener.ExcelListener;
import top.horsemuzi.system.core.office.excel.listener.ExcelResult;
import top.horsemuzi.system.pojo.dto.role.RoleDTO;
import top.horsemuzi.system.pojo.dto.user.UserImportDTO;
import top.horsemuzi.system.pojo.entity.User;
import top.horsemuzi.system.pojo.entity.UserRole;
import top.horsemuzi.system.service.RoleService;
import top.horsemuzi.system.service.UserRoleService;
import top.horsemuzi.system.service.UserService;

import java.util.*;

/**
 * 用户信息Excel导入监听器
 *
 * @author mabin
 * @date 2022/09/23 13:40
 **/
@Slf4j
@Component
@RequiredArgsConstructor
public class ExcelUserImportListener extends AnalysisEventListener<UserImportDTO> implements ExcelListener<UserImportDTO> {

    /**
     * 临时存储数据集合 (单次导入缓存数量默认100)
     */
    private List<UserImportDTO> cacheUsers = new ArrayList<>(Constants.EXCEL_BATCH_COUNT);

    private Integer flag;
    private JwtUser jwtUser;

    public ExcelUserImportListener(Integer flag, JwtUser jwtUser) {
        this.flag = flag;
        this.jwtUser = jwtUser;
    }

    /**
     * 解析每条数据都进行调用
     *
     * @param userImportDTO
     * @param context
     */
    @Override
    public void invoke(UserImportDTO userImportDTO, AnalysisContext context) {
        cacheUsers.add(userImportDTO);
        if (cacheUsers.size() >= Constants.EXCEL_BATCH_COUNT) {
            importUsers();
            // 存储完成清理list缓存数据集合
            cacheUsers = new ArrayList<>(Constants.EXCEL_BATCH_COUNT);
        }
    }

    /**
     * 所有数据解析完成后,会来调用一次
     * 作用: 避免最后集合中小于 MAX_BATCH_COUNT 条的数据没有被保存
     *
     * @param context
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        if (CollectionUtil.isNotEmpty(cacheUsers)) {
            importUsers();
        }
    }

    /**
     * 在转换异常 获取其他异常下会调用本接口。我们如果捕捉并手动抛出异常则停止读取。如果这里不抛出异常则 继续读取下一行
     *
     * @param exception
     * @param context
     * @throws
     */
    @Override
    public void onException(Exception exception, AnalysisContext context) {
        //  如果是某一个单元格的转换异常 能获取到具体行号和数据
        if (exception instanceof ExcelDataConvertException) {
            ExcelDataConvertException convertException = (ExcelDataConvertException) exception;
            ResultCodeEnum codeEnum = ResultCodeEnum.FILE_DATA_FORMAT_ERROR;
            codeEnum.setMessage(StrUtil.format("用户信息导入, 第{}行, 第{}列的单元格数据[{}]解析异常", convertException.getRowIndex()
                    , convertException.getColumnIndex(), convertException.getCellData().getStringValue()));
            throw new BusinessException(codeEnum);
        }
    }

    @Override
    public ExcelResult<UserImportDTO> getExcelResult() {
        return new ExcelResult<UserImportDTO>() {

            @Override
            public String getAnalysis() {
                return null;
            }

            @Override
            public List<UserImportDTO> getList() {
                return null;
            }

            @Override
            public List<String> getErrorList() {
                return null;
            }
        };
    }

    /**
     * 批量保存导入数据
     * 具体逻辑:
     * 导入数据字段为: 用户名、性别、账号状态、手机号、邮箱(必须存在一个合法绑定账号, 并且不能和用户名相同)
     * 根据 用户名、手机号、邮箱 判断是否重复数据
     * 根据用户名查询用户是否已存在, 若已存在则根据条件判断是否进行更新, 更新用户密码不变,
     * 新导入用户密码默认为: 666888, 角色默认为: common(普通角色)
     * 保存用户信息、用户角色关联信息
     */
    @Transactional(rollbackFor = Exception.class)
    public void importUsers() {
        Set<String> usernameSet = new HashSet<>();
        Set<String> phoneSet = new HashSet<>();
        Set<String> emailSet = new HashSet<>();
        Map<String, UserImportDTO> userImportMap = new HashMap<>(16);
        // 校验文件数据格式是否正确
        for (UserImportDTO userImportDTO : cacheUsers) {
            String username = userImportDTO.getUsername();
            ValidatorUtil.allNotBlank(ResultCodeEnum.FILE_DATA_FORMAT_ERROR, username, userImportDTO.getSex(), userImportDTO.getStatus());

            // 手机号、邮箱(必须存在一个合法绑定账号)
            String phone = userImportDTO.getPhone();
            String email = userImportDTO.getEmail();
            ValidatorUtil.notTrue(StrUtil.isAllBlank(phone, email), ResultCodeEnum.BING_ACCOUNT_NOT_ALL_EMPTY_ERROR);
            if (StrUtil.isNotBlank(phone)) {
                ValidatorUtil.isTrue(Validator.isMobile(phone), ResultCodeEnum.PHONE_NOT_ILLEGALITY_ERROR);
                ValidatorUtil.isTrue(!StrUtil.equals(phone, username), ResultCodeEnum.USER_THREE_ACCOUNT_SAME_ERROR);
            }
            if (StrUtil.isNotBlank(email)) {
                ValidatorUtil.isTrue(Validator.isEmail(email), ResultCodeEnum.EMAIL_NOT_ILLEGALITY_ERROR);
                ValidatorUtil.isTrue(!StrUtil.equals(email, username), ResultCodeEnum.USER_THREE_ACCOUNT_SAME_ERROR);
            }
            // 组装数据
            usernameSet.add(username);
            phoneSet.add(phone);
            emailSet.add(email);
            userImportMap.put(username, userImportDTO);
        }
        // 保存导入用户信息
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(User::getUsername, usernameSet)
                .or().in(CollectionUtil.isNotEmpty(phoneSet), User::getPhone, phoneSet)
                .or().in(CollectionUtil.isNotEmpty(emailSet), User::getEmail, emailSet);
        List<User> existUsers = SpringUtil.getBean(UserService.class).list(queryWrapper);
        saveUsers(existUsers, userImportMap);
    }

    /**
     * 保存用户信息
     *
     * @param existUsers
     * @param userImportMap
     */
    private void saveUsers(List<User> existUsers, Map<String, UserImportDTO> userImportMap) {
        if (CollectionUtil.isNotEmpty(existUsers)) {
            List<User> updateUsers = new ArrayList<>();
            for (User user : existUsers) {
                UserImportDTO userImportDTO = userImportMap.get(user.getUsername());
                if (ObjectUtil.isNotNull(userImportDTO)) {
                    // 是否进行更新覆盖
                    if (Constants.COMMON_JUDGMENT.ONE.equals(flag)) {
                        user.setSex(userImportDTO.getSex());
                        user.setStatus(userImportDTO.getStatus());
                        user.setPhone(StrUtil.isNotBlank(userImportDTO.getPhone()) ? userImportDTO.getPhone() : user.getPhone());
                        user.setEmail(StrUtil.isNotBlank(userImportDTO.getEmail()) ? userImportDTO.getEmail() : user.getEmail());
                        updateUsers.add(user);
                    }
                    // 移除已存在用户
                    userImportMap.remove(user.getUsername());
                }
            }
            if (CollectionUtil.isNotEmpty(updateUsers)) {
                SpringUtil.getBean(UserService.class).updateBatchById(updateUsers);
            }
        }

        // 获取默认角色（common）信息
        RoleDTO queryRole = new RoleDTO();
        queryRole.setRoleCode(Constants.ROLE_CODE.COMMON);
        RoleDTO roleDTO = SpringUtil.getBean(RoleService.class).getRole(queryRole);
        ValidatorUtil.notEmpty(roleDTO, ResultCodeEnum.ROLE_NOT_EXIST);

        // 用户信息
        User newUser = null;
        List<User> saveUsers = new ArrayList<>();
        for (Map.Entry<String, UserImportDTO> entry : userImportMap.entrySet()) {
            newUser = OrikaUtil.convert(entry.getValue(), User.class);
            String salt = IdUtil.fastSimpleUUID();
            newUser.setSalt(salt)
                    .setPassword(EncryptUtil.pbkdf2("666888", salt))
                    .setRoleCodes(roleDTO.getRoleCode())
                    .setAvatar(CommonCodeUtil.generateImg(entry.getKey()))
                    .setRemark(StrUtil.format("手动导入用户({})", newUser.getUsername()));
            if (ObjectUtil.isNotNull(jwtUser)) {
                newUser.setCreateId(jwtUser.getId()).setCreateUser(jwtUser.getUsername());
            }
            // 设置用户名拼音、缩写字段，便于页面检索
            setPinyinByUsername(newUser);
            saveUsers.add(newUser);
        }
        if (CollectionUtil.isNotEmpty(saveUsers)) {
            ValidatorUtil.isTrue(SpringUtil.getBean(UserService.class).saveBatch(saveUsers), ResultCodeEnum.DATA_SAVE_ERROR);

            // 用户角色关联信息
            UserRole userRole = null;
            List<UserRole> saveUserRoles = new ArrayList<>();
            for (User user : saveUsers) {
                userRole = new UserRole();
                userRole.setUserId(user.getId());
                userRole.setRoleId(roleDTO.getId());
                saveUserRoles.add(userRole);
            }
            SpringUtil.getBean(UserRoleService.class).addBatchUserRole(saveUserRoles);
        }
    }

    /**
     * 根据用户名设置拼音相关信息
     *
     * @param user
     */
    private void setPinyinByUsername(User user) {
        String username = user.getUsername();
        if (StrUtil.isNotBlank(username)) {
            // 设置用户名拼音值 (汉字解析成拼音, 其他值直接拼接)
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < username.length(); i++) {
                char c = username.charAt(i);
                if (PinyinUtil.isChinese(c)) {
                    builder.append(PinyinUtil.getPinyin(c));
                } else {
                    builder.append(c);
                }
            }
            if (!StrUtil.equals(user.getPinyinUsername(), builder.toString())) {
                user.setPinyinUsername(builder.toString());
            }
            // 设置用户名拼音缩写值(全中文解析成缩写, 其他值不进行设置缩写)
            boolean isChinese = true;
            for (int i = 0; i < username.length(); i++) {
                char c = username.charAt(i);
                if (!PinyinUtil.isChinese(c)) {
                    isChinese = false;
                    break;
                }
            }
            if (isChinese) {
                String firstLetter = PinyinUtil.getFirstLetter(username, StrUtil.EMPTY);
                if (!StrUtil.equals(user.getPinyinAcronymUsername(), firstLetter)) {
                    user.setPinyinAcronymUsername(firstLetter);
                }
            }
        }
    }

}
