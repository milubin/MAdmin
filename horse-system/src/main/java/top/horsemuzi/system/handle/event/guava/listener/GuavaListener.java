package top.horsemuzi.system.handle.event.guava.listener;

import cn.hutool.core.util.StrUtil;
import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.Subscribe;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import top.horsemuzi.system.core.socket.WebSocketService;
import top.horsemuzi.system.handle.event.guava.event.NoticeSocketEvent;

import javax.annotation.PostConstruct;

/**
 * Guava的EventBus事件监听处理
 *
 * @author mabin
 * @date 2022/08/06 17:19
 **/
@Slf4j
@Component
@RequiredArgsConstructor
public class GuavaListener {

    private final AsyncEventBus asyncEventBus;
    private final WebSocketService webSocketService;

    /**
     * 初始化订阅者注册到EventBus
     */
    @PostConstruct
    public void init() {
        asyncEventBus.register(this);
        log.info("guava EventBus register success!");
    }

    /**
     * 通知公告异步推送客户端事件监听处理
     *
     * @param event
     */
    @Subscribe
    public void onEvent(NoticeSocketEvent event) {
        String content = event.getContent();
        log.info("【通知公告WebSocket推送客户端事件】推送信息内容 content={}", content);
        if (StrUtil.isNotBlank(content)) {
            webSocketService.sendNotice(content);
        }
    }

    /**
     * 监听没有配置订阅者的事件
     *
     * @param event
     */
    @Subscribe
    public void onEvent(DeadEvent event) {
        log.error("由 {} 发起的事件 {} 没有配置相应订阅者, 无法处理!", event.getSource().getClass().getSimpleName(),
                event.getEvent().getClass().getName());
    }

}
