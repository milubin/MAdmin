package top.horsemuzi.system.handle.event.guava.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * EventBus事件(通知公告异步推送客户端)
 *
 * @author mabin
 * @date 2022/08/06 17:18
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NoticeSocketEvent {

    /**
     * 推送消息内容
     */
    private String content;

}
