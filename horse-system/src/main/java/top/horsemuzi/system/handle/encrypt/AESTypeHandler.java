package top.horsemuzi.system.handle.encrypt;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.ObjectUtil;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.util.EncryptUtil;
import top.horsemuzi.system.config.properties.EncryptProperties;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 基于MyBatis-Plus自带的TypeHandler的AES字段加解密处理
 *
 * @author mabin
 * @date 2022/06/11 16:40
 **/
@Component
public class AESTypeHandler extends BaseTypeHandler {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, Object parameter, JdbcType jdbcType) throws SQLException {
        if (ObjectUtil.isNotNull(parameter)) {
            ps.setString(i, EncryptUtil.aesEncrypt(Convert.toStr(parameter), EncryptProperties.aesKey));
        }
    }

    @Override
    public Object getNullableResult(ResultSet rs, String columnName) throws SQLException {
        String columnValue = rs.getString(columnName);
        if (CharSequenceUtil.isNotBlank(columnValue)) {
            return EncryptUtil.aesDecrypt(columnValue, EncryptProperties.aesKey);
        }
        return columnValue;
    }

    @Override
    public Object getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        String columnValue = rs.getString(columnIndex);
        if (CharSequenceUtil.isNotBlank(columnValue)) {
            return EncryptUtil.aesDecrypt(columnValue, EncryptProperties.aesKey);
        }
        return columnValue;
    }

    @Override
    public Object getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        String columnValue = cs.getString(columnIndex);
        if (CharSequenceUtil.isNotBlank(columnValue)) {
            return EncryptUtil.aesDecrypt(columnValue, EncryptProperties.aesKey);
        }
        return columnValue;
    }
}
