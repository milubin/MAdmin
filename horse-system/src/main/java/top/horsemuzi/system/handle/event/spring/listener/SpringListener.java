package top.horsemuzi.system.handle.event.spring.listener;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.util.JsonUtil;
import top.horsemuzi.common.util.OrikaUtil;
import top.horsemuzi.system.core.redis.service.RedisService;
import top.horsemuzi.system.handle.event.spring.event.AsyncEvent;
import top.horsemuzi.system.pojo.dto.log.LoginLogDTO;
import top.horsemuzi.system.pojo.dto.log.SystemLogDTO;
import top.horsemuzi.system.pojo.dto.open.api.ApiDTO;
import top.horsemuzi.system.pojo.dto.open.app.AppDTO;
import top.horsemuzi.system.pojo.dto.open.log.ApiLogDTO;
import top.horsemuzi.system.pojo.dto.schedule.quartz.QuartzJobLogDTO;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskDTO;
import top.horsemuzi.system.pojo.dto.schedule.task.TaskLogDTO;
import top.horsemuzi.system.pojo.entity.QuartzJobLog;
import top.horsemuzi.system.service.*;

/**
 * 事件监听器
 *
 * @author mabin
 * @date 2022/07/01 11:32
 **/
@Slf4j
@RequiredArgsConstructor
@Component
public class SpringListener {
    private final SystemLogService logService;
    private final QuartzJobLogService quartzJobLogService;
    private final TaskLogService taskLogService;
    private final TaskService taskService;
    private final LoginLogService loginLogService;
    private final RedisService redisService;
    private final AppService appService;
    private final ApiService apiService;
    private final ApiLogService apiLogService;

    @Async("async")
    @EventListener
    public void logListener(AsyncEvent event) {
        Object source = event.getSource();
        if (ObjectUtil.isNotNull(source)) {
            if (source instanceof SystemLogDTO) {
                // 系统操作日志
                SystemLogDTO systemLogDTO = (SystemLogDTO) source;
                systemLogDTO.setThreadName(generateFullThreadName(systemLogDTO.getThreadName()));
                logService.saveLog(systemLogDTO);
            } else if (source instanceof LoginLogDTO) {
                // 登录日志
                LoginLogDTO loginLogDTO = (LoginLogDTO) source;
                loginLogDTO.setThreadName(generateFullThreadName(loginLogDTO.getThreadName()));
                loginLogService.saveLoginLog(loginLogDTO);
            } else if (source instanceof QuartzJobLogDTO) {
                // Quartz调度任务执行日志
                QuartzJobLogDTO quartzJobLogDTO = (QuartzJobLogDTO) source;
                quartzJobLogDTO.setThreadName(generateFullThreadName(quartzJobLogDTO.getThreadName()));
                quartzJobLogService.save(OrikaUtil.convert(quartzJobLogDTO, QuartzJobLog.class));
            } else if (source instanceof TaskLogDTO) {
                // Spring调度任务执行日志（根据 className, methodName, methodParams 可以确定一个唯一任务）
                TaskLogDTO taskLogDTO = (TaskLogDTO) source;
                taskLogDTO.setThreadName(generateFullThreadName(taskLogDTO.getThreadName()));
                TaskDTO query = new TaskDTO();
                query.setClassName(taskLogDTO.getClassName());
                query.setMethodName(taskLogDTO.getMethodName());
                query.setMethodParams(taskLogDTO.getMethodParams());
                TaskDTO taskDTO = taskService.getTask(query);
                if (ObjectUtil.isNotNull(taskDTO)) {
                    taskLogDTO.setTaskId(taskDTO.getId());
                }
                taskLogService.addTaskLog(taskLogDTO);
            } else if (source instanceof ApiLogDTO) {
                // API请求日志
                ApiLogDTO apiLogDTO = (ApiLogDTO) source;
                // 获取Api应用信息
                String cacheAppKey = Constants.REDIS_PREFIX.OPEN_APP + StrUtil.COLON + apiLogDTO.getAppKey();
                String appJson = redisService.get(cacheAppKey);
                if (StrUtil.isNotBlank(appJson)) {
                    AppDTO appDTO = JsonUtil.fromJson(appJson, AppDTO.class);
                    if (ObjectUtil.isNotNull(appDTO)) {
                        apiLogDTO.setAppId(appDTO.getId());
                        apiLogDTO.setAppName(appDTO.getAppName());
                    }
                } else {
                    AppDTO currentAppDTO = appService.getApp(apiLogDTO.getAppKey());
                    if (ObjectUtil.isNotNull(currentAppDTO)) {
                        apiLogDTO.setAppId(currentAppDTO.getId());
                        apiLogDTO.setAppName(currentAppDTO.getAppName());
                        // 重新放入缓存(一般不会存在记录日志时获取不到缓存信息的情况)
                        redisService.set(cacheAppKey, JsonUtil.toJson(currentAppDTO));
                    }
                }
                // 获取Api接口信息
                String cacheApiKey = Constants.REDIS_PREFIX.OPEN_API + StrUtil.COLON + apiLogDTO.getApiCode();
                String apiJson = redisService.get(cacheApiKey);
                if (StrUtil.isNotBlank(apiJson)) {
                    ApiDTO apiDTO = JsonUtil.fromJson(apiJson, ApiDTO.class);
                    if (ObjectUtil.isNotNull(apiDTO)) {
                        apiLogDTO.setApiFunction(apiDTO.getApiFunction());
                    }
                } else {
                    ApiDTO currentApiDTO = apiService.getApi(apiLogDTO.getApiCode());
                    if (ObjectUtil.isNotNull(currentApiDTO)) {
                        apiLogDTO.setApiFunction(currentApiDTO.getApiFunction());
                        redisService.set(cacheApiKey, JsonUtil.toJson(currentApiDTO));
                    }
                }
                // 保存API请求日志
                apiLogService.addApiLog(apiLogDTO);
            }
        } else {
            log.error("The content of the system monitoring message is abnormal");
        }
    }

    /**
     * 组装全线程名称信息
     *
     * @param threadName
     * @return
     */
    private String generateFullThreadName(String threadName) {
        return StrUtil.concat(true, threadName, "|", Thread.currentThread().getName());
    }

}
