package top.horsemuzi.system.handle.encrypt;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.ObjectUtil;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Component;
import top.horsemuzi.common.util.EncryptUtil;
import top.horsemuzi.system.config.properties.EncryptProperties;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 基于MP的SM4字段自动加解密处理
 *
 * @author 马滨
 * @date 2023/05/01 16:03
 **/
@Component
public class SM4TypeHandler extends BaseTypeHandler {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, Object parameter, JdbcType jdbcType) throws SQLException {
        if (ObjectUtil.isNotNull(parameter)) {
            ps.setString(i, EncryptUtil.sm4Encrypt(EncryptProperties.sm4Key, Convert.toStr(parameter)));
        }
    }

    @Override
    public Object getNullableResult(ResultSet rs, String columnName) throws SQLException {
        String columnValue = rs.getString(columnName);
        if (CharSequenceUtil.isNotBlank(columnValue)) {
            return EncryptUtil.sm4Decrypt(EncryptProperties.sm4Key, columnValue);
        }
        return columnValue;
    }

    @Override
    public Object getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        String columnValue = rs.getString(columnIndex);
        if (CharSequenceUtil.isNotBlank(columnValue)) {
            return EncryptUtil.sm4Decrypt(EncryptProperties.sm4Key, columnValue);
        }
        return columnValue;
    }

    @Override
    public Object getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        String columnValue = cs.getString(columnIndex);
        if (CharSequenceUtil.isNotBlank(columnValue)) {
            return EncryptUtil.sm4Decrypt(EncryptProperties.sm4Key, columnValue);
        }
        return columnValue;
    }

}
