package top.horsemuzi.system.handle.init;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import top.horsemuzi.system.core.redis.delay.init.DelayQueueConsumeInit;

/**
 * 延时队列启动初始化
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/05/13 10:25
 **/
@Slf4j
@Order(2)
@Component
@RequiredArgsConstructor
public class DelayQueueInit implements CommandLineRunner {

    private final DelayQueueConsumeInit delayQueueConsumeInit;

    @Override
    public void run(String... args) throws Exception {
        try {
            delayQueueConsumeInit.init();
        } catch (Exception e) {
            log.error("delay queue start init exception", e);
        }
    }

}
