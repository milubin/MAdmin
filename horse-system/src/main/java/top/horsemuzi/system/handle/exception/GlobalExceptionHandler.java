package top.horsemuzi.system.handle.exception;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.i18n.I18nMessageFactory;
import top.horsemuzi.common.result.Result;
import top.horsemuzi.common.util.ExceptionUtil;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.stream.Collectors;

/**
 * 全局统一异常处理
 *
 * @author mabin
 * @date 2022/05/06 15:24
 **/
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 全局异常处理
     * 兼容 Spring Validation 参数校验异常的国际化消息响应处理（ConstraintViolationException，MethodArgumentNotValidException，BindException）
     * 兼容 自定义异常 BusinessException 的国际化消息响应处理（BusinessException）
     *
     * @param e
     * @param <T>
     * @return
     */
    @ExceptionHandler(value = {BadSqlGrammarException.class, HttpMessageNotReadableException.class, MethodArgumentNotValidException.class,
            MissingServletRequestParameterException.class, HttpMediaTypeNotSupportedException.class, HttpMediaTypeNotAcceptableException.class,
            ServletRequestBindingException.class, ConversionNotSupportedException.class, TypeMismatchException.class, NoHandlerFoundException.class,
            ConstraintViolationException.class, BindException.class, HttpRequestMethodNotSupportedException.class, DuplicateKeyException.class,
            BusinessException.class, RuntimeException.class, Exception.class})
    public <T> Result<T> error(Exception e) {
        log.error(ExceptionUtil.getMessage(e));
        if (e instanceof BadSqlGrammarException || e instanceof DuplicateKeyException) {
            return Result.error(ResultCodeEnum.BAD_SQL_GRAMMAR.getCode(), getI18nMessage(ResultCodeEnum.BAD_SQL_GRAMMAR.name()));
        } else if (e instanceof HttpMessageNotReadableException) {
            return Result.error(ResultCodeEnum.JSON_PARSE_ERROR.getCode(), getI18nMessage(ResultCodeEnum.JSON_PARSE_ERROR.name()));
        } else if (e instanceof ServletRequestBindingException || e instanceof TypeMismatchException) {
            return Result.error(ResultCodeEnum.PARAM_VERIFY_ERROR.getCode(), getI18nMessage(ResultCodeEnum.PARAM_VERIFY_ERROR.name()));
        } else if (e instanceof HttpRequestMethodNotSupportedException) {
            return Result.error(ResultCodeEnum.METHOD_NOT_SUPPORTED.getCode(), getI18nMessage(ResultCodeEnum.METHOD_NOT_SUPPORTED.name()));
        } else if (e instanceof HttpMediaTypeNotSupportedException) {
            return Result.error(ResultCodeEnum.UNSUPPORTED_MEDIA_TYPE.getCode(), getI18nMessage(ResultCodeEnum.UNSUPPORTED_MEDIA_TYPE.name()));
        } else if (e instanceof HttpMediaTypeNotAcceptableException) {
            return Result.error(ResultCodeEnum.NOT_ACCEPTABLE.getCode(), getI18nMessage(ResultCodeEnum.NOT_ACCEPTABLE.name()));
        } else if (e instanceof NoHandlerFoundException) {
            return Result.error(ResultCodeEnum.NOT_FOUND.getCode(), getI18nMessage(ResultCodeEnum.NOT_FOUND.name()));
        } else if (e instanceof ConstraintViolationException) {
            String message = ((ConstraintViolationException) e).getConstraintViolations().stream()
                    .map(ConstraintViolation::getMessage).collect(Collectors.joining(StrUtil.COMMA));
            return Result.error(ResultCodeEnum.PARAM_VERIFY_ERROR.getCode(), message);
        } else if (e instanceof MethodArgumentNotValidException) {
            String message = ((MethodArgumentNotValidException) e).getBindingResult().getFieldError().getDefaultMessage();
            return Result.error(ResultCodeEnum.PARAM_VERIFY_ERROR.getCode(), message);
        } else if (e instanceof BindException) {
            String message = ((BindException) e).getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining(StrUtil.COMMA));
            return Result.error(ResultCodeEnum.PARAM_VERIFY_ERROR.getCode(), message);
        } else if (e instanceof BusinessException) {
            BusinessException businessException = (BusinessException) e;
            ResultCodeEnum codeEnum = businessException.getCodeEnum();
            return Result.error(businessException.getCode(), ObjectUtil.isNotNull(codeEnum) ? getI18nMessage(codeEnum.name()) : businessException.getMessage());
        } else {
            return Result.error();
        }
    }

    /**
     * 获取国际化消息
     *
     * @param codeEnumName
     * @return java.lang.String
     * @author mabin
     * @date 2023/4/8 14:58
     **/
    private String getI18nMessage(String codeEnumName) {
        return I18nMessageFactory.message(StrUtil.replace(codeEnumName, StrUtil.UNDERLINE, StrUtil.DOT).toLowerCase());
    }

}
