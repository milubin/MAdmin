package top.horsemuzi.system.handle.init;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import top.horsemuzi.system.service.ScheduleService;

/**
 * Spring调度任务服务启动初始化
 *
 * @author mabin
 * @date 2022/09/08 20:27
 **/
@Slf4j
@Order(1)
@Component
@RequiredArgsConstructor
public class TaskRunInit implements CommandLineRunner {

    private final ScheduleService scheduledService;

    @Override
    public void run(String... args) {
        try {
            scheduledService.asyncInit();
        } catch (Exception e) {
            log.error("Spring scheduling task asynchronous initialization exception", e);
        }
    }

}
