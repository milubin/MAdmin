package top.horsemuzi.system.handle.excel;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.exception.ExcelDataConvertException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;
import top.horsemuzi.common.util.ValidatorUtil;
import top.horsemuzi.system.core.office.excel.listener.ExcelListener;
import top.horsemuzi.system.core.office.excel.listener.ExcelResult;
import top.horsemuzi.system.pojo.dto.open.api.ApiDTO;
import top.horsemuzi.system.pojo.dto.open.api.ApiImportDTO;
import top.horsemuzi.system.pojo.entity.ApiCategory;
import top.horsemuzi.system.service.ApiCategoryService;
import top.horsemuzi.system.service.ApiService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * API接口批量导入excel监听器
 *
 * @author mabin
 * @date 2022/11/29 10:00
 **/
@Slf4j
@Component
@RequiredArgsConstructor
public class ExcelApiImportListener extends AnalysisEventListener<ApiImportDTO> implements ExcelListener<ApiImportDTO> {

    /**
     * 临时存储数据集合 (单次导入缓存数量默认100)
     */
    private List<ApiImportDTO> cacheList = new ArrayList<>(Constants.EXCEL_BATCH_COUNT);

    private JwtUser jwtUser;

    public ExcelApiImportListener(JwtUser jwtUser) {
        this.jwtUser = jwtUser;
    }

    /**
     * 解析每条数据都进行调用
     *
     * @param apiImportDTO
     * @param context
     */
    @Override
    public void invoke(ApiImportDTO apiImportDTO, AnalysisContext context) {
        cacheList.add(apiImportDTO);
        if (cacheList.size() >= Constants.EXCEL_BATCH_COUNT) {
            importApis();
            cacheList = new ArrayList<>(Constants.EXCEL_BATCH_COUNT);
        }
    }

    /**
     * 所有数据解析完成后,会来调用一次
     * 作用: 避免最后集合中小于 MAX_BATCH_COUNT 条的数据没有被保存
     *
     * @param context
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        if (CollectionUtil.isNotEmpty(cacheList)) {
            importApis();
        }
    }

    /**
     * 在转换异常 获取其他异常下会调用本接口。我们如果捕捉并手动抛出异常则停止读取。如果这里不抛出异常则 继续读取下一行
     *
     * @param exception
     * @param context
     * @throws
     */
    @Override
    public void onException(Exception exception, AnalysisContext context) {
        //  如果是某一个单元格的转换异常 能获取到具体行号和数据
        if (exception instanceof ExcelDataConvertException) {
            ExcelDataConvertException convertException = (ExcelDataConvertException) exception;
            ResultCodeEnum codeEnum = ResultCodeEnum.FILE_DATA_FORMAT_ERROR;
            codeEnum.setMessage(StrUtil.format("Excel信息导入, 第{}行, 第{}列的单元格数据[{}]解析异常", convertException.getRowIndex()
                    , convertException.getColumnIndex(), convertException.getCellData().getStringValue()));
            throw new BusinessException(codeEnum);
        }
    }

    /**
     * 批量保存数据
     */
    @Transactional(rollbackFor = Exception.class)
    public void importApis() {
        ApiCategoryService apiCategoryService = SpringUtil.getBean(ApiCategoryService.class);
        ApiService apiService = SpringUtil.getBean(ApiService.class);

        // 获取api接口类型信息
        List<ApiCategory> apiCategoryList = apiCategoryService.list();
        Map<String, Long> apiCategoryMap = new HashMap<>(16);
        if (CollectionUtil.isNotEmpty(apiCategoryList)) {
            apiCategoryMap = apiCategoryList.stream().collect(Collectors.toMap(ApiCategory::getCategoryName, ApiCategory::getId, (v1, v2) -> v1));
        }
        List<ApiDTO> apiDTOList = new ArrayList<>();
        ApiDTO apiDTO = null;
        for (ApiImportDTO apiImportDTO : cacheList) {
            apiDTO = new ApiDTO();
            Long apiCategoryId = apiCategoryMap.get(apiImportDTO.getCategoryName());
            ValidatorUtil.notEmpty(apiCategoryId, ResultCodeEnum.OPEN_API_CATEGORY_NOT_EXIST_ERROR);
            apiDTO.setApiCategoryId(apiCategoryId);
            apiDTO.setApiCode(apiImportDTO.getApiCode());
            apiDTO.setApiFunction(apiImportDTO.getApiFunction());
            apiDTO.setApiClass(apiImportDTO.getApiClass());
            apiDTO.setApiMethod(apiImportDTO.getApiMethod());
            ValidatorUtil.isTrue(apiService.checkApiRepeat(apiDTO), ResultCodeEnum.OPEN_API_NAME_REPEAT_ERROR);
            apiDTO.setApiDescription(apiImportDTO.getApiDescription());
            apiDTO.setApiVersion("v1");
            apiDTO.setApiStatus(apiImportDTO.getApiStatus());
            apiDTO.setRemark(apiImportDTO.getRemark());
            if (ObjectUtil.isNotNull(jwtUser)) {
                apiDTO.setCreateId(jwtUser.getId());
                apiDTO.setCreateUser(jwtUser.getUsername());
            }
            if (ObjectUtil.isNotNull(apiImportDTO.getDailyRequestLimit())) {
                apiDTO.setDailyRequestLimit(apiImportDTO.getDailyRequestLimit());
            }
            apiDTOList.add(apiDTO);
        }
        // 保存入库
        apiService.addApi(apiDTOList);
    }

    /**
     * 获取导入数据结果实例
     *
     * @return
     */
    @Override
    public ExcelResult<ApiImportDTO> getExcelResult() {
        return new ExcelResult<ApiImportDTO>() {

            @Override
            public String getAnalysis() {
                return null;
            }

            @Override
            public List<ApiImportDTO> getList() {
                return null;
            }

            @Override
            public List<String> getErrorList() {
                return null;
            }
        };
    }
}
