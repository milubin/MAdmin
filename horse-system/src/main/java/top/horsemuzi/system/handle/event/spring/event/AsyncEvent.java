package top.horsemuzi.system.handle.event.spring.event;

import org.springframework.context.ApplicationEvent;

import java.io.Serializable;

/**
 * 系统日志记录事件
 *
 * @author mabin
 * @date 2022/07/01 11:28
 **/
public class AsyncEvent extends ApplicationEvent implements Serializable {

    private static final long serialVersionUID = -429597911784780636L;

    public AsyncEvent(Object object) {
        super(object);
    }

}
