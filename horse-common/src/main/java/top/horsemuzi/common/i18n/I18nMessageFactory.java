package top.horsemuzi.common.i18n;

import cn.hutool.extra.spring.SpringUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Objects;

/**
 * I18n服务实例工厂
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/04/08 10:59
 **/
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class I18nMessageFactory {

    private static final MessageSource MESSAGE_SOURCE = SpringUtil.getBean(MessageSource.class);

    /**
     * 获取Spring MessageSource实例
     *
     * @param
     * @return org.springframework.context.MessageSource
     * @author mabin
     * @date 2023/4/8 11:11
     **/
    public static MessageSource instance() {
        return MESSAGE_SOURCE;
    }

    /**
     * 根据消息键和参数获取消息值（委托给Spring MessageSource）
     *
     * @param code
     * @param args
     * @return java.lang.String
     * @author mabin
     * @date 2023/4/8 11:11
     **/
    public static String message(String code, Objects... args) {
        return MESSAGE_SOURCE.getMessage(code, args, LocaleContextHolder.getLocale());
    }

}
