package top.horsemuzi.common.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.util.ExceptionUtil;

/**
 * @author mabin
 * @date 2020/8/2 15:19
 * 自定义业务异常
 */
@Slf4j
@Getter
@Setter
@NoArgsConstructor
public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = 7683466324584252019L;

    /**
     * 状态码
     */
    private Integer code;

    /**
     * 结果枚举（用于自定义异常消息国际化处理）
     */
    private ResultCodeEnum codeEnum = null;

    /**
     * 接收异常状态码、消息
     *
     * @param code
     * @param message
     */
    public BusinessException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    /**
     * 接收异常实例、状态码、消息
     *
     * @param e
     * @param code
     * @param message
     */
    public BusinessException(Exception e, Integer code, String message) {
        super(message);
        log.error("{} exception info: {}", message, ExceptionUtil.getMessage(e));
        this.code = code;
    }

    /**
     * 接收枚举类型
     *
     * @param codeEnum
     */
    public BusinessException(ResultCodeEnum codeEnum) {
        // 调用父类的构造函数,且必须放在第一行的位置
        super(codeEnum.getMessage());
        this.code = codeEnum.getCode();
        this.codeEnum = codeEnum;
    }

    /**
     * 接收异常实例、枚举
     *
     * @param e
     * @param codeEnum
     */
    public BusinessException(Exception e, ResultCodeEnum codeEnum) {
        super(codeEnum.getMessage());
        log.error("{} exception info: {}", codeEnum.getMessage(), ExceptionUtil.getMessage(e));
        this.code = codeEnum.getCode();
        this.codeEnum = codeEnum;
    }

}
