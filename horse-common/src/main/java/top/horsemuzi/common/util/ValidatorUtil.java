package top.horsemuzi.common.util;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Map;

/**
 * 数据校验相关处理
 *
 * @author mabin
 * @date 2022/05/11 09:42
 **/
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ValidatorUtil {

    public static void exception() {
        throw new RuntimeException();
    }

    public static void exception(ResultCodeEnum codeEnum) {
        throw new BusinessException(codeEnum);
    }

    public static void isTrue(boolean expression) {
        if (!expression) {
            throw new RuntimeException();
        }
    }

    public static void isTrue(boolean expression, RuntimeException e) {
        if (!expression) {
            throw e;
        }
    }

    public static void isTrue(boolean expression, ResultCodeEnum codeEnum) {
        if (!expression) {
            throw new BusinessException(codeEnum);
        }
    }

    public static void notTrue(boolean expression) {
        if (expression) {
            throw new BusinessException(ResultCodeEnum.PARAM_VERIFY_ERROR);
        }
    }

    public static void notTrue(boolean expression, ResultCodeEnum codeEnum) {
        if (expression) {
            throw new BusinessException(codeEnum);
        }
    }

    public static void isInstanceOf(@NotNull Class<?> type, Object object) {
        if (!type.isInstance(object)) {
            throw new BusinessException(ResultCodeEnum.PARAM_VERIFY_ERROR);
        }
    }

    public static void isInstanceOf(@NotNull Class<?> type, Object object, ResultCodeEnum codeEnum) {
        if (!type.isInstance(object)) {
            throw new BusinessException(codeEnum);
        }
    }

    public static void equals(String str1, String str2, ResultCodeEnum codeEnum) {
        equals(str1, str2, false, codeEnum);
    }

    public static void equals(String str1, String str2, boolean ignoreCase, ResultCodeEnum codeEnum) {
        if (ignoreCase ? !StrUtil.equalsIgnoreCase(str1, str2) : !StrUtil.equals(str1, str2)) {
            throw new BusinessException(codeEnum);
        }
    }

    public static void notEquals(String str1, String str2, ResultCodeEnum codeEnum) {
        if (StrUtil.equals(str1, str2)) {
            throw new BusinessException(codeEnum);
        }
    }

    public static void equals(Object obj1, Object obj2, ResultCodeEnum codeEnum) {
        if (ObjectUtil.notEqual(obj1, obj2)) {
            throw new BusinessException(codeEnum);
        }
    }

    public static void notEquals(Object obj1, Object obj2, ResultCodeEnum codeEnum) {
        if (ObjectUtil.equals(obj1, obj2)) {
            throw new BusinessException(codeEnum);
        }
    }

    public static <T> void isEmpty(T[] arrays) {
        if (ArrayUtil.isNotEmpty(arrays)) {
            throw new BusinessException(ResultCodeEnum.PARAM_VERIFY_ERROR);
        }
    }

    public static <T> void isEmpty(T[] arrays, ResultCodeEnum codeEnum) {
        if (ArrayUtil.isNotEmpty(arrays)) {
            throw new BusinessException(codeEnum);
        }
    }

    public static void isEmpty(Map<?, ?> map) {
        if (MapUtil.isNotEmpty(map)) {
            throw new BusinessException(ResultCodeEnum.PARAM_VERIFY_ERROR);
        }
    }

    public static void isEmpty(Map<?, ?> map, ResultCodeEnum codeEnum) {
        if (MapUtil.isNotEmpty(map)) {
            throw new BusinessException(codeEnum);
        }
    }

    public static void isEmpty(Object param) {
        if (ObjectUtil.isNotEmpty(param)) {
            throw new BusinessException(ResultCodeEnum.PARAM_VERIFY_ERROR);
        }
    }

    public static void isEmpty(Object param, ResultCodeEnum codeEnum) {
        if (ObjectUtil.isNotEmpty(param)) {
            throw new BusinessException(codeEnum);
        }
    }

    public static void isEmpty(Collection<?> collection) {
        if (CollectionUtil.isNotEmpty(collection)) {
            throw new BusinessException(ResultCodeEnum.PARAM_VERIFY_ERROR);
        }
    }

    public static void isEmpty(Collection<?> collection, ResultCodeEnum codeEnum) {
        if (CollectionUtil.isNotEmpty(collection)) {
            throw new BusinessException(codeEnum);
        }
    }

    public static <T> void notEmpty(T[] arrays) {
        if (ObjectUtil.isNull(arrays) || ArrayUtil.isEmpty(arrays)) {
            throw new BusinessException(ResultCodeEnum.PARAM_VERIFY_ERROR);
        }
    }

    public static <T> void notEmpty(T[] arrays, ResultCodeEnum codeEnum) {
        if (ObjectUtil.isNull(arrays) || ArrayUtil.isEmpty(arrays)) {
            throw new BusinessException(codeEnum);
        }
    }

    public static void notEmpty(Map<?, ?> map) {
        if (ObjectUtil.isNull(map) || MapUtil.isEmpty(map)) {
            throw new BusinessException(ResultCodeEnum.PARAM_VERIFY_ERROR);
        }
    }

    public static void notEmpty(Map<?, ?> map, ResultCodeEnum codeEnum) {
        if (ObjectUtil.isNull(map) || MapUtil.isEmpty(map)) {
            throw new BusinessException(codeEnum);
        }
    }

    public static void notEmpty(Object param) {
        if (ObjectUtil.isNull(param) || ObjectUtil.isEmpty(param)) {
            throw new BusinessException(ResultCodeEnum.PARAM_VERIFY_ERROR);
        }
    }

    public static void notEmpty(Collection<?> collection) {
        if (ObjectUtil.isNull(collection) || CollectionUtil.isEmpty(collection)) {
            throw new BusinessException(ResultCodeEnum.PARAM_VERIFY_ERROR);
        }
    }

    public static void notEmpty(Collection<?> collection, ResultCodeEnum codeEnum) {
        if (ObjectUtil.isNull(collection) || CollectionUtil.isEmpty(collection)) {
            throw new BusinessException(codeEnum);
        }
    }

    public static void notEmpty(Object param, ResultCodeEnum codeEnum) {
        if (ObjectUtil.isNull(param) || ObjectUtil.isEmpty(param)) {
            throw new BusinessException(codeEnum);
        }
    }

    public static void allNotEmpty(Object... params) {
        if (ObjectUtil.hasEmpty(params)) {
            throw new BusinessException(ResultCodeEnum.PARAM_VERIFY_ERROR);
        }
    }

    public static void allNotEmpty(ResultCodeEnum codeEnum, Object... params) {
        if (ObjectUtil.hasEmpty(params)) {
            throw new BusinessException(codeEnum);
        }
    }

    public static void isBlank(String param) {
        if (StrUtil.isNotBlank(param)) {
            throw new BusinessException(ResultCodeEnum.PARAM_VERIFY_ERROR);
        }
    }

    public static void isBlank(String param, ResultCodeEnum codeEnum) {
        if (StrUtil.isNotBlank(param)) {
            throw new BusinessException(codeEnum);
        }
    }

    public static void notBlank(String param) {
        if (StrUtil.isBlank(param)) {
            throw new BusinessException(ResultCodeEnum.PARAM_VERIFY_ERROR);
        }
    }

    public static void notBlank(String param, ResultCodeEnum codeEnum) {
        if (StrUtil.isBlank(param)) {
            throw new BusinessException(codeEnum);
        }
    }

    public static void allNotBlank(String... params) {
        if (StrUtil.hasBlank(params)) {
            throw new BusinessException(ResultCodeEnum.PARAM_VERIFY_ERROR);
        }
    }

    public static void allNotBlank(ResultCodeEnum codeEnum, String... params) {
        if (StrUtil.hasBlank(params)) {
            throw new BusinessException(codeEnum);
        }
    }

}
