package top.horsemuzi.common.util;

import cn.hutool.captcha.*;
import cn.hutool.captcha.generator.MathGenerator;
import cn.hutool.captcha.generator.RandomGenerator;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpressionParser;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description 基于HuTool的验证码处理工具
 * @date 2022/5/8 18:45
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class KaptchaUtil {

    private static final Integer CODE_COUNT = 4;
    private static final Integer WIDTH = 160;
    private static final Integer HEIGHT = 60;
    private static final Integer INTERFERENCE_COUNT = 2;
    private static final Integer NUMBER_LENGTH = 1;
    private static final String BASE_STR = RandomUtil.BASE_NUMBER;


    /**
     * 生成器类型
     */
    public static class GeneratorType {
        // 纯数字验证码
        public static final String RANDOM = "RANDOM";
        // 四则运算验证码
        public static final String MATH = "MATH";
    }

    /**
     * 验证码类型
     */
    public static class KaptchaType {
        public static final String LINE = "LINE";
        public static final String CIRCLE = "CIRCLE";
        public static final String SHEAR = "SHEAR";
        public static final String GIF = "GIF";
    }

    /**
     * 线段干扰线验证码
     *
     * @param generatorType 自定义验证码类型
     * @return
     */
    public static LineCaptcha createLineCaptcha(String generatorType) {
        // 默认字母+随机数字
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(WIDTH, HEIGHT, CODE_COUNT, INTERFERENCE_COUNT);
        if (StrUtil.equals(generatorType, GeneratorType.RANDOM)) {
            // 纯数字验证码
            lineCaptcha.setGenerator(new RandomGenerator(BASE_STR, CODE_COUNT));
        } else if (StrUtil.equals(generatorType, GeneratorType.MATH)) {
            // 四则运算验证码
            lineCaptcha.setGenerator(new MathGenerator(NUMBER_LENGTH));
        }
        lineCaptcha.createCode();
        return lineCaptcha;
    }

    /**
     * 圆圈干扰验证码
     *
     * @param generatorType 自定义验证码类型
     * @return
     */
    public static CircleCaptcha createCircleCaptcha(String generatorType) {
        CircleCaptcha circleCaptcha = CaptchaUtil.createCircleCaptcha(WIDTH, HEIGHT, CODE_COUNT, INTERFERENCE_COUNT);
        if (StrUtil.equals(generatorType, GeneratorType.RANDOM)) {
            circleCaptcha.setGenerator(new RandomGenerator(BASE_STR, CODE_COUNT));
        } else if (StrUtil.equals(generatorType, GeneratorType.MATH)) {
            circleCaptcha.setGenerator(new MathGenerator(NUMBER_LENGTH));
        }
        circleCaptcha.createCode();
        return circleCaptcha;
    }

    /**
     * 扭曲干扰验证码
     *
     * @param generatorType 自定义验证码类型
     * @return
     */
    public static ShearCaptcha createShearCaptcha(String generatorType) {
        ShearCaptcha shearCaptcha = CaptchaUtil.createShearCaptcha(WIDTH, HEIGHT, CODE_COUNT, INTERFERENCE_COUNT);
        if (StrUtil.equals(generatorType, GeneratorType.RANDOM)) {
            shearCaptcha.setGenerator(new RandomGenerator(BASE_STR, CODE_COUNT));
        } else if (StrUtil.equals(generatorType, GeneratorType.MATH)) {
            shearCaptcha.setGenerator(new MathGenerator(NUMBER_LENGTH));
        }
        shearCaptcha.createCode();
        return shearCaptcha;
    }

    /**
     * Gif验证码
     *
     * @param generatorType 自定义验证码类型
     * @return
     */
    public static GifCaptcha createGifCaptcha(String generatorType) {
        GifCaptcha gifCaptcha = CaptchaUtil.createGifCaptcha(WIDTH, HEIGHT, CODE_COUNT);
        if (StrUtil.equals(generatorType, GeneratorType.RANDOM)) {
            gifCaptcha.setGenerator(new RandomGenerator(BASE_STR, CODE_COUNT));
        } else if (StrUtil.equals(generatorType, GeneratorType.MATH)) {
            gifCaptcha.setGenerator(new MathGenerator(NUMBER_LENGTH));
        }
        gifCaptcha.createCode();
        return gifCaptcha;
    }

    /**
     * 获取四则运算的结果(SPEL引擎自动计算)
     *
     * @param code 四则运算验证码
     * @return
     */
    public static String getKaptchaMathResult(String code) {
        if (StrUtil.isBlank(code)) {
            return null;
        }
        SpelExpressionParser parser = new SpelExpressionParser();
        Expression expression = parser.parseExpression(StrUtil.removeSuffix(code, "="));
        return expression.getValue(String.class);
    }

}
