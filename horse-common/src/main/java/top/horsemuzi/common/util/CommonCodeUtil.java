package top.horsemuzi.common.util;

import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.pinyin.PinyinUtil;
import cn.hutool.http.Header;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 系统公共代码片段处理（零碎代码片段，部分仅适用于本系统）
 *
 * @author mabin
 * @date 2022/09/23 17:14
 **/

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CommonCodeUtil {

    /**
     * 绘制字体头像
     * 如果是英文名，只显示首字母大写，
     * 如果是中文名，只显示最后两个字
     * 返回图片base64
     *
     * @param name
     * @return java.lang.String
     * @author mabin
     * @date 2023/3/21 16:18
     **/
    public static String generateImg(String name) {
        int width = 100;
        int height = 100;
        int nameLength = name.length();
        String nameWritten;
        // 如果用户输入的姓名少于等于2个字符，不用截取
        if (nameLength <= 2) {
            nameWritten = name;
        } else {
            // 如果用户输入的姓名大于等于3个字符，截取后面两位
            String first = StrUtil.sub(name, 0, 1);
            if (isChinese(first)) {
                // 截取倒数两位汉字
                nameWritten = name.substring(nameLength - 2);
            } else {
                // 截取前面的两个英文字母
                nameWritten = StrUtil.sub(name, 0, 1).toUpperCase();
            }
        }
        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = (Graphics2D) bufferedImage.getGraphics();
        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2.setBackground(getRandomColor());
        g2.clearRect(0, 0, width, height);
        g2.setPaint(Color.WHITE);
        Font font;
        // 两个字及以上
        if (nameWritten.length() >= 2) {
            font = new Font("微软雅黑", Font.BOLD, 30);
            g2.setFont(font);
            String firstWritten = StrUtil.sub(nameWritten, 0, 1);
            String secondWritten = StrUtil.sub(nameWritten, 0, 2);
            // 两个中文 如 言曌
            if (isChinese(firstWritten) && isChinese(secondWritten)) {
                g2.drawString(nameWritten, 20, 60);
            }
            // 首中次英 如 罗Q
            else if (isChinese(firstWritten) && !isChinese(secondWritten)) {
                g2.drawString(nameWritten, 27, 60);
                // 首英 如 AB
            } else {
                nameWritten = nameWritten.substring(0, 1);
            }
        }
        // 一个字
        if (nameWritten.length() == 1) {
            // 中文
            if (isChinese(nameWritten)) {
                font = new Font("微软雅黑", Font.PLAIN, 50);
                g2.setFont(font);
                g2.drawString(nameWritten, 25, 70);
            } else {
                font = new Font("微软雅黑", Font.PLAIN, 55);
                g2.setFont(font);
                g2.drawString(nameWritten.toUpperCase(), 33, 67);
            }
        }
        return ImgUtil.toBase64DataUri(bufferedImage, ImgUtil.IMAGE_TYPE_JPG);
    }

    /**
     * 设置文件下载响应都信息, 文件名重新编码
     *
     * @param response
     * @param downloadFileName 文件名
     */
    public static void generateAttachmentResponseHeader(HttpServletResponse response, String downloadFileName) throws Exception {
        // 文件名重新编码
        downloadFileName = URLEncoder.encode(downloadFileName, StandardCharsets.UTF_8.toString()).replaceAll("\\+", "%20");
        StringBuilder builder = new StringBuilder();
        builder.append("attachment; filename=")
                .append(downloadFileName)
                .append(";")
                .append("filename*=")
                .append(StandardCharsets.UTF_8.toString())
                .append(downloadFileName);
        response.setCharacterEncoding(StandardCharsets.UTF_8.toString());
        response.addHeader("Access-Control-Expose-Headers", "Content-Disposition,download-filename");
        response.setHeader(Header.CONTENT_DISPOSITION.getValue(), builder.toString());
        response.setHeader("download-filename", downloadFileName);
    }

    /**
     * 根据输入文章链接，自动显示出链接的标题，简介和icon(个别网站icon可能无法识别获取)
     *
     * @param url 待解析链接
     * @return icon: 网页图标, content: 网页简介, title: 网页标题
     */
    public static Map<String, Object> generateParseUrl(String url) {
        Map<String, Object> parseMap = new HashMap<>(5);
        try {
            if (Validator.isUrl(url)) {
                Document document = Jsoup.connect(url).timeout(10000).header("Access-Control-Allow-Origin", "*").header("Content-type", "application/json").userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) " + "Chrome/103.0.5060.114 Safari/537.36 Edg/103.0.1264.62").get();
                // 获取网页图标
                String icon = StrUtil.EMPTY;
                String image1 = document.getElementsByAttributeValueContaining("rel", "shortcut icon").attr("href");
                if (StrUtil.isNotBlank(image1)) {
                    if (StrUtil.equals(image1.substring(0, 4), "http") || StrUtil.equals(image1.substring(0, 2), "//")) {
                        icon = image1;
                    } else {
                        String domainName = getDomainName(url);
                        icon = domainName + image1;
                    }
                }
                parseMap.put("icon", icon);
                // 获取网页简介
                parseMap.put("description", document.getElementsByAttributeValueContaining("name", "description").attr("content"));
                // 获取网页标题
                parseMap.put("title", document.getElementsByTag("title").text());
            }
        } catch (IOException e) {
            log.error("Jsoup解析异常: {}", ExceptionUtil.getMessage(e));
        }
        return parseMap;
    }

    /**
     * 获取URL域名
     *
     * @param url
     * @return
     */
    private static String getDomainName(String url) {
        String str = StrUtil.EMPTY;
        if (!Validator.isUrl(url)) {
            return str;
        }
        String re = "((http|ftp|https)://)(([a-zA-Z0-9._-]+)|([0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}))(([a-zA-Z]{2,6})|(:[0-9]{1,4})?)";
        Pattern pattern = Pattern.compile(re);
        Matcher matcher = pattern.matcher(url);
        if (matcher.matches()) {
            str = url;
        } else {
            String[] split2 = url.split(re);
            if (split2.length > 1) {
                str = url.substring(0, url.length() - split2[1].length());
            } else {
                str = split2[0];
            }
        }
        return str;
    }

    /**
     * 获得随机颜色
     *
     * @author xuyuxiang
     * @date 2022/7/5 17:41
     **/
    private static Color getRandomColor() {
        String[] beautifulColors =
                new String[]{"114,101,230", "255,191,0", "0,162,174", "245,106,0", "24,144,255", "96,109,128"};
        String[] color = beautifulColors[RandomUtil.randomInt(beautifulColors.length)].split(StrUtil.COMMA);
        return new Color(Integer.parseInt(color[0]), Integer.parseInt(color[1]),
                Integer.parseInt(color[2]));
    }

    /**
     * 判断字符串是否为中文
     *
     * @author xuyuxiang
     * @date 2022/7/5 17:41
     **/
    private static boolean isChinese(String str) {
        String regEx = "[\\u4e00-\\u9fa5]+";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        return m.find();
    }

}
