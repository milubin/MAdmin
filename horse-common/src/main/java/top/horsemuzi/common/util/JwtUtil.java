package top.horsemuzi.common.util;

import cn.hutool.core.exceptions.ValidateException;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.jwt.JWTUtil;
import cn.hutool.jwt.JWTValidator;
import cn.hutool.jwt.signers.JWTSigner;
import cn.hutool.jwt.signers.JWTSignerUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.horsemuzi.common.base.JwtUser;
import top.horsemuzi.common.constants.Constants;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description 基于HuTool封装的JwtUtil处理工具
 * @date 2022/5/8 14:07
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class JwtUtil {

    private static final Logger log = LoggerFactory.getLogger(JwtUtil.class);

    /**
     * 自定义签名方法的key
     */
    private static final String ALGORITHM_KEY = RandomUtil.BASE_CHAR_NUMBER;

    /*
     * JWT结构:
     * Header 头部信息，主要声明了JWT的签名算法等信息
     * Payload 载荷信息，主要承载了各种声明并传递明文数据
     * Signature 签名，拥有该部分的JWT被称为JWS，也就是签了名的JWS，用于校验数据
     * */


    /**
     * 创建HS256(HmacSHA256) JWT Token: 存入redis, 携带用户信息, 真实意义上的token
     *
     * @param jwtUser 载荷信息实体
     * @return token字符串
     */
    public static String createToken(JwtUser jwtUser) {
        if (ObjectUtil.isNull(jwtUser)) {
            return null;
        }
        return JWTUtil.createToken(JsonUtil.fromJson(JsonUtil.toJson(jwtUser), new TypeReference<Map<String, Object>>() {
        }), JWTSignerUtil.hs512(ALGORITHM_KEY.getBytes(StandardCharsets.UTF_8)));
    }

    public static JwtUser parseToken(String token) {
        return commonParseToken(token);
    }

    public static JwtUser parseToken(HttpServletRequest request) {
        return request == null ? null : commonParseToken(request.getHeader(Constants.ACCESS_TOKEN));
    }

    private static JwtUser commonParseToken(String token) {
        if (CharSequenceUtil.isBlank(token)) {
            return null;
        }
        JwtUser jwtUser = JsonUtil.fromJson(JsonUtil.toJson(JWTUtil.parseToken(token).getPayloads()), JwtUser.class);
        if (ObjectUtil.isNull(jwtUser)) {
            return null;
        }
        return jwtUser;
    }

    public static Boolean verifyToken(String token) {
        return commonVerifyToken(token);
    }

    public static Boolean verifyToken(HttpServletRequest request) {
        return request == null ? null : commonVerifyToken(request.getHeader(Constants.ACCESS_TOKEN));
    }

    private static Boolean commonVerifyToken(String token) {
        if (CharSequenceUtil.isBlank(token)) {
            return Boolean.FALSE;
        }
        // step1: 校验token的签名是否有效
        JWTSigner signer = JWTSignerUtil.hs512(ALGORITHM_KEY.getBytes(StandardCharsets.UTF_8));
        boolean verify = JWTUtil.verify(token, signer);
        if (!verify) {
            return Boolean.FALSE;
        }
        // step2: 校验token的算法
        try {
            JWTValidator.of(token).validateAlgorithm(signer);
            return Boolean.TRUE;
        } catch (ValidateException e) {
            log.error(ExceptionUtil.getMessage(e));
            return Boolean.FALSE;
        }
    }

}
