package top.horsemuzi.common.util;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import cn.hutool.crypto.symmetric.AES;
import cn.hutool.crypto.symmetric.DESede;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import com.antherd.smcrypto.sm2.Keypair;
import com.antherd.smcrypto.sm2.Sm2;
import com.antherd.smcrypto.sm3.Sm3;
import com.antherd.smcrypto.sm4.Sm4;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * 加解密处理工具
 *
 * @author mabin
 * @date 2022/05/12 15:08
 **/
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EncryptUtil {

    private static final Logger log = LoggerFactory.getLogger(EncryptUtil.class);

    /**
     * MD5加密，生成16进制MD5字符串
     *
     * @param plainText
     * @return
     */
    public static String md5(String plainText) {
        if (CharSequenceUtil.isBlank(plainText)) {
            log.error("[EncryptUtil.MD5] plainText can not be blank!");
            return null;
        }
        return SecureUtil.md5(plainText);
    }

    /**
     * SHA1加密，生成16进制SHA1字符串
     *
     * @param plainText
     * @return
     */
    public static String sha1(String plainText) {
        if (CharSequenceUtil.isBlank(plainText)) {
            log.error("[EncryptUtil.sha1] plainText can not be blank!");
            return null;
        }
        return SecureUtil.sha1(plainText);
    }

    /**
     * SHA256加密，生成16进制SHA256字符串
     *
     * @param plainText
     * @return
     */
    public static String sha256(String plainText) {
        if (CharSequenceUtil.isBlank(plainText)) {
            log.error("[EncryptUtil.sha256] plainText can not be blank!");
            return null;
        }
        return SecureUtil.sha256(plainText);
    }

    /**
     * AES加密为Base64字符串, 使用UTF-8编码
     *
     * @param plaintext
     * @param key
     * @return
     */
    public static String aesEncrypt(String plaintext, String key) {
        if (CharSequenceUtil.hasBlank(plaintext, key)) {
            log.error("[EncryptUtil.aesEncrypt] plainText or key can not be blank!");
            return null;
        }
        return new AES(Base64.decode(key)).encryptBase64(plaintext);
    }

    /**
     * 解密Hex（16进制）或Base64表示的字符串，默认UTF-8编码
     *
     * @param cipherText
     * @param key
     * @return
     */
    public static String aesDecrypt(String cipherText, String key) {
        if (CharSequenceUtil.hasBlank(cipherText, key)) {
            log.error("[EncryptUtil.aesDecrypt] cipherText or key can not be blank!");
            return null;
        }
        return new AES(Base64.decode(key)).decryptStr(cipherText);
    }

    /**
     * 加密为Base64字符串, 使用UTF-8编码
     *
     * @param plainText
     * @param key
     * @return
     */
    public static String desedeEncrypt(String plainText, String key) {
        if (CharSequenceUtil.hasBlank(plainText, key)) {
            log.error("[EncryptUtil.desedeEncrypt] plainText or key can not be blank!");
            return null;
        }
        return new DESede(Base64.decode(key)).encryptBase64(plainText);
    }

    /**
     * 解密Hex（16进制）或Base64表示的字符串，默认UTF-8编码
     *
     * @param cipherText
     * @param key
     * @return
     */
    public static String desedeDecrypt(String cipherText, String key) {
        if (CharSequenceUtil.hasBlank(cipherText, key)) {
            log.error("[EncryptUtil.desedeDecrypt] cipherText or key can not be blank!");
            return null;
        }
        return new DESede(Base64.decode(key)).decryptStr(cipherText);
    }

    /**
     * pbkdf2算法加盐加密
     *
     * @param password 明文密码
     * @param slat     盐值
     * @return 密文密码 长度:128
     */
    public static String pbkdf2(String password, String slat) {
        if (CharSequenceUtil.hasBlank(password, slat)) {
            log.error("[EncryptUtil.pbkdf2] Neither the password nor the salt can be empty!");
            return null;
        }
        return SecureUtil.pbkdf2(password.toCharArray(), slat.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * RSA 公钥加密&私钥解密
     * 解密为字符串，密文需为Hex（16进制）或Base64字符串
     *
     * @param privateKey 私钥
     * @param cipherText 密文
     * @return 明文
     */
    public static String rsaDecrypt(String privateKey, String cipherText) {
        if (CharSequenceUtil.hasBlank(privateKey, cipherText)) {
            log.error("[EncryptUtil.rsaDecrypt] Neither the private key nor the cipherText can be empty!");
        }
        return new RSA(privateKey, null).decryptStr(cipherText, KeyType.PrivateKey);
    }

    /**
     * RSA 公钥加密&私钥解密
     * 编码为Base64字符串，使用UTF-8编码
     *
     * @param publicKey 公钥
     * @param plainText 明文
     * @return 密文
     */
    public static String rsaEncrypt(String publicKey, String plainText) {
        if (CharSequenceUtil.hasBlank(publicKey, plainText)) {
            log.error("[EncryptUtil.rsaEncrypt] Neither the public key nor the plainText can be empty!");
        }
        return new RSA(null, publicKey).encryptBase64(plainText, KeyType.PublicKey);
    }

    /**
     * SM2公钥加密&私钥解密
     *
     * @param publicKey
     * @param plainText
     * @return java.lang.String
     * @author mabin
     * @date 2023/3/21 15:55
     **/
    public static String sm2Encrypt(String publicKey, String plainText) {
        if (CharSequenceUtil.hasBlank(publicKey, plainText)) {
            log.error("[EncryptUtil.sm2Encrypt] Neither the public key nor the plainText can be empty!");
        }
        return Sm2.doEncrypt(plainText, publicKey);
    }

    /**
     * SM2公钥加密&私钥解密
     *
     * @param privateKey
     * @param cipherText
     * @return java.lang.String
     * @author mabin
     * @date 2023/3/21 15:55
     **/
    public static String sm2Decrypt(String privateKey, String cipherText) {
        if (CharSequenceUtil.hasBlank(privateKey, cipherText)) {
            log.error("[EncryptUtil.sm2Decrypt] Neither the private key nor the cipherText can be empty!");
        }
        return Sm2.doDecrypt(cipherText, privateKey);
    }

    /**
     * SM4加密
     *
     * @param key
     * @param plainText
     * @return java.lang.String
     * @author mabin
     * @date 2023/3/21 16:00
     **/
    public static String sm4Encrypt(String key, String plainText) {
        if (CharSequenceUtil.hasBlank(key, plainText)) {
            log.error("[EncryptUtil.sm4Encrypt] Neither the key nor the plainText can be empty!");
            return null;
        }
        return Sm4.encrypt(plainText, key);
    }

    /**
     * SM4解密
     * 如果采用加密机的方法，用try catch 捕捉异常，返回原文值即可
     *
     * @param key
     * @param cipherText
     * @return java.lang.String
     * @author mabin
     * @date 2023/3/21 16:01
     **/
    public static String sm4Decrypt(String key, String cipherText) {
        if (CharSequenceUtil.hasBlank(key, cipherText)) {
            log.error("[EncryptUtil.sm4Decrypt] Neither the key nor the cipherText can be empty!");
            return null;
        }
        String docString = Sm4.decrypt(cipherText, key);
        if (CharSequenceUtil.equals(CharSequenceUtil.EMPTY, docString)) {
            log.warn("[EncryptUtil.sm4Decrypt] 解密失败, 返回原文值: {}", cipherText);
            return cipherText;
        } else {
            return docString;
        }
    }

    /**
     * SM3杂凑散列
     *
     * @param text
     * @return java.lang.String
     * @author mabin
     * @date 2023/6/10 13:55
     **/
    public static String sm3(String text) {
        if (CharSequenceUtil.isBlank(text)) {
            log.error("[EncryptUtil.sm3] Neither the key nor the text can be empty!");
            return null;
        }
        return Sm3.sm3(text);
    }


    // ======================================= 初始化相关密钥 =======================================

    /**
     * 初始化RSA密钥
     *
     * @return
     */
    public static Map<String, String> generateRsaKey() {
        Map<String, String> keyMap = new HashMap<>(4);
        RSA rsa = new RSA();
        keyMap.put("privateKey", rsa.getPrivateKeyBase64());
        keyMap.put("publicKey", rsa.getPublicKeyBase64());
        return keyMap;
    }

    /**
     * 初始化AES密钥
     *
     * @return
     */
    public static String generateAesKey() {
        return Base64.encode(SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue()).getEncoded());
    }

    /**
     * 初始化3DES密钥
     *
     * @return
     */
    public static String generate3DesKey() {
        return Base64.encode(SecureUtil.generateKey(SymmetricAlgorithm.DESede.getValue()).getEncoded());
    }

    /**
     * 初始化SM2公私钥
     *
     * @param
     * @return java.util.Map<java.lang.String, java.lang.String>
     * @author mabin
     * @date 2023/3/21 15:51
     **/
    public static Map<String, String> generateSm2Key() {
        Keypair keypair = Sm2.generateKeyPairHex();
        Map<String, String> keyMap = new HashMap<>(4);
        keyMap.put("privateKey", keypair.getPrivateKey());
        keyMap.put("publicKey", keypair.getPublicKey());
        return keyMap;
    }

    /**
     * 初始化SM4密钥 16进制字符串 128bit
     *
     * @param
     * @return java.lang.String
     * @author mabin
     * @date 2023/3/21 16:06
     **/
    public static String generateSm4Key() {
        return Convert.toHex(RandomUtil.randomString(RandomUtil.BASE_CHAR_NUMBER, 16), CharsetUtil.CHARSET_UTF_8);
    }

}
