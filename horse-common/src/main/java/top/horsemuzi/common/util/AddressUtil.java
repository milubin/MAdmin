package top.horsemuzi.common.util;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.net.NetUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.lionsoul.ip2region.xdb.Searcher;
import org.springframework.core.io.ClassPathResource;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * 地区处理工具
 *
 * @author mabin
 **/
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AddressUtil {

    private static final Searcher SEARCHER;

    // 静态代码块初始化
    static {
        try {
            String fileName = "/ip2region.xdb";
            File existFile = FileUtil.file(Constants.TEMP_DOC + FileUtil.FILE_SEPARATOR + fileName);
            if (!FileUtil.exist(existFile)) {
                ClassPathResource classPathResource = new ClassPathResource(fileName);
                if (ObjectUtil.isEmpty(classPathResource.getInputStream())) {
                    throw new BusinessException(ResultCodeEnum.INTERNAL_ERROR.getCode(), "IP地址库文件不存在");
                }
                FileUtil.writeFromStream(classPathResource.getInputStream(), existFile);
            }
            SEARCHER = Searcher.newWithBuffer(Searcher.loadContentFromFile(existFile.getPath()));
        } catch (Exception e) {
            log.error("IP地址库初始化失败", e);
            throw new BusinessException(ResultCodeEnum.INTERNAL_ERROR.getCode(), "IP地址库初始化失败");
        }
    }

    /**
     * 根据ip在线获取地区信息
     *
     * @param ip
     * @return java.lang.String
     * @author mabin
     * @date 2023/6/2 14:57
     **/
    public static String onLineByIp(String ip) {
        if (StrUtil.isBlank(ip)) {
            log.error("IP address cannot be empty");
            return "UNKNOWN";
        }
        if (NetUtil.isInnerIP(ip)) {
            return Constants.INTRANET_AREA;
        }
        Map<String, Object> paramsMap = new HashMap<>(4);
        paramsMap.put("ip", ip.trim());
        paramsMap.put("json", true);
        String response = HttpRequest.get("http://whois.pconline.com.cn/ipJson.jsp")
                .form(paramsMap)
                .timeout(1000)
                .execute().body();
        if (StrUtil.isBlank(response)) {
            log.error("获取地理位置异常, ip={}", ip);
            return "UNKNOWN";
        }
        JsonNode jsonNode = JsonUtil.fromJson(response);
        String pro = JsonUtil.getNodeStr(jsonNode, "pro");
        String city = JsonUtil.getNodeStr(jsonNode, "city");
        return pro + city;
    }

    /**
     * 根据ip离线获取地区信息
     *
     * @param ip
     * @return java.lang.String
     * @author mabin
     * @date 2023/6/2 14:58
     **/
    public static String offLineByIp(String ip) {
        if (StrUtil.isBlank(ip)) {
            log.error("IP address cannot be empty");
            return "UNKNOWN";
        }
        if (NetUtil.isInnerIP(ip)) {
            return Constants.INTRANET_AREA;
        }
        String search = null;
        try {
            search = SEARCHER.search(ip.trim());
        } catch (Exception e) {
            log.error("IP地址离线获取地区异常", e);
            return "UNKNOWN";
        }
        log.info("search={}", search);
        return search.replace("0|", StrUtil.EMPTY).replace("|0", StrUtil.EMPTY);
    }

}
