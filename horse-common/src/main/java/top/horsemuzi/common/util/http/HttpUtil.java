package top.horsemuzi.common.util.http;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import top.horsemuzi.common.util.http.httpclient.HttpClientUtil;
import top.horsemuzi.common.util.http.okhttp.OkHttp3Util;

import java.util.Map;

/**
 * 系统HTTP客户端
 *
 * @author mabin
 * @date 2022/09/22 17:53
 **/
@Slf4j
public class HttpUtil {

    /**
     * 基于HttpClient封装
     */
    public static class HTTP_CLIENT {

        /**
         * get请求
         *
         * @param url
         * @return
         */
        public static String get(String url) {
            return HttpClientUtil.doGet(url);
        }

        /**
         * get请求
         *
         * @param url
         * @param paramsMap
         * @return
         */
        public static String get(String url, Map<String, String> paramsMap) {
            return HttpClientUtil.doGet(url, paramsMap);
        }

        /**
         * get请求
         *
         * @param url
         * @param paramsMap
         * @return
         */
        public static String get(String url, Map<String, String> headersMap, Map<String, String> paramsMap) {
            return HttpClientUtil.doGet(url, headersMap, paramsMap);
        }

        /**
         * post请求
         *
         * @param url
         * @return
         */
        public static String post(String url) {
            return HttpClientUtil.doPost(url);
        }

        /**
         * post请求
         *
         * @param url
         * @param paramsMap
         * @param isJson    是否json格式交互
         * @return
         */
        public static String post(String url, Map<String, String> paramsMap, Boolean isJson) {
            return HttpClientUtil.doPost(url, paramsMap, isJson);
        }

        /**
         * post请求
         *
         * @param url
         * @param paramsMap
         * @return
         */
        public static String post(String url, Map<String, String> headersMap, Map<String, String> paramsMap, Boolean isJson) {
            return HttpClientUtil.doPost(url, headersMap, paramsMap, isJson);
        }

    }

    /**
     * 基于OkHttp3封装
     */
    public static class OK_HTTP {

        /**
         * get请求, 默认异步
         *
         * @param url
         * @return
         */
        public static String get(String url) {
            return get(url, null, null, Boolean.TRUE);
        }

        /**
         * get请求, 同步/异步
         *
         * @param url
         * @param async true: 异步, false: 同步
         * @return
         */
        public static String get(String url, Boolean async) {
            return get(url, null, null, async);
        }

        /**
         * get请求, 默认异步
         *
         * @param url
         * @param paramsMap
         * @return
         */
        public static String get(String url, Map<String, Object> paramsMap) {
            return get(url, null, paramsMap, Boolean.TRUE);
        }

        /**
         * get请求
         *
         * @param url
         * @param paramsMap
         * @param async
         * @return
         */
        public static String get(String url, Map<String, Object> paramsMap, Boolean async) {
            return get(url, null, paramsMap, async);
        }

        /**
         * get请求, 默认异步
         *
         * @param url
         * @param headersMap
         * @param paramsMap
         * @return
         */
        public static String get(String url, Map<String, Object> headersMap, Map<String, Object> paramsMap) {
            return get(url, headersMap, paramsMap, Boolean.TRUE);
        }

        /**
         * get请求
         *
         * @param url
         * @param headersMap 请求头
         * @param paramsMap  请求参数
         * @param async
         * @return
         */
        public static String get(String url, Map<String, Object> headersMap, Map<String, Object> paramsMap, Boolean async) {
            OkHttp3Util http3Util = OkHttp3Util.builder().url(url);
            if (MapUtil.isNotEmpty(headersMap)) {
                http3Util.addHeader(headersMap);
            }
            if (MapUtil.isNotEmpty(paramsMap)) {
                http3Util.addParam(paramsMap);
            }
            http3Util.get();
            return async ? http3Util.async() : http3Util.sync();
        }

        /**
         * post方式
         *
         * @param url
         * @return
         */
        public static String post(String url) {
            return post(url, null, null, null, Boolean.TRUE, Boolean.TRUE);
        }

        /**
         * post方式
         *
         * @param url
         * @param async
         * @return
         */
        public static String post(String url, Boolean async) {
            return post(url, null, null, null, async, Boolean.TRUE);
        }

        /**
         * post方式
         *
         * @param url
         * @param async
         * @param json
         * @return
         */
        public static String post(String url, Boolean async, Boolean json) {
            return post(url, null, null, null, async, json);
        }

        /**
         * post方式
         *
         * @param url
         * @param jsonParam
         * @return
         */
        public static String post(String url, String jsonParam) {
            return post(url, jsonParam, null, null, Boolean.TRUE, Boolean.TRUE);
        }

        /**
         * post方式
         *
         * @param url
         * @param jsonParam
         * @param async
         * @return
         */
        public static String post(String url, String jsonParam, Boolean async) {
            return post(url, jsonParam, null, null, async, Boolean.TRUE);
        }

        /**
         * post方式
         *
         * @param url
         * @param jsonParam
         * @param async
         * @param json
         * @return
         */
        public static String post(String url, String jsonParam, Boolean async, Boolean json) {
            return post(url, jsonParam, null, null, async, json);
        }

        /**
         * post方式
         *
         * @param url
         * @param paramsMap
         * @return
         */
        public static String post(String url, Map<String, Object> paramsMap) {
            return post(url, null, null, paramsMap, Boolean.TRUE, Boolean.TRUE);
        }

        /**
         * post方式
         *
         * @param url
         * @param paramsMap
         * @param async
         * @return
         */
        public static String post(String url, Map<String, Object> paramsMap, Boolean async) {
            return post(url, null, null, paramsMap, async, Boolean.TRUE);
        }

        /**
         * post方式
         *
         * @param url
         * @param paramsMap
         * @param async
         * @param json
         * @return
         */
        public static String post(String url, Map<String, Object> paramsMap, Boolean async, Boolean json) {
            return post(url, null, null, paramsMap, async, json);
        }

        /**
         * post方式
         *
         * @param url
         * @param json
         * @param headersMap
         * @param paramsMap
         * @param async
         * @param json       true: json方式, false: form表单方式
         * @return
         */
        public static String post(String url, String jsonParam, Map<String, Object> headersMap, Map<String, Object> paramsMap, Boolean async, Boolean json) {
            OkHttp3Util http3Util = OkHttp3Util.builder().url(url);
            if (StrUtil.isNotBlank(jsonParam)) {
                http3Util.addJson(jsonParam);
            }
            if (MapUtil.isNotEmpty(headersMap)) {
                http3Util.addHeader(headersMap);
            }
            if (MapUtil.isNotEmpty(paramsMap)) {
                http3Util.addParam(paramsMap);
            }
            http3Util.post(json);
            return async ? http3Util.async() : http3Util.sync();
        }

    }

}
