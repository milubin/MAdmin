package top.horsemuzi.common.util.http.httpclient;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

/**
 * Http客户端连接池
 *
 * @author mabin
 * @date 2022/10/12 15:13
 **/

public class HttpClientPool {

    private static PoolingHttpClientConnectionManager getHttpClientPool() {
        // 创建连接池管理器
        PoolingHttpClientConnectionManager poolingHttpClientConnectionManager = new PoolingHttpClientConnectionManager();
        // 设置最大连接数
        poolingHttpClientConnectionManager.setMaxTotal(100);
        // 设置每个主机的最大连接数
        poolingHttpClientConnectionManager.setDefaultMaxPerRoute(10);
        return poolingHttpClientConnectionManager;
    }

    /**
     * 获取连接池中的HttpClient对象
     *
     * @return
     */
    public static CloseableHttpClient getClient() {
        return HttpClients.custom().setConnectionManager(getHttpClientPool()).build();
    }


}
