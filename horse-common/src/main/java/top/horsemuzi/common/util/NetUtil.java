package top.horsemuzi.common.util;

import cn.hutool.core.util.StrUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.exception.BusinessException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.Optional;

/**
 * 网络处理相关，例如ip定位、地区获取等
 *
 * @author mabin
 * @date 2022/05/11 16:29
 **/
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class NetUtil {

    private static final String UNKNOWN = "UNKNOWN";

    /**
     * 获取请求客户端的ip
     *
     * @return
     */
    public static String getRequestIp() {
        return getRequestIp(getGlobalRequest());
    }

    /**
     * 获取请求客户端的ip
     *
     * @param request
     * @return
     */
    public static String getRequestIp(HttpServletRequest request) {
        String xip = request.getHeader("X-Real-IP");
        String xfor = request.getHeader("X-Forwarded-For");
        if (StrUtil.isNotBlank(xfor) && !StrUtil.equalsIgnoreCase(UNKNOWN, xfor)) {
            //多次反向代理后会有多个ip值，第一个ip才是真实ip
            int index = xfor.indexOf(StrUtil.COMMA);
            if (index != -1) {
                return xfor.substring(0, index);
            } else {
                return xfor;
            }
        }
        xfor = xip;
        if (StrUtil.isNotBlank(xfor) && !StrUtil.equalsIgnoreCase(UNKNOWN, xfor)) {
            return xfor;
        }
        if (StrUtil.isBlank(xfor) || StrUtil.equalsIgnoreCase(UNKNOWN, xfor)) {
            xfor = request.getHeader("Proxy-Client-IP");
        }
        if (StrUtil.isBlank(xfor) || StrUtil.equalsIgnoreCase(UNKNOWN, xfor)) {
            xfor = request.getHeader("WL-Proxy-Client-IP");
        }
        if (StrUtil.isBlank(xfor) || StrUtil.equalsIgnoreCase(UNKNOWN, xfor)) {
            xfor = request.getHeader("HTTP_CLIENT_IP");
        }
        if (StrUtil.isBlank(xfor) || StrUtil.equalsIgnoreCase(UNKNOWN, xfor)) {
            xfor = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (StrUtil.isBlank(xfor) || StrUtil.equalsIgnoreCase(UNKNOWN, xfor)) {
            xfor = request.getRemoteAddr();
        }
        return StrUtil.contains(xfor, "0:0:0:0:0:0:0:1") ? Constants.INTRANET_IP : xfor;
    }


    /**
     * 获取当前机器IP
     *
     * @return
     */
    public static String getLocalIp() {
        try {
            InetAddress candidateAddress = null;
            // 遍历所有的网络接口
            for (Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces(); interfaces.hasMoreElements(); ) {
                NetworkInterface anInterface = interfaces.nextElement();
                // 在所有的接口下再遍历IP
                for (Enumeration<InetAddress> inetAddresses = anInterface.getInetAddresses(); inetAddresses.hasMoreElements(); ) {
                    InetAddress inetAddr = inetAddresses.nextElement();
                    // 排除loopback类型地址
                    if (!inetAddr.isLoopbackAddress()) {
                        if (inetAddr.isSiteLocalAddress()) {
                            // 如果是site-local地址，就是它了
                            return inetAddr.getHostAddress();
                        } else if (candidateAddress == null) {
                            // site-local类型的地址未被发现，先记录候选地址
                            candidateAddress = inetAddr;
                        }
                    }
                }
            }
            if (candidateAddress != null) {
                return candidateAddress.getHostAddress();
            }
            // 如果没有发现 non-loopback地址.只能用最次选的方案
            InetAddress jdkSuppliedAddress = InetAddress.getLocalHost();
            if (jdkSuppliedAddress == null) {
                return "";
            }
            return jdkSuppliedAddress.getHostAddress();
        } catch (Exception e) {
            return "";
        }
    }


    /**
     * 获取全局HttpServletRequest
     *
     * @return
     */
    public static HttpServletRequest getGlobalRequest() {
        ServletRequestAttributes servletRequestAttributes =
                Optional.ofNullable((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                        .orElseThrow(() -> new BusinessException(ResultCodeEnum.GET_GLOBAL_REQUEST_ERROR));
        return servletRequestAttributes.getRequest();
    }

    /**
     * 获取全局HttpServletResponse
     *
     * @return
     */
    public static HttpServletResponse getGlobalResponse() {
        ServletRequestAttributes servletRequestAttributes =
                Optional.ofNullable((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                        .orElseThrow(() -> new BusinessException(ResultCodeEnum.GET_GLOBAL_RESPONSE_ERROR));
        return servletRequestAttributes.getResponse();
    }

    /**
     * 设置主线程的上下文数据共享给子线程 (在开启异步线程之前调用, 否则子线程中将无法使用 NetUtil.getGlobalRequest() 方法获取到全局的request对象)
     */
    public static void setRequestInheritable() {
        RequestContextHolder.setRequestAttributes(RequestContextHolder.getRequestAttributes(), true);
    }

}
