package top.horsemuzi.common.util;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.SCPClient;
import ch.ethz.ssh2.Session;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.ObjectUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * 基于ganymend-ssh2的服务器连接处理
 *
 * @author mabin
 * @date 2022/05/05 16:54
 **/
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class GanymedSSH2Util {

    private static final Logger log = LoggerFactory.getLogger(GanymedSSH2Util.class);

    /**
     * 默认端口
     */
    private static final Integer DEFAULT_PORT = 22;

    /**
     * ssh密钥文件 动态端口号远程登录, 执行shell脚本
     *
     * @param connection
     * @param command    执行的命令
     * @return 命令执行完后返回的结果值
     */
    public static String exec(Connection connection, String command) {
        return getResult(connection, command);
    }

    /**
     * 下载远程文件
     *
     * @param connection
     * @param sourceFile 源文件绝对路径
     * @param targetFile 目标文件夹
     */
    public static void download(Connection connection, String sourceFile, String targetFile) {
        try {
            SCPClient scpClient = new SCPClient(connection);
            scpClient.get(sourceFile, targetFile);
        } catch (Exception e) {
            log.error("exec scp command error! ", e);
        }
    }

    public static void download(Connection connection, String[] sourceFile, String targetFile) {
        try {
            SCPClient scpClient = new SCPClient(connection);
            scpClient.get(sourceFile, targetFile);
        } catch (Exception e) {
            log.error("exec scp command error! ", e);
        }
    }

    /**
     * 以 用户名&密码 方式创建连接
     *
     * @param ip
     * @param port
     * @param user
     * @param password
     * @return
     */
    public static Connection connection(String ip, @Nullable Integer port, String user, String password) {
        return connection(ip, port, user, null, password);
    }

    /**
     * 以 用户名&密码&密钥 方式创建连接
     *
     * @param ip
     * @param port
     * @param user
     * @param privateFile
     * @param password
     * @return
     */
    public static Connection connection(String ip, @Nullable Integer port, String user, @Nullable File privateFile,
                                        String password) {
        Connection connection = null;
        try {
            connection = new Connection(ip, ObjectUtil.isEmpty(port) ? DEFAULT_PORT : port);
            connection.connect();
            if (ObjectUtil.isNotNull(privateFile)) {
                connection.authenticateWithPublicKey(user, privateFile, password);
                log.info("Start server authenticate by privateKey successfully");
            } else {
                connection.authenticateWithPassword(user, password);
                log.info("Start server authenticate by user&password successfully");
            }
        } catch (IOException e) {
            log.error("Server authenticate fail!", e);
            connection.close();
        }
        return connection;
    }


    /**
     * 执行命令,获取纯文本结果
     *
     * @param connection 有效连接
     * @param command    自定义命令
     * @return
     */
    private static String getResult(Connection connection, String command) {
        String result = null;
        if (connection != null) {
            Session session = null;
            try {
                session = connection.openSession();
                // 执行命令
                session.execCommand(command);
                // 获取控制台标准输出
                result = processStdout(session.getStdout());
                // 如果输出信息为空, 则说明你执行失败, 获取失败信息
                if (CharSequenceUtil.isBlank(result)) {
                    result = processStdout(session.getStderr());
                }
            } catch (Exception e) {
                log.error("Execute command exception! ", e);
            } finally {
                connection.close();
                if (session != null) {
                    session.close();
                }
            }
        }
        return result;
    }

    /**
     * 解析脚本执行返回的结果集
     *
     * @param inputStream 输入流
     * @return 以纯文本的格式返回
     */
    private static String processStdout(InputStream inputStream) {
        StringBuilder builder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
            String line;
            while ((line = br.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException e) {
            log.error("解析脚本执行结果异常", e);
        }
        return builder.toString();
    }

}
