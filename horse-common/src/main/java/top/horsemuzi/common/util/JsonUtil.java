package top.horsemuzi.common.util;

import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Objects;

/**
 * @author horse
 * @date To0To0/10/17 1To:ToTo
 * jackson序列化工具类
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class JsonUtil {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    static {
        // 统一日期格式yyyy-MM-dd HH:mm:ss
        MAPPER.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        // 设置解析遇到未知属性时不失败
        MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        // 空对象转json不抛出异常
        MAPPER.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        // 是否允许解析使用Java/C++ 样式的注释（包括'/'+'*' 和'//' 变量）
        MAPPER.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
        // 是否允许属性名称没有引号
        MAPPER.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        // 是否允许单引号
        MAPPER.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
        // 是否允许特殊字符
        MAPPER.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
    }

    public static <T> String toJson(T obj) {
        if (ObjectUtils.isEmpty(obj)) {
            return null;
        }
        try {
            return obj instanceof String ? (String) obj : MAPPER.writeValueAsString(obj);
        } catch (Exception e) {
            log.error("Parse object to String error", e);
            return null;
        }
    }

    public static <T> String toPrettyJson(T obj) {
        if (ObjectUtils.isEmpty(obj)) {
            return null;
        }
        try {
            return obj instanceof String ? (String) obj :
                    MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
        } catch (Exception e) {
            log.error("Parse object to String error", e);
            return null;
        }
    }

    public static <T> T fromJson(String str, Class<T> clazz) {
        if (StringUtils.isBlank(str) || ObjectUtils.isEmpty(clazz)) {
            return null;
        }
        try {
            return clazz.equals(String.class) ? (T) str : MAPPER.readValue(str, clazz);
        } catch (IOException e) {
            log.error("Parse String to Object error", e);
            return null;
        }
    }

    public static <T> T fromJson(String str, TypeReference<T> typeReference) {
        if (StringUtils.isBlank(str) || ObjectUtils.isEmpty(typeReference)) {
            return null;
        }
        try {
            return (T) (typeReference.getType().equals(String.class) ? str :
                    MAPPER.readValue(str, typeReference));
        } catch (IOException e) {
            log.error("Parse String to Object error", e);
            return null;
        }
    }

    public static <T> T fromJson(String str, Class<?> collectionClass, Class<?>... clazz) {
        if (StringUtils.isBlank(str) || !ObjectUtils.allNotNull(collectionClass, clazz)) {
            return null;
        }
        JavaType javaType = MAPPER.getTypeFactory().constructParametricType(collectionClass, clazz);
        try {
            return MAPPER.readValue(str, javaType);
        } catch (IOException e) {
            log.error("Parse String to Object error", e);
            return null;
        }
    }

    public static JsonNode fromJson(String str) {
        if (StringUtils.isBlank(str)) {
            return null;
        }
        try {
            return MAPPER.readTree(str);
        } catch (Exception e) {
            log.error("Parse object to String error", e);
            return null;
        }
    }

    public static <T> T fromJson(JsonNode jsonNode, Class<T> clazz) {
        if (!ObjectUtils.allNotNull(jsonNode, clazz)) {
            return null;
        }
        try {
            return MAPPER.treeToValue(jsonNode, clazz);
        } catch (Exception e) {
            log.error("Parse object to String error", e);
            return null;
        }
    }

    public static JsonNode atNode(JsonNode node, String ptr) {
        if (Objects.isNull(node) || StrUtil.isBlank(ptr)) {
            return null;
        }
        JsonNode at = node.at(ptr);
        return Objects.isNull(at.textValue()) ? null : at;
    }

    public static ObjectNode getObjectNode() {
        return MAPPER.createObjectNode();
    }

    public static ObjectNode getObjectNode(String str, String key) {
        JsonNode jsonNode = fromJson(str);
        if (Objects.isNull(jsonNode)) {
            return null;
        }
        JsonNode node = jsonNode.get(key);
        if (Objects.isNull(node)) {
            return null;
        }
        return (ObjectNode) node;
    }

    public static ObjectNode getObjectNode(JsonNode jsonNode, String key) {
        if (Objects.isNull(jsonNode) || StringUtils.isBlank(key)) {
            return null;
        }
        if (jsonNode.hasNonNull(key)) {
            JsonNode node = jsonNode.get(key);
            if (Objects.isNull(node)) {
                return null;
            }
            return (ObjectNode) node;
        }
        return null;
    }

    public static ArrayNode getArrayNode() {
        return MAPPER.createArrayNode();
    }

    public static ArrayNode getArrayNode(String str, String key) {
        JsonNode jsonNode = fromJson(str);
        if (Objects.isNull(jsonNode)) {
            return null;
        }
        JsonNode node = jsonNode.get(key);
        if (Objects.isNull(node) || !node.isArray()) {
            return null;
        }
        return (ArrayNode) node;
    }

    public static ArrayNode getArrayNode(JsonNode jsonNode, String key) {
        if (Objects.isNull(jsonNode) || StringUtils.isBlank(key)) {
            return null;
        }
        JsonNode node = jsonNode.get(key);
        if (Objects.isNull(node) || !node.isArray()) {
            return null;
        }
        return (ArrayNode) node;
    }

    public static String getNodeStr(JsonNode node, String key) {
        return getNodeStr(node, key, null);
    }

    public static String getNodeStr(JsonNode node, String key, String defaultValue) {
        if (ObjectUtils.isEmpty(node) || StringUtils.isBlank(key)) {
            return null;
        }
        if (node.hasNonNull(key)) {
            return StringUtils.isNotBlank(defaultValue) ? node.get(key).asText(defaultValue) : node.get(key).asText();
        }
        return null;
    }

    public static Integer getNodeInt(JsonNode node, String key) {
        return getNodeInt(node, key, null);
    }

    public static Integer getNodeInt(JsonNode node, String key, Integer defaultValue) {
        if (ObjectUtils.isEmpty(node) || StringUtils.isBlank(key)) {
            return null;
        }
        if (node.hasNonNull(key)) {
            return ObjectUtils.isNotEmpty(defaultValue) ? node.get(key).asInt(defaultValue) : node.get(key).asInt();
        }
        return null;
    }

    public static Long getNodeLong(JsonNode node, String key) {
        return getNodeLong(node, key, null);
    }

    public static Long getNodeLong(JsonNode node, String key, Long defaultValue) {
        if (ObjectUtils.isEmpty(node) || StringUtils.isBlank(key)) {
            return null;
        }
        if (node.hasNonNull(key)) {
            return ObjectUtils.isNotEmpty(defaultValue) ? node.get(key).asLong(defaultValue) : node.get(key).asLong();
        }
        return null;
    }

    public static Boolean getNodeBool(JsonNode node, String key) {
        return getNodeBool(node, key, null);
    }

    public static Boolean getNodeBool(JsonNode node, String key, Boolean defaultValue) {
        if (ObjectUtils.isEmpty(node) || StringUtils.isBlank(key)) {
            return null;
        }
        if (node.hasNonNull(key)) {
            return ObjectUtils.isNotEmpty(defaultValue) ? node.get(key).asBoolean(defaultValue) : node.get(key).asBoolean();
        }
        return null;
    }

    public static Float getNodeFloat(JsonNode node, String key) {
        if (ObjectUtils.isEmpty(node) || StringUtils.isBlank(key)) {
            return null;
        }
        if (node.hasNonNull(key)) {
            return node.get(key).floatValue();
        }
        return null;
    }

    public static Double getNodeDouble(JsonNode node, String key) {
        return getNodeDouble(node, key, null);
    }

    public static Double getNodeDouble(JsonNode node, String key, Double defaultValue) {
        if (ObjectUtils.isEmpty(node) || StringUtils.isBlank(key)) {
            return null;
        }
        if (node.hasNonNull(key)) {
            return ObjectUtils.isNotEmpty(defaultValue) ? node.get(key).asDouble(defaultValue) : node.get(key).asDouble();
        }
        return null;
    }

    public static BigDecimal getNodeDecimal(JsonNode node, String key) {
        if (ObjectUtils.isEmpty(node) || StringUtils.isBlank(key)) {
            return null;
        }
        if (node.hasNonNull(key)) {
            return node.get(key).decimalValue();
        }
        return null;
    }

}
