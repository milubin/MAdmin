package top.horsemuzi.common.validator.custom;

import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.StrUtil;
import top.horsemuzi.common.validator.custom.annotation.IdCard;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author 马滨
 * @date 2022/11/20 14:25
 **/

public class IdCardValidator implements ConstraintValidator<IdCard, String> {

    private String message = StrUtil.EMPTY;

    @Override
    public void initialize(IdCard constraintAnnotation) {
        message = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(String idCard, ConstraintValidatorContext context) {
        if (StrUtil.isBlank(idCard)) {
            return true;
        }
        if (StrUtil.isNotBlank(message)) {
            // 自定义提示语支持 {} 或者 %s 模板填充处理
            if (StrUtil.contains(message, StrUtil.EMPTY_JSON)) {
                message = StrUtil.format(message, idCard);
            }
            if (StrUtil.contains(message, "%s")) {
                message = String.format(message, idCard);
            }
            // 禁用默认提示信息
            context.disableDefaultConstraintViolation();
            // 重新设置提示语
            context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
        }
        return Validator.isCitizenId(idCard);
    }

}
