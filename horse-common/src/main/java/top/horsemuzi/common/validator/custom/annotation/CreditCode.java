package top.horsemuzi.common.validator.custom.annotation;

import top.horsemuzi.common.validator.custom.CreditCodeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 自定义社会统一信用码校验注解
 *
 * @author mabin
 * @date 2022/11/28 14:31
 **/
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = CreditCodeValidator.class)
public @interface CreditCode {

    /**
     * 自定义提示语支持 {} 或者 %s 模板填充处理（目前默认填充：当前参数值）
     */
    String message() default "统一社会信用代码不合法";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
