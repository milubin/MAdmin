package top.horsemuzi.common.validator.custom;

import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.StrUtil;
import top.horsemuzi.common.validator.custom.annotation.Phone;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 手机号校验处理
 *
 * @author 马滨
 * @date 2022/11/20 14:05
 **/

public class PhoneValidator implements ConstraintValidator<Phone, String> {

    /**
     * 使用@Phone注解时的自定义提示消息
     */
    private String message = StrUtil.EMPTY;

    @Override
    public void initialize(Phone constraintAnnotation) {
        message = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(String phone, ConstraintValidatorContext context) {
        if (StrUtil.isBlank(phone)) {
            return true;
        }
        if (StrUtil.isNotBlank(message)) {
            // 自定义提示语支持 {} 或者 %s 模板填充处理
            if (StrUtil.contains(message, StrUtil.EMPTY_JSON)) {
                message = StrUtil.format(message, phone);
            }
            if (StrUtil.contains(message, "%s")) {
                message = String.format(message, phone);
            }
            // 禁用默认提示信息
            context.disableDefaultConstraintViolation();
            // 重新设置提示语
            context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
        }
        return Validator.isMobile(phone);
    }
}
