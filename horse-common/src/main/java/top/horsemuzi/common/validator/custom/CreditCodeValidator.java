package top.horsemuzi.common.validator.custom;

import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.StrUtil;
import top.horsemuzi.common.validator.custom.annotation.CreditCode;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author mabin
 * @date 2022/11/28 14:32
 **/

public class CreditCodeValidator implements ConstraintValidator<CreditCode, String> {

    private String message = StrUtil.EMPTY;

    @Override
    public void initialize(CreditCode constraint) {
        message = constraint.message();
    }

    @Override
    public boolean isValid(String creditCode, ConstraintValidatorContext context) {
        if (StrUtil.isBlank(creditCode)) {
            return true;
        }
        if (StrUtil.isNotBlank(message)) {
            // 自定义提示语支持 {} 或者 %s 模板填充处理
            if (StrUtil.contains(message, StrUtil.EMPTY_JSON)) {
                message = StrUtil.format(message, creditCode);
            }
            if (StrUtil.contains(message, "%s")) {
                message = String.format(message, creditCode);
            }
            // 禁用默认提示信息
            context.disableDefaultConstraintViolation();
            // 重新设置提示语
            context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
        }
        return Validator.isCreditCode(creditCode);
    }

}
