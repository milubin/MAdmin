package top.horsemuzi.common.validator.custom;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import top.horsemuzi.common.validator.custom.annotation.ValidEnum;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * 自定义枚举校验处理
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/04/23 09:31
 **/

public class EnumValidator implements ConstraintValidator<ValidEnum, Object> {

    /**
     * 使用@ValidEnum注解时的自定义提示消息
     */
    private String message = StrUtil.EMPTY;

    /**
     * 收集当前枚举中所有的枚举值列表
     */
    private final Set<String> validValues = new HashSet<>(16);

    @Override
    public void initialize(ValidEnum constraintAnnotation) {
        message = constraintAnnotation.message();
        String value = constraintAnnotation.value();
        Arrays.stream(constraintAnnotation.name().getEnumConstants())
                .forEach(e -> validValues.add(Convert.toStr(ReflectUtil.getFieldValue(e, value))));
    }


    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (Objects.isNull(value)) {
            return true;
        }
        if (StrUtil.isNotBlank(message)) {
            // 自定义提示语支持 {} 或者 %s 模板填充处理
            if (StrUtil.contains(message, StrUtil.EMPTY_JSON)) {
                message = StrUtil.format(message, value);
            }
            if (StrUtil.contains(message, "%s")) {
                message = String.format(message, value);
            }
            // 禁用默认提示信息
            context.disableDefaultConstraintViolation();
            // 重新设置提示语
            context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
        }
        return validValues.contains(Convert.toStr(value));
    }

}
