package top.horsemuzi.common.validator.custom.annotation;

import top.horsemuzi.common.validator.custom.EnumValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 自定义枚举校验注解
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/04/23 09:31
 **/

@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = EnumValidator.class)
public @interface ValidEnum {


    /**
     * 自定义提示语支持 {} 或者 %s 模板填充处理（目前默认填充：当前参数值）
     */
    String message() default "无效的枚举值";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    Class<? extends Enum<?>> name();

    String value() default "code";

}
