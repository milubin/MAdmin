package top.horsemuzi.common.validator.custom;

import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HtmlUtil;
import top.horsemuzi.common.validator.custom.annotation.Xss;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author mabin
 * @date 2022/11/28 14:13
 **/

public class XssValidator implements ConstraintValidator<Xss, String> {

    /**
     * 使用@Xss注解时的自定义提示消息
     */
    private String message = StrUtil.EMPTY;

    @Override
    public void initialize(Xss constraint) {
        message = constraint.message();
    }

    @Override
    public boolean isValid(String xss, ConstraintValidatorContext context) {
        if (StrUtil.isBlank(xss)) {
            return true;
        }
        if (StrUtil.isNotBlank(message)) {
            // 自定义提示语支持 {} 或者 %s 模板填充处理
            if (StrUtil.contains(message, StrUtil.EMPTY_JSON)) {
                message = StrUtil.format(message, xss);
            }
            if (StrUtil.contains(message, "%s")) {
                message = String.format(message, xss);
            }
            // 禁用默认提示信息
            context.disableDefaultConstraintViolation();
            // 重新设置提示语
            context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
        }
        return !ReUtil.contains(HtmlUtil.RE_HTML_MARK, xss);
    }

}
