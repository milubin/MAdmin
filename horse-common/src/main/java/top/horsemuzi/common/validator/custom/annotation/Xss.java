package top.horsemuzi.common.validator.custom.annotation;

import top.horsemuzi.common.validator.custom.PhoneValidator;
import top.horsemuzi.common.validator.custom.XssValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 自定义XSS校验注解
 *
 * @author mabin
 * @date 2022/11/28 14:11
 **/

@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = XssValidator.class)
public @interface Xss {

    /**
     * 自定义提示语支持 {} 或者 %s 模板填充处理（目前默认填充：当前参数值）
     */
    String message() default "不允许任何脚本运行";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
