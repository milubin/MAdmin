package top.horsemuzi.common.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Set;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description Jwt载荷信息实体
 * @date 2022/7/10 17:53
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class JwtUser implements Serializable {

    private static final long serialVersionUID = 8982347254825038899L;

    /**
     * 用户id
     */
    private Long id;

    /**
     * 用户名&账号
     */
    private String username;

    /**
     * 头像
     */
    private String avatar;

    /**
     * uuid(JWT校验相关)
     */
    private String uuid;

    /**
     * 是否为管理员(根据roles的值判断)
     */
    private Boolean admin;

    /**
     * 用户角色标识集合
     */
    private Set<String> roles;

    /**
     * 用户权限标识集合
     */
    private Set<String> permissions;

    /**
     * 终端平台类型
     */
    private String terminal;

}
