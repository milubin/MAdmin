package top.horsemuzi.common.convert;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 日期时间转换处理
 *
 * @author mabin
 * @date 2022/06/06 11:39
 **/
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DateConvert {

    /**
     * 获取指定日期的凌晨0点0分0秒
     *
     * @param dateStr yyyy-MM-dd
     * @return
     */
    public static String getStartTimeOfDay(String dateStr) {
        if (StrUtil.isBlank(dateStr)) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(DateUtil.parse(dateStr, DatePattern.NORM_DATE_PATTERN));
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH),
                0, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return DateUtil.format(calendar.getTime(), DatePattern.NORM_DATETIME_PATTERN);
    }


    /**
     * 获取指定日期的凌晨0点0分0秒
     *
     * @param date
     * @return
     */
    public static Date getStartTimeOfDay(Date date) {
        if (ObjectUtil.isNull(date)) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH),
                0, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获取指定日期的23点59分59秒
     *
     * @param dateStr yyyy-MM-dd
     * @return
     */
    public static String getEndTimeOfDay(String dateStr) {
        if (StrUtil.isBlank(dateStr)) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(DateUtil.parse(dateStr, DatePattern.NORM_DATE_PATTERN));
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH),
                23, 59, 59);
        calendar.set(Calendar.MILLISECOND, 0);
        return DateUtil.format(calendar.getTime(), DatePattern.NORM_DATETIME_PATTERN);
    }

    /**
     * 获取指定日期的23点59分59秒
     *
     * @param date
     * @return
     */
    public static Date getEndTimeOfDay(Date date) {
        if (ObjectUtil.isNull(date)) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH),
                23, 59, 59);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获取两个日期范围内所有有效的日期信息(默认精度: yyyy-MM-dd)
     *
     * @param startDate
     * @param endDate
     * @return
     */
    public static List<Date> getRangeDate(Date startDate, Date endDate) {
        List<Date> rangeDateList = new ArrayList<>();
        if (ObjectUtil.hasEmpty(startDate, endDate)) {
            return rangeDateList;
        }
        Calendar startCalendar = Calendar.getInstance();
        startCalendar.setTime(startDate);
        Calendar endCalendar = Calendar.getInstance();
        endCalendar.setTime(endDate);
        while (endCalendar.after(startCalendar)) {
            rangeDateList.add(startCalendar.getTime());
            startCalendar.add(Calendar.DAY_OF_MONTH, 1);
        }
        return rangeDateList;
    }

    /**
     * 获取两个日期范围内所有有效的日期信息(默认精度: yyyy-MM-dd)
     *
     * @param startDate
     * @param endDate
     * @return
     */
    public static List<String> getRangeDateStr(Date startDate, Date endDate) {
        List<String> rangeDateStrList = new ArrayList<>();
        if (ObjectUtil.hasEmpty(startDate, endDate)) {
            return rangeDateStrList;
        }
        Calendar startCalendar = Calendar.getInstance();
        startCalendar.setTime(startDate);
        Calendar endCalendar = Calendar.getInstance();
        endCalendar.setTime(endDate);
        while (endCalendar.after(startCalendar) || endCalendar.equals(startCalendar)) {
            rangeDateStrList.add(DateUtil.formatDate(startCalendar.getTime()));
            startCalendar.add(Calendar.DAY_OF_MONTH, 1);
        }
        return rangeDateStrList;
    }

}
