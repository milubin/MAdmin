package top.horsemuzi.common.enums.generate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * MP自动填充相关枚举处理
 *
 * @author 马滨
 * @date 2022/12/28 13:43
 **/
@Getter
@ToString
@AllArgsConstructor
public enum AutoFillEnum {

    /**
     * 自动填充标识值
     */
    DEFAULT("DEFAULT"),
    INSERT("INSERT"),
    UPDATE("UPDATE"),
    INSERT_UPDATE("INSERT_UPDATE");

    private final String text;
}
