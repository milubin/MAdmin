package top.horsemuzi.common.enums;

import cn.hutool.core.text.CharSequenceUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mr.Horse
 * @version 1.0
 * @description {description}
 * @date 2022/5/28 17:33
 */
@Getter
@AllArgsConstructor
public enum ConfigEnum {

    /**
     * 验证码相关配置
     */
    KAPTCHA_SWITCH(GROUP.KAPTCHA, "kaptcha_switch"),
    KAPTCHA_TYPE(GROUP.KAPTCHA, "kaptcha_type"),
    KAPTCHA_GENERATOR_TYPE(GROUP.KAPTCHA, "kaptcha_generator_type"),

    /**
     * OSS对象存储相关
     */
    OSS_SERVICE(GROUP.OSS, "oss_service"),
    OSS_SYNC_DELETE(GROUP.OSS, "oss_sync_delete"),

    /**
     * 系统文件模板相关
     */
    TEMPLATE_USER_IMPORT(GROUP.TEMPLATE, "template_user_import"),
    TEMPLATE_API_IMPORT(GROUP.TEMPLATE, "template_api_import"),

    /**
     * 系统登录错误相关
     */
    WRONG_PASSWORD_COUNT(GROUP.WRONG_PASSWORD, "wrong_password_count"),
    WRONG_PASSWORD_LOCK_TIME(GROUP.WRONG_PASSWORD, "wrong_password_lock_time"),

    /**
     * Gitee个人主页相关
     */
    GITEE_USER(GROUP.GITEE, "gitee_user"),

    /**
     * 系统业务验证码相关
     * length: 验证码长度: 4/6 (邮箱)
     * type: 验证码类型: 1-纯数字/2-纯字母/3-混合码 (邮箱)
     * time: 验证码缓存有效时间, 单位: 分钟
     * sms_enabled: 系统短信功能是否启用
     * sms_limit: 短信限制, 默认: 5
     * sms_template: 短信模板id
     * email_limit: 邮件限制, 默认: 100
     * email_enabled: 系统邮箱功能是否启用
     */
    VERIFY_LENGTH(GROUP.VERIFY, "verify_length"),
    VERIFY_TYPE(GROUP.VERIFY, "verify_type"),
    VERIFY_SMS_ENABLED(GROUP.VERIFY, "sms_enabled"),
    VERIFY_SMS_LIMIT(GROUP.VERIFY, "sms_limit"),
    VERIFY_SMS_TEMPLATE(GROUP.VERIFY, "sms_template"),
    VERIFY_EMAIL_LIMIT(GROUP.VERIFY, "email_limit"),
    VERIFY_EMAIL_ENABLED(GROUP.VERIFY, "email_enabled"),
    VERIFY_CACHE_TIME(GROUP.VERIFY, "verify_cache_time"),

    // API请求超时时间, 默认: 3分钟
    OPEN_API_TIMEOUT(GROUP.OPEN_PLATFORM, "open_api_timeout"),
    // 开放平台的token有效期, 默认: 2小时
    OPEN_TOKEN_EXPIRE(GROUP.OPEN_PLATFORM, "open_token_expire"),

    // 用户快捷菜单个数限制, 默认: 9
    SHORTCUT_MENU_LIMIT_COUNT(GROUP.USER_SHORTCUT_MENU, "shortcut_menu_limit_count"),

    // 系统管理员邮箱号(英文逗号分隔)
    ADMIN_EMAIL_ACCOUNT(GROUP.ADMIN_EMAIL, "admin_email_account"),

    // 用户实名认证功能是否开启, 开启 (YES), 关闭(NO)
    ADMIN_USER_AUTH_ENABLED(GROUP.ADMIN_USER_AUTH, "admin_user_auth_enabled"),

    // 系统白名单IP限制信息
    SERVER_WHITE_LIST(GROUP.WHITE_LIST, "ip_white_list");

    /**
     * 参数配置组别
     */
    private final String group;

    /**
     * 参数键名
     */
    private final String key;

    /**
     * 参数配置接口
     */
    public interface GROUP {
        // 系统登录验证码
        String KAPTCHA = "KAPTCHA";
        // OSS对象存储
        String OSS = "OSS";
        // 系统文件模板
        String TEMPLATE = "TEMPLATE";
        // 系统登录错误相关
        String WRONG_PASSWORD = "WRONG_PASSWORD";
        // Gitee个人主页相关
        String GITEE = "GITEE";
        // 系统业务验证码配置相关(短信、邮箱等渠道发送验证码)
        String VERIFY = "VERIFY";
        // 开放平台API配置相关
        String OPEN_PLATFORM = "OPEN_PLATFORM";
        // 用户快捷菜单个数相关
        String USER_SHORTCUT_MENU = "USER_SHORTCUT_MENU";
        // 系统管理员邮箱号相关
        String ADMIN_EMAIL = "ADMIN_EMAIL";
        // 用户实名认证功能相关
        String ADMIN_USER_AUTH = "ADMIN_USER_AUTH";
        // 系统白名单限制相关
        String WHITE_LIST = "WHITE_LIST";
    }

    /**
     * 根据组别获取所有配置key
     *
     * @param group ConfigEnum.GROUP
     * @return
     */
    public static List<String> getKeysByGroup(String group) {
        List<String> configKeys = new ArrayList<>();
        if (CharSequenceUtil.isBlank(group)) {
            return configKeys;
        }
        ConfigEnum[] configEnums = values();
        for (ConfigEnum value : configEnums) {
            if (value.group.equals(group)) {
                configKeys.add(value.getKey());
            }
        }
        return configKeys;
    }

}
