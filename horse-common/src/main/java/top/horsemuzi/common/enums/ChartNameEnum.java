package top.horsemuzi.common.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Chart图表名称字符串枚举处理
 *
 * @author mabin
 * @date 2022/09/08 19:40
 **/
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public enum ChartNameEnum {

    /**
     * Chart图表名称
     */
    PIE("pie"),
    LINE("line");

    private String name;

}
