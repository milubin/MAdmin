package top.horsemuzi.common.enums;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * 消息状态枚举处理
 *
 * @author 马滨
 * @date 2022/10/01 11:10
 **/
@Getter
@ToString
@AllArgsConstructor
public enum MessageStatusEnum {

    /**
     * 消息状态
     */
    INIT("0", "初始"),
    SUCCESS("1", "消费成功"),
    ACK_SUCCESS("2", "消费且ACK成功"),
    FAIL("3", "消费失败");


    private final String code;
    private final String text;

    /**
     * 获取状态描述
     *
     * @param code
     * @return
     */
    public static String getByCode(String code) {
        for (MessageStatusEnum value : values()) {
            if (StrUtil.equals(code, value.getCode())) {
                return value.getText();
            }
        }
        return null;
    }
}
