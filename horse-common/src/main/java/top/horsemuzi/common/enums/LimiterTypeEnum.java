package top.horsemuzi.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * 限流的枚举类型
 *
 * @author mabin
 **/
@Getter
@ToString
@AllArgsConstructor
public enum LimiterTypeEnum {

    /**
     * 针对ip进行限流
     */
    IP,

    /**
     * 针对SPEL动态参数限流(灵活性较高)
     */
    SPEL,

    /**
     * 针对用户的id进行限流
     */
    UID,

    /**
     * 针对指定的key进行限流
     */
    KEY;

}
