package top.horsemuzi.common.enums;

import cn.hutool.core.util.ObjectUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * 通知公告发布对象类型枚举处理
 *
 * @author 马滨
 * @date 2022/10/06 16:24
 **/
@Getter
@ToString
@AllArgsConstructor
public enum PublishTypeEnum {

    /**
     * 发布对象类型
     */
    ALL_USER(0, "所有用户"),
    CUSTOM_ROLE(1, "自定义角色"),
    CUSTOM_USER(2, "自定义用户");

    private final Integer code;
    private final String text;

    public static String getByCode(Integer code) {
        for (PublishTypeEnum value : values()) {
            if (ObjectUtil.equals(value.code, code)) {
                return value.text;
            }
        }
        return null;
    }

}
