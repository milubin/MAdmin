package top.horsemuzi.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * 短链类型
 *
 * @author mabin
 * @project MAdmin
 * @date 2023/03/01 14:47
 **/
@Getter
@AllArgsConstructor
@ToString
public enum ShortLinkTypeEnum {

    /**
     * 短链类型
     */
    ORDINARY(0, "普通短链"),
    ACCESS_LIMIT(1, "访问次数限制短链"),
    TIME_LIMIT(2, "时间限制短链"),
    ACCESS_TIME_LIMIT(3, "访问次数和时间限制短链");

    private Integer code;
    private String text;

}
