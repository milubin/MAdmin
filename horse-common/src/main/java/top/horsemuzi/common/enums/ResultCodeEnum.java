package top.horsemuzi.common.enums;

import cn.hutool.http.HttpStatus;
import lombok.ToString;

/**
 * 响应结果枚举处理类
 *
 * @author mabin
 * @date 2022/05/06 14:11
 **/
@ToString
public enum ResultCodeEnum {

    /**
     * 自定义结果枚举信息(特殊状态遵循通用响应码)
     */
    SUCCESS(Boolean.TRUE, HttpStatus.HTTP_OK, "操作成功"),
    PARAM_VERIFY_ERROR(Boolean.FALSE, HttpStatus.HTTP_BAD_REQUEST, "参数错误"),
    NOT_LOGIN(Boolean.FALSE, HttpStatus.HTTP_UNAUTHORIZED, "用户未登录"),
    AUTHENTICATION_FAILED(Boolean.FALSE, HttpStatus.HTTP_UNAUTHORIZED, "鉴权失败"),
    NOT_PERMISSION_ERROR(Boolean.FALSE, HttpStatus.HTTP_FORBIDDEN, "无操作权限, 请联系管理员开通"),
    NOT_ROLE_ERROR(Boolean.FALSE, HttpStatus.HTTP_FORBIDDEN, "无角色权限, 请联系管理员开通"),
    UNSUPPORTED_MEDIA_TYPE(Boolean.FALSE, HttpStatus.HTTP_UNSUPPORTED_TYPE, "不支持的媒体类型"),
    NOT_ACCEPTABLE(Boolean.FALSE, HttpStatus.HTTP_NOT_ACCEPTABLE, "不接受的媒体类型"),
    METHOD_NOT_SUPPORTED(Boolean.FALSE, HttpStatus.HTTP_BAD_METHOD, "不支持的请求类型"),
    NOT_FOUND(Boolean.FALSE, HttpStatus.HTTP_NOT_FOUND, "哎呀, 接口找不到了"),
    INTERNAL_ERROR(Boolean.FALSE, HttpStatus.HTTP_INTERNAL_ERROR, "哎呀, 内部程序报错了!"),

    BAD_SQL_GRAMMAR(Boolean.FALSE, 40000, "SQL执行异常"),
    JSON_PARSE_ERROR(Boolean.FALSE, 40001, "JSON解析异常"),
    KAPTCHA_ERROR(Boolean.FALSE, 40004, "验证码错误"),
    KAPTCHA_EXPIRE(Boolean.FALSE, 40005, "验证码无效"),
    USER_NOT_EXIST(Boolean.FALSE, 40006, "用户不存在"),
    WRONG_USER_OR_PASSWORD(Boolean.FALSE, 40007, "账号或密码错误"),
    ACCOUNT_IS_DISABLED(Boolean.FALSE, 40008, "账号已停用"),
    TOKEN_CREATION_FAILED(Boolean.FALSE, 40009, "token创建失败"),
    SIGN_ERROR(Boolean.FALSE, 40010, "签名错误"),
    FAILED_TO_EXIT(Boolean.FALSE, 40011, "退出失败"),
    GET_USER_INFO_FAIL(Boolean.FALSE, 40012, "获取用户信息失败"),
    OBJECT_ID_IS_NULL(Boolean.FALSE, 40017, "对象实例ID为空"),
    RESOURCE_NOT_EXIST(Boolean.FALSE, 40018, "资源不存在"),
    GET_GLOBAL_REQUEST_ERROR(Boolean.FALSE, 40019, "获取全局request异常"),
    GET_GLOBAL_RESPONSE_ERROR(Boolean.FALSE, 40020, "获取全局response异常"),
    FILE_EXPORT_ERROR(Boolean.FALSE, 40021, "文件导出异常"),
    FILE_IMPORT_ERROR(Boolean.FALSE, 40022, "文件导入异常"),
    FILE_DATA_FORMAT_ERROR(Boolean.FALSE, 40023, "导入文件数据格式不正确"),
    REPEATED_SUBMIT_ERROR(Boolean.FALSE, 40024, "重复提交, 请稍后重试!"),
    BEYOND_LIMIT_ERROR(Boolean.FALSE, 40025, "访问限制异常"),

    USER_EXIST_ERROR(Boolean.FALSE, 40026, "用户已存在"),
    PHONE_EXIST_ERROR(Boolean.FALSE, 40027, "手机号已存在"),
    EMAIL_EXIST_ERROR(Boolean.FALSE, 40028, "邮箱号已存在"),

    DATA_SAVE_ERROR(Boolean.FALSE, 40029, "数据保存执行异常"),
    DATA_UPDATE_ERROR(Boolean.FALSE, 40030, "数据更新执行异常"),
    DATA_DELETE_ERROR(Boolean.FALSE, 40031, "数据删除执行异常"),

    CACHE_KEY_ERROR(Boolean.FALSE, 40032, "缓存键组装异常"),
    DICT_TYPE_REPEAT_ERROR(Boolean.FALSE, 40033, "字典类型重复"),
    DICT_TYPE_NOT_EXIST_ERROR(Boolean.FALSE, 40034, "字典类型不存在"),
    DICT_DATA_REPEAT_ERROR(Boolean.FALSE, 40035, "字典数据重复"),

    NOT_OPERATE_SELF_ACCOUNT(Boolean.FALSE, 40036, "不允许操作本人账号"),
    NOT_OPERATE_INTERNAL_DICT_TYPE(Boolean.FALSE, 40038, "不允许操作内置字典类型"),
    NOT_ALLOW_ADMIN_ROLE(Boolean.FALSE, 40039, "不允许新增管理员角色"),
    ROLE_EXIST_ERROR(Boolean.FALSE, 40040, "角色已存在"),
    NOT_OPERATE_ADMIN_ROLE(Boolean.FALSE, 40041, "不允许操作管理员角色"),
    ROLE_ASSIGNED_USER(Boolean.FALSE, 40042, "角色已分配"),
    ROLE_NOT_EXIST(Boolean.FALSE, 40043, "角色不存在"),
    USER_ALREADY_DEPLOY_ROLE(Boolean.FALSE, 40044, "用户已分配角色"),

    CONFIG_EXIST_ERROR(Boolean.FALSE, 40045, "系统配置已存在"),
    CONFIG_NOT_EXIST_ERROR(Boolean.FALSE, 40046, "系统配置不存在"),
    NOT_OPERATE_INTERNAL_ERROR(Boolean.FALSE, 40047, "不允许操作内置系统配置"),
    KAPTCHA_NOT_SETTING_ERROR(Boolean.FALSE, 40048, "验证码配置异常"),
    KAPTCHA_SMS_NOT_ENABLED_ERROR(Boolean.FALSE, 40049, "短信功能未启用, 请联系管理员"),
    KAPTCHA_EMAIL_NOT_ENABLED_ERROR(Boolean.FALSE, 40050, "邮箱功能未启用, 请联系管理员"),

    PERMISSION_MENU_NAME_EXIST(Boolean.FALSE, 40056, "权限菜单名称重复"),
    PERMISSION_MENU_PARENT_NOT_EXIST(Boolean.FALSE, 40057, "父级权限菜单不存在"),
    PERMISSION_MENU_FRAME_PATH_ERROR(Boolean.FALSE, 40058, "外联路由地址不合法"),
    PERMISSION_NOT_EXIST(Boolean.FALSE, 40059, "权限菜单不存在"),
    PERMISSION_PARENT_NOT_SELF(Boolean.FALSE, 40060, "父级权限菜单不能是自己"),
    PERMISSION_EXIST_RELATE_ERROR(Boolean.FALSE, 40061, "存在关联子菜单"),
    PERMISSION_ROLE_DEPLOY_ERROR(Boolean.FALSE, 40062, "权限菜单已分配"),

    DICT_TYPE_STATUS_ERROR(Boolean.FALSE, 40063, "字典类型已停用"),

    CHECK_ANNOTATION_SETTING_ERROR(Boolean.FALSE, 40070, "权限校验注解配置错误"),

    OSS_STORAGE_NOT_EXIST(Boolean.FALSE, 40080, "OSS对象存储实例不存在"),
    OSS_SERVICE_CONFIG_IS_NULL(Boolean.FALSE, 40081, "OSS对象存储服务商配置信息为空"),
    OSS_SERVICE_UPLOAD_ERROR(Boolean.FALSE, 40082, "文件上传失败"),
    OSS_SERVICE_DOWNLOAD_ERROR(Boolean.FALSE, 40083, "文件下载失败"),
    OSS_SERVICE_DELETE_ERROR(Boolean.FALSE, 40084, "文件删除失败"),
    OSS_SERVICE_PREVIEW_ERROR(Boolean.FALSE, 40085, "文件预览失败"),
    OSS_SERVICE_INTERNAL_ERROR(Boolean.FALSE, 40086, "内置文件不允许删除"),
    OSS_PART_LIMIT_ERROR(Boolean.FALSE, 40087, "OSS分片上传超限"),
    QR_CODE_GENERATE_ERROR(Boolean.FALSE, 40088, "二维码生成失败"),

    LOG_NOT_PRO_ENVIRONMENT_ERROR(Boolean.FALSE, 40090, "开发环境禁止查询"),
    GET_SERVER_INFO_ERROR(Boolean.FALSE, 40091, "获取系统服务信息异常"),
    NOT_ALLOWED_QUIT_SELF_ACCOUNT(Boolean.FALSE, 40092, "不允许强制退出当前登录账号"),

    QUARTZ_JOB_ADD_ERROR(Boolean.FALSE, 40100, "Quartz任务调度创建失败"),
    QUARTZ_JOB_UPDATE_ERROR(Boolean.FALSE, 40101, "Quartz任务调度更新失败"),
    QUARTZ_JOB_DELETE_ERROR(Boolean.FALSE, 40102, "Quartz任务调度删除失败"),
    QUARTZ_JOB_PAUSE_ERROR(Boolean.FALSE, 40103, "Quartz任务调度暂停失败"),
    QUARTZ_JOB_REBOOT_ERROR(Boolean.FALSE, 40104, "Quartz任务调度重启失败"),
    CRON_EXPRESSION_ERROR(Boolean.FALSE, 40105, "cron表达式错误"),
    QUARTZ_JOB_RUN_ONCE_ERROR(Boolean.FALSE, 40106, "Quartz立即运行一次失败"),
    QUARTZ_JOB_MISFIRE_POLICY_ERROR(Boolean.FALSE, 40107, "cron的错误策略错误"),
    QUARTZ_JOB_INTERVAL_ERROR(Boolean.FALSE, 40108, "Quartz调度任务执行间隔错误"),
    QUARTZ_JOB_NOT_EXISTS(Boolean.FALSE, 40109, "Quartz调度任务不存在"),
    QUARTZ_JOB_REPEAT_ERROR(Boolean.FALSE, 40110, "Quartz调度任务重复"),

    TASK_JOB_NOT_EXISTS(Boolean.FALSE, 40120, "Spring调度任务不存在"),
    TASK_JOB_REPEAT_ERROR(Boolean.FALSE, 40121, "Spring调度任务重复"),
    TASK_JOB_PAUSE_ERROR(Boolean.FALSE, 40122, "Spring调度任务暂停失败"),
    TASK_JOB_REBOOT_ERROR(Boolean.FALSE, 40123, "Spring调度任务重启失败"),

    LOGIN_ACCOUNT_LOCK_ERROR(Boolean.FALSE, 40150, "账户已锁定"),
    LOGIN_ACCOUNT_UNLOCK_ERROR(Boolean.FALSE, 40151, "账户解锁失败"),
    BING_ACCOUNT_NOT_ALL_EMPTY_ERROR(Boolean.FALSE, 40152, "至少绑定一个有效账号"),
    PHONE_NOT_ILLEGALITY_ERROR(Boolean.FALSE, 40153, "手机号不合法"),
    EMAIL_NOT_ILLEGALITY_ERROR(Boolean.FALSE, 40154, "邮箱不合法"),
    LOGIN_NEW_AND_OLD_PASSWORD_SAME_ERROR(Boolean.FALSE, 40155, "新旧密码不能相同"),
    USER_THREE_ACCOUNT_SAME_ERROR(Boolean.FALSE, 40156, "用户名和绑定账号(手机号/邮箱)不能相同"),
    VERIFY_THAN_SEND_LIMIT_ERROR(Boolean.FALSE, 40157, "验证码发送次数超出限制, 请明天再试"),
    USER_FORCE_UPDATE_PASSWORD_ERROR(Boolean.FALSE, 40158, "用户强制更新密码策略配置异常"),

    BACKUP_NOT_DEV_ENVIRONMENT_ERROR(Boolean.FALSE, 40170, "非生产环境禁止操作"),

    MESSAGE_PARAMS_ERROR(Boolean.FALSE, 40200, "消息发送参数异常"),
    MESSAGE_TYPE_NOT_EXIST_ERROR(Boolean.FALSE, 40201, "消息类型不存在"),
    MESSAGE_NOT_EXIST_ERROR(Boolean.FALSE, 40202, "消息信息不存在"),
    MESSAGE_CHANNEL_NOT_EXIST_ERROR(Boolean.FALSE, 40203, "消息渠道不存在"),
    MESSAGE_HANDLER_NOT_EXIST_ERROR(Boolean.FALSE, 40204, "消息服务不存在"),
    MESSAGE_CHANNEL_REPEAT_ERROR(Boolean.FALSE, 40205, "消息渠道重复"),
    MESSAGE_CHANNEL_PARAMS_ERROR(Boolean.FALSE, 40206, "消息渠道参数配置异常"),


    NOTICE_PUBLISH_STATUS_ERROR(Boolean.FALSE, 40250, "通知公告发布对象状态异常"),
    NOTICE_NOT_EXIST_ERROR(Boolean.FALSE, 40251, "通知公告不存在"),
    NOTICE_STATUS_ERROR(Boolean.FALSE, 40252, "已发布状态不允许操作"),
    NOTICE_RECORD_NOT_EXIST_ERROR(Boolean.FALSE, 40253, "通知公告记录不存在"),
    NOTICE_RECORD_NOT_READ_ERROR(Boolean.FALSE, 40254, "未读状态不允许删除"),
    NOTICE_REVOKE_STATUS_ERROR(Boolean.FALSE, 40255, "未发布状态不允许撤回"),
    NOTICE_RECORD_STATUS_ERROR(Boolean.FALSE, 40256, "记录状态异常"),

    DIVISION_NOT_REPEAT_ERROR(Boolean.FALSE, 40300, "行政区划不能重复"),
    DIVISION_PARENT_NOT_EXIST_ERROR(Boolean.FALSE, 40301, "父级行政区划不存在"),
    DIVISION_CODE_ERROR(Boolean.FALSE, 40302, "区划编码不合法"),
    DIVISION_SHORT_NAME_ERROR(Boolean.FALSE, 40303, "此等级区划无法设置简称"),
    DIVISION_NOT_EXIST_ERROR(Boolean.FALSE, 40304, "行政区划不存在"),
    DIVISION_SON_EXIST_ERROR(Boolean.FALSE, 40305, "子级行政区划存在"),

    SPRING_BEAN_IS_NULL_ERROR(Boolean.FALSE, 40310, "容器实例不存在"),


    OPEN_APP_NAME_REPEAT_ERROR(Boolean.FALSE, 40350, "开放API应用名称重复"),
    CREDIT_CODE_NOT_ILLEGALITY_ERROR(Boolean.FALSE, 40351, "社会统一信用码不合法"),
    OPEN_APP_NOT_EXIST_ERROR(Boolean.FALSE, 40352, "开放API应用不存在"),
    OPEN_API_NOT_EXIST_ERROR(Boolean.FALSE, 40353, "开放API接口不存在"),
    OPEN_API_CATEGORY_NOT_EXIST_ERROR(Boolean.FALSE, 40354, "开放API接口分类不存在"),
    OPEN_API_CATEGORY_EXIST_RELATION_ERROR(Boolean.FALSE, 40355, "开放API接口分类存在关联"),
    OPEN_API_NAME_REPEAT_ERROR(Boolean.FALSE, 40356, "开放API接口重复"),
    OPEN_APP_APPROVED_COMPLETE_ERROR(Boolean.FALSE, 40357, "开放API应用已审批完成"),


    API_REQUEST_TIMEOUT_ERROR(Boolean.FALSE, 50001, "请求超时"),
    API_SIGN_ERROR(Boolean.FALSE, 50002, "签名错误"),
    API_APP_NOT_EXIST_ERROR(Boolean.FALSE, 50003, "API应用不存在"),
    API_TOKEN_ERROR(Boolean.FALSE, 50004, "token已失效"),
    API_NOT_EXIST_ERROR(Boolean.FALSE, 50005, "API接口不存在"),
    API_SERVER_ERROR(Boolean.FALSE, 50006, "服务异常"),
    API_STATUS_ERROR(Boolean.FALSE, 50007, "API接口已下线"),
    API_APP_STATUS_ERROR(Boolean.FALSE, 50008, "API应用状态异常"),
    API_APP_IP_WHITE_LIST_LIMIT_ERROR(Boolean.FALSE, 50009, "API应用IP限制异常"),
    API_DAILY_REQUEST_LIMIT_ERROR(Boolean.FALSE, 50010, "API接口日请求超限"),

    GENERATE_DATASOURCE_NOT_EXIST_ERROR(Boolean.FALSE, 50050, "数据源不存在"),
    GENERATE_DATASOURCE_REPEAT_ERROR(Boolean.FALSE, 50051, "数据源重复"),
    GENERATE_DATASOURCE_RELATION_TABLE_ERROR(Boolean.FALSE, 50052, "存在业务表关联数据源"),
    GENERATE_COLUMN_CONFIG_REPEAT_ERROR(Boolean.FALSE, 50053, "字段类型关联重复"),
    GENERATE_COLUMN_CONFIG_NOT_EXIST_ERROR(Boolean.FALSE, 50054, "字段类型关联不存在"),
    GENERATE_TABLE_CONFIG_REPEAT_ERROR(Boolean.FALSE, 50055, "业务表配置重复"),
    GENERATE_TABLE_CONFIG_NOT_EXIST_ERROR(Boolean.FALSE, 50056, "业务表配置不存在"),
    GENERATE_TABLE_CONFIG_RELATION_TABLE_ERROR(Boolean.FALSE, 50057, "存在业务表关联配置"),
    GENERATE_DATASOURCE_CONNECT_ERROR(Boolean.FALSE, 50058, "数据源连接异常"),
    GENERATE_TABLE_NOT_EXIST_ERROR(Boolean.FALSE, 50059, "业务表不存在"),
    GENERATE_TABLE_COLUMN_NOT_EXIST_ERROR(Boolean.FALSE, 50060, "业务表字段不存在"),
    GENERATE_TABLE_GENERATE_CODE_ERROR(Boolean.FALSE, 50061, "业务表代码生成失败"),
    GENERATE_DATASOURCE_EXECUTE_ERROR(Boolean.FALSE, 50062, "数据源连接执行异常"),
    GENERATE_TABLE_REPEAT_ERROR(Boolean.FALSE, 50063, "业务表重复"),
    GENERATE_DATABASE_NOT_EXIST_ERROR(Boolean.FALSE, 50064, "数据库不存在"),
    GENERATE_COLUMN_CONFIG_PACKAGE_FORMAT_ERROR(Boolean.FALSE, 50065, "字段类型依赖包格式错误"),
    GENERATE_TABLE_COLUMN_PRIMARY_REPEAT_ERROR(Boolean.FALSE, 50066, "主键字段重复"),
    GENERATE_TABLE_COLUMN_REPEAT_ERROR(Boolean.FALSE, 50067, "业务表字段重复"),
    GENERATE_TABLE_CONFIG_SUPPLER_CLASS_NOT_EXIST_ERROR(Boolean.FALSE, 50068, "类路径异常"),
    GENERATE_TABLE_CONFIG_FILE_FORMAT_NAME_ERROR(Boolean.FALSE, 50069, "业务表配置文件格式化名称异常"),
    GENERATE_TABLE_CONFIG_DATE_FORMAT_INVALID_ERROR(Boolean.FALSE, 50070, "业务表配置注释日期格式无效"),
    GENERATE_TEMPLATE_FILE_NOT_EXIST_ERROR(Boolean.FALSE, 50071, "模板文件不存在"),
    GENERATE_BASE_CLASS_NOT_EXIST_ERROR(Boolean.FALSE, 50072, "基类不存在"),


    USER_SHORTCUT_MENU_REPEAT_ERROR(Boolean.FALSE, 50100, "个人快捷菜单重复"),
    USER_SHORTCUT_MENU_NOT_EXIST_ERROR(Boolean.FALSE, 50101, "个人快捷菜单不存在"),
    USER_SHORTCUT_MENU_LIMIT_ERROR(Boolean.FALSE, 50102, "个人快捷菜单数量超出限制"),

    ERROR_CODE_REPEAT_ERROR(Boolean.FALSE, 50110, "系统状态码菜单重复"),
    ERROR_CODE_NOT_EXIST_ERROR(Boolean.FALSE, 50111, "系统状态码不存在"),
    ERROR_CODE_ENUM_NAME_NOT_UPPER_ERROR(Boolean.FALSE, 50112, "系统状态码枚举名称未大写"),
    ERROR_CODE_LOCAL_CODE_NOT_EXIST_ERROR(Boolean.FALSE, 50113, "本地系统状态码不存在"),

    SHORT_LINK_REPEAT_ERROR(Boolean.FALSE, 50120, "短链信息重复"),
    SHORT_LINK_NOT_EXIST_ERROR(Boolean.FALSE, 50121, "短链信息不存在"),
    SHORT_LINK_LOG_REPEAT_ERROR(Boolean.FALSE, 50122, "短链日志信息重复"),
    SHORT_LINK_LOG_NOT_EXIST_ERROR(Boolean.FALSE, 50123, "短链日志信息不存在"),
    SHORT_LINK_EXIST_SHORT_LINK_UPDATE_ERROR(Boolean.FALSE, 50125, "已生成短链接不允许修改"),
    SHORT_LINK_ACCESS_COUNT_NULL_ERROR(Boolean.FALSE, 50126, "短链访问次数不能为空"),
    SHORT_LINK_INVALID_TIME_NULL_ERROR(Boolean.FALSE, 50127, "短链失效时间不能为空"),
    SHORT_LINK_INVALID_TIME_SETTING_ERROR(Boolean.FALSE, 50128, "短链失效时间不能小于当前时间"),
    SHORT_LINK_GENERATE_CODE_ERROR(Boolean.FALSE, 50129, "短链生成异常"),
    SHORT_LINK_REDIRECT_ERROR(Boolean.FALSE, 50130, "短链重定向异常"),

    OSS_CONFIG_REPEAT_ERROR(Boolean.FALSE, 50150, "OSS对象存储配置重复"),
    OSS_CONFIG_NOT_EXIST_ERROR(Boolean.FALSE, 50151, "OSS对象存储配置不存在"),
    USER_REAL_NAME_AUTH_ERROR(Boolean.FALSE, 50160, "用户实名认证异常"),
    USER_REAL_NAME_AUTH_NOT_ENABLED_ERROR(Boolean.FALSE, 50161, "用户实名认证功能服务未开启"),
    REQUEST_IP_LIMIT_ERROR(Boolean.FALSE, 50162, "IP白名单限制异常");


    private Boolean success;

    private Integer code;

    private String message;

    ResultCodeEnum(Boolean success, Integer code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

}
