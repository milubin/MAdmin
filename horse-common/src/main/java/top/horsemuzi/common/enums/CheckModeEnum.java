package top.horsemuzi.common.enums;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * 权限校验模式
 *
 * @author mabin
 * @date 2022/07/19 09:25
 **/
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public enum CheckModeEnum {

    /**
     * 必须具有所有元素
     */
    ADD,

    /**
     * 只需要具备其中一个元素即可
     */
    OR;

}
