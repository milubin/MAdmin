package top.horsemuzi.common.enums;

import cn.hutool.core.util.DesensitizedUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.function.Function;

/**
 * 脱敏策略枚举处理
 *
 * @author mabin
 * @date 2022/09/29 11:33
 **/
@Getter
@ToString
@AllArgsConstructor
public enum SensitiveStrategyEnum {

    /**
     * 身份证脱敏
     */
    ID_CARD(s -> DesensitizedUtil.idCardNum(s, 3, 4)),

    /**
     * 手机号脱敏
     */
    PHONE(DesensitizedUtil::mobilePhone),

    /**
     * 地址脱敏
     */
    ADDRESS(s -> DesensitizedUtil.address(s, 8)),

    /**
     * 邮箱脱敏
     */
    EMAIL(DesensitizedUtil::email),

    /**
     * 银行卡
     */
    BANK_CARD(DesensitizedUtil::bankCard);

    /**
     * 可自定义实现脱敏实现
     */
    private Function<String, String> desensitizer;


}
