package top.horsemuzi.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * 用户登录类型枚举处理
 *
 * @author mabin
 * @date 2022/09/28 15:33
 **/
@Getter
@ToString
@AllArgsConstructor
public enum LoginTypeEnum {

    /**
     * 登录方式
     */
    PASSWORD(1),
    SMS(2),
    EMAIL(3);

    private Integer type;


}
