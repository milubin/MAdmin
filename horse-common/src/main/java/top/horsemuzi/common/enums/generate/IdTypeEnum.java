package top.horsemuzi.common.enums.generate;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 代码生成器主键类型枚举处理
 *
 * @author mabin
 * @date 2022/12/20 13:53
 **/
@Getter
@ToString
@AllArgsConstructor
public enum IdTypeEnum {

    /**
     * 主键自增
     */
    AUTO(0) {
        /**
         * @param menuId 最大权限菜单id
         * @return 默认返回10个权限菜单id
         */
        @Override
        public List<Object> generateMenuIds(Object menuId) {
            List<Object> menuIds = new ArrayList<>();
            Long longMenuId = Convert.toLong(menuId) + 1;
            menuIds.add(longMenuId);
            for (int i = 0; i < 9; i++) {
                menuIds.add(++longMenuId);
            }
            return menuIds;
        }
    },

    /**
     * 雪花算法
     */
    ASSIGN_ID(1) {
        @Override
        public List<Object> generateMenuIds(Object menuId) {
            List<Object> menuIds = new ArrayList<>();
            Snowflake snowflake = IdUtil.getSnowflake();
            for (int i = 0; i < 10; i++) {
                menuIds.add(snowflake.nextId());
            }
            return menuIds;
        }
    },

    /**
     * UUID
     */
    ASSIGN_UUID(2) {
        @Override
        public List<Object> generateMenuIds(Object menuId) {
            List<Object> menuIds = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                menuIds.add(IdUtil.fastSimpleUUID());
            }
            return menuIds;
        }
    };

    private final Integer type;

    protected abstract List<Object> generateMenuIds(Object menuId);


    /**
     * 生成权限菜单id列表
     *
     * @param type
     * @param menuId
     * @return
     */
    public static List<Object> generateMenuIdList(Integer type, @Nullable Object menuId) {
        for (IdTypeEnum anEnum : values()) {
            if (ObjectUtil.equals(anEnum.getType(), type)) {
                return anEnum.generateMenuIds(menuId);
            }
        }
        return Collections.emptyList();
    }


}
