package top.horsemuzi.common.enums.generate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * 表单类型枚举处理
 *
 * @author mabin
 * @date 2022/12/27 11:42
 **/
@Getter
@ToString
@AllArgsConstructor
public enum FormTypeEnum {

    /**
     * 表单类型枚举处理
     */
    INPUT("input", "单行文本"),
    TEXTAREA("textarea", "多行文本"),
    RICHTEXT("richText", "富文本"),
    SELECT("select", "下拉框"),
    RADIO("radio", "单选按钮"),
    CHECKBOX("checkbox", "富文本"),
    DATE("date", "下拉框"),
    DATETIME("dateTime", "单选按钮");


    /**
     * 表单类型
     */
    private final String type;

    /**
     * 类型说明
     */
    private final String text;

}
