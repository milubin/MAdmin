package top.horsemuzi.common.enums;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import top.horsemuzi.common.util.EncryptUtil;

/**
 * 开放API签名生成枚举处理
 *
 * @author mabin
 * @date 2022/11/24 15:47
 **/
@Getter
@AllArgsConstructor
@ToString
public enum ApiSignEnum {

    /**
     * 生成签名处理
     */
    MD5("md5") {
        @Override
        public String createSign(String plainText) {
            return EncryptUtil.md5(plainText);
        }
    },
    SHA1("sha1") {
        @Override
        public String createSign(String plainText) {
            return EncryptUtil.sha1(plainText);
        }
    },
    SHA256("sha256") {
        @Override
        public String createSign(String plainText) {
            return EncryptUtil.sha256(plainText);
        }
    };

    /**
     * 签名方法类型
     */
    private String signMethod;

    /**
     * 签名处理
     *
     * @param plainText
     * @return
     */
    protected abstract String createSign(String plainText);


    /**
     * 生成签名
     *
     * @param signMethod 签名方法类型
     * @param plainText  纯文本
     * @return {@code String}
     */
    public static String sign(String signMethod, String plainText) {
        for (ApiSignEnum apiSignEnum : values()) {
            if (StrUtil.equals(signMethod, apiSignEnum.signMethod)) {
                return apiSignEnum.createSign(plainText);
            }
        }
        return null;
    }


}
