package top.horsemuzi.common.wrapper;

import cn.hutool.core.io.IoUtil;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * 对HttpServletRequest进行重写包装
 * 处理目的: 解决使用getInputStream或getReader在拦截器中获取会导致控制器层拿到的参数为空
 *
 * @author mabin
 * @date 2022/05/16 15:18
 **/

public class RequestWrapper extends HttpServletRequestWrapper {

    private byte[] requestBody;

    private HttpServletRequest request;

    public RequestWrapper(HttpServletRequest request) {
        super(request);
        this.request = request;
    }

    public RequestWrapper(HttpServletRequest request, byte[] body) {
        super(request);
        this.request = request;
        this.requestBody = body;
    }


    @Override
    public ServletInputStream getInputStream() throws IOException {
        if (null == this.requestBody) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            IoUtil.copy(request.getInputStream(), baos);
            this.requestBody = baos.toByteArray();
        }
        final ByteArrayInputStream bais = new ByteArrayInputStream(requestBody);
        return new ServletInputStream() {
            @Override
            public boolean isFinished() {
                return false;
            }

            @Override
            public boolean isReady() {
                return false;
            }

            @Override
            public void setReadListener(ReadListener listener) {

            }

            @Override
            public int read() throws IOException {
                return bais.read();
            }
        };
    }

    @Override
    public BufferedReader getReader() throws IOException {
        return new BufferedReader(new InputStreamReader(this.getInputStream(), StandardCharsets.UTF_8));
    }

}
