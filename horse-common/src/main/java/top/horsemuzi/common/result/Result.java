package top.horsemuzi.common.result;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.slf4j.MDC;
import top.horsemuzi.common.constants.Constants;
import top.horsemuzi.common.enums.ResultCodeEnum;
import top.horsemuzi.common.i18n.I18nMessageFactory;

import java.io.Serializable;

/**
 * 统一响应结果实体类
 *
 * @author mabin
 * @date 2022/05/06 14:31
 **/
@ApiModel(value = "响应实体")
@Data
public class Result<T> implements Serializable {

    private static final long serialVersionUID = 2350376184359492975L;

    @ApiModelProperty(value = "是否成功: true-成功,false-失败")
    private Boolean success;

    @ApiModelProperty(value = "响应码")
    private Integer code;

    @ApiModelProperty(value = "响应消息")
    private String message;

    @ApiModelProperty(value = "响应信息")
    private T data;

    @ApiModelProperty(value = "时间戳")
    private Long timestamp;

    @ApiModelProperty(value = "链路ID")
    private String traceId;

    /**
     * 私有化响应结果类,不允许实例化操作
     */
    private Result() {
        // 设置时间戳
        this.timestamp = System.currentTimeMillis();
        // 设置全局链路ID
        this.traceId = MDC.get(Constants.TRACE_ID);
    }

    /**
     * 成功 默认消息 无数据
     *
     * @param <T>
     * @return
     */
    public static <T> Result<T> success() {
        Result<T> result = new Result<>();
        result.setSuccess(ResultCodeEnum.SUCCESS.getSuccess());
        result.setCode(ResultCodeEnum.SUCCESS.getCode());
        result.setMessage(getI18nMessage(ResultCodeEnum.SUCCESS.name()));
        return result;
    }

    /**
     * 成功 默认消息 有数据
     *
     * @param <T>
     * @return
     */
    public static <T> Result<T> success(T data) {
        return success(getI18nMessage(ResultCodeEnum.SUCCESS.name()), data);
    }

    /**
     * 成功 自定义消息 无数据
     *
     * @param <T>
     * @return
     */
    public static <T> Result<T> success(String message) {
        return success(message, null);
    }

    /**
     * 成功 自定义消息 有数据
     *
     * @param <T>
     * @return
     */
    public static <T> Result<T> success(String message, T data) {
        Result<T> result = new Result<>();
        result.setSuccess(ResultCodeEnum.SUCCESS.getSuccess());
        result.setCode(ResultCodeEnum.SUCCESS.getCode());
        result.setMessage(message);
        result.setData(data);
        return result;
    }

    /**
     * 内部错误 默认消息 无数据
     *
     * @param <T>
     * @return
     */
    public static <T> Result<T> error() {
        Result<T> result = new Result<>();
        result.setSuccess(ResultCodeEnum.INTERNAL_ERROR.getSuccess());
        result.setCode(ResultCodeEnum.INTERNAL_ERROR.getCode());
        result.setMessage(getI18nMessage(ResultCodeEnum.INTERNAL_ERROR.name()));
        return result;
    }

    /**
     * 内部错误 默认消息 无数据
     *
     * @param <T>
     * @return
     */
    public static <T> Result<T> error(T data) {
        return error(getI18nMessage(ResultCodeEnum.INTERNAL_ERROR.name()), data);
    }

    /**
     * 内部错误 自定义消息 无数据
     *
     * @param <T>
     * @return
     */
    public static <T> Result<T> error(String message) {
        return error(message, null);
    }

    public static <T> Result<T> error(Integer code, String message) {
        Result<T> result = new Result<>();
        result.setSuccess(ResultCodeEnum.INTERNAL_ERROR.getSuccess());
        result.setCode(code);
        result.setMessage(message);
        return result;
    }

    /**
     * 内部错误 自定义消息 有数据
     *
     * @param <T>
     * @return
     */
    public static <T> Result<T> error(String message, T data) {
        Result<T> result = new Result<>();
        result.setSuccess(ResultCodeEnum.INTERNAL_ERROR.getSuccess());
        result.setCode(ResultCodeEnum.INTERNAL_ERROR.getCode());
        result.setMessage(message);
        result.setData(data);
        return result;
    }

    /**
     * 内部错误 自定义错误码&消息 无数据
     *
     * @param <T>
     * @return
     */
    public static <T> Result<T> error(ResultCodeEnum codeEnum) {
        Result<T> result = new Result<>();
        result.setSuccess(codeEnum.getSuccess());
        result.setCode(codeEnum.getCode());
        result.setMessage(getI18nMessage(codeEnum.name()));
        return result;
    }

    /**
     * 内部错误 自定义错误码&消息 有数据
     *
     * @param <T>
     * @return
     */
    public static <T> Result<T> error(ResultCodeEnum codeEnum, T data) {
        Result<T> result = new Result<>();
        result.setSuccess(codeEnum.getSuccess());
        result.setCode(codeEnum.getCode());
        result.setMessage(getI18nMessage(codeEnum.name()));
        result.setData(data);
        return result;
    }

    /**
     * 获取国际化消息
     *
     * @param codeEnumName
     * @return java.lang.String
     * @author mabin
     * @date 2023/4/8 14:58
     **/
    private static String getI18nMessage(String codeEnumName) {
        return I18nMessageFactory.message(StrUtil.replace(codeEnumName, StrUtil.UNDERLINE, StrUtil.DOT).toLowerCase());
    }

}
