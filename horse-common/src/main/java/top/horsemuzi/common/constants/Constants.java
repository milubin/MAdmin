package top.horsemuzi.common.constants;

import cn.hutool.core.util.EnumUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import top.horsemuzi.common.enums.ResultCodeEnum;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 全局统一常量实体
 *
 * @author mabin
 * @date 2022/05/06 14:35
 **/
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constants {

    /**
     * 全局日志追踪标识
     */
    public static final String TRACE_ID = "traceId";

    /**
     * access_token访问令牌标识名称(贯穿前后端虚拟token的标识)
     */
    public static final String ACCESS_TOKEN = "token";

    /**
     * redis默认过期时间
     */
    public static final Long REDIS_DEFAULT_EXPIRE_TIME = 5L;

    /**
     * 管理员权限
     */
    public static final String ADMIN_PERMISSION = "*:*:*";

    /**
     * excel导入单次缓存的数量
     */
    public static final Integer EXCEL_BATCH_COUNT = 100;

    /**
     * 本地IP/区域
     */
    public static final String INTRANET_IP = "127.0.0.1";
    public static final String INTRANET_AREA = "内网IP";

    /**
     * 系统临时文件夹
     * Windows: C:\Documents and Settings\用户名\Local Settings\Temp
     * Linux: /tmp
     */
    public static final String TEMP_DOC = System.getProperty("java.io.tmpdir");

    /**
     * 非严谨意义上的操作方法类型(为了兼容日志记录的导入、导出操作等)
     */
    public interface METHOD {
        String GET = "GET";
        String POST = "POST";
        String PUT = "PUT";
        String DELETE = "DELETE";
        String EXPORT = "EXPORT";
        String IMPORT = "IMPORT";
        String REFRESH = "REFRESH";
        String EXCEPTION = "EXCEPTION";
        String TEST = "TEST";
        String PREVIEW = "PREVIEW";
        String DOWNLOAD = "DOWNLOAD";
    }

    /**
     * 系统通用各类型数字标识
     */
    public interface COMMON_CODE {
        Integer BURDEN_ONE = -1;
        int BURDEN_BASE_INT_ONE = -1;
        Long BURDEN_LONG_ONE = -1L;
        long BURDEN_BASE_LONG_ONE = -1;
        String BURDEN_STR_ONE = "-1";

        Integer ZONE = 0;
        int BASE_INT_ZONE = 0;
        Long LONG_ZONE = 0L;
        long BASE_LONG_ZONE = 0;
        String STR_ZONE = "0";

        Integer ONE = 1;
        int BASE_INT_ONE = 1;
        Long LONG_ONE = 1L;
        long BASE_LONG_ONE = 1;
        String STR_ONE = "1";

        Integer TWO = 2;
        int BASE_INT_TWO = 2;
        Long LONG_TWO = 2L;
        long BASE_LONG_TWO = 2;
        String STR_TWO = "2";

        Integer THREE = 3;
        int BASE_INT_THREE = 3;
        Long LONG_THREE = 3L;
        long BASE_LONG_THREE = 3;
        String STR_THREE = "3";

        Integer FOUR = 4;
        int BASE_INT_FOUR = 4;
        Long LONG_FOUR = 4L;
        long BASE_LONG_FOUR = 4;
        String STR_FOUR = "4";

        Integer FIVE = 5;
        int BASE_INT_FIVE = 5;
        Long LONG_FIVE = 5L;
        long BASE_LONG_FIVE = 5;
        String STR_FIVE = "5";

        Integer SIX = 6;
        int BASE_INT_SIX = 6;
        Long LONG_SIX = 6L;
        long BASE_LONG_SIX = 6;
        String STR_SIX = "6";

        Integer SEVEN = 7;
        int BASE_INT_SEVEN = 7;
        Long LONG_SEVEN = 7L;
        long BASE_LONG_SEVEN = 7;
        String STR_SEVEN = "7";

        Integer EIGHT = 8;
        int BASE_INT_EIGHT = 8;
        Long LONG_EIGHT = 8L;
        long BASE_LONG_EIGHT = 8;
        String STR_EIGHT = "8";

        Integer NINE = 9;
        int BASE_INT_NINE = 9;
        Long LONG_NINE = 9L;
        long BASE_LONG_NINE = 9;
        String STR_NINE = "9";

        Integer TEN = 10;
        int BASE_INT_TEN = 10;
        Long LONG_TEN = 10L;
        long BASE_LONG_TEN = 10;
        String STR_TEN = "10";

        Integer TWENTY_FOUR = 24;
        int BASE_TWENTY_FOUR = 24;
        Long LONG_TWENTY_FOUR = 24L;
        long BASE_LONG_TWENTY_FOUR = 24;
        String STR_TWENTY_FOUR = "24";

        Integer ONE_HUNDRED = 100;
        int BASE_INT_ONE_HUNDRED = 100;

        Integer TWO_HUNDRED = 200;
        int BASE_INT_TWO_HUNDRED = 200;

        Integer THREE_HUNDRED = 300;
        int BASE_THREE_HUNDRED = 300;

        Integer ONE_THOUSAND = 1000;
        int BASE_INT_ONE_THOUSAND = 1000;

        Integer FIVE_THOUSAND = 5000;
        int BASE_INT_FIVE_THOUSAND = 5000;

    }

    /**
     * 系统通用判断值
     */
    public interface COMMON_JUDGMENT {
        String YES = "YES";
        String NO = "NO";

        String Y = "Y";
        String N = "N";

        /**
         * 一般作为开关标识: ON-开放, OFF-关闭
         */
        String ON = "ON";
        String OFF = "OFF";

        String BEGIN = "BEGIN";
        String END = "END";

        /**
         * 一般作为状态标识: 0-正常, 1-停用
         */
        Integer ONE = 1;
        Integer ZONE = 0;
        String ONE_STR = "1";
        String ZONE_STR = "0";
    }

    /**
     * redis缓存前缀
     */
    public interface REDIS_PREFIX {
        // 默认前缀
        String COMMON = "redis";
        // 系统参数config相关
        String CONFIG = "config";
        // 验证码相关
        String KAPTCHA = "kaptcha:";
        // token相关
        String TOKEN = "token";
        // 字典数据相关
        String DICT_DATA = "dict:data";
        // 密码错误限制相关
        String WRONG_PWD = "wrong_pwd";
        // 菜单权限标识相关
        String MENU_PERMS = "perms";
        // 菜单路由相关
        String MENU_ROUTER = "menu_router";
        // 菜单权限树相关
        String MENU_TREE = "menu_tree";
        // 消息渠道配置相关
        String MESSAGE_CHANNEL = "message_channel";
        // 钉钉工作通知发送消息的token相关
        String MESSAGE_DING_WORK_TOKEN = "message_ding_work_token";
        // 系统业务验证码相关
        String VERIFY_CODE = "verify_code";
        // 开放API相关
        String OPEN_APP = "open:app";
        String OPEN_API = "open:api";
        String OPEN_TOKEN = "open:token";
        String OPEN_API_DAILY_LIMIT = "open:limit";
        // 用户快捷菜单相关
        String SHORTCUT_MENU = "shortcut_menu";
        // 短链平台相关
        String SHORT_LINK_BASE_ID = "short_link_base_id";
        String SHORT_LINK_BLOOM_FILTER = "short_link_bloom_filter";
        String SHORT_LINK_INFO = "short_link_info";
        // oss对象存储配置相关
        String OSS_CONFIG = "oss_config";
    }

    /**
     * 角色标识相关
     */
    public interface ROLE_CODE {

        /**
         * 管理员
         */
        String ADMIN = "admin";

        /**
         * 普通角色
         */
        String COMMON = "common";
    }

    /**
     * 菜单路由相关
     */
    public interface MENU {
        /**
         * 是否菜单外链（是）
         */
        String YES_FRAME = "0";

        /**
         * 是否菜单外链（否）
         */
        String NO_FRAME = "1";

        /**
         * 菜单类型（目录）
         */
        String TYPE_DIR = "M";

        /**
         * 菜单类型（菜单）
         */
        String TYPE_MENU = "C";

        /**
         * 菜单类型（按钮）
         */
        String TYPE_BUTTON = "F";

        /**
         * Layout组件标识
         */
        String LAYOUT = "Layout";

        /**
         * ParentView组件标识
         */
        String PARENT_VIEW = "ParentView";

        /**
         * InnerLink组件标识
         */
        String INNER_LINK = "InnerLink";

        /**
         * 外链标识
         */
        String HTTP = "http://";
        String HTTPS = "https://";
    }

    /**
     * 终端类型
     */
    public interface TERMINAL {
        String PC = "PC";
        String Android = "Android";
    }

    /**
     * 媒体类型配置
     */
    public interface MIME_TYPE {
        String[] IMAGE_EXTENSION = {"bmp", "gif", "jpg", "jpeg", "png"};

        String[] FLASH_EXTENSION = {"swf", "flv"};

        String[] MEDIA_EXTENSION = {"swf", "flv", "mp3", "wav", "wma", "wmv", "mid", "avi", "mpg", "asf", "rm", "rmvb"};

        String[] VIDEO_EXTENSION = {"mp4", "avi", "rmvb"};

        String[] DEFAULT_ALLOWED_EXTENSION = {
                // 图片
                "bmp", "gif", "jpg", "jpeg", "png",
                // word excel powerpoint
                "doc", "docx", "xls", "xlsx", "ppt", "pptx", "html", "htm", "txt",
                // 压缩文件
                "rar", "zip", "gz", "bz2",
                // 视频格式
                "mp4", "avi", "rmvb",
                // pdf
                "pdf"};
    }

}
